/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "input.h"
#include "error.h"

#include <ios>
#include "output_impl.h"

namespace read_data {
marker_data read_marker(file& is, int first, int last) {
    if (!is.good()) {
        throw input_exception(is, "couldn't read from file");
    }
    char tr[256];
    for (int i = 0; i < 256; ++i) { tr[i] = i; }

    marker_data ret;
    std::string s;
    int n_obs;
    is >> std::noskipws;
    is >> s; if (s != "data") { throw input_exception(is, "expected 'data'"); }
    ws_nonl(is);
    is >> s; if (s != "type") { throw input_exception(is, "expected 'type'"); }
    ws_nonl(is);
    /*is >> ret.data_type;*/
    /*ws_nl(is);*/
    getline(is, ret.data_type);
    ret.n_mark = ret.n_obs = 0;
    is >> n_obs;
    if (n_obs == 0) { throw input_exception(is, "Expected the number of observed organisms"); }
    ws_nonl(is);
    is >> ret.n_mark;
    if (ret.n_mark == 0) { throw input_exception(is, "Expected the number of observed markers"); }

    /*MSG_DEBUG("first=" << first << " last=" << last);*/
    if (first != -1) {
        --first;
        if (last == -1 || last >= n_obs) {
            /* whine */
            last = n_obs - 1;
        } else {
            --last;
        }
    } else {
        first = 0;
        last = n_obs - 1;
    }
    ret.n_obs = last - first + 1;
    /*MSG_DEBUG("first=" << first << " last=" << last);*/

    while (!ws_opt_nl(is)) {
        is >> s;
        if (s.size() == 3 && s[1] == '=') {
            tr[(int)s[0]] = s[2];
        }
    }
    for (size_t nm = 0; !is.eof() && nm < ret.n_mark; ++nm) {
        s = read_star_name(is);
        std::stringstream mdat;
        char c;
        for (int no = 0; !is.eof() && no < n_obs; ++no) {
            ws_nonl(is);
            c = tr[is.get()];
            switch (c) {
                case '\r':
                case '\n':
                    throw input_exception(is, "Expected more marker observations");
                /*case ' ':*/
                /*case '\t':*/
                    /*break;*/
                default:
                    if (no >= first && no <= last) {
                        mdat << c;
                    }
            };
        }
        ws_nl(is);
        ret.data[s] = mdat.str();
    }
    if (!is.eof()) {
        MSG_WARNING("Warning, read " << ret.data.size() << " markers from file '" << is.m_path << "' and did not reach end of file.");
    }
    if (ret.data.size() != ret.n_mark) {
        throw input_exception(is, "Wrong number of markers.");
    }
    /*MSG_DEBUG(ret);*/
    return ret;
}
}

