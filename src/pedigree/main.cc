/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pedigree.h"
#include "pedigree_settings.h"
#include "output_impl.h"

extern "C"{
    #include<string.h>
    #include<libgen.h>
}

void print_usage_pedigree();


#ifndef SPELL_PEDIGREE_MAIN
#  define SPELL_PEDIGREE_MAIN main
#endif

int SPELL_PEDIGREE_MAIN(int argc, const char** argv)
{
    msg_handler_t::debug_enabled() = false;
    ped_settings_t* settings = ped_settings_t::from_args(argc, argv);
    if (!settings) {
        char* argv0 = strdup(argv[0]); //// Franck Gauthier 2020-12-15
        prg_name(basename(argv0));
        if(argv0 != NULL) free(argv0); //// Franck Gauthier 2020-12-15
        print_usage_pedigree();
    } else {
        MSG_DEBUG("with_LC? " << std::boolalpha << settings->with_LC);
        pedigree_type ped = read_pedigree(settings->pedigree_filename, settings->with_LC, settings->csv_sep, settings->max_states);
        if (msg_handler_t::check(false)) {
            return -1;
        }
        std::string ped_output;
        if (settings->work_directory == "") {
            settings->work_directory = ".";
        }
//            ped_output = settings->work_directory + "/" + basename(settings->pedigree_filename.c_str()) + ".ped-data";
        std::string dir = settings->work_directory + "/" + settings->pop_name + ".cache";
        if (!check_file(dir, true, true, false)) {
            ensure_directories_exist(dir);
            check_file(dir, true, true, true);
        }
        ped_output = dir + "/" + settings->pop_name + ".spell-pedigree.data";
        ped.save(ped_output);
        delete settings;
    }
    return 0;
}

