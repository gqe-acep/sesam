/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pedigree_settings.h"

extern "C"{
    #include<string.h>
    #include<libgen.h>
}

#define PREDICATE CONSTRAINT_PREDICATE(ped_settings_t)
#define CALLBACK_ARGS ARGUMENT_CALLBACK_ARGS(ped_settings_t)


ped_settings_t* ensure(ped_settings_t*& t)
{
    if (t == NULL) {
        t = new ped_settings_t();
    }
    return t;
}


static
argument_section_list_t<ped_settings_t>
arguments = {
    {"Advanced (NOT INTENDED FOR USERS!)", "", true, {
        {{"--full-help", "--advanced-help"},
            {},
            "Display usage.",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                print_usage_impl(true, arguments);
                exit(0);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-D", "--debug"},
            {},
            "Display debug messages",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                msg_handler_t::debug_enabled() = true;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"--no-LC"},
            {},
            "Don't compute LC for each individual",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                ensure(target)->with_LC = false;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

    }},
    {"Miscellaneous", "", false, {
        {{"-h", "--help"},
            {},
            "Display usage.",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                print_usage_impl(false, arguments);
                exit(0);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

         {{"-n", "--name"},
                 {"population name"},
                 "The name of this population (will also be used to create the output filename)",
                 false,
                 {true},
                 [](CALLBACK_ARGS) {
                     ensure(target)->pop_name = *++ai;
                     SAFE_IGNORE_CALLBACK_ARGS;
                 }},

         {{"-wd", "--work-directory"},
            {"path"},
            "Path to directory for output files",
            false,
            {&ped_settings_t::work_directory},
            [](CALLBACK_ARGS) {
                ensure(target)->work_directory = *++ai;
                if (!check_file(ensure(target)->work_directory, true, true, false)) {
                    ensure_directories_exist(ensure(target)->work_directory);
                    check_file(ensure(target)->work_directory, true, true, true);
                }
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

//        {{"-ms", "--max-states"},
//            {"path"},
//            "Maximum number of states allowed in a generation",
//            false,
//            {"unlimited"},
//            [](CALLBACK_ARGS) {
//                ensure(target)->max_states = to<size_t>(*++ai);
//                SAFE_IGNORE_CALLBACK_ARGS;
//            }},

    }},
    {"Input", "The expected pedigree file must be a CSV file with each row in the following format:\nGENERATION_NAME;Individual number;Parent1 number;Parent2 number\nIndividual numbers are expected to increase and all GREATER than zero, and parent numbers for a given individual are expected to be LESSER than the individual number.\nBreeding lines are encoded with Parent1 = Parent2 = 0.\nSelfings are encoded with Parent1 = Parent2.\nDoubled haploids are encoded with Parent2 = 0.\nThe generation names will be used when specifying genotype and phenotype observations in the later steps.\nThe first line is expected to be a header line and will be ignored.", false, {
        {{"-p", "--pedigree-file"},
            {"path"},
            "Path to the pedigree file",
            false,
            {true},
            [](CALLBACK_ARGS)
            {
                ensure(target)->pedigree_filename = *++ai;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-s", "--separator"},
            {"separator character"},
            "Column delimiter character used in the pedigree file",
            false,
            {&ped_settings_t::csv_sep},
            [](CALLBACK_ARGS)
            {
                ensure(target)->csv_sep = (*++ai)[0];
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
    }},
};


argument_parser<ped_settings_t> arg_map_ped(arguments);


ped_settings_t* ped_settings_t::from_args(int argc, const char** argv)
{
    std::vector<const char*> args(argv + 1, argv + argc);
    std::vector<const char*>::iterator ai = args.begin();
    std::vector<const char*>::iterator aj = args.end();

    ped_settings_t* ret = NULL;
    char* argv0 = strdup(argv[0]); //// Franck Gauthier 2020-12-15

    ::prg_name(basename(argv0));

    while (ai != aj && arg_map_ped(ret, ai, aj)) {
        ++ai;
    }

    if (ret) {
        ret->prg_name = basename(argv0);
        ret->command_line.assign(argv, argv + argc);
    }
    if(argv0 != NULL) free(argv0); //// Franck Gauthier 2020-12-15
    return ret;
}

void print_usage_pedigree() { print_usage_impl(false, arguments); }

#include "output_impl.h"
