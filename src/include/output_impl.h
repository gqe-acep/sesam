#ifndef SPELL_QTL_OUTPUT_IMPL_H
#define SPELL_QTL_OUTPUT_IMPL_H

#ifdef SPELL_QTL_STL_OUTPUT_H
namespace output {
    template <typename _Stream, typename _Container>
    _Stream& output_value_container(_Stream& os, _Container&& container) {
        typedef traits<_Container> _Traits;
        auto beg = container.begin(), end = container.end();
        os << _Traits::prefix;
        if (beg != end) {
            os << (*beg);
            for (++beg; beg != end; ++beg) {
                os << _Traits::separator << (*beg);
            }
        }
        return os << _Traits::suffix;
    };
}
#endif //SPEL_QTL_STL_OUTPUT_H

#ifdef _SPEL_ERROR_H_
    template <typename S> builder<S>& builder<S>::operator << (const char* x) { std::move(_s) << x; return *this; }
    template <typename S> template <typename X> builder<S>& builder<S>::operator << (X&& x) { _s << x; return *this; }
    template <typename S> builder<S>& builder<S>::operator << (_Stream&(*x)(_Stream&)) { _s << x; return *this; }
    template <typename S> builder<S>& builder<S>::operator << (std::ios_base& (&x)(std::ios_base&)) { _s << x; return *this; }


    template <typename S> builder<S> endpoint<S>::operator << (const char* x) { builder<S> b(out); b._s << x; return b; }
    template <typename S> template <typename X> builder<S> endpoint<S>::operator << (X&& x) { builder<S> b(out); b._s << x; return b; }
    template <typename S> builder<S> endpoint<S>::operator << (_Stream&(*x)(_Stream&)) { builder<S> b(out); b._s << x; return b; }
    template <typename S> builder<S> endpoint<S>::operator << (std::ios_base& (&x)(std::ios_base&)) { builder<S> b(out); b._s << x; return b; }
#endif

#ifdef _SPELL_FILE_H_
    template <typename ARG>
        file& operator << (file& f, ARG&& arg)
        {
            if (f.m_impl) {
                f.m_impl << arg;
                f.check_state();
            }
            return f;
        }

    template <typename ARG>
        file& operator >> (file& f, ARG&& arg)
        {
            if (f.m_impl) {
                f.check_state();
                f.m_impl >> arg;
            }
            return f;
        }

        inline
        file& operator << (file& f, std::ostream& (&func) (std::ostream&))
        {
            if (f.m_impl) {
                f.m_impl << func;
                f.check_state();
            }
            return f;
        }
#endif

#endif
