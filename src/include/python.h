/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _SPELL_PYTHON_H_

#ifndef _SPELL_PYTHON_H_
#define _SPELL_PYTHON_H_

#include <boost/python.hpp>
extern "C" {
#include <node.h>  // Python
#include <compile.h>  // Python
}
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
// #include <iostream>
// #include "error.h"
#include "computations/frontends4.h"

namespace py = boost::python;


// void SetupEigenConverters();

typedef std::vector<model_block_key> model_block_key_list;

struct python_environment {

    struct py_release_scope {
        py_release_scope() { PyEval_ReleaseLock(); }
        ~py_release_scope() { PyEval_AcquireLock(); }
    };

    struct helpers {
        template <typename M>
        static void mset(M* self, int r, int c, typename M::Scalar v) { (*self)(r, c) = v; }

        template <typename M>
        static void vset(M* self, int c, typename M::Scalar v) { (*self)(c) = v; }

        template <typename M>
        static std::string str(M* self) { std::stringstream s; s << *self; return s.str(); }
        
        static std::string test_result_get_chrom_name(test_result* self) { return self->chrom->name; }

        static std::string tr_str(test_result* self) { return SPELL_STRING("<test best @" << self->chrom->name << ':' << self->locus << ' ' << (self->over_threshold ? "GOOD" : "BAD") << '>'); }
        
        static void algo_phase(std::string str) { report_algo_phase(str); }
        
        template <typename M, typename N=M, typename R=N>
        static R matrix_mul(const M& a, const N& b) { return a * b; }

        template <typename M, typename N=M, typename R=N>
        static R matrix_add(const M& a, const N& b) { return a + b; }

        template <typename M, typename N=M, typename R=N>
        static R matrix_sub(const M& a, const N& b) { return a - b; }
        
        static MatrixXd model_get_X(value<model> m) { return m->X(); }
        static MatrixXd model_get_Y(value<model> m) { return m->Y(); }
        static VectorXd model_get_coefficients(value<model> m) { return m->coefficients(); }
        static VectorXd model_get_rss(value<model> m) { return m->rss(); }
        static MatrixXd model_get_xtx(value<model> m) { return m->XtX(); }
        static MatrixXd model_get_xtx_inv(value<model> m) { return m->XtX_pseudo_inverse(); }
        static model_block_key_list model_get_keys(value<model> m)
        {
            model_block_key_list ret;
            ret.reserve(m->m_blocks.size());
            for (const auto& kv: m->m_blocks) {
                ret.emplace_back(kv.first);
            }
            return ret;
        }
        
        static std::string block_key_to_str(const model_block_key& mbk) { return SPELL_STRING((*mbk)); }
        
        static py::list mm_get_selection(const model_manager& mm)
        {
            py::list ret;
            MSG_DEBUG("Selection: " << mm.get_selection());
            for (const auto& kv: mm.get_selection()) {
                MSG_DEBUG("[selection] k=" << kv.first << " v=" << kv.second);
                py::list loci;
                auto v = kv.second->to_vec();
                MSG_DEBUG("[selection] vec: " << v);
                bool empty = !v.size();
                for (double d: v) {
                    loci.append(d);
                }
                if (!empty) {
                    ret.append(py::make_tuple(kv.first->name, py::tuple(loci)));
                }
            }
            return ret;
        }
        
        static void algo_log(std::string msg) { report_algo_phase(msg); }
        static void msg_debug(std::string msg) { MSG_DEBUG(msg); }
        static void msg_info(std::string msg) { MSG_INFO(msg); }
        static void msg_warn(std::string msg) { MSG_WARNING(msg); }
        static void msg_error(std::string msg, std::string suggest) { MSG_ERROR(msg, suggest); }

        static std::map<std::string, test_result>
        mm_snbpc(model_manager& mm, bool clear_chromosome=false, std::string chr_name="")
        {
            py_release_scope pyre;
            std::map<std::string, test_result> ret;
            chromosome_value chr = NULL;
            if (chr_name != "") {
                chr = active_settings->find_chromosome(chr_name);
                if (!chr) {
                    throw std::runtime_error(SPELL_STRING("Invalid chromosome name '" << chr_name << '\''));
                }
            }
            for (const auto& kv: mm.search_new_best_per_chromosome(clear_chromosome, chr)) {
                ret[kv.first->name] = kv.second;
            }
            return ret;
        }
        
        static std::pair<bool, test_result>
        mm_challenge_qtl(model_manager& mm, std::string chrom_name, double locus)
        {
            py_release_scope pyre;
            auto chr = active_settings->find_chromosome(chrom_name);
            if (!chr) {
                throw std::runtime_error(SPELL_STRING("Invalid chromosome name '" << chrom_name << '\''));
            }
            return mm.challenge_qtl(chr, locus);
        }
    };

    template <typename Scalar>
    void
    def_matrix_type(const char* py_name)
    {
        typedef Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> M;
        typedef Eigen::Matrix<Scalar, Eigen::Dynamic, 1> CV;
        typedef Eigen::Matrix<Scalar, 1, Eigen::Dynamic> RV;
        main_namespace[py_name]
            = py::class_<M>(py_name, py::init<int, int>())
                .def("rows", static_cast<Eigen::Index (M::*) () const>(&M::rows))
                .def("cols", static_cast<Eigen::Index (M::*) () const>(&M::cols))
                .def("__call__", static_cast<typename M::CoeffReturnType (M::*) (Eigen::Index, Eigen::Index) const>(&M::operator ()),
                     py::return_value_policy<py::copy_const_reference>()
                )
                .def("set", &helpers::mset<M>)
                .def("__str__", &helpers::str<M>)
                .def("__mul__", &helpers::matrix_mul<M>)
                .def("__add__", &helpers::matrix_add<M>)
                .def("__sub__", &helpers::matrix_sub<M>)
                ;
    }
    
    template <typename Scalar>
    void
    def_vector_type(const char* py_name)
    {
        typedef Eigen::Matrix<Scalar, Eigen::Dynamic, 1> V;
        main_namespace[py_name]
            = py::class_<V>(py_name, py::init<int>())
                .def("rows", static_cast<Eigen::Index (V::*) () const>(&V::rows))
                .def("cols", static_cast<Eigen::Index (V::*) () const>(&V::cols))
                .def("__call__", static_cast<typename V::CoeffReturnType (V::*) (Eigen::Index) const>(&V::operator ()),
                     py::return_value_policy<py::copy_const_reference>()
                )
                .def("set", &helpers::mset<V>)
                .def("__str__", &helpers::str<V>)
                .def("__mul__", &helpers::matrix_mul<V>)
                .def("__add__", &helpers::matrix_add<V>)
                .def("__sub__", &helpers::matrix_sub<V>)
                ;
    }
    
    void
    init()
    {
        try {
            Py_Initialize();
            PyEval_InitThreads();
//             SetupEigenConverters();

            MSG_DEBUG(__FILE__ << ":" << __LINE__);
            main_module = py::object(py::handle<>(py::borrowed(PyImport_AddModule("__main__"))));
            MSG_DEBUG(__FILE__ << ":" << __LINE__);
            main_namespace = main_module.attr("__dict__");
           
            MSG_DEBUG(__FILE__ << ":" << __LINE__);
            def_matrix_type<double>("MatrixDouble");
            MSG_DEBUG(__FILE__ << ":" << __LINE__);
            def_vector_type<double>("VectorDouble");
            MSG_DEBUG(__FILE__ << ":" << __LINE__);
            def_matrix_type<int>("MatrixInt");
            MSG_DEBUG(__FILE__ << ":" << __LINE__);
            def_vector_type<int>("VectorInt");

            MSG_DEBUG(__FILE__ << ":" << __LINE__);
            main_namespace["TestResult"]
                = py::class_<test_result>("TestResult")
                    .add_property("chromosome", &helpers::test_result_get_chrom_name)
                    .def_readonly("locus", &test_result::locus)
                    .def_readonly("test_value", &test_result::test_value)
                    .def_readonly("index", &test_result::index)
                    .def_readonly("over_threshold", &test_result::over_threshold)
                    .def("select", &test_result::select)
                    /* TODO add model blocks and block keys */
                    .def_readonly("pop_key", &test_result::pop_block_key)
                    .def_readonly("dom_key", &test_result::dom_block_key)
                    .def("__str__", &helpers::tr_str)
                    ;

            main_namespace["ChallengeResult"]
                = py::class_<std::pair<bool, test_result>>("ChallengeResult")
                    .def_readonly("passed", &std::pair<bool, test_result>::first)
                    .def_readonly("test_result", &std::pair<bool, test_result>::second)
                    ;
                    
//             main_module.def("algo_log", &helpers::algo_log, py::args("text"));
//             MSG_DEBUG(__FILE__ << ":" << __LINE__);
            py::scope pouet = main_module;
            py::def("algo_log", &helpers::algo_log, py::args("text"));
//             MSG_DEBUG(__FILE__ << ":" << __LINE__);
            py::def("msg_debug", &helpers::msg_debug, py::args("message"));
//             MSG_DEBUG(__FILE__ << ":" << __LINE__);
            py::def("msg_warn", &helpers::msg_warn, py::args("message"));
//             MSG_DEBUG(__FILE__ << ":" << __LINE__);
            py::def("msg_info", &helpers::msg_info, py::args("message"));
//             MSG_DEBUG(__FILE__ << ":" << __LINE__);
            py::def("msg_error", &helpers::msg_error, py::args("message", "suggestion"));
//             MSG_DEBUG(__FILE__ << ":" << __LINE__);

            MSG_DEBUG(__FILE__ << ":" << __LINE__);
            main_namespace["BlockKey"]
                = py::class_<model_block_key>("BlockKey")
                    .def("__str__", &helpers::block_key_to_str)
                    ;

            MSG_DEBUG(__FILE__ << ":" << __LINE__);
            main_namespace["TestResultsPerChromosome"]
                = py::class_<std::map<std::string, test_result>>("TestResultsPerChromosome", py::no_init)
                    .def(py::map_indexing_suite<std::map<std::string, test_result>, true>())
                    ;
                    
            MSG_DEBUG(__FILE__ << ":" << __LINE__);
            main_namespace["BlockKeyList"]
                = py::class_<model_block_key_list>("BlockKeyList", py::no_init)
                    .def(py::vector_indexing_suite<model_block_key_list, true>())
                    ;
                    
//             MSG_DEBUG(__FILE__ << ":" << __LINE__);
//             main_namespace["VectorOfDouble"]
//                 = py::class_<std::vector<double>>("VectorOfDouble", py::no_init)
//                     .def(py::vector_indexing_suite<std::vector<double>, true>())
//                     ;
                    
            MSG_DEBUG(__FILE__ << ":" << __LINE__);
            main_namespace["Model"]
                = py::class_<value<model>>("Model", py::no_init)
                    .add_property("X", &helpers::model_get_X)
                    .add_property("Y", &helpers::model_get_Y)
                    .add_property("coefficients", &helpers::model_get_coefficients)
                    .add_property("RSS", &helpers::model_get_rss)
                    .add_property("XtX", &helpers::model_get_xtx)
                    .add_property("XtX_inv", &helpers::model_get_xtx_inv)
                    .add_property("keys", &helpers::model_get_keys)
                    ;

            MSG_DEBUG(__FILE__ << ":" << __LINE__);
            main_namespace["ModelManager"]
                = py::class_<model_manager>("ModelManager", py::no_init)
                    .def_readonly("trait_name", &model_manager::trait_name)
                    .add_property("threshold", &model_manager::threshold)
                    .def_readonly("model", &model_manager::vMcurrent)
                    .def_readonly("y_block_cols", &model_manager::y_block_cols)
                    .add_property("max_order", &model_manager::max_order)
                    .add_property("selection", &helpers::mm_get_selection)
                    
                    .def("select_all_loci", &model_manager::select_all_loci)
                    .def("deselect", &model_manager::deselect)
                    .def("weakest_link", &model_manager::weakest_link)
                    .def("search_new_best_per_chromosome", &helpers::mm_snbpc)
                    .def("challenge_qtl", &helpers::mm_challenge_qtl)
                    .def("clear_selection", &model_manager::clear_selection)
                    
                    ;

        } catch(py::error_already_set) {
            PyErr_Print();
        }
    }

    std::string algo_script_directory()
    {
        return ".";
    }

    std::string algo_name_to_file_name(std::string algo_name)
    {
        return SPELL_STRING(algo_script_directory() << '/' << algo_name << ".py");
    }

//     #include <node.h> // from python

// Copied from pythonrun.c
    PyObject*
    run_node(struct _node *n, const char *filename, PyObject *globals, PyObject *locals) {
//         PyCodeObject *co;
        PyObject *co;
        PyObject *v;
        co = (PyObject*) PyNode_Compile(n, filename);
        PyNode_Free(n);
        if (co == NULL)
        return NULL;
        v = PyEval_EvalCode(co, globals, locals);
        Py_DECREF(co);
        return v;
    }

    // This is missing from python: a PyRun_String that also takes a filename, to show in backtraces
    PyObject*
    MyPyRun_StringFileName(const char *str, const char* filename, int start, PyObject *globals, PyObject *locals) {
        struct _node* n = PyParser_SimpleParseString(str, start);
        if (!n) return 0;
        return run_node(n, filename, globals, locals);
    }
    
    void
    run_script(std::string algo_name, model_manager& context)
    {
        MSG_DEBUG(__FILE__ << ":" << __LINE__);
        MSG_DEBUG("py_th_init? " << PyEval_ThreadsInitialized());
        std::string file_name = algo_name_to_file_name(algo_name);
        std::ifstream is(file_name);
//         FILE* f = fopen(file_name.c_str(), "rb");
        if (is.is_open()) {
           MSG_DEBUG(__FILE__ << ":" << __LINE__);
 //         if (f) {
            std::string algo_source((std::istreambuf_iterator<char>(is)),
                                     std::istreambuf_iterator<char>());

            try {
                PyObject *pName, *pModule, *pArgs, *pValue, *pFunc;
                char* code = strdup(algo_source.c_str());
                char* name = strdup(algo_name.c_str());

                PyObject *pGlobal = PyDict_New();
                PyObject *pLocal;

                //Create a new module object
                PyObject *pNewMod = PyModule_New(name);

                PyModule_AddStringConstant(pNewMod, "__file__", file_name.c_str());

                //Get the dictionary object from my module so I can pass this to PyRun_String
                pLocal = PyModule_GetDict(pNewMod);

                //Define my function in the newly created module
                /*pValue =*/ MyPyRun_StringFileName(code, file_name.c_str(), Py_file_input, main_namespace.ptr(), pLocal);
//                 Py_DECREF(pValue);

                py::object m = py::object(py::handle<>(py::borrowed(pNewMod)));
                
                m.attr("spell")(context);
                //Get a pointer to the function I just defined
//                 pFunc = PyObject_GetAttrString(pNewMod, "blah");

                //Build a tuple to hold my arguments (just the number 4 in this case)
//                 pArgs = PyTuple_New(1);
//                 pValue = PyInt_FromLong(4);
//                 PyTuple_SetItem(pArgs, 0, pValue);

                //Call my function, passing it the number four
//                 pValue = PyObject_CallObject(pFunc, pArgs);
//                 Py_DECREF(pArgs);
//                 printf("Returned val: %ld\n", PyInt_AsLong(pValue));
//     Py_DECREF(pValue);

//     Py_XDECREF(pFunc);
    Py_DECREF(pNewMod);

#if 0
                MSG_DEBUG(__FILE__ << ":" << __LINE__);
                auto module = py::object(py::handle<>(py::borrowed(PyImport_AddModule(name))));
                MSG_DEBUG(__FILE__ << ":" << __LINE__);
                auto namespace = main_module.attr("__dict__");
                MSG_DEBUG(__FILE__ << ":" << __LINE__);

                MSG_DEBUG("<EXEC>" << std::endl << algo_source << std::endl << "</EXEC>");
                PyRun_String(algo_source.c_str(), Py_file_input, main_namespace.ptr(), main_namespace.ptr());
                MSG_DEBUG(__FILE__ << ":" << __LINE__);
//                 PyRun_AnyFile(f, file_name.c_str());
//                 main_module.attr(algo_name.c_str())(context);

                auto script_code = py::object(py::handle<>(py::borrowed(
                                    Py_CompileString(code, name, 0)
                                )));
                auto algo_module = py::object(py::handle<>(py::borrowed(
                                    PyImport_ExecCodeModule(name, script_code.ptr())
                                )));
                algo_module.attr(algo_name.c_str())(context);
#endif
                free(code);
                free(name);
                report_algo_phase("END QTL DETECTION");
            } catch(py::error_already_set) {
                PyErr_Print();
                MSG_ERROR("Something went wrong trying to execute Python code", "Contact your local voodoo priest");
            }
        } else {
            MSG_ERROR("Couldn't open file " << file_name, SPELL_STRING("Check the file " << file_name << " exists and is readable."));
        }
    }
    
#if 0
    std::string
    read()
    {
        std::string line;
        std::cout << "mypy» ";
        std::getline(std::cin, line);
        while (line.back() == ' ' || line.back() == '\t') { line.pop_back(); }
        if (line.back() == ':') {
            std::stringstream buffer(line);
            while (true) {
            std::cout << "mypy… ";
                std::getline(std::cin, line);
                if (line == "") {
                    return buffer.str();
                }
                buffer << std::endl << line;
            }
        }
        return line;
    }
    
    py::object
    eval(std::string cmd)
    {
        try {
            return py::object(py::handle<>((PyRun_String(cmd.c_str(),
                                                        Py_file_input,
                                                        main_namespace.ptr(),
                                                        main_namespace.ptr()))));
        } catch(py::error_already_set) {
            PyErr_Print();
            return py::object(py::handle<>(Py_None));
        }
    }

    void
    print(py::object result)
    {
//         PySys_WriteStdout(result);
//         py::object s = py::str(result);
        try {
//             std::cout << py::extract<std::string>(result)() << std::endl;
//             PySys_WriteStdout(result.get());
        } catch(py::error_already_set) {
            PyErr_Print();
        }
    }
    
    void
    REPL()
    {
        while (!std::cin.eof()) {
            std::string input = read();
            if (input == "exit") {
                return;
            }
            print(eval(input));
        };
    }
#else
    void
    REPL()
    {
        PyRun_InteractiveLoop(stdin, "User input");
    }
#endif

    python_environment() : main_module(), main_namespace() { init(); }

private:
    py::object main_module, main_namespace;
};


#endif
