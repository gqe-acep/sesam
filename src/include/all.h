/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*#include <x2c/x2c.h>*/

#error "DO NOT USE THIS HEADER!!!"

#define EIGEN_NO_DEPRECATED_WARNING
#include <cctype>
#include <locale>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <iostream>
#include <iomanip>

#include "outbred.h"
#include "chrono.h"
#include "input.h"
#include "settings.h"

#include "banner.h"
#include "cache2.h"
#include "model.h"


template <typename T1, typename T2>
std::ostream& operator << (std::ostream& os, const std::pair<T1, T2>& p) { return os << p.first << ':' << p.second; }

template <typename T1, typename T2>
std::ostream& operator << (std::ostream& os, const std::map<T1, T2>& m)
{
    auto i = m.begin(), j = m.end();
    os << '{';
    if (i != j) {
        os << (*i);
        for (++i; i != j; ++i) {
            os << ' ' << (*i);
        }
    }
    return os << '}';
}

/*#include "computations.h"*/

