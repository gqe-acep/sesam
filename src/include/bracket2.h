/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_BRACKET_H_
#define _SPEL_BRACKET_H_

#include "eigen.h"
#include "error.h"
#include <vector>
#include <map>
#include <algorithm>

namespace impl {
    struct generation_rs;
};

typedef MatrixXb BracketMatrix;

namespace BK {

typedef std::vector<BracketMatrix> state_list_type;

struct bracket_type {
    state_list_type bra;
    state_list_type ket;
    friend std::ostream& operator << (std::ostream& os, const bracket_type& b)
    {
        return os << "<|:" << b.bra.size() << " |>:" << b.ket.size();
    }
};

typedef std::map<size_t, bracket_type> ancestor_list_type;

inline
std::ostream& operator << (std::ostream& os, const ancestor_list_type& al)
{
    for (const auto& kv: al) {
        os << " <" << kv.first << "|:" << kv.second.bra.size();
        os << " |" << kv.first << ">:" << kv.second.ket.size();
    }
    return os;
}

struct individual_type {
    size_t index;
    bracket_type bracket;
    ancestor_list_type mother;
    ancestor_list_type father;
    friend std::ostream& operator << (std::ostream& os, const individual_type& i)
    {
        os << "[Ind. #" << i.index << " constraint =" << i.bracket << std::endl
           << " inherited constraints" << std::endl
           << " * M#" << i.mother.size() << " " << i.mother << std::endl
           << " * P#" << i.father.size() << " " << i.father << std::endl
           << ']' << std::endl;
        return os;
    }
};


template <typename MATRIX_TYPE>
state_list_type
combine_m(const state_list_type& b1, MATRIX_TYPE&& m)
{
    state_list_type ret;
    ret.reserve(b1.size());
    for (size_t i = 0; i < b1.size(); ++i) {
        ret.emplace_back(kroneckerProduct(b1[i], m));
    }
    return ret;
}


template <typename MATRIX_TYPE>
state_list_type
combine_m(MATRIX_TYPE&& m, const state_list_type& b1)
{
    state_list_type ret;
    ret.reserve(b1.size());
    for (size_t i = 0; i < b1.size(); ++i) {
        ret.emplace_back(kroneckerProduct(m, b1[i]));
    }
    return ret;
}


inline
state_list_type
combine_bk(const state_list_type& b1, const state_list_type& b2)
{
    state_list_type ret;
    if (b1.size() != b2.size()) {
        throw std::runtime_error("Incompatible bra|ket for product");
    }
    ret.reserve(b1.size());
    for (size_t i = 0; i < b1.size(); ++i) {
        ret.emplace_back(kroneckerProduct(b1[i], b2[i]));
    }
    return ret;
}


inline
void
apply_redux(bracket_type& b1, const BracketMatrix& m)
{
    for (size_t i = 0; i < b1.bra.size(); ++i) {
        b1.bra[i] = m * b1.bra[i] * m.transpose();
        b1.ket[i] = m * b1.ket[i] * m.transpose();
    }
}


bracket_type b_init(size_t n_states)
{
    BracketMatrix I = BracketMatrix::Identity(n_states, n_states);
    auto ones = MatrixXb::Ones(1, n_states);

    bracket_type ret;
    ret.bra.resize(n_states);
    ret.ket.resize(n_states);

    for (size_t i = 0; i < n_states; ++i) {
        ret.bra[i] = I.col(i) * I.row(i);
        ret.ket[i] = I.col(i) * ones;
    }

    return ret;
}


bracket_type b_gamete(const bracket_type& b)
{
    static auto I = MatrixXb::Identity(2, 2);
    return {combine_m(b.bra, I), combine_m(b.ket, I)};
}


BracketMatrix bracket(const state_list_type& bra, const state_list_type& ket)
{
    BracketMatrix ret(bra.front().rows(), ket.front().cols());
    for (size_t i = 0; i < bra.size(); ++i) {
        ret += bra[i] * ket[i];
    }
    return ret;
}



inline
individual_type
i_line(size_t index)
{
    return {index, b_init(1), {}, {}};
}


inline
individual_type
i_gamete_impl(const individual_type& I, bool is_dh=false)
{
    static auto GI = MatrixXb::Identity(2, 2);
    individual_type ret;
    ret.index = I.index;
    for (const auto& kv: I.mother) {
        /*if (!kv.second.bra.size()) {*/
            /*continue;*/
        /*}*/
        ret.mother.insert({kv.first,
                           {combine_m(kv.second.bra, GI),
                            combine_m(kv.second.ket, GI)}
                           });
    }
    for (const auto& kv: I.father) {
        /*if (!kv.second.bra.size()) {*/
            /*continue;*/
        /*}*/
        ret.mother.insert({kv.first,
                           {combine_m(kv.second.bra, GI),
                            combine_m(kv.second.ket, GI)}
                           });
    }
    if (is_dh) {
        ret.bracket.bra = combine_m(I.bracket.bra, GI);
        ret.bracket.ket = combine_m(I.bracket.ket, GI);
    } else {
        ret.mother.insert({I.index,
                           {combine_m(I.bracket.bra, GI),
                            combine_m(I.bracket.ket, GI)}
                           });
    }
    MSG_DEBUG("gamete contains " << ret.mother.size() << " projectors");
    return ret;
}


inline
individual_type
i_gamete(const individual_type& I) { return i_gamete_impl(I, false); }

inline
individual_type
i_dh(const individual_type& I) { return i_gamete_impl(I, true); }


/* algo gamete
 *
 * une seule liste de bra|ket
 * - combiner les listes mother/father et kroneckerProduct(bra|ket, Id(2))   \  la liste mother est utilisée.
 * - ajouter le bra|ket de l'individu, kroneckerProduct(~, Id(2)) aussi      /
 *
 * algo crossing
 *
 * - identifier parents communs
 * - combiner bra entre gamètes
 *      - pour tout parent commun X : bra(X, mother) (x) bra(X, father) (pareil pour ket)
 *      - sinon pour tout autre X : bra(X, mother) (x) Id(taille gamète father), Id(taille gamète mother) (x) bra(X, father)
 * - associer bra(X, mother) et ket(X, father), et bra(X, father) et ket(X, mother) pour tout X commun
 * - calculer bra(X).ket(X) => proj(X).
 * - calculer le produit de tous les proj(X) et supprimer les colonnes vides => PROJ.
 * - TEST : NORMALEMENT LES PROJECTEURS DEVRAIENT COMMUTER.
 * - passer tous les bra|ket ainsi que generation_rs à la moulinette (t(PROJ) . x . PROJ).
 *
 * haploïde doublé
 * - je prends une gamète
 *   mais on ajoute pas individu::bracket à la liste, on le laisse en bracket ((x) Id(2))
 * - ...
 * - PROFIT!!!
 * 
 */

inline
individual_type
i_cross(size_t idx, const individual_type& M, const individual_type& P)
{
    individual_type ret;
    ret.index = idx;

    MSG_DEBUG("CROSSING");
    MSG_DEBUG(M);
    MSG_DEBUG(P);
    /* GAMETES */
    individual_type GM = i_gamete(M);
    individual_type GP = i_gamete(P);
    size_t sgm = GM.mother.size();
    size_t sgp = GP.mother.size();
    MSG_DEBUG("sgm=" << sgm);
    MSG_DEBUG("sgp=" << sgp);
    /*ret.mother = GM.mother;*/
    /*ret.father = GP.mother;*/

    /* COMMON ANCESTORS */
    std::vector<size_t> gmk, gpk;
    gmk.reserve(sgm);
    gpk.reserve(sgp);
    for (const auto& kv: GM.mother) { gmk.push_back(kv.first); }
    for (const auto& kv: GP.mother) { gpk.push_back(kv.first); }
    std::vector<size_t> common(sgm > sgp ? sgm : sgp);
    auto it = std::set_intersection(gmk.begin(), gmk.end(),
                                    gpk.begin(), gpk.end(),
                                    common.begin());
    common.resize(it - common.begin());

    /* COMBINE BRAs and KETs */
    ancestor_list_type al;
    auto common_i = common.begin(), common_j = common.end();
    auto mi = GM.mother.begin(), mj = GM.mother.end();
    auto pi = GP.mother.begin(), pj = GP.mother.end();
    size_t msz = GM.mother.begin()->second.bra.front().cols();
    size_t psz = GP.mother.begin()->second.bra.front().cols();
    auto IP = MatrixXb::Identity(psz, psz);
    auto IM = MatrixXb::Identity(msz, msz);

    while (mi != mj && pi != pj) {
        if (mi->second.bra.size() == 1) {
            ++mi;
            continue;
        }
        if (pi->second.bra.size() == 1) {
            ++pi;
            continue;
        }
        if (common_i != common_j && mi->first == pi->first && mi->first == *common_i) {
            al.insert({*common_i,
                       {combine_bk(pi->second.bra, mi->second.bra),
                        combine_bk(pi->second.ket, mi->second.ket)}
                       });
            ++common_i;
            ++pi;
            ++mi;
        } else if (mi->first < pi->first) {
            al.insert({mi->first,
                       {combine_m(IP, mi->second.bra),
                        combine_m(IP, mi->second.ket)}
                       });
            ++mi;
        } else if (pi->first < mi->first) {
            al.insert({pi->first,
                       {combine_m(pi->second.bra, IM),
                        combine_m(pi->second.ket, IM)}
                       });
            ++pi;
        } else {
            throw std::runtime_error("Hein??");
        }
    }
    while (mi != mj) {
        if (mi->second.bra.size() == 1) {
            ++mi;
            continue;
        }
        al.insert({mi->first,
                   {combine_m(IP, mi->second.bra),
                    combine_m(IP, mi->second.ket)}
                   });
        ++mi;
    }
    while (pi != pj) {
        if (pi->second.bra.size() == 1) {
            ++pi;
            continue;
        }
        al.insert({pi->first,
                   {combine_m(pi->second.bra, IM),
                    combine_m(pi->second.ket, IM)}
                   });
        ++pi;
    }

    ret.mother = al;

    if (common.size() != 0) {
        /* COMPUTE PROJECTOR */
        /* for debug only. should accumulate in P from the start. */
        std::vector<BracketMatrix> proj;
        /*BracketMatrix PROJ = BracketMatrix::Identity(msz * psz);*/
        BracketMatrix PROJ;
        proj.reserve(al.size());
        for (size_t i: common) {
            MSG_DEBUG("common parent #" << i << "  " << al[i]);
            proj.emplace_back(bracket(al[i].bra, al[i].ket));
        }
        /*for (const auto& m: proj) { PROJ *= m; }*/
        for (const auto& m: proj) {
            /*MSG_DEBUG("projector");*/
            /*MSG_DEBUG(m);*/
            MatrixXb tmp = m;
            /*MSG_DEBUG("TEST: " << (tmp * tmp == tmp));*/
        }
        PROJ = proj.front();
        for (size_t i = 1; i < proj.size(); ++i) { PROJ = PROJ * proj[i]; }
        /*MSG_DEBUG("raw PROJ");*/
        /*MSG_DEBUG(PROJ);*/
        /*{*/
            /*MatrixXb tmp = PROJ;*/
            /*MSG_DEBUG("TEST: " << (tmp * tmp == tmp));*/
        /*}*/

        std::vector<int> keep;

        {
            /*MatrixXb tmp = PROJ;*/
            /*MatrixXb tmp = PROJ * VectorXb::Ones(PROJ.cols());*/
            for (int i = 0; i < PROJ.rows(); ++i) {
                /*MSG_DEBUG("row(" << i << ").sum() = " << tmp.row(i).sum());*/
                if (PROJ.row(i).sum() != 0) {
                    keep.push_back(i);
                }
            }
        }

        BracketMatrix redux = BracketMatrix::Zero(PROJ.rows(), keep.size());
        for (size_t i = 0; i < keep.size(); ++i) {
            redux(keep[i], i) = true;
        }

        PROJ = redux.transpose() * PROJ;

        /*MSG_DEBUG("Have PROJ=");*/
        /*MSG_DEBUG(PROJ);*/
        /*MSG_DEBUG("Index " << idx << " " << MATRIX_SIZE(PROJ) << " " << MATRIX_SIZE(redux));*/

        apply_redux(ret.bracket, PROJ);
        for (auto& kv: ret.mother) {
            apply_redux(kv.second, PROJ);
        }
        for (auto& kv: ret.father) {
            apply_redux(kv.second, PROJ);
        }

        /* INIT CONSTRAINT */
        ret.bracket = b_init(PROJ.rows());
    } else {
        ret.bracket = b_init(msz * psz);
    }

    return ret;
}

} /* namespace BK */

#endif

