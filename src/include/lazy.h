/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_LAZY_FIELD_H_
#define _SPEL_LAZY_FIELD_H_

#include <atomic>

struct spin_lock {
	std::atomic_flag m_lock = ATOMIC_FLAG_INIT;
	void lock()
	{
		while (m_lock.test_and_set(std::memory_order_acquire)); // acquire lock
	}
	void unlock()
	{
		m_lock.clear(std::memory_order_release);                // release lock
	}
};

struct scoped_lock {
	spin_lock& sl;
	scoped_lock(spin_lock& _) : sl(_) { sl.lock(); }
	~scoped_lock() { sl.unlock(); }
};


#endif

