/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _SPEL_X2C_PROXY_H_

#ifndef _SPEL_X2C_PROXY_H_
#define _SPEL_X2C_PROXY_H_

namespace x2c {
static inline void from_string(const std::string& s, bool& x)
{
    std::string word;
    std::stringstream(s) >> word;
    if (word == "true" || word == "yes") {
        x = true;
    } else if (word == "false" || word == "no") {
        x = false;
    } else {
        MSG_ERROR("Expected 'true', 'false', 'yes', or 'no' instead of '" << word << "'", "");
        x = false;
    }
	/*MSG_DEBUG("converting bool... read '" << word << "', made " << x);*/
}

#if 0
static inline void from_string(const std::string& s, double& x)
{
    std::stringstream(s) >> x;
	MSG_DEBUG("converting double... made " << x);
}
#endif
}


#define X2C_OUT(__x__) do { msg_handler_t::cout() << __x__ << std::endl; } while (0)
#define X2C_ERR(__x__) do { msg_handler_t::cerr() << __x__ << std::endl; } while (0)
#define X2C_LOG(__x__) do { msg_handler_t::clog() << __x__ << std::endl; } while (0)
#include <x2c/x2c.h>

using namespace x2c;

DTD_FORWARD_DECL(design_dtd, design_type);
DTD_FORWARD_DECL(analysis_dtd, settings_t);
DTD_FORWARD_DECL(format_dtd, format_specification_t);

static inline
bool check_true(const std::string* s)
{
    return *s == "true" || *s == "yes";
}

static inline
bool check_false(const std::string* s)
{
    return *s == "false" || *s == "no";
}

static inline
bool check_mapmaker(const std::string* s)
{
    return *s == "mapmaker";
}

template <typename DTD>
typename attr_func<typename DTD::type>::func_type
make_loader(DTD& dtd)
{
    return [&](const std::string* filename, typename DTD::type* ret)
    {
        if (check_file(*filename, false, false)) {
            MSG_INFO("Processing " << (*filename));
            try {
                ifile ifs(*filename); // text mode OK ??? anyway this file (x2c.h) does not seem to be used/compiled. 
                auto result = dtd.parse(ifs);
                if (result) {
                    *ret = *result;
                    delete result;
                    ret->filename = *filename;
                    return true;
                }
            } catch (file::error& fuuuuu) {
                MSG_ERROR(fuuuuu.what(), "Make sure the file exists, is readable, and contains valid XML.");
                exit(-1);
            }
        }
        return false;
    };
}

#endif

