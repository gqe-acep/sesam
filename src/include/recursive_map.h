/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_RECURSIVE_MAP_H_
#define _SPEL_RECURSIVE_MAP_H_

template <typename TupleType>
struct last_element_index : std::integral_constant<int, std::tuple_size<TupleType>::value - 1>;

template <typename TupleType>
using last_element_type = typename std::tuple_element<TupleType, last_element_index<TupleType>::value>::type;

template <typename T>
struct recursive_map_impl_traits {
    template <typename V>
    using map_type = std::map<T, V>;
};

template <typename KeyTuple, int I, typename ValueType> struct recursive_map_impl;

template <typename KeyTuple, typename ValueType>
struct recursive_map_impl<KeyTuple, last_element_index<KeyTuple>::value, ValueType>
        : recursive_map_impl_traits<last_element_type<KeyTuple>>::map_type<ValueType> {
};


template <typename KeyTuple, int I, typename ValueType>
struct recursive_map_impl
        : typename recursive_map_impl_traits<std::tuple_element<KeyTuple, I>, recursive_map_impl<KeyTuple, I + 1, ValueType>> {};


template <typename KeyTuple, typename ValueType>
    struct recursive_map : recursive_map_impl<KeyTuple, 0, ValueType> {
        
    };




#endif

