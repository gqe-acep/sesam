/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_SYMMETRY_H_
#define _SPEL_SYMMETRY_H_

#include <vector>
#include <map>
#include <iostream>
#include "eigen.h"
#include "lumping2.h"
#include "permutation.h"
#include "error.h"

/*using namespace Eigen;*/

typedef Eigen::Matrix<char, Eigen::Dynamic, Eigen::Dynamic> MatrixXc;
typedef Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic> MatrixXb;
typedef Eigen::Matrix<bool, Eigen::Dynamic, 1> VectorXb;


/*typedef std::pair<char, char> label_type;*/

#define GAMETE_L '<'
#define GAMETE_R '>'
#define GAMETE_EMPTY 0

#define NOT_GAMETE(_l) (_l != GAMETE_EMPTY)


#if 0
struct label_type {
    typedef int32_t letter_type;
    typedef uint64_t compact_type;
    union {
        struct {
            letter_type first;
            letter_type second;
        } p;
        compact_type compact;
    } data;

    label_type() : data({0, 0}) {}
    label_type(letter_type G) : data({G, 0}) {}
    label_type(letter_type l, letter_type r) : data({l, r}) {}

    label_type& operator = (const label_type& other) { data.compact = other.data.compact; return *this; }

    bool operator == (const label_type& other) const { return data.compact == other.data.compact; }
    bool operator != (const label_type& other) const { return data.compact != other.data.compact; }
    bool operator < (const label_type& other) const { return data.compact < other.data.compact; }

    letter_type& first() const { return data.p.first; }
    letter_type& second() const { return data.p.second; }
    compact_type compact() const { return data.compact; }

    friend
        std::ostream& operator << (std::ostream& os, const label_type& l)
        {
            if (l.data.p.first == 0) {
                os << '*';
                if (l.data.p.second != 0) {
                    os << ((int) l.data.p.second);
                }
            } else {
                os << l.data.p.first;
                if (l.data.p.second != 0) {
                    os << l.data.p.second;
                }
                if (l.data.p.third != 0) {
                    os << l.data.p.third;
                }
            }
            return os;
        }
};



#else
struct label_type {
    typedef char letter_type;
    union {
        struct {
            char first;
            char second;
            char third;
            char count;
        } p;
        uint32_t compact;
    } data;

    label_type() : data({{0, 0, 0, 0}}) {}
    label_type(char G) : data({{G, 0, 0, 1}}) {}
    label_type(char l, char r) : data({{l, r, 0, 2}}) {}
    label_type(char l, char r, char t) : data({{l, r, t, 3}}) {}

    label_type& operator = (const label_type& other) { data.compact = other.data.compact; return *this; }

    bool operator == (const label_type& other) const { return data.compact == other.data.compact; }
    bool operator != (const label_type& other) const { return data.compact != other.data.compact; }
    bool operator < (const label_type& other) const { return data.compact < other.data.compact; }

    const char& first() const { return data.p.first; }
    const char& second() const { return data.p.second; }
    decltype(data.compact) compact() const { return data.compact; }

    label_type
        mult(const label_type& other, const std::vector<label_type>& history) const
        {
            if (other.first() == 0) {
                /* recall ! */
                /*MSG_DEBUG("recall! " << history[other.second() - 1]);*/
                return operator * (history[other.second() - 1]);
            }
            return operator * (other);
        }

    label_type
        operator * (const label_type& other) const
        {
            switch (data.p.count) {
                case 0:
                    /* ø * x = x */
                    return other;
                case 1:
                    /* H * P = HP */
                    /* genotype on the other side */
                    /* haplotype also? */
                    return {data.p.first, other.data.p.first, other.data.p.second, (char) (1 + other.data.p.count)};
                case 2:
                    /* P * G = H */
                    /* gamete on the other side */
                    return {other.data.p.first == GAMETE_L ? data.p.first : data.p.second, 0, 0, 1};
                case 3:
                    /* HP * G = HH = P */
                    /* gamete on the other side */
                    return {data.p.first, other.data.p.first == GAMETE_L ? data.p.second : data.p.third, 0, 2};
            };
            return {};  /* make the compiler happy, but this will never be reached. */
        }

    void
        apply_to(std::vector<char>& stack, const std::vector<label_type>& history) const
        {
            /*MSG_DEBUG("apply_to(" << stack << ", " << (*this) << ", " << history << ')');*/
            /*MSG_QUEUE_FLUSH();*/
            if (first() == 0) {
                history[second() - 1].apply_to(stack, history);
            } else {
                if (second() == 0) {
                    /* pop last two, push one back */
                    if (first() == GAMETE_L) {
                        stack.pop_back();
                    } else {
                        char tmp = stack.back();
                        stack.pop_back();
                        stack.back() = tmp;
                    }
                } else {
                    stack.push_back(first());
                    stack.push_back(second());
                }
            }
            /*MSG_DEBUG(" = " << stack);*/
            /*MSG_QUEUE_FLUSH();*/
        }

    friend
        std::ostream& operator << (std::ostream& os, const label_type& l)
        {
            if (l.data.p.first == 0) {
                os << '*';
                if (l.data.p.second != 0) {
                    os << ((int) l.data.p.second);
                }
            } else {
                os << l.data.p.first;
                if (l.data.p.second != 0) {
                    os << l.data.p.second;
                }
                if (l.data.p.third != 0) {
                    os << l.data.p.third;
                }
            }
            return os;
        }

protected:
    label_type(char f, char s, char t, char c) : data({{f, s, t, c}}) {}
};
#endif

#if 0

struct letter_permutation_type {
    std::map<label_type::letter_type, label_type::letter_type> table;
    std::map<label_type::letter_type, label_type::letter_type> transposed;

    letter_permutation_type() : table() {}

    letter_permutation_type(const std::map<label_type::letter_type, label_type::letter_type>& init)
        : table(init), transposed()
    {
        for (const auto& kv: table) {
            transposed[kv.second] = kv.first;
        }
    }

    letter_permutation_type(std::initializer_list<std::pair<const label_type::letter_type, label_type::letter_type>> init)
        : table(init), transposed()
    {
        for (const auto& kv: table) {
            transposed[kv.second] = kv.first;
        }
    }

    letter_permutation_type(const letter_permutation_type& reference, const MatrixXb& new_permut)
        : table(), transposed()
    {
        int col = 0, row;
        std::vector<label_type::letter_type> letters;
        letters.reserve(reference.table.size());
        for (const auto& kv: reference.table) {
            letters.push_back(kv.first);
        }
        for (const auto& kv: reference.table) {
            new_permut.col(col).maxCoeff(&row);
            table.emplace_hint(table.end(), kv.first, letters[row]);
            ++col;
        }
        for (const auto& kv: table) {
            transposed[kv.second] = kv.first;
        }
    }

    static
        letter_permutation_type identity(const std::vector<label_type>& labels)
        {
            letter_permutation_type ret;
            for (const auto& l: labels) {
                ret.table[l.first()] = l.first();
                ret.table[l.second()] = l.second();
            }
            ret.transposed = ret.table;
            return ret;
        }

    bool empty() const { return table.size() == 0; }

    letter_permutation_type transpose() const { letter_permutation_type ret; ret.table = transposed; ret.transposed = table; return ret; }

    MatrixXb matrix() const
    {
        MatrixXb ret = MatrixXb::Zero(table.size(), table.size());
        std::map<label_type::letter_type, int> indices;
        for (const auto& kv: table) {
            int sz = indices.size();
            indices[kv.first] = sz;
        }
        for (const auto& kv: table) {
            /*MSG_DEBUG(kv.second << '[' << indices[kv.second] << "] " << kv.first << '[' << indices[kv.first] << ']');*/
            /*MSG_QUEUE_FLUSH();*/
            ret(indices[kv.second], indices[kv.first]) = 1;
        }
        return ret;
    }

    bool in_same_space(const letter_permutation_type& other) const
    {
        if (table.size() != other.table.size()) {
            return false;
        }
        auto i = table.begin();
        auto j = table.end();
        auto o = other.table.begin();
        for (; i != j; ++i, ++o) {
            if (i->first != o->first) {
                return false;
            }
        }
        return true;
    }

    MatrixXc pretty_print_matrix() const
    {
        MatrixXc ret = MatrixXc::Constant(table.size() + 3, table.size() + 3, ' ');
        int x = 0;
        for (int c = 0; c < ret.cols(); ++c) {
            ret(0, c) = '-';
            ret(ret.rows() - 1, c) = '-';
            ret(c, 0) = ':';
            ret(c, ret.cols() - 1) = ':';
        }
        ret(0, 0) = ret(0, ret.cols() - 1) = '.';
        ret(ret.rows() - 1) = ret(ret.rows() - 1, ret.cols() - 1) = '\'';
        for (const auto& kv: table) {
            ret(1, 2 + x) = kv.first;
            ret(2 + x, 1) = kv.first;
            ++x;
        }
        ret.block(2, 2, table.size(), table.size()) = (' ' + ('@' - ' ') * matrix().cast<label_type::letter_type>().array()).matrix();
        return ret;
    }

    friend
        std::ostream& operator << (std::ostream& os, const letter_permutation_type& st)
        {
            auto i = st.table.begin();
            auto j = st.table.end();
            os << '{';
            if (i != j) {
                os << i->first << ':' << i->second;
                for (++i; i != j; ++i) {
                    os << ' ' << i->first << ':' << i->second;
                }
            }
            return os << '}';
        }

    bool combine(const letter_permutation_type& other, letter_permutation_type& result) const
    {
        if (other.table.size() == 0) {
            result.table = table;
            return true;
        }
        if (table.size() == 0) {
            result.table = other.table;
            return true;
        }
        letter_permutation_type ret;
        auto ti = table.begin(), tj = table.end();
        auto oi = other.table.begin(), oj = other.table.end();

        for (; ti != tj && oi != oj;) {
            if (ti->first < oi->first) {
                result.table.insert(result.table.end(), *ti);
                ++ti;
            } else if (ti->first > oi->first) {
                result.table.insert(result.table.end(), *oi);
                ++oi;
            } else if (ti->second != oi->second) {
                result.table.clear();
                return false;
            } else {
                result.table.insert(result.table.end(), *ti);
                ++ti;
                ++oi;
            }
        }
        for (; ti != tj; ++ti) {
            result.table.insert(result.table.end(), *ti);
        }
        for (; oi != oj; ++oi) {
            result.table.insert(result.table.end(), *oi);
        }
        return true;
    }

    letter_permutation_type operator * (const letter_permutation_type& other) const
    {
        return compose(other);
    }

    letter_permutation_type compose(const letter_permutation_type& other) const
    {
        if (other.table.size() == 0) {
            return *this;
        }
        if (table.size() == 0) {
            return other;
        }
        MatrixXb m = matrix() * other.matrix();
        if (!(m.transpose() * m - MatrixXb::Identity(m.rows(), m.cols())).isZero()) {
            MSG_DEBUG("FOIRURE MATRICE DE PERMUTATION DE LETTRES COMBINEE");
            MSG_DEBUG(matrix());
            MSG_DEBUG("--");
            MSG_DEBUG(other.matrix());
            MSG_DEBUG("--");
            MSG_DEBUG(m);
            MSG_QUEUE_FLUSH();
            /*abort();*/
            return {};
        }
        letter_permutation_type ret{*this, m};
        MSG_DEBUG("[product A] " << (*this));
        MSG_DEBUG("[product B] " << other);
        MSG_DEBUG("[product R] " << ret);
        return ret;
    }

    bool operator < (const letter_permutation_type& other) const { return table < other.table; }
    bool operator == (const letter_permutation_type& other) const { return table == other.table; }
};

struct symmetry_table_type {
    std::vector<std::pair<label_type::letter_type, label_type::letter_type>> switches;  /* over letters */
    std::map<int, int> table;                     /* over states */
    permutation_type table;
    letter_permutation_type letters;

    symmetry_table_type(const std::map<int, int>& _t, const letter_permutation_type& _l)
        : table(_t), letters(_l)
    {}

    template <typename DERIVED>
        symmetry_table_type(const permutation_base<DERIVED>& state_matrix, const letter_permutation_type& l)
        : table(state_matrix), letters(l)
        {}

    symmetry_table_type() : table() {}

    symmetry_table_type(const symmetry_table_type& other) : table(other.table), letters(other.letters) {}

    symmetry_table_type& operator = (const symmetry_table_type& other) { table = other.table; letters = other.letters; return *this; }

    symmetry_table_type inverse() const { return {table.transpose(), letters.transpose()}; }


    template <typename DERIVED>
        static
        std::pair<bool, symmetry_table_type>
        build(const permutation_base<DERIVED>& permut, const std::vector<label_type>& labels, const MatrixXd& inf_mat, bool latent)
        {
            MSG_DEBUG("#@#@#@ TRYING TO BUILD SYMMETRY FROM");
            MSG_DEBUG("* labels " << labels);
            MSG_DEBUG("* inf_mat");
            MSG_DEBUG(inf_mat);
            MSG_DEBUG("* permutation");
            MSG_DEBUG(permut);
            MSG_QUEUE_FLUSH();

            if (permut.size() != labels.size()) {
                return {false, {}};
            }

            bool ok = true;

            auto new_labels = permut * labels;

            auto li = labels.begin();
            auto lj = labels.end();
            auto ni = new_labels.begin();

            std::map<label_type::letter_type, label_type::letter_type> switches;
            for (; li != lj; ++li, ++ni) {
                label_type::letter_type c1, n1, c2, n2;
                if (latent) {
                    std::tie(c1, n2) = std::tie(li->first(), ni->first());
                    std::tie(c2, n1) = std::tie(li->second(), ni->second());
                } else {
                    std::tie(c1, n1) = std::tie(li->first(), ni->first());
                    std::tie(c2, n2) = std::tie(li->second(), ni->second());
                }
                auto i = switches.find(c1);
                if (i == switches.end()) {
                    switches[c1] = n1;
                } else if (i->second != n1) {
                    ok = false;
                    break;
                }
                i = switches.find(c2);
                if (i == switches.end()) {
                    switches[c2] = n2;
                } else if (i->second != n2) {
                    ok = false;
                    break;
                }
            }

            if (ok) {
                MatrixXd new_mat = permut % inf_mat;
                ok = (new_mat - inf_mat).isZero();
            } else {
                MSG_DEBUG("PERMUTATION CREATES INVALID LETTER MAPPING");
                MSG_DEBUG(permut);
            }

            if (ok) {
                return {true, {permut, switches}};
            }

            return {false, {}};
        }

    friend
        std::ostream& operator << (std::ostream& os, const symmetry_table_type& st)
        {
            os << st.letters << '-';
            auto i = st.table.begin();
            auto j = st.table.end();
            os << '{';
            if (i != j) {
                os << i->first << ':' << i->second;
                for (++i; i != j; ++i) {
                    os << ' ' << i->first << ':' << i->second;
                }
            }
            return os << '}';
        }

    std::vector<std::pair<label_type::letter_type, label_type::letter_type>> switches_from_labels(const std::vector<label_type>& labels, bool latent=false) const
    {
        std::set<std::pair<label_type::letter_type, label_type::letter_type>> tmp;
        /*MSG_DEBUG_INDENT_EXPR("[switches_from_labels#" << latent << "] ");*/
        if (latent && labels.front().second() != GAMETE_EMPTY) {
            for (const auto& kv: table) {
                /*MSG_DEBUG(kv.first << ',' << kv.second << ' ' << labels[kv.first] << ',' << labels[kv.second]);*/
                tmp.emplace(labels[kv.first].first(), labels[kv.second].second());
                tmp.emplace(labels[kv.first].second(), labels[kv.second].first());
            }
        } else {
            for (const auto& kv: table) {
                /*MSG_DEBUG(kv.first << ',' << kv.second << ' ' << labels[kv.first] << ',' << labels[kv.second]);*/
                tmp.emplace(labels[kv.first].first(), labels[kv.second].first());
                if (labels[kv.first].second() != GAMETE_EMPTY) {
                    tmp.emplace(labels[kv.first].second(), labels[kv.second].second());
                }
            }
        }
        /*MSG_DEBUG_DEDENT;*/
        return {tmp.begin(), tmp.end()};
    }

    bool is_consistent(const std::vector<label_type>& labels, bool latent=false) const
    {
        auto switches = switches_from_labels(labels, latent);
        auto smap = switch_map(labels, latent);
        letter_permutation_type tmp(smap);
        MSG_DEBUG_INDENT_EXPR("[is_consistent] ");
        MSG_DEBUG("smap");
        MSG_DEBUG(tmp);
        MSG_DEBUG("letters");
        MSG_DEBUG(letters);
        MSG_DEBUG_DEDENT;
        for (const auto& s: smap) {
            if (s.second == '!') {
                return false;
            }
        }
        return smap == letters.table;
        /*MSG_DEBUG("switches.size() = " << switches.size());*/
        /*MSG_DEBUG("switch_map().size() = " << smap.size());*/
        /*return switches.size() == smap.size();*/
    }

    static
        std::map<label_type::letter_type, int> letter_indices(const std::vector<label_type>& data)
        {
            std::map<label_type::letter_type, int> ret;
            for (const auto& s: data) {
                if (s.first() != GAMETE_EMPTY) {
                    ret[s.first()] = 0;
                }
                if (s.second() != GAMETE_EMPTY) {
                    ret[s.second()] = 0;
                }
            }
            int i = 0;
            for (auto& l: ret) {
                l.second = i++;
                /*MSG_DEBUG("assign " << l.second << " to letter " << l.first);*/
            }
            return ret;
        }

    std::map<label_type::letter_type, label_type::letter_type> switch_map(const std::vector<label_type>& labels, bool latent=false) const
    {
        auto switches = switches_from_labels(labels, latent);
        std::map<label_type::letter_type, label_type::letter_type> ret;
        for (const auto& s: switches) {
            auto it = ret.find(s.first);
            if (it != ret.end() && it->second != s.second) {
                it->second = '!';
            } else {
                ret[s.first] = s.second;
            }
        }
        return ret;
    }

    MatrixXb switch_matrix(const std::vector<label_type>& labels, bool latent) const
    {
        auto l = letter_indices(labels);
        auto switches = switches_from_labels(labels, latent);
        MatrixXb ret = MatrixXb::Zero(l.size(), l.size());
        for (const auto& s: switches) {
            /*MSG_DEBUG("switch " << s.first << " -> " << s.second);*/
            ret(l[s.second], l[s.first]) = 1;
        }
        /* TODO: parano */
        return ret;
    }

    static
        std::vector<std::pair<label_type::letter_type, label_type::letter_type>> switches_from_matrix(const std::vector<label_type>& labels, const MatrixXb& switch_mat)
        {
            auto l = letter_indices(labels);
            std::vector<std::pair<label_type::letter_type, label_type::letter_type>> ret;
            ret.reserve(switch_mat.cols());
            for (const auto& kv1: l) {
                for (const auto& kv2: l) {
                    if (switch_mat(kv2.second, kv1.second)) {
                        ret.emplace_back(kv1.first, kv2.first);
                    }
                }
            }
            return ret;
        }

    MatrixXb matrix() const
    {
        MatrixXb ret = MatrixXb::Zero(table.size(), table.size());
        for (const auto& kv: table) {
            /*MSG_DEBUG("kv={" << kv.first << ',' << kv.second << '}');*/
            ret(kv.second, kv.first) = 1;
        }
        /* TODO: parano */
        /*MSG_DEBUG_INDENT_EXPR("[symmetry matrix] ");*/
        /*MSG_DEBUG(ret);*/
        /*MSG_DEBUG_DEDENT;*/
        return ret;
    }

    bool operator < (const symmetry_table_type& other) const { return letters < other.letters; }
    bool operator == (const symmetry_table_type& other) const { return letters == other.letters; }
    bool operator == (const letter_permutation_type& lp) const { return letters == lp; }

    friend
        symmetry_table_type kronecker(const symmetry_table_type& s1, const symmetry_table_type& s2)
        {
            MatrixXb mat = kroneckerProduct(s1.matrix(), s2.matrix());
            /*return {labels, mat};*/
            letter_permutation_type comb;
            s1.letters.combine(s2.letters, comb);
            return {permutation_type{mat}, comb};
        }

    struct dumpable {
        const symmetry_table_type* ptr;
        dumpable(const symmetry_table_type* p) : ptr(p) {}
        friend std::ostream& operator << (std::ostream& os, const dumpable& d)
        {
            os << d.ptr->letters << std::endl << d.ptr->table;
            return os;
        }
    };

    /*dumpable dump(const std::vector<label_type>&, bool latent=false) const { return {this}; }*/
    dumpable dump(const std::vector<label_type>&, bool) const { return {this}; }
    dumpable dump(const std::vector<label_type>&) const { return {this}; }

    std::vector<label_type> reorder_labels(const std::vector<label_type>& l, int method)
    {
        std::vector<label_type> ret(l.size());
        switch (method) {
            case 0:
                for (const auto& kv: table) {
                    ret[kv.first] = l[kv.second];
                }
                break;
            case 1:
                for (const auto& kv: table) {
                    ret[kv.second] = l[kv.first];
                }
                break;
            case 2:
                {
                    VectorXi idx(l.size());
                    for (int i = 0; i < idx.size(); ++i) { idx(i) = i; }
                    VectorXi n_idx = idx.transpose() * matrix().cast<int>();
                    for (int i = 0; i < idx.size(); ++i) {
                        ret[i] = l[n_idx[i]];
                    }
                }
                break;
            case 3:
                {
                    VectorXi idx(l.size());
                    for (int i = 0; i < idx.size(); ++i) { idx(i) = i; }
                    VectorXi n_idx = idx.transpose() * matrix().cast<int>();
                    for (int i = 0; i < idx.size(); ++i) {
                        ret[n_idx[i]] = l[i];
                    }
                }
                break;
            case 4:
                {
                    VectorXi idx(l.size());
                    for (int i = 0; i < idx.size(); ++i) { idx(i) = i; }
                    VectorXi n_idx = matrix().cast<int>() * idx;
                    for (int i = 0; i < idx.size(); ++i) {
                        ret[i] = l[n_idx[i]];
                    }
                }
                break;
            case 5:
                {
                    VectorXi idx(l.size());
                    for (int i = 0; i < idx.size(); ++i) { idx(i) = i; }
                    VectorXi n_idx = matrix().cast<int>() * idx;
                    for (int i = 0; i < idx.size(); ++i) {
                        ret[n_idx[i]] = l[i];
                    }
                }
                break;
        };
        return ret;
    }

    symmetry_table_type operator * (const symmetry_table_type& other) const
    {
        if (!letters.in_same_space(other.letters)) {
            return {};
        }
        return {table * other.table, letters * other.letters};
#if 0
        MSG_DEBUG("COMPOSING SYMMETRIES");
        MSG_DEBUG("" << (*this));
        MSG_DEBUG("AND");
        MSG_DEBUG("" << other);
        MSG_DEBUG("...");
        MatrixXb M1 = matrix();
        MatrixXb M2 = other.matrix();
        MatrixXb new_mat = kroneckerProduct(M1, M2);

        std::map<int, int> new_table;

        for (int j = 0; j < new_mat.cols(); ++j) {
            int i;
            new_mat.col(j).maxCoeff(&i);
            new_table.emplace(j, i);
        }

        letter_permutation_type new_letters;
        if (!letters.combine(other.letters, new_letters)) {
            MSG_DEBUG("foirure.");
        }

        symmetry_table_type ret(new_table, new_letters);
        MSG_DEBUG("RESULT:");
        MSG_DEBUG(ret);
        return ret;
#endif
    }

    symmetry_table_type
        lump(const std::set<subset>& P0) const
        {
            return {table.lump(P0), letters};
        }

    symmetry_table_type
        lump(const Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic>& mat) const
        {
            return {table.lump(mat), letters};
        }
};


typedef std::vector<symmetry_table_type> symmetry_list_type;

struct symmetry_group_type {
    typedef symmetry_list_type::const_iterator const_iterator;
    typedef symmetry_list_type::iterator iterator;
    typedef symmetry_list_type::reverse_iterator reverse_iterator;

    std::map<letter_permutation_type, size_t> m_letter_permutations;
    symmetry_list_type m_symmetries;

    symmetry_group_type() : m_letter_permutations(), m_symmetries() {}
    symmetry_group_type(const std::vector<label_type>& labels)
        : m_letter_permutations(), m_symmetries()
    {
        MSG_DEBUG("NEW GROUP FROM " << labels.size() << " LABELS");
        permutation_type permut = permutation_type::identity(labels.size());
        std::map<label_type::letter_type, label_type::letter_type> letters;
        for (const auto& l: labels) {
            letters[l.first()] = l.first();
            letters[l.second()] = l.second();
        }
        insert(symmetry_table_type{permut, letters});
    }

    static const symmetry_group_type&
        gamete()
        {
            static symmetry_group_type _(2);
            return _;
        }

    symmetry_group_type
        operator - (const symmetry_group_type& other) const
        {
            symmetry_group_type ret;
            for (const auto& kv: m_letter_permutations) {
                if (other.m_letter_permutations.find(kv.first) == other.m_letter_permutations.end()) {
                    /*ret.insert(m_symmetries[kv.second]);*/
                    ret.m_letter_permutations[kv.first] = ret.m_symmetries.size();
                    ret.m_symmetries.emplace_back(m_symmetries[kv.second]);
                }
            }
            return ret;
        }

    const_iterator cbegin() const { return m_symmetries.cbegin(); }
    const_iterator cend() const { return m_symmetries.cend(); }
    const_iterator begin() const { return m_symmetries.begin(); }
    const_iterator end() const { return m_symmetries.end(); }

    iterator begin() { return m_symmetries.begin(); }
    iterator end() { return m_symmetries.end(); }

    reverse_iterator rbegin() { return m_symmetries.rbegin(); }
    reverse_iterator rend() { return m_symmetries.rend(); }

    size_t size() const { return m_symmetries.size(); }

    bool
        insert_impl(const symmetry_table_type& sym)
        {
            auto i = m_letter_permutations.find(sym.letters);
            if (i != m_letter_permutations.end()) {
                MSG_DEBUG("-- SKIP   " << sym.letters);
                return false;
            }
            if (m_letter_permutations.emplace(sym.letters, m_symmetries.size()).second) {
                MSG_DEBUG("## INSERT " << sym.letters);
                m_symmetries.push_back(sym);
            }
            return true;
        }

    bool
        insert(const symmetry_table_type& sym)
        {
            size_t result = insert_impl(sym) + insert_impl(sym.inverse());
            if (result > 0) {
                generate(result);
            }
            return !!result;
        }

    void
        generate(size_t n)
        {
            MSG_DEBUG_INDENT;
            for (size_t j = m_symmetries.size() - n; j < m_symmetries.size(); ++j) {
                for (size_t i = 1; i < j; ++i) { /* skip identity ! WARNING must be certain identity has been put there first */
                    insert_impl(m_symmetries[i] * m_symmetries[j]);
                    insert_impl(m_symmetries[j] * m_symmetries[i]);
                }
                insert_impl(m_symmetries[j] * m_symmetries[j]);
            }
            MSG_DEBUG_DEDENT;
        }

    bool
        operator == (const symmetry_group_type& other) const
        {
            if (m_letter_permutations.size() != other.m_letter_permutations.size()) {
                return false;
            }
            auto i = m_letter_permutations.begin();
            auto j = m_letter_permutations.end();
            auto oi = other.m_letter_permutations.begin();
            for (; i != j && oi->first == i->first; ++i, ++oi);
            return i == j;
        }

    bool
        operator < (const symmetry_group_type& other) const
        {
            if (m_letter_permutations.size() < other.m_letter_permutations.size()) {
                return true;
            }
            if (m_letter_permutations.size() > other.m_letter_permutations.size()) {
                return false;
            }
            auto i = m_letter_permutations.begin();
            auto j = m_letter_permutations.end();
            auto oi = other.m_letter_permutations.begin();
            for (; i != j; ++i, ++oi) {
                if (i->first < oi->first) {
                    return true;
                } else if (oi->first < i->first) {
                    return false;
                }
            }
            return false;
        }

    friend
        std::ostream&
        operator << (std::ostream& os, const symmetry_group_type& grp)
        {
            if (grp.size() == 0) {
                return os << "[empty]";
            }
            std::vector<std::string> bracket;
            std::vector<std::string> bracket_r;
            bracket.reserve(grp.size());
            if (grp.size() == 1) {
                bracket.push_back("[");
                bracket_r.push_back("]");
            } else {
                bracket.push_back("⎡");
                bracket_r.push_back("⎤");
                for (size_t i = 2; i < grp.size(); ++i) {
                    bracket.push_back("⎢");
                    bracket_r.push_back("⎥");
                }
                bracket.push_back("⎣");
                bracket_r.push_back("⎦");
            }
            auto bi = bracket.begin();
            auto bri = bracket_r.begin();
            for (const auto& s: grp) {
                os << (*bi++) << s << (*bri++) << std::endl;
            }
            return os;
        }

    symmetry_group_type
        lump(const std::set<subset>& P0) const
        {
            symmetry_group_type ret;
            for (const auto& sym: m_symmetries) {
                ret.insert(sym.lump(P0));
            }
            return ret;
        }

    symmetry_group_type
        lump(const Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic>& mat) const
        {
            symmetry_group_type ret;
            for (const auto& sym: m_symmetries) {
                ret.insert(sym.lump(mat));
            }
            return ret;
        }

    template <typename DERIVED>
        symmetry_group_type
        permute(const permutation_base<DERIVED>& permut) const
        {
            MSG_DEBUG_INDENT_EXPR("[group.permute] ");
            MSG_DEBUG((*this));
            MSG_DEBUG("" << permut);
            symmetry_group_type ret;
            for (const auto& sym: m_symmetries) {
                ret.insert({permut * sym.table, sym.letters});
            }
            MSG_DEBUG_DEDENT;
            return ret;
        }

    private:
    symmetry_group_type(int) /* gamete letter-agnostic group */
        : m_letter_permutations(), m_symmetries()
    {
        insert(symmetry_table_type{permutation_type{1, 0}, letter_permutation_type{}});
    }
};

struct symmetry_mask_type {
    std::vector<std::vector<int>> m_mask;

    bool is_invalid() const { for (const auto& v: m_mask) { if (!v.size()) { return true; } } return false; }
    bool is_singletons() const { for (const auto& v: m_mask) { if (v.size() != 1) { return false; } } return true; }

    permutation_type
        as_permutation() const
        {
            std::vector<int> ret;
            ret.reserve(m_mask.size());
            for (const auto& v: m_mask) {
                ret.push_back(v.front());
            }
            return permutation_type{ret};
        }

    MatrixXb
        as_matrix() const
        {
            MatrixXb ret = MatrixXb::Zero(m_mask.size(), m_mask.size());
            for (size_t j = 0; j < m_mask.size(); ++j) {
                for (int i: m_mask[j]) {
                    ret(i, j) = 1;
                }
            }
            return ret;
        }

    static
        symmetry_mask_type
        build(const size_t L, const size_t R, const permutation_type& permut)
        {
            symmetry_mask_type ret;
            ret.m_mask.resize(L * R * permut.size(), {});
            L is the outer shape factor (number of repeats of the permutation pattern)
            R is the inner shape factor (number of repeats of each permutted state)
            const size_t inner_size = R * permut.size();
            auto
                pos = [&] (size_t li, int state, size_t ri)
                {
                    return li * inner_size + state * R + ri;
                };

            for (auto p: permut) {
                for (size_t li = 0; li < L; ++li) {
                    for (size_t lj = 0; lj < L; ++lj) {
                        for (size_t ri = 0; ri < R; ++ri) {
                            size_t s1 = pos(li, p.first, ri);
                            for (size_t rj = 0; rj < R; ++rj) {
                                size_t s2 = pos(lj, p.second, rj);
                                ret.m_mask[s1].push_back(s2);
                            }
                        }
                    }
                }
            }
            return ret;
        }

    symmetry_mask_type
        operator & (const symmetry_mask_type& other) const
        {
            symmetry_mask_type ret;
            ret.m_mask.resize(m_mask.size());
            for (size_t i = 0; i < m_mask.size(); ++i) {
                ret.m_mask[i].resize(std::max(m_mask[i].size(), other.m_mask[i].size()));
                auto end = std::set_intersection(
                        m_mask[i].begin(), m_mask[i].end(),
                        other.m_mask[i].begin(), other.m_mask[i].end(),
                        ret.m_mask[i].begin());
                ret.m_mask[i].resize(end - ret.m_mask[i].begin());
            }
            return ret;
        }

    friend
        std::ostream&
        operator << (std::ostream& os, const symmetry_mask_type& sm)
        {
            size_t n = 0;
            for (const auto& v: sm.m_mask) {
                os << (n++) << ":";
                if (v.size() == 0) {
                    os << "(empty)" << std::endl;
                    continue;
                }
                for (int i: v) {
                    os << ' ' << i;
                }
                os << std::endl;
            }
            return os;
        }
};


inline
permutation_type
combine_permutations(
        const std::pair<std::vector<size_t>, std::vector<size_t>>& LR,
        const std::vector<permutation_type>& permuts)
{
    symmetry_mask_type ret = symmetry_mask_type::build(LR.first[0], LR.second[0], permuts[0]);
    for (size_t i = 1; i < LR.first.size(); ++i) {
        ret = ret & symmetry_mask_type::build(LR.first[i], LR.second[i], permuts[i]);
        if (ret.is_invalid()) {
            MSG_DEBUG("<sym invalid after " << i << " steps>");
            return {};
        }
    }
    return ret.as_permutation();
}


inline
std::pair<bool, symmetry_table_type>
combine_symmetries(
        std::pair<std::vector<size_t>, std::vector<size_t>> LR, 
        std::function<symmetry_table_type(size_t)> get_sym,
        const std::vector<label_type>& labels, const MatrixXd& inf_mat)
{
    symmetry_mask_type ret = symmetry_mask_type::build(LR.first[0], LR.second[0], get_sym(0).table);
    for (size_t i = 1; i < LR.first.size(); ++i) {
        ret = ret & symmetry_mask_type::build(LR.first[i], LR.second[i], get_sym(i).table);
        if (ret.is_invalid()) {
            MSG_DEBUG("<sym invalid after " << i << " steps>");
            return {false, {}};
        }
    }
    return symmetry_table_type::build(ret.as_permutation(), labels, inf_mat, false);
}


inline
symmetry_group_type
combine_groups(
        std::pair<std::vector<size_t>, std::vector<size_t>> LR, 
        std::function<const symmetry_group_type&(size_t)> get_group,
        const std::vector<label_type>& labels, const MatrixXd& inf_mat)
{
    std::vector<symmetry_group_type::const_iterator> begins, ends, iterators;
    begins.resize(LR.first.size());
    iterators.resize(LR.first.size());
    ends.resize(LR.first.size());
    for (size_t i = 0; i < LR.first.size(); ++i) {
        MSG_DEBUG("group #" << i);
        MSG_DEBUG(get_group(i));
        iterators[i] = begins[i] = get_group(i).begin();
        ends[i] = get_group(i).end();
    }

    auto
        next = [&] ()
        {
            auto i = iterators.rbegin();
            auto j = iterators.rend();
            auto e = ends.rbegin();
            auto b = begins.rbegin();
            while (true) {
                if (i == j) {
                    return false;
                }
                ++*i;
                if (*i == *e) {
                    *i = *b;
                    ++i;
                    ++b;
                    ++e;
                } else {
                    return true;
                }
            }
        };

    std::function<const symmetry_table_type&(size_t)>
        get_sym = [&] (size_t i)
        {
            return *iterators[i];
        };

    symmetry_group_type ret(labels);

    do {
        auto result = combine_symmetries(LR, get_sym, labels, inf_mat);
        if (result.first) {
            ret.insert(result.second);
        }
    } while (next());

    return ret;
}


inline
symmetry_group_type
combine_groups(
        std::pair<std::vector<size_t>, std::vector<size_t>> LR, 
        const std::vector<symmetry_group_type>& groups,
        const std::vector<label_type>& labels, const MatrixXd& inf_mat)
{
    std::vector<symmetry_group_type::const_iterator> begins, ends, iterators;
    begins.resize(LR.first.size());
    iterators.resize(LR.first.size());
    ends.resize(LR.first.size());
    for (size_t i = 0; i < LR.first.size(); ++i) {
        MSG_DEBUG("group #" << i);
        MSG_DEBUG(groups[i]);
        MSG_QUEUE_FLUSH();
        iterators[i] = begins[i] = groups[i].begin();
        ends[i] = groups[i].end();
    }

    auto
        next = [&] ()
        {
            auto i = iterators.rbegin();
            auto j = iterators.rend();
            auto e = ends.rbegin();
            auto b = begins.rbegin();
            while (true) {
                if (i == j) {
                    return false;
                }
                ++*i;
                if (*i == *e) {
                    *i = *b;
                    ++i;
                    ++b;
                    ++e;
                } else {
                    return true;
                }
            }
        };

    std::function<symmetry_table_type(size_t)>
        get_sym = [&] (size_t i)
        {
            return *iterators[i];
        };

    symmetry_group_type ret(labels);

    do {
        auto result = combine_symmetries(LR, get_sym, labels, inf_mat);
        if (result.first) {
            ret.insert(result.second);
        } else {
            MSG_DEBUG("<couldn't build sym>");
        }
    } while (next());

    return ret;
}

#endif  /* 0 */



#endif

