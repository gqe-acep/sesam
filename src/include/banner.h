/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_BANNER_H_
#define _SPEL_BANNER_H_

/*#define VERSION_MAJOR "0"*/
/*#define VERSION_MINOR "1"*/
/*#define VERSION_PATCH "0"*/

#define BANNER ( \
"┌──────────────────────────────────────────────\n" \
"│Spell-QTL v" VERSION_MAJOR "." VERSION_MINOR "." VERSION_PATCH "\n" \
"│© 2013-ad aeternam S. Jasson, D. Leroux, Inra\n" \
"│\n" \
"│Species Perscrutandis Enixe Locis Locabuntur\n")

#if 0
"│▞▀▖         ▗        ▌        ▗  ▛▀▖                  ▐          ▌▗  ▞▀▖   ▜▜          ▌        ▐          │\n" \
"│▚▄ ▛▀▖▞▀▖▞▀▖▄ ▞▀▖▞▀▘ ▌  ▞▀▖▞▀▖▄  ▙▄▘▞▀▖▙▀▖▞▀▘▞▀▖▙▀▖▌ ▌▜▀ ▝▀▖▛▀▖▞▀▌▄  ▌  ▞▀▖▐▐ ▞▀▖▞▀▖▝▀▖▛▀▖▌ ▌▛▀▖▜▀ ▌ ▌▙▀▖  │\n" \
"│▖ ▌▙▄▘▛▀ ▌ ▖▐ ▛▀ ▝▀▖ ▌  ▌ ▌▌ ▖▐  ▌  ▛▀ ▌  ▝▀▖▌ ▖▌  ▌ ▌▐ ▖▞▀▌▌ ▌▌ ▌▐  ▌ ▖▌ ▌▐▐ ▌ ▌▌ ▖▞▀▌▌ ▌▌ ▌▌ ▌▐ ▖▌ ▌▌  ▗▖│\n" \
"│▝▀ ▌  ▝▀▘▝▀ ▀▘▝▀▘▀▀  ▀▀▘▝▀ ▝▀ ▀▘ ▘  ▝▀▘▘  ▀▀ ▝▀ ▘  ▝▀▘ ▀ ▝▀▘▘ ▘▝▀▘▀▘ ▝▀ ▝▀  ▘▘▝▀ ▝▀ ▝▀▘▀▀ ▝▀▘▘ ▘ ▀ ▝▀▘▘  ▝▘│\n" \
"└───────────────────────────────────────────────────────────────────────────────────────────────────────────┘")
#endif




#endif

