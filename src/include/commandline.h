/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_CMDLINE_H_
#define _SPEL_CMDLINE_H_

#include <set>
#include <map>
#include <algorithm>

#include <regex>

#include <cerrno>  /* for errno */
#include <cstring> /* for strerror */

#include "model/print.h"
#include "input.h"

extern "C" {
#include <ftw.h>
}

#define OUT(__x__) do { msg_handler_t::cout() << __x__; } while (0)

/*#include "computations.h"*/

/*namespace read_data {*/
/*settings_t* read_settings(std::istream&) {*/
#if 0
    if (!is.good()) {
        throw input_exception(is, "couldn't read from file");
    }
    return check_consistency(settings_dtd.parse(is));
#endif
    /*return NULL;*/
/*}*/
/*}*/


/*extern const char* prg_name = NULL;*/

inline
const std::string& prg_name(const std::string& new_name="")
{
    static std::string name = "";
    if (new_name != "") { name = new_name; }
    return name;
}


template <typename T>
T to(const std::string& x)
{
    std::stringstream s(x);
    T ret;
    s >> ret;
    return ret;
}


#define VCCI std::vector<const char*>::iterator
#define ARGUMENT_CALLBACK_ARGS(_T) _T*& target, VCCI& ai, VCCI& aj
#define SAFE_IGNORE_CALLBACK_ARGS do { (void)target; (void)ai; (void)aj; } while(0)

/*typedef std::function<void(CALLBACK_ARGS)> argument_parser_callback_type;*/

template <typename TARGET>
using argument_parser_callback_type = std::function<void(ARGUMENT_CALLBACK_ARGS(TARGET))>;


struct default_value_display {
    std::string str;

    default_value_display()
        : str()
    {}

    default_value_display(bool req)
        : str()
    {
        std::stringstream ss;
        ss << WHITE << (req ? "MANDATORY" : "Optional") << NORMAL;
        str = ss.str();
    }

    default_value_display(const char* s)
        : str()
    {
        std::stringstream ss;
        ss << "Default: " << WHITE << s << NORMAL;
        str = ss.str();
    }

    default_value_display(default_value_display&& dvd)
        : str(std::forward<std::string>(dvd.str))
    {}

    default_value_display(const default_value_display& dvd)
        : str(dvd.str)
    {}

    template <typename C, typename X>
    default_value_display(X C::* x)
        : str()
    {
        C s;
        std::stringstream ss;
        ss << "Default: " << WHITE << SPELL_STRING(s.*x) << NORMAL;
        str = ss.str();
    }

    template <typename CX, typename X, typename CY, typename Y>
    default_value_display(X CX::* x, Y CY::* y)
        : str()
    {
        CX cx;
        CY cy;
        std::stringstream ss;
        ss << "Default: " << WHITE << model_print::to_string(cx.*x) << NORMAL << ", " << WHITE << model_print::to_string(cy.*y) << NORMAL;
        str = ss.str();
    }

    template <typename K>
    default_value_display(const K* var)
        : str()
    {
        std::stringstream ss;
        ss << "Default: " << WHITE << model_print::to_string(*var) << NORMAL;
        str = ss.str();
    }
};


template <typename TARGET>
struct argument {
    std::vector<std::string> names;
    std::vector<std::string> parameters;
    std::string description;
    bool hidden;
    default_value_display dvd;
    argument_parser_callback_type<TARGET> callback;

    argument(std::vector<std::string>&& n,
             std::vector<std::string>&& p,
             std::string&& d,
             bool h,
             default_value_display&& dv,
             argument_parser_callback_type<TARGET> c)
        : names(std::forward<std::vector<std::string>>(n))
        , parameters(std::forward<std::vector<std::string>>(p))
        , description(std::forward<std::string>(d))
        , hidden(h)
        , dvd(std::forward<default_value_display>(dv))
        , callback(std::forward<argument_parser_callback_type<TARGET>>(c))
    {}

    void install(std::map<std::string, argument_parser_callback_type<TARGET>>& amap,
                 std::map<std::string, int>& pmap) const
    {
        for (auto& n: names) {
            amap[n] = callback;
            pmap[n] = parameters.size();
        }
    }
};


template <typename TARGET>
struct argument_section {
    std::string title;
    std::string description;
    bool hidden;
    std::vector<argument<TARGET>> arguments;
};

/*typedef std::vector<argument_section> argument_section_list_t;*/

template <typename TARGET>
using argument_section_list_t = std::vector<argument_section<TARGET>>;



inline
std::vector<std::string>
cut(const std::string& s, int width)
{
    std::vector<std::string> ret;
    auto i = s.begin();
    auto z = s.end();
    while (i != z) {
        auto j = z;
        if ((z - i) >= width) {
            j = i + width;
#if 0
        } else {
            ret.push_back(std::string(i, z));
            return ret;
            /*j = z;*/
#endif
        }
        auto last = j;
        auto k = i;
        for (; k != j; ++k) {
            if (*k == ' ') {
                last = k;
            } else if (*k == '\n') {
                last = k;
                break;
            }
        }
        if (k == z) {
            last = z;
        }
        ret.push_back(std::string(i, last));
        /*OUT(ret.back() << std::endl);*/
        i = last + (last != z);
    }
    return ret;
}


template <typename TARGET>
void print_usage_impl(bool show_hidden, const argument_section_list_t<TARGET>& aslt)
{
    int max_cols = msg_handler_t::termcols();
    int opt_width = 0;
    int doc_width = 0;
    if (max_cols <= 0) {
        max_cols = 80;
    }
    settings_t tmp;
    OUT("Usage: " << WHITE << prg_name() << " [arguments...]" << NORMAL << std::endl);
    OUT("Arguments:" << std::endl);
    for (auto& kv: aslt) {
        if (kv.hidden && !show_hidden) {
            continue;
        }
        for (auto& a: kv.arguments) {
            if (a.hidden && !show_hidden) {
                continue;
            }
            auto ni = a.names.begin(), nj = a.names.end();
            int width = 0;
            if (ni != nj) {
                width = ni->size();
                for (++ni; ni != nj; ++ni) {
                    width += 1 + ni->size();
                }
            }
            for (auto& par: a.parameters) {
                width += 3 + par.size();
            }
            if (width > opt_width) {
                opt_width = width;
            }
        }
    }
    opt_width += 2;
    doc_width = max_cols - opt_width;
    for (auto& kv: aslt) {
        if (kv.hidden && !show_hidden) {
            continue;
        }
        OUT(std::endl << CYAN <<  "* " << kv.title << NORMAL << std::endl);
        if (kv.description.size() > 0) {
            for (const auto& line: cut(kv.description, max_cols - 2)) {
                OUT("  " << line << std::endl);
            }
        }
        for (auto& a: kv.arguments) {
            if (a.hidden && !show_hidden) {
                continue;
            }
            auto ni = a.names.begin(), nj = a.names.end();
            std::stringstream s;
            s << WHITE << (*ni);
            ++ni;
            for (; ni != nj; ++ni) { s << NORMAL << ',' << WHITE << (*ni); }
            s << NORMAL;
            for (auto& par: a.parameters) { s << " <" << YELLOW << par << NORMAL << '>'; }
            /* find max length under 30 without cutting words */
            std::vector<std::string> lines = cut(a.description, doc_width);
            auto i = lines.begin(), j = lines.end();
            OUT(std::setw(opt_width) <<  "" << (*i));
            OUT('\r' << s.str() << std::endl);
            for (++i; i != j; ++i) {
                OUT(std::setw(opt_width) << "" << (*i) << std::endl);
            }
            if (a.dvd.str.size()) {
                lines = cut(a.dvd.str, doc_width);
                i = lines.begin();
                j = lines.end();
                OUT(std::setw(opt_width) <<  "" << (*i) << std::endl);
                for (++i; i != j; ++i) {
                    OUT(std::setw(opt_width) << "" << (*i) << std::endl);
                }
            }
        }
    }
}



template <typename TARGET>
struct argument_parser {
    std::map<std::string, argument_parser_callback_type<TARGET>> amap;
    std::map<std::string, int> pmap;
    std::function<void(bool)> print_usage;
    argument_parser(const std::vector<argument<TARGET>>& args)
        : amap()
        , print_usage()
    {
        for (auto& a: args) { a.install(amap, pmap); }
    }
    argument_parser(const argument_section_list_t<TARGET>& sections)
        : amap()
        , print_usage()
    {
        for (auto& kv: sections) {
            for (auto& a: kv.arguments) { a.install(amap, pmap); }
        }
        print_usage = [&] (bool t) { print_usage_impl(t, sections); };
    }

    void fail_usage()
    {
        print_usage(false);
        exit(-1);
    }

    bool operator () (ARGUMENT_CALLBACK_ARGS(TARGET))
    {
        if (amap.find(*ai) == amap.end()) {
            MSG_ERROR("Unknown argument " << (*ai), "");
            fail_usage();
        }
        bool parameters_ok = (aj - ai) > pmap[*ai]; /* il reste au moins n_param mots dans la CLI */
        if (parameters_ok) {
            auto ak = ai;
            ++ak;
            for (int k = pmap[*ai]; parameters_ok && k > 0; --k, ++ak) {
                parameters_ok = (pmap.find(*ak) == pmap.end());
            }
            if (parameters_ok) {
                parameters_ok = (ak == aj || pmap.find(*ak) != pmap.end()); /* il faut s'assurer que le mot suivant est une option valide, ou qu'on a fini */
                if (!parameters_ok && ak != aj) {
                    MSG_ERROR("Unknown argument " << (*ak), "");
                    return false;
                }
            }
        }
        if (!parameters_ok) {
            MSG_ERROR("Expected exactly " << pmap[*ai] << " parameter" << (pmap[*ai] > 1 ? "s" : "") << " after argument " << (*ai), "");
            fail_usage();
            return false;
        }
        amap[*ai](target, ai, aj);
        return true;
    }
};



template <typename TARGET>
struct constraint_t {
    std::string message;
    std::string workaround;
    std::function<bool(const TARGET*)> predicate;

    static std::vector<constraint_t<TARGET>>& list()
    {
        static std::vector<constraint_t<TARGET>> _;
        return _;
    }

    static void check(const TARGET* target)
    {
        /*msg_handler_t::reset();*/
        std::pair<bool, std::set<std::string>> ret;
        /*MSG_DEBUG("constraints: " << list().size());*/
        chrono::start("Consistency check");
        for (auto& c: list()) {
            bool result = c.predicate(target);
            if (!result) {
                MSG_ERROR(c.message, c.workaround);
            }
        }
        chrono::stop("Consistency check");
        msg_handler_t::check(true);
    }

    constraint_t(
            const std::string& m,
            const std::string& w,
            std::function<bool(const TARGET*)> p)
        : message(m), workaround(w), predicate(p)
    {
        list().push_back(*this);
    }
};


/*range<int>*/
/*individual_range(const population* pop);*/

/*multi_generation_observations*/
/*population_marker_obs(const context_key& ck, int ind);*/


#define CONSTRAINT_PREDICATE(_T) [](const _T* s)




#endif

