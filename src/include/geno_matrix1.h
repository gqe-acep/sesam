/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_GENO_MATRIX_H_
#define _SPEL_GENO_MATRIX_H_

#include "eigen.h"
#include "error.h"
#include "labelled_matrix.h"
#include "lumping2.h"
#include "permutation.h"
#include <limits>
#include <cmath>

#define FLOAT_TOL 1.e-6


#undef WITH_OVERLUMPING
#define WITH_OVERLUMPING


#define GAMETE_L '<'
#define GAMETE_R '>'
#define GAMETE_EMPTY ' '

#define NOT_GAMETE(_l) (_l != GAMETE_EMPTY)
/*#define eq_dbl(_x_, _y_) (fabs((_x_) - (_y_)) < FLOAT_TOL)*/
/*#define neq_dbl(_x_, _y_) (fabs((_x_) - (_y_)) >= FLOAT_TOL)*/

inline
bool eq_dbl(double a, double b) { return fabs(a - b) < FLOAT_TOL; }

inline
bool eq_dbl(double a, double b, double tol_) { return fabs(a - b) < tol_; }

inline
bool neq_dbl(double a, double b) { return fabs(a - b) >= FLOAT_TOL; }

inline
bool operator < (const std::vector<int>& v1, const std::vector<int>& v2)
{
    if (v1.size() < v2.size()) {
        return true;
    }
    if (v1.size() == v2.size()) {
        auto i1 = v1.begin();
        auto i2 = v2.begin();
        auto j1 = v1.end();
        for (; i1 != j1 && (*i1 == *i2); ++i1, ++i2);
        return i1 != j1 && (*i1 < *i2);
    }
    return false;
}

/*using namespace Eigen;*/

typedef Eigen::Matrix<char, Eigen::Dynamic, Eigen::Dynamic> MatrixXc;
typedef Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic> MatrixXb;
typedef Eigen::Matrix<bool, Eigen::Dynamic, 1> VectorXb;


typedef std::pair<char, char> label_type;
enum geno_matrix_variant_type {Nonsense=0, Gamete, DoublingGamete, SelfingGamete, Haplo, Geno, _GMVT_size};


extern geno_matrix_variant_type geno_matrix_variant_table[_GMVT_size][_GMVT_size];


inline
std::ostream& operator << (std::ostream& os, const label_type& l)
{
    return os << (l.first ? l.first : '*') << (l.second ? l.second : '*');
}

inline
std::ostream& operator << (std::ostream& os, const std::vector<label_type>& vl)
{
    for (const auto& l: vl) { os << ' ' << l; }
    return os;
}


inline
bool operator == (const label_type& l1, const label_type& l2)
{
    return (l1.first == l2.first && l1.second == l2.second)
        || ((l1.first == 0 || l2.first == 0) && (l1.second == 0 || l2.second == 0));
}


inline
std::ostream& operator << (std::ostream& os, const std::map<char, int>& L)
{
    os << '{' << L.size() << ':';
    for (const auto& kv: L) {
        os << ' ' << kv.first;
    }
    return os << '}';
}


#ifdef WITH_OVERLUMPING

    inline
    MatrixXb latent_lump_m(const MatrixXb& collect, const MatrixXb& sym)
    {
        MSG_DEBUG_INDENT_EXPR("[latent_lump] ");
        MatrixXb tco = collect * sym;
        MSG_DEBUG("tco");
        MSG_DEBUG(tco);
        MatrixXb ret = MatrixXb::Zero(tco.rows(), tco.rows());
        int col = 0;
        for (int i = 0; i < tco.cols(); ++i) {
            bool new_one = true;
            for (int c = 0; c < col; ++c) {
                if (ret.col(c) == tco.col(i)) {
                    new_one = false;
                    break;
                }
            }
            if (new_one) {
                ret.col(col) = tco.col(i);
                ++col;
            }
        }
        MSG_DEBUG("ret");
        MSG_DEBUG(ret);
        MSG_DEBUG_DEDENT;
        return ret;
    }

    inline
    permutation_type latent_lump(const MatrixXb& collect, const permutation_type& sym)
    {
        return permutation_type{latent_lump_m(collect, sym.matrix<bool>())};
    }

    /*MatrixXb generate_lozenge(size_t n0, size_t n1, bool antidiag=false)*/
    inline
    MatrixXb generate_lozenge(size_t n0, size_t n1, bool rev0=false, bool rev1=false)
    {
        int N = n0 * n1;
        /*if (N == 2) {*/
            /*return (MatrixXb::Ones(2, 2) - MatrixXb::Identity(2, 2));*/
        /*}*/
        MatrixXb ret = MatrixXb::Zero(N, N);
        for (size_t i0 = 0; i0 < n0; ++i0) {
            for (size_t i1 = 0; i1 < n1; ++i1) {
                /*int i = i1 * n0 + (rev0 ? n0 - 1 - i0 : i0);*/
                /*int j = i0 * n1 + (rev1 ? n1 - 1 - i1 : i1);*/
                int i = (rev1 ? n1 - 1 - i1 : i1) * n0 + i0;
                int j = (rev0 ? n0 - 1 - i0 : i0) * n1 + i1;
                /*ret(antidiag ? N - 1 - i : i, j) = 1;*/
                ret(i, j) = 1;
            }
        }
        /*MSG_DEBUG("lozenge(" << n0 << ", " << n1 << ',' << (antidiag ? "anti" : "diag") << ')');*/
        /*MSG_DEBUG(ret);*/
        /*MSG_DEBUG("-- self-adjoint?");*/
        /*MSG_DEBUG((ret - ret.transpose()));*/
        /*MSG_DEBUG("-- self-inverse?");*/
        /*MSG_DEBUG((ret * ret));*/
        return ret;
    }

    inline
    MatrixXb generate_lozenge(size_t n0, size_t n1, const MatrixXb& permut)
    {
        int N = n0 * n1;
        /*if (N == 2) {*/
            /*return (MatrixXb::Ones(2, 2) - MatrixXb::Identity(2, 2));*/
        /*}*/

        std::vector<int> permut_col(permut.cols());
        std::vector<int> permut_row(permut.rows());

        for (int c = 0; c < permut.cols(); ++c) {
            int r;
            permut.col(c).maxCoeff(&r);
            permut_col[r] = c;
            permut_row[c] = r;
        }

        MatrixXb ret = MatrixXb::Zero(N, N);
        for (size_t i0 = 0; i0 < n0; ++i0) {
            for (size_t i1 = 0; i1 < n1; ++i1) {
                int i = i1 * n0 + i0;
                int j = i0 * n1 + i1;
                /*ret(permut_col[i], permut_row[j]) = 1;*/
                ret(permut_row[i], permut_col[j]) = 1;
            }
        }
        /*MSG_DEBUG("lozenge(" << n0 << ", " << n1 << ',' << (antidiag ? "anti" : "diag") << ')');*/
        /*MSG_DEBUG(ret);*/
        /*MSG_DEBUG("-- self-adjoint?");*/
        /*MSG_DEBUG((ret - ret.transpose()));*/
        /*MSG_DEBUG("-- self-inverse?");*/
        /*MSG_DEBUG((ret * ret));*/
        return ret;
    }

    inline
    MatrixXb generate_lozenge(const std::vector<size_t>& sizes, const std::vector<size_t>& idx_permut, const std::vector<bool>& reverse_gamete)
    {
        std::vector<size_t> cursors(sizes.size(), 0);

        size_t sz = 1;
        for (size_t s: sizes) { sz *= s; }

        int start = cursors.size() - 1;

        auto idx1 = [&] ()
        {
            size_t ret = 0;
            for (int i = start; i >= 0; --i) {
                ret *= sizes[i];
                ret += cursors[i];
            }
            return ret;
        };

        auto idx2 = [&] () {
            size_t ret = 0;
            for (int i = start; i >= 0; --i) {
                ret *= sizes[idx_permut[i]];
                size_t curs = cursors[idx_permut[i]];
                if (reverse_gamete[i]) {
                    curs = sizes[i] - 1 - curs;
                }
                ret += curs;
            }
            return ret;
        };

        auto next = [&] ()
        {
            auto i = cursors.rbegin();
            auto j = cursors.rend();
            auto si = sizes.rbegin();
            for (; i != j; ++i) {
                ++*i;
                if (*i == *si) {
                    *i = 0;
                } else {
                    break;
                }
            }
            return i != j;
        };

        MatrixXb ret = MatrixXb::Zero(sz, sz);

        do {
            ret(idx2(), idx1()) = 1;
        } while (next());

        return ret;
    }

    struct letter_permutation_type {
        std::map<char, char> table;

        letter_permutation_type() : table() {}

        letter_permutation_type(const std::map<char, char>& init)
            : table(init)
        {}

        letter_permutation_type(std::initializer_list<std::pair<const char, char>> init)
            : table(init)
        {}

        letter_permutation_type(const letter_permutation_type& reference, const MatrixXb& new_permut)
            : table()
        {
            int col = 0, row;
            std::vector<char> letters;
            letters.reserve(reference.table.size());
            for (const auto& kv: reference.table) {
                letters.push_back(kv.first);
            }
            for (const auto& kv: reference.table) {
                new_permut.col(col).maxCoeff(&row);
                table.emplace_hint(table.end(), kv.first, letters[row]);
                ++col;
            }
        }

        static
            letter_permutation_type identity(const std::vector<label_type>& labels)
            {
                letter_permutation_type ret;
                for (const auto& l: labels) {
                    ret.table[l.first] = l.first;
                    ret.table[l.second] = l.second;
                }
                return ret;
            }

        bool empty() const { return table.size() == 0; }

        MatrixXb matrix() const
        {
            MatrixXb ret = MatrixXb::Zero(table.size(), table.size());
            std::map<char, int> indices;
            for (const auto& kv: table) {
                int sz = indices.size();
                indices[kv.first] = sz;
            }
            for (const auto& kv: table) {
                /*MSG_DEBUG(kv.second << '[' << indices[kv.second] << "] " << kv.first << '[' << indices[kv.first] << ']');*/
                /*MSG_QUEUE_FLUSH();*/
                ret(indices[kv.second], indices[kv.first]) = 1;
            }
            return ret;
        }

        MatrixXc pretty_print_matrix() const
        {
            MatrixXc ret = MatrixXc::Constant(table.size() + 3, table.size() + 3, ' ');
            int x = 0;
            for (int c = 0; c < ret.cols(); ++c) {
                ret(0, c) = '-';
                ret(ret.rows() - 1, c) = '-';
                ret(c, 0) = ':';
                ret(c, ret.cols() - 1) = ':';
            }
            ret(0, 0) = ret(0, ret.cols() - 1) = '.';
            ret(ret.rows() - 1) = ret(ret.rows() - 1, ret.cols() - 1) = '\'';
            for (const auto& kv: table) {
                ret(1, 2 + x) = kv.first;
                ret(2 + x, 1) = kv.first;
                ++x;
            }
            ret.block(2, 2, table.size(), table.size()) = (' ' + ('@' - ' ') * matrix().cast<char>().array()).matrix();
            return ret;
        }

        friend
            std::ostream& operator << (std::ostream& os, const letter_permutation_type& st)
            {
                auto i = st.table.begin();
                auto j = st.table.end();
                os << '{';
                if (i != j) {
                    os << i->first << ':' << i->second;
                    for (++i; i != j; ++i) {
                        os << ' ' << i->first << ':' << i->second;
                    }
                }
                return os << '}';
            }

        bool combine(const letter_permutation_type& other, letter_permutation_type& result) const
        {
            if (other.table.size() == 0) {
                result.table = table;
                return true;
            }
            if (table.size() == 0) {
                result.table = other.table;
                return true;
            }
            letter_permutation_type ret;
            auto ti = table.begin(), tj = table.end();
            auto oi = other.table.begin(), oj = other.table.end();

            for (; ti != tj && oi != oj;) {
                if (ti->first < oi->first) {
                    result.table.insert(result.table.end(), *ti);
                    ++ti;
                } else if (ti->first > oi->first) {
                    result.table.insert(result.table.end(), *oi);
                    ++oi;
                } else if (ti->second != oi->second) {
                    result.table.clear();
                    return false;
                } else {
                    result.table.insert(result.table.end(), *ti);
                    ++ti;
                    ++oi;
                }
            }
            for (; ti != tj; ++ti) {
                result.table.insert(result.table.end(), *ti);
            }
            for (; oi != oj; ++oi) {
                result.table.insert(result.table.end(), *oi);
            }
            return true;
        }

        letter_permutation_type compose(const letter_permutation_type& other) const
        {
            if (other.table.size() == 0) {
                return *this;
            }
            if (table.size() == 0) {
                return other;
            }
            MatrixXb m = matrix() * other.matrix();
            if (!(m.transpose() * m - MatrixXb::Identity(m.rows(), m.cols())).isZero()) {
                MSG_DEBUG("FOIRURE MATRICE DE PERMUTATION DE LETTRES COMBINEE");
                MSG_DEBUG(matrix());
                MSG_DEBUG("--");
                MSG_DEBUG(other.matrix());
                MSG_DEBUG("--");
                MSG_DEBUG(m);
                MSG_QUEUE_FLUSH();
                /*abort();*/
                return {};
            }
            return {*this, m};
        }

        bool operator < (const letter_permutation_type& other) const { return table < other.table; }
        bool operator == (const letter_permutation_type& other) const { return table == other.table; }
    };

    struct symmetry_table_type {
        //std::vector<std::pair<char, char>> switches;  /* over letters */
        //std::map<int, int> table;                     /* over states */
        permutation_type table;
        letter_permutation_type letters;

        symmetry_table_type(/*const std::vector<std::pair<char, char>>& _s, */const std::map<int, int>& _t, const letter_permutation_type& _l)
            : /*switches(_s),*/ table(_t), letters(_l)
        {
            /*MSG_DEBUG("NEW SYMMETRY_TABLE_TYPE WITH " << switches.size() << " SWITCHES AND " << table.size() << " STATES");*/
            /*for (const auto& s: switches) {*/
                /*if (s.first < 2) {*/
                    /*throw 0;*/
                /*}*/
                /*if (s.second < 2) {*/
                    /*throw 1;*/
                /*}*/
            /*}*/
        }

#if 0
        symmetry_table_type(const permutation_type& state_matrix, const std::vector<int>& permut_col, const std::vector<int>& permut_row)
            : table(), letters()
        {
            /*std::set<std::pair<char, char>> tmp_switches;*/
            for (int j = 0; j < state_matrix.cols(); ++j) {
                for (int i = 0; i < state_matrix.rows(); ++i) {
                    if (state_matrix(i, j)) {
                        /*MSG_DEBUG("" << labels[j] << " -> " << labels[i]);*/
                        /*tmp_switches.emplace(labels[j].first, labels[i].first);*/
                        /*if (NOT_GAMETE(labels[i].second) && NOT_GAMETE(labels[j].second)) {*/
                            /*tmp_switches.emplace(labels[j].second, labels[i].second);*/
                        /*}*/
                        table[permut_col[j]] = permut_row[i];
                        /*MSG_DEBUG(i << ',' << j << " => " << permut_col[j] << ',' << permut_row[i]);*/
                    }
                }
            }
        }
#endif

        symmetry_table_type(/*const std::vector<label_type>& labels, */const permutation_type& state_matrix, const letter_permutation_type& l)
            : /*switches(),*/ table(state_matrix), letters(l)
        {}

        symmetry_table_type() : table() {}
        symmetry_table_type& operator = (const symmetry_table_type& other) { table = other.table; letters = other.letters; return *this; }

        friend
            std::ostream& operator << (std::ostream& os, const symmetry_table_type& st)
            {
                os << st.letters << '-';
                auto i = st.table.begin();
                auto j = st.table.end();
                os << '{';
                if (i != j) {
                    os << i->first << ':' << i->second;
                    for (++i; i != j; ++i) {
                        os << ' ' << i->first << ':' << i->second;
                    }
                }
                return os << '}';
            }

        std::vector<std::pair<char, char>> switches_from_labels(const std::vector<label_type>& labels, bool latent=false) const
        {
            std::set<std::pair<char, char>> tmp;
            /*MSG_DEBUG_INDENT_EXPR("[switches_from_labels#" << latent << "] ");*/
            if (latent && labels.front().second != GAMETE_EMPTY) {
                for (const auto& kv: table) {
                    /*MSG_DEBUG(kv.first << ',' << kv.second << ' ' << labels[kv.first] << ',' << labels[kv.second]);*/
                    tmp.emplace(labels[kv.first].first, labels[kv.second].second);
                    tmp.emplace(labels[kv.first].second, labels[kv.second].first);
                }
            } else {
                for (const auto& kv: table) {
                    /*MSG_DEBUG(kv.first << ',' << kv.second << ' ' << labels[kv.first] << ',' << labels[kv.second]);*/
                    tmp.emplace(labels[kv.first].first, labels[kv.second].first);
                    if (labels[kv.first].second != GAMETE_EMPTY) {
                        tmp.emplace(labels[kv.first].second, labels[kv.second].second);
                    }
                }
            }
            /*MSG_DEBUG_DEDENT;*/
            return {tmp.begin(), tmp.end()};
        }

        bool is_consistent(const std::vector<label_type>& labels, bool latent=false) const
        {
            auto switches = switches_from_labels(labels, latent);
            auto smap = switch_map(labels, latent);
            letter_permutation_type tmp(smap);
            MSG_DEBUG_INDENT_EXPR("[is_consistent] ");
            MSG_DEBUG("smap");
            MSG_DEBUG(tmp);
            MSG_DEBUG("letters");
            MSG_DEBUG(letters);
            MSG_DEBUG_DEDENT;
            for (const auto& s: smap) {
                if (s.second == '!') {
                    return false;
                }
            }
            return smap == letters.table;
            /*MSG_DEBUG("switches.size() = " << switches.size());*/
            /*MSG_DEBUG("switch_map().size() = " << smap.size());*/
            /*return switches.size() == smap.size();*/
        }

        static
        std::map<char, int> letter_indices(const std::vector<std::pair<char, char>>& data)
        {
            std::map<char, int> ret;
            for (const auto& s: data) {
                if (s.first != GAMETE_EMPTY) {
                    ret[s.first] = 0;
                }
                if (s.second != GAMETE_EMPTY) {
                    ret[s.second] = 0;
                }
            }
            int i = 0;
            for (auto& l: ret) {
                l.second = i++;
                /*MSG_DEBUG("assign " << l.second << " to letter " << l.first);*/
            }
            return ret;
        }

        std::map<char, char> switch_map(const std::vector<label_type>& labels, bool latent=false) const
        {
            auto switches = switches_from_labels(labels, latent);
            std::map<char, char> ret;
            for (const auto& s: switches) {
                auto it = ret.find(s.first);
                if (it != ret.end() && it->second != s.second) {
                    it->second = '!';
                } else {
                ret[s.first] = s.second;
                }
            }
            return ret;
        }

        MatrixXb switch_matrix(const std::vector<label_type>& labels, bool latent) const
        {
            auto l = letter_indices(labels);
            auto switches = switches_from_labels(labels, latent);
            MatrixXb ret = MatrixXb::Zero(l.size(), l.size());
            for (const auto& s: switches) {
                /*MSG_DEBUG("switch " << s.first << " -> " << s.second);*/
                ret(l[s.second], l[s.first]) = 1;
            }
            /* TODO: parano */
            return ret;
        }

        static
        std::vector<std::pair<char, char>> switches_from_matrix(const std::vector<label_type>& labels, const MatrixXb& switch_mat)
        {
            auto l = letter_indices(labels);
            std::vector<std::pair<char, char>> ret;
            ret.reserve(switch_mat.cols());
            for (const auto& kv1: l) {
                for (const auto& kv2: l) {
                    if (switch_mat(kv2.second, kv1.second)) {
                        ret.emplace_back(kv1.first, kv2.first);
                    }
                }
            }
            return ret;
        }

        MatrixXb matrix() const
        {
            MatrixXb ret = MatrixXb::Zero(table.size(), table.size());
            for (const auto& kv: table) {
                /*MSG_DEBUG("kv={" << kv.first << ',' << kv.second << '}');*/
                ret(kv.second, kv.first) = 1;
            }
            /* TODO: parano */
            /*MSG_DEBUG_INDENT_EXPR("[symmetry matrix] ");*/
            /*MSG_DEBUG(ret);*/
            /*MSG_DEBUG_DEDENT;*/
            return ret;
        }

        bool operator < (const symmetry_table_type& other) const { return letters < other.letters; }
        bool operator == (const symmetry_table_type& other) const { return letters == other.letters; }
        bool operator == (const letter_permutation_type& lp) const { return letters == lp; }

        friend
            symmetry_table_type kronecker(const symmetry_table_type& s1, const symmetry_table_type& s2)
            {
                MatrixXb mat = kroneckerProduct(s1.matrix(), s2.matrix());
                /*return {labels, mat};*/
                letter_permutation_type comb;
                s1.letters.combine(s2.letters, comb);
                return {permutation_type{mat}, comb};
            }

#if 0
        struct dumpable {
            /*std::map<char, int> letters;*/
            std::vector<label_type> labels;
            MatrixXb switch_matrix;
            MatrixXb matrix;
            letter_permutation_type letters;
            dumpable(const std::vector<label_type>& labels_, const symmetry_table_type& st, bool latent, const letter_permutation_type& l)
                : /*letters(st.letter_indices(labels_))*/
                /*, */
                  labels(labels_)
                , switch_matrix(st.switch_matrix(labels_, latent))
                , matrix(st.matrix())
                , letters(l)
            {}

            friend
                std::ostream& operator << (std::ostream& os, const dumpable& st)
                {
                    auto mat = st.switch_matrix;
#if 0
                    Eigen::Matrix<char, Eigen::Dynamic, Eigen::Dynamic> temp = 
                        Eigen::Matrix<char, Eigen::Dynamic, Eigen::Dynamic>::Constant(st.letters.size() + 3, st.letters.size() + 3, ' ');
                    temp.block(2, 2, st.letters.size(), st.letters.size()) = ('.' + ('@' - '.') * mat.cast<char>().array()).matrix();
                    int i = 1;
                    for (const auto& kv: st.letters) {
                        temp(1, i + 1) = temp(i + 1, 1) = kv.first;
                        temp(i + 1, 0) = temp(i + 1, 2 + st.letters.size()) = '|';
                        temp(0, 1 + i) = temp(2 + st.letters.size(), i + 1) = '-';
                        ++i;
                    }
                    temp(1, 0) = temp(1, 2 + st.letters.size()) = '|';
                    temp(0, 1) = temp(2 + st.letters.size(), 1) = '-';
                    os << temp << std::endl;
#else
                    os << st.letters.pretty_print_matrix() << std::endl;
#endif
                    os << "  ";
                    for (const auto& l: st.labels) {
                        os << ' ' << l;
                    }
                    os << std::endl;

                    for (int r = 0; r < st.matrix.rows(); ++r) {
                        os << "" << st.labels[r];
                        for (int c = 0; c < st.matrix.cols(); ++c) {
                            os << "  " << (st.matrix(r, c) ? '@' : '.');
                        }
                        if (r != st.matrix.rows() - 1) {
                            os << std::endl;
                        }
                    }

                    /*return os << ('.' + ('@' - '.') * st.matrix.cast<char>().array());*/
                    return os;
                }
        };

        dumpable dump(const std::vector<label_type>& labels, bool latent=false) const { return {labels, *this, latent, letters}; }
#else
        struct dumpable {
            const symmetry_table_type* ptr;
            dumpable(const symmetry_table_type* p) : ptr(p) {}
            friend std::ostream& operator << (std::ostream& os, const dumpable& d)
            {
                os << d.ptr->letters << std::endl << d.ptr->table;
                return os;
            }
        };

        dumpable dump(const std::vector<label_type>&, bool latent=false) const { return {this}; }
#endif

        std::vector<label_type> reorder_labels(const std::vector<label_type>& l, int method)
        {
            std::vector<label_type> ret(l.size());
            switch (method) {
                case 0:
                    for (const auto& kv: table) {
                        ret[kv.first] = l[kv.second];
                    }
                    break;
                case 1:
                    for (const auto& kv: table) {
                        ret[kv.second] = l[kv.first];
                    }
                    break;
                case 2:
                    {
                        VectorXi idx(l.size());
                        for (int i = 0; i < idx.size(); ++i) { idx(i) = i; }
                        VectorXi n_idx = idx.transpose() * matrix().cast<int>();
                        for (int i = 0; i < idx.size(); ++i) {
                            ret[i] = l[n_idx[i]];
                        }
                    }
                    break;
                case 3:
                    {
                        VectorXi idx(l.size());
                        for (int i = 0; i < idx.size(); ++i) { idx(i) = i; }
                        VectorXi n_idx = idx.transpose() * matrix().cast<int>();
                        for (int i = 0; i < idx.size(); ++i) {
                            ret[n_idx[i]] = l[i];
                        }
                    }
                    break;
                case 4:
                    {
                        VectorXi idx(l.size());
                        for (int i = 0; i < idx.size(); ++i) { idx(i) = i; }
                        VectorXi n_idx = matrix().cast<int>() * idx;
                        for (int i = 0; i < idx.size(); ++i) {
                            ret[i] = l[n_idx[i]];
                        }
                    }
                    break;
                case 5:
                    {
                        VectorXi idx(l.size());
                        for (int i = 0; i < idx.size(); ++i) { idx(i) = i; }
                        VectorXi n_idx = matrix().cast<int>() * idx;
                        for (int i = 0; i < idx.size(); ++i) {
                            ret[n_idx[i]] = l[i];
                        }
                    }
                    break;
            };
            return ret;
        }

        symmetry_table_type operator * (const symmetry_table_type& other) const
        {
            MSG_DEBUG("COMPOSING SYMMETRIES");
            MSG_DEBUG("" << (*this));
            MSG_DEBUG("AND");
            MSG_DEBUG("" << other);
            MSG_DEBUG("...");
            MatrixXb M1 = matrix();
            MatrixXb M2 = other.matrix();
            MatrixXb new_mat = kroneckerProduct(M1, M2);

            std::map<int, int> new_table;

            for (int j = 0; j < new_mat.cols(); ++j) {
                int i;
                new_mat.col(j).maxCoeff(&i);
                new_table.emplace(j, i);
            }

            letter_permutation_type new_letters;
            if (!letters.combine(other.letters, new_letters)) {
                MSG_DEBUG("foirure.");
            }

            symmetry_table_type ret(new_table, new_letters);
            MSG_DEBUG("RESULT:");
            MSG_DEBUG(ret);
            return ret;
        }
    };


    typedef std::vector<symmetry_table_type> symmetry_list_type;

#endif /* WITH_OVERLUMPING */

inline
MatrixXi classify_values(const MatrixXd& mat, double tol_)
{
    MatrixXi ret(mat.rows(), mat.cols());
    std::set<double> all_values;
    for (int i = 0; i < mat.rows(); ++i) {
        for (int j = 0; j < mat.cols(); ++j) {
            all_values.insert(mat(i, j));
        }
    }
    std::map<double, unsigned long> indices;
    double prev = std::numeric_limits<double>::infinity();
    for (double d: all_values) {
        if (eq_dbl(prev, d)) {
            indices[d] = indices[prev];
        } else {
            unsigned long s = indices.size();
            indices[d] = s;
        }
    }
    const double* dbl_data = mat.data();
    int* i_data = ret.data();
    for (int i = 0; i < mat.size(); ++i) {
        i_data[i] = indices[dbl_data[i]];
    }
    return ret;
    (void) tol_;
}


inline
MatrixXd round_values(const MatrixXd& mat, const MatrixXi& cls)
{
    std::vector<double> accum;
    std::vector<int> weight;
    int m = cls.maxCoeff();
    accum.resize(m + 1);
    weight.resize(m + 1);
    for (int i = 0; i < mat.rows(); ++i) {
        for (int j = 0; j < mat.cols(); ++j) {
            int k = cls(i, j);
            accum[k] += mat(i, j);
            ++weight[k];
        }
    }
    for (size_t i = 0; i < accum.size(); ++i) {
        accum[i] /= weight[i];
    }
    MatrixXd ret(mat.rows(), mat.cols());
    for (int i = 0; i < mat.rows(); ++i) {
        for (int j = 0; j < mat.cols(); ++j) {
            ret(i, j) = accum[cls(i, j)];
        }
    }
    return ret;
}


struct geno_matrix {
    geno_matrix_variant_type variant;
    std::vector<label_type> labels;
    MatrixXd inf_mat;
    MatrixXd p, p_inv, diag;
    VectorXd stat_dist;
    MatrixXd collect, dispatch;
    /*double norm_factor;*/

#ifdef WITH_OVERLUMPING
    symmetry_list_type symmetries;
    symmetry_list_type latent_symmetries;
#endif

#if 0
    geno_matrix() : variant(Nonsense), labels(), inf_mat(), p(), p_inv(), diag(), stat_dist() {}
    geno_matrix(geno_matrix_variant_type v,
                std::vector<label_type>&& l,
                MatrixXd& i, MatrixXd& _p, MatrixXd& pi, MatrixXd& d, VectorXd& sd)
    {
        variant = v;
        labels.swap(l);
        inf_mat.swap(i);
        p.swap(_p);
        p_inv.swap(pi);
        diag.swap(d);
        stat_dist.swap(sd);
    }


    geno_matrix(const geno_matrix& gm) { *this = gm; }
    geno_matrix(geno_matrix&& gm) { *this = std::move(gm); }

    geno_matrix& operator = (geno_matrix&& gm)
    {
        variant = gm.variant;
        labels.swap(gm.labels);
        inf_mat.swap(gm.inf_mat);
        p.swap(gm.p);
        p_inv.swap(gm.p_inv);
        diag.swap(gm.diag);
        stat_dist.swap(gm.stat_dist);
        return *this;
    }
#endif

    size_t rows() const { return inf_mat.rows(); }
    size_t cols() const { return inf_mat.cols(); }
    size_t size() const { return rows(); }

    geno_matrix& operator = (const geno_matrix& gm)
    {
        variant = gm.variant;
        labels.assign(gm.labels.begin(), gm.labels.end());
        inf_mat = gm.inf_mat;
        p = gm.p;
        p_inv = gm.p_inv;
        diag = gm.diag;
        stat_dist = gm.stat_dist;
#ifdef WITH_OVERLUMPING
        symmetries = gm.symmetries;
        latent_symmetries = gm.latent_symmetries;
#endif
        collect = gm.collect;
        dispatch = gm.dispatch;
        return *this;
    }

    MatrixXd exp(double d) const
    {
        MatrixXd ret = p * ((d * diag).array().exp().matrix().asDiagonal()) * p_inv;
        /*MatrixXd check = (d * inf_mat).exp();*/
        /*if (!ret.isApprox(check)) {*/
            /*MSG_ERROR("BAD EXP" << std::endl << "with diag:" << std::endl << check << std::endl << "with exp:" << std::endl << ret, "");*/
        /*} else {*/
            /*MSG_INFO("GOOD EXP");*/
        /*}*/
        return ret;
    }

    void cleanup(MatrixXd& m)
    {
        m = (m.array().abs() <= FLOAT_TOL).select(MatrixXd::Zero(m.rows(), m.cols()), m);
    }

    void cleanup(VectorXd& v)
    {
        v = (v.array().abs() <= FLOAT_TOL).select(VectorXd::Zero(v.size()), v);
    }

    geno_matrix& cleanup()
    {
        cleanup(inf_mat);
        cleanup(diag);
        cleanup(p);
        cleanup(p_inv);
        return *this;
    }

    std::vector<double> unique_eigenvalues() const
    {
        std::set<double> tmp;
        for (int i = 0; i < diag.size(); ++i) {
            tmp.insert(diag(i));
        }
        return {tmp.begin(), tmp.end()};
    }

#if 0
    std::vector<MatrixXd> eigen_components() const
    {
        auto uev = unique_eigenvalues();
        std::vector<MatrixXd> ret;
        ret.reserve(uev.size());
        MSG_DEBUG_INDENT_EXPR("[EIGEN COMPONENTS] ");
        for (double ev: uev) {
            /*VectorXd tmp_diag = (diag.array() == ev).select(VectorXd::Ones(diag.size()), VectorXd::Zero(diag.size())).matrix();*/
            VectorXd tmp_diag = VectorXd::Zero(diag.size());
            for (int i = 0; i < tmp_diag.size(); ++i) {
                if (fabs(diag(i) - ev) < FLOAT_TOL) {
                    tmp_diag(i) = 1.;
                }
            }
            ret.emplace_back(p * tmp_diag.asDiagonal() * p_inv);
            MSG_DEBUG("eigenvalue " << ev << " at " << tmp_diag.transpose());
            MSG_DEBUG(ret.back());
            std::set<double> all_values;
            for (int i = 0; i < ret.back().rows(); ++i) {
                for (int j = 0; j < ret.back().cols(); ++j) {
                    all_values.insert(ret.back()(i, j));
                }
            }
            std::map<double, unsigned long> indices;
            double prev = std::numeric_limits<double>::infinity();
            for (double d: all_values) {
                if (eq_dbl(prev, d)) {
                    indices[d] = indices[prev];
                } else {
                    unsigned long s = indices.size();
                    indices[d] = s;
                }
            }
            MatrixXi grouped(ret.back().rows(), ret.back().cols());
            const double* dbl_data = ret.back().data();
            int* i_data = grouped.data();
            for (int i = 0; i < ret.back().size(); ++i) {
                i_data[i] = indices[dbl_data[i]];
            }
            MSG_DEBUG("--");
            MSG_DEBUG(('a' + grouped.array()).cast<char>());
        }
        MSG_DEBUG_DEDENT;
        return ret;
    }
#else
    std::vector<MatrixXi> eigen_components() const
    {
        auto uev = unique_eigenvalues();
        std::vector<MatrixXi> ret;
        ret.reserve(uev.size());
        /*MSG_DEBUG_INDENT_EXPR("[EIGEN COMPONENTS] ");*/
        for (double ev: uev) {
            /*VectorXd tmp_diag = (diag.array() == ev).select(VectorXd::Ones(diag.size()), VectorXd::Zero(diag.size())).matrix();*/
            VectorXd tmp_diag = VectorXd::Zero(diag.size());
            for (int i = 0; i < tmp_diag.size(); ++i) {
                if (fabs(diag(i) - ev) < FLOAT_TOL) {
                    tmp_diag(i) = 1.;
                }
            }
            /*MSG_DEBUG("eigenvalue " << ev << " at " << tmp_diag.transpose());*/
            MatrixXd tmp = p * tmp_diag.asDiagonal() * p_inv;
            /*MSG_DEBUG(tmp);*/
            std::set<double> all_values;
            for (int i = 0; i < tmp.rows(); ++i) {
                for (int j = 0; j < tmp.cols(); ++j) {
                    all_values.insert(tmp(i, j));
                }
            }
            std::map<double, unsigned long> indices;
            double prev = std::numeric_limits<double>::infinity();
            for (double d: all_values) {
                if (eq_dbl(prev, d)) {
                    indices[d] = indices[prev];
                } else {
                    unsigned long s = indices.size();
                    indices[d] = s;
                }
            }
            ret.emplace_back(tmp.rows(), tmp.cols());
            const double* dbl_data = tmp.data();
            int* i_data = ret.back().data();
            for (int i = 0; i < tmp.size(); ++i) {
                i_data[i] = indices[dbl_data[i]];
            }
            /*MSG_DEBUG("--");*/
            /*MSG_DEBUG(('A' + ret.back().array()).cast<char>());*/
        }
        /*MSG_DEBUG_DEDENT;*/
        return ret;
    }
#endif

    MatrixXd lim_inf() const
    {
        /*MSG_DEBUG("LIM_INF ####### LIM_INF");*/
        /*MSG_DEBUG((*this));*/
        MatrixXd diag_inf = (diag.array() == 0).select(VectorXd::Ones(diag.size()), VectorXd::Zero(diag.size())).matrix();
        return p * diag_inf.asDiagonal() * p_inv;
    }

    bool check_not_nan() const
    {
        if (!(inf_mat == inf_mat)) {
            MSG_DEBUG("NAN INF_MAT");
            return false;
        }
        if (!(p == p)) {
            MSG_DEBUG("NAN P");
            return false;
        }
        if (!(p_inv == p_inv)) {
            MSG_DEBUG("NAN P_INV");
            return false;
        }
        if (!(diag == diag)) {
            MSG_DEBUG("NAN DIAG");
            return false;
        }
        if (!(stat_dist == stat_dist)) {
            MSG_DEBUG("NAN STAT_DIST");
            return false;
        }
        return true;
    }
};


inline
std::ostream& operator << (std::ostream& os, const geno_matrix& gm)
{
    Eigen::Map<Eigen::Array<label_type, Eigen::Dynamic, 1>> labels(const_cast<label_type*>(gm.labels.data()), gm.labels.size(), 1);
    std::set<label_type> uniq_labels(gm.labels.begin(), gm.labels.end());
    for (const auto& l: uniq_labels) {
        MSG_DEBUG(((labels == l).cast<int>().sum()) << ' ' << l << " states");
    }
    Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic> tmp(gm.rows() + 1, gm.cols() + 1);
    for (size_t i = 0; i < gm.labels.size(); ++i) {
        tmp(0, 1 + i) = "  ";
        tmp(0, 1 + i)[0] = gm.labels[i].first;
        tmp(0, 1 + i)[1] = gm.labels[i].second;
        tmp(1 + i, 0) = tmp(0, 1 + i);
    }
    for (size_t i = 0; i < gm.rows(); ++i) {
        for (size_t j = 0; j < gm.rows(); ++j) {
            std::stringstream ss; ss << gm.inf_mat(i, j);
            tmp(i + 1, j + 1) = ss.str();
        }
    }
    os << tmp;
    os << std::endl;
    /*os << "P" << std::endl << gm.p << std::endl;*/
    /*os << "Pinv" << std::endl << gm.p_inv << std::endl;*/
    /*MatrixXd diag = gm.diag.asDiagonal();*/
    os << "D" << std::endl << gm.diag.transpose() << std::endl;
    os << "STATIONARY DISTRIBUTION " << gm.stat_dist.transpose() << std::endl;
#ifdef WITH_OVERLUMPING
    os << "SYMMETRIES" << std::endl;
    for (const auto& S: gm.symmetries) {
        os << S.dump(gm.labels) << std::endl;
    }
    os << "LATENT SYMMETRIES" << std::endl;
    for (const auto& S: gm.latent_symmetries) {
        os << S.dump(gm.labels, true) << std::endl;
    }
#endif
    os << "COLLECT" << std::endl;
    os << braille_grid(gm.collect) << std::endl;
    /*os << "DISPATCH" << std::endl;*/
    /*os << gm.dispatch << std::endl;*/
    return os;
}



#define P_NORM_FACTOR (.707106781186547524400844362104849039284835937688474036588339868995366239231053519425193767163820786367506923115456148512462418027925)


#define SELECT(__p, __b) ((__b) == GAMETE_R ? (__p).second : (__p).first)


#ifdef WITH_OVERLUMPING

#if 0
    inline
    bool combine_sym(const geno_matrix& ret, const geno_matrix& m1, const geno_matrix& m2,
                     std::set<symmetry_table_type>& tmp,
                     const symmetry_table_type& s1, const symmetry_table_type& s2)
    {
        /*MSG_DEBUG_INDENT_EXPR("[combine_sym] ");*/
        /*MSG_DEBUG("NEW SYM FROM");*/
        /*MSG_DEBUG(s1.dump(m1.labels));*/
        /*MSG_DEBUG("AND");*/
        /*MSG_DEBUG(s2.dump(m2.labels));*/
        /*MSG_DEBUG("...");*/
        symmetry_table_type stmp = s1 * s2;
        /*MSG_DEBUG(stmp.dump(ret.labels));*/
        MatrixXd sm = stmp.matrix().cast<double>();
        /*MSG_DEBUG(sm);*/
        bool result = false;
        if (stmp.is_consistent(ret.labels) && (sm.transpose() * ret.inf_mat * sm - ret.inf_mat).isZero(FLOAT_TOL)) {
        /*if ((sm.transpose() * ret.inf_mat * sm - ret.inf_mat).isZero(FLOAT_TOL)) {*/
            /*MSG_DEBUG("OK!" << std::endl << stmp);*/
            /*MSG_DEBUG("OK!");*/
            tmp.emplace(stmp);
            result = true;
        } else {
            /*MSG_DEBUG("NOT GOOD.");*/
            /*MSG_DEBUG(ret.inf_mat);*/
            /*MSG_DEBUG("--");*/
            /*MSG_DEBUG((sm.transpose() * ret.inf_mat * sm - ret.inf_mat));*/

            /*for (const auto& kv: stmp.switches) {*/
            /*MSG_DEBUG("switch " << kv.first << " -> " << kv.second);*/
            /*}*/
            /*for (const auto& kv: stmp.switch_map()) {*/
            /*MSG_DEBUG("switch map " << kv.first << " -> " << kv.second);*/
            /*}*/
        }
        /*MSG_DEBUG_DEDENT;*/
        return result;
    };
#endif /* 0 */

#endif /* WITH_OVERLUMPING */


inline
geno_matrix kronecker(const geno_matrix& m1, const geno_matrix& m2)
{
    m1.check_not_nan();
    m2.check_not_nan();
    geno_matrix ret;
    ret.inf_mat.resize(m1.rows() * m2.rows(), m1.cols() * m2.cols());
    ret.labels.reserve(m1.labels.size() * m2.labels.size());
    switch (m1.variant) {
        case Gamete:
            if (m2.variant == Gamete) {
                for (const auto& l1: m1.labels) {
                    for (const auto& l2: m2.labels) {
                        ret.labels.emplace_back(l1.first, l2.first);
                    }
                }
                ret.variant = SelfingGamete;
                break;
            }
        case SelfingGamete:
        case DoublingGamete:
            MSG_ERROR("Gamete matrices can only be the right operand in a kronecker product", "");
            MSG_QUEUE_FLUSH();
            throw 0;
        case Haplo:
            if (m2.variant != Haplo) {
                MSG_ERROR("Only Haplo (x) Haplo is defined, not with RHS " << m2.variant, "");
                MSG_QUEUE_FLUSH();
                throw 0;
            }
            ret.variant = Geno;
            for (const auto& l1: m1.labels) {
                for (const auto& l2: m2.labels) {
                    ret.labels.emplace_back(l1.first, l2.first);
                }
            }
            break;
        case Geno:
            switch (m2.variant) {
                case Gamete:
                    ret.variant = Haplo;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first), GAMETE_EMPTY);
                        }
                    }
                    break;
                case DoublingGamete:
                    ret.variant = Geno;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first), SELECT(l1, (int)l2.first));
                        }
                    }
                    break;
                case SelfingGamete:
                    ret.variant = Geno;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first), SELECT(l1, (int)l2.second));
                        }
                    }
                    break;
                default:
                    MSG_ERROR("Only Geno (x) {any gamete} is defined", "");
                    MSG_QUEUE_FLUSH();
                    throw 0;
            };
            break;
        case Nonsense:
        case _GMVT_size:
            ;
    };
    ret.inf_mat = kroneckerProduct(m1.inf_mat, MatrixXd::Identity(m2.rows(), m2.cols()))
                + kroneckerProduct(MatrixXd::Identity(m1.rows(), m1.cols()), m2.inf_mat);
    ret.p = kroneckerProduct(m1.p, m2.p);
    ret.p_inv = kroneckerProduct(m1.p_inv, m2.p_inv);
    ret.diag = (kroneckerProduct(m1.diag, VectorXd::Ones(m2.cols()))
             + kroneckerProduct(VectorXd::Ones(m1.cols()), m2.diag));

    ret.collect = MatrixXd::Identity(ret.diag.size(), ret.diag.size());
    ret.dispatch = MatrixXd::Identity(ret.diag.size(), ret.diag.size());
    /*ret.norm_factor = m1.norm_factor * m2.norm_factor;*/

    /* Check! */
    /*MSG_DEBUG("CHECK");*/
    /*MSG_DEBUG((ret.p * ret.diag.asDiagonal() * ret.p_inv * ret.norm_factor) - ret.inf_mat);*/
    /*MSG_DEBUG("END CHECK");*/
    ret.stat_dist = kroneckerProduct(m1.stat_dist, m2.stat_dist);

#ifdef WITH_OVERLUMPING___NOT
    /* TODO: update symmetries */

    std::set<symmetry_table_type> tmp;

    for (const auto& s1: m1.symmetries) {
        for (const auto& s2: m2.symmetries) {
            combine_sym(ret, m1, m2, tmp, s1, s2);
        }
    }

    for (const auto& s1: m1.latent_symmetries) {
        for (const auto& s2: m2.symmetries) {
            combine_sym(ret, m1, m2, tmp, s1, s2);
        }
    }

    for (const auto& s1: m1.symmetries) {
        for (const auto& s2: m2.latent_symmetries) {
            combine_sym(ret, m1, m2, tmp, s1, s2);
        }
    }

    std::set<symmetry_table_type> latent_tmp;
    for (const auto& s1: m1.latent_symmetries) {
        for (const auto& s2: m2.latent_symmetries) {
            if (!combine_sym(ret, m1, m2, tmp, s1, s2)) {
                latent_tmp.emplace(s1 * s2);
            }
        }
    }

    if (ret.variant == Geno) {
        permutation_type loz = generate_lozenge(m1.cols(), m2.cols());
        symmetry_table_type solange(loz);
        if (solange.is_consistent(ret.labels)) {
            /*MSG_DEBUG("HAVE AN ACTUAL SYMMETRY:");*/
            /*MSG_DEBUG(solange.dump(ret.labels));*/
            tmp.emplace(solange);
        } else if (solange.is_consistent(ret.labels, true)) {
            /*MSG_DEBUG("HAVE A LATENT SYMMETRY:");*/
            /*MSG_DEBUG(solange.dump(ret.labels, true));*/
            latent_tmp.emplace(solange);
        } else {
            /*MSG_DEBUG("NOT GOOD:");*/
            /*MSG_DEBUG_INDENT_EXPR("[actual] ");*/
            /*MSG_DEBUG(solange.dump(ret.labels, true));*/
            /*MSG_DEBUG_DEDENT;*/
            /*MSG_DEBUG_INDENT_EXPR("[latent] ");*/
            /*MSG_DEBUG(solange.dump(ret.labels, true));*/
            /*MSG_DEBUG_DEDENT;*/
            /*latent_tmp.emplace(MatrixXb::Identity(m1.cols() * m2.cols(), m1.cols() * m2.cols()));*/
        }
#if 1
#else
        std::vector<bool> iter = {false, true};
        for (auto r0: iter) {
            for (auto r1: iter) {
                MatrixXb loz = generate_lozenge(m1.cols(), m2.cols(), r0, r1);
                symmetry_table_type solange(loz);
                if (solange.is_consistent(ret.labels)) {
                    /*MSG_DEBUG("HAVE AN ACTUAL SYMMETRY:");*/
                    /*MSG_DEBUG(solange.dump(ret.labels));*/
                    tmp.emplace(solange);
                } else if (solange.is_consistent(ret.labels, true)) {
                    /*MSG_DEBUG("HAVE A LATENT SYMMETRY:");*/
                    /*MSG_DEBUG(solange.dump(ret.labels, true));*/
                    latent_tmp.emplace(solange);
                } else {
                    /*MSG_DEBUG("NOT GOOD:");*/
                    /*MSG_DEBUG_INDENT_EXPR("[actual] ");*/
                    /*MSG_DEBUG(solange.dump(ret.labels, true));*/
                    /*MSG_DEBUG_DEDENT;*/
                    /*MSG_DEBUG_INDENT_EXPR("[latent] ");*/
                    /*MSG_DEBUG(solange.dump(ret.labels, true));*/
                    /*MSG_DEBUG_DEDENT;*/
                    /*latent_tmp.emplace(MatrixXb::Identity(m1.cols() * m2.cols(), m1.cols() * m2.cols()));*/
                }
            }
#endif
        }
#if 0
        MatrixXb loz = generate_lozenge(m1.cols(), m2.cols());
        symmetry_table_type solange(loz);
        MSG_DEBUG(solange.dump(ret.labels));
        if (solange.is_consistent(ret.labels)) {
            MSG_DEBUG("ACTUAL SYMMETRY");
            tmp.emplace(solange);
        } else if (solange.is_consistent(ret.labels, true)) {
            MSG_DEBUG("LATENT SYMMETRY");
            latent_tmp.emplace(solange);
        } else {
            MSG_DEBUG("NOT GOOD.");
            /*latent_tmp.emplace(MatrixXb::Identity(m1.cols() * m2.cols(), m1.cols() * m2.cols()));*/
        }

        MatrixXb loz2 = generate_lozenge(m1.cols(), m2.cols(), true);
        symmetry_table_type solange2(loz2);
        MSG_DEBUG(solange2.dump(ret.labels));
        if (solange2.is_consistent(ret.labels)) {
            MSG_DEBUG("ACTUAL SYMMETRY");
            tmp.emplace(solange2);
        } else if (solange2.is_consistent(ret.labels, true)) {
            MSG_DEBUG("LATENT SYMMETRY");
            latent_tmp.emplace(solange2);
        } else {
            MSG_DEBUG("NOT GOOD.");
            /*latent_tmp.emplace(MatrixXb::Identity(m1.cols() * m2.cols(), m1.cols() * m2.cols()));*/
        }
#endif
/*
        symmetry_table_type solange2(loz * MatrixXb::Identity(loz.cols(), loz.cols()));
        if (solange2.is_consistent(ret.labels)) {
            tmp.emplace(solange2);
        } else if (solange2.is_consistent(ret.labels, true)) {
            latent_tmp.emplace(solange2);
        }
 */
    }

    ret.symmetries.assign(tmp.begin(), tmp.end());
    ret.latent_symmetries.assign(latent_tmp.begin(), latent_tmp.end());
#endif

    return ret.cleanup();
}



inline
geno_matrix ancestor_matrix(char a)
{
    std::vector<label_type> l;
    l.emplace_back(a, a);
    return {Geno, std::move(l),
            (MatrixXd(1, 1) << 0).finished(),
            (MatrixXd(1, 1) << 1).finished(),
            (MatrixXd(1, 1) << 1).finished(),
            (MatrixXd(1, 1) << 0).finished()/*, 1.*/,
            (VectorXd(1) << 1.).finished(),
            /*{{{{a, a}}, {{0, 0}}}},*/
            (MatrixXd(1, 1) << 1).finished(),
            (MatrixXd(1, 1) << 1).finished(),
#ifdef WITH_OVERLUMPING
            {{std::map<int, int>{{0, 0}}, letter_permutation_type{{a, a}}}},
            /*{{std::map<int, int>{{0, 0}}, letter_permutation_type{{a, a}}}},*/
            {}
#endif
    };
}






inline
geno_matrix lump_using_partition_weighted(const geno_matrix& m, const std::set<subset>& lumping_partition)
{
    m.check_not_nan();
    /*MSG_DEBUG("|| LUMPING");*/
    /*MSG_DEBUG("" << m);*/
    /*MSG_DEBUG("|| WITH PARTITION");*/
    /*MSG_DEBUG("" << lumping_partition);*/
    /*MSG_QUEUE_FLUSH();*/

    geno_matrix ret_lump;

    /* Creation of L1 */
    MatrixXd L1 = MatrixXd::Zero(lumping_partition.size(), m.cols());
    int i = 0;
    ret_lump.labels.clear();
    ret_lump.labels.reserve(lumping_partition.size());
    for (const auto& D: lumping_partition) {
        ret_lump.labels.push_back(m.labels[D.front()]);
        for (int s: D) {
            L1(i, s) = 1.;
        }
        ++i;
    }
    /*MSG_DEBUG("L1");*/
    /*MSG_DEBUG(L1);*/
    /* Creation of L2 */
    MatrixXd L2 = m.stat_dist.asDiagonal() * L1.transpose();
    L2.array().rowwise() /= L2.array().colwise().sum();

    /* Let's be blunt. */
    ret_lump.inf_mat =
        /* <em>And bold.</em> */
        L1 * m.inf_mat * L2;

    /*MSG_DEBUG("## m.inf_mat" << std::endl << m.inf_mat);*/
    /*MSG_DEBUG("## L1" << std::endl << L1);*/
    /*MSG_DEBUG("## L2" << std::endl << L2);*/
    /*MSG_DEBUG("## ret_lump.inf_mat" << std::endl << ret_lump.inf_mat);*/

    /*MSG_DEBUG(MATRIX_SIZE(L1));*/
    /*MSG_DEBUG(L1);*/
    /*MSG_DEBUG(MATRIX_SIZE(m.stat_dist));*/
    /*MSG_DEBUG(m.stat_dist.transpose());*/
    /*MSG_QUEUE_FLUSH();*/

    ret_lump.stat_dist = L1 * m.stat_dist;

    VectorXd s = ret_lump.stat_dist.array().sqrt().matrix();
    /*MSG_DEBUG("## s = " << s.transpose());*/
    MatrixXd Uinv = s.asDiagonal();
    /*MSG_DEBUG("## Uinv" << std::endl << Uinv);*/
    MatrixXd U = s.array().inverse().matrix().asDiagonal();
    /*MSG_DEBUG("## U" << std::endl << U);*/
    MatrixXd tmp = U * ret_lump.inf_mat * Uinv;
    /*MSG_DEBUG("## tmp" << std::endl << tmp);*/

    if (!(tmp - tmp.transpose()).isZero()) {
        MSG_DEBUG("NOT SYMMETRIC!!!");
        MSG_DEBUG(tmp);
    }

    SelfAdjointEigenSolver<MatrixXd> saes(tmp);

    ret_lump.diag = saes.eigenvalues().real();
    MatrixXd p = saes.eigenvectors().real();
    MatrixXd p_inv = p.transpose();
    ret_lump.cleanup(p);
    ret_lump.cleanup(p_inv);
    /*MSG_DEBUG("================= p");*/
    /*MSG_DEBUG(p);*/
    /*MSG_DEBUG("================= p^-1");*/
    /*MSG_DEBUG(p_inv);*/
    /*MSG_DEBUG("================= diag");*/
    /*MSG_DEBUG(ret_lump.diag);*/
    /*MSG_DEBUG("================= p.p^-1");*/
    /*MSG_DEBUG((p * p_inv));*/

    ret_lump.p = Uinv * p;
    ret_lump.p_inv = p_inv * U;

    ret_lump.variant = m.variant;

    ret_lump.cleanup();

#ifdef WITH_OVERLUMPING
    {
        std::set<symmetry_table_type> tmp_sym;
        MatrixXb shrink = L1.cast<bool>();
        /*MSG_DEBUG("SHRINK . SHRINK:transpose");*/
        /*MSG_DEBUG((shrink * shrink.transpose()));*/
        for (const auto& S: m.symmetries) {
            /*tmp_sym.emplace(ret_lump.labels, shrink * S.matrix() * shrink.transpose());*/
            /*MSG_DEBUG("SHRINKING SYMMETRY");*/
            /*MSG_DEBUG(S.matrix());*/
            /*MSG_DEBUG("WITH");*/
            /*MSG_DEBUG(shrink);*/
            /*auto it_ok = tmp_sym.emplace(shrink * S.matrix() * shrink.transpose(), S.letters);*/
            /*auto it_ok =*/ tmp_sym.emplace(S.table.lump(lumping_partition), S.letters);
            /*MSG_DEBUG("RESULT");*/
            /*MSG_DEBUG(it_ok.first->matrix());*/
        }
        ret_lump.symmetries.assign(tmp_sym.begin(), tmp_sym.end());
    }
    {
        std::set<symmetry_table_type> tmp_sym;
        MatrixXb shrink = L1.cast<bool>();
        for (const auto& S: m.latent_symmetries) {
            /*tmp_sym.emplace(ret_lump.labels, shrink * S.matrix() * shrink.transpose());*/
            /*MSG_DEBUG(MATRIX_SIZE(shrink));*/
            /*MSG_DEBUG(MATRIX_SIZE(S.matrix()));*/
            /*MSG_QUEUE_FLUSH();*/
            MatrixXb ret = latent_lump_m(shrink, S.matrix());

            /*auto it_ok =*/ tmp_sym.emplace(permutation_type(ret), S.letters);
            /*MSG_DEBUG("LUMPING LATENT SYMMETRY");*/
            /*MSG_DEBUG(S.dump(m.labels, true));*/
            /*MSG_DEBUG("INTO");*/
            /*MSG_DEBUG(it_ok.first->dump(ret_lump.labels, true));*/

            /*tmp_sym.emplace(shrink * S.matrix() * shrink.transpose(), S.letters);*/
            /*tmp_sym.emplace(S);*/
        }
        ret_lump.latent_symmetries.assign(tmp_sym.begin(), tmp_sym.end());
    }
#endif

    /*MSG_DEBUG(MATRIX_SIZE(m.collect));*/
    /*MSG_DEBUG(m.collect);*/
    /*MSG_DEBUG(MATRIX_SIZE(L1));*/
    /*MSG_DEBUG(L1);*/
    /*MSG_QUEUE_FLUSH();*/
    ret_lump.collect = L1 * m.collect;
    /*MSG_DEBUG("New collect");*/
    /*MSG_DEBUG(ret_lump.collect);*/
    /*MSG_QUEUE_FLUSH();*/
    /*MSG_DEBUG(MATRIX_SIZE(m.dispatch));*/
    /*MSG_DEBUG(m.dispatch);*/
    /*MSG_DEBUG(MATRIX_SIZE(L2));*/
    /*MSG_DEBUG(L2);*/
    /*MSG_QUEUE_FLUSH();*/
    ret_lump.dispatch = m.dispatch * L2;
    /*MSG_DEBUG("New dispatch");*/
    /*MSG_DEBUG(ret_lump.dispatch);*/
    /*MSG_QUEUE_FLUSH();*/

    return ret_lump;
}




inline
void compute_LR(const geno_matrix& m, const std::set<subset>& lumping_partition, MatrixXd& L1, MatrixXd& L2)
{
    /* Creation of L1 */
    L1 = MatrixXd::Zero(lumping_partition.size(), m.cols());
    int i = 0;
    for (const auto& D: lumping_partition) {
        for (int s: D) {
            L1(i, s) = 1.;
        }
        ++i;
    }
    /* Creation of L2 */
    L2 = m.lim_inf().col(0).asDiagonal() * L1.transpose();
    L2.array().rowwise() /= L2.array().colwise().sum();
}


inline
VectorXd column_angles(const MatrixXd& g1, const MatrixXd& g2)
{
    /*MSG_DEBUG(MATRIX_SIZE(g1));*/
    /*MSG_DEBUG(g1);*/
    /*MSG_DEBUG(MATRIX_SIZE(g2));*/
    /*MSG_DEBUG(g2);*/
    VectorXd n1 = (g1.array() * g1.array()).colwise().sum().array().sqrt().matrix();
    /*MSG_DEBUG(MATRIX_SIZE(n1) << std::endl << n1.transpose());*/
    VectorXd n2 = (g2.array() * g2.array()).colwise().sum().array().sqrt().matrix();
    /*MSG_DEBUG(MATRIX_SIZE(n2) << std::endl << n2.transpose());*/
    VectorXd angles(n1.size());
    for (int i = 0; i < angles.size(); ++i) {
        double n = n1(i) * n2(i);
        if (n != 0.) {
            double dot = (g1.col(i).array() * g2.col(i).array()).sum();
            /*MSG_DEBUG("(" << g1.col(i).transpose() << ") . (" << g2.col(i).transpose() << ") = " << dot << ", n = " << n);*/
            if (dot < -n) {
                dot = -n;
            } else if (dot > n) {
                dot = n;
            }
            angles(i) = acos(dot / n);
        } else {
            angles(i) = 0.;
        }
    }
    /*MSG_DEBUG("ANGLES " << angles.transpose());*/
    return angles;
}



#ifdef WITH_OVERLUMPING
#include <forward_list>

template <typename T>
std::ostream& operator << (std::ostream& os, const std::forward_list<T>& l)
{
    auto i = l.begin();
    auto j = l.end();
    if (i == j) {
        return os;
    }
    os << (**i++);
    for (; i != j; ++i) {
        os << ' ' << (**i);
    }
    return os;
}


    struct experimental_lumper {
        geno_matrix M;

        experimental_lumper(const geno_matrix& m)
            : M(m)
        {
            /*MSG_DEBUG_INDENT_EXPR("[lump] ");*/
            /*MSG_DEBUG("COMPLETING SYMMETRIES FOR GENERATION");*/
            /*MSG_DEBUG("" << M);*/
            /*complete_symmetries();*/
            /*MSG_DEBUG_DEDENT;*/
#if 0
            /*M.inf_mat = round_values(M.inf_mat, classify_values(M.inf_mat, FLOAT_TOL));*/
            M.symmetries = analyse_symmetries(M);
            complete_symmetries();
            for (const auto& tab: M.symmetries) {
                MSG_DEBUG("SYMMETRY " << std::endl << label_switches(M, tab).to_matrix(M));
                MSG_DEBUG("" << tab);
                /*for (const auto& kv: tab) {*/
                    /*MSG_DEBUG(kv.first << " -> " << kv.second);*/
                /*}*/
            }
            if (M.symmetries.size()) {
                MatrixXd accum = MatrixXd::Zero(M.rows(), M.cols());
                for (const auto& tab: M.symmetries) {
                    MatrixXd S = make_symmetry_matrix<double>(tab);
                    accum += S.transpose() * M.inf_mat * S;
                }
                accum.array() /= M.symmetries.size();
                MSG_DEBUG_INDENT_EXPR("[DIFF inf_mat] ");
                MSG_DEBUG((M.inf_mat - accum));
                MSG_DEBUG("--");
                MSG_DEBUG(accum.colwise().sum());
                MSG_DEBUG_DEDENT;
                /*M.inf_mat = accum;*/
            }
#endif
        }

        experimental_lumper(const geno_matrix& m, const symmetry_list_type& /*sym_table*/)
            : M(m)
        {
            /*complete_symmetries();*/
#if 0
            /*M.inf_mat = round_values(M.inf_mat, classify_values(M.inf_mat, FLOAT_TOL));*/
            std::set<symmetry_table_type> temp;
            for (const auto& st: sym_table) {
                temp.insert(st);
            }
            M.symmetries.assign(temp.begin(), temp.end());
            MSG_DEBUG_INDENT_EXPR("[init with sym] ");
            complete_symmetries();
            check_partition_against_symmetries(partition_on_labels());
            for (const auto& tab: M.symmetries) {
                MSG_DEBUG("SYMMETRY " << std::endl << label_switches(M, tab).to_matrix(M));
                MSG_DEBUG("" << tab);
            }
            MSG_DEBUG_DEDENT;
            if (M.symmetries.size()) {
                MatrixXd accum = MatrixXd::Zero(M.rows(), M.cols());
                for (const auto& tab: M.symmetries) {
                    MatrixXd S = make_symmetry_matrix<double>(tab);
                    accum += S.transpose() * M.inf_mat * S;
                }
                accum.array() /= M.symmetries.size();
                MSG_DEBUG_INDENT_EXPR("[DIFF inf_mat] ");
                MSG_DEBUG((M.inf_mat - accum));
                MSG_DEBUG("--");
                MSG_DEBUG(accum.colwise().sum());
                MSG_DEBUG_DEDENT;
                /*M.inf_mat = accum;*/
            }
#endif
        }

        double
            compute_sum(size_t s, const subset& B)
            {
                double accum = 0;
                for (int j: B) {
                    accum += M.inf_mat(j, s);
                }
                return accum;
            }

        std::map<double, subset>
            compute_keys(const subset& C, const subset& B)
            {
                std::map<double, subset> ret;
                for (int i: C) {
                    double sum = compute_sum(i, B);
                    ret[sum].push_back(i);
                }
                return ret;
            }

        std::set<subset>
            partition_on_labels() const
            {
                std::set<subset> ret;
                std::map<label_type, subset> tmp;
                for (size_t i = 0; i < M.labels.size(); ++i) {
                    tmp[M.labels[i]].push_back(i);
                }
                for (const auto& kv: tmp) {
                    ret.insert(kv.second);
                }
                return ret;
            }

        experimental_lumper
            derisavi()
            {
                auto P = partition_on_labels();
                auto adj = make_adjacency_matrix(P);
                std::forward_list<const subset*> stack;
                for (const auto& x: P) { stack.push_front(&x); }
                size_t count = 0;
                /*MSG_DEBUG_INDENT_EXPR("[derisavi] ");*/
                /*MSG_DEBUG("+ + + + + + + + + + + + + + + + + + + + + + + + +");*/
                /*MSG_DEBUG(M);*/
                /*MSG_DEBUG("+ + + + + + + + + + + + + + + + + + + + + + + + +");*/
                while (!stack.empty()) {
                    ++count;
                    const subset& C = *stack.front();
                    stack.pop_front();
                    /*MSG_DEBUG("Studying " << C);*/
                    MSG_QUEUE_FLUSH();
                    if (C.size() <= 1) {
                        continue;
                    }
                    for (auto& B: P) {
                        std::map<double, subset> subsets = compute_keys(C, B);
                        if (subsets.size() > 1) {
                            split_and_apply_symmetries(P, adj, C, subsets, stack);
                            break;
                        }
                        if (0 && subsets.size() > 1) {
                            /*MSG_DEBUG("CONFLICT! " << B << ' ' << subsets);*/
                            /*MSG_QUEUE_FLUSH();*/
                            std::set<subset> sym_C = apply_symmetries(C);
                            /*MSG_DEBUG("REMOVING");*/
                            /*MSG_DEBUG("" << sym_C);*/
                            /*MSG_QUEUE_FLUSH();*/
                            stack.remove_if([&](const subset*& s) { return sym_C.find(*s) != sym_C.end(); });
                            for (const auto& c: sym_C) {
                                P.erase(c);
                            }
                            for (auto& s: subsets) {
                                for (const auto& sym_s: apply_symmetries(s.second)) {
                                    /*MSG_DEBUG("ADDING " << sym_s);*/
                                    /*MSG_QUEUE_FLUSH();*/
                                    auto io = P.insert(sym_s);
                                    if (!io.second) {
                                        /*MSG_DEBUG("PROUT SUBSET ALREADY IN P " << (*io.first));*/
                                    }
                                    if (io.first->size() > 1) {
                                        stack.push_front(&*io.first);
                                    }
                                }
                            }
                            break;
                        }
                    }
                }

                /*MSG_DEBUG("FINAL PARTITION after " << count << " iterations");*/
                /*MSG_DEBUG("final base cost = " << compute_cost(P));*/
                /*MSG_DEBUG("" << P);*/

                check_partition_against_symmetries(P);

                /*MSG_DEBUG_INDENT_EXPR("[result] ");*/
                experimental_lumper ret(lump_using_partition_weighted(M, P));
                /*MSG_DEBUG(ret.M);*/
                /*MSG_DEBUG_DEDENT;*/
                /*MSG_DEBUG_DEDENT;*/

                return ret;
            }

        struct conflict_type {
            subset C;
            subset B;
            std::map<double, subset> keys;
            double cost_;

            conflict_type(const experimental_lumper& /*el*/, const std::set<subset>& /*P0*/,
                          const subset& c, const subset& b, const std::map<double, subset>& k)
                : C(c), B(b), keys(k)
                /*, cost_(opportunity_cost(el, P0))*/
                , cost_(k.rbegin()->first - k.begin()->first)
            {}

            /*conflict_type(const subset& c, const subset& b, const std::map<double, subset>& k)*/
                /*: C(c), B(b), keys(k), cost_(0)*/
            /*{}*/

            friend
                std::ostream& operator << (std::ostream& os, const conflict_type ct)
                {
                    os << "< " << ct.C << " vs " << ct.B;
                    for (const auto& kv: ct.keys) {
                        os << ' ' << kv.first << ':' << kv.second;
                    }
                    return os << " >";
                }

            double
                magnitude() const
                {
                    return keys.rbegin()->first - keys.begin()->first;
                }

            bool
                operator < (const conflict_type& other) const
                {
                    return C.size() > other.C.size() || (C.size() == other.C.size() && cost_ < other.cost_);
                    /*return C.size() > other.C.size() || (C.size() == other.C.size() && cost_ > other.cost_);*/
                    /*return cost_ < other.cost_;*/
                    /*double m1 = magnitude();*/
                    /*double m2 = other.magnitude();*/
                    /*return m1 < m2*/
                        /*|| (m1 == m2*/
                            /*&& (C < other.C*/
                                /*|| (C == other.C && B < other.B)))*/
                        /*;*/
                }

            double
                opportunity_cost0(const experimental_lumper& el, const std::set<subset>& P0) const
                {
                    std::set<subset> P(P0);
                    P.erase(C);
                    for (const auto& kv: keys) { P.insert(kv.second); }
                    double this_cost = el.compute_cost(P);
                    /*double base_cost = el.compute_cost(P0);*/
                    /*return this_cost - base_cost;*/
                    return this_cost;
                }

            double
                opportunity_cost(const experimental_lumper& el, const std::set<subset>& P0) const
                {
                    double accum = 0;
                    size_t total_size = 0;
                    for (const auto& kv: keys) {
                        accum += abs(kv.first) * kv.second.size();
                        total_size += kv.second.size();
                    }
                    double avg = accum / total_size;
                    accum = 0;
                    for (const auto& kv: keys) {
                        accum += abs(avg - abs(kv.first)) / avg;
                    }
                    return accum;
                    (void) el; (void) P0;
                }

            std::multimap<double, subset>
                opportunity_cost_per_part(const experimental_lumper& el, const std::set<subset>& P0) const
                {
#if 0
                    std::multimap<double, subset> ret;
                    /*double base_cost = el.compute_cost(P0);*/
                    subset C1;
                    for (const auto& kv: keys) {
                        std::set<subset> P(P0);
                        subset_difference(C, kv.second, C1);
                        P.erase(C);
                        P.insert(kv.second);
                        P.insert(C1);
                        double this_cost = el.compute_cost(P);
                        /*ret.insert({this_cost - base_cost, kv.second});*/
                        ret.insert({this_cost, kv.second});
                    }
#else
                    std::multimap<double, subset> ret;
                    subset C1;
                    double accum = 0;
                    size_t total_size = 0;
    /*#define OP(_x_) ((_x_) * (_x_))*/
    /*#define OP(_x_) ((_x_) * (_x_))*/
#define OP(_x_) (_x_)
                    for (const auto& kv: keys) {
                        accum += OP(kv.first) * kv.second.size();
                        /*accum += OP(kv.first);*/
                        total_size += kv.second.size();
                    }

                    /*double avg = accum / keys.size();*/
                    double avg = accum / total_size;
                    double div;

                    if (avg == 0.) {
                        div = 1.;
                    } else {
                        div = 1. / avg;
                    }

                    for (const auto& kv: keys) {
                        /*double this_cost = (accum - OP(kv.first) * kv.second.size());*/
                        /*double this_cost = (accum - OP(kv.first));*/
                        double this_cost = abs(avg - OP(kv.first)) * div;
                        ret.insert({this_cost, kv.second});
                    }
#endif
                    return ret;
                    (void) el; (void) P0;
                }
        };

        double
            compute_cost0(const std::set<subset>& P) const 
            {
                MatrixXd PI, PI_inv;
                compute_LR(M, P, PI, PI_inv);
                MatrixXd Ptilde = PI * M.inf_mat * PI_inv;
                VectorXd stat_dist = PI * M.stat_dist;

#if 1
                VectorXd s = stat_dist.array().sqrt().inverse().matrix();
                MatrixXd U = s.array().matrix().transpose();
                MatrixXd Uinv = M.stat_dist.array().sqrt().matrix().asDiagonal();
#else
                auto Pl = partition_on_labels();
                MatrixXd U, discard;
                compute_LR(M, Pl, U, discard);
                U = (PI * U.transpose()).transpose();
                /*MatrixXd Uinv = M.stat_dist.array().sqrt().matrix();*/
                MatrixXd Uinv = M.stat_dist.asDiagonal();
#endif
                /*MatrixXd U = s.array().matrix().asDiagonal();*/

                /*MSG_DEBUG(MATRIX_SIZE(U));*/
                /*MSG_DEBUG(MATRIX_SIZE(Uinv));*/
                /*MSG_DEBUG(MATRIX_SIZE(PI));*/
                /*MSG_DEBUG(MATRIX_SIZE(M.inf_mat));*/
                /*MSG_DEBUG(MATRIX_SIZE(Ptilde));*/
                /*MSG_QUEUE_FLUSH();*/
                /*return (U * (PI * M.inf_mat - Ptilde * PI) * Uinv).lpNorm<Eigen::Infinity>();*/

                MatrixXd mat = (U * (PI * M.inf_mat - Ptilde * PI) * Uinv);
                double accum = 0;
                for (int i = 0; i < mat.cols(); ++i) {
                    accum += mat.col(i).lpNorm<Eigen::Infinity>();
                }
                /*for (int i = 0; i < mat.cols(); ++i) {*/
                    /*double tmp = mat.col(i).array().abs().sum();*/
                    /*if (tmp > accum) {*/
                        /*accum = tmp;*/
                    /*}*/
                /*}*/
                return accum;
                /*return mat.array().abs().colwise().max().sum();*/


                /*return (U * (PI * M.inf_mat - Ptilde * PI) * Uinv)(0, 0);*/
                /*return (PI * M.inf_mat - Ptilde * PI).lpNorm<2>();*/
            }

        double
            compute_cost(const std::set<subset>& P) const 
            {
                MatrixXd PI, PI_inv;
                compute_LR(M, P, PI, PI_inv);
                MatrixXd Gtilde1 = PI * M.inf_mat * PI_inv;
                VectorXd stat_dist = PI * M.stat_dist;

                auto Pl = partition_on_labels();
                MatrixXd U, Uinv;
                compute_LR(M, Pl, U, Uinv);

                U = (PI * U.transpose()).transpose();
                /*MatrixXd Uinv = M.stat_dist.asDiagonal();*/

                MatrixXd G = M.inf_mat;
                MatrixXd Gtilde = Gtilde1;
#if 0
                double accum = 0;
                int n = 1;
                double weight = .15;
                for (int i = 0; i < 10; ++i) {
                    MatrixXd mat = (U * (PI * G - Gtilde * PI) * Uinv);
                    accum += weight * mat.lpNorm<Eigen::Infinity>() / n;
                    n *= (i + 2);
                    G *= M.inf_mat;
                    Gtilde *= Gtilde1;
                    weight *= .15;
                }

                /*MSG_DEBUG(MATRIX_SIZE(U));*/
                /*MSG_DEBUG(MATRIX_SIZE(Uinv));*/
                /*MSG_DEBUG(MATRIX_SIZE(PI));*/
                /*MSG_DEBUG(MATRIX_SIZE(M.inf_mat));*/
                /*MSG_DEBUG(MATRIX_SIZE(Gtilde));*/
                /*MSG_QUEUE_FLUSH();*/
                /*return (U * (PI * M.inf_mat - Gtilde * PI) * Uinv).lpNorm<Eigen::Infinity>();*/

                return accum;
#elif 0
#define DIST_0 .3
                MatrixXd mat = (U * (PI * G - Gtilde * PI) * Uinv);
                double weight = DIST_0 / 2;
                MatrixXd accum = weight * mat;

                for (int n = 1; n < ORDER; ++n) {
                    G *= M.inf_mat;
                    Gtilde *= Gtilde1;
                    weight *= DIST_0 / (n + 1);
                    /*weight *= DIST_0 / (n + 2);*/
                    /*weight *= DIST_0;*/
                    mat = (U * (PI * G - Gtilde * PI) * Uinv);
                    accum += weight * mat;
                }

                /*return accum.lpNorm<Eigen::Infinity>();*/
                return accum.lpNorm<1>();
#else
                /*return (U * (PI * G - Gtilde * PI) * Uinv).lpNorm<1>();*/
                /*MatrixXd g1 = U * PI * G * Uinv;*/
                /*MatrixXd g2 = U * Gtilde * PI * Uinv;*/
                MatrixXd g1 = PI * G;
                MatrixXd g2 = Gtilde * PI;
                return column_angles(g1, g2).lpNorm<2>();
#endif
            }

        std::vector<conflict_type>
            check_conflicts(std::set<subset>& P)
            {
                std::vector<conflict_type> ret;
                for (const auto& C: P) {
                    for (const auto& B: P) {
                        auto keys = compute_keys(C, B);
                        if (keys.size() > 1) {
                            ret.emplace_back(*this, P, C, B, keys);
                        }
                    }
                }
                return ret;
            }

        void
            dump_conflicts(const std::set<subset>& P, const std::multiset<conflict_type>& ordered)
            {
                MSG_DEBUG("===============================================================================================================================");
                MSG_DEBUG("base cost = " << compute_cost(P));
                MSG_DEBUG("conflicts");
                for (const auto& c: ordered) {
                    MSG_DEBUG(c);
                    /*MSG_DEBUG("opportunity_cost = " << c.opportunity_cost(*this, P));*/
                    MSG_DEBUG("opportunity_cost = " << c.cost_);
                    auto ocpp = c.opportunity_cost_per_part(*this, P);
                    for (const auto& kv: ocpp) {
                        MSG_DEBUG("| cost for " << kv.second << " only: " << kv.first);
                    }
                }
            }


        /*void*/
            /*pick_conflict(const std::vector<conflict>& conflicts, const std::map<label_type, size_t>& state_count)*/
            /*{*/
                /* TODO */
            /*}*/

        label_type state_label(const subset& s) { return M.labels[s.front()]; }

        inline
            void _safe_insert(subset& s, int i, std::vector<bool>& visited)
            {
                if (!visited[i]) {
                    s.push_back(i);
                    visited[i] = true;
                }
            }

        inline
            void _safe_commit(subset& s, std::set<subset>& P)
            {
                if (!s.size()) {
                    return;
                }
                std::sort(s.begin(), s.end());
                P.insert(s);
            }

        inline
            std::set<subset>
            apply_symmetries(const subset& B)
            {
                std::set<subset> ret;
                ret.insert(B);
                for (const auto& S: M.symmetries) {
                    subset tmp;
                    for (int i: B) {
                        /*tmp.push_back(S.table.find(i)->second);*/
                        tmp.push_back(S.table[i]);
                    }
                    std::sort(tmp.begin(), tmp.end());
                    ret.insert(tmp);
                }
                return ret;
            }

        MatrixXb make_adjacency_matrix(const std::set<subset>& P0)
        {
            MatrixXb adj = MatrixXb::Zero(M.cols(), M.cols());
            for (const auto& s: P0) {
                for (int i: s) {
                    for (int j: s) {
                        adj(i, j) = 1;
                    }
                }
            }
            return adj;
        }

        void split_and_apply_symmetries(std::set<subset>& P, MatrixXb& adj, const subset& splitted, const std::map<double, subset>& splits, std::forward_list<const subset*>& stack)
        {
            /*MSG_DEBUG("NEW SPLIT IMPLEMENTATION");*/
            /*MSG_DEBUG("stack before " << stack);*/
            /*MSG_DEBUG("SPLITTING " << splits << " FROM PARTITION");*/
            /*MSG_DEBUG("" << P);*/

            /*MSG_DEBUG_INDENT_EXPR("[adj BEFORE] ");*/
            /*MSG_DEBUG(adj);*/
            /*MSG_DEBUG_DEDENT;*/

            std::vector<bool> modified(M.cols(), false);
            std::vector<bool> stacked(M.cols(), false);

            for (const subset* S: stack) {
                stacked[S->front()] = true;
            }

            auto do_split = [&] (const subset& S1, const subset& S2)
            {
                /*MSG_DEBUG("Splitting " << S1 << " and " << S2);*/
                for (int s1: S1) {
                    for (int s2: S2) {
                        /*MSG_DEBUG(s1 << "." << s2);*/
                        /*MSG_QUEUE_FLUSH();*/
                        adj(s2, s1) = adj(s1, s2) = 0;
                    }
                    modified[s1] = true;
                }
            };

            auto make_sym = [&] (const symmetry_table_type& S, const subset& split)
            {
                subset sym_split;
                sym_split.reserve(split.size());
                for (int i: split) {
                    /*sym_split.push_back(S.table.find(i)->second);*/
                    sym_split.push_back(S.table[i]);
                }
                return sym_split;
            };

            for (const auto& S1: splits) {
                for (const auto& S2: splits) {
                    if (S1.first == S2.first) {
                        continue;
                    }
                    do_split(S1.second, S2.second);
                }
            }

            for (const auto& S: M.symmetries) {
                for (const auto& S1: splits) {
                    for (const auto& S2: splits) {
                        if (S1.first == S2.first) {
                            continue;
                        }
                        do_split(make_sym(S, S1.second), make_sym(S, S2.second));
                    }
                }
            }

            (void) splitted;
#if 0
            /*for (int s1: splitted) {*/
                /*for (const auto& split: splits) {*/
                    /*for (int s2: split.second) {*/
                        /*if (s1 != s2) {*/
                            /*adj(s1, s2) = adj(s2, s1) = 0;*/
                            /*modified[s2] = true;*/
                        /*}*/
                    /*}*/
                /*}*/
            /*}*/

            for (const auto& S: M.symmetries) {
                for (const auto& split: splits) {
                    subset sym_split;
                    sym_split.reserve(split.second.size());
                    for (int i: split.second) {
                        sym_split.push_back(S.table.find(i)->second);
                    }
                    if (sym_split == split.second) {
                        continue;
                    }
                    for (const auto& sub: splits) {
                        do_split(sym_split, sub.second);
                    }
                    /*do_split(split.second, sym_split);*/
                    /*for (int s1: splitted) {*/
                        /*int i = S.table.find(s1)->second;*/
                        /*for (int j: sym_split) {*/
                            /*if (i != j) {*/
                                /*adj(i, j) = adj(j, i) = 0;*/
                            /*}*/
                        /*}*/
                    /*}*/
                }
            }
#endif
            /*MSG_DEBUG_INDENT_EXPR("[adj AFTER] ");*/
            /*MSG_DEBUG(adj);*/
            /*MSG_DEBUG_DEDENT;*/
            /*MSG_QUEUE_FLUSH();*/

            std::vector<bool> visited(M.cols(), false);

            P.clear();
            stack.clear();

            for (int j = 0; j < adj.cols(); ++j) {
                subset s;
                bool enstack = false;
                if (visited[j]) {
                    continue;
                }
                for (int i = 0; i < adj.rows(); ++i) {
                    if (adj(i, j)) {
                        s.push_back(i);
                        visited[i] = true;
                        enstack |= modified[i];
                    }
                }
                /*MSG_DEBUG("BUILT PART " << s);*/
                auto it = P.insert(s).first;
                if (enstack || stacked[it->front()]) {
                    stack.push_front(&*it);
                    /*MSG_DEBUG("RESTACK " << (*stack.front()));*/
                }
            }

            /*MSG_DEBUG("stack after " << stack);*/
            check_partition_against_symmetries(P);
        }

        std::set<subset> split_and_apply_symmetries(const std::set<subset>& /*P0*/, MatrixXb& adj, const subset& splitted, const subset& split)
        {
            /*MSG_DEBUG("NEW SPLIT IMPLEMENTATION");*/
            /*MSG_DEBUG("SPLITTING " << split << " FROM PARTITION");*/
            /*MSG_DEBUG("" << P0);*/

            /*MSG_DEBUG_INDENT_EXPR("[adj BEFORE] ");*/
            /*MSG_DEBUG(adj);*/
            /*MSG_DEBUG_DEDENT;*/

            for (int s1: splitted) {
                for (int s2: split) {
                    if (s1 != s2) {
                        adj(s1, s2) = adj(s2, s1) = 0;
                    }
                }
            }

            for (const auto& S: M.symmetries) {
                subset sym_split;
                sym_split.reserve(split.size());
                for (int i: split) {
                    /*sym_split.push_back(S.table.find(i)->second);*/
                    sym_split.push_back(S.table[i]);
                }
                for (int s1: splitted) {
                    /*int i = S.table.find(s1)->second;*/
                    int i = S.table[s1];
                    for (int j: sym_split) {
                        if (i != j) {
                            adj(i, j) = adj(j, i) = 0;
                        }
                    }
                }
            }

            /*MSG_DEBUG_INDENT_EXPR("[adj AFTER] ");*/
            /*MSG_DEBUG(adj);*/
            /*MSG_DEBUG_DEDENT;*/
            /*MSG_QUEUE_FLUSH();*/

            std::vector<bool> visited(M.cols(), false);
            std::set<subset> P;

            for (int j = 0; j < adj.cols(); ++j) {
                subset s;
                if (visited[j]) {
                    continue;
                }
                for (int i = 0; i < adj.rows(); ++i) {
                    if (adj(i, j)) {
                        s.push_back(i);
                        visited[i] = true;
                    }
                }
                P.insert(s);
            }

            check_partition_against_symmetries(P);

            return P;
        }

        subset make_symmetric_subset(const symmetry_table_type& S, const subset& B)
        {
            subset s;
            s.reserve(B.size());
            for (int i: B) {
                /*s.push_back(S.table.find(i)->second);*/
                s.push_back(S.table[i]);
            }
            std::sort(s.begin(), s.end());
            return s;
        }

        void check_partition_against_symmetries(const std::set<subset>& P0)
        {
            for (const auto& S: M.symmetries) {
                std::set<subset> P;
                for (const auto& B: P0) {
                    P.insert(make_symmetric_subset(S, B));
                }
                if (P != P0) {
                    MSG_DEBUG("PARTITION IS NOT SYMMETRIC!");
                    MSG_DEBUG("SYMMETRY:");
                    MSG_DEBUG("" << S.dump(M.labels));
                    MSG_DEBUG("P0:");
                    MSG_DEBUG("" << P0);
                    MSG_DEBUG("P:");
                    MSG_DEBUG("" << P);
                    MSG_QUEUE_FLUSH();
                    /* whine */
                    throw 0;
                }
            }
        }

#if 0
        int rec_complete_symmetries(std::set<symmetry_table_type>& uniq_sym)
        {
            int count = 0;
            for (size_t S1 = 0; S1 < M.symmetries.size(); ++S1) {
            /*for (const auto& S1: M.symmetries) {*/
                /*MatrixXb s1 = make_symmetry_matrix<bool>(S1);*/
                MatrixXb s1 = M.symmetries[S1].matrix();
                /*for (const auto& S2: M.symmetries) {*/
                for (size_t S2 = 0; S2 <= S1; ++S2) {
                    MatrixXb s2 = M.symmetries[S2].matrix();
                    /*symmetry_table_type comp(M.labels, s1 * s2);*/
                    symmetry_table_type comp(s1 * s2);

                    /*symmetry_table_type comp;*/
                    /*for (const auto& kv1: M.symmetries[S1]) {*/
                        /*comp[kv1.first] = M.symmetries[S2].find(kv1.second)->second;*/
                    /*}*/

                    /*symmetry_table_type comp;*/
                    /*for (const auto& kv: S1) {*/
                    /*comp[kv.first] = S2.find(kv.second)->second;*/
                    /*}*/
                    /*auto temp = make_symmetry_matrix<bool>(comp);*/
                    /*MSG_DEBUG(MATRIX_SIZE(temp));*/
                    /*MSG_QUEUE_FLUSH();*/
                    if (uniq_sym.insert(comp).second) {
                        M.symmetries.push_back(comp);
                        /*MSG_DEBUG("NEW SYM");*/
                        /*MSG_DEBUG(comp.dump(M.labels));*/
                        /*MSG_DEBUG(label_switches(M, M.symmetries[S1]) << " x " << label_switches(M, M.symmetries[S2]) << " => " << label_switches(M, M.symmetries.back()));*/
                        /*MSG_DEBUG("" << comp);*/
                        /*MSG_QUEUE_FLUSH();*/
                        ++count;
                        /*rec_complete_symmetries(uniq_sym);*/
                        /*return;*/
                    }
                }
            }
            return count;
        }

        void quotient_set0()
        {
            std::vector<bool> visited(M.cols(), false);
            std::set<subset> P;
            for (int i = 0; i < M.cols(); ++i) {
                if (visited[i]) { continue; }
                std::vector<bool> local_visited(M.cols(), false);
                visited[i] = true;
                local_visited[i] = true;
                subset s;
                s.push_back(i);
                subset stack;
                stack.push_back(i);
                while (stack.size()) {
                    int state = stack.back();
                    stack.pop_back();
                    for (const auto& S: M.symmetries) {
                        int ss = S.table.find(i)->second;
                        if (!local_visited[ss]) {
                            local_visited[ss] = true;
                            stack.push_back(ss);
                            s.push_back(ss);
                        }
                    }
                }
                std::sort(s.begin(), s.end());
                P.insert(s);
            }
            MSG_DEBUG("QUOTIENT SET");
            MSG_DEBUG("" << P);
        }

       void quotient_set()
        {
            MatrixXb accum = MatrixXb::Identity(M.rows(), M.cols());
            while (true) {
                MatrixXb tmp = accum;
                for (const auto& S: M.symmetries) {
                    MatrixXb sm = S.matrix();
                    accum += tmp * sm;
                }
                if (accum == tmp) {
                    break;
                }
            }
            MSG_DEBUG("QUOTIENT SET");
            MSG_DEBUG(('.' + ('@' - '.') * accum.cast<char>().array()));
        }

        void complete_symmetries()
        {
            std::set<symmetry_table_type> uniq_sym(M.symmetries.begin(), M.symmetries.end());
            MSG_DEBUG_INDENT_EXPR("[complete sym] ");
            int count = rec_complete_symmetries(uniq_sym);
            MSG_DEBUG("Found " << count << " new symmetries by composition for a total of " << M.symmetries.size() << " symmetries.");
            MSG_DEBUG_DEDENT;
            quotient_set();
        }
#endif

        geno_matrix
            do_lump(size_t max_size)
            {
                size_t count = 0;
                /*MSG_DEBUG(M);*/
                /*MSG_QUEUE_FLUSH();*/
                auto P = partition_on_labels();
                auto adj = make_adjacency_matrix(P);
                MSG_DEBUG("INITIAL PARTITION SIZE " << P.size());
                check_partition_against_symmetries(P);
                /*auto conflicts = check_conflicts(P);*/
                /*std::multiset<conflict_type> ordered(conflicts.begin(), conflicts.end());*/
                /*dump_conflicts(P, ordered);*/
                /*double max = ordered.rbegin()->opportunity_cost(*this, P);*/
                std::map<subset, std::pair<double, subset>> split_picker;
                while (P.size() < max_size && compute_cost(P) > 0.) {
                    auto conflicts = check_conflicts(P);
                    if (0) {
                        std::multiset<conflict_type> ordered(conflicts.begin(), conflicts.end());
                        dump_conflicts(P, ordered);
                    }

                    if (!conflicts.size()) {
                        break;
                    }
#if 0
                    auto best = std::min_element(conflicts.begin(), conflicts.end());

                    auto ocpp = best->opportunity_cost_per_part(*this, P);
                    /*auto bestppi = ocpp.rbegin();*/
                    auto bestppi = ocpp.begin();

                    P.erase(best->C);
                    subset C1;
                    subset_difference(best->C, bestppi->second, C1);
                    P.insert(bestppi->second);
                    P.insert(C1);
                    ++count;
#else
#if 1
                    double min_cost = std::numeric_limits<double>::infinity();
                    /*const subset* split = NULL;*/
                    subset split;
                    subset splitted;
                    for (const auto& c: conflicts) {
                        auto ocpp = c.opportunity_cost_per_part(*this, P);
                        auto bestppi = ocpp.begin();
                        if (bestppi->first < min_cost) {
                            split = bestppi->second;
                            splitted = c.C;
                            min_cost = bestppi->first;
                        }
                    }

                    if (split.size()) {
                        auto tmp = split_and_apply_symmetries(P, adj, splitted, split);
                        P = tmp;
                        MSG_DEBUG("NEW PARTITION SIZE " << P.size());
                    }
#else
                    split_picker.clear();
                    double min_cost = std::numeric_limits<double>::infinity();
                    for (const auto& c: conflicts) {
                        auto& pick = split_picker[c.C];
                        pick.first = std::numeric_limits<double>::infinity();
                    }
                    for (const auto& c: conflicts) {
                        auto& pick = split_picker[c.C];
                        auto ocpp = c.opportunity_cost_per_part(*this, P);
                        auto bestppi = ocpp.begin();
                        if (bestppi->first < pick.first) {
                            pick.first = bestppi->first;
                            pick.second = bestppi->second;
                            if (min_cost > bestppi->first) {
                                min_cost = bestppi->first;
                            }
                        }
                    }

                    for (const auto& kv: split_picker) {
                        if (P.size() >= max_size) {
                            break;
                        }
                        if (kv.second.first - min_cost < FLOAT_TOL) {
                            P.erase(kv.first);
                            subset C1;
                            subset_difference(kv.first, kv.second.second, C1);
                            P.insert(C1);
                            P.insert(kv.second.second);
                        }
                    }
#endif
                    ++count;
#endif
                }
                MSG_DEBUG("FINAL PARTITION after " << count << " iterations");
                /*MSG_DEBUG("final base cost = " << compute_cost(P));*/
                MSG_DEBUG("" << P);

                check_partition_against_symmetries(P);

                auto ret = lump_using_partition_weighted(M, P);

                return ret;
            }
    };

#define DUMP_GEN(_x) do { MSG_DEBUG_INDENT_EXPR("[" #_x "] "); MSG_DEBUG(_x); MSG_DEBUG_DEDENT; } while (0)

    inline
    geno_matrix lump(const geno_matrix& M, size_t max_states=std::numeric_limits<size_t>::max())
    {
        experimental_lumper el(M);
        return el.derisavi().do_lump(max_states);
    }

#else /* WITH_OVERLUMPING */

    inline
    geno_matrix lump(const geno_matrix& M, size_t max_states=std::numeric_limits<size_t>::max())
    {
        lumper<MatrixXd, label_type> l(M.inf_mat, M.labels);
        return lump_using_partition_weighted(M, l.refine_all());
        (void) max_states;
    }

#endif /* WITH_OVERLUMPING */


extern geno_matrix gamete, selfing_gamete, doubling_gamete;


inline
geno_matrix operator * (const geno_matrix& a, const geno_matrix& b)
{
    return kronecker(a, b);
}


inline
geno_matrix operator | (const geno_matrix& a, const geno_matrix& b)
{
    return lump((a * gamete) * (b * gamete));
}


inline
geno_matrix operator ~ (const geno_matrix& a)
{
    return lump(a * selfing_gamete);
}





#endif

