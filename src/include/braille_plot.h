/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_BRAILLE_PLOT_H_
#define _SPELL_BRAILLE_PLOT_H_

#include <iostream>
#include "eigen.h"


struct palette {
    struct rgb { int r, g, b; };
    typedef int palette_type[256][3];

    static const palette_type& _256()
    {
        static constexpr palette_type _ = {
            /* reformatted from http://www.calmar.ws/vim/256-xterm-24bit-rgb-color-chart.html */
            {0x00, 0x00, 0x00}, {0x80, 0x00, 0x00}, {0x00, 0x80, 0x00}, {0x80, 0x80, 0x00}, {0x00, 0x00, 0x80}, {0x80, 0x00, 0x80}, {0x00, 0x80, 0x80}, {0xc0, 0xc0, 0xc0},
            {0x80, 0x80, 0x80}, {0xff, 0x00, 0x00}, {0x00, 0xff, 0x00}, {0xff, 0xff, 0x00}, {0x00, 0x00, 0xff}, {0xff, 0x00, 0xff}, {0x00, 0xff, 0xff}, {0xff, 0xff, 0xff},
            {0x00, 0x00, 0x00}, {0x00, 0x00, 0x5f}, {0x00, 0x00, 0x87}, {0x00, 0x00, 0xaf}, {0x00, 0x00, 0xd7}, {0x00, 0x00, 0xff},
            {0x00, 0x5f, 0x00}, {0x00, 0x5f, 0x5f}, {0x00, 0x5f, 0x87}, {0x00, 0x5f, 0xaf}, {0x00, 0x5f, 0xd7}, {0x00, 0x5f, 0xff},
            {0x00, 0x87, 0x00}, {0x00, 0x87, 0x5f}, {0x00, 0x87, 0x87}, {0x00, 0x87, 0xaf}, {0x00, 0x87, 0xd7}, {0x00, 0x87, 0xff},
            {0x00, 0xaf, 0x00}, {0x00, 0xaf, 0x5f}, {0x00, 0xaf, 0x87}, {0x00, 0xaf, 0xaf}, {0x00, 0xaf, 0xd7}, {0x00, 0xaf, 0xff},
            {0x00, 0xd7, 0x00}, {0x00, 0xd7, 0x5f}, {0x00, 0xd7, 0x87}, {0x00, 0xd7, 0xaf}, {0x00, 0xd7, 0xd7}, {0x00, 0xd7, 0xff},
            {0x00, 0xff, 0x00}, {0x00, 0xff, 0x5f}, {0x00, 0xff, 0x87}, {0x00, 0xff, 0xaf}, {0x00, 0xff, 0xd7}, {0x00, 0xff, 0xff},
            {0x5f, 0x00, 0x00}, {0x5f, 0x00, 0x5f}, {0x5f, 0x00, 0x87}, {0x5f, 0x00, 0xaf}, {0x5f, 0x00, 0xd7}, {0x5f, 0x00, 0xff},
            {0x5f, 0x5f, 0x00}, {0x5f, 0x5f, 0x5f}, {0x5f, 0x5f, 0x87}, {0x5f, 0x5f, 0xaf}, {0x5f, 0x5f, 0xd7}, {0x5f, 0x5f, 0xff},
            {0x5f, 0x87, 0x00}, {0x5f, 0x87, 0x5f}, {0x5f, 0x87, 0x87}, {0x5f, 0x87, 0xaf}, {0x5f, 0x87, 0xd7}, {0x5f, 0x87, 0xff},
            {0x5f, 0xaf, 0x00}, {0x5f, 0xaf, 0x5f}, {0x5f, 0xaf, 0x87}, {0x5f, 0xaf, 0xaf}, {0x5f, 0xaf, 0xd7}, {0x5f, 0xaf, 0xff},
            {0x5f, 0xd7, 0x00}, {0x5f, 0xd7, 0x5f}, {0x5f, 0xd7, 0x87}, {0x5f, 0xd7, 0xaf}, {0x5f, 0xd7, 0xd7}, {0x5f, 0xd7, 0xff},
            {0x5f, 0xff, 0x00}, {0x5f, 0xff, 0x5f}, {0x5f, 0xff, 0x87}, {0x5f, 0xff, 0xaf}, {0x5f, 0xff, 0xd7}, {0x5f, 0xff, 0xff},
            {0x87, 0x00, 0x00}, {0x87, 0x00, 0x5f}, {0x87, 0x00, 0x87}, {0x87, 0x00, 0xaf}, {0x87, 0x00, 0xd7}, {0x87, 0x00, 0xff},
            {0x87, 0x5f, 0x00}, {0x87, 0x5f, 0x5f}, {0x87, 0x5f, 0x87}, {0x87, 0x5f, 0xaf}, {0x87, 0x5f, 0xd7}, {0x87, 0x5f, 0xff},
            {0x87, 0x87, 0x00}, {0x87, 0x87, 0x5f}, {0x87, 0x87, 0x87}, {0x87, 0x87, 0xaf}, {0x87, 0x87, 0xd7}, {0x87, 0x87, 0xff},
            {0x87, 0xaf, 0x00}, {0x87, 0xaf, 0x5f}, {0x87, 0xaf, 0x87}, {0x87, 0xaf, 0xaf}, {0x87, 0xaf, 0xd7}, {0x87, 0xaf, 0xff},
            {0x87, 0xd7, 0x00}, {0x87, 0xd7, 0x5f}, {0x87, 0xd7, 0x87}, {0x87, 0xd7, 0xaf}, {0x87, 0xd7, 0xd7}, {0x87, 0xd7, 0xff},
            {0x87, 0xff, 0x00}, {0x87, 0xff, 0x5f}, {0x87, 0xff, 0x87}, {0x87, 0xff, 0xaf}, {0x87, 0xff, 0xd7}, {0x87, 0xff, 0xff},
            {0xaf, 0x00, 0x00}, {0xaf, 0x00, 0x5f}, {0xaf, 0x00, 0x87}, {0xaf, 0x00, 0xaf}, {0xaf, 0x00, 0xd7}, {0xaf, 0x00, 0xff},
            {0xaf, 0x5f, 0x00}, {0xaf, 0x5f, 0x5f}, {0xaf, 0x5f, 0x87}, {0xaf, 0x5f, 0xaf}, {0xaf, 0x5f, 0xd7}, {0xaf, 0x5f, 0xff},
            {0xaf, 0x87, 0x00}, {0xaf, 0x87, 0x5f}, {0xaf, 0x87, 0x87}, {0xaf, 0x87, 0xaf}, {0xaf, 0x87, 0xd7}, {0xaf, 0x87, 0xff},
            {0xaf, 0xaf, 0x00}, {0xaf, 0xaf, 0x5f}, {0xaf, 0xaf, 0x87}, {0xaf, 0xaf, 0xaf}, {0xaf, 0xaf, 0xd7}, {0xaf, 0xaf, 0xff},
            {0xaf, 0xd7, 0x00}, {0xaf, 0xd7, 0x5f}, {0xaf, 0xd7, 0x87}, {0xaf, 0xd7, 0xaf}, {0xaf, 0xd7, 0xd7}, {0xaf, 0xd7, 0xff},
            {0xaf, 0xff, 0x00}, {0xaf, 0xff, 0x5f}, {0xaf, 0xff, 0x87}, {0xaf, 0xff, 0xaf}, {0xaf, 0xff, 0xd7}, {0xaf, 0xff, 0xff},
            {0xd7, 0x00, 0x00}, {0xd7, 0x00, 0x5f}, {0xd7, 0x00, 0x87}, {0xd7, 0x00, 0xaf}, {0xd7, 0x00, 0xd7}, {0xd7, 0x00, 0xff},
            {0xd7, 0x5f, 0x00}, {0xd7, 0x5f, 0x5f}, {0xd7, 0x5f, 0x87}, {0xd7, 0x5f, 0xaf}, {0xd7, 0x5f, 0xd7}, {0xd7, 0x5f, 0xff},
            {0xd7, 0x87, 0x00}, {0xd7, 0x87, 0x5f}, {0xd7, 0x87, 0x87}, {0xd7, 0x87, 0xaf}, {0xd7, 0x87, 0xd7}, {0xd7, 0x87, 0xff},
            {0xd7, 0xaf, 0x00}, {0xd7, 0xaf, 0x5f}, {0xd7, 0xaf, 0x87}, {0xd7, 0xaf, 0xaf}, {0xd7, 0xaf, 0xd7}, {0xd7, 0xaf, 0xff},
            {0xd7, 0xd7, 0x00}, {0xd7, 0xd7, 0x5f}, {0xd7, 0xd7, 0x87}, {0xd7, 0xd7, 0xaf}, {0xd7, 0xd7, 0xd7}, {0xd7, 0xd7, 0xff},
            {0xd7, 0xff, 0x00}, {0xd7, 0xff, 0x5f}, {0xd7, 0xff, 0x87}, {0xd7, 0xff, 0xaf}, {0xd7, 0xff, 0xd7}, {0xd7, 0xff, 0xff},
            {0xff, 0x00, 0x00}, {0xff, 0x00, 0x5f}, {0xff, 0x00, 0x87}, {0xff, 0x00, 0xaf}, {0xff, 0x00, 0xd7}, {0xff, 0x00, 0xff},
            {0xff, 0x5f, 0x00}, {0xff, 0x5f, 0x5f}, {0xff, 0x5f, 0x87}, {0xff, 0x5f, 0xaf}, {0xff, 0x5f, 0xd7}, {0xff, 0x5f, 0xff},
            {0xff, 0x87, 0x00}, {0xff, 0x87, 0x5f}, {0xff, 0x87, 0x87}, {0xff, 0x87, 0xaf}, {0xff, 0x87, 0xd7}, {0xff, 0x87, 0xff},
            {0xff, 0xaf, 0x00}, {0xff, 0xaf, 0x5f}, {0xff, 0xaf, 0x87}, {0xff, 0xaf, 0xaf}, {0xff, 0xaf, 0xd7}, {0xff, 0xaf, 0xff},
            {0xff, 0xd7, 0x00}, {0xff, 0xd7, 0x5f}, {0xff, 0xd7, 0x87}, {0xff, 0xd7, 0xaf}, {0xff, 0xd7, 0xd7}, {0xff, 0xd7, 0xff},
            {0xff, 0xff, 0x00}, {0xff, 0xff, 0x5f}, {0xff, 0xff, 0x87}, {0xff, 0xff, 0xaf}, {0xff, 0xff, 0xd7}, {0xff, 0xff, 0xff},
            {0x08, 0x08, 0x08}, {0x12, 0x12, 0x12}, {0x1c, 0x1c, 0x1c}, {0x26, 0x26, 0x26}, {0x30, 0x30, 0x30}, {0x3a, 0x3a, 0x3a},
            {0x44, 0x44, 0x44}, {0x4e, 0x4e, 0x4e}, {0x58, 0x58, 0x58}, {0x60, 0x60, 0x60}, {0x66, 0x66, 0x66}, {0x76, 0x76, 0x76},
            {0x80, 0x80, 0x80}, {0x8a, 0x8a, 0x8a}, {0x94, 0x94, 0x94}, {0x9e, 0x9e, 0x9e}, {0xa8, 0xa8, 0xa8}, {0xb2, 0xb2, 0xb2},
            {0xbc, 0xbc, 0xbc}, {0xc6, 0xc6, 0xc6}, {0xd0, 0xd0, 0xd0}, {0xda, 0xda, 0xda}, {0xe4, 0xe4, 0xe4}, {0xee, 0xee, 0xee}
        };
        return _;
    }

    static
        int closest_index(const palette_type& pal, size_t pal_size, int r, int g, int b)
        {
            int best_idx = -1;
            int best_accum = 0x7FFFFFFF;
            for (size_t i = 0; i < pal_size; ++i) {
                int r_delta = abs(r - pal[i][0]) * 3 * 256 / 10;
                int g_delta = abs(g - pal[i][1]) * 6 * 256 / 10;
                int b_delta = abs(b - pal[i][2]) * 1 * 256 / 10;
                int accum = r_delta + g_delta + b_delta;
                if (accum < best_accum) {
                    best_idx = i;
                    best_accum = accum;
                }
            }
            /*MSG_DEBUG("best matching color for (" << r << ", " << g << ", " << b << ") at #" << best_idx);*/
            return best_idx;
        }

    static
        int closest_index_256(int r, int g, int b) { return closest_index(_256(), 256, r, g, b); }

    struct color256 {
        int idx;
        bool bg;
        color256(int r, int g, int b, bool back=false) : idx(closest_index_256(r, g, b)), bg(back) {}
        color256(int i) : idx(i), bg(false) {}
        friend std::ostream& operator << (std::ostream& os, const color256& col)
        {
            return os << "\x1b[" << (3 + col.bg) << "8;5;" << std::dec << col.idx << 'm';
        }
    };
};




struct braille_grid {
    int m_width;
    int m_height;
    typedef Eigen::Matrix<unsigned long, Eigen::Dynamic, Eigen::Dynamic> plot_data_type;
    plot_data_type m_data;
    plot_data_type m_pixel_count;
    std::vector<std::vector<std::vector<std::vector<double>>>> m_colors;
    int bgR, bgG, bgB;

    braille_grid(int dot_width, int dot_height)
        : m_width(dot_width), m_height(dot_height)
        , m_data(plot_data_type::Constant((dot_height + 3) >> 2, (dot_width + 1) >> 1, 0x0080A0E2))
        , m_pixel_count(plot_data_type::Zero(dot_height, dot_width))
        , bgR(0), bgG(0), bgB(0)
    {
        std::vector<std::vector<std::vector<double>>> col_row(m_data.cols(),
                                                              {{-1, -1, -1}, {-1, -1, -1}, {-1, -1, -1}, {-1, -1, -1},
                                                               {-1, -1, -1}, {-1, -1, -1}, {-1, -1, -1}, {-1, -1, -1}});
        m_colors.resize(m_data.rows(), col_row);
    }

    braille_grid&
        set_background(int r, int g, int b) { bgR = r; bgG = g; bgB = b; return *this; }

    braille_grid
        compose_vert(bool leftright, const braille_grid& other, bool other_leftright)
        {
            int w = 2 * std::max(m_data.cols(), other.m_data.cols());
            int h = 4 * (m_data.rows() + other.m_data.rows());
            int this_x = ((leftright ? w - m_width : 0) & ((int) ~1)) >> 1;
            int this_y = 0;
            int other_x = ((other_leftright ? w - other.m_width : 0) & ((int) ~1)) >> 1;
            int other_y = ((m_height + 3) & ((int) ~3)) >> 2;
            int r, c;
            braille_grid ret(w, h);
            ret.set_background((bgR + other.bgR) >> 1, (bgG + other.bgG) >> 1, (bgB + other.bgB) >> 1);
            for (int y = 0; y < m_data.rows(); ++y) {
                for (int x = 0; x < m_data.cols(); ++x) {
                    r = this_y + y;
                    c = this_x + x;
                    ret.m_colors[r][c] = m_colors[y][x];
                }
            }
            for (int y = 0; y < other.m_data.rows(); ++y) {
                for (int x = 0; x < other.m_data.cols(); ++x) {
                    r = other_y + y;
                    c = other_x + x;
                    ret.m_colors[r][c] = other.m_colors[y][x];
                }
            }
            if (leftright) {
                ret.m_pixel_count.topRightCorner(m_pixel_count.rows(), m_pixel_count.cols()) = m_pixel_count;
                ret.m_data.topRightCorner(m_data.rows(), m_data.cols()) = m_data;
            } else {
                ret.m_pixel_count.topLeftCorner(m_pixel_count.rows(), m_pixel_count.cols()) = m_pixel_count;
                ret.m_data.topLeftCorner(m_data.rows(), m_data.cols()) = m_data;
            }
            if (other_leftright) {
                ret.m_pixel_count.bottomRightCorner(other.m_pixel_count.rows(), other.m_pixel_count.cols()) = other.m_pixel_count;
                ret.m_data.bottomRightCorner(m_data.rows(), m_data.cols()) = m_data;
            } else {
                ret.m_pixel_count.bottomLeftCorner(other.m_pixel_count.rows(), other.m_pixel_count.cols()) = other.m_pixel_count;
                ret.m_data.bottomLeftCorner(other.m_data.rows(), other.m_data.cols()) = other.m_data;
            }
            return ret;
        }

    const std::vector<int>
        get_background() { return {bgR, bgG, bgB}; }

    template <typename Scalar>
    braille_grid(const Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>& data, int r=255, int g=255, int b=255)
        : m_width(data.cols()), m_height(data.rows())
        , m_data(plot_data_type::Constant((m_height + 3) >> 2, (m_width + 1) >> 1, 0x0080A0E2))
        , m_pixel_count(plot_data_type::Zero(data.rows(), data.cols()))
        , bgR(40), bgG(40), bgB(40)
    {
        std::vector<std::vector<std::vector<double>>> col_row(m_data.cols(),
                                                              {{-1, -1, -1}, {-1, -1, -1}, {-1, -1, -1}, {-1, -1, -1},
                                                               {-1, -1, -1}, {-1, -1, -1}, {-1, -1, -1}, {-1, -1, -1}});
        m_colors.resize(m_data.rows(), col_row);
        for (int j = 0; j < data.cols(); ++j) {
            for (int i = 0; i < data.rows(); ++i) {
                if (data(i, j)) {
                    put_pixel(j, i, r, g, b);
                }
            }
        }
    }

    braille_grid&
        put_pixel(int ix, int iy, int R=255, int G=255, int B=255)
        {
            int idx = xy2bitidx(ix, iy);
            unsigned long bit = idx2bit(idx);
            int r = y2data(iy);
            int c = x2data(ix);
            if (r >= 0 && r < m_data.rows() && c >= 0 && c < m_data.cols()) {
                m_data(r, c) |= bit;
                /*std::cout << "put_pixel(" << std::dec << ix << ", " << iy << ") bit=" << std::hex << bit << std::dec << " r=" << r << " c=" << c << " data=" << std::hex << m_data(r, c) << std::endl;*/
                if (m_pixel_count(iy, ix) == 0) {
                    m_colors[r][c][idx] = {(double) R, (double) G, (double) B};
                    m_pixel_count(iy, ix) = 1;
                } else {
                    auto rgb = m_colors[r][c][idx];
                    auto count = m_pixel_count(iy, ix);
                    double inv = 1. / (count + 1);
                    m_colors[r][c][idx] = {inv * (rgb[0] * count + R), inv * (rgb[1] * count + G), inv * (rgb[2] * count + B)};
                    m_pixel_count(iy, ix) = count + 1;
                }
            }
            return *this;
        }

    braille_grid&
        clear_pixel(int ix, int iy)
        {
            int idx = xy2bitidx(ix, iy);
            unsigned long bit = idx2bit(idx);
            int r = y2data(iy);
            int c = x2data(ix);
            if (r >= 0 && r < m_data.rows() && c >= 0 && c < m_data.cols()) {
                m_data(r, c) &= ~bit;
                m_colors[r][c][idx] = {-1, -1, -1};
            }
            return *this;
        }

    bool
        get_pixel(int ix, int iy) const
        {
            int idx = xy2bitidx(ix, iy);
            unsigned long bit = idx2bit(idx);
            int r = y2data(iy);
            int c = x2data(ix);
            if (r >= 0 && r < m_data.rows() && c >= 0 && c < m_data.cols()) {
                return !!(m_data(r, c) & bit);
            }
            return false;
        }

    std::vector<int>
        get_pixel_color(int ix, int iy) const
        {
            static std::vector<int> no_color = {-1, -1, -1};
            int idx = xy2bitidx(ix, iy);
            int r = y2data(iy);
            int c = x2data(ix);
            if (r >= 0 && r < m_data.rows() && c >= 0 && c < m_data.cols()) {
                const auto& v = m_colors[r][c][idx];
                return {(int) v[0], (int) v[1], (int) v[2]};
            }
            return no_color;
        }

    braille_grid&
        add_label(int ix, int iy, const std::string& s, int R=255, int G=255, int B=255)
        {
            int r = y2data(iy + 3);
            int c = x2data(ix + 1);
            auto i = s.begin();
            auto j = s.end();
            int c_orig = c;
            while (i != j) {
                unsigned long c1 = (unsigned char) *i++;
                unsigned long c2 = 0;
                unsigned long c3 = 0;
                unsigned long c4 = 0;
                switch (c1) {
                    case ' ':  ++c; continue;
                    case '\n': ++r; c = c_orig; continue;
                    case '\v': --r; c = c_orig; continue;
                    case '\b': --c; continue;
                    default:
                               if (c1 >= 0xC0) {
                                   c2 = (unsigned char) *i++;
                               }
                               if (c1 >= 0xE0) {
                                   c3 = (unsigned char) *i++;
                               }
                               if (c1 >= 0xF0) {
                                   c4 = (unsigned char) *i++;
                               }
                               for (int i = 0; i < 8; ++i) {
                                   m_colors[r][c][i] = {(double) R, (double) G, (double) B};
                               }
                               m_data(r, c++) = c1 | (c2 << 8) | (c3 << 16) | (c4 << 24);
                };
            }
            return *this;
        }

    braille_grid&
        line(int x0, int y0, int x1, int y1, int dash_length, int space_length, int r=255, int g=255, int b=255)
        {
            int counter = 0;
            return line(x0, y0, x1, y1, dash_length, space_length, r, g, b, counter);
        }


    braille_grid&
        line(int x0, int y0, int x1, int y1, int dash_length, int space_length, int r, int g, int b, int& counter)
        {
            /*int counter = 0;*/
            space_length += dash_length;
            if (abs(x1 - x0) >= abs(y1 - y0)) {
                if (x1 < x0) {
                    std::swap(x0, x1);
                    std::swap(y0, y1);
                }
                int dist_x = x1 - x0;
                int dist_y = abs(y1 - y0);
                if (dist_y == 0) {
                    for (; x0 <= x1; ++x0) {
                        if (counter++ < dash_length) {
                            put_pixel(x0, y0, r, g, b);
                        }
                        counter %= space_length;
                    }
                } else {
                    int dy = (y1 > y0 ? 1 : -1);
                    int accum = dist_x >> 1;
                    for (; x0 <= x1; ++x0) {
                        if (counter++ < dash_length) {
                            put_pixel(x0, y0, r, g, b);
                        }
                        counter %= space_length;
                        accum += dist_y;
                        if (accum > dist_x) {
                            accum -= dist_x;
                            y0 += dy;
                        }
                    }
                }
            } else {
                if (y1 < y0) {
                    std::swap(x0, x1);
                    std::swap(y0, y1);
                }
                int dist_x = abs(x1 - x0);
                int dist_y = y1 - y0;
                if (dist_x == 0) {
                    for (; y0 <= y1; ++y0) {
                        if (counter++ < dash_length) {
                            put_pixel(x0, y0, r, g, b);
                        }
                        counter %= space_length;
                    }
                } else {
                    int dx = (x1 > x0 ? 1 : -1);
                    int accum = dist_y >> 1;
                    for (; y0 <= y1; ++y0) {
                        if (counter++ < dash_length) {
                            put_pixel(x0, y0, r, g, b);
                        }
                        counter %= space_length;
                        accum += dist_x;
                        if (accum > dist_y) {
                            accum -= dist_y;
                            x0 += dx;
                        }
                    }
                }
            }
            counter += space_length - 1;
            counter %= space_length;
            return *this;
        }

    braille_grid&
        box(int x0, int y0, int x1, int y1, int dash_length, int space_length, int r=255, int g=255, int b=255)
        {
            return line(x0, y0, x0, y1, dash_length, space_length, r, g, b)
                  .line(x0, y1, x1, y1, dash_length, space_length, r, g, b)
                  .line(x1, y1, x1, y0, dash_length, space_length, r, g, b)
                  .line(x1, y0, x0, y0, dash_length, space_length, r, g, b);
        }

    braille_grid&
        filled_triangle(int x0, int y0, int x1, int y1, int x2, int y2, int r=255, int g=255, int b=255)
        {
            std::vector<std::pair<int, int>> V = {{x0, y0}, {x1, y1}, {x2, y2}};
            std::sort(V.begin(), V.end(), [] (const std::pair<int, int>& v1, const std::pair<int, int>& v2) { return v1.second < v2.second; });
            std::tie(x0, y0) = V[0];
            std::tie(x1, y1) = V[1];
            std::tie(x2, y2) = V[2];
            /* draw two triangles with a horizontal base using horizontal lines */
            auto X = [] (int x0, int y0, int x1, int y1, int y) { return x0 + (x1 - x0) * (y - y0) / (y1 - y0); };
            int y = y0;
            for (; y <= y1; ++y) {
                line(X(x0, y0, x1, y1, y), y, X(x0, y0, x2, y2, y), y, 1, 0, r, g, b);
            }
            for (; y <= y2; ++y) {
                line(X(x1, y1, x2, y2, y), y, X(x0, y0, x2, y2, y), y, 1, 0, r, g, b);
            }

            return *this;
        }

    braille_grid&
        filled_box(int x0, int y0, int x1, int y1, int r=255, int g=255, int b=255)
        {
            int y_begin = std::min(y0, y1);
            int y_end = std::max(y0, y1);
            for (int y = y_begin; y <= y_end; ++y) {
                line(x0, y, x1, y, 1, 0, r, g, b);
            }
            return *this;
        }

    braille_grid& ellipse(int center_x, int center_y, int radius_x, int radius_y, int dash_length, int space_length, int r=255, int g=255, int b=255)
    {
        typedef std::pair<int, int> point;
        struct point_compare {
            int cx;
            int cy;
            bool operator () (const point& p1, const point& p2) const
            {
                int q1 = (p1.first < cx
                          ? p1.second < cy ? 3 : 0
                          : p1.second < cy ? 2 : 1);
                int q2 = (p2.first < cx
                          ? p2.second < cy ? 3 : 0
                          : p2.second < cy ? 2 : 1);
                if (q1 == q2) {
                    switch (q1) {
                        case 0:
                            return p1.first < p2.first || (p1.first == p2.first && p1.second < p2.second);
                        case 1:
                            return p1.first < p2.first || (p1.first == p2.first && p1.second > p2.second);
                        case 2:
                            return p1.first > p2.first || (p1.first == p2.first && p1.second > p2.second);
                        case 3:
                            return p1.first > p2.first || (p1.first == p2.first && p1.second < p2.second);
                        default:
                            return false;
                    };
                } else {
                    return q1 < q2;
                }
            }
        };
        std::set<point, point_compare> points(point_compare{center_x, center_y});
        /* from https://dai.fmph.uniba.sk/upload/0/01/Ellipse.pdf */
        int x, y, deltax, deltay, ellipse_error, twoAsqr, twoBsqr, stoppingX, stoppingY;
        twoAsqr = 2 * radius_x * radius_x;
        twoBsqr = 2 * radius_y * radius_y;
        x = radius_x;
        y = 0;
        deltax = radius_y * radius_y * (1 - 2 * radius_x);
        deltay = radius_x * radius_x;
        ellipse_error = 0;
        stoppingX = twoBsqr * radius_x;
        stoppingY = 0;
        while (stoppingX >= stoppingY) {
            points.insert({center_x + x, center_y - y});
            points.insert({center_x + x, center_y + y});
            points.insert({center_x - x, center_y - y});
            points.insert({center_x - x, center_y + y});
            ++y;
            stoppingY += twoAsqr;
            ellipse_error += deltay;
            deltay += twoAsqr;
            if ((ellipse_error + ellipse_error + deltax) > 0) {
                --x;
                stoppingX -= twoBsqr;
                ellipse_error += deltax;
                deltax += twoBsqr;
            }
        }
        x = 0;
        y = radius_y;
        ellipse_error = 0;
        deltax = radius_y * radius_y;
        deltay = radius_x * radius_x * (1 - 2 * radius_y);
        stoppingX = 0;
        stoppingY = twoAsqr * radius_y;
        while (stoppingX <= stoppingY) {
            points.insert({center_x + x, center_y - y});
            points.insert({center_x + x, center_y + y});
            points.insert({center_x - x, center_y - y});
            points.insert({center_x - x, center_y + y});
            ++x;
            stoppingX += twoBsqr;
            ellipse_error += deltax;
            deltax += twoBsqr;
            if ((ellipse_error + ellipse_error + deltay) > 0) {
                --y;
                stoppingY -= twoAsqr;
                ellipse_error += deltay;
                deltay += twoAsqr;
            }
        }
        int counter = 0;
        space_length += dash_length;
        for (const point& p: points) {
            if (counter ++ < dash_length) {
                put_pixel(p.first, p.second, r, g, b);
            }
            counter %= space_length;
        }
        return *this;
    }

    friend
        std::ostream& operator << (std::ostream& os, const braille_grid& plot)
        {
            int rows = plot.m_data.rows() - 1;
            int r;
            auto bg_col = palette::color256(plot.bgR, plot.bgG, plot.bgB, true);
            os << bg_col;
            for (r = 0; r < rows; ++r) {
                for (int c = 0; c < plot.m_data.cols(); ++c) {
                    unsigned long tmp = plot.m_data(r, c);
                    os << plot.compute_color(r, c);
                    if (tmp & 0xFF000000) {
                        os.write((char*) &tmp, 4);
                    } else if (tmp & 0x00FF0000) {
                        os.write((char*) &tmp, 3);
                    } else if (tmp & 0x0000FF00) {
                        os.write((char*) &tmp, 2);
                    } else {
                        os.write((char*) &tmp, 1);
                    }
                }
                os << _NORMAL << std::endl << bg_col;
            }
            for (int c = 0; c < plot.m_data.cols(); ++c) {
                unsigned long tmp = plot.m_data(r, c);
                os << plot.compute_color(r, c);
                if (tmp & 0xFF000000) {
                    os.write((char*) &tmp, 4);
                } else if (tmp & 0x00FF0000) {
                    os.write((char*) &tmp, 3);
                } else if (tmp & 0x0000FF00) {
                    os.write((char*) &tmp, 2);
                } else {
                    os.write((char*) &tmp, 1);
                }
            }
            return os << _NORMAL;
        }

private:
    palette::color256 compute_color(int r, int c) const
    {
        int n = 0;
        double R = 0, G = 0, B = 0;
        const auto& block = m_colors[r][c];
#ifdef COMPUTE_COL_BY_MEAN_SQR
        for (int i = 0; i < 8; ++i) {
            if (block[i][0] != -1) {
                R += block[i][0] * block[i][0];
                G += block[i][1] * block[i][1];
                B += block[i][2] * block[i][2];
                ++n;
            }
        }
        if (n) {
            R /= n;
            G /= n;
            B /= n;
            return {(int) sqrt(R), (int) sqrt(G), (int) sqrt(B)};
        }
#else
        for (int i = 0; i < 8; ++i) {
            if (block[i][0] != -1) {
                R += block[i][0];
                G += block[i][1];
                B += block[i][2];
                ++n;
            }
        }
        if (n) {
            R /= n;
            G /= n;
            B /= n;
            return {(int) R, (int) G, (int) B};
        }
#endif
        return {15}; /* default: white */
    }

    int x2data(int x) const { return x >> 1; }
    int y2data(int y) const { return y >> 2; }

    int xy2bitidx(int x, int y) const { return ((y & 0x3) << 1) | (x & 1); }

    unsigned long idx2bit(int idx) const
    {
        static unsigned int bitval[8] = {
            0x010000, 0x080000,
            0x020000, 0x100000,
            0x040000, 0x200000,
            0x000100, 0x000200
        };

        return bitval[idx];
    }
};


struct braille_plot : public braille_grid {
    double m_xmin;
    double m_xmax;
    double m_ymin;
    double m_ymax;
    double m_xrange_inv;
    double m_yrange_inv;
    double m_xres;
    double m_yres;

    braille_plot(int w, int h, double min_x, double max_x, double min_y, double max_y)
        : braille_grid(w << 1, h << 2),
          /*m_width((w << 1)), m_height((h << 2)), m_data(plot_data_type::Constant(h + 1, w + 1, 0x0080A0E2)),*/
          m_xmin(min_x), m_xmax(max_x), m_ymin(min_y), m_ymax(max_y),
          m_xrange_inv(1. / (m_xmax - m_xmin)),
          m_yrange_inv(1. / (m_ymax - m_ymin)),
          m_xres((m_xmax - m_xmin) / (m_width - 1)),
          m_yres((m_ymax - m_ymin) / (m_height - 1))
    {
        /*std::cout << "m_xres=" << m_xres << " m_yres=" << m_yres << std::endl;*/
    }

    braille_plot&
        set_range(double min_x, double max_x, double min_y, double max_y)
        {
            m_xmin = min_x;
            m_xmax = max_x;
            m_ymin = min_y;
            m_ymax = max_y;
            m_xrange_inv = 1. / (m_xmax - m_xmin);
            m_yrange_inv = 1. / (m_ymax - m_ymin);
            m_xres = (m_xmax - m_xmin) / (m_width - 1);
            m_yres = (m_ymax - m_ymin) / (m_height - 1);
            return *this;
        }

    template <typename FUN>
    braille_plot&
        plot(FUN fun, double step, int R=255, int G=255, int B=255)
        {
            for (double x = m_xmin; x < m_xmax; x += step) {
                put_pixel(x, fun(x), R, G, B);
            }
            return *this;
        }

    braille_plot&
        plot(const std::vector<double>& X, const std::vector<double>& Y, int R=255, int G=255, int B=255, int dash_length=1, int space_length=0)
        {
            int counter = 0;
            if (X.size() == Y.size()) {
                if (X.size() == 1) {
                    put_pixel(X[0], Y[0], R, G, B);
                } else {
                    double x0 = X[0];
                    double y0 = Y[0];
                    for (size_t i = 1; i < X.size(); ++i) {
                        /*MSG_DEBUG("plot line " << x0 << ',' << y0 << '-' << X[i] << ',' << Y[i]);*/
                        line(x0, y0, X[i], Y[i], dash_length, space_length, R, G, B, counter);
                        x0 = X[i];
                        y0 = Y[i];
                    }
                }
            }
            return *this;
        }

    braille_plot&
        line(double x0, double y0, double x1, double y1, int dash_length, int space_length, int R=255, int G=255, int B=255)
        {
            braille_grid::line(x2grid(x0), y2grid(y0), x2grid(x1), y2grid(y1), dash_length, space_length, R, G, B);
            return *this;
        }

    braille_plot&
        line(double x0, double y0, double x1, double y1, int dash_length, int space_length, int R, int G, int B, int& counter)
        {
            braille_grid::line(x2grid(x0), y2grid(y0), x2grid(x1), y2grid(y1), dash_length, space_length, R, G, B, counter);
            return *this;
        }

    braille_plot&
        hline(double y, int dash_length, int space_length, int R=255, int G=255, int B=255)
        {
            return line(m_xmin, y, m_xmax, y, dash_length, space_length, R, G, B);
        }

    braille_plot&
        vline(double x, int dash_length, int space_length, int R=255, int G=255, int B=255)
        {
            return line(x, m_ymin, x, m_ymax, dash_length, space_length, R, G, B);
        }

    braille_plot&
        box(double x0, double y0, double x1, double y1, int dash_length, int space_length, int R=255, int G=255, int B=255)
        {
            return line(x0, y0, x0, y1, dash_length, space_length, R, G, B)
                  .line(x0, y1, x1, y1, dash_length, space_length, R, G, B)
                  .line(x1, y1, x1, y0, dash_length, space_length, R, G, B)
                  .line(x1, y0, x0, y0, dash_length, space_length, R, G, B);
        }

    braille_plot&
        filled_box(double x0, double y0, double x1, double y1, int R=255, int G=255, int B=255)
        {
            braille_grid::filled_box(x2grid(x0), y2grid(y0), x2grid(x1), y2grid(y1), R, G, B);
            return *this;
        }

    braille_plot&
        haxis(double y, double tick_origin, double tick_major, double tick_minor, int R=255, int G=255, int B=255)
        {
            std::vector<double> ticks_maj, ticks_min;
            for (double x = tick_origin; x > m_xmin; x -= tick_major) { ticks_maj.push_back(x); }
            for (double x = tick_origin; x < m_xmax; x += tick_major) { ticks_maj.push_back(x); }
            for (double x = tick_origin; x > m_xmin; x -= tick_minor) { ticks_min.push_back(x); }
            for (double x = tick_origin; x < m_xmax; x += tick_minor) { ticks_min.push_back(x); }
            return haxis(y, ticks_maj, ticks_min, R, G, B);
        }

    braille_plot&
        vaxis(double x, double tick_origin, double tick_major, double tick_minor, int R=255, int G=255, int B=255)
        {
            std::vector<double> ticks_maj, ticks_min;
            for (double y = tick_origin; y > m_ymin; y -= tick_major) { ticks_maj.push_back(y); }
            for (double y = tick_origin; y < m_ymax; y += tick_major) { ticks_maj.push_back(y); }
            for (double y = tick_origin; y > m_ymin; y -= tick_minor) { ticks_min.push_back(y); }
            for (double y = tick_origin; y < m_ymax; y += tick_minor) { ticks_min.push_back(y); }
            return vaxis(x, ticks_maj, ticks_min, R, G, B);
        }

    braille_plot&
        vaxis(double x, const std::vector<double>& ticks_major, const std::vector<double>& ticks_minor, int R=255, int G=255, int B=255)
        {
            vline(x, 1, 0, R, G, B);
            for (double y: ticks_minor) {
                put_pixel(x - m_xres, y, R, G, B);
                put_pixel(x + m_xres, y, R, G, B);
            }
            for (double y: ticks_major) {
                put_pixel(x - m_xres - m_xres, y, R, G, B);
                put_pixel(x + m_xres, y, R, G, B);
                put_pixel(x - m_xres, y, R, G, B);
                put_pixel(x + m_xres + m_xres, y, R, G, B);
            }
            return *this;
        }

    braille_plot&
        haxis(double y, const std::vector<double>& ticks_major, const std::vector<double>& ticks_minor, int R=255, int G=255, int B=255)
        {
            hline(y, 1, 0, R, G, B);
            for (double x: ticks_minor) {
                put_pixel(x, y - m_yres, R, G, B);
                put_pixel(x, y + m_yres, R, G, B);
            }
            for (double x: ticks_major) {
                put_pixel(x, y - m_yres - m_yres, R, G, B);
                put_pixel(x, y + m_yres, R, G, B);
                put_pixel(x, y - m_yres, R, G, B);
                put_pixel(x, y + m_yres + m_yres, R, G, B);
            }
            return *this;
        }

    braille_plot& put_pixel(double x, double y, int R=255, int G=255, int B=255)
    {
        braille_grid::put_pixel(x2grid(x), y2grid(y), R, G, B);
        return *this;
    }

    braille_plot& add_label(double x, double y, const std::string& label, int R=255, int G=255, int B=255)
    {
        braille_grid::add_label(x2grid(x), y2grid(y), label, R, G, B);
        return *this;
    }

private:
    int x2grid(double x) const
    {
        double rel_x = (x - m_xmin) * m_xrange_inv;
        return (int) round((m_width - 1) * rel_x);
    }

    int y2grid(double y) const
    {
        double rel_y = 1 - (y - m_ymin) * m_yrange_inv;
        return (int) round((m_height - 1) * rel_y);
    }
};


template <typename KeyType, typename KeyComp=std::less<KeyType>>
struct braille_histogram : public braille_plot {
    std::map<KeyType, int> m_data_points;
    int m_n_bins;
    double m_bin_width;

    braille_histogram(int char_width, int char_height, double bin_width=.5)
        : braille_plot(char_width, char_height, -1, 1, -1, 1)
        , m_data_points()
        , m_bin_width(bin_width)
    {}

    braille_histogram
        add_point(const KeyType& x)
        {
            ++m_data_points[x];
            return *this;
        }

    std::vector<int>
        make_bins(int& height, std::vector<KeyType>& bin_labels)
        {
            bin_labels.clear();
            /*MSG_DEBUG("data points " << m_data_points);*/
            height = 0;
            std::vector<int> ret;
            for (const auto& kv: m_data_points) {
                ret.push_back(kv.second);
                bin_labels.push_back(kv.first);
                if (height < ret.back()) {
                    height = ret.back();
                }
            }
            /*MSG_DEBUG("bins " << ret);*/
            /*MSG_DEBUG("labels " << bin_labels);*/
            return ret;
        }

    braille_histogram&
        render(int R=255, int G=255, int B=255)
        {
            int height = 0;
            std::vector<double> labels;
            std::vector<int> bins = make_bins(height, labels);
            double w = m_bin_width * .5;
            double x0 = -w;
            double x1 = bins.size() - w;
            double y0 = 0;
            double y1 = height;
            double pad_x = .1;
            double pad_y = height * .1;
            set_range(x0 - pad_x, x1 + pad_x, y0 - pad_y, y1 + pad_y);
            for (size_t b = 0; b < bins.size(); ++b) {
                filled_box(b - w, 0, b + w, bins[b], R, G, B);
            }
            haxis(-5 * m_yres, 0, bins.size(), 1);
            vaxis(-w - 5 * m_xres, 0, 10, 1);
            /*for (size_t x = 0; x < labels.size(); ++x) {*/
                /*std::string lab = SPELL_STRING("\n" << labels[x]);*/
                /*add_label(x, -3 * m_yres, lab);*/
            /*}*/
            return *this;
        }
};


#if 0
template <>
struct braille_histogram<double> : public braille_plot {
    std::vector<double> m_data_points;
    int m_n_bins;
    double m_min, m_max;
    double m_bin_width;
    bool m_linear;

    braille_histogram(int char_width, int char_height, int n_bins, double bin_width)
        : braille_plot(char_width, char_height, -1, 1, -1, 1)
        , m_data_points()
        , m_n_bins(n_bins)
        , m_min(std::numeric_limits<double>::infinity())
        , m_max(-std::numeric_limits<double>::infinity())
        , m_bin_width(bin_width)
        , m_linear(!!n_bins)
    {}

    braille_histogram
        add_point(double x)
        {
            m_data_points.push_back(x);
            if (x > m_max) { m_max = x; }
            if (x < m_min) { m_min = x; }
            return *this;
        }

    std::vector<int>
        make_bins(int& height, std::vector<double>& bin_labels)
        {
            bin_labels.clear();
            MSG_DEBUG("data points " << m_data_points);
            if (m_linear) {
                std::vector<int> bins(m_n_bins, 0);
                if (m_max == m_min) { m_max += 1; }
                for (double x: m_data_points) {
                    int bin = (int) (((x - m_min) / (m_max - m_min)) * m_n_bins);
                    MSG_DEBUG(x << " => " << bin);
                    if (bin == m_n_bins) { --bin; }
                    ++bins[bin];
                    if (bins[bin] > height) {
                        height = bins[bin];
                    }
                }
                if (bins.size() > 1) {
                    for (size_t b = 0; b < bins.size(); ++b) {
                        bin_labels.push_back(m_min + b * (m_max - m_min) / (m_n_bins - 1));
                    }
                } else {
                    bin_labels.push_back(m_min);
                }
                MSG_DEBUG("bins " << bins);
                MSG_DEBUG("labels " << bin_labels);
                return bins;
            } else {
                std::map<double, int> tmp_bins;
                for (double d: m_data_points) {
                    ++tmp_bins[d];
                }
                std::vector<int> bins(tmp_bins.size());
                size_t b = 0;
                for (const auto& kv: tmp_bins) {
                    bins[b] = kv.second;
                    ++b;
                    bin_labels.push_back(kv.first);
                }
                return bins;
            }
        }

    braille_histogram&
        render(int R=255, int G=255, int B=255)
        {
            int height = 0;
            std::vector<double> labels;
            std::vector<int> bins = make_bins(height, labels);
            double w = m_bin_width * .5;
            double x0 = -w;
            double x1 = bins.size() - w;
            double y0 = 0;
            double y1 = height;
            double pad_x = .1;
            double pad_y = height * .1;
            set_range(x0 - pad_x, x1 + pad_x, y0 - pad_y, y1 + pad_y);
            for (size_t b = 0; b < bins.size(); ++b) {
                filled_box(b - w, 0, b + w, bins[b], R, G, B);
            }
            haxis(-5 * m_yres, 0, bins.size(), 1);
            vaxis(-w - 5 * m_xres, 0, 10, 1);
            /*for (size_t x = 0; x < labels.size(); ++x) {*/
                /*std::string lab = SPELL_STRING("\n" << labels[x]);*/
                /*add_label(x, -3 * m_yres, lab);*/
            /*}*/
            return *this;
        }
};

#endif



#endif

