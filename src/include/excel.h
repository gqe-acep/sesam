/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPELL_EXCEL_STREAM_H
#define SPELL_EXCEL_STREAM_H

#include <xlnt/xlnt.hpp>
#include <iostream>
#include <functional>
#include <vector>

namespace excel {
    typedef std::vector<std::pair<std::string, xlnt::font>> rich_text;

    struct stream {        
        xlnt::workbook* bookptr;
        xlnt::worksheet sheet;
        xlnt::cell_reference origin;
        xlnt::cell_reference cursor;
        std::vector<xlnt::cell_reference> stack;

        stream() : bookptr(NULL), sheet(), origin(), cursor(), stack() {}
        
        stream(xlnt::workbook* ptr) : bookptr(ptr), sheet(ptr ? ptr->sheet_by_index(0) : xlnt::worksheet()), origin("A1"), cursor("A1"), stack() {}

        void reset(xlnt::workbook* ptr=NULL)
        {
            bookptr = ptr;
            stack.clear();
            origin = cursor = xlnt::cell_reference("A1");
            sheet = xlnt::worksheet();
        }
        
        bool is_open() const
        {
            return !!bookptr;
        }
        
//         void anchor(int r, int c)
//         {
//         }
        
        xlnt::cell current_cell()
        {
            return sheet[cursor];
        }
    };
    
    typedef std::function<void(stream&)> manipulator;
    
    xlnt::range_reference
    find_cell_range(xlnt::worksheet sheet, xlnt::cell_reference pos);
    
    manipulator
    new_worksheet(std::string name);
    
    manipulator
    select_worksheet(std::string name);
    
    manipulator
    rename_worksheet(std::string name);
    
    manipulator
    move(std::string ref);
    
    manipulator
    move(int row, int col);

    manipulator
    moveby(int row_delta, int col_delta);
    
    manipulator
    move(xlnt::cell_reference ref);
    
    extern
    manipulator next_col, next_row, prev_col, prev_row, set_origin, push, pop, to_origin, to_topright, to_bottomleft;
    
    manipulator
    merge(int width, int height);
    
    manipulator
    apply_style(std::string name);
    
    manipulator
    create_style(std::string name, std::function<void(xlnt::style&)> init);

    manipulator
    cell_format(std::function<void(xlnt::format&)> init);
    
    inline
    void
    apply_border_once(stream& s, xlnt::cell_reference pos, xlnt::border_side side, xlnt::color color, xlnt::border_style style)
    {
        xlnt::border border;
        if (s.sheet[pos].has_format()) {
            border = s.sheet[pos].border();
        }
        border.side(side, xlnt::border::border_property().color(color).style(style));
        s.sheet[pos].border(border);
    }
    
//     inline
//     void
//     apply_border_once(stream& s, xlnt::cell_reference pos, xlnt::border_side side, xlnt::color color, xlnt::border_style style)
//     {
//         
//         switch (side) {
//             case xlnt::border_side::start:
//                 break;
//             case xlnt::border_side::top:
//                 break;
//             case xlnt::border_side::end:
//                 break;
//             case xlnt::border_side::bottom:
//                 break;
//             default:;
//         };
//     }
    
    inline
    void
    apply_border(stream& s, xlnt::border_side side, xlnt::color color, xlnt::border_style style)
    {
        auto box = find_cell_range(s.sheet, s.cursor);
        int c1 = 0, r1 = 0;
        xlnt::cell_reference origin;
        switch (side) {
            case xlnt::border_side::start:
                origin = box.top_left();
                c1 = 0;
                r1 = box.height();
                break;
            case xlnt::border_side::top:
                origin = box.top_left();
                c1 = box.width();
                r1 = 0;
                break;
            case xlnt::border_side::end:
                origin = box.top_right();
                c1 = 0;
                r1 = box.height();
                break;
            case xlnt::border_side::bottom:
                origin = box.bottom_left();
                c1 = box.width();
                r1 = 0;
                break;
            default:;
        };
        for (int c = 0; c <= c1; ++c) {
            for (int r = 0; r <= r1; ++r) {
                xlnt::cell_reference pos = s.cursor.make_offset(c, r);
                apply_border_once(s, pos, side, color, style);
#if 0
                switch (side) {
                    case xlnt::border_side::start:
                        if (pos.column().index > 1) {
                            apply_border_once(s, pos.make_offset(-1, 0), xlnt::border_side::end, color, style);
                        }
                        break;
                    case xlnt::border_side::top:
                        if (pos.row() > 1) {
                            apply_border_once(s, pos.make_offset(0, -1), xlnt::border_side::bottom, color, style);
                        }
                        break;
                    case xlnt::border_side::end:
                        apply_border_once(s, pos.make_offset(1, 0), xlnt::border_side::start, color, style);
                        break;
                    case xlnt::border_side::bottom:
                        apply_border_once(s, pos.make_offset(0, 1), xlnt::border_side::top, color, style);
                        break;
                    default:;
                };
#endif
            }
        }
    }
    
    
    manipulator
    border_bottom(xlnt::color c, xlnt::border_style sty);
    
    manipulator
    border_right(xlnt::color c, xlnt::border_style sty);
    
    manipulator
    border_top(xlnt::color c, xlnt::border_style sty);
    
    manipulator
    border_left(xlnt::color c, xlnt::border_style sty);
    
    manipulator
    border_box(int x1, int y1, int x2, int y2, const std::vector<std::pair<xlnt::color, xlnt::border_style>>& style);
    
    manipulator
    border_box(xlnt::cell_reference c0, xlnt::cell_reference c1, const std::vector<std::pair<xlnt::color, xlnt::border_style>>& style);

    manipulator
    style(const std::string& name, std::function<void(xlnt::style&)> init);

    manipulator
    style(const std::string& name);
    
    manipulator
    align(std::function<void(xlnt::alignment&)> init);
    
    manipulator
    formula(const std::string& fmla);
}


inline
excel::stream&
operator << (excel::stream& s, excel::manipulator f) { f(s); return s; }

inline
std::string strip(std::string v)
{
    auto i = v.begin(), j = v.end(), k = j;
    if (i != j) {
        --k;
        while (k != i && (*k == '\n' || *k == '\r' || *k == ' ')) { --k; }
        ++k;
        while (i != k && (*i == '\n' || *i == '\r' || *i == ' ')) { ++i; }
//         MSG_DEBUG("strip(" << v << ") = " << (std::string{i, k}));
        return {i, k};
    }
//     MSG_DEBUG("strip(" << v << ") = " << v);
    return v;
}

inline excel::stream& operator << (excel::stream& s, int v) { s.sheet[s.cursor].value(v); return s; }
inline excel::stream& operator << (excel::stream& s, const std::string& v) { s.sheet[s.cursor].value(strip(v)); return s; }
// inline excel::stream& operator << (excel::stream& s, std::string v) { s.sheet[s.cursor].value(v); return s; }
inline excel::stream& operator << (excel::stream& s, double v) { s.sheet[s.cursor].value(v); return s; }
inline excel::stream& operator << (excel::stream& s, xlnt::date v) { s.sheet[s.cursor].value(v); return s; }
inline excel::stream& operator << (excel::stream& s, xlnt::datetime v) { s.sheet[s.cursor].value(v); return s; }

inline
std::vector<excel::manipulator>
operator << (excel::manipulator f1, excel::manipulator f2) { return {f1, f2}; }

inline
std::vector<excel::manipulator>&&
operator << (std::vector<excel::manipulator>&& vf, excel::manipulator f) { vf.push_back(f); return std::move(vf); }

inline
excel::stream&
operator << (excel::stream& s, const std::vector<excel::manipulator>& vf) { for (const auto& f: vf) { f(s); } return s; }

#endif
