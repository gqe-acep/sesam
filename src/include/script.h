/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_SCRIPT_H_
#define _SPELL_SCRIPT_H_

#include "script_parser.h"
#include <map>
#include "../attic/frontends2.h"
#include <exception>

namespace script {

enum Type { type_none, type_filename, type_bool, type_model, type_model_manager, type_locus, type_chromosome, type_result, type_ct };

static inline Type str_to_type(const std::string& s)
{
    static const std::map<std::string, Type> _ = {
        {"filename", type_filename},
        {"bool", type_bool},
        {"model", type_model_manager},
        {"locus", type_locus},
        {"chromosome", type_chromosome},
        {"result", type_result}
    };
    auto it = _.find(s);
    return it == _.end() ? type_none : it->second;
}

static inline const char* type_to_str(Type t)
{
    static const std::map<Type, const char*> _ = {
        {type_none, "none"},
        {type_filename, "filename"},
        {type_bool, "bool"},
        {type_model_manager, "model"},
        {type_locus, "locus"},
        {type_chromosome, "chromosome"},
        {type_result, "result"}
    };
    auto it = _.find(t);
    return it == _.end() ? "<invalid>" : it->second;
}


inline std::ostream& operator << (std::ostream& os, Type t) { return os << type_to_str(t); }


struct DuplicateName : public std::runtime_error {
    DuplicateName(const std::string& name)
        : std::runtime_error(SPELL_STRING("Name \"" << name << "\" is already defined."))
    {}
};

struct Environment;

struct UndefinedVariable : public std::runtime_error {
    UndefinedVariable(const Environment& env, const std::string& name);
};

static inline ComputationType str_to_test(const std::string& s)
{
    std::map<std::string, ComputationType> _ = {
        {"FTest", ComputationType::FTest},
        {"FTestLOD", ComputationType::FTestLOD},
        {"R2", ComputationType::R2},
        {"Chi2", ComputationType::Chi2},
    };
    auto it = _.find(s);
    if (it == _.end()) {
        MSG_ERROR(s << " is not a valid test name", "");
        return ComputationType::NoTest;
    }
    return it->second;
}

static inline std::string test_to_str(ComputationType ct)
{
    std::map<ComputationType, std::string> _ = {
        {ComputationType::FTest, "FTest"},
        {ComputationType::FTestLOD, "FTestLOD"},
        {ComputationType::R2, "R2"},
        {ComputationType::Chi2, "Chi2"},
    };
    auto it = _.find(ct);
    if (it == _.end()) {
        MSG_ERROR(ct << " is not a valid test value", "");
        return "<invalid>";
    }
    return it->second;
}

struct Variable;

struct WrongType : public std::runtime_error {
    WrongType(const Variable& v, Type t);
};


struct Variable {
private:
    Type type;
public:
    union data_container {
        double loc;
        const chromosome* chrom;
        bool b;
        model M;
        std::string s;
        model_manager* mm;
        model_manager::test_result res;
        ComputationType ct;
        data_container() {}
        ~data_container() {}
    } data;

    Variable()
        : type(type_none), data()
    {}

    Variable(const Variable& v)
        : type(type_none)
    { *this = v; }

    Type get_type() const { return type; }

    operator model& () { if (type != type_model) { throw WrongType(*this, type_model); } return data.M; }
    operator model_manager& () { if (type != type_model_manager) { throw WrongType(*this, type_model_manager); } return *data.mm; }
    operator const chromosome*& () { if (type != type_chromosome) { throw WrongType(*this, type_chromosome); } return data.chrom; }
    operator double& () { if (type != type_locus) { throw WrongType(*this, type_locus); } return data.loc; }
    operator model_manager::test_result& () { if (type != type_result) { throw WrongType(*this, type_result); } return data.res; }
    operator bool () { if (type != type_bool) { throw WrongType(*this, type_bool); } return data.b; }
    operator ComputationType () { if (type != type_ct) { throw WrongType(*this, type_ct); } return data.ct; }
    operator const std::string& () { if (type != type_filename) { throw WrongType(*this, type_filename); } return data.s; }

    Variable& operator = (const model& m) { reset(m); return *this; }
    Variable& operator = (model_manager* m) { reset(type_model_manager); data.mm = m; return *this; }
    Variable& operator = (const chromosome* c) { reset(type_chromosome); data.chrom = c; return *this; }
    Variable& operator = (const model_manager::test_result& r) { reset(r); return *this; }
    Variable& operator = (double l) { reset(type_locus); data.loc = l; return *this; }
    Variable& operator = (bool b) { reset(type_bool); data.b = b; return *this; }
    Variable& operator = (ComputationType ct) { reset(type_ct); data.ct = ct; return *this; }
    Variable& operator = (const std::string& s) { reset(s); return *this; }

    void reset(Type new_type)
    {
        if (type == new_type) { return; }
        switch (type) {
            case type_filename: data.s.~basic_string(); break;
            case type_model: data.M.~model(); break;
            case type_result: data.res.~test_result(); break;
            default:;
        };
        type = new_type;
        switch (type) {
            case type_filename: new(&data.s) std::string(); break;
            case type_model: new(&data.M) model(); break;
            case type_result: new (&data.res) model_manager::test_result(); break;
            default:;
        };
    }

    void reset(const model& m)
    {
        reset(type_model);
        new (&data.M) model(m);
    }

    void reset(const model_manager::test_result& r)
    {
        reset(type_result);
        new (&data.res) model_manager::test_result(r);
    }

    void reset(const std::string& s)
    {
        reset(type_filename);
        new (&data.s) std::string(s);
    }

    Variable& operator = (const Variable& v)
    {
#define CASE_ASSIGN(__type, __field) case type_##__type: data.__field = v.data.__field; break;
        reset(v.type);
        switch (type) {
            CASE_ASSIGN(locus, loc);
            CASE_ASSIGN(filename, s);
            CASE_ASSIGN(chromosome, chrom);
            CASE_ASSIGN(bool, b);
            CASE_ASSIGN(model, M);
            CASE_ASSIGN(model_manager, mm);
            CASE_ASSIGN(result, res);
            CASE_ASSIGN(ct, ct);
            default:;
        };
#undef CASE_ASSIGN
        return *this;
    }

#if 0
    Variable& operator = (const std::string& s)
    {
        std::stringstream ss(s);
        switch (type) {
            case type_bool:
                ss >> data.b;
                break;
            case type_locus:
                ss >> data.loc;
                break;
            case type_chromosome:
                data.chrom = active_settings->find_chromosome(s);
                break;
            default:
                throw 0;
        };
        return *this;
    }
#endif

    friend std::ostream& operator << (std::ostream& os, const Variable& v)
    {
        switch (v.type) {
            case type_locus: os << "(locus) " << v.data.loc; break;
            case type_filename: os << "(filename) " << v.data.s; break;
            case type_chromosome: os << "(chromosome) " << v.data.chrom; break;
            case type_bool: os << "(bool) " << (v.data.b ? "true" : "false"); break;
            case type_model: os << "(model) " << v.data.M; break;
            case type_model_manager: os << "(model_manager) " << v.data.mm->Mcurrent.keys(); break;
            case type_result: os << "(result) " << v.data.res; break;
            case type_ct: os << "(ComputationType) " << test_to_str(v.data.ct); break;
            case type_none: os << "<none>"; break;
            default:;
        };
        return os;
    }
};


inline WrongType::WrongType(const Variable& v, Type t)
    : std::runtime_error(SPELL_STRING("Wrong type: have " << v.get_type() << " but expected " << t))
{}

struct Environment {
    std::map<std::string, Variable> vars;
    std::shared_ptr<Environment> parent;
    Environment(std::shared_ptr<Environment> par) : parent(par) {}
    Variable& create(const std::string& type, const std::string& name) { return create(str_to_type(type), name); }
    Variable& create(Type t, const std::string& name)
    {
        if (has(name)) {
            throw DuplicateName(name);
        }
        Variable& v = vars[name];
        v.reset(t);
        return v;
    }
    Variable& resolve(const std::string& name)
    {
        auto it = vars.find(name);
        if (it != vars.end()) {
            return it->second;
        } else if (parent != NULL) {
            return parent->resolve(name);
        }
        throw UndefinedVariable(*this, name);
    }
    bool has(const std::string& name)
    {
        auto it = vars.find(name);
        return it != vars.end() || (parent && parent->has(name));
    }

    friend std::ostream& operator << (std::ostream& os, const Environment& env)
    {
        if ((&env) == NULL) {
            return os << "NULL ENVIRONMENT";
        }
        os << "Variables defined:" << std::endl;
        for (const auto& kv: env.vars) {
            os << "  - " << kv.first << " = " << kv.second << std::endl;
        }
        if (env.parent != NULL) {
            os << *env.parent;
        }
        return os;
    }
};


inline UndefinedVariable::UndefinedVariable(const Environment& env, const std::string& name)
    : std::runtime_error(SPELL_STRING("Name \"" << name << "\" is not defined." << std::endl << env))
{}

/* EXPRESSIONS */

struct Expression {
    virtual Variable eval(Environment&) = 0;
};

struct LocusAdd : public Expression {
    std::shared_ptr<Expression> left;
    std::shared_ptr<Expression> right;
    LocusAdd(std::shared_ptr<Expression> l, std::shared_ptr<Expression> r)
        : left(l), right(r)
    {}
    Variable eval(Environment& env)
    {
        double l = left->eval(env);
        double r = right->eval(env);
        Variable v;
        v = l + r;
        return v;
    }
};

struct LocusSub : public Expression {
    std::shared_ptr<Expression> left;
    std::shared_ptr<Expression> right;
    LocusSub(std::shared_ptr<Expression> l, std::shared_ptr<Expression> r)
        : left(l), right(r)
    {}
    Variable eval(Environment& env)
    {
        double l = left->eval(env);
        double r = right->eval(env);
        Variable v;
        v = l - r;
        return v;
    }
};

struct MaxResult : public Expression {
    ComputationType test_name;
    MaxResult(ComputationType ct) : test_name(ct) {}

    Variable eval(Environment& env)
    {
        model_manager& mm = env.resolve("");
        ComputationType last_comp;
        last_comp = env.resolve("Last computation");
        if (last_comp != test_name) {
            /* whine */
            throw 0;
        }
        return env.resolve("Last computation max");
    }
};

struct Locus : public Expression {
    double d;
    Locus(double x) : d(x) {}
    Variable eval(Environment&)
    {
        Variable v;
        v = d;
        return v;
    }
};

struct SingleVar : public Expression {
    std::string var_name;
    SingleVar(const std::string& s) : var_name(s) {}
    Variable eval(Environment& e)
    {
        return e.resolve(var_name);
    }
};

struct Above : public Expression {
    std::shared_ptr<Expression> left;
    std::shared_ptr<Expression> right;
    Above(std::shared_ptr<Expression> l, std::shared_ptr<Expression> r)
        : left(l), right(r)
    {}
    Variable eval(Environment& env)
    {
        Variable vl = left->eval(env);
        Variable vr = right->eval(env);
        Variable v;
        double l, r;
        if (vl.get_type() == type_result) {
            l = vl.data.res.test_value;
        } else {
            l = vl;
        }
        if (vr.get_type() == type_result) {
            r = vr.data.res.test_value;
        } else {
            r = vr;
        }
        v = (l > r);
        /*MSG_DEBUG("Above? l=" << l << " r=" << r << " (l > r)=" << (l > r) << " v.type=" << v.get_type() << " (bool)v=" << ((bool) v));*/
        return v;
    }
};

/* SOURCES */

struct Source {
    virtual void start_iter(Environment&) = 0;
    virtual bool iter(Environment&, const std::string& v) = 0;
    virtual Type type() const = 0;
};

struct Chromosome : public Source {
    std::vector<chromosome>::iterator i, j;
    void start_iter(Environment&)
    {
        i = std::begin(active_settings->map);
        j = std::end(active_settings->map);
    }
    bool iter(Environment& env, const std::string& v) {
        if (i == j) {
            return false;
        }
        env.resolve(v) = &*i++;
        return true;
    }
    Type type() const { return type_chromosome; }
};

struct SelectedLocus : public Source {
    locus_key_struc::iterator i, j;
    void start_iter(Environment& env)
    {
        model_manager& mm = env.resolve("");
        locus_key lk = mm.get_selection(mm.chrom_under_study);
        i = std::begin(lk);
        j = std::end(lk);
    }
    bool iter(Environment& env, const std::string& v) {
        if (i == j) {
            return false;
        }
        env.resolve(v) = *i;
        ++i;
        return true;
    }
    Type type() const { return type_locus; }
};

/* STATEMENTS */

struct Statement {
    virtual void exec(Environment&) = 0;
};

struct Block : public Environment, public Statement {
    std::vector<std::shared_ptr<Statement>> statements;
    Block() : Environment(NULL), Statement() {}
    Block(std::shared_ptr<Environment> parent)
        : Environment(parent), Statement()
    {}

    void exec(Environment&)
    {
        for (auto& s: statements) {
            s->exec(*this);
        }
    }

    void exec()
    {
        exec(*this);
    }
};

struct For : public Block {
    std::string var_name;
    std::shared_ptr<Source> source;

    For(std::shared_ptr<Environment> parent, std::shared_ptr<Source> s, const std::string& var)
        : Block(parent), var_name(var), source(s)
    { create(s->type(), var_name); }

    void exec(Environment&)
    {
        source->start_iter(*this);
        while (source->iter(*this, var_name)) {
            Block::exec(*this);
        }
    }
};

struct If : public Statement {
    std::shared_ptr<Expression> cond;
    std::shared_ptr<Statement> then;
    If(std::shared_ptr<Expression> c, std::shared_ptr<Statement> t)
        : cond(c), then(t)
    {}
    void exec(Environment& env)
    {
        if ((bool) cond->eval(env)) {
            then->exec(env);
        }
    }
};

struct Compute : public Statement {
    ComputationType ct;
    std::string chr;
    std::shared_ptr<Expression> start;
    std::shared_ptr<Expression> end;
    Compute(ComputationType c, const std::string& target, std::shared_ptr<Expression> expr_start, std::shared_ptr<Expression> expr_end)
        : ct(c), chr(target), start(expr_start), end(expr_end)
    {}
    void exec(Environment& env)
    {
        model_manager& mm = env.resolve("");
        const chromosome* c = env.resolve(chr);
        mm.select_chromosome(c);
        double locus_start = start ? (double) start->eval(env) : 0.;
        double locus_end = end ? (double) end->eval(env) : mm.max_testpos();
        mm.test_type = ct;
        env.resolve("Last computation") = ct;
        /*MSG_DEBUG("Computing " << ct << " (FTest is " << ComputationType::FTest << ") on " << c->name << " between " << locus_start << " and " << locus_end);*/
        env.resolve("Last computation max") = mm.test_along_chromosome(locus_start, locus_end);
        /*MSG_DEBUG("Max is " << ((model_manager::test_result)env.resolve("Last computation max")));*/
    }
};

struct SelectionAdd : public Statement {
    std::shared_ptr<Expression> expr;
    SelectionAdd(std::shared_ptr<Expression> e) : expr(e) {}
    void exec(Environment& env)
    {
        model_manager& mm = env.resolve("");
        Variable v = expr->eval(env);
        if (v.get_type() == type_locus) {
            mm.add(mm.chrom_under_study, (double) v);
        } else if (v.get_type() == type_result) {
            mm.add((model_manager::test_result)v);
        }
    }
};

struct SelectionSub : public Statement {
    std::shared_ptr<Expression> expr;
    SelectionSub(std::shared_ptr<Expression> e) : expr(e) {}
    void exec(Environment& env)
    {
        model_manager& mm = env.resolve("");
        double locus = expr->eval(env);
        mm.remove(locus);
    }
};

struct Assign : public Statement {
    std::string name;
    std::shared_ptr<Expression> expr;
    Assign(const std::string& n, std::shared_ptr<Expression> e) : name(n), expr(e) {}
    void exec(Environment& env)
    {
        env.resolve(name) = expr->eval(env);
    }
};

/* OUTPUT */

struct Printable {
    virtual void print(msg_channel, Environment&) const = 0;
    virtual std::vector<std::vector<double>> columns(Environment&) const = 0;
    virtual std::vector<std::string> colnames(Environment&) const = 0;
};


struct PrintString : public Printable {
    std::string s;
    PrintString(const std::string& _) : s(_) {}
    virtual void print(msg_channel channel, Environment&) const
    {
        CREATE_MESSAGE(channel, SPELL_STRING(s));
    }

    virtual std::vector<std::vector<double>> columns(Environment&) const
    {
        return {};
    }

    virtual std::vector<std::string> colnames(Environment&) const
    {
        return {};
    }
};


static inline std::vector<std::vector<double>> matrix_to_columns(const MatrixXd& m)
{
    std::vector<std::vector<double>> ret;
    ret.resize(m.outerSize());
    for (int j = 0; j < m.outerSize(); ++j) {
        ret[j].resize(m.innerSize());
        for (int i = 0; i < m.innerSize(); ++i) {
            ret[j][i] = m(i, j);
        }
    }
    return ret;
}


struct PrintLoci : public Printable {
    void print(msg_channel channel, Environment& env) const
    {
        model_manager& mm = env.resolve("");
        CREATE_MESSAGE(channel, SPELL_STRING(mm.loci));
    }

    virtual std::vector<std::vector<double>> columns(Environment& env) const
    {
        model_manager& mm = env.resolve("");
        std::vector<std::vector<double>> ret;
        ret.push_back(*mm.loci);
        return ret;
    }

    virtual std::vector<std::string> colnames(Environment&) const
    {
        std::vector<std::string> ret;
        ret.push_back("Loci");
        return ret;
    }
};


struct PrintScores : public Printable {
    void print(msg_channel channel, Environment& env) const
    {
        model_manager& mm = env.resolve("");
        CREATE_MESSAGE(channel, SPELL_STRING(mm.last_computation->transpose()));
    }

    virtual std::vector<std::vector<double>> columns(Environment& env) const
    {
        model_manager& mm = env.resolve("");
        return matrix_to_columns(mm.last_computation->transpose());
    }

    virtual std::vector<std::string> colnames(Environment& env) const
    {
        std::vector<std::string> ret;
        model_manager& mm = env.resolve("");
        ret.resize(mm.last_computation->innerSize(), test_to_str(env.resolve("Last computation")));
        return ret;
    }
};


struct PrintX : public Printable {
    void print(msg_channel channel, Environment& env) const
    {
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        CREATE_MESSAGE(channel, SPELL_STRING(mm.Mcurrent));
    }

    virtual std::vector<std::vector<double>> columns(Environment& env) const
    {
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        return matrix_to_columns(mm.Mcurrent.X());
    }

    virtual std::vector<std::string> colnames(Environment& env) const
    {
        std::vector<std::string> ret;
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        for (const auto& kv: mm.Mcurrent.m_blocks) {
            for (const auto& cl: kv.second->column_labels) {
                std::stringstream label;
                std::string tmp(cl.begin(), cl.end());
                label << kv.first << ':' << tmp;
                ret.push_back(label.str());
            }
        }
        return ret;
    }
};


struct PrintXtX : public Printable {
    void print(msg_channel channel, Environment& env) const
    {
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        model::xtx_printable _(mm.Mcurrent, false);
        CREATE_MESSAGE(channel, SPELL_STRING(_));
    }

    std::vector<std::vector<double>> columns(Environment& env) const
    {
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        return matrix_to_columns(mm.Mcurrent.XtX());
    }

    std::vector<std::string> colnames(Environment& env) const
    {
        std::vector<std::string> ret;
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        for (const auto& kv: mm.Mcurrent.m_blocks) {
            for (const auto& cl: kv.second->column_labels) {
                std::stringstream label;
                std::string tmp(cl.begin(), cl.end());
                label << kv.first << ':' << tmp;
                ret.push_back(label.str());
            }
        }
        return ret;
    }
};


struct PrintCoefficients : public Printable {
    struct constraints_guard {
        model_manager& mm;
        bool ctr;
        constraints_guard(model_manager& _)
            : mm(_)
        {
            ctr = mm.Mcurrent.m_with_constraints;
            mm.Mcurrent.enable_constraints();
            mm.Mcurrent.compute();
        }
        ~constraints_guard()
        {
            mm.Mcurrent.m_with_constraints = ctr;
            mm.Mcurrent.compute();
        }
    };

    void print(msg_channel channel, Environment& env) const
    {
        model_manager& mm = env.resolve("");
        constraints_guard _(mm);
        CREATE_MESSAGE(channel, SPELL_STRING(mm.Mcurrent.coefficients().transpose()));
    }

    std::vector<std::vector<double>> columns(Environment& env) const
    {
        model_manager& mm = env.resolve("");
        constraints_guard _(mm);
        return matrix_to_columns(mm.Mcurrent.coefficients());
    }

    std::vector<std::string> colnames(Environment& env) const
    {
        std::vector<std::string> ret;
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        for (const auto& kv: mm.Mcurrent.m_blocks) {
            for (const auto& cl: kv.second->column_labels) {
                std::stringstream label;
                std::string tmp(cl.begin(), cl.end());
                label << "Coef:" << kv.first << ':' << tmp;
                ret.push_back(label.str());
            }
        }
        return ret;
    }
};


struct PrintResiduals : public Printable {
    void print(msg_channel channel, Environment& env) const
    {
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        CREATE_MESSAGE(channel, SPELL_STRING(mm.Mcurrent.residuals()));
    }

    std::vector<std::vector<double>> columns(Environment& env) const
    {
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        return matrix_to_columns(mm.Mcurrent.residuals());
    }

    std::vector<std::string> colnames(Environment& env) const
    {
        std::vector<std::string> ret;
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        for (const auto& kv: mm.Mcurrent.m_blocks) {
            for (const auto& cl: kv.second->column_labels) {
                std::stringstream label;
                std::string tmp(cl.begin(), cl.end());
                label << "Res:" << kv.first << ':' << tmp;
                ret.push_back(label.str());
            }
        }
        return ret;
    }
};


struct PrintRank : public Printable {
    void print(msg_channel channel, Environment& env) const
    {
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        CREATE_MESSAGE(channel, SPELL_STRING(mm.Mcurrent.rank()));
    }

    std::vector<std::vector<double>> columns(Environment& env) const
    {
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        std::vector<std::vector<double>> ret;
        ret.emplace_back();
        ret.back().push_back(mm.Mcurrent.rank());
        return ret;
    }

    std::vector<std::string> colnames(Environment&) const
    {
        std::vector<std::string> ret;
        ret.emplace_back("Rank");
        return ret;
    }
};


struct PrintXtX_inv : public Printable {
    void print(msg_channel channel, Environment& env) const
    {
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        model::xtx_printable _(mm.Mcurrent, true);
        CREATE_MESSAGE(channel, SPELL_STRING(_));
    }

    virtual std::vector<std::vector<double>> columns(Environment& env) const
    {
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        return matrix_to_columns(mm.Mcurrent.XtX_pseudo_inverse());
    }

    virtual std::vector<std::string> colnames(Environment& env) const
    {
        std::vector<std::string> ret;
        model_manager& mm = env.resolve("");
        mm.Mcurrent.compute();
        for (const auto& kv: mm.Mcurrent.m_blocks) {
            for (const auto& cl: kv.second->column_labels) {
                std::stringstream label;
                std::string tmp(cl.begin(), cl.end());
                label << kv.first << ':' << tmp;
                ret.push_back(label.str());
            }
        }
        return ret;
    }
};


struct PrintKeys : public Printable {
    void print(msg_channel channel, Environment& env) const
    {
        model_manager& mm = env.resolve("");
        CREATE_MESSAGE(channel, SPELL_STRING(mm.Mcurrent.keys()));
    }

    virtual std::vector<std::vector<double>> columns(Environment&) const
    {
        return {};
    }

    virtual std::vector<std::string> colnames(Environment&) const
    {
        return {};
    }

};

struct Output : public Statement {
    std::vector<std::shared_ptr<Printable>> to_print;
};


struct Print : public Output {
    void exec(Environment& env)
    {
        for (const auto& p: to_print) { p->print(msg_channel::Out, env); }
    }
};


struct TableOutput : public Output {
    std::string filename_var;
    bool append;
    void exec(Environment& env)
    {
        MSG_DEBUG("filename_var " << filename_var << " " << env.resolve(filename_var));
        MSG_DEBUG("append " << append);
        MSG_DEBUG("to_print.size() " << to_print.size());
        model_manager& mm = env.resolve("");
        std::vector<std::vector<double>> columns;
        std::vector<std::string> column_names;
        for (const auto& p: to_print) {
            auto c = p->columns(env);
            auto cn = p->colnames(env);
            columns.insert(columns.end(), c.begin(), c.end());
            column_names.insert(column_names.end(), cn.begin(), cn.end());
        }
        MSG_DEBUG("filename=" << ((const std::string&) env.resolve(filename_var)));
        ofile f((const std::string&) env.resolve(filename_var),
                        append
                            ? std::ios_base::out | std::ios_base::ate | std::ios_base::app
                            : std::ios_base::out | std::ios_base::trunc); // text mode OK
        MSG_DEBUG("columns.size() " << columns.size());
        MSG_DEBUG("column_names.size() " << column_names.size());
        if (!columns.size()) {
            MSG_DEBUG("Nothing to write!");
            return;
        }
        if (!append) {
            auto cni = column_names.begin();
            auto cnj = column_names.end();
            f << (*cni);
            for (++cni; cni != cnj; ++cni) {
                f << "\t" << (*cni);
            }
            f << std::endl;
        }
        auto ci = columns.begin();
        auto cj = columns.end();
        for (size_t i = 0; i < columns.front().size(); ++i) {
            auto c = ci;
            f << (*c)[i];
            for (++c; c != cj; ++c) {
                if (i > c->size()) {
                    f << "\tNA";
                } else {
                    f << "\t" << (*c)[i];
                }
            }
            f << std::endl;
        }
    }
};

} /* namespace script */

#endif

