/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_FUNC_WRAP_H_
#define _SPEL_FUNC_WRAP_H_

extern "C" {
#ifdef _WIN32
#include "dlfcn_win.h"
#else
#include <dlfcn.h>
#endif
#ifdef __APPLE__
#include<stdlib.h>
#else
#include<malloc.h>
#endif
#include <string.h>
}

#include <cxxabi.h>

#include <functional>

/*
char*
__cxa_demangle(const char* __mangled_name, char* __output_buffer, size_t* __length, int* __status);
*/

/*using demangle = abi::__cxa_demangle;*/

// static inline
// std::unordered_map<void*, std::string>&
// demangled_names_registry()
// {
// 	static std::unordered_map<void*, std::string> _;
// 	return _;
// }



template <typename Ret, typename... Args>
std::string&
get_func_name(Ret (*f) (Args...))
{
	static std::unordered_map<void*, std::string> demangled_names_registry;
    static std::mutex m;
    std::unique_lock<std::mutex> l(m);
	union {
		Ret (*fptr) (Args...);
		void* vptr;
	} tmp = {f};
	std::string& ret = demangled_names_registry[tmp.vptr];
	if (!ret.size()) {
		Dl_info info;
		dladdr(tmp.vptr, &info);
		int status = 0;
		char* buf = abi::__cxa_demangle(info.dli_sname, NULL, 0, &status);
        if (buf) {
            char* paren = strchr(buf, '(');
            if (!paren) {
                paren = buf + strlen(buf);
            }
            ret.assign(buf, paren);
            free(buf);
        } else {
            ret.assign("<no name>");
        }
		/*std::cout << tmp.vptr << " => " << info.dli_sname << std::endl;*/
	}
	return ret;
}


template <typename Ret, typename... Args>
std::string&
get_func_name(Ret (&f) (Args...))
{
	return get_func_name(&f);
}


template <typename F> struct function_wrapper;


template <typename Ret, typename... Args>
struct function_wrapper<Ret(Args...)> {
    const std::string& name;
    std::function<Ret(Args...)> func;

    function_wrapper(Ret (*f) (Args...))
        : name(get_func_name(f)), func(f)
    {}

    function_wrapper(Ret (&f) (Args...))
        : name(get_func_name(f)), func(f)
    {}

    function_wrapper(const function_wrapper<Ret(Args...)>& fw)
        : name(fw.name), func(fw.func)
    {}

    function_wrapper(const function_wrapper<Ret(Args...)>&& fw)
        : name(fw.name), func(std::forward<decltype(func)>(fw.func))
    {}

    template <typename F>
    function_wrapper(const std::string& n, const F&& fw)
        : name(n), func(std::forward<decltype(func)>(fw))
    {}

    Ret operator () (Args... x)
    {
        return func(x...);
    }

    Ret operator () (Args&&... x)
    {
        return func(std::forward<Args>(x)...);
    }
};

#endif

