/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_BAYES_DISPATCH_H_
#define _SPELL_BAYES_DISPATCH_H_

#include "bayes/cli.h"

typedef std::function<bool(bn_settings_t*, size_t)> job_type;
typedef std::function<size_t(bn_settings_t*)> count_jobs_type;

extern std::map<std::string, std::pair<count_jobs_type, job_type>> job_registry;

bool do_the_job(bn_settings_t* settings, std::string job);

#endif
