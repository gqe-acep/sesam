/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPELL_QTL_INET_H
#define SPELL_QTL_INET_H

#ifndef _WIN32 //// this file is not used on MS windows system execpt in cygwin (posix) context

extern "C" {
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
};

#define MAXMSG  512
#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX  255
#endif

struct inet_server {
    uint16_t port;
    int sock;
    struct sockaddr_in name;
    std::string hostname;
    std::thread thread;
    std::atomic<int> running;
    std::condition_variable is_running_cv;
    std::mutex is_running_mutex;

    int
    make_socket (uint16_t port)
    {
        int _sock;

        /* Create the socket. */
        _sock = socket (PF_INET, SOCK_STREAM, 0);
        if (_sock < 0)
        {
            MSG_ERROR("Couldn't create socket: " << strerror(errno), "");
            return -1;
        }

        /* Give the socket a name. */
        name.sin_family = AF_INET;
        name.sin_port = htons (port);
        name.sin_addr.s_addr = htonl (INADDR_ANY);
        if (bind (_sock, (struct sockaddr *) &name, sizeof (name)) < 0)
        {
            return -1;
        }

        return _sock;
    }

    std::string
    address() const
    {
        return SPELL_STRING(hostname << ':' << port);
    }

    inet_server(uint16_t start_port, uint16_t end_port)
        : thread(), running(false), is_running_cv(), is_running_mutex()
    {
        port = (uint16_t) (start_port - 1);
        do {
            ++port;
            sock = make_socket(port);
        } while (sock == -1 && port <= end_port);
        char buffer[HOST_NAME_MAX];
        gethostname(buffer, HOST_NAME_MAX);
        buffer[HOST_NAME_MAX - 1] = 0;
        hostname = buffer;
        thread = std::thread([this]() { main(); });
    }

    ~inet_server()
    {
        running = false;
        thread.join();
        close(sock);
    }

    virtual bool on_client_message(const std::string& msg) = 0;
    virtual bool on_client_error(int) = 0;
    virtual void on_start() = 0;
    virtual void on_stop() = 0;

    int
    read_from_client (int filedes)
    {
        char buffer[MAXMSG];
        ssize_t nbytes;

        nbytes = read (filedes, buffer, MAXMSG);
        if (nbytes < 0) {
            /* Read error. */
            on_client_error(errno);
        }
        else if (nbytes == 0) {
            /* End-of-file. */
            return -1;
        }
        /* Data read. */
        running.fetch_and(on_client_message(buffer), std::memory_order::memory_order_relaxed);
        return 0;
    }

    void
    wait_for_server_to_run()
    {
        std::unique_lock<std::mutex> lk(is_running_mutex);
        is_running_cv.wait(lk, [this] () -> bool { return running; });
    }

    void
    main (void)
    {
        fd_set active_fd_set, read_fd_set;
        int i;
        struct sockaddr_in clientname;
        socklen_t size;

        /* Create the socket and set it up to accept connections. */
        if (listen(sock, 1) < 0)
        {
            MSG_ERROR("Can't listen: " << strerror(errno), "");
            return;
        }

        /* Initialize the set of active sockets. */
        FD_ZERO (&active_fd_set);
        FD_SET (sock, &active_fd_set);

        {
            std::lock_guard<std::mutex> guard(is_running_mutex);
            running = true;
            on_start();
            is_running_cv.notify_all();
        }

        while (running)
        {
            /* Block until input arrives on one or more active sockets. */
            read_fd_set = active_fd_set;
            if (select (FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0)
            {
                perror ("select");
                exit (EXIT_FAILURE);
            }

            /* Service all the sockets with input pending. */
            for (i = 0; i < FD_SETSIZE; ++i)
                if (FD_ISSET (i, &read_fd_set))
                {
                    if (i == sock)
                    {
                        /* Connection request on original socket. */
                        int newcli;
                        size = sizeof (clientname);
                        newcli = accept (sock,
                                      (struct sockaddr *) &clientname,
                                      &size);
                        if (newcli < 0)
                        {
                            perror ("accept");
                            exit (EXIT_FAILURE);
                        }
                        FD_SET (newcli, &active_fd_set);
                    }
                    else
                    {
                        /* Data arriving on an already-connected socket. */
                        if (read_from_client (i) < 0)
                        {
                            close (i);
                            FD_CLR (i, &active_fd_set);
                        }
                    }
                }
        }

        on_stop();
    }
};

struct inet_client {
    struct sockaddr_in name;

    inet_client(const std::string &uri)
    {
        init_sockaddr(uri);
    }

    void
    init_sockaddr (const char *hostname,
                   uint16_t port)
    {
        struct hostent *hostinfo;

        name.sin_family = AF_INET;
        name.sin_port = htons (port);
        hostinfo = gethostbyname (hostname);
        if (hostinfo == NULL)
        {
            fprintf (stderr, "Unknown host %s.\n", hostname);
            exit (EXIT_FAILURE);
        }
        name.sin_addr = *(struct in_addr *) hostinfo->h_addr;
    }

    void
    init_sockaddr(const std::string& uri)
    {
        size_t ofs = uri.find(':');
        std::string hostname(uri.begin(), uri.begin() + ofs);
        std::string portstr(uri.begin() + ofs + 1, uri.end());
        uint16_t port = atoi(portstr.c_str());
        init_sockaddr(hostname.c_str(), port);
    }

    void
    connect_and_send(const std::string& s)
    {
        int sock = socket(PF_INET, SOCK_STREAM, 0);
        if (sock < 0) {
            MSG_ERROR("Couldn't create socket: " << strerror(errno), "");
            return;
        }
        if (connect(sock,  (struct sockaddr*) &name, sizeof(name)) < 0) {
            MSG_ERROR("Couldn't connect to server: " << strerror(errno), "");
        }
        int r = write(sock, s.c_str(), s.size());
        (void) r;
        close(sock);
    }
};

#endif // _WIN32

#endif //SPELL_QTL_INET_H
