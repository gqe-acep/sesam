/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_BAYES_OUTPUT_H_
#define _SPELL_BAYES_OUTPUT_H_

#include <map>
#include <vector>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
#include <string>
#include <iostream>
#include "file.h"
#include <cstring>
#include <cstdint> // defines PTRDIFF_MAX
#include <limits>  // defines INT_MAX
#include <type_traits>

#include "eigen.h"
#include "error.h"
#include "data/chromosome.h"
/*#include "generation_rs_fwd.h"*/
#include "input/read_trait.h"
#include "tensor.h"
#include "linear_combination.h"

#include <boost/math/tools/minima.hpp>

/** FOURCC **/

inline
bool check_fourcc(ifile& ifs, const char* fourcc)
{
    char buf[4] = {0, 0, 0, 0};
    ifs.read(buf, 4);
    return strncmp(fourcc, buf, 4);
}

inline
void write_fourcc(ofile& ofs, const char* fourcc)
{
    ofs.write(fourcc, 4);
}

/** SIZE_T **/

inline
void write_size(ofile& ofs, size_t sz)
{
    ofs.write((const char*) &sz, sizeof sz);
}

inline
size_t read_size(ifile& ifs)
{
    size_t ret;
    ifs.read((char*) &ret, sizeof ret);
    return ret;
}

/** PTRDIFF_T **/

inline
void write_ptrdiff(ofile& ofs, ptrdiff_t sz)
{
    ofs.write((const char*) &sz, sizeof sz);
}

inline
ptrdiff_t read_ptrdiff(ifile& ifs)
{
    ptrdiff_t ret;
    ifs.read((char*) &ret, sizeof ret);
    return ret;
}

/** STRING **/

inline
std::string read_str(ifile& ifs)
{
    size_t sz = read_size(ifs);
    if(sz < 1){ return ""; }// DEBUG Frank Gauthier 03/07/20
    std::vector<char> tmp(sz);
    ifs.read(&tmp[0], sz);
    return {tmp.begin(), tmp.end()};
}

inline
void write_str(ofile& ofs, const std::string& s)
{
    write_size(ofs, s.size());
    ofs.write(s.c_str(), s.size());
}

/** DOUBLE **/

inline
void write_double(ofile& ofs, double sz)
{
    ofs.write((const char*) &sz, sizeof sz);
}

inline
double read_double(ifile& ifs)
{
    double ret;
    ifs.read((char*) &ret, sizeof ret);
    return ret;
}

/** INT **/

inline
void write_int(ofile& ofs, int sz)
{
    ofs.write((const char*) &sz, sizeof sz);
}

inline
int read_int(ifile& ifs)
{
    int ret;
    ifs.read((char*) &ret, sizeof ret);
    return ret;
}

/** LONG **/

inline
void write_long(ofile& ofs, long sz)
{
    ofs.write((const char*) &sz, sizeof sz);
}

inline
long read_long(ifile& ifs)
{
    long ret;
    ifs.read((char*) &ret, sizeof ret);
    return ret;
}

/** CHAR **/

inline
void write_char(ofile& ofs, char sz)
{
    ofs.write((const char*) &sz, sizeof sz);
}

inline
char read_char(ifile& ifs)
{
    char ret;
    ifs.read(&ret, sizeof ret);
    return ret;
}

#if 0
/** FAST_POLYNOM **/

inline
void write_fast_polynom(ofile& ofs, const fast_polynom& fp)
{
    impl::f_polynom f = fp;
    write_int(ofs, fp.value);
    write_int(ofs, f.r_exp);
    write_int(ofs, f.s_exp);
    write_size(ofs, f.P.size());
    for (double c: f.P) {
        write_double(ofs, c);
    }
}

inline
fast_polynom read_fast_polynom(ifile& ifs, int& original_key)
{
    original_key = read_int(ifs);
    impl::f_polynom ret = fast_polynom::zero;
    ret.r_exp = read_int(ifs);
    ret.s_exp = read_int(ifs);
    size_t sz = read_size(ifs);
    ret.P.resize(sz);
    for (size_t i = 0; i < sz; ++i) {
        ret.P[i] = read_double(ifs);
    }
    return ret;
}

/** ALGREBRAIC GENOTYPE **/

inline void write_algebraic_genotype(ofile& ofs, const algebraic_genotype& ag)
{
    ofs.write((const char*) &ag.genotype, sizeof ag.genotype);
    ofs.write((const char*) &ag.type, sizeof ag.type);
    write_int(ofs, ag.poly.value);
}

inline algebraic_genotype read_algebraic_genotype(ifile& ifs, const std::map<int, fast_polynom>& pt)
{
    algebraic_genotype ag;
    ifs.read((char*) &ag.genotype, sizeof ag.genotype);
    ifs.read((char*) &ag.type, sizeof ag.type);
    ag.poly = pt.find(read_int(ifs))->second;
    return ag;
}

/** GENOMATRIX **/

inline void write_genomatrix(ofile& ofs, const GenoMatrix& mat)
{
    write_fourcc(ofs, "SGEM");
    /*std::map<decltype(fast_polynom::value), impl::f_polynom>*/
    std::set<fast_polynom> poly_table;
    for (int j = 0; j < mat.cols(); ++j) {
        for (int i = 0; i < mat.rows(); ++i) {
            poly_table.insert(mat(i, j).poly);
        }
    }
    write_size(ofs, poly_table.size());
    for (const auto& fp: poly_table) {
        write_fast_polynom(ofs, fp);
    }
    write_int(ofs, mat.cols());
    write_int(ofs, mat.rows());
    /*MSG_DEBUG("[write_genomatrix] cols=" << mat.cols() << " rows=" << mat.rows());*/
    for (int j = 0; j < mat.cols(); ++j) {
        for (int i = 0; i < mat.rows(); ++i) {
            /*write_int(ofs, mat(i, j).value);*/
            write_algebraic_genotype(ofs, mat(i, j));
        }
    }
}

inline
void read_genomatrix(ifile& ifs, GenoMatrix& mat)
{
    if (check_fourcc(ifs, "SGEM")) {
        MSG_ERROR("File is not valid or has been corrupted (expected SGEM):\n=> " << ifs.m_path, "");
        return;
    }
    std::map<int, fast_polynom> poly_map;

    size_t table_size = read_size(ifs);
    int key;

    for (size_t i = 0; i < table_size; ++i) {
        auto f = read_fast_polynom(ifs, key);
        poly_map[key] = f;
    }

    int cols = read_int(ifs);
    int rows = read_int(ifs);
    /*MSG_DEBUG("[read_genomatrix] cols=" << cols << " rows=" << rows);*/
    mat.resize(rows, cols);
    for (int j = 0; j < mat.cols(); ++j) {
        for (int i = 0; i < mat.rows(); ++i) {
            mat(i, j) = read_algebraic_genotype(ifs, poly_map);
        }
    }
}

/** GENERATION_RS **/

inline void write_geno_matrix(ofile& ofs, const geno_matrix* gen)
{
    write_fourcc(ofs, "SGRS");
    write_str(ofs, gen->name);
    write_size(ofs, gen->P.size());
    for (const auto& p: gen->P) {
        write_double(ofs, p.weight);
        write_genomatrix(ofs, p.G.data);
    }
}

inline
geno_matrix* read_geno_matrix(ifile& ifs)
{
    if (check_fourcc(ifs, "SGRS")) {
        MSG_ERROR("File is not valid or has been corrupted (expected SGRS):\n=> " << ifs.m_path, "");
    }
    /*MSG_DEBUG("pouet 1"); MSG_QUEUE_FLUSH();*/
    std::string name = read_str(ifs);
    /*MSG_DEBUG("pouet 2"); MSG_QUEUE_FLUSH();*/
    geno_matrix* ret = geno_matrix::blank(name);
    /*MSG_DEBUG("pouet 3"); MSG_QUEUE_FLUSH();*/
    size_t n_p = read_size(ifs);
    /*MSG_DEBUG("Have " << n_p << " processes"); MSG_QUEUE_FLUSH();*/
    ret->P.resize(n_p);
    for (size_t i = 0; i < n_p; ++i) {
        ret->P[i].weight = read_double(ifs);
        GenoMatrix tmp;
        read_genomatrix(ifs, tmp);
        ret->P[i].G = convert(tmp);
        /*MSG_DEBUG("Read process");*/
        /*MSG_DEBUG(ret->P[i]);*/
    }
    ret->precompute();
    return ret;
}
#endif


/** MATRIX<SCALAR, R, C> **/

template <typename MATRIX_TYPE>
struct resize_matrix_impl;

template <> struct resize_matrix_impl<VectorXd> { void operator () (VectorXd& v, size_t rows, size_t) { v.resize(rows); } };
template <> struct resize_matrix_impl<MatrixXd> { void operator () (MatrixXd& m, size_t rows, size_t cols) { m.resize(rows, cols); } };

template <typename SCALAR, int ROW, int COL, int C, int D, int E>
struct resize_matrix_impl<Eigen::Matrix<SCALAR, ROW, COL, C, D, E>> {
    void operator () (Eigen::Matrix<SCALAR, ROW, COL, C, D, E>& m, size_t r, size_t c) { m.resize(r, c); }
};

template <typename SCALAR, int ROW, int C, int D, int E>
struct resize_matrix_impl<Eigen::Matrix<SCALAR, ROW, 1, C, D, E>> {
    void operator () (Eigen::Matrix<SCALAR, ROW, 1, C, D, E>& v, size_t r, size_t) { v.resize(r); }
};

template <typename SCALAR, int COL, int C, int D, int E>
struct resize_matrix_impl<Eigen::Matrix<SCALAR, 1, COL, C, D, E>> {
    void operator () (Eigen::Matrix<SCALAR, 1, COL, C, D, E>& v, size_t, size_t c) { v.resize(c); }
};

template <typename MATRIX_TYPE>
void resize_matrix(MATRIX_TYPE& m, size_t r, size_t c) { resize_matrix_impl<MATRIX_TYPE>()(m, r, c); }

template <typename SCALAR, int ROW, int COL, int C, int D, int E>
void read_matrix(ifile& ifs, Eigen::Matrix<SCALAR, ROW, COL, C, D, E>& mat)
{
    size_t scalar_sz = read_size(ifs);
    if (scalar_sz != sizeof(SCALAR)) {
        MSG_ERROR("WRONG SIZE OF SCALAR, CAN'T READ FILE", "Make sure spell-marker and spell-qtl are always executed on machines with same word size.");
    }
    size_t n_row = read_size(ifs);
    size_t n_col = read_size(ifs);
    if (ROW != Eigen::Dynamic && ((int) n_row) != ROW) {
        MSG_ERROR("WRONG ROW COUNT. FILE IS NOT A LOCUS VECTOR FILE OR IS CORRUPTED", "You may want to run spell-marker again");
    }
    if (COL != Eigen::Dynamic && ((int) n_col) != COL) {
        MSG_ERROR("WRONG COLUMN COUNT. FILE IS NOT A LOCUS VECTOR FILE OR IS CORRUPTED", "You may want to run spell-marker again");
    }
    resize_matrix(mat, n_row, n_col);
    ifs.read((char*) mat.data(), n_row * n_col * sizeof(SCALAR));
}

template <typename SCALAR, int ROW, int COL, int C, int D, int E>
void write_matrix(ofile& ofs, const Eigen::Matrix<SCALAR, ROW, COL, C, D, E>& mat)
{
    write_size(ofs, sizeof(SCALAR));
    write_size(ofs, mat.rows());
    write_size(ofs, mat.cols());
    ofs.write((const char*) mat.data(), mat.size() * sizeof(SCALAR));
}


template <typename V, typename READ_ELEM_FUNC>
void read_vector(ifile& ifs, std::vector<V>& vec, READ_ELEM_FUNC read_elem)
{
    size_t sz = read_size(ifs);
    vec.clear();
    vec.reserve(sz);
    for (size_t i = 0; i < sz; ++i) {
        vec.emplace_back(read_elem(ifs));
    }
}


template <typename V, typename WRITE_ELEM_FUNC>
void write_vector(ofile& ofs, const std::vector<V>& vec, WRITE_ELEM_FUNC write_elem)
{
    write_size(ofs, vec.size());
    for (const auto& e: vec) {
        write_elem(ofs, e);
    }
}


/** LABEL_TYPE **/

inline
label_type read_label(ifile& ifs)
{
    char f = 0, s = 0;
    ifs >> f >> s;
//     MSG_DEBUG("[read_label] " << int(f) << ':' << int(s));
    return {f, s};
}

inline
void write_label(ofile& ofs, const label_type& l)
{
    ofs << l.first() << l.second();
}


/** GENO_MATRIX **/

inline
void write_geno_matrix(ofile& ofs, const geno_matrix& mat)
{
    write_fourcc(ofs, "SGEM");
    write_str(ofs, mat.name);
    /* skip variant, it's deprecated. Or should be. */
    write_vector(ofs, mat.labels, write_label);
    write_matrix(ofs, mat.inf_mat);
    write_matrix(ofs, mat.p);
    write_matrix(ofs, mat.p_inv);
    write_matrix(ofs, mat.diag);
    write_matrix(ofs, mat.stat_dist);
    write_matrix(ofs, mat.collect);
    write_matrix(ofs, mat.dispatch);
    /* also skip symmetries for now. */
}

inline
void read_geno_matrix(ifile& ifs, geno_matrix& mat)
{
    if (check_fourcc(ifs, "SGEM")) {
        MSG_ERROR("File is not valid or has been corrupted (expected SGEM):\n=> " << ifs.m_path, "");
        return;
    }
    mat.name = read_str(ifs);
    /* skip variant. */
    read_vector(ifs, mat.labels, read_label);
    read_matrix(ifs, mat.inf_mat);
    read_matrix(ifs, mat.p);
    read_matrix(ifs, mat.p_inv);
    read_matrix(ifs, mat.diag);
    read_matrix(ifs, mat.stat_dist);
    read_matrix(ifs, mat.collect);
    read_matrix(ifs, mat.dispatch);
    /* also skip symmetries for now. */
    MSG_DEBUG("read geno_matrix " << MATRIX_SIZE(mat.inf_mat) << " labels " << mat.labels);
}


inline
void write_geno_matrix(ofile& ofs, const geno_matrix* ptr)
{
    write_geno_matrix(ofs, *ptr);
}

inline
geno_matrix* read_geno_matrix(ifile& ifs)
{
    geno_matrix* ret = new geno_matrix();
    read_geno_matrix(ifs, *ret);
    return ret;
}



template <class DERIVED>
struct rw_any {
    bool fourcc(ifile& ifs, const char* cc)
    {
        if (check_fourcc(ifs, cc)) {
            MSG_ERROR("File is not valid or has been corrupted (expected " << cc << "):\n=> " << ifs.m_path, "");
            return true;
        }
        return false;
    }

    bool fourcc(ofile& ofs, const char* cc)
    {
        write_fourcc(ofs, cc);
        return false;
    }

    virtual ~rw_any() {}

    DERIVED& ref() { return *dynamic_cast<DERIVED*>(this); }

    void operator () (ifile& ifs, std::string& s) { s = read_str(ifs); }
    void operator () (ofile& ofs, const std::string& s) { write_str(ofs, s); }

    void operator () (ifile& ifs, char& i) { i = read_char(ifs); }
    void operator () (ofile& ofs, char i) { write_char(ofs, i); }

    void operator () (ifile& ifs, int& i) { i = read_int(ifs); }
    void operator () (ofile& ofs, int i) { write_int(ofs, i); }

    void operator () (ifile& ifs, bool& i) { i = !!read_char(ifs); }
    void operator () (ofile& ofs, bool i) { write_char(ofs, i); }

    void operator () (ifile& ifs, double& i) { i = read_double(ifs); }
    void operator () (ofile& ofs, double i) { write_double(ofs, i); }

    void operator () (ifile& ifs, size_t& i) { i = read_size(ifs); }
    void operator () (ofile& ofs, size_t i) { write_size(ofs, i); }

// On some 32 bits system (not all of them), ptrdiff_t may be a typedef of int
// => this lead to compilation error: redefinition of operator () (ifile& ifs, int& i)
// DEBUG: define operator() using type ptrdiff_t only on 64 bits system (PTRDIFF_MAX != INT_MAX)
#ifdef PTRDIFF_MAX
#if PTRDIFF_MAX != INT_MAX
    // 64 bits system
    void operator () (ifile& ifs, ptrdiff_t& i) { i = read_ptrdiff(ifs); }
    void operator () (ofile& ofs, ptrdiff_t i) { write_ptrdiff(ofs, i); }
#else
    // 32 bits system, PTRDIFF_MAX == INT_MAX == LONG_MAX
    // ptrdiff_t is a typedef of either int or long
	// => we have to define operator() using type long
    void operator () (ifile& ifs, long& i) { i = read_long(ifs); }
    void operator () (ofile& ofs, long i) { write_long(ofs, i); }
#endif
#endif
    void operator () (ifile& ifs, label_type& l) { l = {read_char(ifs), read_char(ifs)}; }
    void operator () (ofile& ofs, label_type& l) { write_char(ofs, l.first()); write_char(ofs, l.second()); }

    template <typename K>
        auto operator () (ifile& fs, K& has_file_io)
        -> decltype(has_file_io.file_io(fs), void())
        {
            has_file_io.file_io(fs);
        }

    template <typename K>
        auto operator () (ofile& fs, K& has_file_io)
        -> decltype(has_file_io.file_io(fs), void())
        {
            has_file_io.file_io(fs);
        }

    template <typename V>
        void operator () (ifile& ifs, std::set<V>& vec)
        {
            if (fourcc(ifs, "OSET")) { return; }
            size_t sz = read_size(ifs);
            vec.clear();
            for (size_t i = 0; i < sz; ++i) {
                V tmp;
                ref() (ifs, tmp);
                vec.emplace(tmp);
            }
        }

    template <typename V>
        void operator () (ofile& ofs, std::set<V>& vec)
        {
            if (fourcc(ofs, "OSET")) { return; }
            write_size(ofs, vec.size());
            for (auto& e: vec) {
                ref() (ofs, e);
            }
        }


    template <typename V>
        void operator () (ifile& ifs, std::unordered_set<V>& vec)
        {
            if (fourcc(ifs, "USET")) { return; }
            size_t sz = read_size(ifs);
            vec.clear();
            for (size_t i = 0; i < sz; ++i) {
                V tmp;
                ref() (ifs, tmp);
                vec.emplace(tmp);
            }
        }

    template <typename V>
        void operator () (ofile& ofs, std::unordered_set<V>& vec)
        {
            if (fourcc(ofs, "USET")) { return; }
            write_size(ofs, vec.size());
            for (auto& e: vec) {
                ref() (ofs, e);
            }
        }

    void operator () (ifile& ifs, std::vector<bool>::reference i) { i = !!read_char(ifs); }

    template <typename V, typename A>
        void operator () (ifile& ifs, std::vector<V, A>& vec)
        {
            if (fourcc(ifs, "VECT")) { return; }
            size_t sz = read_size(ifs);
            vec.clear();
            vec.reserve(sz);
            for (size_t i = 0; i < sz; ++i) {
                vec.emplace_back();
                ref() (ifs, vec.back());
            }
        }

    template <typename V, typename A>
        void operator () (ofile& ofs, std::vector<V, A>& vec)
        {
            if (fourcc(ofs, "VECT")) { return; }
            write_size(ofs, vec.size());
            for (auto& e: vec) {
                ref() (ofs, e);
            }
        }

        void operator () (ofile& ofs, std::vector<bool>& vec)
        {
            if (fourcc(ofs, "VECT")) { return; }
            write_size(ofs, vec.size());
            for (const auto& e: vec) {
                ref() (ofs, e);
            }
        }

    template <typename V, typename A, typename RWElem>
        void operator () (ifile& ifs, std::vector<V, A>& vec, RWElem rw_elem)
        {
            if (fourcc(ifs, "VECT")) { return; }
            size_t sz = read_size(ifs);
            vec.clear();
            vec.reserve(sz);
            for (size_t i = 0; i < sz; ++i) {
                vec.emplace_back();
                rw_elem(ifs, vec.back());
            }
        }

    template <typename V, typename A, typename RWElem>
        void operator () (ofile& ofs, std::vector<V, A>& vec, RWElem rw_elem)
        {
            if (fourcc(ofs, "VECT")) { return; }
            write_size(ofs, vec.size());
            for (auto& e: vec) {
                rw_elem(ofs, e);
            }
        }

    template <typename K, typename V, typename A, typename C>
        void operator () (ifile& ifs, std::map<K, V, A, C>& map)
        {
            if (fourcc(ifs, "MAP ")) { return; }
            size_t count = read_size(ifs);
            map.clear();
            for (size_t i = 0; i < count; ++i) {
                K key;
                V value;
                ref() (ifs, key);
                ref() (ifs, value);
                map.emplace(std::move(key), std::move(value));
            }
        }

    template <typename K, typename V, typename A, typename C>
        void operator () (ifile& ifs, std::unordered_map<K, V, A, C>& map)
        {
            if (fourcc(ifs, "MAP ")) { return; }
            size_t count = read_size(ifs);
            map.clear();
            for (size_t i = 0; i < count; ++i) {
                K key;
                V value;
                ref() (ifs, key);
                ref() (ifs, value);
                map.emplace(std::move(key), std::move(value));
            }
        }

    template <typename K, typename V, typename A, typename C>
        void operator () (ofile& ofs, std::map<K, V, A, C>& map)
        {
            if (fourcc(ofs, "MAP ")) { return; }
            write_size(ofs, map.size());
            for (auto& kv: map) {
                ref() (ofs, const_cast<K&>(kv.first));
                ref() (ofs, kv.second);
            }
        }

    template <typename K, typename V, typename A, typename C>
        void operator () (ofile& ofs, std::unordered_map<K, V, A, C>& map)
        {
            if (fourcc(ofs, "MAP ")) { return; }
            write_size(ofs, map.size());
            for (auto& kv: map) {
                ref() (ofs, kv.first);
                ref() (ofs, kv.second);
            }
        }

    template <typename IO, typename A, typename B>
        void operator () (IO& fs, std::pair<A, B>& p)
        {
            ref() (fs, p.first);
            ref() (fs, p.second);
        }

    template <typename SCALAR, int ROW, int COL, int C, int D, int E>
        void operator () (ifile& ifs, Eigen::Matrix<SCALAR, ROW, COL, C, D, E>& mat) { read_matrix(ifs, mat); }

    template <typename SCALAR, int ROW, int COL, int C, int D, int E>
        void operator () (ofile& ofs, Eigen::Matrix<SCALAR, ROW, COL, C, D, E>& mat) { write_matrix(ofs, mat); }

    void operator () (ifile& ifs, geno_matrix& mat) { read_geno_matrix(ifs, mat); }
    void operator () (ofile& ofs, geno_matrix& mat) { write_geno_matrix(ofs, mat); }

    void operator () (ifile& ifs, std::shared_ptr<geno_matrix>& ptr)
    {
        ptr.reset();
        ptr = std::make_shared<geno_matrix>();
        ref() (ifs, *ptr);
        if (!ptr->size()) {
            ptr.reset();
        }
    }

    void operator () (ofile& ofs, std::shared_ptr<geno_matrix>& ptr)
    {
        if (ptr) {
            ref() (ofs, *ptr.get());
        } else {
            geno_matrix _;
            ref() (ofs, _);
        }
    }
};


struct rw_base : public rw_any<rw_base> {
    virtual ~rw_base() {}
    using rw_any<rw_base>::fourcc;
    using rw_any<rw_base>::ref;
    using rw_any<rw_base>::operator ();
};



/** **/


typedef union _gamete_lv_value_type {
    Eigen::Vector4d binary;
    Eigen::Vector2d unary;
    
    static _gamete_lv_value_type unknown_binary() { static _gamete_lv_value_type ret = {(Eigen::Vector4d() << .25, .25, .25, .25).finished()}; return ret; }
    static _gamete_lv_value_type unknown_unary() { static _gamete_lv_value_type ret = {(Eigen::Vector4d() << .5, .5, 0, 0).finished()}; return ret; }
    
    _gamete_lv_value_type&
    operator = (const _gamete_lv_value_type& other)
    {
        binary = other.binary;
        return *this;
    }
    
    template <typename STREAM_TYPE>
    void
    file_io(STREAM_TYPE& fs)
    {
        rw_base rw;
        rw(fs, binary);
    }
    
    _gamete_lv_value_type() : binary() {}
    _gamete_lv_value_type(const _gamete_lv_value_type& other) : binary(other.binary) {}
    _gamete_lv_value_type(const Eigen::Vector2d& v) { unary = v; }
    _gamete_lv_value_type(const Eigen::Vector4d& v) { binary = v; }
    friend std::ostream& operator << (std::ostream& os, const _gamete_lv_value_type& glv) { return os << glv.binary.transpose(); }
} gamete_lv_value_type;


struct gamete_LV_type {
    bool is_single;
    std::unordered_map<std::string, gamete_lv_value_type> lv;

    friend std::ostream& operator << (std::ostream& os, const gamete_LV_type& glv) { return os << (glv.is_single ? "Unary{" : "Binary{") << glv.lv << '}'; }
    
    void get(const std::string& key, Eigen::Vector2d& value) const
    {
        auto it = lv.find(key);
        if (it == lv.end()) {
            value = Eigen::Vector2d::Zero();
        } else {
            value = it->second.unary;
        }
    }

    void get(const std::string& key, Eigen::Vector4d& value) const
    {
        auto it = lv.find(key);
        if (it == lv.end()) {
            value = Eigen::Vector4d::Zero();
        } else {
            value = it->second.binary;
        }
    }
    
    void add_normalized(const std::string& key, const Eigen::VectorXd& value) { if (value.size() == 2) { lv[key].unary = value; } else { lv[key].binary = value; } }

    template <typename T>
    void add(const std::string& key, const T& value) { add_normalized(key, value); }

    template <typename STREAM_TYPE>
    void
    file_io(STREAM_TYPE& fs)
    {
        rw_base rw;
        if (rw.fourcc(fs, "GLVT")) { return; }
        rw(fs, is_single);
        rw(fs, lv);
    }
};


#if 0
struct em_dist_opt : public Eigen::Functor<double> {
    em_dist_opt() : Eigen::Functor<double>(1, 5) {}
    int
    operator() (const VectorXd& b, VectorXd& fvec) const
    {
        double r = b(0);
        double r2 = r * r;
        fvec <<
            (r2 - r - r + 1),
            (r2 - r),
            (r2),
            (r),
            (1 - r);
        return 0;
    }
    
    int
    df(const VectorXd& b, VectorXd& fjac) const
    {
        double r = b(0);
        double _2r = r + r;
        fjac <<
            (_2r - 2),
            (_2r - 1),
            (_2r),
            (1),
            (-1);
        return 0;
    }
};
#endif


struct EM_helpers {
#   define sqrt5 2.236067977499789696

    struct max_return_type {
        double a, b, fa, fb;
    };
    
    template <typename F>
    max_return_type
    find_max(F&& f, double a0, double b0)
    {
#       define interval_too_small(_x, _y) ((_y - _x) < tolerance)
        static const double lambda = 0.5 * (sqrt5 - 1.0);
        static const double mu = 0.5 * (3.0 - sqrt5);         // = 1 - lambda
        static const double tolerance = 3.e-8;
        double x1;
        double x2;
        double fx1;
        double fx2;
        max_return_type ret = {a0, b0, f(a0), f(b0)};
        

                        // Find first two internal points and evaluate 
                        // the function at the two internal points.

        x1 = ret.b - lambda * (ret.b - ret.a);                            
        x2 = ret.a + lambda * (ret.b - ret.a);                         
        fx1 = f(x1);
        fx2 = f(x2);

                    // Verify that the tolerance is an acceptable number

//         if (tolerance <= 0.0) tolerance = sqrt(DBL_EPSILON) * (ret.b - ret.a);

                // Loop by exluding segments from current endpoints a, b
                // to current internal points x1, x2 and then calculating
                // a new internal point until the length of the interval
                // is less than or equal to the tolerance.
        MSG_INFO(std::setprecision(10));
        for (double x = .42; x < .5; x += .001) {
            MSG_INFO(x << "\t" << f(x));
        }

        while (!interval_too_small(ret.a, ret.b)) {
            if (fx1 < fx2) {
                ret.a = x1;
                ret.fa = fx1;
                if (interval_too_small(ret.a, ret.b)) break;
                x1 = x2;
                fx1 = fx2;
                x2 = ret.b - mu * (ret.b - ret.a);
                fx2 = f(x2);
            } else {
                ret.b = x2;
                ret.fb = fx2;
                if (interval_too_small(ret.a, ret.b)) break;
                x2 = x1;
                fx2 = fx1;
                x1 = ret.a + mu * (ret.b - ret.a);
                fx1 = f(x1);
            }
        }
        return ret;
    }
    
    static
    const Eigen::Matrix2d&
    S_kernel(const Eigen::Vector2d&)
    {
        static Eigen::Matrix2d kernel = (Eigen::Matrix2d() << 1, 0, 0, 1).finished();
        return kernel;
    }

    static
    const Eigen::Matrix4d&
    S_kernel(const Eigen::Vector4d&)
    {
        static Eigen::Matrix4d kernel = (Eigen::Matrix4d() << 2, 1, 1, 0,
                                                              1, 2, 0, 1,
                                                              1, 0, 2, 1,
                                                              0, 1, 1, 2).finished();
        return kernel;
    }
    
    template <typename VTYPE>
    static
    double
    compute_S(VTYPE&& a, VTYPE&& b)
    {
        const auto& kernel = S_kernel(a);
        double ret = a.transpose() * kernel * b;
        scoped_indent _("[compute_S] ");
//         MSG_DEBUG("a = " << a.transpose());
//         MSG_DEBUG("b = " << b.transpose());
//         MSG_DEBUG("kernel" << std::endl << kernel);
//         MSG_DEBUG("ret = " << ret);
        return ret;
    }

    /*
    static
    double
    compute_S(const std::vector<gamete_LV_type>& v1, const std::vector<gamete_LV_type>& v2)
    {
        size_t n_mei = 0;
        auto i1 = v1.begin(), i2 = v2.begin(), j = v1.end();
        double s = 0;
        // should assert that v1.size() == v2.size()
        for (; i1 != j; ++i1, ++i2) {
            n_mei += 2 - i1->is_single;
            s += compute_S(*i1, *i2);
        }
        return s / n_mei;
    }
    //*/
    
    static
    double
    S_to_dist(double s)
    {
        scoped_indent _("[S_to_dist] ");
// HAXX
//// ADRIEN VIDAL : Modification to recombination rates thresholds to allow measuring anti-linkage (Linkage with inverse phase).
#define EM_R_MAX .999999 // Search for optimal R (max likelihood) in range [0;1[.
#define EM_R_INF .499999
#define EM_S_MAX .999999
        MSG_DEBUG("s = " << s);
        if (s > EM_S_MAX) {
            s = EM_S_MAX;
        } else if (s < EM_R_MAX) {
            s = EM_R_MAX;
        }
            
        MSG_DEBUG("s = " << s);
        
        return -.5 * log(s + s - 1);
    }
};


struct EM_map {
    std::vector<std::string> marker_names;
    std::vector<double> distances;
    std::vector<double> r;
    double likelihood;
    int n_iterations;
    bool converged;
    double delta;
};


struct gamete_LV_database {
    /* IND.ped / MARK => 1 or 2 gametes (.second will be empty when cross type is DH) */
    std::unordered_map<int, gamete_LV_type> data;
    std::unordered_map<double, Eigen::Matrix2d> cache2;
    std::unordered_map<double, Eigen::Matrix4d> cache4;
    std::vector<Eigen::Matrix2d> tr2;
    std::vector<Eigen::Matrix4d> tr4;
    // FIXME need mutexes around get_TR_* and cache* stuff
    gamete_LV_database() : data(), cache2(), cache4(), tr2(), tr4() {}

    Eigen::Matrix2d
    get_TR_unary(double dist_or_r, bool use_r=false)
    {
        double r, s;
        if (use_r) {
            r = dist_or_r;
            s = 1. - r;
        } else {
            s = .5 + exp(-2. * dist_or_r) * .5;
            r = 1. - s;
        }
//         auto it = cache2.find(r);
//         if (it == cache2.end()) {
            Eigen::Matrix2d ret;
            ret << s, r, r, s;
//             cache2[r] = ret;
            return ret;
//         }
//         return it->second;
    }
    
    Eigen::Matrix4d
    get_TR_binary(double dist_or_r, bool use_r=false)
    {
        double r, s;
        if (use_r) {
            r = dist_or_r;
            s = 1. - r;
        } else {
            s = .5 + exp(-2. * dist_or_r) * .5;
            r = 1. - s;
        }
//         auto it = cache4.find(r);
//         if (it == cache4.end()) {
            double rs = r * s;
            double r2 = r * r;
            double s2 = s * s;
            Eigen::Matrix4d ret;
            ret <<
                s2, rs, rs, r2,
                rs, s2, r2, rs,
                rs, r2, s2, rs,
                r2, rs, rs, s2;
//             cache4[r] = ret;
            return ret;
//         }
//         return it->second;
    }
    
    void get_TR(double dist, Eigen::Matrix2d& result) { result = get_TR_unary(dist); }
    void get_TR(double dist, Eigen::Matrix4d& result) { result = get_TR_binary(dist); }

    void
    init_2pt_tr_at_inf()
    {
        tr2.clear();
        tr4.clear();
        tr2.push_back(MatrixXd::Constant(2, 2, .5));
        tr4.push_back(MatrixXd::Constant(4, 4, .25));
    }
    
    void
    add_gametes(const std::string& mark, int ind, const VectorXd& lv, bool dh)
    {
        auto it = data.find(ind);
        if (it == data.end()) {
            data[ind].is_single = dh;
        } else {
            if (dh != it->second.is_single) {
                MSG_ERROR("Individual #" << ind << " was first declared as " << (it->second.is_single ? "intercross" : "doubled haploid") << " and is now requested to be the other kind.", "");
            }
        }
//         data[ind].add(mark, (lv.array() > 0).select(VectorXd::Ones(lv.size()), VectorXd::Zero(lv.size())).matrix());
        data[ind].add(mark, lv);
    }

    void init_tr(bool is_single, const std::vector<double>& distances)
    {
        if (is_single) {
            if (tr2.size()) {
                return;
            }
            tr2.reserve(distances.size());
            for (double d: distances) {
                tr2.emplace_back();
                get_TR(d, tr2.back());
            }
        } else {
            if (tr4.size()) {
                return;
            }
            tr4.reserve(distances.size());
            for (double d: distances) {
                tr4.emplace_back();
                get_TR(d, tr4.back());
            }
        }
    }

    template <typename V, typename M>
    double gamete_likelihood_impl(std::vector<M>& TR, V& accum, const std::vector<std::string>& marker_names, const gamete_LV_type& gam)
    {
        auto name_i = marker_names.begin();
        V tmp;
        gam.get(*name_i++, tmp);
//         accum = tmp / tmp.sum();
// //         accum = tmp / (tmp.array() > 0).template cast<double>().sum();
        accum = tmp/* * .25*/;
        for (const auto& t: TR) {
            gam.get(*name_i++, tmp);
//             MSG_DEBUG("accum = " << accum.transpose() << " LV = " << tmp.transpose());
            accum = accum.transpose() * t * tmp.asDiagonal();
        }
//         MSG_DEBUG("final accum = " << accum.transpose());
        return accum.sum();
    }

    double gamete_likelihood(int ind, const gamete_LV_type& gam, const std::vector<std::string>& marker_names, const std::vector<double>& distances)
    {
        init_tr(gam.is_single, distances);
        if (gam.is_single) {
            Eigen::Vector2d accum;
            return gamete_likelihood_impl(tr2, accum, marker_names, gam);
        } else {
            Eigen::Vector4d accum;
            return gamete_likelihood_impl(tr4, accum, marker_names, gam);
        }
    }
    
    double
    map_likelihood(const std::vector<std::string>& marker_names, const std::vector<double>& distances)
    {
        if (marker_names.size() != distances.size() + 1) {
            MSG_ERROR("Need to have exactly one more marker name than the number of inter-marker distances", "");
            return NAN;
        }
        double ret = 0;
        for (const auto& kv: data) {
            ret += log(gamete_likelihood(kv.first, kv.second, marker_names, distances));
        }
        return ret;
    }

    struct EM_computer_type : public EM_helpers {
        gamete_LV_database* db;
        std::vector<double> em_distances;
        std::vector<double> em_old_distances;
        std::vector<Eigen::Matrix2d> em_TR2;
        std::vector<Eigen::Matrix4d> em_TR4;
        bool em_have_single = false;
        bool em_have_double = false;
        std::vector<std::vector<gamete_lv_value_type>> em_state_per_mark_per_ind;
        std::vector<std::vector<gamete_lv_value_type>> em_forward, em_backward, em_obs;
        double em_likelihood;
        std::vector<bool> em_chain_is_single;

        EM_computer_type(gamete_LV_database* ptr)
            : db(ptr)
            , em_distances(), em_old_distances(), em_TR2(), em_TR4()
            , em_have_single(false), em_have_double(false)
            , em_state_per_mark_per_ind(), em_forward(), em_backward(), em_obs()
            , em_likelihood(0), em_chain_is_single(false)
        {}

        void
        EM_init(const std::vector<std::string>& marker_names)
        {
            em_distances.clear();
            em_distances.resize(marker_names.size() - 1, .5);
            em_old_distances.clear();
            em_old_distances.resize(marker_names.size() - 1, 0);
            
            em_chain_is_single.clear();
            em_chain_is_single.reserve(db->data.size());
            em_state_per_mark_per_ind.resize(marker_names.size());
            auto spmpi = em_state_per_mark_per_ind.begin();
            em_forward.clear();
            em_forward.resize(marker_names.size());
            em_backward.clear();
            em_backward.resize(marker_names.size());
            auto forwardi = em_forward.begin();
            auto backwardi = em_backward.begin();
    //         MSG_DEBUG("forward " << em_forward);
    //         MSG_DEBUG("backward " << em_backward);
    //         size_t i = 0;
            //for (const auto& m: marker_names) {
            for (size_t i = marker_names.size(); i!=0; --i) { // avoid "unused variable m warnings"
                (spmpi++)->resize(db->data.size());
                (forwardi++)->resize(db->data.size(), gamete_lv_value_type::unknown_binary());
                (backwardi++)->resize(db->data.size(), gamete_lv_value_type::unknown_binary());
            }
    //         MSG_DEBUG("data");
    //         MSG_DEBUG(data);
            for (const auto& kv: db->data) {  // all markers for one individual
                em_chain_is_single.push_back(kv.second.is_single);
                em_have_single |= kv.second.is_single;
                em_have_double |= !kv.second.is_single;
    //             auto mi = marker_names.begin();
    //             MSG_DEBUG("marker_names " << marker_names);
    //             if (em_chain_is_single.back()) {
    //                 for (auto& vec: *spmpi) {
    //                     vec.unary = kv.second.lv.find(*mi++)->second.unary;
    //                 }
    //             } else {
    //                 for (auto& vec: *spmpi) {
    //                     vec.binary = kv.second.lv.find(*mi++)->second.binary;
    //                 }
    //             }
    //             ++i;
            }

            auto di = db->data.begin();
            for (size_t i = 0; i < db->data.size(); ++i, ++di) {
                size_t mi = 0;
                for (const auto& m: marker_names) {
                    auto dii = di->second.lv.find(m);
                    if (dii == di->second.lv.end()) {
                        std::string msg("Unknown marker ");
                        msg += m;
                        throw std::runtime_error(msg);
                    }
                    em_state_per_mark_per_ind[mi][i].binary = dii->second.binary;
                    ++mi;
                }
            }

            if (em_have_double) {
                em_TR4.resize(em_distances.size());
            }
            if (em_have_single) {
                em_TR2.resize(em_distances.size());
            }

            em_obs = em_state_per_mark_per_ind;

    //         MSG_DEBUG("data");
    //         MSG_DEBUG(data);
    //         
    //         MSG_DEBUG("em_state_per_mark_per_ind");
    //         MSG_DEBUG(em_state_per_mark_per_ind);
    //         MSG_DEBUG("Have single? " << std::boolalpha << em_have_single);
    //         MSG_DEBUG("Have double? " << std::boolalpha << em_have_double);
            
            /*EM_update_distances();*/
        }
        
        void
        EM_init_TR()
        {
            if (em_have_double) {
                for (size_t i = 0; i < em_distances.size(); ++i) {
                    em_TR4[i] = db->get_TR_binary(em_distances[i], true);
                }
            }
            if (em_have_single) {
                for (size_t i = 0; i < em_distances.size(); ++i) {
                    em_TR2[i] = db->get_TR_unary(em_distances[i], true);
                }
            }
        }

        template <typename TYPE>
        double
        norm(TYPE&& m_or_v, double& sum)
        {
            sum = m_or_v.sum();
            if (sum > 0) {
                sum = 1. / sum;
                m_or_v *= sum;
                return sum;
            }
            return std::numeric_limits<double>::signaling_NaN();
        }

        template <typename TYPE>
        double
        norm(TYPE&& m_or_v)
        {
            double sum;
            return norm(m_or_v, sum);
        }

        template <typename VTYPE>
        typename std::decay<VTYPE>::type
        mult_and_norm(VTYPE&& a, VTYPE&& b)
        {
            typename std::decay<VTYPE>::type ret = (a.array() * b.array()).matrix();
            norm(ret);
            return ret;
        }

        template <typename VTYPE>
        typename std::decay<VTYPE>::type
        mult_and_norm(VTYPE&& a, VTYPE&& b, VTYPE&& c)
        {
            typename std::decay<VTYPE>::type ret = (a.array() * b.array() * c.array()).matrix();
            norm(ret);
            return ret;
        }

        void
        EM_update_gamete_prob()
        {
            scoped_indent _("[EM update state] ");
            EM_init_TR();

            Eigen::Vector4d v4;
            Eigen::Vector2d v2;

            for (size_t i = 0; i < em_chain_is_single.size(); ++i) {
                if (em_chain_is_single[i]) {
                    size_t m = 0;
                    em_forward[m][i].unary = Eigen::Vector2d::Constant(.5);
                    // forward.
                    for (; m < em_distances.size(); ++m) {
                        double norm_accum;
                        em_forward[m + 1][i].unary = em_TR2[m] * mult_and_norm(em_forward[m][i].unary, em_obs[m][i].unary);
                        norm(em_forward[m + 1][i].unary, norm_accum);
                    }
                    // backward.
                    em_backward[m][i].unary = Eigen::Vector2d::Constant(.5);
                    for (; m > 0; --m) {
                        em_backward[m - 1][i].unary = em_TR2[m - 1] * mult_and_norm(em_backward[m][i].unary, em_obs[m][i].unary);
                        norm(em_backward[m - 1][i].unary);
                    }
                    // forward * backward * obs then normalize.
                    for (; m < em_state_per_mark_per_ind.size(); ++m) {
                        em_state_per_mark_per_ind[m][i].unary = mult_and_norm(em_forward[m][i].unary, em_backward[m][i].unary, em_obs[m][i].unary);
                    }
                } else {
                    size_t m = 0;
                    em_forward[m][i].binary = Eigen::Vector4d::Constant(.25);
                    // forward.
                    for (; m < em_distances.size(); ++m) {
                        em_forward[m + 1][i].binary = em_TR4[m] * mult_and_norm(em_forward[m][i].binary, em_obs[m][i].binary);
                        norm(em_forward[m + 1][i].binary);
                    }
                    // backward.
                    em_backward[m][i].binary = Eigen::Vector4d::Constant(.25);
                    for (; m > 0; --m) {
                        em_backward[m - 1][i].binary = em_TR4[m - 1] * mult_and_norm(em_backward[m][i].binary, em_obs[m][i].binary);
                        norm(em_backward[m - 1][i].binary);
                    }
                    // forward * backward * obs then normalize.
                    for (; m < em_state_per_mark_per_ind.size(); ++m) {
                        em_state_per_mark_per_ind[m][i].binary = mult_and_norm(em_forward[m][i].binary, em_backward[m][i].binary, em_obs[m][i].binary);
                    }
                }
            }
            
    //         MSG_DEBUG("em_forward");
    //         MSG_DEBUG(em_forward);
    //         MSG_DEBUG("em_backward");
    //         MSG_DEBUG(em_backward);
    //         MSG_DEBUG("em_obs");
    //         MSG_DEBUG(em_obs);
    //         MSG_DEBUG("em_state_per_mark_per_ind");
    //         MSG_DEBUG(em_state_per_mark_per_ind);
        }

        double
        EM_forward_likelihood()
        {
            scoped_indent _("[EM forward likelihood] ");
            double accum = 0;

            for (size_t i = 0; i < em_chain_is_single.size(); ++i) {
                if (em_chain_is_single[i]) {
                    size_t m = 0;
                    Eigen::Vector2d v2 = Eigen::Vector2d::Constant(1);
                    // forward.
                    for (; m < em_distances.size(); ++m) {
                        v2 = em_TR2[m] * (v2.array() * em_obs[m][i].unary.array()).matrix();
                    }
                    v2 = (v2.array() * em_obs[m][i].unary.array()).matrix();
                    accum += log(v2.sum());
                } else {
                    size_t m = 0;
                    Eigen::Vector4d v4 = Eigen::Vector4d::Constant(1);
                    // forward.
                    for (; m < em_distances.size(); ++m) {
                        v4 = em_TR4[m] * (v4.array() * em_obs[m][i].binary.array()).matrix();
                    }
                    v4 = (v4.array() * em_obs[m][i].binary.array()).matrix();
                    accum += log(v4.sum());
                }
            }
            return accum;
        }

        template <typename M>
        double
        L1(const M& m)
        {
            double d = m.sum();
            return d < DBL_EPSILON ? 1. / d : 1.;
        }
        
        double
        twoMarkerLikelihoodAtInf()
        {
            //return twoMarkerLikelihood(0, EM_R_MAX);
            //// ADRIEN VIDAL : Threshold for the theorical recombination rate for infinitesimal linkage.
            return twoMarkerLikelihood(0, EM_R_INF); //HAXX
            auto o1 = em_obs[0].begin();
            auto done = em_obs[0].end();
            auto o2 = em_obs[1].begin();
            auto sing = em_chain_is_single.begin();
            double accum = 0;
            for (; o1 != done; ++sing/*, ++m1, ++m2*/, ++o1, ++o2) {
                if (*sing) {
    //                 accum += log(o1->unary.transpose() * L1(o1->unary) * Eigen::Matrix2d::Constant(.5) * o2->unary);
    //                 MSG_INFO("sing " << o1->unary.transpose() << " ; " << o2->unary.transpose());
                    accum += log(.5 * o1->unary.sum() * o2->unary.sum());
                } else {
    //                 accum += log(o1->binary.transpose() * L1(o1->binary) * Eigen::Matrix4d::Constant(.25) * o2->binary);
    //                 MSG_INFO("doub " << o1->binary.transpose() << " ; " << o2->binary.transpose());
                    accum += log(.25 * o1->binary.sum() * o2->binary.sum());
                }
            }
            return accum;
        }

        double
        twoMarkerLikelihood(size_t i, double r)
        {
            auto m1 = em_forward[i].begin();
            auto o1 = em_obs[i].begin();
            auto m2 = em_backward[i + 1].begin();
            auto o2 = em_obs[i + 1].begin();
            auto sing = em_chain_is_single.begin(), done = em_chain_is_single.end();
            double accum = 0;
            for (; sing != done; ++sing, ++m1, ++m2, ++o1, ++o2) {
                if (*sing) {
    //                 accum += log(mult_and_norm(m1->unary, o1->unary).transpose() * get_TR_unary(r, true) * mult_and_norm(m2->unary, o2->unary));
                    accum += log((2 * m1->unary.array() * o1->unary.array()).matrix().transpose()
                                * db->get_TR_unary(r, true)
                                * (2 * m2->unary.array() * o2->unary.array()).matrix());
    //                 MSG_INFO("o1 " << o1->unary.transpose());
    //                 MSG_INFO("o2 " << o2->unary.transpose());
                } else {
    //                 Eigen::Vector4d mm1 = mult_and_norm(m1->binary, o1->binary);
    //                 Eigen::Vector4d mm2 = mult_and_norm(m2->binary, o2->binary);
    //                 MSG_INFO("o1 " << o1->binary.transpose());
    //                 MSG_INFO("o2 " << o2->binary.transpose());
                    Eigen::Vector4d mm1 = 4 * (m1->binary.array() * o1->binary.array()).matrix();
                    Eigen::Vector4d mm2 = 4 * (m2->binary.array() * o2->binary.array()).matrix();
                    /*MSG_DEBUG("mm1 " << mm1.transpose());*/
                    /*MSG_DEBUG("mm2 " << mm2.transpose());*/
                    accum += log(mm1.transpose() * db->get_TR_binary(r, true) * mm2);
                }
            }
            return accum;
        }

        double
        compute_2pt_dist(size_t i) // NOTE: This is actually the 2pt recombination rate...
        {
            using boost::math::tools::brent_find_minima;
    //         for (double r = 0; r < .5; r += .05) {
    //             MSG_DEBUG("i=" << i << " r=" << r << " log(L)=" << twoMarkerLikelihood(i, r ? r : 0.0001));
    //         }
//             auto opt = find_max([&] (double r) { return twoMarkerLikelihood(i, r); }, 0, EM_R_MAX);
//             return (opt.a + opt.b) * .5;
            //// ADRIEN VIDAL : Threshold raised to allow measuring recombination rates for anti-linkage.
            auto opt = brent_find_minima([&] (double r) { return -twoMarkerLikelihood(i, r); }, 0., EM_R_MAX, 24);
            return opt.first;
            /*double s = 1. - (opt.a + opt.b) * .5;*/
            /*return 100. * S_to_dist(s);*/
            /*
            const auto& m1 = em_state_per_mark_per_ind[i1];
            const auto& m2 = em_state_per_mark_per_ind[i2];
            auto m1i = m1.begin(), m1j = m1.end(), m2i = m2.begin();
            auto single_i = em_chain_is_single.begin();
            double S_accum = 0;
            size_t n_mei = em_chain_is_single.size() + std::count(em_chain_is_single.begin(), em_chain_is_single.end(), false);
            MSG_DEBUG("em_chain_is_single.size " << em_chain_is_single.size());
            for (; m1i != m1j; ++m1i, ++m2i, ++single_i) {
    //             ++n_mei;
                if (*single_i) {
                    S_accum += compute_S(m1i->unary, m2i->unary);
                } else {
                    S_accum += compute_S(m1i->binary, m2i->binary);
                }
                MSG_DEBUG("S_accum " << S_accum << " n_mei " << n_mei);
            }
            double ret = S_to_dist(S_accum / n_mei);
            MSG_DEBUG("computed 2pt dist = " << ret);
            return ret;
            //*/        
        }

        void
        EM_update_distances()
        {
            em_old_distances = em_distances;
    //         MSG_DEBUG("[update dist] old distances " << em_old_distances);
            for (size_t i = 0; i < em_distances.size(); ++i) {
                em_distances[i] = compute_2pt_dist(i);
            }
    //         MSG_DEBUG("[update dist] new distances " << em_distances);
        }
        
        double
        EM_delta()
        {
            double accum = 0;
            auto i = em_distances.begin(), j = em_distances.end(), k = em_old_distances.begin();
            for (; i != j; ++i, ++k) {
                accum += fabs(*i - *k);
            }
    //         MSG_DEBUG("distances old " << em_old_distances << " new " << em_distances << " => " << accum);
            return accum;
        }


        EM_map
        EM(const std::vector<std::string>& marker_names, bool dist_if_true_else_r = true, double convergence_threshold = 1.e-12, int max_iterations = 100)
        {
            double delta;
            EM_map ret;
            ret.n_iterations = 0;
            ret.marker_names = marker_names;
            EM_init(marker_names);
            EM_update_gamete_prob(); // E
            do {
    //             MSG_DEBUG("*** ITERATION ***");
                EM_update_gamete_prob(); // E
                EM_update_distances(); // M
                delta = EM_delta();
                ++ret.n_iterations;
            } while (delta > convergence_threshold && ret.n_iterations < max_iterations);
            ret.r = em_distances;
            ret.distances = em_distances;
            if (dist_if_true_else_r) {
                for (double& d: ret.distances) {
                    d = -50. * log(1 - 2. * d);
                }
            }
    //         ret.likelihood = map_likelihood(marker_names, em_distances);
            ret.likelihood = EM_forward_likelihood();
    //         auto sing = em_chain_is_single.begin();
    //         for (const auto& v: em_forward.back()) {
    //             if (*sing++) {
    //                 ret.likelihood += log(v.unary.sum());
    //             } else {
    //                 ret.likelihood += log(v.binary.sum());
    //             }
    //         }
            ret.converged = delta < convergence_threshold;
            ret.delta = delta;
            return ret;
        }
    };
    
    EM_computer_type EM_computer()
    {
        return {this};
    }
    

    template <typename STREAM_TYPE>
        void
        file_io(STREAM_TYPE& fs)
        {
            rw_base rw;
            if (rw.fourcc(fs, "GMLV")) { return; }
            rw(fs, data);
        }

    static
        gamete_LV_database
        load_from(const std::string& filename)
        {
            // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
            ifile ifs(filename, std::ios_base::in | std::ios_base::binary);
            gamete_LV_database ret;
            ret.file_io(ifs);
            return ret;
        }

    void
        save_to(const std::string& filename)
        {
            // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
            ofile ofs(filename, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);
            file_io(ofs);
        }
};


struct LV_database {
    /* GEN / MARK / IND => LV_vec */
    std::map<std::string, std::map<std::string, std::vector<VectorXd>>> data_by_marker;
    /* CHROM / GEN / IND => LV_mat */
    std::map<std::string, std::map<std::string, std::vector<MatrixXd>>> data;

    LV_database() : data_by_marker(), data() {}

    void assemble_using_map(const std::vector<chromosome>& map)
    {
        data.clear();
        for (const auto& chr: map) {
            for (const auto& gen_dat: data_by_marker) {
                const auto& gen = gen_dat.first;
                const auto& mark_ind_lv = gen_dat.second;
                size_t n_states = mark_ind_lv.begin()->second.front().size();
                size_t n_mark = chr.raw.marker_name.size();
                size_t n_condensed_mark = chr.condensed.marker_name.size();
                size_t n_ind = mark_ind_lv.begin()->second.size();
                auto& ind_LVmat = data[chr.name][gen];
                ind_LVmat.resize(n_ind, {n_states, n_mark});
                /*MSG_DEBUG("[assemble.using.map] #haplo_size=" << chr.haplo_sizes.size());*/
                /*MSG_QUEUE_FLUSH();*/
                for (size_t ind = 0; ind < n_ind; ++ind) {
                    auto& mat = ind_LVmat[ind];
                    mat = MatrixXd::Ones(n_states, n_condensed_mark);
                    size_t m = 0;
                    int haplo_counter = 0;
                    auto haplo_size_i = chr.haplo_sizes.begin();
                    for (const auto& mark: chr.raw.marker_name) {
                        const auto& svec = mark_ind_lv.find(mark)->second;
                        /*MSG_DEBUG("[assemble.using.map] haplo_size=" << (*haplo_size_i) << " column=" << m);*/
                        /*MSG_QUEUE_FLUSH();*/
                        mat.col(m).array() *= svec[ind].array();
                        ++haplo_counter;
                        if (haplo_counter == *haplo_size_i) {
                            ++m;
                            ++haplo_size_i;
                            haplo_counter = 0;
                        }
                    }
                    auto hi = chr.begin();
                    for (int c = 0; c < mat.cols(); ++c, ++hi) {
                        double sum = mat.col(c).sum();
                        if (sum == 0) {
                            /*std::stringstream ss;*/
                            /*ss << "Detected inconsistent observations! Generation is " << gen << ", chromosome " << chr.name << ", individual #" << (ind + 1) << ", involving markers";*/
                            /*auto haplo = *hi;*/
                            /*for (size_t i = haplo.first; i < haplo.second; ++i) {*/
                                /*ss << ' ' << chr.raw.marker_name[i];*/
                            /*}*/
                            /*MSG_WARNING(ss.str());*/
                        } else {
                            mat.col(c) /= sum;
                        }
                    }
                }
            }
        }
    }

    MatrixXd operator () (
            std::vector<std::string>::const_iterator begin,
            std::vector<std::string>::const_iterator end,
            const std::string& gen, size_t ind) const
    {
        const auto& milv = data_by_marker.find(gen)->second;
        size_t n_states = milv.find(*begin)->second.front().size();
        MatrixXd LV(n_states, end - begin);
        for (size_t i = 0; i < n_states; ++i) {
            LV.col(i) = milv.find(*begin)->second[ind];
        }
        return LV;
    }

    MatrixXd& operator () (const std::string& chr, const std::string& gen, size_t ind)
    {
        return data[chr][gen][ind];
    }

    const MatrixXd& operator () (const std::string& chr, const std::string& gen, size_t ind) const
    {
        return data.find(chr)->second.find(gen)->second[ind];
    }

    std::map<std::string, std::vector<MatrixXd>>
        extract(const std::string& gen, const std::vector<size_t> ind_vec) const
        {
            std::map<std::string, std::vector<MatrixXd>> ret;
            for (const auto& chr_gen_lv_vec: data) {
                const std::string& chr = chr_gen_lv_vec.first;
                auto it = chr_gen_lv_vec.second.find(gen);
                if (it == chr_gen_lv_vec.second.end()) {
                    /* whine */
                    MSG_ERROR("Generation " << gen << " doesn't exist!", "Double-check the generation names given in commandline.");
                    continue;
                }
                const auto& lv_vec = it->second;
                auto& ret_lv_vec = ret[chr];
                ret_lv_vec.reserve(ind_vec.size());
                for (size_t i: ind_vec) {
                    ret_lv_vec.push_back(lv_vec[i]);
                }
            }
            return ret;
        }

    template <typename STREAM_TYPE>
        void
        file_io(STREAM_TYPE& fs)
        {
            rw_base rw;
            if (rw.fourcc(fs, "SMLV")) { return; }
            rw(fs, data_by_marker);
            rw(fs, data);
        }

    static
        LV_database
        load_from(const std::string& filename)
        {
            // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
            ifile ifs(filename, std::ios_base::in | std::ios_base::binary);
            LV_database ret;
            ret.file_io(ifs);
            return ret;
        }

    void
        save_to(const std::string& filename)
        {
            // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
            ofile ofs(filename, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);
            file_io(ofs); //// file_io does binary I/O operations
        }

    /*
    static
        bool lv_check_fourcc(ifile& ifs, const char* fourcc)
        {
            if (check_fourcc(ifs, fourcc)) {
                MSG_ERROR("FILE IS NOT A LOCUS VECTOR FILE OR IS CORRUPTED", "You may want to run spell-marker again.");
                return true;
            }
            return false;
        }

    static
        LV_database load_from(const std::string& filename)
        {
            ifile ifs(filename);
            return load_from(ifs);
        }

    static
        LV_database load_from(ifile& ifs)
        {
            LV_database LV;

            if (lv_check_fourcc(ifs, "SMLV")) { return {}; }
            size_t n_chrom = read_size(ifs);
            size_t n_gen = read_size(ifs);
            for (size_t c = 0; c < n_chrom; ++c) {
                if (lv_check_fourcc(ifs, "SCHR")) { return {}; }
                std::string chr_name = read_str(ifs);
                for (size_t g = 0; g < n_gen; ++g) {
                    if (lv_check_fourcc(ifs, "SGEN")) { return {}; }
                    std::string gen_name = read_str(ifs);
                    size_t n_ind = read_size(ifs);
                    LV.data[chr_name][gen_name].resize(n_ind);
                    for (size_t i = 0; i < n_ind; ++i) {
                        if (lv_check_fourcc(ifs, "SLV_")) { return {}; }
                        read_matrix(ifs, LV.data[chr_name][gen_name][i]);
                    }
                }
            }
            return LV;
        }

    void save_to(const std::string& filename)
    {
        ofile ofs(filename);
        save_to(ofs);
    }

    void save_to(ofile& ofs)
    {
        write_fourcc(ofs, "SMLV");
        write_size(ofs, data.size());
        write_size(ofs, data.begin()->second.size());
        for (const auto& chr_gen_vec_lv: data) {
            write_fourcc(ofs, "SCHR");
            write_str(ofs, chr_gen_vec_lv.first);
            for (const auto& gen_vec_lv: chr_gen_vec_lv.second) {
                write_fourcc(ofs, "SGEN");
                write_str(ofs, gen_vec_lv.first);
                write_size(ofs, gen_vec_lv.second.size());
                for (const auto& lv: gen_vec_lv.second) {
                    write_fourcc(ofs, "SLV_");
                    write_matrix(ofs, lv);
                }
            }
        }
    }
    */

    friend
        std::ostream& operator << (std::ostream& os, const LV_database& LV)
        {
            for (const auto& chr_gen_vec_lv: LV.data) {
                os << "CHROMOSOME " << chr_gen_vec_lv.first << std::endl;
                for (const auto& gen_vec_lv: chr_gen_vec_lv.second) {
                    os << "* generation " << gen_vec_lv.first << std::endl;
                    size_t i = 0;
                    for (const auto& lv: gen_vec_lv.second) {
                        os << "  #" << i << std::endl;
                        ++i;
                        os << lv << std::endl;
                    }
                }
            }
            return os;
        }
};


/** **/


template <typename V>
std::vector<V>
filter_vector(const std::vector<V>& vec, const std::vector<size_t>& list)
{
    std::vector<V> ret;
    ret.reserve(list.size());
    for (size_t i: list) {
        ret.push_back(vec[i]);
    }
    return ret;
}


struct qtl_pop_type {
    std::string name;
    std::string qtl_generation_name;
    std::vector<size_t> indices;
    std::shared_ptr<geno_matrix> gen;
    std::map<std::string, std::vector<MatrixXd>> LV;
    std::string observed_traits_filename;
    std::vector<trait> observed_traits;
    std::map<char, std::string> ancestor_names;
    trait covariables;

    qtl_pop_type()
        : name(), qtl_generation_name(), indices(), gen(), LV(), observed_traits_filename(), observed_traits(), covariables()
    {}
    qtl_pop_type(const std::string& n, const std::string& qgn, const std::vector<size_t>& i, std::shared_ptr<geno_matrix> g,
                 const std::map<std::string, std::vector<MatrixXd>>& lv, const std::string& otf, const std::vector<trait>& ot,
                 const std::map<char, std::string>& anam, const trait& covar)
        : name(n), qtl_generation_name(qgn), indices(i), gen(g), LV(lv), observed_traits_filename(otf), observed_traits(ot), ancestor_names(anam), covariables(covar)
    {
        /*MSG_DEBUG("new qtl_pop with " << ancestor_names.size() << " ancestor names");*/
        /*for (const auto& kv: ancestor_names) {*/
            /*MSG_DEBUG(" * " << kv.first << ": " << kv.second);*/
        /*}*/
    }

    std::shared_ptr<qtl_pop_type>
        filter_clone(const trait& this_trait) const
        {
            const std::vector<size_t>& keep = this_trait.good_indices;
            std::shared_ptr<qtl_pop_type> ret = std::make_shared<qtl_pop_type>();
            ret->name = name;
            ret->qtl_generation_name = qtl_generation_name;
            ret->gen = gen;
            ret->observed_traits_filename = observed_traits_filename;
            for (const auto& t: observed_traits) {
                if (t.name == this_trait.name) {
                    ret->observed_traits.emplace_back(t);
                }
            }
            ret->ancestor_names = ancestor_names;
            auto i = indices.begin();
            auto j = indices.end();
            auto filt = keep.begin();
            auto filt_end = keep.end();

            ret->covariables = covariables % this_trait;

            /*MSG_DEBUG("Filtering <" << indices << "> with <" << keep << '>');*/
            std::vector<size_t> keep_indices;
            keep_indices.reserve(keep.size());
            while (filt != filt_end && i != j) {
                if (*i == *filt) {
                    keep_indices.push_back(i - indices.begin());
                    ++i;
                    ++filt;
                } else if (*i < *filt) {
                    ++i;
                } else {
                    ++filt;
                }
            }

            ret->indices = filter_vector(indices, keep_indices);
            /*MSG_DEBUG("Filtered indices " << ret->indices << " (" << ret->indices.size() << ')');*/
            for (const auto& kv: LV) {
                /*MSG_DEBUG("Filtering LV on " << kv.first << ", keeping " << keep_indices.size() << " elements");*/
                ret->LV[kv.first] = filter_vector(kv.second, keep_indices);
            }
            /*MSG_DEBUG("Resulting LV (" << ret->LV.size() << ')');*/
            /*for (const auto& lv: ret->LV) {*/
                /*MSG_DEBUG("** " << lv.first << " (" << lv.second.size() << ')');*/
                /*auto ri = ret->indices.begin();*/
                /*for (const auto& v: lv.second) {*/
                    /*MSG_DEBUG("* " << (*ri++));*/
                    /*MSG_DEBUG("" << v);*/
                /*}*/
            /*}*/

            return ret;
        }

    size_t size() const
    {
        /*return observed_traits.front().values.size();*/
        return indices.size();
    }

    const MatrixXd& get_LV(const std::string& chr, size_t i) const { return LV.find(chr)->second[i]; }
};


/** **/


struct pop_data_type {
    std::string name;
    std::map<std::string, std::string> marker_observation_filenames;
    std::string genetic_map_filename;
    std::string pedigree_filename;
    std::string qtl_generation_name;
    std::vector<std::shared_ptr<geno_matrix>> generations;
    LV_database LV;
    std::map<std::string, std::vector<size_t>> families;
    std::map<size_t, std::shared_ptr<geno_matrix>> gen_by_id;
    std::map<char, std::string> ancestor_names;

    void
        assemble_chromosomes(const std::vector<chromosome>& map)
        {
            LV.assemble_using_map(map);
        }

    void
        set_qtl_generation(const std::string& name)
        {
            qtl_generation_name = name;
        }

    std::string save(const std::string& dir)
    {
//        static const char* forbidden = ":?*/\\";
        std::stringstream filename;
//        for (char c: name) {
//            if (strchr(forbidden, c)) {
//                filename << '_';
//            } else {
//                filename << c;
//            }
//        }
        filename << dir << '/' << name << ".spell-marker.data";
        save_to(filename.str());
        return filename.str();
    }

    void save_to(const std::string& filename)
    {
        // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
        ofile ofs(filename, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);
        write_fourcc(ofs, "SPOP");
        write_str(ofs, name);
        write_size(ofs, marker_observation_filenames.size());
        for (const auto& kv: marker_observation_filenames) {
            write_str(ofs, kv.first);
            write_str(ofs, kv.second);
        }
        write_str(ofs, genetic_map_filename);
        write_str(ofs, pedigree_filename);
        write_str(ofs, qtl_generation_name);
        write_size(ofs, generations.size());
        for (const auto& g: generations) {
            write_fourcc(ofs, "SGTE");
            if (g) {
                write_geno_matrix(ofs, *g);
            } else {
                geno_matrix _;
                write_geno_matrix(ofs, _);
            }
        }
        LV.file_io(ofs);
        write_fourcc(ofs, "SFAM");
        write_size(ofs, families.size());
        for (const auto& kv: families) {
            write_str(ofs, kv.first);
            write_size(ofs, kv.second.size());
            for (size_t i: kv.second) {
                write_size(ofs, i);
            }
        }
        write_fourcc(ofs, "SGBI");
        write_size(ofs, gen_by_id.size());
        for (const auto& kv: gen_by_id) {
            write_size(ofs, kv.first);
            write_size(ofs, std::find(generations.begin(), generations.end(), kv.second) - generations.begin());
        }
        write_fourcc(ofs, "ANAM");
        write_size(ofs, ancestor_names.size());
        for (const auto& kv: ancestor_names) {
            write_char(ofs, kv.first);
            write_str(ofs, kv.second);
        }
    }

    static
        bool pop_check_fourcc(ifile& ifs, const char* fourcc)
        {
            if (check_fourcc(ifs, fourcc)) {
                MSG_ERROR("Could not read FOURCC \"" << fourcc << "\". This is not a valid population data file.", "");
                return true;
            }
            return false;
        }

    static
        pop_data_type* load_from(const std::string& filename)
        {
#define CHECK_4CC(_fourcc_) if (pop_check_fourcc(ifs, _fourcc_)) { return ret; }
            // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
            ifile ifs(filename, std::ios_base::in | std::ios_base::binary);
            pop_data_type* ret = new pop_data_type();
            CHECK_4CC("SPOP");
            ret->name = read_str(ifs);
            size_t n_mof = read_size(ifs);
            for (size_t i = 0; i < n_mof; ++i) {
                std::string k = read_str(ifs);
                std::string v = read_str(ifs);
                ret->marker_observation_filenames[k] = v;
            }
            ret->genetic_map_filename = read_str(ifs);
            ret->pedigree_filename = read_str(ifs);
            ret->qtl_generation_name = read_str(ifs);
            size_t n_gen = read_size(ifs);
            for (size_t i = 0; i < n_gen; ++i) {
                CHECK_4CC("SGTE");
                ret->generations.emplace_back(std::make_shared<geno_matrix>());
                read_geno_matrix(ifs, *ret->generations.back());
            }
            /*ret.LV = LV_database::load_from(ifs);*/
            ret->LV.file_io(ifs);
            CHECK_4CC("SFAM");
            size_t n_fam = read_size(ifs);
            for (size_t f = 0; f < n_fam; ++f) {
                std::string k = read_str(ifs);
                size_t n_ind = read_size(ifs);
                /*auto& fam = ret.families[k];*/
                for (size_t i = 0; i < n_ind; ++i) {
                    ret->families[k].push_back(read_size(ifs));
                }
            }
            CHECK_4CC("SGBI");
            size_t n_gbi = read_size(ifs);
            for (size_t g = 0; g < n_gbi; ++g) {
                size_t k = read_size(ifs);
                ret->gen_by_id[k] = ret->generations[read_size(ifs)];
            }
            CHECK_4CC("ANAM");
            size_t n_an = read_size(ifs);
            for (size_t a = 0; a < n_an; ++a) {
                char c = read_char(ifs);
                ret->ancestor_names[c] = read_str(ifs);
            }
            return ret;
        }

    size_t size() const { return families.find(qtl_generation_name)->second.size(); }

    std::shared_ptr<geno_matrix> get_geno_matrix(const std::string& family, size_t ind) const
    {
        return gen_by_id.find(families.find(family)->second[ind])->second;
    }

    std::map<std::shared_ptr<geno_matrix>, std::vector<size_t>>
        all_qtl_geno_matrix() const
        {
            std::map<std::shared_ptr<geno_matrix>, std::vector<size_t>> ret;
            size_t i = 0;
            for (size_t ind: families.find(qtl_generation_name)->second) {
                ret[gen_by_id.find(ind)->second].push_back(i);
                ++i;
            }
            return ret;
        }

    std::string extract_subpops(std::vector<std::shared_ptr<qtl_pop_type>>& pops, const std::string& traits_filename, const std::vector<trait>& traits,
                                std::map<std::string, std::vector<std::vector<std::shared_ptr<qtl_pop_type>>>>& linked_pops,
                                const trait& covariables)
    {
        auto extract_traits
            = [&] (const std::vector<size_t>& ind_vec)
            {
                std::vector<trait> ret;
                ret.resize(traits.size());
                for (size_t ti = 0; ti < traits.size(); ++ti) {
                    ret[ti].name = traits[ti].name;
                    if (covariables.good_indices.size()) {
                        ret[ti].good_indices = ind_vec % traits[ti].good_indices % covariables.good_indices;
                    } else {
                        ret[ti].good_indices = ind_vec % traits[ti].good_indices;
                    }
                    ret[ti].P = traits[ti].P;
                    ret[ti].raw = reduce_with_good_indices(traits[ti].raw, traits[ti].good_indices, ret[ti].good_indices);
                    ret[ti].values = reduce_with_good_indices(traits[ti].values, traits[ti].good_indices, ret[ti].good_indices);
                    ret[ti].dim_names = traits[ti].dim_names;
                }
                return ret;
            };

        for (const auto& this_trait: traits) {
            linked_pops[this_trait.name].emplace_back();
        }
        auto aqg = all_qtl_geno_matrix();
        for (const auto& kv: aqg) {
            /*MSG_DEBUG("subpop kv.first " << kv.first << " kv.second " << kv.second);*/
            auto indices = kv.second;
            if (covariables.good_indices.size()) {
                indices = indices % covariables.good_indices;
            }
            std::shared_ptr<qtl_pop_type> this_pop = std::make_shared<qtl_pop_type>(name, qtl_generation_name, kv.second, kv.first,
                                                                                    LV.extract(qtl_generation_name, kv.second), traits_filename,
                                                                                    extract_traits(kv.second), ancestor_names, covariables);
            for (const auto& this_trait: traits) {
                auto& lp = linked_pops[this_trait.name];
                auto pop_ptr = this_pop->filter_clone(this_trait);
                lp.back().emplace_back(pop_ptr);
            }
            pops.emplace_back(this_pop);
        }

/*
        linked_pops.emplace_back();
        linked_pops.back().reserve(aqg.size());
        for (const auto& kv: aqg) {
            auto it = 
                pops.insert({name, {
                    name,
                    qtl_generation_name,
                    kv.second,
                    kv.first,
                    LV.extract(qtl_generation_name, kv.second),
                    traits_filename,
                    extract_traits(kv.second)
                }});
            linked_pops.back().push_back(&it->second);
        }
*/
        return name;
    }

    template <typename PRINTABLE>
    static
        void prepend(std::ostream& os, const std::string& pfx, PRINTABLE&& p)
        {
            std::stringstream ss;
            ss << p;
            while (!ss.eof()) {
                std::string line;
                std::getline(ss, line);
                os << pfx << line << std::endl;
            }
        }

    friend
        std::ostream& operator << (std::ostream& os, const pop_data_type& pop_data)
        {
            os << "POPULATION " << pop_data.name << std::endl
               << "| QTL generation: " << pop_data.qtl_generation_name << std::endl
               << "| Marker observations:" << std::endl;
            for (const auto& kv: pop_data.marker_observation_filenames) {
                os << "| - " << kv.first << ": " << kv.second << std::endl;
            }
            os << "| Generation matrices:" << std::endl;
            for (const auto& gen: pop_data.generations) {
                if (!gen) { continue; }
                prepend(os, "|   ", (*gen));
            }
            os << "| Families" << std::endl;
            for (const auto& kv: pop_data.families) {
                os << "| * " << kv.first << ": " << kv.second << std::endl;
            }
            os << "| Computed locus vectors:" << std::endl;
            /*prepend(os, "| ", pop_data.LV);*/
            if (pop_data.LV.data.size()) {
                for (const auto& kv: pop_data.LV.data) {
                    os << "| Chromosome " << kv.first << std::endl;
                    for (const auto& gen_lv: kv.second) {
                        os << "|   Generation " << gen_lv.first << std::endl;
                        /*const auto& family = pop_data.families.find(gen_lv.first)->second;*/
                        for (size_t i = 0; i < gen_lv.second.size(); ++i) {
                            os << "|     #" << i << "  " << pop_data.get_geno_matrix(gen_lv.first, i)->name << std::endl;
                            prepend(os, "|     ", gen_lv.second[i]);
                        }
                    }
                }
            } else {
                for (const auto& kv: pop_data.LV.data_by_marker) {
                    os << "|   Generation " << kv.first << std::endl;
                    /*const auto& family = pop_data.families.find(kv.first)->second;*/
                    for (const auto& mark_lv: kv.second) {
                        os << "| Marker " << mark_lv.first << std::endl;
                        for (size_t i = 0; i < mark_lv.second.size(); ++i) {
                            os << "|     #" << i << std::endl;
                            prepend(os, "|     ", mark_lv.second[i].transpose());
                        }
                    }
                }
            }

            return os;
        }
};




#endif

