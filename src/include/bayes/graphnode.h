/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_BAYES_GRAPH_NODE_H_
#define _SPEL_BAYES_GRAPH_NODE_H_

#include "graphnode_base.h"

#include "../pedigree.h"

struct graph_type {
    node_vec rank;
    node_vec represented_by;
    std::vector<node_type> type;
    std::vector<colour_proxy> colour;
    std::vector<node_vec> neighbours_in;
    std::vector<node_vec> neighbours_out;
    std::vector<node_vec> inner_nodes;
    std::vector<var_vec> rules;
    var_vec variables;
    std::vector<var_vec> node_variables;
    std::map<variable_index_type, VariableIO> io;

    std::map<variable_index_type, node_index_type> interface_to_node;
    std::map<node_index_type, variable_index_type> node_to_interface;

    std::vector<std::shared_ptr<graph_type>> subgraphs;

    std::vector<std::shared_ptr<message_type>> tables;
    /*std::vector<message_type> state;*/
    std::map<var_vec, genotype_comb_type> domains;
    /* TODO suppress joint_parent_domains and all afferent code */
    std::map<var_vec, genotype_comb_type> joint_parent_domains;
    std::map<variable_index_type, char> ancestor_letters;
    /*std::vector<compute_state_operation_type> compute_state_ops;*/

    const graph_type* parent;
    node_index_type index_in_parent;

    bool aggregate_cycles;
    bool generate_interfaces;

    size_t n_alleles;

    /* THIS DOESN'T HAVE  TO BE SAVED/LOADED. TEMPORARY STATE IN ORDER TO COMPUTE THE SEQUENCES OF OPERATIONS. */
    std::vector<var_vec> annotations;

    std::vector<bool> is_dh;

    graph_type& operator = (graph_type&& other) = delete;

    /* node index 0 is the trash bin */

    graph_type(const graph_type& other) = delete;

    graph_type()
        : rank(1), represented_by(1), type(1), colour(1), neighbours_in(1), neighbours_out(1), inner_nodes(1), rules(1), variables(1), node_variables(1), io(), interface_to_node(), node_to_interface(), subgraphs(1), tables(1), /*state(1),*/ parent(nullptr), index_in_parent(0),aggregate_cycles(true), generate_interfaces(true), n_alleles(1), annotations(1), is_dh(1, false)
    {}

    graph_type(size_t n_al)
        : rank(1), represented_by(1), type(1), colour(1), neighbours_in(1), neighbours_out(1), inner_nodes(1), rules(1), variables(1), node_variables(1), io(), interface_to_node(), node_to_interface(), subgraphs(1), tables(1), /*state(1),*/ parent(nullptr), index_in_parent(0), aggregate_cycles(true), generate_interfaces(true), n_alleles(n_al), annotations(1), is_dh(1, false)
    {}

    bool is_aggregate(node_index_type node) const { return type[node] == Aggregate/*inner_nodes[node].size() > 1*/; }

    bool
        is_compound_interface(node_index_type node) const
        {
            if (is_aggregate(node)) {
                for (node_index_type n: inner_nodes[node]) {
                    if (type[n] != Interface) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

    bool
        is_interface(node_index_type node) const
        {
            if (is_aggregate(node)) {
                for (node_index_type n: inner_nodes[node]) {
                    if (type[n] != Interface) {
                        return false;
                    }
                }
                return true;
            }
            return type[node] == Interface;
        }

    bool
        is_computable(node_index_type node) const
        {
            return !is_interface(node);
        }

    size_t size() const { return rank.size(); }

    node_vec
        active_nodes() const
        {
            node_vec ret;
            ret.reserve(represented_by.size());
            for (node_index_type i = 1; i < represented_by.size(); ++i) {
                if (represented_by[i] == i) {
                    ret.push_back(i);
                }
            }
            return ret;
        }

    node_vec
        resolve_vector(const node_vec& vec) const
        {
            node_vec ret;
            ret.reserve(vec.size());
            for (node_index_type i: vec) {
                node_index_type r = resolve(i);
                if (r > 0) {
                    ret.push_back(r);
                }
            }
            sort_and_unique(ret);
            return ret;
        }

    var_vec
        interface_nodes(var_vec inputs) const
        {
            var_vec ret;
            for (variable_index_type v: inputs) {
                ret.push_back(resolve(interface_to_node.find(v)->second));
            }
            sort_and_unique(ret);
            return ret;
        }

    std::vector<edge_type>
        active_edges() const
        {
            std::vector<edge_type> ret;
            for (node_index_type n: active_nodes()) {
                for (node_index_type o: nei_out(n)) {
                    ret.emplace_back(this, n, o);
                }
            }
            return ret;
        }

    node_vec
        nei_in(node_index_type n) const
        {
            return resolve_vector(neighbours_in[n]);
        }

    node_vec
        nei_out(node_index_type n) const
        {
            return resolve_vector(neighbours_out[n]);
        }

    node_vec
        all_nei(node_index_type n) const
        {
            return nei_in(n) + nei_out(n);
        }

    void
        remove_link(node_index_type nin, node_index_type nout)
        {
            neighbours_out[nin] = resolve_vector(neighbours_out[nin]) - node_vec{nout};
            neighbours_in[nout] = resolve_vector(neighbours_in[nout]) - node_vec{nin};
        }

    void
        dump_node(node_index_type n)
        {
            MSG_DEBUG(
                   '[' << rank[n] << "] " << (is_interface(n) ? "INTERFACE " : (n == inner_nodes[n][0] ? "FACTOR " : "AGGREGATE ")) << n << std::endl
                << "  creation rule " << rules[n] << std::endl
                << "  represented by " << represented_by[n] << " (" << resolve(n) << ')' << std::endl
                << "  colour " << get_colour_impl(colour[n]) << std::endl
                << "  inputs " << neighbours_in[n] << " (" << resolve_vector(neighbours_in[n]) << ')' << std::endl
                << "  outputs " << neighbours_out[n] << " (" << resolve_vector(neighbours_out[n]) << ')' << std::endl
                << "  inner nodes " << inner_nodes[n] << std::endl
                << "  variable(s) " << variables_of(n) << std::endl
                );
            if (node_domains.size() == rank.size()) {
                MSG_DEBUG("  TABLE " << node_domains[n] << std::endl);
            }
            if (inner_nodes[n].size() > 1) {
                if (subgraphs[n]) {
                    scoped_indent _("  | ");
                    subgraphs[n]->dump_active();
                }
            }
            MSG_DEBUG("");
        }

#define DUMP_SZ(_x) << " * " #_x " " << _x.size() << std::endl

    void
        dump_sizes() const
        {
            MSG_DEBUG(""
                DUMP_SZ(rank)
                DUMP_SZ(type)
                DUMP_SZ(rules)
                DUMP_SZ(colour)
                DUMP_SZ(variables)
                DUMP_SZ(inner_nodes)
                DUMP_SZ(neighbours_in)
                DUMP_SZ(neighbours_out)
                DUMP_SZ(represented_by)
                );
        }

    void
        dump()
        {
            MSG_DEBUG("ALL NODES");
            /*dump_sizes();*/
            for (node_index_type i = 1; i < rank.size(); ++i) {
                dump_node(i);
            }
        }

    void
        dump_active()
        {
            MSG_DEBUG("ACTIVE NODES");
            /*dump_sizes();*/
            for (node_index_type i: active_nodes()) {
                dump_node(i);
            }
        }

    void
        compute_ranks()
        {
            std::vector<bool> visited(rank.size(), false);
            compute_ranks(active_nodes(), visited);
        }

    void
        compute_ranks(const node_vec& nodes, std::vector<bool>& visited)
        {
            for (node_index_type n: nodes) {
                if (visited[n]) { continue; }
                visited[n] = true;
                auto nin = nei_in(n);
                compute_ranks(nin, visited);
                rank[n] = 0;
                if (nin.size()) {
                    for (node_index_type i: nin) {
                        if (rank[n] < rank[i]) {
                            rank[n] = rank[n];
                        }
                    }
                    ++rank[n];
                }
            }
        }

    typedef std::pair<size_t, var_vec> emitter_and_interface_type;
    struct compare_eai {
        bool operator () (const emitter_and_interface_type& e1, const emitter_and_interface_type& e2) const { return e1.first < e2.first || (e1.first == e2.first && e1.second < e2.second); }
    };

    typedef std::map<emitter_and_interface_type, size_t, compare_eai> interface_map_type;

    node_index_type
        create_interface(const var_vec& varset)
        {
            node_index_type i;
            if (varset.size() == 1) {
                i = add_interface(node_vec{}, varset.front());
                colour[i] = create_colour();
            } else {
                node_vec iv;
                iv.reserve(varset.size());
                for (variable_index_type v: varset) {
                    iv.push_back(add_interface(node_vec{}, v));
                }
                i = add_node(node_vec{}, node_vec{}, var_vec{}, create_colour(), Aggregate, iv, -1);
                for (node_index_type ni: iv) {
                    represented_by[ni] = i;
                    colour[ni] = colour[i];
                }
            }
            return i;
        }

    node_index_type
        create_interface_between(node_index_type n1, node_index_type n2)
        {
            auto varset = variables_of(n1) % variables_of(n2);
            node_index_type i = create_interface(varset);
            colour[i] = get_colour_impl(colour[n1]);
            neighbours_in[i].push_back(n1);
            neighbours_out[i].push_back(n2);
            return i;
        }

    void
        rebuild_interface_between(node_index_type n1, node_index_type n2, interface_map_type& interface_map)
        {
            auto varset = variables_of(n1) % variables_of(n2);
            node_index_type& i = interface_map[{n1, varset}];
            if (i == 0) {
                i = create_interface_between(n1, n2);
            } else {
                neighbours_out[i] = neighbours_out[i] + node_vec{n2};
            }
            neighbours_in[n2] = nei_in(n2) + node_vec{i} - nei_in(i);
            neighbours_out[n1] = nei_out(n1) + node_vec{i} - nei_out(i);
            remove_link(n1, n2);
            /*filter_out_and_replace_by(neighbours_out[n1], node_vec{n2}, i);*/
            /*filter_out_and_replace_by(neighbours_in[n2], node_vec{n1}, i);*/
            /*MSG_DEBUG("REBUILD " << n1 << ' ' << i << ' ' << n2);*/
            /*dump_node(n1);*/
            /*dump_node(i);*/
            /*dump_node(n2);*/
        }

    void
        remove_nei(node_vec& neighbours, node_index_type n)
        {
            node_vec new_nei;
            new_nei.reserve(neighbours.size());
            auto N = resolve(n);
            for (auto x: resolve_vector(neighbours)) {
                if (x != N) {
                    new_nei.push_back(x);
                }
            }
            new_nei.swap(neighbours);
        }

    bool
        suppress_all_cycles()
        {
            bool redo;
            bool redone = false;
            do {
                redo = false;
                for (node_index_type n: active_nodes()) {
                    auto nei = nei_in(n);
                    /*MSG_DEBUG("LOOK FOR CYCLES FROM NEIGHBOURS OF " << n << "   " << nei);*/
                    for (auto i = nei.begin(), j = nei.end(); i != j; ++i) {
                        std::list<node_index_type> path;
                        for (auto k = i + 1; k != j; ++k) {
                            path = find_path_between_parents(*i, *k, n);
                            if (path.size()) {
                                /*MSG_DEBUG("FOUND BETWEEN " << (*i) << " AND " << (*k) << ": " << path);*/
                                /*for (node_index_type x: path) {*/
                                    /*dump_node(x);*/
                                /*}*/
                                break;
                            }
                        }
                        if (path.size()) {
                            MSG_DEBUG("FOUND CYCLE ENDING ON " << n << " === " << path);
                            path.push_back(n);
                            MSG_DEBUG("path to aggregate " << path);
                            aggregate_path(path);
                            redo = true;
                            redone = true;
                            break;
                        }
                    }
                }
            } while (redo);
            return redone;
        }

    void
        finalize_stage1(bool reconstruct_itf=true)
        {
            /*check_neighbours();*/
            debug_graph("finalize", 10);
            /*scoped_indent _("[stage1] ");*/
            /* search for cycles and aggregate them */
            interface_map_type interface_map;
            if (reconstruct_itf) {
                /*dump_active();*/
                auto edges = active_edges();
                for (const auto& e: edges) {
                    if (is_interface(e.first) ^ !is_interface(e.second)) {
                        /* By construction, it's a factor->factor edge, never an interface->interface edge */
                        rebuild_interface_between(e.first, e.second, interface_map);
                    }
                }
            }
            /*check_neighbours();*/
            debug_graph("finalize", 11);
            suppress_all_cycles();
            /*check_neighbours();*/
            debug_graph("finalize", 12);

            if (size()) {
                node_vec A = active_nodes();

                for (node_index_type n: A) {
                    if (is_aggregate(n) && !is_interface(n)) {
                        /*MSG_DEBUG("subgraphs.size() = " << subgraphs.size() << " n = " << n);*/
                        subgraphs[n] = subgraph(n);
                        /* protect all interfaced variables with this level */
                        var_vec var;
                        for (node_index_type ni: nei_in(n)) {
                            auto nv = variables_of(ni);
                            var.insert(var.end(), nv.begin(), nv.end());
                        }
                        for (node_index_type no: nei_out(n)) {
                            auto nv = variables_of(no);
                            var.insert(var.end(), nv.begin(), nv.end());
                        }
                        sort_and_unique(var);
                        for (auto v: var) {
                            subgraphs[n]->io[v] = Input | Output;
                        }
                    }
                    /* then optimize */
                    /*subgraphs[n]->optimize();*/
                }
                debug_graph("finalize", 13);
            }
        }

    void
        finalize_stage2(bool reconstruct_itf)
        {
            if (reconstruct_itf) {
                /* Create interfaces for all variables used in this layer not already represented in a top-level interface */
                var_vec all_variables;
                std::vector<bool> var_represented(1 + *std::max_element(variables.begin(), variables.end()), false);
                node_vec A = active_nodes();
                for (node_index_type n: A) {
                    if (is_interface(n)) {
                        for (variable_index_type v: variables_of(n)) {
                            var_represented[v] = true;
                        }
                    } else if (is_aggregate(n) && nei_in(n).size() == 0) {
                        var_vec vv;
                        for (node_index_type subn: subgraphs[n]->active_nodes()) {
                            if (subgraphs[n]->is_interface(subn) && subgraphs[n]->nei_in(subn).size() == 0) {
                                vv = vv + subgraphs[n]->variables_of(subn);
                            }
                        }
                        if (vv.size()) {
                            node_index_type i = create_interface(vv);
                            neighbours_out[i].push_back(n);
                            neighbours_in[n] = nei_in(n) + node_vec{i};
                            colour[i] = get_colour_impl(colour[n]);
                            for (variable_index_type v: vv) {
                                var_represented[v] = true;
                            }
                        }
                    }
                }
                debug_graph("finalize", 21);

                /* Discover input interfaces in order to merge distinct trees */
                /* Actually we need to discover their neighbours so we can sort-unique them by colours */
                if (parent != NULL) {
                    node_vec itf_nei;
                    for (node_index_type n: A) {
                        if (is_interface(n) && nei_in(n).size() == 0) {
                            itf_nei = itf_nei + nei_out(n);
                        } else if (!is_interface(n)) {
                            for (variable_index_type v: variables_of(n)) {
                                if (var_represented[v]) { continue; }
                                node_index_type i = add_interface(node_vec{n}, v);
                                nei_out(n).push_back(i);
                            }
                        }
                    }
                    std::sort(itf_nei.begin(), itf_nei.end(), [this] (node_index_type a, node_index_type b) { return colour[a] < colour[b]; });
                    itf_nei.erase(std::unique(itf_nei.begin(), itf_nei.end(), [this] (node_index_type a, node_index_type b) { return colour_equal(colour[a], colour[b]); }), itf_nei.end());

                    node_vec itf;
                    node_vec out, inner;
                    for (node_index_type nei: itf_nei) {
                        for (node_index_type n: nei_in(nei)) {
                            if (nei_in(n).size() == 0) {
                                itf.push_back(n);
                                out = out + nei_out(n);
                                inner = inner + inner_nodes[n];
                            }
                        }
                    }

                    /* now also add single interfaces that have no neighbours */

                    for (node_index_type i: A) {
                        if (all_nei(i).size() == 0) {
                            itf.push_back(i);
                            inner = inner + inner_nodes[i];
                        }
                    }
                    std::sort(itf.begin(), itf.end());
                    std::sort(inner.begin(), inner.end());

                    /* Merge inputs of distinct trees to make one single tree */
#if 1
                    /*MSG_DEBUG("Input interfaces " << itf);*/
                    /*for (auto i: itf) {*/
                        /*MSG_DEBUG(" * " << i << " has colour " << get_colour_impl(colour[i]));*/
                    /*}*/
                    /*std::sort(itf.begin(), itf.end(), [this] (node_index_type a, node_index_type b) { return colour[a] < colour[b]; });*/
                    /*itf.erase(std::unique(itf.begin(), itf.end(), [this] (node_index_type a, node_index_type b) { return colour_equal(colour[a], colour[b]); }), itf.end());*/
                    /*MSG_DEBUG("Colour unique input interfaces " << itf);*/
                    /*for (auto i: itf) {*/
                        /*MSG_DEBUG(" * " << i << " has colour " << get_colour_impl(colour[i]));*/
                    /*}*/
                    if (itf.size() > 1) {
                        /* FIXME merge interface IIF factor colours are different. Otherwise this creates a cycle. */
                        node_index_type agr = add_node(node_vec{}, out, var_vec{}, create_colour(), Aggregate, inner, -1);
                        node_vec va = {agr};
                        for (node_index_type i: itf) {
                            represented_by[i] = agr;
                        }
                        for (node_index_type n: out) {
                            neighbours_in[n] = nei_in(n) - itf + va;
                            if (!colour_equal(colour[n], colour[agr])) {
                                assign_colour_impl(colour[n], colour[agr]);
                            }
                        }
                    }
                    debug_graph("finalize", 22);
#endif
                /*} else {*/
                    /* FIXME we shouldn't have to recompute this here */
                    A = active_nodes();
                    for (node_index_type n: A) {
                        if (is_interface(n)) {
                            for (variable_index_type v: variables_of(n)) {
                                var_represented[v] = true;
                            }
                        }
                    }
                    for (node_index_type n: A) {
                        if (!is_interface(n)) {
                            var_vec all_output;
                            for (node_index_type o: nei_out(n)) {
                                all_output = all_output + variables_of(o);
                            }
                            for (variable_index_type v: variables_of(n)) {
                                if (var_represented[v] || std::find(all_output.begin(), all_output.end(), v) == all_output.end()) { continue; }
                                node_index_type i = add_interface(node_vec{n}, v);
                                neighbours_out[n].push_back(i);
                                colour[i] = colour[n];
                            }
                        }
                    }
                    debug_graph("finalize", 23);
                }
            }
            /* unbox single-factor layers */
            for (node_index_type n: active_nodes()) {
                for (node_index_type o: nei_out(n)) {
                    if (!colour_equal(colour[n], colour[o])) {
                        assign_colour_impl(colour[o], colour[n]);
                    }
                }
            }
#if 1
            for (node_index_type n: active_nodes()) {
                if (type[n] == Aggregate && !is_interface(n)) {
                    size_t count_factors = 0;
                    node_index_type factor_node = (node_index_type) -1;
                    for (node_index_type i: inner_nodes[n]) {
                        if (type[i] == Factor) {
                            ++count_factors;
                            factor_node = i;
                        }
                    }
                    MSG_DEBUG("Aggregate #" << n << " has " << count_factors << " factors");
                    if (count_factors == 1) {
                        represented_by[factor_node] = factor_node;
                        neighbours_in[factor_node] = nei_in(n);
                        neighbours_out[factor_node] = nei_out(n);
                        represented_by[n] = factor_node;
                        subgraphs[n].reset();
                        for (node_index_type i: nei_in(n)) {
                            neighbours_out[i] = nei_out(i);
                        }
                        for (node_index_type o: nei_out(n)) {
                            neighbours_in[o] = nei_in(o);
                        }
                    }
                }
            }
#endif
        }

    void
        finalize_import_inputs()
        {
            for (node_index_type n: active_nodes()) {
                if (is_aggregate(n) && !is_interface(n)) {
                    std::vector<var_vec> inputs;
                    for (node_index_type i: nei_in(n)) {
                        inputs.push_back(variables_of(i));
                    }
                }
            }
        }

    void
        finalize(bool reconstruct_itf=true)
        {
            scoped_indent _("[finalize] ");
            finalize_stage1(reconstruct_itf);
            finalize_stage2(reconstruct_itf);
            update_all_ranks();
            /*if (reconstruct_itf) {*/
                /*finalize_import_inputs();*/
            /*}*/
        }

    node_index_type
        add_node(const node_vec& in, const node_vec& out,
                 const var_vec& rule, colour_proxy col, node_type t,
                 const node_vec& inner, variable_index_type var)
        {
            node_index_type ret = rank.size();
            /*MSG_DEBUG("adding node " << ret);*/
            neighbours_out.emplace_back(out);
            neighbours_in.emplace_back(in);
            rules.emplace_back(rule);
            colour.emplace_back(col);
            if (in.size()) {
                size_t r = 0;
                for (node_index_type i: in) {
                    r = std::max(r, rank[i]);
                }
                rank.push_back(r + 1);
            } else {
                rank.push_back(0);
            }
            /*MSG_DEBUG("rank=" << rank.back());*/
            type.push_back(t);
            variables.push_back(var);
            node_variables.emplace_back();
            represented_by.push_back(ret);
            subgraphs.emplace_back();
            tables.emplace_back();
            /*state.emplace_back();*/
            annotations.emplace_back();
            is_dh.push_back(false);
            if (inner.size()) {
                inner_nodes.emplace_back(inner);
            } else {
                inner_nodes.emplace_back(node_vec{ret});
            }
            /*dump();*/
            return ret;
        }

    node_index_type
        resolve_interface(variable_index_type var)
        {
            auto it = interface_to_node.find(var);
            if (it == interface_to_node.end()) {
                /*MSG_DEBUG("resolve_interface(" << var << ") => new interface");*/
                if (generate_interfaces) {
                    return add_interface(node_vec{}, var);
                } else {
                    return (node_index_type) -1;
                }
            }
            /*MSG_DEBUG("resolve_interface(" << var << ") => resolve(" << it->second << ") = " << resolve(it->second));*/
            return resolve(it->second);
        }

    node_index_type
        add_interface(const node_vec& producer, variable_index_type var, bool force=false)
        {
            node_index_type ret = add_node(producer, node_vec{}, var_vec{}, create_colour(), Interface, node_vec{}, var);
            if (force || interface_to_node.find(var) == interface_to_node.end()) {
                interface_to_node[var] = ret;
                node_to_interface[ret] = var;
            }
            for (node_index_type p: producer) {
                neighbours_out[p].push_back(ret);
            }
            return ret;
        }

    node_index_type
        add_factor(const var_vec& rule, colour_proxy col,
                 variable_index_type var)
        {
            node_vec in, out;
            /*
            if (generate_interfaces) {
                for (variable_index_type v: rule) {
                    in.push_back(resolve_interface(v));
                }
            } else {
                for (variable_index_type v: rule) {
                    auto it = interface_to_node.find(v);
                    if (it != interface_to_node.end()) {
                        in.push_back(resolve(it->second));
                    }
                }
            }
            sort_and_unique(in);
            */
            node_index_type ret = add_node(in, node_vec{}, rule, col, Factor, node_vec{}, var);
            for (node_index_type n: in) {
                neighbours_out[n].push_back(ret);
            }
            if (generate_interfaces) {
                node_index_type i = add_node(node_vec{ret}, node_vec{}, var_vec{}, colour[ret], Interface, node_vec{}, var);
                interface_to_node[var] = i;
                node_to_interface[i] = var;
                neighbours_out[ret].push_back(i);
            } else {
                interface_to_node[var] = ret;
                node_to_interface[ret] = var;
            }
            return ret;
        }

    node_index_type
        add_factor(variable_index_type var)
        {
            /*MSG_DEBUG("add_factor(" << var << ')');*/
            node_index_type ret = add_factor(var_vec{}, create_colour(), var);
            /*dump();*/
            return ret;
        }

    node_index_type
        resolve(node_index_type n) const
        {
            while (n != represented_by[n]) { n = represented_by[n]; }
            return n;
        }

    node_index_type
        add_factor(variable_index_type v1, variable_index_type var)
        {
            /*MSG_DEBUG("add_factor(" << v1 << ", " << var << ')');*/
            node_index_type p1r;
            if (!generate_interfaces) {
                auto it = interface_to_node.find(v1);
                if (it == interface_to_node.end()) {
                    auto ret = add_factor(var);
                    rules.back() = {v1};
                    return ret;
                } else {
                    p1r = resolve(it->second);
                }
            } else {
                p1r = resolve_interface(v1);
            }
            node_index_type ret = add_factor(var_vec{v1}, colour[p1r], var);
            neighbours_in[ret] = {p1r};
            neighbours_out[p1r] = {ret};
            compute_ranks();
            /*dump();*/
            return ret;
        }

    void
        aggregate_path(std::list<node_index_type>& path)
        {
            parents_of_max_rank aggr_first;
            while (path.size() > 2 && ((aggr_first = find_parents_of_max_rank(path)), !aggregate(*aggr_first.p1, *aggr_first.p2, *aggr_first.child, path, aggr_first.p1))) {
                path.erase(aggr_first.p1);
                path.erase(aggr_first.p2);
                path.erase(aggr_first.child);
            }
        }

    node_index_type
        add_factor(variable_index_type v1,variable_index_type v2, variable_index_type var)
        {
            /*MSG_DEBUG("add_factor(" << v1 << ", " << v2 << ", " << var << ')');*/
            node_index_type p1r, p2r;
            /* first, search for a factor/aggregate containing both parents */
            var_vec sorted_rule = v1 < v2 ? var_vec{v1, v2} : var_vec{v2, v1};
            bool found = false;
            node_index_type common = 0;
            for (auto n: active_nodes()) {
                if (type[n] != Factor) {
                    continue;
                }
                auto varset = variables_of(n);
                auto result = sorted_rule % varset;
                MSG_DEBUG("searching for rule " << sorted_rule << " in #" << n << ' ' << varset << " => " << result);
                if (result == sorted_rule) {
                    MSG_DEBUG("Found!");
                    found = true;
                    common = n;
                    break;
                }
            }
            if (!generate_interfaces) {
                if (found) {
                    p1r = p2r = common;
                } else {
                    auto it = interface_to_node.find(v1);
                    if (it == interface_to_node.end()) {
                        node_index_type ret = add_factor(v2, var);
                        rules.back() = {v1, v2};
                        return ret;
                    } else {
                        p1r = resolve(it->second);
                    }
                    it = interface_to_node.find(v2);
                    if (it == interface_to_node.end()) {
                        node_index_type ret = add_factor(v1, var);
                        rules.back() = {v1, v2};
                        return ret;
                    } else {
                        p2r = resolve(it->second);
                    }
                }
            } else {
                if (found) {
                    found = false;
                    node_index_type agr;
                    for (auto n: nei_out(common)) {
                        if (sorted_rule % variables_of(n) == sorted_rule) {
                            agr = n;
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        node_vec common_fac = {common};
                        node_index_type i1, i2;
                        i1 = add_interface(common_fac, v1);
                        i2 = add_interface(common_fac, v2);
                        agr = add_node(common_fac, node_vec{}, var_vec{}, colour[common_fac.front()], Aggregate, node_vec{i1, i2}, -1);
                        represented_by[i1] = represented_by[i2] = agr;
                    }
                    p1r = p2r = agr;
                } else {
                    p1r = resolve_interface(v1);
                    p2r = resolve_interface(v2);
                }

                /*p1r = resolve_interface(v1);*/
                /*p2r = resolve_interface(v2);*/
            }

            MSG_DEBUG("p1r=" << p1r << " p2r=" << p2r);

            if (p1r > p2r) {
                p1r ^= p2r;
                p2r ^= p1r;
                p1r ^= p2r;
            }

            node_index_type ret;

            if (p1r != (node_index_type) -1) {
                ret = add_factor(var_vec{v1, v2}, colour[p1r], var);

                bool cycle = false;
                if (p1r != p2r) {
                    neighbours_in[ret] = {p1r, p2r};
                    neighbours_out[p1r].push_back(ret);
                    neighbours_out[p2r].push_back(ret);
                    if (colour_equal(colour[p1r], colour[p2r])) {
                        /* cycle! */
                        MSG_DEBUG("cycle!");
                        cycle = true;
                    } else {
                        MSG_DEBUG("no cycle.");
                        assign_colour_impl(colour[p1r], colour[p2r]);
                    }
                } else {
                    neighbours_in[ret] = {p1r};
                    neighbours_out[p1r].push_back(ret);
                }
                rank[ret] = std::max(rank[p1r], rank[p2r]) + 1;
                dump();
                MSG_QUEUE_FLUSH();

                if (cycle && aggregate_cycles) {
                    MSG_DEBUG("search a path between " << p1r << " and " << p2r);
                    std::vector<bool> visited(rank.size(), false);
                    visited[ret] = true;
                    auto path = find_shortest_path(node_vec{p1r, p2r},
                            [&, this](node_index_type n) { return nei_in(n) + nei_out(n); },
                            [&] (node_index_type n) { return visited[n]; },
                            [&] (node_index_type n) { visited[n] = true; });
                    path.push_back(ret);
                    MSG_DEBUG("New algo path: " << path);
                    path = find_path_between_parents(p1r, p2r, ret);
                    MSG_DEBUG("Old algo path: " << path);
                    /*dump_active();*/
                    MSG_DEBUG("Found path: "; for (size_t n: path) { std::cout << ' ' << n; } std::cout);
                    /*size_t min_rank = 1 + rank[*std::min_element(path.begin(), path.end(), [this](node_index_type i1, node_index_type i2) { return rank[i1] < rank[i2]; })];*/
                    aggregate_path(path);
                }
            } else {
                ret = add_factor(var_vec{v1, v2}, create_colour(), var);
            }

            compute_ranks();
            /*dump();*/
            dump_active();
            return ret;
        }

    void
        add_ancestor(variable_index_type id)
        {
            auto& domain = domains[{id}];
            char letter = ancestor_letters.size() + 'a';
            ancestor_letters[id] = letter;
            for (char al = 0; al < (char) n_alleles; ++al) {
                domain.m_combination.emplace_back(genotype_comb_type::element_type{{{id, {letter, letter, al, al}}}, 1.});
            }
            /*MSG_DEBUG("Domains " << domains);*/
        }

    node_index_type
        add_cross(variable_index_type p1, variable_index_type p2, variable_index_type id)
        {
            /*MSG_DEBUG("################ ADD FACTOR " << p1 << " " << p2 << " " << id);*/
            /*auto factor = compute_factor_table(id, var_vec{p1, p2}, false);*/
            node_index_type ret = add_factor(p1, p2, id);
            /*tables[ret] = factor;*/
            /*state[ret] = *tables[ret];*/
            /*MSG_DEBUG("Computed factor for #" << id << ": " << (*tables[ret]));*/
            /*operations.push_back(op);*/
            return ret;
        }

    node_index_type
        add_dh(variable_index_type p1, variable_index_type id)
        {
            /*auto factor = compute_factor_table(id, var_vec{p1}, true);*/
            node_index_type ret = add_factor(p1, id);
            /*tables[ret] = factor;*/
            /*state[ret] = *tables[ret];*/
            is_dh[ret] = true;
            /*MSG_DEBUG("Computed factor for #" << ret << ": " << (*tables[ret]));*/
            /*operations.push_back(op);*/
            return ret;
        }

    node_index_type
        add_selfing(variable_index_type p1, variable_index_type id)
        {
            /*auto factor = compute_factor_table(id, var_vec{p1}, false);*/
            node_index_type ret = add_factor(p1, id);
            /*tables[ret] = factor;*/
            /*state[ret] = *tables[ret];*/
            /*MSG_DEBUG("Computed factor for #" << ret << ": " << (*tables[ret]));*/
            /*operations.push_back(op);*/
            return ret;
        }

    bool
        find_ascending_path(node_index_type p1, node_index_type p2, node_vec& path, std::vector<bool>& visited)
        {
            /*MSG_DEBUG("path from " << p1 << " to " << p2);*/
            if (p1 == p2) {
                return true;
            }
            for (node_index_type i: nei_in(p1)) {
                i = resolve(i);
                if (visited[i]) {
                    return false;
                }
                visited[i] = true;
                if (find_ascending_path(i, p2, path, visited)) {
                    path.push_back(i);
                    return true;
                }
            }
            return false;
        }

    std::vector<bool>
        create_visited()
        {
            return std::vector<bool>(rank.size(), false);
        }

    node_vec
        find_aggregate_chain(node_index_type p1, node_index_type p2)
        {
            node_vec path;
            auto vis1 = create_visited(), vis2 = create_visited();

            if (rank[p1] > rank[p2] && find_ascending_path(p1, p2, path, vis1)) {
                /*MSG_DEBUG("found ascending path from " << p1 << " to " << p2);*/
                path.push_back(p1);
            } else if (rank[p2] > rank[p1] && find_ascending_path(p2, p1, path, vis2)) {
                /*MSG_DEBUG("found ascending path from " << p2 << " to " << p1);*/
                path.push_back(p2);
            } else {
                /*MSG_DEBUG("no ascending path between parents");*/
                path = {p1, p2};
            }
            return path;
        }

    bool
        filter_out(node_vec& vec, const node_vec& aggr)
        {
            node_vec tmp(vec.size());
            auto it = std::set_difference(vec.begin(), vec.end(), aggr.begin(), aggr.end(), tmp.begin());
            tmp.resize(it - tmp.begin());
            tmp.swap(vec);
            return tmp.size() != vec.size();
        }

    void
        filter_out_and_replace_by(node_vec& vec, const node_vec& aggr, node_index_type new_node)
        {
            /*MSG_DEBUG("filter_out_and_replace_by(" << vec << ", " << aggr << ", " << new_node << ')');*/
            /*if (filter_out(vec, aggr)) {*/
                /*vec.push_back(new_node);*/
            /*}*/
            node_vec tmp;
            tmp.reserve(vec.size());
            for (auto n: vec) {
                node_index_type nr = resolve(n);
                /*MSG_DEBUG("" << nr << " vs " << aggr);*/
                if (std::find(aggr.begin(), aggr.end(), nr) == aggr.end()) {
                    tmp.push_back(n);
                }
            }
            sort_and_unique(tmp);
            if (tmp.size() != vec.size()) {
                tmp.push_back(new_node);
            }
            vec.swap(tmp);
            /*MSG_DEBUG(" => " << vec);*/
        }

    bool
        update_rank(node_index_type node)
        {
            node_index_type r = 0;
            if (nei_in(node).size()) {
                for (node_index_type i: nei_in(node)) {
                    if (rank[i] > r) {
                        r = rank[i];
                    }
                }
                ++r;
            }
            if (rank[node] != r) {
                rank[node] = r;
                return true;
            }
            return false;
        }

    void
        propagate_update_rank(node_index_type source)
        {
            auto nout = nei_out(source);
            std::deque<node_index_type> stack(nout.begin(), nout.end());
            while (stack.size()) {
                node_index_type n = stack.front();
                stack.pop_front();
                if (update_rank(n)) {
                    nout = nei_out(n);
                    stack.insert(stack.end(), nout.begin(), nout.end());
                }
            }
        }

    void
        update_all_ranks()
        {
            std::deque<node_index_type> stack;
            std::vector<bool> visited(rank.size(), false);
            for (node_index_type n: active_nodes()) {
                if (nei_in(n).size() == 0) {
                    rank[n] = 0;
                    auto out = nei_out(n);
                    stack.insert(stack.end(), out.begin(), out.end());
                    visited[n] = true;
                }
            }
            while (stack.size()) {
                node_index_type n = stack.front();
                stack.pop_front();
                if (!visited[n]) {
                    update_rank(n);
                    auto out = nei_out(n);
                    stack.insert(stack.end(), out.begin(), out.end());
                    visited[n] = true;
                }
            }
        }

    void
        check_neighbours()
        {
            for (node_index_type i : active_nodes()) {
                for (node_index_type n: nei_in(i)) {
                    if (represented_by[n] != n) {
                        MSG_DEBUG("error neighbour_in[" << i << "] " << n << " is represented by " << represented_by[n]);
                    }
                    auto tmp = nei_out(n);
                    if (std::find(tmp.begin(), tmp.end(), i) == tmp.end()) {
                        MSG_DEBUG("error neighbour_in[" << i << "] " << n << " doesn't have " << i << " as a neighbour_out " << tmp);
                    }
                }
                for (node_index_type n: nei_out(i)) {
                    if (represented_by[n] != n) {
                        MSG_DEBUG("error neighbour_out[" << i << "] " << n << " is represented by " << represented_by[n]);
                    }
                    auto tmp = nei_in(n);
                    if (std::find(tmp.begin(), tmp.end(), i) == tmp.end()) {
                        MSG_DEBUG("error neighbour_out[" << i << "] " << n << " doesn't have " << i << " as a neighbour_in " << tmp);
                    }
                }
            }
        }

    bool
        recursive_path_finder(node_index_type node, node_index_type goal, std::list<node_index_type>& path, std::vector<bool>& visited) const
        {
            /*MSG_DEBUG("recursive_path_finder(" << node << ", " << goal << ')');*/
            if (node == goal) {
                return true;
            }
            visited[node] = true;
            for (node_index_type n: nei_in(node)) {
                if (!visited[n] && recursive_path_finder(n, goal, path, visited)) {
                    path.push_back(n);
                    return true;
                }
            }
            for (node_index_type n: nei_out(node)) {
                if (!visited[n] && recursive_path_finder(n, goal, path, visited)) {
                    path.push_back(n);
                    return true;
                }
            }
            return false;
        }

    bool
        recursive_path_finder_a(node_index_type node, node_index_type goal, std::list<node_index_type>& path, std::vector<bool>& visited) const
        {
            /*MSG_DEBUG("recursive_path_finder(" << node << ", " << goal << ')');*/
            if (node == goal) {
                return true;
            }
            visited[node] = true;
            for (node_index_type n: nei_in(node)) {
                if (!visited[n] && (recursive_path_finder_a(n, goal, path, visited) || recursive_path_finder_d(n, goal, path, visited))) {
                    path.push_back(n);
                    return true;
                }
            }
            return false;
        }

    bool
        recursive_path_finder_d(node_index_type node, node_index_type goal, std::list<node_index_type>& path, std::vector<bool>& visited) const
        {
            /*MSG_DEBUG("recursive_path_finder(" << node << ", " << goal << ')');*/
            if (node == goal) {
                return true;
            }
            visited[node] = true;
            for (node_index_type n: nei_out(node)) {
                if (!visited[n] && recursive_path_finder_d(n, goal, path, visited)) {
                    path.push_back(n);
                    return true;
                }
            }
            return false;
        }

    std::list<node_index_type>
        find_path_between_parents(node_index_type p1, node_index_type p2, node_index_type child)
        {
            std::vector<bool> visited(rank.size(), false);
            visited[child] = true;
            std::list<node_index_type> path;
            if (recursive_path_finder(p1, p2, path, visited)) {
                path.push_back(p1);
                path.push_back(child);
            }
            return path;
        }

    template <typename GET_NEIGHBOURS, typename VISITED, typename VISIT>
    std::list<node_index_type>
        find_shortest_path(const node_vec& between, GET_NEIGHBOURS&& get_neighbours, VISITED&& visited, VISIT&& visit)
        {
            /*static constexpr node_index_type msb1 = 1 << (8 * sizeof(node_index_type) - 1);*/
            /*static constexpr node_index_type msb2 = msb1 >> 1;*/
            /*static constexpr node_index_type uninitialized = msb1;*/
            /*static constexpr node_index_type starting_point = msb2;*/
#define all_bits ((node_index_type) -1)
#define uninitialized (all_bits ^ (all_bits >> 1))
#define starting_point (uninitialized >> 1)
#define node_is_parent(_n) (from[_n] & starting_point)
#define node_is_initialized(_n) (!(from[_n] & uninitialized))
            /*scoped_indent _(SPELL_STRING("[breadth-first " << between << "] "));*/
            /*MSG_DEBUG("uninitialized " << uninitialized << " starting_point " << starting_point);*/
            node_vec from(rank.size(), uninitialized);
            std::deque<node_index_type> stack;
            for (node_index_type n: between) {
                /*stack.insert(stack.end(), direction[n].begin(), direction[n].end());*/
                stack.push_back(n);
                from[n] = starting_point;
            }
            if (0) {
                std::stringstream ss;
                ss << "from";
                size_t i = 0;
                for (node_index_type f: from) {
                    ss << ' ' << i << ':';
                    if (node_is_parent(f)) { ss << 'P'; } else if (node_is_initialized(i)) { ss << f; } else { ss << '-'; }
                }
                MSG_DEBUG(ss.str());
            }
            while (stack.size()) {
                node_index_type this_node = stack.front();
                stack.pop_front();
                if (visited(this_node)) {
                    continue;
                }
                visit(this_node);
                auto neighbours = get_neighbours(this_node);
                if (0) {
                    std::stringstream ss;
                    ss << "ON " << this_node << ':';
                    if (node_is_parent(this_node)) { ss << 'P'; } else if (node_is_initialized(this_node)) { ss << from[this_node]; } else { ss << '-'; }
                    ss << " neighbours " << neighbours;
                    MSG_DEBUG(ss.str());
                }
                for (node_index_type n: neighbours) {
                    if (visited(n)) {
                        continue;
                    }
                    /*MSG_DEBUG("neighbour " << n);*/
                    if (node_is_parent(n)) {
                        std::list<node_index_type> ret;
                        ret.push_back(n);
                        node_index_type p = this_node;
                        while (!node_is_parent(p)) {
                            ret.push_back(p);
                            /*MSG_DEBUG("building path (1) " << ret);*/
                            p = from[p];
                        }
                        /*MSG_DEBUG("end branch");*/
                        ret.push_back(p);
                        /*MSG_DEBUG("complete path " << ret);*/
                        return ret;
                    } else if (node_is_initialized(n)) {
                        std::list<node_index_type> ret;
                        /* compute path and return */
                        node_index_type p = n;
                        while (!node_is_parent(p)) {
                            ret.push_back(p);
                            /*MSG_DEBUG("building path (1) " << ret);*/
                            p = from[p];
                        }
                        ret.push_back(p);
                        /*MSG_DEBUG("building path (2) " << ret);*/
                        p = this_node;
                        while (!node_is_parent(p)) {
                            ret.push_front(p);
                            /*MSG_DEBUG("building path (3) " << ret);*/
                            p = from[p];
                        }
                        ret.push_front(p);

                        /*MSG_DEBUG("complete path " << ret);*/
                        return ret;
                    }
                    stack.push_back(n);
                    from[n] = this_node;
                }
                if (0) {
                    std::stringstream ss;
                    ss << "from";
                    size_t i = 0;
                    for (node_index_type f: from) {
                        ss << ' ' << i << ':';
                        if (node_is_parent(i)) { ss << 'P'; } else if (node_is_initialized(i)) { ss << f; } else { ss << '-'; }
                        ++i;
                    }
                    MSG_DEBUG(ss.str());
                }
            }
#undef node_is_parent
#undef node_is_initialized
#undef uninitialized
#undef starting_point
            /*MSG_DEBUG("did not find v-path");*/
            return {};
        }

    std::list<node_index_type>
        find_path(node_index_type p1, node_index_type p2) const
        {
            /*scoped_indent _(SPELL_STRING("[find_path " << p1 << ' ' << p2 << "] "));*/
            std::vector<bool> visited(rank.size(), false);
            std::list<node_index_type> path;
            if (recursive_path_finder(p1, p2, path, visited)) {
                path.push_back(p1);
            }
            /*MSG_DEBUG("PATH " << path);*/
            return path;
        }

    node_vec
        find_all_ancestors(node_index_type p, const std::vector<bool>& other_branch, std::vector<bool>& visited) const
        {
            node_vec ret;
            ret.reserve(rank.size());
            node_vec stack(nei_in(p));
            while (stack.size()) {
                node_index_type n = stack.back();
                stack.pop_back();
                if (!visited[n]) {
                    ret.push_back(n);
                    visited[n] = true;
                    if (other_branch[n]) {
                        return ret;
                    }
                    auto nei = nei_in(n);
                    stack.insert(stack.end(), nei.begin(), nei.end());
                }
            }
            return ret;
        }

    node_vec
        find_all_descendants(node_index_type p, const std::vector<bool>& other_branch, std::vector<bool>& visited) const
        {
            node_vec ret;
            ret.reserve(rank.size());
            node_vec stack(nei_out(p));
            while (stack.size()) {
                node_index_type n = stack.back();
                stack.pop_back();
                if (!visited[n]) {
                    ret.push_back(n);
                    visited[n] = true;
                    if (other_branch[n]) {
                        return ret;
                    }
                    auto nei = nei_out(n);
                    stack.insert(stack.end(), nei.begin(), nei.end());
                }
            }
            return ret;
        }

    std::list<node_index_type>
        find_path1(node_index_type p1, node_index_type p2) const
        {
            /*scoped_indent _(SPELL_STRING("[find_path " << p1 << ' ' << p2 << "] "));*/
            std::vector<bool> anc_visited1(rank.size(), false), anc_visited2(rank.size(), false);
            auto a1 = find_all_ancestors(p1, anc_visited2, anc_visited1);
            auto a2 = find_all_ancestors(p2, anc_visited1, anc_visited2);
            /*MSG_DEBUG("a1 " << a1);*/
            /*MSG_DEBUG("a2 " << a2);*/
            auto common = a1 % a2;
            /*MSG_DEBUG("common " << common);*/
            std::list<node_index_type> path;
            if (common.size()) {
                std::vector<bool> visited(rank.size(), true);
                std::vector<bool> targets(rank.size(), true);
                for (node_index_type n: a1 + a2) {
                    visited[n] = false;
                }
                for (node_index_type n: common) {
                    targets[n] = true;
                }
                recursive_path_finder(common.front(), p2, path, visited);
                recursive_path_finder(p1, common.front(), path, visited);
                path.push_back(p1);
                /*MSG_DEBUG("PATH " << path);*/
            }
            return path;
        }

    std::list<node_index_type>
        find_path2(node_index_type p1, node_index_type p2) const
        {
            /*scoped_indent _(SPELL_STRING("[find_path " << p1 << ' ' << p2 << "] "));*/
            std::vector<bool> visited(rank.size(), false);
            std::list<node_index_type> path;
            if (recursive_path_finder_a(p1, p2, path, visited) || recursive_path_finder_d(p1, p2, path, visited)) {
                path.push_back(p1);
            }
            /*MSG_DEBUG("PATH " << path);*/
            return path;
        }

    struct parents_of_max_rank {
        std::list<node_index_type>::iterator p1, child, p2;
    };

    parents_of_max_rank
        find_parents_of_max_rank(std::list<node_index_type>& path)
        {
            auto child = std::max_element(path.begin(), path.end(), [this](node_index_type n1, node_index_type n2) { return rank[n1] < rank[n2]; });
            auto p1 = child, p2 = child;
            if (child == path.begin()) {
                p1 = path.end();
            }
            --p1;
            ++p2;
            if (p2 == path.end()) {
                p2 = path.begin();
            }
            return {p1, child, p2};
        }

    const var_vec&
        variables_of(node_index_type n) const
        {
            var_vec& ret = const_cast<graph_type*>(this)->node_variables[n];
            if (ret.size() == 0) {
                for (node_index_type i: inner_nodes[resolve(n)]) {
                    ret.insert(ret.end(), rules[i].begin(), rules[i].end());
                    if (variables[i] != -1) {
                        ret.push_back(variables[i]);
                    }
                    ret.insert(ret.end(), annotations[i].begin(), annotations[i].end());
                }
                sort_and_unique(ret);
            }
            return node_variables[n];
        }


    bool
        aggregate(node_index_type p1, node_index_type p2, node_index_type new_node, std::list<node_index_type>& path, std::list<node_index_type>::iterator pos)
        {
            p1 = resolve(p1);
            p2 = resolve(p2);
            node_index_type ret = rank.size();

            node_vec aggr = find_aggregate_chain(p1, p2);
            sort_and_unique(aggr);

            node_vec all_nodes, nin, nout;
            for (node_index_type i: aggr) {
                all_nodes = all_nodes + inner_nodes[i];
                nin = nin + nei_in(i);
                nout = nout + nei_out(i);
            }

            nin = nin - all_nodes - aggr;
            nout = nout - all_nodes - aggr;

            if (aggr.size() == 1) {
                ret = aggr.front();
            } else {
                MSG_DEBUG("-----------------------------------------------------------------------------");
                MSG_DEBUG("adding aggregate " << ret);
                colour.emplace_back(colour[p1]);
                rank.push_back(1 + std::min(rank[p1], rank[p2]));
                annotations.emplace_back();
                is_dh.push_back(false);
                represented_by.push_back(ret);
                MSG_DEBUG("initial aggr " << aggr);

                MSG_DEBUG("ready to aggregate nodes " << all_nodes << std::endl
                          << "             aggr " << aggr << std::endl
                          << "    neighbours in " << nin << std::endl
                          << "   neighbours out " << nout);

                inner_nodes.emplace_back();
                neighbours_in.emplace_back();
                neighbours_out.emplace_back();
                rules.emplace_back();
                subgraphs.emplace_back();
                type.push_back(Aggregate);
                variables.push_back(-1);
                node_variables.emplace_back();
                tables.emplace_back();
                /*state.emplace_back();*/

                for (node_index_type i: aggr) {
                    represented_by[i] = ret;
                }
            }

            neighbours_in[new_node] = nei_in(new_node) - aggr + node_vec{ret};

            inner_nodes[ret].swap(all_nodes);
            neighbours_in[ret].swap(nin);
            neighbours_out[ret].swap(nout);

            dump_active();
            MSG_QUEUE_FLUSH();

            update_rank(ret);
            propagate_update_rank(ret);

            path.insert(pos, ret);

            check_neighbours();

            return aggr.size() > 2;
        }


    std::list<node_index_type>
        find_vpath(variable_index_type v1, variable_index_type v2) const
        {
            node_vec nv1, nv2;
            std::list<node_index_type> ret;
            if (v1 > v2) {
                v1 ^= v2;
                v2 ^= v1;
                v1 ^= v2;
            }
            var_vec V = {v1, v2};
            /*MSG_DEBUG("v1 " << v1 << " v2 " << v2);*/
            for (node_index_type n: active_nodes()) {
                if (is_interface(n)) {
                    for (variable_index_type v: V % variables_of(n)) {
                        (v == v1 ? nv1 : nv2).push_back(n);
                    }
                }
            }
            /*MSG_DEBUG("nv1 " << nv1 << " nv2 " << nv2);*/
            auto joint_itf = nv1 % nv2;
            if (joint_itf.size()) {
                return {*std::min_element(joint_itf.begin(), joint_itf.end(), [this](node_index_type n1, node_index_type n2) { return variables_of(n1).size() < variables_of(n2).size(); })};
            }
            size_t path_size = (size_t) -1;
            for (auto n1: nv1) {
                for (auto n2: nv2) {
                    if (!colour_equal(colour[n1], colour[n2])) {
                        return {};
                    }
                    auto path = find_path2(n1, n2);
                    /*MSG_DEBUG("n1 " << n1 << " n2 " << n2 << " path " << path);*/
                    size_t psz = path.size();
                    if (psz && psz < path_size) {
                        path_size = psz;
                        path.swap(ret);
                    }
                }
            }
            return ret;
        }

    node_vec
        var_to_nodes(variable_index_type v) const
        {
            node_vec nv;
            for (node_index_type n: active_nodes()) {
                if (is_interface(n) || type[n] == Aggregate) {
                    auto varz = variables_of(n);
                    if (std::find(varz.begin(), varz.end(), v) != varz.end()) {
                        nv.push_back(n);
                    }
                }
            }
            return nv;
        }

    /*std::map<colour_proxy, std::vector<std::list<node_index_type>>>*/
    std::vector<std::vector<std::list<node_index_type>>>
        find_vpath(var_vec vv) const
        {
            /*std::map<colour_proxy, std::vector<std::list<node_index_type>>> ret;*/
            std::map<variable_index_type, std::vector<node_vec>> nv;
            std::map<colour_proxy, var_vec> by_colour;
            for (auto v: vv) {
                auto nodes = var_to_nodes(v);
                /*MSG_DEBUG("variable " << v << " nodes " << nodes);*/
                /*for (auto n: nodes) {*/
                    /*MSG_DEBUG("  node colour " << get_colour_impl(colour[n]));*/
                /*}*/
                auto col = get_colour_impl(colour[nodes.front()]);
                nv[v].push_back(nodes);
                by_colour[col].push_back(v);
            }
            std::vector<var_vec> cliques;
            std::map<std::pair<variable_index_type, variable_index_type>, std::list<node_index_type>> paths;
            std::vector<std::vector<std::list<node_index_type>>> paths_by_clique;
            std::map<variable_index_type, size_t> var_clique;
            for (auto& kv: by_colour) {
                /*MSG_DEBUG("In component " << kv.second);*/
                if (kv.second.size() > 1) {
                    auto i = kv.second.begin(), j = kv.second.end();
                    for (; i != j; ++i) {
                        if (var_clique.find(*i) == var_clique.end()) {
                            var_clique[*i] = cliques.size();
                            cliques.emplace_back();
                            cliques.back().push_back(*i);
                            paths_by_clique.emplace_back();
                            /*MSG_DEBUG("Variable " << (*i) << " is in a new clique");*/
                        }
                        for (auto k = i + 1; k != j; ++k) {
                            /*MSG_DEBUG("Find path between " << (*i) << " and " << (*k));*/
                            auto path = find_vpath(*i, *k);
                            if (path.size()) {
                                paths[{*i, *k}] = path;
                                paths_by_clique[var_clique[*i]].emplace_back(path);
                                cliques[var_clique[*i]].push_back(*k);
                                var_clique[*k] = var_clique[*i];
                            }
                        }
                        if (paths_by_clique[var_clique[*i]].size() == 0) {
                            paths_by_clique[var_clique[*i]].emplace_back();
                            /*paths_by_clique[var_clique[*i]].back().emplace_back(*i);*/
                            paths_by_clique[var_clique[*i]].back().emplace_back(nv[*i].front().front());
                        }
                    }
                    /*MSG_DEBUG("var_clique " << var_clique);*/
                } else {
                    auto var = kv.second.front();
                    /*MSG_DEBUG("var is single " << var);*/
                    var_clique[var] = cliques.size();
                    cliques.emplace_back();
                    cliques.back().push_back(var);
                    paths_by_clique.emplace_back();
                    paths_by_clique.back().emplace_back();
                    /*paths_by_clique.back().emplace_back(var);*/
                    /*paths_by_clique.back().emplace_back(find_vpath(var, var));*/
                    paths_by_clique.back().back().emplace_back(nv[var].front().front());
                }
            }
            /*MSG_DEBUG("cliques " << cliques);*/
            /*MSG_DEBUG("paths " << paths);*/
            /*MSG_DEBUG("paths_by_clique " << paths_by_clique);*/

            return paths_by_clique;
        }

    std::shared_ptr<graph_type>
        subgraph(node_index_type index, bool fin=true)
        {
            std::shared_ptr<graph_type> ret = std::make_shared<graph_type>(n_alleles);
            ret->generate_interfaces = generate_interfaces;
            ret->aggregate_cycles = aggregate_cycles;
            ret->parent = this;
            ret->index_in_parent = index;
            var_vec remaining, imported;
            /*MSG_DEBUG("CREATING SUBGRAPH " << inner_nodes[index]);*/
            /*MSG_DEBUG("index " << index);*/
            /*MSG_DEBUG("this = " << ret.get());*/
            /*MSG_DEBUG("parent " << this);*/
            for (node_index_type i: nei_in(index)) {
                if (is_interface(i)) {
                    ret->create_interface(variables_of(i));
                } else {
                    ret->create_interface(variables_of(i) % variables_of(index));
                }
            }
            for (node_index_type i: inner_nodes[index]) {
                const auto& rule = rules[i];
                auto var = variables[i];
                /*MSG_DEBUG("subgraph " << i << "  " << rule << " " << var);*/
                if (type[i] == Factor) {
                    node_index_type n;
                    switch (rule.size()) {
                        case 0: n = ret->add_factor(var);
                                break;
                        case 1: n = ret->add_factor(rule[0], var);
                                break;
                        case 2: n = ret->add_factor(rule[0], rule[1], var);
                                break;
                    };
                    ret->tables[n] = tables[i];
                } else {
                    if (ret->interface_to_node.find(var) == ret->interface_to_node.end()) {
                        ret->add_interface(node_vec{}, var);
                    }
                }
                /*for (variable_index_type v: variables_of(i)) {*/
                    /*auto& domain = ret->domains[{v}];*/
                    /*if (!domain.size()) {*/
                        /*domain = domains[{v}];*/
                    /*}*/
                /*}*/
            }
            if (fin) {
                ret->finalize();
            } else {
                for (node_index_type n: ret->active_nodes()) {
                    if (ret->is_aggregate(n) && !ret->is_interface(n)) {
                        ret->subgraphs[n] = ret->subgraph(n);
                    }
                }
            }
            return ret;
        }

    VariableIO
        node_io(node_index_type n) const
        {
            VariableIO ret = None;
            if (type[n] == Factor) {
                return io.find(variables[n])->second;
            }
            for (variable_index_type v: variables_of(n)) {
                ret |= io.find(v)->second;
            }
            return ret;
        }

    std::string
        to_image(const std::string& prefix, const std::string& format) const
        {
            auto A = active_nodes();
            std::string dotfilename = SPELL_STRING("/tmp/prout" << prefix); //std::tempnam(nullptr);
            std::vector<std::string> sub_filenames;
            {
                std::ofstream dotfile(dotfilename);
                dotfile << "digraph spell {rankdir=LR;" << std::endl;
                if (parent != NULL) {
                    dotfile << "bgcolor=transparent;" << std::endl;
                }
                for (node_index_type n: A) {
                    std::string col;
                    auto io_val = node_io(n);
                    if (io_val == (Input | Output)) {
                        col = ", style=filled, fillcolor=mistyrose2";
                    } else if (io_val == Input) {
                        col = ", style=filled, fillcolor=lightsteelblue1";
                    } else if (io_val == Output) {
                        col = ", style=filled, fillcolor=darkseagreen1";
                    } else {
                        col = ", style=filled, fillcolor=white";
                    }
                    dotfile << n << "[";
                    if (is_interface(n)) {
                        dotfile << "shape=diamond, label=\"" << n  << " [" << inner_nodes[n] << "] " << variables_of(n) << "\"";
                    } else if (is_aggregate(n)) {
                        std::string filename = subgraphs[n]->to_image(SPELL_STRING("_sub_" << prefix << '_' << n), format);
                        dotfile << "shape=box,labeljust=\"l\", labelloc=\"t\", label=\"" << n << " [" << inner_nodes[n] << "]\",image=\"" << filename << "\"";
                        sub_filenames.push_back(filename);
                    } else {
                        dotfile << "shape=box,label=\"" << n << ' ' << variables_of(n) << "\"";
                    }
                    dotfile << col << "];" << std::endl;
                }
                for (node_index_type h: A) {
                    for (node_index_type t: nei_out(h)) {
                        dotfile << h << " -> " << t << std::endl;
                    }
                }
                dotfile << '}' << std::endl;
            }
            std::string ret = SPELL_STRING(prefix << '.' << format);
            std::string cmd = SPELL_STRING("dot -T" << format << ' ' << dotfilename << " -o " << ret);
            if (system(cmd.c_str())) {
                MSG_ERROR("Dot failed " << strerror(errno), "");
            }
            for (const auto& s: sub_filenames) { unlink(s.c_str()); }
            return ret;
        }

    bool
        var_is_protected(variable_index_type var)
        {
            return io[var] != None;
        }

    bool
        var_is_output(variable_index_type var) const
        {
            auto mode = io.find(var)->second;
            return mode & Output;
        }

    static
        std::unique_ptr<graph_type>
        from_pedigree(const std::vector<pedigree_item>& ped, size_t n_al, const std::vector<std::string>& input_gen, const std::vector<std::string>& output_gen, const std::string& debug_prefix = "")
        {
            std::unique_ptr<graph_type> g(new graph_type(n_al));
            g->aggregate_cycles = true;
            g->generate_interfaces = false;
            /*system("rm -vf test_*.png");*/
            for (const auto& p: ped) {
                /*MSG_DEBUG("-------------------------------------------------------------------------------");*/
                /*MSG_DEBUG("-------------------------------------------------------------------------------");*/
                /*MSG_DEBUG("-------------------------------------------------------------------------------");*/
                VariableIO io = None;
                if (std::find(input_gen.begin(), input_gen.end(), p.gen_name) != input_gen.end()) {
                    io |= Input;
                }
                if (std::find(output_gen.begin(), output_gen.end(), p.gen_name) != output_gen.end()) {
                    io |= Output;
                }
                g->io[p.id] = io;
                if (p.is_ancestor()) {
                    g->add_interface(node_vec{}, p.id);
                    /*g->add_factor(p.id);*/
                    g->add_ancestor(p.id);
                } else if (p.is_cross()) {
                    g->add_cross(p.p1, p.p2, p.id);
                } else if (p.is_dh()) {
                    g->add_dh(p.p1, p.id);
                } else if (p.is_self()) {
                    g->add_selfing(p.p1, p.id);
                }
                /*if (io) {*/
                    /*MSG_DEBUG("IO for var #" << p.id << ": " << io);*/
                /*}*/
            }
            g->finalize();
            g->compute_domains_and_factors();
            return g;
        }

    std::vector<bn_label_type>
        get_domain(variable_index_type var)
        {
            std::vector<bn_label_type> ret;
            const auto& dom = domains[{var}];
            ret.reserve(dom.size());
            for (const auto& e: dom) {
                ret.push_back(e.keys.keys.front().state);
            }
            return ret;
        }

    bool
        variables_are_independent(variable_index_type v1, variable_index_type v2, const std::list<node_index_type>& path) const
        {
            var_vec V = v1 < v2 ? var_vec{v1, v2} : var_vec{v2, v1};
            for (node_index_type n: path) {
                if (!(variables_of(n) % V).size()) {
                    return true;
                }
            }
            return false;
        }

    std::map<variable_index_type, node_index_type>
        get_output_interfaces(const var_vec& V) const
        {
            std::map<variable_index_type, node_index_type> ret;
            std::map<variable_index_type, node_vec> candidates;
            std::map<node_index_type, size_t> candidate_scores;
            for (node_index_type n: active_nodes()) {
                if (!is_interface(n)) {
                    continue;
                }
                auto vn = variables_of(n) % V;
                if (vn.size()) {
                    candidate_scores[n] += vn.size();
                    for (auto v: vn) {
                        candidates[v].push_back(n);
                    }
                }
            }

            for (auto& vn: candidates) {
                std::sort(vn.second.begin(), vn.second.end(), [&](node_index_type n1, node_index_type n2) { return candidate_scores[n1] > candidate_scores[n2]; });
                ret[vn.first] = vn.second.front();
            }

            return ret;
        }

    node_index_type
        find_anchor(const var_vec& V)
        {
            std::map<node_index_type, size_t> candidate_scores;
            for (node_index_type n: active_nodes()) {
                auto vn = variables_of(n) % V;
                if (vn.size()) {
                    candidate_scores[n] = vn.size();
                }
            }
            size_t max = 0;
            node_index_type best = (node_index_type) -1;
            for (const auto& kv: candidate_scores) {
                if (kv.second > max) {
                    max = kv.second;
                    best = kv.first;
                }
            }
            return best;
        }

    bool
        recursive_find_path_to_var(node_index_type node, variable_index_type v, std::list<node_index_type>& path, std::vector<bool>& visited) const
        {
            auto V = variables_of(node);
            if (std::find(V.begin(), V.end(), v) != V.end()) {
                /*path.push_back(node);*/
                return true;
            }
            visited[node] = true;
            for (node_index_type n: nei_in(node)) {
                n = resolve(n);
                if (!visited[n] && recursive_find_path_to_var(n, v, path, visited)) {
                    path.push_back(n);
                    return true;
                }
            }
            for (node_index_type n: nei_out(node)) {
                n = resolve(n);
                if (!visited[n] && recursive_find_path_to_var(n, v, path, visited)) {
                    path.push_back(n);
                    return true;
                }
            }
            return false;
        }

    std::list<node_index_type>
        find_path_to_var(node_index_type start, variable_index_type v)
        {
            std::vector<bool> visited(rank.size(), false);
            visited[start] = true;
            std::list<node_index_type> path;
            recursive_find_path_to_var(start, v, path, visited);
            return path;
        }

    void
        annotate(node_index_type anchor, const var_vec& V)
        {
            for (variable_index_type v: V) {
                for (node_index_type n: find_path_to_var(anchor, v)) {
                    annotations[n].push_back(v);
                }
            }
        }

    void
        sort_annotations()
        {
            for (auto& annot: annotations) {
                std::sort(annot.begin(), annot.end());
            }
        }

    void
        reset_annotations()
        {
            annotations.resize(rank.size());
            clear_annotations();
        }

    void
        clear_annotations()
        {
            for (auto& annot: annotations) {
                annot.clear();
            }
        }

    std::map<size_t, node_index_type>
        anchor_incoming(const std::vector<var_vec>& incoming)
        {
            std::map<size_t, node_index_type> ret;
            std::vector<size_t> indices(incoming.size());
            size_t index = 0;
            std::generate(indices.begin(), indices.end(), [&] () { return index++; });
            std::sort(indices.begin(), indices.end(), [&] (size_t i, size_t j) { return incoming[i].size() > incoming[j].size(); });
            for (size_t i: indices) {
                node_index_type anchor = find_anchor(incoming[i]);
                annotate(anchor, incoming[i]);
                ret[i] = anchor;
            }
            sort_annotations();
            MSG_DEBUG("ANCHORS " << ret);
            return ret;
        }


    std::vector<message_type> node_domains;
    std::map<node_index_type, node_index_type> anchor_points;
    std::vector<bool> node_domain_computed;

    message_type
        get_joint_domain(const var_vec& varset)
        {
            MSG_QUEUE_FLUSH();
            /*scoped_indent _(SPELL_STRING("[get_joint_domain " << varset << "] "));*/
            auto path = find_vpath(varset);
            message_type ret;
            for (const auto& cliq: path) {
                ret.emplace_back();
                message_type accum;
                for (const auto& path: cliq) {
                    message_type tmp;
                    for (node_index_type n: path) {
                        accumulate(tmp, get_node_domain(n), domains);
                        tmp %= varset + variables_of(n);
                        /*MSG_DEBUG("tmp " << tmp);*/
                    }
                    accumulate(accum, tmp, domains);
                    accum %= varset;
                }
                accumulate(ret, accum, domains);
            }
            return ret;
        }

    message_type
        get_node_domain(node_index_type n)
        {
            if (is_aggregate(n) && !is_interface(n)) {
                if (!node_domain_computed[n]) {
                    subgraphs[n]->compute_domains_and_factors();
                    node_domain_computed[n] = true;
                }
                return subgraphs[n]->get_joint_domain(variables_of(n));
            } else {
                if (!node_domain_computed[n]) {
                    compute_node_domain(n);
                }
                return node_domains[n];
            }
        }

    message_type
        get_node_domain(node_index_type n, const var_vec& vv)
        {
            /*MSG_DEBUG("get_node_domain(" << n << ", " << vv << ')');*/
            if (is_aggregate(n) && !is_interface(n)) {
                if (!node_domain_computed[n]) {
                    subgraphs[n]->compute_domains_and_factors();
                    node_domain_computed[n] = true;
                }
                return subgraphs[n]->get_joint_domain(vv);
            } else {
                if (!node_domain_computed[n]) {
                    compute_node_domain(n);
                }
                return node_domains[n] % vv;
            }
        }

    void
        propagate_spawnling_domain_rec(const var_vec& V, const genotype_comb_type& dom, std::map<const graph_type*, bool>& visited)
        {
            /*if (visited[this]) {*/
                /*return;*/
            /*}*/
            visited[this] = true;
            domains[V] = dom;
            /*MSG_DEBUG("Domains " << (parent ? SPELL_STRING(parent << "->" << index_in_parent) : std::string("top-level")) << ' ' << domains);*/
            if (parent && !visited[parent]) {
                const_cast<graph_type*>(parent)->propagate_spawnling_domain(V, dom);
            }
            for (auto& sub: subgraphs) {
                if (sub && !visited[sub.get()]) {
                    sub->propagate_spawnling_domain_rec(V, dom, visited);
                }
            }
        }

    void
        propagate_spawnling_domain(const var_vec& V, const genotype_comb_type& dom)
        {
            std::map<const graph_type*, bool> visited;
            propagate_spawnling_domain_rec(V, dom, visited);
        }

    void
        compute_node_domain(node_index_type n)
        {
            scoped_indent _(SPELL_STRING("[compute_node_domain #" << n << "] "));
            auto inputs = nei_in(n);
            auto vv = variables_of(n);
            if (is_interface(n)) {
                multiple_product_type mp;
                std::vector<message_type> stack;
                for (node_index_type i: inputs) {
                    stack.emplace_back(get_node_domain(i, vv));
                }
                for (const auto& m: stack) {
                    mp.add(m);
                }
                node_domains[n] = mp.compute(vv, domains);
                for (auto& dom: node_domains[n]) {
                    for (auto& e: dom) {
                        e.coef = 1;
                    }
                }
            } else if (is_aggregate(n)) {
                subgraphs[n]->compute_domains_and_factors();
            } else /* factor */ {
                scoped_indent _(SPELL_STRING("[compute_factor {" << rules[n] << "} => " << variables[n] << "] "));
                MSG_DEBUG(" ### ### COMPUTING FACTOR {" << rules[n] << "} => " << variables[n] << " ### ###");
                MSG_QUEUE_FLUSH();
                joint_variable_product_type jvp;

                var_vec varset = rules[n] + var_vec{variables[n]};
                std::sort(varset.begin(), varset.end());
                /*for (node_index_type o: nei_out(n)) {*/
                    /*varset = varset + variables_of(o);*/
                /*}*/
                message_type stack;
                stack.reserve(64);  /* FIXME arbitrary value */
                for (node_index_type i: inputs) {
                    MSG_DEBUG("[jpar_dom] varset " << varset);
                    auto idom = get_node_domain(i, varset);
                    MSG_DEBUG("[jpar_dom] from domain " << idom);
                    for (const auto& t: idom) {
                        stack.emplace_back(t);
                    }
                }

                            /*jvp.add_table(stack.back());*/
                            /*MSG_DEBUG(" * ADDING " << stack.back());*/
                        /*}*/
                    /*}*/
                /*}*/
                /*jvp.set_output(varset);*/
                /*jvp.compile(domains);*/
                /*auto jpar_dom = jvp.compute();*/
                genotype_comb_type jpar_dom;
                MSG_QUEUE_FLUSH();
                if (stack.size() > 1) {
                    MSG_DEBUG("Domains " << (parent ? SPELL_STRING(parent << "->" << index_in_parent) : std::string("top-level")) << ' ' << domains);
                    jpar_dom = compute_product(stack.begin(), stack.end(), varset, domains);
                } else {
                    jpar_dom = stack.front();
                }
                MSG_DEBUG("[jpar_dom] stack " << stack);
                MSG_DEBUG("[jpar_dom] result " << jpar_dom);
                node_domains[n].emplace_back(generate_factor(rules[n], is_dh[n], variables[n], jpar_dom, domains));
                /* Add domain for spawnling */
                variable_index_type spawnling = variables[n];
                auto dom = (node_domains[n] % var_vec{spawnling}).front();
                for (auto& e: dom) {
                    e.coef = 1;
                }
                propagate_spawnling_domain({spawnling}, dom);
                MSG_DEBUG("Domain for spawnling #" << spawnling << ": " << domains[{spawnling}]);
            }
            MSG_DEBUG("COMPUTED DOMAIN " << node_domains[n]);
            dump_node(n);
            node_domain_computed[n] = true;
        }

    void
        compute_domains_and_factors()
        {
            scoped_indent _(SPELL_STRING("[compute_domains_and_factors " << (parent ? SPELL_STRING(parent << "->" << index_in_parent) : std::string("top-level")) << "] "));
            /*MSG_DEBUG("n_alleles = " << n_alleles);*/
            if (parent) {
                domains = parent->domains;
                auto nei_nodes = parent->nei_in(index_in_parent) + parent->nei_out(index_in_parent);
                std::vector<var_vec> all_nei_vars;
                all_nei_vars.reserve(nei_nodes.size());
                MSG_DEBUG("NEI NODES " << nei_nodes);
                for (node_index_type n: nei_nodes) {
                    all_nei_vars.push_back(parent->variables_of(n) % parent->variables_of(index_in_parent));
                }
                MSG_DEBUG("ALL NEI VARS " << all_nei_vars);
                reset_annotations();
                for (const auto& ia: anchor_incoming(all_nei_vars)) {
                    anchor_points[nei_nodes[ia.first]] = ia.second;
                }
                
            }
            node_domains.clear();
            node_domains.resize(rank.size());
            node_domain_computed.clear();
            node_domain_computed.resize(rank.size());
            /*MSG_DEBUG("Domains " << domains);*/

            auto active = active_nodes();

            for (node_index_type n: active) {
                if (is_interface(n) && nei_in(n).size() == 0) {
                    auto vv = variables_of(n);
                    if (parent == NULL) {
                        /* ancestor(s) ! */
                        for (variable_index_type v: vv) {
                            node_domains[n].emplace_back(domains[{v}]);
                        }
                        node_domain_computed[n] = true;
                        MSG_DEBUG("Pre-filled ancestor interface #" << n << " domain " << node_domains[n]);
                    } else {
                        multiple_product_type mp;
                        std::vector<message_type> stack;
                        for (node_index_type i: parent->resolve_vector(parent->nei_in(index_in_parent))) {
                            MSG_DEBUG("SUBGRAPH EXTERNAL INPUT #" << i);
                            stack.emplace_back(const_cast<graph_type*>(parent)->get_node_domain(i, vv));
                            MSG_DEBUG("Have domain " << stack.back());
                        }
                        for (const auto& m: stack) {
                            mp.add(m);
                        }
                        node_domains[n] = mp.compute(vv, domains);
                        node_domain_computed[n] = true;
                        MSG_DEBUG("HAVE DOMAIN FOR INPUT #" << n << ' ' << node_domains[n]);
                    }
                }
            }
            for (node_index_type n: active) {
                if (!node_domain_computed[n]) {
                    MSG_QUEUE_FLUSH();
                    compute_node_domain(n);
                }
            }

            MSG_DEBUG("domains computes " << node_domain_computed);

            for (node_index_type n: active) {
                dump_node(n);
                MSG_DEBUG("=========================================================");
            }
        }

    void
        delete_node(node_index_type n)
        {
            represented_by[n] = 0;
            const node_vec& N = inner_nodes[n] + node_vec{n};
            for (auto i: nei_in(n)) {
                neighbours_out[i] = nei_out(i) - N;
            }
            for (auto o: nei_out(n)) {
                neighbours_in[o] = nei_in(o) - N;
            }
        }

#define foreach_node_in(_var, _set) for (node_index_type _var: _set)
#define foreach_node_in_if(_var, _set, _pred) for (node_index_type _var: _set) if (_pred)
#define has_single_input(_n) nei_in(_n).size() == 1
#define has_single_output(_n) nei_out(_n).size() == 1
#define has_no_output(_n) nei_out(_n).size() == 0

    bool is_factor(node_index_type n) const { return type[n] == Factor; }
    bool is_unprotected(node_index_type n) const { for (node_index_type i: inner_nodes[n]) { if (io.find(variables[i])->second != None) { return false; } } return true; }

    size_t
        get_node_domain_size(node_index_type n)
        {
            auto dom = get_node_domain(n);
            if (dom.size() == 0) {
                return 0;
            }
            size_t accum = 1;
            for (const auto& t: dom) {
                accum *= t.size();
            }
            return accum;
        }

    void
        squeeze_factor(const node_vec& f1in, node_index_type f1, node_index_type i1, node_index_type f2)
        {
            var_vec var_f1in;
            for (node_index_type i: f1in) {
                var_f1in = var_f1in + variables_of(i);
            }
            var_vec var_f2out;
            for (node_index_type i: nei_out(f2)) {
                var_f2out = var_f2out + variables_of(i);
            }
            var_vec var_fac = variables_of(f1) + variables_of(f2);
            auto final_varset = (var_f1in + var_f2out) % var_fac;
            /*auto in_f2_squeeze = variables_of(i1);*/
            /*foreach_node_in_if(if2, nei_in(f2), if2 != i1) {*/
                /*in_f2_squeeze = in_f2_squeeze - variables_of(if2);*/
            /*}*/
            /*auto final_varset = var_f1in + variables_of(f2) - in_f2_squeeze;*/
            multiple_product_type mp;
            message_type f1_subdom = get_node_domain(f1, var_f1in + variables_of(i1) + final_varset);
            message_type f2_subdom = get_node_domain(f2, var_f2out + variables_of(i1) + final_varset);
            mp.add(f1_subdom);
            mp.add(f2_subdom);
            node_domains[f2] = mp.compute(final_varset, domains); //product(node_domains[f1], node_domains[f2], domains) % varset;
            inner_nodes[f2].push_back(f1);
            neighbours_in[f2] = nei_in(f1) + nei_in(f2) - node_vec{i1};
            neighbours_out[i1] = nei_out(i1) - node_vec{f2};
            foreach_node_in(if1, f1in) {
                neighbours_out[if1] = nei_out(if1) + node_vec{f2};
            }
            if (has_no_output(i1)) {
                delete_node(i1);
            }
            node_variables[f2] = final_varset;
        }

    std::unique_ptr<graph_type>
        clone() const
        {
            std::unique_ptr<graph_type> ret(new graph_type());

            ret->rank = rank;
            ret->represented_by = represented_by;
            ret->type = type;
            ret->colour = colour;
            ret->neighbours_in = neighbours_in;
            ret->neighbours_out = neighbours_out;
            ret->inner_nodes = inner_nodes;
            ret->rules = rules;
            ret->variables = variables;
            ret->node_variables = node_variables;
            ret->io = io;
            ret->interface_to_node = interface_to_node;
            ret->node_to_interface = node_to_interface;
            ret->subgraphs.resize(rank.size());
            ret->tables = tables;
            /*ret->state = state;*/
            ret->parent = parent;
            ret->index_in_parent = index_in_parent;
            ret->aggregate_cycles = aggregate_cycles;
            ret->generate_interfaces = generate_interfaces;
            ret->n_alleles = n_alleles;
            ret->annotations = annotations;
            ret->is_dh = is_dh;
            ret->domains = domains;
            ret->ancestor_letters = ancestor_letters;
            ret->node_domains = node_domains;
            ret->node_domain_computed = node_domain_computed;
            return ret;
        }

    void
        debug_graph(const std::string& pfx, size_t x)
        {
            if (parent != NULL) {
                return;
            }
            auto tmp = clone();
            for (node_index_type n: tmp->active_nodes()) {
                if (tmp->is_aggregate(n) && !tmp->is_interface(n) && !tmp->subgraphs[n]) {
                    tmp->subgraphs[n] = tmp->subgraph(n, false);
                }
            }
            /*tmp->finalize();*/
            tmp->to_image(SPELL_STRING(pfx << '_' << this << '_' << std::setw(4) << std::setfill('0') << x), "png");
        }

    void
        optimize()
        {
            debug_graph("optimize", 0);
            /*scoped_indent _("[optimize] ");*/
            foreach_node_in_if(n, active_nodes(), subgraphs[n]) {
                subgraphs[n]->optimize();
            }
            debug_graph("optimize", 1);

            /*MSG_DEBUG("Squeeze intermediate subgraphs");*/
            foreach_node_in_if(f1, active_nodes(), is_aggregate(f1) && !is_interface(f1) && has_single_input(f1) && is_unprotected(f1)) {
                /*MSG_DEBUG("On subgraph#1 " << f1 << ' ' << variables_of(f1));*/
                auto f1in = nei_in(f1);
                foreach_node_in_if(i1, nei_out(f1), is_interface(i1)) {
                    /*MSG_DEBUG("On interface#1 " << i1 << ' ' << variables_of(i1) << " / " << nei_out(i1));*/
                    if (has_single_output(i1)) {
                        foreach_node_in_if(f2, nei_out(i1), type[f2] == Factor) {
                            /*MSG_DEBUG("On factor#2 " << f2 << ' ' << variables_of(f2));*/
                            squeeze_factor(f1in, f1, i1, f2);
                    }
                    }
                }
            }
            debug_graph("optimize", 2);

            /*MSG_DEBUG("Squeeze intermediate factors");*/
            foreach_node_in_if(f1, active_nodes(), is_factor(f1) && has_single_output(f1) && is_unprotected(f1)) {
                /*MSG_DEBUG("On factor#1 " << f1 << ' ' << variables_of(f1));*/
                auto f1in = nei_in(f1);
                foreach_node_in_if(i1, nei_out(f1), is_interface(i1) && has_single_output(i1)) {
                    /*MSG_DEBUG("On interface#1 " << i1 << ' ' << variables_of(i1));*/
                    foreach_node_in_if(f2, nei_out(i1), type[f2] == Factor) {
                        /*MSG_DEBUG("On factor#2 " << f2 << ' ' << variables_of(f2));*/
                        squeeze_factor(f1in, f1, i1, f2);
                    }
                }
            }
            debug_graph("optimize", 3);

            /* remove dangling interfaces that have no I/O */
            /*MSG_DEBUG("Purging dangling interfaces");*/
            foreach_node_in_if(n, active_nodes(), has_no_output(n) && is_unprotected(n)) {
                delete_node(n);
            }
            debug_graph("optimize", 4);
        }

    struct message_compute_operation_type {
        std::vector<size_t> incoming_messages;
        std::vector<var_vec> incoming_domains;
        var_vec output;
        node_index_type emitter;
        node_index_type receiver;

        friend
            inline
            std::ostream&
            operator << (std::ostream& os, const message_compute_operation_type& mco)
            {
                return os << "{(" << mco.incoming_messages << ") x " << mco.emitter << " ==" << mco.output << "=> " << mco.receiver << '}';
            }

        template <typename IO>
            void
            file_io(IO& f)
            {
                rw_base rw;
                if (rw.fourcc(f, "MCpO")) { return; }
                rw(f, incoming_messages);
                rw(f, incoming_domains);
                rw(f, output);
                rw(f, emitter);
                rw(f, receiver);
            }
    };

    struct computation_variant_type {
        std::vector<size_t> outer_inputs;
        std::vector<node_index_type> anchors;
        std::vector<size_t> operations;
        /* FIXME must be a vector, in case of joint probabilities along a path, or multiple ones */
        std::vector<message_compute_operation_type> output_operations;

        template <typename IO>
            void
            file_io(IO& f)
            {
                rw_base rw;
                if (rw.fourcc(f, "CVar")) { return; }
                rw(f, outer_inputs);
                rw(f, anchors);
                rw(f, operations);
                rw(f, output_operations, [](IO& f, message_compute_operation_type& mco) { mco.file_io(f); });
            }

        friend
            inline
            std::ostream&
            operator << (std::ostream& os, const computation_variant_type& cv)
            {
                return os << "<variant outer=" << cv.outer_inputs << " anchors=" << cv.anchors << " ops=" << cv.operations << " output=" << cv.output_operations << ">";
            }
    };

    struct instance_type {
        std::vector<message_type> tables;
        std::vector<message_type> evidence;
        std::unordered_map<variable_index_type, node_index_type> evidence_affectations;
        std::vector<std::unique_ptr<instance_type>> sub_instances;
        const instance_type* parent;
        std::unordered_map<node_index_type, computation_variant_type> variants; /* computations for the given output (as key) */
        std::map<std::pair<node_index_type, node_index_type>, size_t> message_index;
        std::vector<message_type> messages; /* internal messages */
        std::vector<message_compute_operation_type> operations;

        template <typename IO>
            void
            file_io(IO& f)
            {
                rw_comb<int, bn_label_type> rw;
                if (rw.fourcc(f, "BN_I")) { return; }
                rw(f, tables);
                rw(f, evidence_affectations);
                /*rw(f, variants, [](IO& f, computation_variant_type& cv) { cv.file_io(f); });*/
                rw(f, variants);
                rw(f, message_index);
                /*rw(f, operations, [](IO& f, message_compute_operation_type& mco) { mco.file_io(f); });*/
                rw(f, operations);
                rw_sub_instances(f);
            }

        void
            rw_sub_instances(ifile& ifs)
            {
                rw_base rw;
                parent = nullptr;
                size_t n, i;
                rw(ifs, n);
                sub_instances.clear();
                sub_instances.resize(tables.size());
                for (; n; --n) {
                    rw(ifs, i);
                    auto& sub = sub_instances[i];
                    sub.reset(new instance_type());
                    sub->file_io(ifs);
                    sub->parent = this;
                }
                messages.resize(message_index.size());
                clear_evidence();
            }

        void
            rw_sub_instances(ofile& ofs)
            {
                rw_base rw;
                size_t n = 0;
                for (const auto& sub: sub_instances) if (sub) { ++n; }
                rw(ofs, n);
                for (size_t i = 0; i < sub_instances.size(); ++i) if (sub_instances[i]) {
                    rw(ofs, i);
                    sub_instances[i]->file_io(ofs);
                }
            }

        instance_type() : tables(), evidence(), evidence_affectations(), sub_instances(), parent(nullptr) {}
        instance_type(graph_type* g, instance_type* par=nullptr)
            : tables(g->node_domains.size()), evidence(g->node_domains.size()), sub_instances(g->node_domains.size()), parent(par)
        {
            var_vec varz; for (node_index_type n: g->active_nodes()) { varz = varz + g->variables_of(n); }
            scoped_indent _(SPELL_STRING("[instance " << varz << "] "));
            size_t mi = 0;
            for (node_index_type n: g->active_nodes()) {
                if (g->is_interface(n) || g->is_factor(n)) {
                    tables[n] = g->node_domains[n];
                }
                for (variable_index_type v: g->variables_of(n)) {
                    if (evidence_affectations.find(v) == evidence_affectations.end()) {
                        evidence_affectations[v] = n;
                    }
                }
                for (node_index_type o: g->nei_out(n)) {
                    message_index[{n, o}] = mi++;
                    message_index[{o, n}] = mi++;
                }
            }
            messages.resize(mi);
            operations.resize(mi);
            for (const auto& er_idx: message_index) {
                auto& op = operations[er_idx.second];
                std::tie(op.emitter, op.receiver) = er_idx.first;
                op.output = g->variables_of(op.emitter) % g->variables_of(op.receiver);
                auto nei = g->all_nei(op.emitter) - node_vec{op.receiver};
                op.incoming_messages.reserve(nei.size());
                for (node_index_type i: nei) {
                    op.incoming_messages.push_back(get_message_index(i, op.emitter));
                }
            }
            for (const auto& er_idx: message_index) {
                compile_incoming_message_domains(operations[er_idx.second]);
            }
            build_variants(g);
            for (node_index_type n: g->active_nodes()) {
                if (g->subgraphs[n]) {
                    sub_instances[n].reset(new instance_type(g->subgraphs[n].get(), this));
                }
            }
        }

        void
            compile_incoming_message_domains(message_compute_operation_type& op)
            {
                op.incoming_domains.clear();
                op.incoming_domains.reserve(op.incoming_messages.size());
                for (size_t msg: op.incoming_messages) {
                    var_vec other_dom = op.output;
                    for (size_t other: op.incoming_messages) if (other != msg) {
                        other_dom = other_dom + operations[other].output;
                    }
                    op.incoming_domains.push_back(operations[msg].output % other_dom);
                }
            }

        size_t
            get_message_index(node_index_type emitter, node_index_type receiver) const
            {
                auto it = message_index.find({emitter, receiver});
                if (it == message_index.end()) {
                    throw 0;
                }
                return it->second;
            }

        void
            add_evidence(variable_index_type v, const genotype_comb_type& obs)
            {
                node_index_type index = evidence_affectations[v];
                if (sub_instances[index]) {
                    sub_instances[index]->add_evidence(v, obs);
                } else {
                    evidence[index].push_back(obs);
                }
            }

        void
            clear_evidence()
            {
                evidence.clear();
                evidence.resize(tables.size());
            }

        message_type
            compute_message(const message_compute_operation_type& op, const std::map<var_vec, genotype_comb_type>& domains)
            {
                MSG_DEBUG("[compute message " << (int) (op.receiver == 0 ? -op.emitter : get_message_index(op.emitter, op.receiver)) << " % " << op.output << "] " << op);
                if (sub_instances[op.emitter]) {
                    return sub_instances[op.emitter]->compute(op.receiver, domains);
                }
                multiple_product_type mp;
                mp.add(evidence[op.emitter]);
                mp.add(tables[op.emitter]);
                /*for (size_t inc: op.incoming_messages) {*/
                auto inci = op.incoming_messages.begin(), incj = op.incoming_messages.end();
                auto domi = op.incoming_domains.begin();
                std::vector<message_type> msgs;
                msgs.reserve(op.incoming_messages.size());
                for (; inci != incj; ++inci, ++domi) {
                    msgs.emplace_back(messages[*inci] % *domi);
                    mp.add(msgs.back());
                }
                auto ret = mp.compute(op.output, domains);
                MSG_DEBUG("[compute message " << (int) (op.receiver == 0 ? -op.emitter : get_message_index(op.emitter, op.receiver)) << " % " << op.output << "] " << ret);
                return ret;
            }

        message_type
            compute(node_index_type n, const std::map<var_vec, genotype_comb_type>& domains)  /* compute an external message (this -> external node #n) */
            {
                const auto& variant = variants[n];
                clear_evidence();
                for (size_t i = 0; i < variant.outer_inputs.size(); ++i) {
                    evidence[variant.anchors[i]] = parent->messages[variant.outer_inputs[i]];
                }
                for (size_t o: variant.operations) {
                    const auto& op = operations[o];
                    messages[get_message_index(op.emitter, op.receiver)] = compute_message(op, domains);
                }
                message_type ret;
                for (const auto& op: variant.output_operations) {
                    auto tmp = compute_message(op, domains);
                    ret.insert(ret.end(), tmp.begin(), tmp.end());
                }
                return ret;
            }

        void
            get_message_order_rec(size_t final_message, std::vector<bool>& message_visited, std::vector<size_t>& ret)
            {
                for (size_t inc: operations[final_message].incoming_messages) {
                    if (!message_visited[inc]) {
                        get_message_order_rec(inc, message_visited, ret);
                    }
                }
                message_visited[final_message] = true;
                ret.push_back(final_message);
            }

        std::vector<size_t>
            get_message_order(size_t final_message)
            {
                std::vector<bool> message_visited(operations.size());
                std::vector<size_t> order;
                get_message_order_rec(final_message, message_visited, order);
                return order;
            }

        void
            build_variant(node_index_type towards, graph_type* g, const std::map<size_t, node_index_type>& incoming, node_index_type output)
            {
                /* incoming is [external message index] => internal anchor node */
                auto& V = variants[towards];
                V.outer_inputs.clear();
                V.outer_inputs.reserve(incoming.size());
                V.anchors.clear();
                V.anchors.reserve(incoming.size());
                for (const auto& msg_anchor: incoming) {
                    V.outer_inputs.push_back(msg_anchor.first);
                    V.anchors.push_back(msg_anchor.second);
                }
                std::vector<bool> message_visited(operations.size());
                if (output != 0) {
                    V.output_operations.emplace_back();
                    auto& output_operation = V.output_operations.back();
                    /*output_operation.output = g->variables_of(output);*/
                    for (variable_index_type v: g->variables_of(output)) {
                        if (g->var_is_output(v)) {
                            output_operation.output.push_back(v);
                        }
                    }
                    output_operation.receiver = 0;
                    output_operation.emitter = output;
                    for (node_index_type i: g->all_nei(output)) {
                        size_t m = get_message_index(i, output);
                        get_message_order_rec(m, message_visited, V.operations);
                        output_operation.incoming_messages.push_back(m);
                    }
                    compile_incoming_message_domains(output_operation);
                } else {
                    for (node_index_type output: g->active_nodes()) {
                        /*MSG_DEBUG("active node " << output << " has io " << g->node_io(output));*/
                        if (g->node_io(output) & Output) {
                            V.output_operations.emplace_back();
                            auto& output_operation = V.output_operations.back();
                            /*output_operation.output = g->variables_of(output);*/
                            for (variable_index_type v: g->variables_of(output)) {
                                if (g->var_is_output(v)) {
                                    output_operation.output.push_back(v);
                                }
                            }
                            output_operation.receiver = 0;
                            output_operation.emitter = output;
                            for (node_index_type i: g->all_nei(output)) {
                                size_t m = get_message_index(i, output);
                                get_message_order_rec(m, message_visited, V.operations);
                                output_operation.incoming_messages.push_back(m);
                            }
                            compile_incoming_message_domains(output_operation);
                        }
                    }
                }
                MSG_DEBUG(this << ' ' << std::setw(4) << towards << ": " << V);
            }

        void
            build_variants(graph_type* g)
            {
                /* FIXME: anchoring should account for forests. Maybe return a map [colour] => { [size_t] => node_index_type } */
                std::map<size_t, node_index_type> all_incoming;
                MSG_DEBUG("Anchor points " << g->anchor_points);
                for (const auto& ext_anchor: g->anchor_points) {
                    node_index_type external = g->parent->resolve(ext_anchor.first);
                    if (external != 0) {
                        MSG_DEBUG("ext " << ext_anchor.first << " anchor " << ext_anchor.second);
                        MSG_QUEUE_FLUSH();
                        all_incoming[parent->get_message_index(external, g->index_in_parent)] = g->resolve(ext_anchor.second);
                    }
                }
                MSG_DEBUG("ALL INCOMING " << all_incoming);
                if (g->parent) {
                    auto nei_nodes = g->parent->all_nei(g->index_in_parent);
                    for (size_t i = 0; i < nei_nodes.size(); ++i) {
                        std::map<size_t, node_index_type> incoming;
                        node_index_type output = 0;
                        for (const auto& ext_anchor: g->anchor_points) {
                            node_index_type external = g->parent->resolve(ext_anchor.first);
                            if (external != 0 && external != nei_nodes[i]) {
                                incoming[parent->get_message_index(external, g->index_in_parent)] = ext_anchor.second;
                            } else {
                                output = ext_anchor.second;
                            }
                        }
                        build_variant(nei_nodes[i], g, incoming, output);
                    }
                }
                build_variant(0, g, all_incoming, 0);  /* to compute full state later, when all messages are computed. */
            }
    };

    std::unique_ptr<instance_type>
        instance()
        {
            return std::unique_ptr<instance_type>(new instance_type(this));
        }
};


inline
std::ostream&
operator << (std::ostream& os, const edge_type& e)
{
    std::stringstream tmp;
    tmp << (e.graph->is_interface(e.first) ? 'I' : 'F') << e.graph->variables_of(e.first)
        << "->"
        << (e.graph->is_interface(e.second) ? 'I' : 'F') << e.graph->variables_of(e.second);
    return os << tmp.str();
}




#endif

