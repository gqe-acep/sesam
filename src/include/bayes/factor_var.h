/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_BAYES_FACTOR_VAR_H_
#define _SPEL_BAYES_FACTOR_VAR_H_


#include "nd_matrix.h"
#include "graph2.h"


typedef nd_dense_matrix var_backend_type;

struct var_type {
    size_t m_label;
    nd_dense_matrix m_state;

    var_backend_type(size_t label, size_t n_par, size_t n_al)
        : m_label(label), m_state({n_par, n_par, n_al, n_al})
    {}

    nd_dense_matrix& state() { return m_state; }
    const nd_dense_matrix& state() const { return m_state; }
    nd_dense_matrix parental_origin_probabilities() const
    {
        static std::vector<size_t> al_dim = {2, 3};
        return sum_over(m_state, al_dim);
    }
    size_t label() const { return m_label; }
};


enum FactorType {
    FObs=0,
    FCross,
    FSelf,
    FAncestor,
    FDH,
    _FactorType_Count
};


static size_t novar;

namespace direction_data {
    /* transform a sub-varset into an index */
    std::unordered_map<std::pair<size_t, size_t>, size_t>
        varset_to_direction;
    std::vector<std::pair<size_t, size_t>>
        direction_to_input_varset_binary,
        direction_to_output_varset_binary,
        direction_to_input_varset_ternary,
        direction_to_output_varset_ternary;
    /* which dimensions to sum over to emit a message in the given direction */
    std::vector<std::vector<size_t>>
        direction_to_sum_over_binary,
        direction_to_sum_over_ternary;
    /* which dimensions to map a sub-varset to in the hyper-product with a factor */
    std::vector<std::vector<std::pair<size_t, size_t>>>
        direction_to_product_binary,
        direction_to_product_ternary;
}



struct factor_type {
    size_t m_label;
    FactorType m_type;
    size_t m_variables[3];
    /*std::vector<size_t> m_variables;*/
    factor_type(size_t label, FactorType type, size_t var1)
        : m_label(label), m_type(type), m_variables({var1, novar, novar})
    {}
    factor_type(size_t label, FactorType type, size_t var1, size_t var2)
        : m_label(label), m_type(type), m_variables({var1, var2, novar})
    {}
    factor_type(size_t label, FactorType type, size_t var1, size_t var2, size_t var3)
        : m_label(label), m_type(type), m_variables({var1, var2, var3})
    {}
    FactorType type() const { return m_type; }
    size_t arity() const
    {
        switch (m_type) {
            case FObs:
            case FSelf:
            case FAncestor:
            case FDH:
                return 2;
            case FCross:
                return 3;
            default:
                return 0;
        };
    }
};


static inline nd_dense_matrix new_obs(size_t n_par, size_t n_al, double noise);
static inline nd_sparse_matrix new_cross_prob(size_t n_par, size_t n_al, bool m_before_p);
static inline nd_sparse_matrix new_ancestor_prob(size_t a, size_t n_par, size_t n_al);
static inline nd_sparse_matrix new_dh_prob(size_t n_par, size_t n_al);
static inline nd_sparse_matrix new_self_prob(size_t n_par, size_t n_al);


struct bayesian_network {
    std::vector<var_type> m_variables;
    std::vector<factor_type> m_factors;
    size_t m_n_par;
    size_t m_n_al;
    double m_noise;
    std::vector<std::vector<nd_sparse_matrix>> m_factor_map;

    bayesian_network(size_t n_par, size_t n_al, double noise)
        : m_variables(), m_n_par(n_par), m_n_al(n_al)
          , m_factor_map()
    {
        init_factor_map();
    }

    size_t
        new_var()
        {
            m_variables.emplace_back(m_variables.size(), m_n_par, m_n_al);
            /* init as uniform */
            nd_dense_matrix::cached_access ac = m_variables.back().state().accessor();
            size_t sz = m_variables.back().state().size()
                double v = 1. / sz;
            for (size_t i = 0; i < sz; ++i) { ac.put(i, v); }
            return m_variables.back().label();
        }

    size_t
        new_ancestor(size_t ancestor)
        {
            m_factors.emplace_back(m_factors.size(), FAncestor, ancestor);
            return m_factors.back().label();
        }

    size_t
        new_obs(size_t var1, size_t obs)
        {
            m_factors.emplace_back(m_factors.size(), FObs, var1, obs);
            return m_factors.back().label();
        }

    size_t
        new_selfing(size_t parent1, size_t child)
        {
            m_factors.emplace_back(m_factors.size(), FSelf, parent1, child);
            return m_factors.back().label();
        }

    size_t
        new_dhing(size_t parent1, size_t child)
        {
            m_factors.emplace_back(m_factors.size(), FDH, parent1, child);
            return m_factors.back().label();
        }

    size_t
        new_crossing(size_t parent1, size_t parent2, size_t child)
        {
            m_factors.emplace_back(m_factors.size(), FCross, parent1, parent2, child);
            return m_factors.back().label();
        }

    const
        nd_dense_matrix& belief(size_t var) const
        {
            return m_variables[var].state();
        }

    nd_dense_matrix
        parental_origin_belief(size_t var) const
        {
            return m_variables[var].parental_origin_probabilities();
        }

    std::pair<size_t, size_t>
        compute_separator(size_t factor1, size_t factor2)
        {
            std::pair<size_t, size_t> ret = {novar, novar};
            for (size_t v1: m_factors[factor1].m_variables) {
                for (size_t v2: m_factors[factor2].m_variables) {
                    if (v1 == v2) {
                        if (ret.first == novar) {
                            ret.first = v1;
                        } else {
                            ret.second = v1;
                        }
                    }
                }
            }
            return ret;
        }

    size_t
        compute_direction(size_t factor, const std::pair<size_t, size_t>& separator)
        {
            size_t ret = 0;
            for (size_t i = 0; i < 3; ++i) {
                size_t v1 = m_factors[factor1].m_variables[i];
                if (v1 == separator.first) {
                    ret += 3*i;
                } else if (v2 == separator.second) {
                    ret += i;
                }
            }
            return ret;
        }

    void init_factor_map()
    {
        m_factor_map.resize(_FactorType_Count);
        {
            nd_sparse_matrix f = new_obs(m_n_par, m_n_al, m_noise);
            m_factor_map[FObs].emplace_back(normalize_over(f, m_direction_sum[0]));
            m_factor_map[FObs].emplace_back(normalize_over(f, m_direction_sum[1]));
        }
        {
            m_factor_map[FAncestor].emplace_back(new_ancestor_prob(m_n_par, m_n_al, m_noise));
        }
        {
            nd_sparse_matrix f = new_self_prob(m_n_par, m_n_al);
            m_factor_map[FObs].emplace_back(normalize_over(f, m_direction_sum[0]));
            m_factor_map[FObs].emplace_back(normalize_over(f, m_direction_sum[1]));
        }
        {
            nd_sparse_matrix f = new_dh_prob(m_n_par, m_n_al);
            m_factor_map[FObs].emplace_back(normalize_over(f, m_direction_sum[0]));
            m_factor_map[FObs].emplace_back(normalize_over(f, m_direction_sum[1]));
        }
        {
            nd_sparse_matrix f = new_cross_prob(m_n_par, m_n_al);
            m_factor_map[FObs].emplace_back(normalize_over(f, m_direction_sum[0]));
            m_factor_map[FObs].emplace_back(normalize_over(f, m_direction_sum[1]));
            m_factor_map[FObs].emplace_back(normalize_over(f, m_direction_sum[2]));
            m_factor_map[FObs].emplace_back(normalize_over(f, m_direction_sum[3]));
            m_factor_map[FObs].emplace_back(normalize_over(f, m_direction_sum[4]));
            m_factor_map[FObs].emplace_back(normalize_over(f, m_direction_sum[5]));
        }
    }

    /* trois opérations
     * - hyper_prod(F, v, dim...)
     * - hyper_prod(F, kron(v1, v2), dim...)
     * - hyper_prod(F, v1xv2, dim...)
     *
     * trois configurations pour l'entrée
     * v, (v1, v2), v1xv2
     * la récupération de v/v1,v2/v1xv2 doit être automatique via la direction (et le type de message ENTRANT correspondant)
     * ça définit aussi le type de message SORTANT (v -> single binary ou joint ternary, k(v1, v2) / v1xv2 -> single)
     *
     * donc pour définir le type de message :
     * ENTRANT N_VARS SORTANT
     * -       1      Single
     * Single  2      Single
     * Single  3      Joint
     * Kron    3      Single
     * Joint   3      Single
     *
     * comment créer un message sortant (requête de VAR_SET sur ce facteur) ?
     *  * Unary
     *      retourne (Single, facteur * VARIABLE)
     *  * Binary
     *      retourne (Single, facteur * message(AUTRE FACTEUR LIÉ, AUTRE VARIABLE))
     *  * Ternary
     *      si |VAR_SET|==2 :
     *          retourne (Single, facteur * message(AUTRE FACTEUR LIÉ, AUTRE VARIABLE))
     *      si |VAR_SET|==1 :
     *          si AUTRES VARIABLES LIÉES AU MÊME FACTEUR :
     *              retourne (Joint, message(AUTRE FACTEUR LIÉ, AUTRES VARIABLES))
     *          sinon :
     *              retourne (Joint, facteur * kron(message(AUTRE FACTEUR VAR 1, VAR 1), message(AUTRE FACTEUR VAR 2, VAR 2)))
     *
     * à définir :
     * VARIABLE
     *      trivial : state() de var_type
     * AUTRE VARIABLE
     *      binaire :
     *          (#facteur X #variable) => #variable
     * AUTRE FACTEUR LIÉ
     *      un accesseur doit donner le # du facteur donnant telle variable
     *      (#facteur X #variable) => #facteur
     * AUTRES VARIABLES LIÉES AU MÊME FACTEUR
     *      simple test si facteur(var1) == facteur(var2) ?
     * AUTRE FACTEUR VARx
     *      un accesseur doit donner le # du facteur donnant telle variable
     *
     * si on reformule en termes de DIRECTION (la/les variable(s) de sortie) :
     * création de message
     * * Unary
     *      retourne (Single, facteur * VARIABLE)
     * * Binary
     *      retourne (Single, facteur[DIRECTION] * message(INPUT_FACTOR[DIRECTION][0], INPUT_DIRECTION[DIRECTION][0])
     * * Ternary
     *      si UNIQUE_FACTOR[DIRECTION] :
     *          retourne (Joint, facteur[DIRECTION] * message(INPUT_FACTOR[DIRECTION][0], INPUT_DIRECTION[DIRECTION]))
     *      sinon :
     *          retourne (Joint, facteur[DIRECTION] * kron(message(INPUT_FACTOR[DIRECTION][0], INPUT_DIRECTION[DIRECTION][0]),
     *                                                     message(INPUT_FACTOR[DIRECTION][1], INPUT_DIRECTION[DIRECTION][1])))
     *
     * à définir :
     * VARIABLE
     *      trivial : state() de var_type
     * DIRECTION
     *      identifiant du var_set en sortie (résultat de la query) pour un facteur.
     *      cf les tableaux m_direction_*
     *      représente les var_sets possibles localement à chaque facteur (l'ordre des variables importe !)
     *      facteur unaire :
     *          pas de direction
     *      facteur binaire :
     *          0 => 0
     *          1 => 1
    *      facteur ternaire :
        *          (1 x 2) => 0 : 0
        *          (0 x 2) => 1 : 1
        *          (0 x 1) => 2 : 2
        *          (2 x 1) => 0 : 3
        *          (2 x 0) => 1 : 4
        *          (1 x 0) => 2 : 5
        *          (2) => 0 x 1 : 6
        *          (1) => 0 x 2 : 7
        *          (0) => 1 x 2 : 8
        * INPUT_FACTOR
        *      tableau qui donne (DIRECTION) => #facteur
        * INPUT_DIRECTION
        *      tableau qui donne (DIRECTION) => direction correspondante dans le facteur retourné par INPUT_FACTOR ci-dessus
        * UNIQUE_FACTOR
        *      tableau qui donne (DIRECTION) => true/false.
        *
        * si on met à plat, qu'on enlève la récursion pour faire de l'itératif :
        * message entrant est v ou (v1, v2) ou v1xv2.
        * création de message
        * * Unary
        *      retourne facteur * VARIABLE
        * * Binary
        *      retourne facteur[DIRECTION] * INPUT_VAR[#facteur][DIRECTION]
        * * Ternary
        *      si UNIQUE_FACTOR[DIRECTION] :
        *          retourne facteur[DIRECTION] * message(INPUT_FACTOR[DIRECTION][0], INPUT_DIRECTION[DIRECTION][0])
        *      sinon :
        *          retourne facteur[DIRECTION] * kron(INPUT_VAR[#facteur][DIRECTION][0], INPUT_VAR[#facteur][DIRECTION][1])
        *
        * il faudrait à la création du graphe de facteurs stocker, au-delà des variables, les variables jointes qui lient des facteurs
        * => le label sur une arête est l'indice du varset correspondant
        * => le label sur un noeud est l'indice du facteur
        */

        struct factor_graph_edge_type {
            enum MessageType { MTSingle, MTKronecker, MTJoint };
            size_t m_factor[2];
            size_t m_bind_direction[2];
            MessageType m_type[2];
        };

    void build_factorgraph()
    {

    }
};




    static inline
nd_dense_matrix new_obs(size_t n_par, size_t n_al, double noise)
{
    nd_dense_matrix ret({n_par, n_al, n_par, n_al, n_al, n_al});
    auto ac = ret.accessor();

    for (size_t p1 = 0; p1 < n_par; ++p1) {
        for (size_t a1 = 0; a1 < n_al; ++a1) {
            for (size_t p2 = 0; p2 < n_par; ++p2) {
                for (size_t a2 = 0; a2 < n_al; ++a2) {
                    /*size_t idxv = idx_to_ofs(dimv, {p1, a1, p2, a2});*/
                    for (size_t im = 0; im < n_al; ++im) {
                        for (size_t jm = 0; jm < n_al; ++jm) {
                            /*size_t idxo = idx_to_ofs(dimo, {im, jm});*/
                            if (a1 == im && a2 == jm) {
                                /*factor_set(F, 1. - noise, {V == idxv, O == idxo});*/
                                ac.put({p1,a1, p2,a2, im,jm}, 1. - noise);
                            } else {
                                /*factor_set(F, noise / (n_al * n_al - 1), {V == idxv, O == idxo});*/
                                ac.put({p1,a1, p2,a2, im,jm}, noise / (n_al * n_al - 1));
                            }
                        }
                    }
                }
            }
        }
    }

    return ret;
}


    static inline
nd_sparse_matrix new_ancestor_prob(size_t a, size_t n_par, size_t n_al)
{
    nd_sparse_matrix ret({n_par, n_par, n_al, n_al});
    auto ac = ret.accessor();
    double v = 1. / n_al;
    for (size_t i = 0; i < n_al; ++i) {
        ac.put({a, a, i, i}, v);
    }
    return ret;
}


    static inline
nd_sparse_matrix new_cross_prob(size_t n_par, size_t n_al, bool m_before_p)
{
    nd_sparse_matrix ret({n_par, n_par, n_al, n_al, n_par, n_par, n_al, n_al, n_par, n_par, n_al, n_al});
    auto ac = ret.accessor();
    if (m_before_p) {
        for (size_t pa2 = 0; pa2 < n_al; ++pa2) {
            for (size_t pa1 = 0; pa1 < n_al; ++pa1) {
                for (size_t pp2 = 0; pp2 < n_par; ++pp2) {
                    for (size_t pp1 = 0; pp1 < n_par; ++pp1) {
                        for (size_t ma2 = 0; ma2 < n_al; ++ma2) {
                            for (size_t ma1 = 0; ma1 < n_al; ++ma1) {
                                for (size_t mp2 = 0; mp2 < n_par; ++mp2) {
                                    for (size_t mp1 = 0; mp1 < n_par; ++mp1) {
                                        size_t idx1 = ret.idx_to_ofs({mp1,mp2,ma1,ma2, pp1,pp2,pa1,pa2, mp1,pp1,ma1,pa1});
                                        size_t idx2 = ret.idx_to_ofs({mp1,mp2,ma1,ma2, pp1,pp2,pa1,pa2, mp1,pp2,ma1,pa2});
                                        size_t idx3 = ret.idx_to_ofs({mp1,mp2,ma1,ma2, pp1,pp2,pa1,pa2, mp2,pp1,ma2,pa1});
                                        size_t idx4 = ret.idx_to_ofs({mp1,mp2,ma1,ma2, pp1,pp2,pa1,pa2, mp2,pp2,ma2,pa2});
                                        ac.put(idx1, .25 + ac.get(idx1));
                                        ac.put(idx2, .25 + ac.get(idx2));
                                        ac.put(idx3, .25 + ac.get(idx3));
                                        ac.put(idx4, .25 + ac.get(idx4));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        for (size_t pa2 = 0; pa2 < n_al; ++pa2) {
            for (size_t pa1 = 0; pa1 < n_al; ++pa1) {
                for (size_t pp2 = 0; pp2 < n_par; ++pp2) {
                    for (size_t pp1 = 0; pp1 < n_par; ++pp1) {
                        for (size_t ma2 = 0; ma2 < n_al; ++ma2) {
                            for (size_t ma1 = 0; ma1 < n_al; ++ma1) {
                                for (size_t mp2 = 0; mp2 < n_par; ++mp2) {
                                    for (size_t mp1 = 0; mp1 < n_par; ++mp1) {
                                        size_t idx1 = ret.idx_to_ofs({pp1,pp2,pa1,pa2, mp1,mp2,ma1,ma2, mp1,pp1,ma1,pa1});
                                        size_t idx2 = ret.idx_to_ofs({pp1,pp2,pa1,pa2, mp1,mp2,ma1,ma2, mp1,pp2,ma1,pa2});
                                        size_t idx3 = ret.idx_to_ofs({pp1,pp2,pa1,pa2, mp1,mp2,ma1,ma2, mp2,pp1,ma2,pa1});
                                        size_t idx4 = ret.idx_to_ofs({pp1,pp2,pa1,pa2, mp1,mp2,ma1,ma2, mp2,pp2,ma2,pa2});
                                        ac.put(idx1, .25 + ac.get(idx1));
                                        ac.put(idx2, .25 + ac.get(idx2));
                                        ac.put(idx3, .25 + ac.get(idx3));
                                        ac.put(idx4, .25 + ac.get(idx4));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    /*return ret.as_sparse();*/
    return ret;
}


    static inline
nd_sparse_matrix new_dh_prob(size_t n_par, size_t n_al)
{
    /* TODO reorder papa->ppaa */
    nd_sparse_matrix ret({n_par, n_al, n_par, n_al, n_par, n_al, n_par, n_al});
    auto ac = ret.accessor();
    for (size_t ma2 = 0; ma2 < n_al; ++ma2) {
        for (size_t mp2 = 0; mp2 < n_par; ++mp2) {
            for (size_t ma1 = 0; ma1 < n_al; ++ma1) {
                for (size_t mp1 = 0; mp1 < n_par; ++mp1) {
                    size_t idx1 = ret.idx_to_ofs({mp1,ma1, mp2,ma2, mp1,ma1, mp1,ma1});
                    size_t idx2 = ret.idx_to_ofs({mp1,ma1, mp2,ma2, mp2,ma2, mp2,ma2});
                    ac.put(idx1, .5 + ac.get(idx1));
                    ac.put(idx2, .5 + ac.get(idx2));
                }
            }
        }
    }
    return ret;
}


    static inline
nd_sparse_matrix new_self_prob(size_t n_par, size_t n_al)
{
    nd_sparse_matrix ret({n_par, n_al, n_par, n_al, n_par, n_al, n_par, n_al});
    auto ac = ret.accessor();
    for (size_t g2a2 = 0; g2a2 < n_al; ++g2a2) {
        for (size_t g2a1 = 0; g2a1 < n_al; ++g2a1) {
            for (size_t g2p2 = 0; g2p2 < n_par; ++g2p2) {
                for (size_t g2p1 = 0; g2p1 < n_par; ++g2p1) {
                    for (size_t g1a2 = 0; g1a2 < n_al; ++g1a2) {
                        for (size_t g1a1 = 0; g1a1 < n_al; ++g1a1) {
                            for (size_t g1p2 = 0; g1p2 < n_par; ++g1p2) {
                                for (size_t g1p1 = 0; g1p1 < n_par; ++g1p1) {  /* sic */
                                    size_t i;
                                    /*#define gpa(_g_, _i_) g##_g_##p##_i_, g##_g_##a##_i_*/
                                    /*i = ret.idx_to_ofs({gpa(2, 2), gpa(2, 1), gpa(1, 2), gpa(1, 1)});*/
                                    /*#undef gpa*/
                                    i = ret.idx_to_ofs({g1p2,g1p1, g1a2, g1a1, g2p2,g2p1, g2a2,g2a1});
                                    if (g1p1 == g1p2 && g1a1 == g1a2 && g2p1 == g2p2 && g2a1 == g2a2 && g2p1 == g1p1 && g2a1 == g1a1) {
                                        ac.put(i, 1);
                                    } else if (g1p1 == g2p1 && g1a1 == g2a1 && g1p2 == g2p2 && g1a2 == g2a2) {
                                        ac.put(i, .25);
                                    } else if (g1p2 == g2p1 && g1a2 == g2a1 && g1p2 == g2p2 && g1a2 == g2a2) {
                                        ac.put(i, .25);
                                    } else if (g1p2 == g2p1 && g1a2 == g2a1 && g1p1 == g2p2 && g1a1 == g2a2) {
                                        ac.put(i, .25);
                                    } else if (g1p1 == g2p1 && g1a1 == g2a1 && g1p1 == g2p2 && g1a1 == g2a2) {
                                        ac.put(i, .25);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return ret;
}

#endif

