/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_BAYES_GRAPH_H_
#define _SPEL_BAYES_GRAPH_H_

#include <list>
#include <unordered_set>
#include <unordered_map>
#include <boost/dynamic_bitset.hpp>

typedef boost::dynamic_bitset<> bitset;


struct bool_matrix {
    std::vector<bitset> by_row;
    std::vector<bitset> by_col;
    bool_matrix(size_t M, size_t N)
        : by_row(), by_col()
    {
        by_row.reserve(M);
        by_col.reserve(N);
        for (size_t r = 0; r < M; ++r) {
            by_row.emplace_back(N);
        }
        for (size_t c = 0; c < M; ++c) {
            by_col.emplace_back(M);
        }
    }

    size_t ncols() const { return by_col.size(); }
    size_t nrows() const { return by_row.size(); }

    bool check_consistency() const
    {
        size_t M = nrows();
        size_t N = ncols();
        for (size_t i = 0; i < M; ++i) {
            if (by_row[i].size() != N) {
                throw std::runtime_error("Wrong row size");
            }
        }
        for (size_t j = 0; j < M; ++j) {
            if (by_col[j].size() != M) {
                throw std::runtime_error("Wrong col size");
            }
        }
        for (size_t i = 0; i < M; ++i) {
            for (size_t j = 0; j < N; ++j) {
                if (by_row[i].test(j) ^ by_col[j].test(i)) {
                    throw std::runtime_error("Matrix representations differ");
                }
            }
        }
        return true;
    }

    struct bit_ref {
        bool_matrix& mat;
        size_t i, j;
        bit_ref& operator = (bool b) { mat.by_row[i].set(j, b); mat.by_col[j].set(i, b); return *this; }
        operator bool () const { return mat.by_row[i].test(j); }
    };

    struct const_bit_ref {
        const bool_matrix& mat;
        size_t i, j;
        operator bool () const { return mat.by_row[i].test(j); }
    };

    struct col_ref {
        bool_matrix& mat;
        size_t j;
        operator bitset& () { return mat.by_col[j]; }
        operator const bitset& () const { return mat.by_col[j]; }
        bit_ref operator () (size_t i) { return {mat, i, j}; }
        template <typename COL_OR_ROW>
        col_ref& operator = (const COL_OR_ROW& cor) {
            const bitset& bs = cor;
            mat.by_col[j] = bs;
            for (size_t b = bs.find_first(); b != bitset::npos; b = bs.find_next(b)) {
                mat.by_row[b].set(j);
            }
            return *this;
        }
    };

    struct const_col_ref {
        const bool_matrix& mat;
        size_t j;
        operator const bitset& () const { return mat.by_col[j]; }
        const_bit_ref operator () (size_t i) { return {mat, i, j}; }
    };

    struct row_ref {
        bool_matrix& mat;
        size_t i;
        operator bitset& () { return mat.by_row[i]; }
        operator const bitset& () const { return mat.by_row[i]; }
        bit_ref operator () (size_t j) { return {mat, i, j}; }
        template <typename COL_OR_ROW>
        row_ref& operator = (const COL_OR_ROW& cor) {
            const bitset& bs = cor;
            mat.by_row[i] = bs;
            for (size_t b = bs.find_first(); b != bitset::npos; b = bs.find_next(b)) {
                mat.by_col[b].set(i);
            }
            return *this;
        }
    };

    struct const_row_ref {
        const bool_matrix& mat;
        size_t i;
        operator const bitset& () const { return mat.by_row[i]; }
        const_bit_ref operator () (size_t j) { return {mat, i, j}; }
    };

    bit_ref operator () (size_t i, size_t j) { return {*this, i, j}; }
    col_ref col(size_t j) { return {*this, j}; }
    row_ref row(size_t i) { return {*this, i}; }
    const_bit_ref operator () (size_t i, size_t j) const { return {*this, i, j}; }
    const_col_ref col(size_t j) const { return {*this, j}; }
    const_row_ref row(size_t i) const { return {*this, i}; }

    friend
        std::ostream& operator << (std::ostream& os, const bool_matrix& bm)
        {
            for (const bitset& r: bm.by_row) {
                std::string s;
                to_string(r, s);
                auto i = s.rbegin();
                auto j = s.rend();
                for (; i != j; ++i) {
                    os << (*i);
                }
                os << std::endl;
            }
            /*size_t N = bm.by_row.front().size();*/
            /*for (; N > 0; --N) {*/
                /*os << '-';*/
            /*}*/
            /*os << std::endl;*/
            /*for (const bitset& c: bm.by_col) {*/
                /*os << c << std::endl;*/
            /*}*/
            return os;
        }

    friend
        bool_matrix operator * (const bool_matrix& M1, const bool_matrix& M2)
        {
            if (M1.ncols() != M2.nrows()) {
                throw std::runtime_error("Incompatible matrix sizes for product");
            }
            size_t M = M1.nrows();
            size_t N = M2.ncols();
            bool_matrix ret(M, N);
            for (size_t i = 0; i < M; ++i) {
                const bitset& row = M1.by_row[i];
                for (size_t j = 0; j < N; ++j) {
                    const bitset& col = M2.by_col[j];
                    ret(i, j) = (row & col).any();
                }
            }
            return ret;
        }
};


typedef bool_matrix adjacency_matrix_type;

struct connectivity_matrix_type {
    bool_matrix m;

    connectivity_matrix_type(size_t N)
        : m(N, N)
    {
        for (size_t i = 0; i < N; ++i) {
            m(i, i) = true;
        }
    }
    void add_edge(size_t i, size_t j)
    {
        bitset joint = m.by_row[i] | m.by_col[j];
        for (size_t n = joint.find_first(); n != bitset::npos; n = joint.find_next(n)) {
            m.by_row[i] = joint;
            m.by_col[i] = joint;
        }
    }

    bool in_same_component(size_t i, size_t j)
    {
        return m.by_row[i].test(j) || m.by_row[j].test(i);
    }
};


static inline
bool operator < (const std::pair<size_t, size_t>& p1, const std::pair<size_t, size_t>& p2)
{
    return p1.first < p2.first || (p1.first == p2.first && p1.second < p2.second);
}


struct vertex_index_type;

struct graph {
    typedef size_t node_type;
    typedef double edge_payload_type;
    typedef std::unordered_set<node_type> vertex_set;
    /*typedef struct _edge {*/
        /*node_type dest;*/
        /*edge_payload_type weight;*/
        /*_edge(node_type nt, edge_payload_type ept) : dest(nt), weight(ept) {}*/
    /*} edge_type;*/

    struct sortable_edge_type {
        node_type from;
        node_type to;
        edge_payload_type weight;
        /*sortable_edge_type(node_type f, const edge_type& e) : from(f), to(e.dest), weight(e.weight) {}*/
        sortable_edge_type(node_type f, node_type t, edge_payload_type p) : from(f), to(t), weight(p) {}
        bool operator < (const sortable_edge_type& other) const { return weight > other.weight; }
    };

    /*typedef std::list<edge_type> edge_list_type;*/
    /*typedef std::unordered_map<node_type, edge_list_type> edge_map_type;*/
    /*edge_map_type edges;*/
    adjacency_matrix_type edges;
    std::map<std::pair<node_type, node_type>, edge_payload_type> edge_payloads;

    graph(size_t n_vertices)
        : edges(n_vertices, n_vertices)
        , edge_payloads()
    {}

    bool find_edge(node_type from, node_type to)
    {
        return edges(from, to);
        /*auto it = edges.find(from);*/
        /*if (it != edges.end()) {*/
            /*for (const auto& e: it->second) {*/
                /*if (e.dest == to) {*/
                    /*return true;*/
                /*}*/
            /*}*/
        /*}*/
        /*return false;*/
    }

    void
        add_edge(node_type from, node_type to, edge_payload_type w, bool directed=false)
        {
            edges(from, to) = true;
            edge_payloads[{from, to}] = w;
            if (!directed) {
                edges(to, from) = true;
                edge_payloads[{to, from}] = w;
            }
            /*if (!find_edge(from, to)) {*/
                /*edges[from].emplace_back(to, w);*/
            /*}*/
            /*if (!(directed || find_edge(to, from))) {*/
                /*edges[to].emplace_back(from, w);*/
            /*}*/
            /*vertices.insert(to);*/
            /*vertices.insert(from);*/
        }

#if 0
    bool
        edge_would_make_cycle(node_type from, node_type to) const
        {
            auto fi = vertices.find(from);
            auto ti = vertices.find(to);
            if (fi == vertices.end() || ti == vertices.end()) {
                return false;
            }
            std::unordered_set<node_type> visited;
            std::deque<node_type> stack;
    /**/
#define pop(_x) do { _x = stack.front(); stack.pop_front(); } while (0)
#define push(_x) stack.push_back(_x);
            node_type n;
            edge_map_type::const_iterator cur_edges;
            while (stack.size()) {
                pop(n);
                visited.insert(n);
                cur_edges = edges.find(n);
                for (const auto& e: cur_edges->second) {
                    if (e.dest == to) {
                        return true;
                    }
                    if (visited.find(e.dest) == visited.end()) {
                        push(e.dest);
                    }
                }
            }
            return false;
#undef push
#undef pop
        }
#endif

    graph
        MST()
        {
            graph ret(edges.nrows());
            std::vector<sortable_edge_type> sorted_edges;
            size_t sz = 0;
            for (const auto& el: edges.by_row) {
                sz += el.count();
            }
            sorted_edges.reserve(sz);
            size_t i = 0;
            for (const auto& el: edges.by_row) {
                for (size_t j = el.find_first(); j != bitset::npos; j = el.find_next(j)) {
                    sorted_edges.emplace_back(i, j, edge_payloads[{i, j}]);
                }
            }
            /*for (const auto& kv: edges) {*/
                /*for (const auto& e: kv.second) {*/
                    /*sorted_edges.emplace_back(kv.first, e);*/
                /*}*/
            /*}*/
            std::sort(sorted_edges.begin(), sorted_edges.end());
            connectivity_matrix_type cm(edges.nrows());
            vertex_set vertices;
            for (const auto& e: sorted_edges) {
                if (!cm.in_same_component(e.from, e.to)) {
                    ret.add_edge(e.from, e.to, e.weight);
                    cm.add_edge(e.from, e.to);
                    vertices.insert(e.from);
                    vertices.insert(e.to);
                }
                if (vertices.size() == edges.nrows()) {
                    break;
                }
            }
            /*for (const auto& e: sorted_edges) {*/
                /*if (!ret.edge_would_make_cycle(e.from, e.to)) {*/
                    /*ret.add_edge(e.from, e.to, e.weight);*/
                /*}*/
                /*if (ret.vertices.size() == vertices.size()) {*/
                    /*break;*/
                /*}*/
            /*}*/
            return ret;
        }

    std::ostream& to_dot(std::ostream& os, const vertex_index_type& vi);
};


struct vertex_index_type {
    std::map<graph::node_type, size_t> m_to_index;
    std::map<size_t, graph::node_type> m_to_pedigree;
    size_t to_index(size_t n) const { return m_to_index.find(n)->second; }
    graph::node_type to_pedigree(size_t n) const { return m_to_pedigree.find(n)->second; }
    size_t size() const { return m_to_index.size(); }
};



inline
std::ostream& graph::to_dot(std::ostream& os, const vertex_index_type& vi)
{
    os << "graph \"" << (this) << "\" {" << std::endl;
    size_t i = 0;
    for (const auto& el: edges.by_row) {
        for (size_t j = el.find_first(); j != bitset::npos; j = el.find_next(j)) {
            if (j <= i) { continue; }
            os << vi.to_pedigree(i) << " -- " << vi.to_pedigree(j);
            /*os << " [label=\"" << edge_payloads[{i, j}] << "\"];" << std::endl;*/
            os << ';' << std::endl;
        }
        ++i;
    }
    /*for (const auto& el: edges) {*/
    /*for (const auto& e: el.second) {*/
    /*os << el.first << " -> " << e.dest;*/
    /*os << " [label=\"" << e.weight << "\"];" << std::endl;*/
    /*}*/
    /*}*/
    return os << "}" << std::endl;
}



#endif

