/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_BAYES_GENERALIZED_PRODUCT_H_
#define _SPELL_BAYES_GENERALIZED_PRODUCT_H_

#include "pedigree.h"


#include <cstring>
#include <unordered_map>
#include <boost/dynamic_bitset.hpp>
#include <gmp.h>

/* La spec est dans le code. */



/* Gère un curseur incrémentable multi-dimensionnel et les différentes opérations nécessaires desuss. */
struct state_index_type {
    /* TODO make data an array in case we need more than 64 bits of indexing space. */
    typedef uint64_t block_type;
    static constexpr size_t n_blocks = 4;
    std::array<block_type, n_blocks> data;


    inline
    state_index_type&
        operator = (const state_index_type& other)
        {
            for (size_t i = 0; i < n_blocks; ++i) {
                data[i] = other.data[i];
            }
            return *this;
        }

    inline
    state_index_type&
        operator = (uint64_t value)
        {
            data[0] = value;
            for (size_t i = 1; i < n_blocks; ++i) {
                data[i] = 0;
            }
            return *this;
        }

    inline
    size_t
        hash() const
        {
            size_t ret = std::hash<block_type>()(data[0]);
            for (size_t i = 0; i < n_blocks; ++i) {
                ret = (ret * 0xd3adb3ef) ^ std::hash<block_type>()(data[i]);
            }
            return ret;
        }

    inline
    void reset() { *this = 0; }

    /* Aide à la compilation des données nécessaires à calculer un produit.
     * Donne le bitshift suivant en s'assurant qu'on ne franchit pas une barrière de 64 bits.
     * Si un indice devait être encodé à cheval sur deux mots, son extraction et son insertion seraient très coûteuses.
     */
    inline
    static
        size_t
        next_shift(size_t current_shift, size_t next_bit_width)
        {
            size_t base_word_ofs = current_shift >> 6;
            size_t ofs_end = (current_shift + next_bit_width) >> 6;
            if (base_word_ofs != ofs_end) {
                /* crossing a 64-bit boundary */
                return ofs_end << 6;
            } else {
                return current_shift;
            }
        }

    /* Calcule le masque local (au mot concerné) pour un indice (bitshift, bitwidth) donné. */
    inline
    static
        block_type
        mask(size_t shift, size_t bit_width)
        {
            /* guaranteed to not cross a 64-bit boundary if shift is given by next_shift() */
            return ((1 << bit_width) - 1) << (shift & 0x3f);
        }

    /* Seuls domain_descr et table_descr ont le droit de manipuler les indices au-delà des opérations de base. */
    friend struct domain_descr;
    friend struct table_descr;

    inline
    bool operator == (const state_index_type& other) const { return data == other.data; }
    inline
    bool operator != (const state_index_type& other) const { return data != other.data; }
    inline
    bool operator > (const state_index_type& other) const { return data > other.data; }
    inline
    bool operator < (const state_index_type& other) const { return data < other.data; }
    inline
    bool operator >= (const state_index_type& other) const { return data >= other.data; }
    inline
    bool operator <= (const state_index_type& other) const { return data <= other.data; }

    inline
    bool is_bad() const { return data[n_blocks - 1] == (uint64_t) -1; }

    static constexpr
        state_index_type
        bad()
        {
            return {{{~0ULL, ~0ULL, ~0ULL, ~0ULL}}};
        }

    friend
    inline
        std::ostream&
        operator << (std::ostream& os, const state_index_type& s)
        {
            // MSG_DEBUG("printing state_index " << s.data);
            for (int i = n_blocks - 1; i >= 0; --i) {
                uint64_t mask = 1ULL << 63;
                while (mask && !(s.data[i] & mask)) { mask >>= 1; }
                while (mask) { os << ((s.data[i] & mask) == 0); mask >>= 1; }
            }
            return os;
        }

protected:
    /* accède au bon indice pour un shift donné */
    inline
        block_type&
        select_data(size_t shift)
        {
            return data[shift >> 6];
        }

    inline
        const block_type&
        select_data(size_t shift) const
        {
            return data[shift >> 6];
        }

    /* Donne la valeur max shiftée d'un indice, pour comparaison avec extract_unshifted() */
    static
    inline
        block_type
        max(size_t shift, size_t domain_size)
        {
            return (domain_size - 1) << (shift & 0x3f);
        }

    /* extrait un indice */
    inline
    uint64_t
        extract(size_t shift, block_type mask) const
        {
            /* data offset is shift >> 6 */
            return (select_data(shift) & mask) >> (shift & 0x3f);
        }

    inline
    void
        assign(size_t shift, block_type mask, uint64_t value)
        {
            select_data(shift) &= ~mask;
            select_data(shift) |= value << (shift & 0x3f);
        }

    /* Incrémente un indice et retourne carry */
    inline
    bool
        incr(size_t shift, block_type mask, block_type max)
        {
            auto& D = select_data(shift);
            block_type sub = (D & mask);
            if (sub == max) {
                D &= ~mask;
                return true;
            } else {
                D += 1 << (shift & 0x3f);
                return false;
            }
        }

    /* met un indice à zéro */
    inline
    void
        zero(size_t shift, block_type mask)
        {
            select_data(shift) &= ~mask;
        }

    /* fusionne deux curseurs étant donné un masque (utilise les dimensions de *this pour mask et d'other pour !mask) */
    inline
    state_index_type
        merge(const state_index_type& other, const state_index_type& mask) const
        {
            /* iterate over block index */
            state_index_type ret;
            for (size_t i = 0; i < n_blocks; ++i) {
                ret.data[i] = data[i] | (other.data[i] & ~mask.data[i]);
            }
            return ret;
        }

    /* extrait un indice mais économise le shift. à utiliser pour comparer avec max (voir incr()). */
    inline
    block_type
        extract_unshifted(size_t shift, block_type mask) const
        {
            return select_data(shift) & mask;
        }

    /* calcule le résultat de *this & m. Permet de passer d'un curseur global à un curseur local à une table. */
    inline
    state_index_type
        mask(const state_index_type& m) const
        {
            state_index_type ret;
            for (size_t i = 0; i < n_blocks; ++i) {
                ret.data[i] = data[i] & m.data[i];
            }
            return ret;
        }
};



namespace std {
    template <>
        struct hash<bn_label_type> {
            size_t operator () (const bn_label_type& l) const { return std::hash<int>()(l.compact()); }
        };

    template <>
        struct hash<genotype_comb_type::key_list> {
            size_t
                operator () (const genotype_comb_type::key_list& kl) const
                {
                    size_t accum = 0xdeadbe3f;
                    for (const auto& k: kl) {
                        accum *= std::hash<int>()(k.parent);
                        accum = (accum << 13) | (accum >> 19);
                        accum *= std::hash<bn_label_type>()(k.state);
                        accum = (accum << 19) | (accum >> 13);
                    }
                    return accum;
                }
        };

    template <>
        struct hash<state_index_type> {
            size_t operator () (const state_index_type& s) const { return s.hash(); }
        };
}



/* Permet d'itérer joliment sur la séquence inverse (rbegin->rend) avec la syntaxe for (v: container) {} */
template <typename CONTAINER> struct const_reversed_impl {
    const CONTAINER& ref;
    typedef typename std::decay<CONTAINER>::type::const_reverse_iterator iterator;
    iterator begin() { return ref.rbegin(); }
    iterator end() { return ref.rend(); }
};

template <typename CONTAINER> struct reversed_impl {
    const CONTAINER& ref;
    typedef typename std::decay<CONTAINER>::type::reverse_iterator iterator;
    iterator begin() { return ref.rbegin(); }
    iterator end() { return ref.rend(); }
};

template <typename CONTAINER> reversed_impl<CONTAINER> reversed(CONTAINER&& x) { return {x}; }
template <typename CONTAINER> const_reversed_impl<CONTAINER> reversed(const CONTAINER& x) { return {x}; }


/* Descripteur du domaine d'une variable participant au produit */
struct domain_descr {
    int variable_name;
    size_t size;
    size_t bit_width;
    size_t shift;
    state_index_type::block_type max;
    state_index_type::block_type mask;
    std::unordered_map<bn_label_type, size_t> label_to_index;
    std::vector<bn_label_type> index_to_label;

    inline
    uint64_t
        extract(const state_index_type& s) const
        {
            return s.extract(shift, mask);
        }

    inline
    void
        init_indexer(size_t last_shift)
        {
            shift = state_index_type::next_shift(last_shift, bit_width);
            max = state_index_type::max(shift, size);
            mask = state_index_type::mask(shift, bit_width);
            /*MSG_DEBUG("last_shift=" << last_shift << " last_bit_width=" << last_bit_width << " shift=" << shift << " max=" << max << " mask=" << mask);*/
        }

    inline
    void
        assign(state_index_type& cursor, uint64_t value) const
        {
            cursor.assign(shift, mask, value);
        }

    inline
    void
        assign_mask(state_index_type& cursor) const
        {
            cursor.assign(shift, mask, (1 << bit_width) - 1);
        }

    inline
    bool
        incr(state_index_type& cursor) const
        {
            return cursor.incr(shift, mask, max);
        }

    inline
    void
        zero(state_index_type& cursor) const
        {
            cursor.zero(shift, mask);
        }

    inline
    uint64_t
        extract_unshifted(const state_index_type& cursor) const
        {
            return cursor.extract_unshifted(shift, mask);
        }
};


/* Descripteur d'une table participant au produit */
struct table_descr {
    const genotype_comb_type* data;
    std::vector<int> variable_names;
    std::vector<bool> variables_in_use;
    std::vector<bool> variables_left;
    std::vector<size_t> variable_indices;
    std::unordered_map<state_index_type, size_t> domain_to_offset;
    std::vector<state_index_type> offset_to_cursor;
    uint64_t offset;
    state_index_type mask;
    bool unif;
    double exponent;

    inline
    const genotype_comb_type::element_type&
        current_element() const
        {
            return data->m_combination[offset];
        }

    inline
    state_index_type
        local(const state_index_type& global) const
        {
            return global.mask(mask);
        }

    inline
    state_index_type
        merge(const state_index_type& local, const state_index_type& global) const
        {
            return local.merge(global, mask);
        }
};


/* L'algorithme à proprement parler. */
struct joint_variable_product_type {
    std::vector<domain_descr> domains;
    std::vector<table_descr> tables;
    std::vector<genotype_comb_type> owned_tables;
    std::vector<int> all_variable_names;
    std::vector<int> output_variable_names;
    std::vector<bool> output_variables_in_use;
    std::vector<std::pair<size_t, size_t>> key_extractors;

    state_index_type max_cursor;

    size_t total_bits;

    size_t last_variable_index;

    bool invalid_product = false;
    bool have_multiple_tables = false;

    const std::map<std::vector<int>, genotype_comb_type>* all_domains = nullptr;
    
    inline
    void
        set_output(const std::vector<int>& variables)
        {
            output_variable_names = variables;
        }

    inline
    std::pair<size_t, size_t>
        find_offsets_around_cursor(const table_descr& t, state_index_type local_cursor)
        {
            auto it = t.domain_to_offset.find(local_cursor);
            if (it != t.domain_to_offset.end()) {
                return {it->second, it->second};
            }
            if (local_cursor > t.offset_to_cursor.back()) {
                return {t.offset_to_cursor.size() - 1, 0};
            } else if (local_cursor < t.offset_to_cursor.front()) {
                return {0, t.offset_to_cursor.size() - 1};
            }
#if 0
            std::vector<state_index_type>::const_iterator min, max, cur;
            min = t.offset_to_cursor.begin();
            max = t.offset_to_cursor.end();
            while ((max - min) > 1) {
                cur = min + ((max - min) >> 1);
                if (*cur > local_cursor) {
                    max = cur;
                } else {
                    min = cur;
                }
            }
            return {min - t.offset_to_cursor.begin(), max - t.offset_to_cursor.begin()};
#else
            size_t min = 0, max = t.offset_to_cursor.size(), cur;
            while ((max - min) > 1) {
                cur = min + ((max - min) >> 1);
                if (t.offset_to_cursor[cur] > local_cursor) {
                    max = cur;
                } else {
                    min = cur;
                }
            }
            return {min, max};
#endif
        }

    /*state_index_type*/
        /*find_smallest_next_state(const table_descr& t, state_index_type global_cursor)*/
        /*{*/
            /*auto offsets = find_offsets_around_cursor(t, global_cursor & t.mask);*/
        /*}*/

    inline
    void
        get_coords_at(const table_descr& t, uint64_t offset, std::vector<size_t>& coords)
        {
            auto i = coords.begin();
            auto vi = t.variable_indices.begin();
            for (const auto& k: t.data->m_combination[offset].keys) {
                /**i++ = k.state;*/
                /**i++ = domains[*vi++].label_to_index.find(k.state)->second;*/
                *i = domains[*vi].label_to_index.find(k.state)->second;
                ++i;
                ++vi;
            }
        }

    inline
    std::string
        dump(state_index_type cursor)
        {
            if (cursor.is_bad()) {
                return "<BAD INDEX>";
            }
            static std::vector<size_t> ret;
            ret.resize(domains.size());
            compute_coordinates(cursor, ret);
            //// DEBUG : quick (and dirty) workaround to deal with clang++ issue on macos
            ////return SPELL_STRING(WHITE << ret << NORMAL);
            return SPELL_STRING(WHITE << vec_to_spell_string(ret) << NORMAL);
        }

    inline
    std::string
        dump(state_index_type cursor, const table_descr& t)
        {
            if (cursor.is_bad()) {
                return "<BAD INDEX>";
            }
            std::stringstream ss;
            auto di = domains.begin();
            bool first = true;
            for (bool in_use: t.variables_in_use) {
                auto index = di->extract(cursor);
                if (!first) {
                    ss << ' ';
                } else {
                    first = false;
                }
                if (in_use) {
                    ss << GREEN << index << NORMAL;
                } else {
                    ss << index;
                }
                ++di;
            }
            return ss.str();
        }

    inline
    bool
        move_cursor_if_state_is_valid(table_descr& t, state_index_type cursor)
        {
//             MSG_DEBUG("table @" << t.data << " cursor=" << dump(cursor));
            auto it = t.domain_to_offset.find(t.local(cursor));
            if (it == t.domain_to_offset.end()) {
//                 MSG_DEBUG("not found");
                return false;
            }
//             MSG_DEBUG("found");
            t.offset = it->second;
            return true;
        }

    inline
    void
        add_table_impl(const genotype_comb_type& t, std::vector<int>& vars)
        {
            tables.emplace_back();
            tables.back().data = &t;
            tables.back().variable_names.swap(vars);
            tables.back().exponent = 1.;
            MSG_DEBUG("[joint_product] Added table " << (&t) << " " << t);
        }
        
    inline
    void
        add_table(const genotype_comb_type& t)
        {
            if (t.size() == 0) {
                invalid_product = true;
                return;
            }
            std::vector<int> vars;
            vars.reserve(t.m_combination[0].keys.size());
            for (auto k: t.m_combination[0].keys) {
                vars.push_back(k.parent);
            }
            for (auto& td: tables) {
                if (vars == td.variable_names && t == *td.data) {
                    td.exponent += 1.;
                    have_multiple_tables = true;
                    return;
                }
            }
            all_variable_names = all_variable_names + vars;
            add_table_impl(t, vars);
        }

    inline
    void
        precompute_multiple_tables()
        {
            std::vector<table_descr> tmp_tables;
            tmp_tables.swap(tables);
            owned_tables.reserve(tmp_tables.size());
            for (auto& t: tmp_tables) {
                if (t.exponent != 1.) {
                    MSG_DEBUG("Have table with multiplicity " << t.exponent);
                    MSG_DEBUG((*t.data));
                    owned_tables.emplace_back();
                    owned_tables.back() = *t.data;
                    MSG_DEBUG(owned_tables.back());
                    for (auto& e: owned_tables.back()) {
                        e.coef = pow(e.coef, t.exponent);
                    }
                    MSG_DEBUG(owned_tables.back());
                     add_table_impl(owned_tables.back(), t.variable_names);
                } else {
                     add_table_impl(*t.data, t.variable_names);
                }
            }
        }

    inline
    void
        compile(const std::map<std::vector<int>, genotype_comb_type>& all_variable_domains)
        {
            scoped_indent _("[jvp compile] ");
            compile_domains(all_variable_domains);
            compile_tables();
            compile_output();
        }

    inline
    void
        compile_domains(const std::map<std::vector<int>, genotype_comb_type>& all_variable_domains)
        {
            scoped_indent _("[domain] ");
            MSG_DEBUG(all_variable_domains);
            all_domains = &all_variable_domains;
            total_bits = 0;
            for (int v: all_variable_names) {
                const auto& domain = all_variable_domains.find(std::vector<int>{v})->second;
                domains.emplace_back();
                auto& d = domains.back();
                d.variable_name = v;
                d.size = domain.size();
                d.bit_width = 1;
                while ((1ULL << d.bit_width) < d.size) { ++d.bit_width; }
                total_bits += d.bit_width;
//                 MSG_DEBUG("Have domain for variable #" << d.variable_name << " size " << d.size << " bit_width " << d.bit_width);
                /*MSG_QUEUE_FLUSH();*/
                d.index_to_label.reserve(domain.size());
                for (size_t i = 0; i < domain.size(); ++i) {
                    const auto& label = domain.m_combination[i].keys.keys.front().state;
                    d.label_to_index[label] = i;
                    d.index_to_label.push_back(label);
                }
            }
            size_t shift = 0;
            max_cursor.reset();
            for (auto& d: reversed(domains)) {
                d.init_indexer(shift);
                /*d.max = (d.size - 1) << shift;*/
                /*d.mask = ((1 << d.bit_width) - 1) << shift;*/
                /*d.shift = shift;*/
                shift += d.bit_width;
                d.assign(max_cursor, d.max);
                /*MSG_QUEUE_FLUSH();*/
//                 MSG_DEBUG("Have domain for variable #" << d.variable_name << " size " << d.size << " bit_width " << d.bit_width << " mask " << boost::dynamic_bitset<uint64_t>(total_bits, d.mask) << " max " <<  boost::dynamic_bitset<uint64_t>(total_bits, d.max));
                /*MSG_QUEUE_FLUSH();*/
                if (0) {
                    std::stringstream ss;
                    for (const auto& kv: d.label_to_index) {
                        ss << ' ' << kv.first << ':' << kv.second;
                    }
                    MSG_DEBUG(ss.str());
                }
            }
            last_variable_index = all_variable_names.size() - 1;
        }

    inline
    void
        compile_tables()
        {
            scoped_indent _("[tables] ");
            if (have_multiple_tables) { precompute_multiple_tables(); }
            std::vector<size_t> coordinates;
            coordinates.reserve(all_variable_names.size());
            for (auto& tab: tables) {
                tab.variable_indices.reserve(tab.variable_names.size());
                tab.variables_in_use.resize(all_variable_names.size(), false);
                tab.mask.reset();
                for (size_t v: tab.variable_names) {
                    tab.variable_indices.push_back(std::find(all_variable_names.begin(), all_variable_names.end(), v) - all_variable_names.begin());
                    tab.variables_in_use[tab.variable_indices.back()] = true;
                    /*tab.mask |= domains[tab.variable_indices.back()].mask;*/
                    domains[tab.variable_indices.back()].assign_mask(tab.mask);
                }
                tab.variables_left.resize(all_variable_names.size(), false);
                for (size_t v = 0; v < tab.variable_indices[0]; ++v) {
                    tab.variables_left[v] = true;
                }
                /*MSG_QUEUE_FLUSH();*/
//                 MSG_DEBUG("Table " << *tab.data);
                /*MSG_QUEUE_FLUSH();*/
//                 MSG_DEBUG("Table variables " << tab.variable_names << " mask " << tab.mask);
                /*MSG_QUEUE_FLUSH();*/
                tab.offset = 0;
                coordinates.resize(tab.variable_names.size());
                tab.offset_to_cursor.reserve(tab.data->size());
                tab.unif = true;
                double coef = tab.data->begin()->coef;
                for (uint64_t i = 0; i < tab.data->size(); ++i) {
                    get_coords_at(tab, i, coordinates);
                    state_index_type cursor = compute_local_index(coordinates, tab);
                    tab.offset_to_cursor.push_back(cursor);
                    tab.domain_to_offset[cursor] = i;
                    tab.unif = tab.unif && std::abs(coef - tab.data->m_combination[i].coef) > _EPSILON;
                }
                if (0) {
                    std::stringstream ss;
                    ss << "Table";
                    for (const auto& x: tab.domain_to_offset) {
                        ss << ' ' << x.first << ':' << x.second;
                    }
                    ss << ',';
                    for (const auto& x: tab.offset_to_cursor) {
                        ss << ' ' << x;
                    }
                    MSG_DEBUG(ss.str());
                }
            }
            /*std::sort(tables.begin(), tables.end(), [](const table_descr& t1, const table_descr& t2) { return t1.data->size() > t2.data->size(); });*/
        }

    inline
    void
        compile_output()
        {
            scoped_indent _("[output] ");
//             MSG_DEBUG("all output variables " << output_variable_names);
            output_variable_names = output_variable_names % all_variable_names;
//             MSG_DEBUG("all_variable_names " << all_variable_names << " output_variable_names " << output_variable_names);
            output_variables_in_use.resize(all_variable_names.size(), false);
            for (int v: output_variable_names) {
                size_t index = std::find(all_variable_names.begin(), all_variable_names.end(), v) - all_variable_names.begin();
                output_variables_in_use[index] = true;
//                 MSG_DEBUG("variable " << v << " has index " << index);
                for (size_t t = 0; t < tables.size(); ++t) {
                    auto iter = std::find(tables[t].variable_names.begin(), tables[t].variable_names.end(), v);
                    if (iter != tables[t].variable_names.end()) {
//                         MSG_DEBUG("variable " << v << " found in table " << t << " at index " << (iter - tables[t].variable_names.begin()));
                        key_extractors.emplace_back(t, iter - tables[t].variable_names.begin());
                        break;
//                     } else {
//                         MSG_DEBUG("variable " << v << " not found in table " << t);
                    }
                }
            }
            if (0) {
                std::stringstream ss;
                ss << "key extractors";
                for (const auto& t_ki: key_extractors) {
                    ss << ' ' << t_ki.first << ':' << t_ki.second;
                }
                MSG_DEBUG(ss.str());
                MSG_QUEUE_FLUSH();
            }
        }

    inline
    state_index_type
        compute_local_index(const std::vector<size_t>& coordinates, const table_descr& t)
        {
            state_index_type accum;
            accum.reset();
            auto coordi = coordinates.begin();
            for (int v: t.variable_indices) {
                const auto& dom = domains[v];
                /*accum |= *coordi++ << dom.shift;*/
                dom.assign(accum, *coordi++);
            }
            return accum;
        }

    inline
    bool
        increment_index(state_index_type& index)
        {
            for (const auto& d: reversed(domains)) {
                if (!d.incr(index)) {
                    return false;
                }
            }
            return true;
                /*auto m = d.mask;*/
                /*if ((index & m) == d.max) {*/
                    /*index &= ~m;*/
                /*} else {*/
                    /*index += 1 << d.shift;*/
                    /*return false;*/
                /*}*/
            /*}*/
            /*return true;*/
        }

    inline
    void
        zero_right(state_index_type& index, size_t digit, const std::vector<bool>& variables_in_use)
        {
            auto di = domains.rbegin();
            auto in_use = variables_in_use.rbegin();
            auto dd = di + digit;
            /*state_index_type mask = 0;*/
            for (; di != dd; ++di) {
                if (!*in_use++) {
                    di->zero(index);
                    /*mask |= di->mask;*/
                }
            }
            /*index &= ~mask;*/
        }

    inline
    bool
        increment_left_and_zero_right(state_index_type& index, size_t digit)
        {
            auto di = domains.rbegin();
            auto dd = di + digit;
            auto dj = domains.rend();
            for (; di != dd; ++di) {
                di->zero(index);
            }
            di->zero(index);
            for (++di; di != dj; ++di) {
                if (!di->incr(index)) {
                    return false;
                }
            }
            return true;
        }

    inline
    state_index_type
        merge_cursors(const table_descr& t, state_index_type global, state_index_type local)
        {
            /* merge cursors, and increment if smaller than global cursor. otherwise, zero to the right of the leftmost modified digit. */
            /*scoped_indent _(SPELL_STRING("[merge_cursors " << dump(global) << " / " << dump(local, t) << "] "));*/
            state_index_type merged = t.merge(local, global);
            /*MSG_DEBUG("merged " << dump(merged, t));*/
            /*MSG_DEBUG("versus " << dump(global));*/
            size_t leftmost_modified_digit = 0;
            bool overflow = false;
            for (size_t v: t.variable_indices) {
                const auto& d = domains[v];
                state_index_type::block_type
                    m = d.extract_unshifted(merged),
                    g = d.extract_unshifted(global);
                /*MSG_DEBUG("m " << m << " g " << g);*/
                if (m != g) {
                    leftmost_modified_digit = last_variable_index - v;
                    overflow = m < g;
                    /*break;*/
                    /*MSG_DEBUG("leftmost_modified_digit " << leftmost_modified_digit << " overflow " << overflow);*/
                    if (overflow) {
                        /*MSG_DEBUG("increment_left_and_zero_right");*/
                        if (increment_left_and_zero_right(merged, leftmost_modified_digit)) {
                            /*MSG_DEBUG("general overflow !");*/
                            return state_index_type::bad();
                        }
                        return merged;
                    } else {
                        /*MSG_DEBUG("zero_right");*/
                        zero_right(merged, leftmost_modified_digit, t.variables_in_use);
                    }
                    /*MSG_DEBUG("return " << dump(merged));*/
                    return merged;
                }
            }
            return merged;
        }

    inline
    void
        compute_coordinates(state_index_type index, std::vector<size_t>& coordinates)
        {
            /*auto di = domains.rbegin();*/
            /*auto finished = domains.rend();*/
            auto coordi = coordinates.rbegin();
            /*size_t shift = 0;*/
            /*for (; di != finished; ++di) {*/
                /**coordi = (index & di->mask) >> shift;*/
                /*++coordi;*/
                /*shift += di->bit_width;*/
            /*}*/
            for (const domain_descr& d: reversed(domains)) {
                /**coordi++ = (index & d.mask) >> d.shift;*/
                *coordi++ = d.extract(index);
            }
        }

    struct update_key {
        size_t
            key_size(const joint_variable_product_type& jvp) const { return jvp.output_variable_names.size(); }
        void
            operator () (const joint_variable_product_type& jvp, genotype_comb_type::key_list& key) const
            {
                auto ki = key.begin();
//                 MSG_DEBUG("extractors " << jvp.key_extractors);
                for (const auto& t_ki: jvp.key_extractors) {
                    *ki++ = jvp.tables[t_ki.first].current_element().keys.keys[t_ki.second];
                }
//                 MSG_DEBUG("key: " << key);
            }
    };

    struct update_key_build_factor {
        size_t
            key_size(const joint_variable_product_type& jvp) const { return jvp.output_variable_names.size(); } /* parents + gameteS, where gameteS will be replaced by the output variable */
        void
            operator () (joint_variable_product_type& jvp, genotype_comb_type::key_list& key) const
            {
                auto ki = key.begin();
                auto ti = jvp.tables.begin();
                bn_label_type G = ti->current_element().keys.keys.front().state;
//                 MSG_DEBUG("Have gamete label " << G); MSG_QUEUE_FLUSH();
//                 MSG_DEBUG("Have output variable names " << jvp.output_variable_names);
                for (const auto& t_ki: jvp.key_extractors) {
                    *ki++ = jvp.tables[t_ki.first].current_element().keys.keys[t_ki.second];
                    /**ki++ = ti->current_element().keys.keys.front();*/
                }
//                 MSG_DEBUG("Have temp key " << key); MSG_QUEUE_FLUSH();
                const auto& p1 = key.keys[G.first_allele];
                const auto& p2 = key.keys[G.second_allele];
                bool f1 = G.first == GAMETE_L;
                bool f2 = G.second == GAMETE_L;
//                 MSG_DEBUG("Have p1 " << p1 << " p2 " << p2 << " f1 " << f1 << " f2 " << f2); MSG_QUEUE_FLUSH();
                --ki;
                *ki = {
                    jvp.output_variable_names.back(), {f1 ? p1.state.first : p1.state.second,
                    f2 ? p2.state.first : p2.state.second,
                    f1 ? p1.state.first_allele : p1.state.second_allele,
                    f2 ? p2.state.first_allele : p2.state.second_allele}
                };
//                 MSG_DEBUG("Have complete key " << key); MSG_QUEUE_FLUSH();
            }
    };

    struct update_key_build_factor_keep_gametes {
        size_t
            key_size(const joint_variable_product_type& jvp) const { return jvp.output_variable_names.size() + 1; } /* parents + gameteS, where gameteS will be replaced by the output variable */
        void
            operator () (joint_variable_product_type& jvp, genotype_comb_type::key_list& key) const
            {
                auto ki = key.begin();
                auto ti = jvp.tables.begin();
                bn_label_type G = ti->current_element().keys.keys.front().state;
//                 MSG_DEBUG("Have gamete label " << G); MSG_QUEUE_FLUSH();
//                 MSG_DEBUG("Have output variable names " << jvp.output_variable_names);
                for (const auto& t_ki: jvp.key_extractors) {
                    *ki++ = jvp.tables[t_ki.first].current_element().keys.keys[t_ki.second];
                    /**ki++ = ti->current_element().keys.keys.front();*/
                }
//                 MSG_DEBUG("Have temp key " << key); MSG_QUEUE_FLUSH();
                const auto& p1 = key.keys[1 + G.first_allele];
                const auto& p2 = key.keys[1 + G.second_allele];
                bool f1 = G.first == GAMETE_L;
                bool f2 = G.second == GAMETE_L;
//                 MSG_DEBUG("Have p1 " << p1 << " p2 " << p2 << " f1 " << f1 << " f2 " << f2); MSG_QUEUE_FLUSH();
                *ki = {
                    -jvp.output_variable_names.front(), {f1 ? p1.state.first : p1.state.second,
                    f2 ? p2.state.first : p2.state.second,
                    f1 ? p1.state.first_allele : p1.state.second_allele,
                    f2 ? p2.state.first_allele : p2.state.second_allele}
                };
//                 MSG_DEBUG("Have complete key " << key); MSG_QUEUE_FLUSH();
            }
    };

#if 0
    inline
    void
        update_key(genotype_comb_type::key_list& key)
        {
            auto ki = key.begin();
            for (const auto& t_ki: key_extractors) {
                *ki++ = tables[t_ki.first].current_element().keys.keys[t_ki.second];
            }
        }
#endif

    inline
    void
        compute_coef(mpf_t& accum)
        {
            auto ti = tables.begin();
            auto tj = tables.end();

            mpf_t coef;
            mpf_init_set_d(accum, ti->current_element().coef);
            mpf_init(coef);
            for (++ti; ti != tj; ++ti) {
                if (!ti->unif) {
                    mpf_set_d(coef, ti->current_element().coef);
                    mpf_mul(accum, accum, coef);
                }
            }
            mpf_clear(coef);
        }

    inline
    state_index_type
        next_state(state_index_type from)
        {
//            uint64_t valid = 0, all = (1 << tables.size()) - 1;
            boost::dynamic_bitset<> valid;
            valid.resize(tables.size(), 0);
            size_t ti = 0;
            while (!valid.all() && !from.is_bad()) {
//                MSG_DEBUG("valid " << valid << " all " << all << " table " << (*tables[ti].data));
                if (!move_cursor_if_state_is_valid(tables[ti], from)) {
                    auto range = find_offsets_around_cursor(tables[ti], tables[ti].local(from));
//                    MSG_DEBUG("offsets are " << range.first << " and " << range.second);
                    state_index_type
                        c1 = merge_cursors(tables[ti], from, tables[ti].offset_to_cursor[range.first]),
                        c2 = merge_cursors(tables[ti], from, tables[ti].offset_to_cursor[range.second]);
//                    MSG_DEBUG("from = " << dump(from) << " c1 = " << dump(c1, tables[ti]) << " c2 = " << dump(c2, tables[ti]));
                    state_index_type tmp; // = std::min(c1, c2);
                    if (c1 > from) {
                        if (c2 > from) {
                            tmp = std::min(c1, c2);
                        } else {
                            tmp = c1;
                        }
                    } else if (c2 > from) {
                        tmp = c2;
                    } else {
                        return state_index_type::bad();
                    }
//                    MSG_DEBUG("new lower bound is now " << dump(tmp));
                    if (tmp < from) {
                        return state_index_type::bad();
                    }
                    from = tmp;
                    valid.reset();
//                    valid = 0;
                    /*ti = !ti;*/
                } else {
                    valid.set(ti);
                }
                ++ti;
                ti %= tables.size();
            }
            return from;
        }


    genotype_comb_type
        extract_domain()
        {
            genotype_comb_type ret;
            genotype_comb_type::key_list key;
            update_key uk;
            key.keys.resize(output_variable_names.size());
            std::unordered_map<genotype_comb_type::key_list, double> values;
            ////size_t& offset = tables.front().offset;
            size_t offset = 0; //// Franck Gauthier 2020-12-15
            size_t max = tables.front().data->size();
            for (offset = 0; offset < max; ++offset) {
                uk(*this, key);
                values[key] = 1;
            }
            tables.front().offset = offset; //// Franck Gauthier 2020-12-15
            for (const auto& kv: values) {
                /*ret.m_combination.emplace_back(kv.first, kv.second * norm);*/
                ret.m_combination.emplace_back(kv.first, kv.second);
            }
            std::sort(ret.m_combination.begin(), ret.m_combination.end());
            /*MSG_DEBUG("have domain " << ret);*/
            return ret;
        }

    template <typename key_computing_variant>
    genotype_comb_type
        compute_generic()
        {
            scoped_indent _("[compute jvp] ");
            MSG_DEBUG("output variable names " << output_variable_names);
            if (invalid_product) { return {}; }

            if (total_bits >= 32) {
                std::vector<size_t> output_variable_indices;
                for (size_t i = 0; i < output_variables_in_use.size(); ++i) {
                    output_variable_indices.push_back(i);
                }
                /* Build adjacency matrix */
                std::vector<std::vector<int>> weights(tables.size());
                for (size_t i = 0; i < tables.size(); ++i) { weights[i].resize(tables.size(), 0); }
                for (size_t i = 0; i < tables.size(); ++i) {
                    const auto& Vi = tables[i].variable_indices;
                    for (size_t j = i + 1; j < tables.size(); ++j) {
                        const auto& Vj = tables[j].variable_indices;
                        int w = 0;
                        for (size_t varidx: (Vi % Vj) - output_variable_indices) {
                            w += domains[varidx].bit_width;
                        }
                        weights[i][j] = w;
                        weights[j][i] = w;
                    }
                }
                /* Find minimum cut */
                std::pair<int, std::vector<int>> cut = get_min_cut(weights);
                std::vector<bool> first_part(tables.size(), false);
                for (int c: cut.second) {
                    first_part[c] = true;
                }
                joint_variable_product_type p1, p2;
                auto ti = tables.begin();
                for (auto b: first_part) {
                    (b ? p1 : p2).add_table(*ti->data);
                }
                auto common = p1.all_variable_names % p2.all_variable_names;
                p1.set_output(output_variable_names + common);
                p2.set_output(output_variable_names + common);
                p1.compile(*all_domains);
                p2.compile(*all_domains);
                genotype_comb_type t1 = p1.compute_generic<key_computing_variant>();
                genotype_comb_type t2 = p2.compute_generic<key_computing_variant>();
                joint_variable_product_type final;
                final.add_table(t1);
                final.add_table(t2);
                final.compile(*all_domains);
                return final.compute_generic<key_computing_variant>();
            }

            key_computing_variant key_update;

            genotype_comb_type ret;
            size_t counter = 0;
            genotype_comb_type::key_list key;
            key.keys.resize(key_update.key_size(*this));
            /*MSG_DEBUG(key.size()); MSG_QUEUE_FLUSH();*/
            state_index_type z;
            z.reset();
//             MSG_DEBUG("Initializing cursor");
            state_index_type
                    cursor = next_state(z),
                    last = cursor;
            std::unordered_map<genotype_comb_type::key_list, mpf_t> values;
            if (cursor.is_bad()) {
                return ret;
            }
            mpf_t norm;
            mpf_init(norm);
            mpf_t accum;
            mpf_init(accum);
            for (;;) {
                /*if (++counter > 10) {*/
                /*break;*/
                /*}*/
                /*jvp.compute_coordinates(cursor, coords);*/
                /*MSG_DEBUG(std::bitset<7>(cursor) << ' ' << coords[0] << ' ' << coords[1] << ' ' << coords[2]);*/

//                MSG_DEBUG("COMPUTE @" << dump(cursor));

                key_update(*this, key);
                mpf_clear(accum);
                compute_coef(accum);
                if (mpf_cmp_d(accum, 0)) {
                    mpf_add(norm, norm, accum);
                    auto it = values.find(key);
                    mpf_t& value = values[key];
                    if (it == values.end()) {
                        mpf_init_set(value, accum);
                    } else {
                        mpf_add(value, value, accum);
                    }
                }

                ++counter;

                last = cursor;
                if (increment_index(cursor)) {
                    /*MSG_DEBUG("couldn't increment any more.");*/
                    break;
                }
                /*MSG_DEBUG("cursor after increment = " << dump(cursor));*/
                cursor = next_state(cursor);
                /*MSG_DEBUG("next valid cursor = " << dump(cursor));*/
                if (cursor.is_bad()) {
                    /*MSG_DEBUG("didn't find any more valid state.");*/
                    break;
                }
                if (cursor <= last) {
                    /*MSG_DEBUG("looped over all possible states.");*/
                    break;
                }
            }
            ret.m_combination.reserve(values.size());
            if (mpf_cmp_d(norm, 0) > 0) {
                mpf_ui_div(norm, 1, norm);
//                norm = 1. / norm;
            }
            for (auto& kv: values) {
                mpf_mul(accum, kv.second, norm);
                ret.m_combination.emplace_back(kv.first, mpf_get_d(accum));
                /*ret.m_combination.emplace_back(kv.first, kv.second);*/
                mpf_clear(kv.second);
            }
            mpf_clear(accum);
            mpf_clear(norm);
            std::sort(ret.m_combination.begin(), ret.m_combination.end());
            MSG_DEBUG("Computed " << counter << " coefficients, projected onto " << ret.size() << " elements");
            MSG_DEBUG(ret);
            return ret;
        }

    inline genotype_comb_type compute() { return compute_generic<update_key>(); }
    inline genotype_comb_type build_factor() { return compute_generic<update_key_build_factor>(); }
    inline genotype_comb_type build_factor_with_gametes() { return compute_generic<update_key_build_factor_keep_gametes>(); }

    /* Algorithm adapted from wikipedia page https://en.wikipedia.org/wiki/Stoer%E2%80%93Wagner_algorithm */
    std::pair<int, std::vector<int>>
    get_min_cut(std::vector<std::vector<int>> &weights) {
        int N = weights.size();
        std::vector<int> used(N), cut, best_cut;
        int best_weight = std::numeric_limits<int>::max();
        int best_size = std::numeric_limits<int>::max();
        int ws = weights.size();

        for (int phase = N-1; phase >= 0; phase--) {
            std::vector<int> w = weights[0];
            std::vector<int> added = used;
            int prev, last = 0;
            for (int i = 0; i < phase; i++) {
                prev = last;
                last = -1;
                for (int j = 1; j < N; j++)
                    if (!added[j] && (last == -1 || w[j] > w[last])) last = j;
                if (i == phase - 1) {
                    for (int j = 0; j < N; j++) weights[prev][j] += weights[last][j];
                    for (int j = 0; j < N; j++) weights[j][prev] = weights[prev][j];
                    used[last] = true;
                    cut.push_back(last);
                    int cs = ws - 2 * cut.size();
                    int sz = cs < 0 ? -cs : cs;
                    if ((w[last] == best_weight && sz < best_size) || w[last] < best_weight) {
                        best_cut = cut;
                        best_weight = w[last];
                        best_size = sz;
                    }
                } else {
                    for (int j = 0; j < N; j++)
                        w[j] += weights[last][j];
                    added[last] = true;
                }
            }
        }
        return std::make_pair(best_weight, best_cut);
    }
};


template <typename Ref> struct __table_getter;

template <typename K> struct __table_getter<std::pair<const K, genotype_comb_type>&> {
    const genotype_comb_type& operator () (std::pair<const K, genotype_comb_type>& ref) const { return ref.second; }
};
template <> struct __table_getter<const genotype_comb_type* const&> {
    const genotype_comb_type& operator () (const genotype_comb_type* const& ref) const { return *ref; }
};
template <> struct __table_getter<genotype_comb_type&> {
    const genotype_comb_type& operator () (genotype_comb_type& ref) const { return ref; }
};
template <> struct __table_getter<genotype_comb_type*&> {
    const genotype_comb_type& operator () (genotype_comb_type*& ref) const { return *ref; }
};



template <typename Iterator>
genotype_comb_type
compute_product(Iterator tables_begin, Iterator tables_end, const std::vector<int>& output_variables, const std::map<std::vector<int>, genotype_comb_type>& domains)
{
    /*scoped_indent _(SPELL_STRING("[compute product > " << output_variables << "] "));*/
    joint_variable_product_type jvp;
    __table_getter<typename std::iterator_traits<Iterator>::reference> getter;
    for (; tables_begin != tables_end; ++tables_begin) {
        if (getter(*tables_begin).size()) {
            jvp.add_table(getter(*tables_begin));
        /*} else {*/
            /*MSG_DEBUG("[compute_product] Didn't add table " << (*tables_begin) << " {" << getter(*tables_begin) << "} because it seems to be empty.");*/
        }
    }
    jvp.set_output(output_variables);
    jvp.compile(domains);
    return jvp.compute();
}

inline
genotype_comb_type
generate_factor(const std::vector<int>& parents, bool dh, int spawnling, const genotype_comb_type& parent_domain, std::map<std::vector<int>, genotype_comb_type>& domains, bool keep_gametes)
{
    //// DEBUG : quick (and dirty) workaround to deal with clang++ issue on macos
    ////scoped_indent _(SPELL_STRING("[generate_factor " << (dh ? "dh " : "") << spawnling << " from parents " << parents << " domain " << parent_domain << "] ")); //// DEBUG
    scoped_indent _(SPELL_STRING("[generate_factor " << (dh ? "dh " : "") << spawnling << " from parents " << vec_to_spell_string(parents) << " domain " << parent_domain << "] ")); //// DEBUG
    std::vector<bn_label_type> label_g;
    std::vector<int> sorted_parents(parents);
    if (parents.size() == 2) {
        if (parents[0] > parents[1]) {
            label_g = {
                {GAMETE_L, GAMETE_L, 1, 0},
                {GAMETE_L, GAMETE_R, 1, 0},
                {GAMETE_R, GAMETE_L, 1, 0},
                {GAMETE_R, GAMETE_R, 1, 0}
            };
            std::swap(sorted_parents[0], sorted_parents[1]);
        } else {
            label_g = {
                {GAMETE_L, GAMETE_L, 0, 1},
                {GAMETE_L, GAMETE_R, 0, 1},
                {GAMETE_R, GAMETE_L, 0, 1},
                {GAMETE_R, GAMETE_R, 0, 1}
            };
        }
    } else if (dh) {
        label_g = {
            {GAMETE_L, GAMETE_L, 0, 0},
            {GAMETE_R, GAMETE_R, 0, 0},
        };
    } else {
        label_g = {
            {GAMETE_L, GAMETE_L, 0, 0},
            {GAMETE_L, GAMETE_R, 0, 0},
            {GAMETE_R, GAMETE_L, 0, 0},
            {GAMETE_R, GAMETE_R, 0, 0}
        };
    }
    
    int gamete = -spawnling;
    genotype_comb_type G, S;
    
    joint_variable_product_type jvp;

    if (keep_gametes) {
        G = state_to_combination(gamete, label_g) * (1. / label_g.size());
        domains[{gamete}] = G;
    } else {
        G = state_to_combination(spawnling, label_g) * (1. / label_g.size());
        domains[{spawnling}] = G;
    }
    MSG_DEBUG("joint parent domain " << parent_domain);
    MSG_DEBUG("all domains " << domains);
    jvp.add_table(G);
    jvp.add_table(parent_domain);
    if (keep_gametes) {
        jvp.set_output(sorted_parents + std::vector<int>{gamete, spawnling});
    } else {
        jvp.set_output(sorted_parents + std::vector<int>{spawnling});
    }
    jvp.compile(domains);
    return keep_gametes ? jvp.build_factor_with_gametes() : jvp.build_factor();
}

inline
void
extract_domain(const genotype_comb_type& factor, const std::vector<int>& variables, std::map<std::vector<int>, genotype_comb_type>& domains)
{
    joint_variable_product_type jvp;
    /*MSG_DEBUG("extract " << variables << " from " << factor);*/
    /*MSG_QUEUE_FLUSH();*/
    jvp.add_table(factor);
    jvp.set_output(variables);
    jvp.compile_output();
    auto& ret = domains[variables] = jvp.extract_domain();
    /*MSG_DEBUG("variables " << variables << " domain " << domains[variables]); MSG_QUEUE_FLUSH();*/
    for (auto& e: ret) {
        e.coef = 1;
    }
}


#endif
