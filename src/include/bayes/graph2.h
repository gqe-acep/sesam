/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_BAYES_GRAPH_H_
#define _SPEL_BAYES_GRAPH_H_

#include <list>
#include <unordered_set>
#include <unordered_map>


enum pedigree_graph_node_type { NTVariable, NTFactor };

typedef std::unordered_map<size_t, std::list<size_t>> edge_map_type;


struct pedigree_graph_node {
    pedigree_graph_node_type m_type;
    size_t m_index;
    pedigree_graph_node(node_type_type t, size_t i) : m_type(t), m_index(i) {}
    bool is_variable() const { return m_type == NTVariable; }
    bool is_factor() const { return m_type == NTFactor; }
};


struct pedigree_graph {
    std::vector<pedigree_graph_node> m_nodes;
    edge_map_type m_edges_to;
    edge_map_type m_edges_from;

    pedigree_graph() : m_nodes(), m_edges_to(), m_edges_from() {}

    size_t add_node(node_type_type type, size_t label)
    {
        m_nodes.emplace_back(type, label);
        return m_nodes.size() - 1;
    }

    size_t add_edge(size_t from, size_t to)
    {
        m_edges_to[from].push_back(to);
        m_edges_from[to].push_back(from);
    }

    template <typename Func>
        apply_var(Func f)
        {
            for (size_t v = 0; v < m_nodes.size(); ++v) {
                if (m_nodes[v].is_variable()) {
                    for (size_t e: m_edges_to[v]) {
                        f(m_nodes[v], m_nodes[e]);
                    }
                    for (size_t e: m_edges_from[v]) {
                        f(m_nodes[v], m_nodes[e]);
                    }
                }
            }
        }

#if 0
    template <typename Func>
        apply_fac_to_var_to_fac(Func f)
        {
            for (size_t f1 = 0; f1 < m_nodes.size(); ++f1) {
                if (m_nodes[f1].is_factor()) {
                    for (size_t v: m_edges_to[f1]) {
                        if (m_nodes[v].is_variable()) {
                            for (size_t f2: m_edges_to[v]) {
                                if (m_nodes[f2].is_factor()) {
                                    f(m_nodes[f1], m_nodes[v], m_nodes[f2]);
                                }
                            }
                        }
                    }
                }
            }
        }

    template <typename Func>
        apply_var_to_two_facs(Func f)
        {
            for (size_t v = 0; v < m_nodes.size(); ++v) {
                if (m_nodes[v].is_variable()) {
                    auto i = m_edges_to[v].begin();
                    auto j = m_edges_to[v].end();
                    for (; i != j; ++i) {
                        if (m_nodes[*i].is_factor()) {
                            auto k = i;
                            for (++k; k != j; ++k) {
                                if (m_nodes[*k].is_factor()) {
                                    f(m_nodes[*i], m_nodes[v], m_nodes[*k]);
                                }
                            }
                        }
                    }
                }
            }
        }
#endif
};


#endif

