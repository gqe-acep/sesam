/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_BAYES_ND_MATRIX_H_
#define _SPEL_BAYES_ND_MATRIX_H_

#include <vector>
#include <algorithm>
#include "error.h"

struct nd_dense_matrix;
struct nd_sparse_matrix;


/******************************************************************************
 ****************************************************************************
 *
 * BASE MATRIX CLASS
 *
 **********************************************************************
 ********************************************************************
 */

struct nd_matrix_base {
    std::vector<size_t> m_dimensions;
    std::vector<size_t> m_strides;

    nd_matrix_base() = delete;

    nd_matrix_base(const nd_matrix_base& other)
        : m_dimensions(other.m_dimensions)
        , m_strides(other.m_strides)
    {}

    static void compute_strides(std::vector<size_t>& strides, const std::vector<size_t>& dim)
    {
        strides.reserve(dim.size() + 1);
        strides.push_back(1);
        for (size_t d: dim) {
            strides.push_back(strides.back() * d);
        }
    }

    nd_matrix_base(const std::vector<size_t>& dim)
        : m_dimensions(dim)
        , m_strides()
    {
        compute_strides(m_strides, m_dimensions);
    }

    size_t idx_to_ofs(const std::vector<size_t>& idx) const
    {
        return idx_to_ofs(m_dimensions, m_strides, idx);
    }

    static size_t idx_to_ofs(const std::vector<size_t>& dim, const std::vector<size_t>& strides, const std::vector<size_t>& idx)
    {
#ifndef NDEBUG
        if (idx.size() != dim.size()) {
            throw std::runtime_error(SPELL_STRING("Wrong number of dimensions (" << idx.size() << " != " << dim.size() << ')'));
        }
#endif
        size_t accum = 0;
        size_t n_dim = dim.size();
        for (size_t i = 0; i < n_dim; ++i) {
#ifndef NDEBUG
            if (idx[i] >= dim[i]) {
                throw std::runtime_error(SPELL_STRING("Index #" << i << " greater than dimension (" << idx[i] << " > " << dim[i] << ')'));
            }
#endif
            accum += idx[i] * strides[i];
        }
        return accum;
    }

    std::vector<size_t> ofs_to_idx(size_t ofs) const
    {
        return ofs_to_idx(m_dimensions, m_strides, ofs);
    }

    static std::vector<size_t> ofs_to_idx(const std::vector<size_t>& dim, const std::vector<size_t>& strides, size_t ofs)
    {
        std::vector<size_t> ret;
        ret.reserve(dim.size());
        for (size_t i = 0; i < dim.size(); ++i) {
            ret.push_back((ofs / strides[i]) % dim[i]);
        }
        return ret;
    }

    const std::vector<size_t>& dimensions() const { return m_dimensions; }
    size_t dimension(size_t n) const { return m_dimensions[n]; }
    size_t size() const { return m_strides.back(); }

    virtual nd_sparse_matrix as_sparse() const = 0;
    virtual nd_dense_matrix as_dense() const = 0;
};


template <typename MT>
struct printable { virtual ~printable() {} const MT& to_type() const { return *dynamic_cast<const MT*>(this); } };


/******************************************************************************
 ****************************************************************************
 *
 * SPARSE MATRIX
 *
 **********************************************************************
 ********************************************************************
 */

struct nd_sparse_storage {
    typedef double value_type;
    typedef std::list<std::pair<size_t, value_type>> storage_type;
    storage_type m_data;
    size_t m_max_size;
    size_t m_count;

    nd_sparse_storage()
        : m_data(), m_max_size(0), m_count(0)
    {}

    nd_sparse_storage(size_t n)
        : m_data(), m_max_size(n), m_count(0)
    {}

    void init(size_t n)
    {
        m_max_size = n;
    }

    struct cached_access {
        nd_sparse_storage* m_this_storage;
        storage_type::iterator m_last, m_begin, m_end;
        size_t m_last_index;

        cached_access(nd_sparse_storage* s)
            : m_this_storage(s)
            , m_last(s->m_data.begin())
            , m_begin(s->m_data.begin())
            , m_end(s->m_data.end())
            , m_last_index(0)
        {}

        value_type get(size_t index)
        {
            if (m_last_index > index) {
                if (m_last == m_end) { --m_last; }
                while (m_last != m_begin && m_last->first > index) { --m_last; }
            } else {
                while (m_last != m_end && m_last->first < index) { ++m_last; }
            }
            if (m_last == m_end) { return 0; }
            if (m_last->first == index) { return m_last->second; }
            m_last_index = index;
            return 0;
        }

        void put(size_t index, value_type v)
        {
            /*MSG_DEBUG("inserting (" << index << ", " << v << ") m_last_index=" << m_last_index << " m_last=(" << m_last->first << ", " << m_last->second << ")");*/
            if (m_last_index > index) {
                /*MSG_DEBUG("rewind");*/
                while (m_last != m_begin && m_last->first > index) { --m_last; }
            } else {
                /*MSG_DEBUG("forward");*/
                while (m_last != m_end && m_last->first < index) { ++m_last; }
                if (m_last != m_end && m_last->first != index) { --m_last; }
            }
            /*MSG_DEBUG("now m_last=(" << m_last->first << ", " << m_last->second << ")");*/
            if (m_last->first == index) {
                if (v) {
                    m_last->second = v;
                } else {
                    m_last = m_this_storage->m_data.erase(m_last);
                    m_this_storage->m_count--;
                }
            } else if (v) {
                auto next = m_last;
                if (next == m_end) {
                    /*MSG_DEBUG("inserting (" << index << ", " << v << ") at end");*/
                } else {
                    ++next;
                    /*MSG_DEBUG("inserting (" << index << ", " << v << ") before (" << next->first << ", " << next->second << ")");*/
                }
                m_last = m_this_storage->m_data.insert(next, {index, v});
                m_this_storage->m_count++;
            }
            m_last_index = index;
        }
    };

    struct const_cached_access {
        const nd_sparse_storage* m_this_storage;
        storage_type::const_iterator m_last, m_begin, m_end;
        size_t m_last_index;

        const_cached_access(const nd_sparse_storage* s)
            : m_this_storage(s)
            , m_last(s->m_data.begin())
            , m_begin(s->m_data.begin())
            , m_end(s->m_data.end())
            , m_last_index(0)
        {}

        value_type get(size_t index)
        {
            if (m_last_index > index) {
                if (m_last == m_end) { --m_last; }
                while (m_last != m_begin && m_last->first > index) { --m_last; }
            } else {
                while (m_last != m_end && m_last->first < index) { ++m_last; }
            }
            if (m_last == m_end) { m_last_index = m_this_storage->m_max_size; return 0; }
            m_last_index = m_last->first;
            if (m_last->first == index) { return m_last->second; }
            return 0;
        }
    };

    friend
        std::ostream& operator << (std::ostream& os, const nd_sparse_storage& ns)
        {
            os << "{";
            for (const auto& iv: ns.m_data) {
                os << " (" << iv.first << ", " << iv.second << ')';
            }
            return os << " }";
        }

    cached_access accessor() { return {this}; }
    const_cached_access accessor() const { return {this}; }
    const_cached_access const_accessor() const { return {this}; }
};


struct nd_sparse_matrix : public nd_matrix_base, public printable<nd_sparse_matrix> {
    std::shared_ptr<nd_sparse_storage> m_data;
    typedef nd_sparse_storage::value_type value_type;

    nd_sparse_matrix(const nd_sparse_matrix& other)
        : nd_matrix_base(other)
        , m_data(other.m_data)
    {}

    nd_sparse_matrix(const std::vector<size_t>& dim)
        : nd_matrix_base(dim)
        , m_data(new nd_sparse_storage())
    { m_data->init(m_strides.back()); }

    struct cached_access {
        typedef nd_sparse_matrix::value_type value_type;

        nd_sparse_matrix* m_this_matrix;
        nd_sparse_storage::cached_access m_accessor;

        cached_access(nd_sparse_matrix* m)
            : m_this_matrix(m), m_accessor(m->m_data->accessor())
        {}
        
        value_type get(size_t idx) { return m_accessor.get(idx); }
        value_type get(const std::vector<size_t>& idx) { return m_accessor.get(m_this_matrix->idx_to_ofs(idx)); }
        void put(size_t idx, value_type v) { m_accessor.put(idx, v); }
        void put(const std::vector<size_t>& idx, value_type v) { m_accessor.put(m_this_matrix->idx_to_ofs(idx), v); }
    };

    struct const_cached_access {
        typedef nd_sparse_matrix::value_type value_type;

        const nd_sparse_matrix* m_this_matrix;
        nd_sparse_storage::const_cached_access m_accessor;

        const_cached_access(const nd_sparse_matrix* m)
            : m_this_matrix(m), m_accessor(m->m_data->const_accessor())
        {}
        
        value_type get(size_t idx) { return m_accessor.get(idx); }
        value_type get(const std::vector<size_t>& idx) { return m_accessor.get(m_this_matrix->idx_to_ofs(idx)); }
    };

    cached_access accessor() { return {this}; }
    const_cached_access accessor() const { return {this}; }
    const_cached_access const_accessor() const { return {this}; }

    virtual nd_sparse_matrix as_sparse() const;
    virtual nd_dense_matrix as_dense() const;
};



/******************************************************************************
 ****************************************************************************
 *
 * DENSE MATRIX
 *
 **********************************************************************
 ********************************************************************
 */

struct nd_dense_matrix : public nd_matrix_base, public printable<nd_dense_matrix> {
    typedef double value_type;
    std::shared_ptr<std::vector<value_type>> m_data;

    nd_dense_matrix(const nd_dense_matrix& other)
        : nd_matrix_base(other)
        , m_data(other.m_data)
    {}
    
    nd_dense_matrix(const std::vector<size_t>& dim)
        : nd_matrix_base(dim)
        , m_data(new std::vector<value_type>())
    {
        /*MSG_DEBUG(dim);*/
        /*MSG_DEBUG(m_strides);*/
        m_data->resize(m_strides.back(), 0);
    }

    struct cached_access {
        typedef nd_dense_matrix::value_type value_type;

        nd_dense_matrix* m_this_matrix;

        cached_access(nd_dense_matrix* m)
            : m_this_matrix(m)
        {}
        
        value_type get(size_t idx) { return (*m_this_matrix->m_data)[idx]; }
        value_type get(const std::vector<size_t>& idx) { return (*m_this_matrix->m_data)[m_this_matrix->idx_to_ofs(idx)]; }
        void put(size_t idx, value_type v) { (*m_this_matrix->m_data)[idx] = v; }
        void put(const std::vector<size_t>& idx, value_type v) { (*m_this_matrix->m_data)[m_this_matrix->idx_to_ofs(idx)] = v; }
    };

    struct const_cached_access {
        typedef nd_dense_matrix::value_type value_type;

        const nd_dense_matrix* m_this_matrix;

        const_cached_access(const nd_dense_matrix* m)
            : m_this_matrix(m)
        {}
        
        value_type get(size_t idx) const { return (*m_this_matrix->m_data)[idx]; }
        value_type get(const std::vector<size_t>& idx) const { return (*m_this_matrix->m_data)[m_this_matrix->idx_to_ofs(idx)]; }
    };

    cached_access accessor() { return {this}; }
    const_cached_access accessor() const { return {this}; }

    virtual nd_sparse_matrix as_sparse() const;
    virtual nd_dense_matrix as_dense() const;
};



/******************************************************************************
 ****************************************************************************
 *
 * IMPLEMENTATION OF SPARSE<->DENSE CONVERSIONS
 *
 **********************************************************************
 ********************************************************************
 */


nd_sparse_matrix nd_sparse_matrix::as_sparse() const { return *this; }
nd_dense_matrix nd_sparse_matrix::as_dense() const
{
    nd_dense_matrix ret(dimensions());
    auto ac = ret.accessor();
    for (const auto& iv: m_data->m_data) {
        ac.put(iv.first, iv.second);
    }
    return ret;
}


nd_dense_matrix nd_dense_matrix::as_dense() const { return *this; }
nd_sparse_matrix nd_dense_matrix::as_sparse() const
{
    nd_sparse_matrix ret(dimensions());
    auto ac = ret.accessor();
    size_t n = m_data->size();
    for (size_t i = 0; i < n; ++i) {
        if ((*m_data)[i]) {
            ac.put(i, (*m_data)[i]);
        }
    }
    return ret;
}


/******************************************************************************
 ****************************************************************************
 *
 * MARGINALS
 *
 **********************************************************************
 ********************************************************************
 */

template <typename BACKEND_TYPE>
struct extract_multiple_dimensions_type : public nd_matrix_base, public printable<extract_multiple_dimensions_type<BACKEND_TYPE>> {
    typedef typename BACKEND_TYPE::value_type value_type;

    const BACKEND_TYPE m_backend;
    std::vector<size_t> m_dim_sum;
    std::vector<size_t> m_dim_size;

    extract_multiple_dimensions_type(const extract_multiple_dimensions_type<BACKEND_TYPE>& other)
        : nd_matrix_base(other)
        , m_backend(other.m_backend)
        , m_dim_sum(other.m_dim_sum)
        , m_dim_size(other.m_dim_size)
    {}

    extract_multiple_dimensions_type(const BACKEND_TYPE& be, std::vector<size_t>&& dim_sum)
        : nd_matrix_base(_remove_dim(be.dimensions(), dim_sum))
        , m_backend(be)
        , m_dim_sum(std::move(dim_sum))
        , m_dim_size()
    {
        m_dim_size.reserve(m_dim_sum.size());
        for (size_t dim: m_dim_sum) {
            m_dim_size.push_back(be.dimension(dim));
        }
        /*MSG_DEBUG("EMD m_dim_sum=(" << m_dim_sum << ") m_dim_size=(" << m_dim_size << ") dimensions=(" << dimensions() << ")");*/
    }

    extract_multiple_dimensions_type(const BACKEND_TYPE& be, const std::vector<size_t>& dim_sum)
        : nd_matrix_base(_remove_dim(be.dimensions(), dim_sum))
        , m_backend(be)
        , m_dim_sum(dim_sum)
        , m_dim_size()
    {
        m_dim_size.reserve(m_dim_sum.size());
        for (size_t dim: m_dim_sum) {
            m_dim_size.push_back(be.dimension(dim));
        }
        /*MSG_DEBUG("EMD m_dim_sum=(" << m_dim_sum << ") m_dim_size=(" << m_dim_size << ") dimensions=(" << dimensions() << ")");*/
    }

    std::vector<size_t> _remove_dim(const std::vector<size_t>& vec, const std::vector<size_t>& dimvec) const
    {
        std::vector<size_t> ret;
        size_t i = (size_t)(-1);
        ret.reserve(vec.size() - dimvec.size());
        for (size_t dim: dimvec) {
            for (++i; i < dim; ++i) { ret.push_back(vec[i]); }
        }
        for (++i; i < vec.size(); ++i) { ret.push_back(vec[i]); }
        return ret;
    }

    void _add_dim(std::vector<size_t>& ret, const std::vector<size_t>& vec) const
    {
        /*MSG_DEBUG("input vec  " << vec);*/
        /*std::vector<size_t> ret;*/
        size_t i = 0;
        size_t vi = 0;
        size_t di = 0;
        size_t n = vec.size() + m_dim_sum.size();
        ret.reserve(n);
        ret.clear();
        for (i = 0; i < n; ++i) {
            ret.push_back(di < m_dim_sum.size() && i == m_dim_sum[di]
                          ? (di++, 0)
                          : vec[vi++]);
        }
        /*MSG_DEBUG("output vec " << ret);*/
        /*return ret;*/
    }

    struct const_cached_access {
        typedef extract_multiple_dimensions_type<BACKEND_TYPE>::value_type value_type;

        const extract_multiple_dimensions_type<BACKEND_TYPE>* m_this_matrix;
        typename BACKEND_TYPE::const_cached_access ac;
        std::vector<size_t> backend_idx;

        const_cached_access(const extract_multiple_dimensions_type<BACKEND_TYPE>* m)
            : m_this_matrix(m)
            , ac(m_this_matrix->m_backend.accessor())
            , backend_idx()
        {
            backend_idx.resize(m_this_matrix->dimensions().size(), 0);
            /*MSG_DEBUG("Cached accessor.");*/
            /*MSG_DEBUG("dimensions " << m_this_matrix->m_dimensions);*/
            /*MSG_DEBUG("dim_sum " << m_this_matrix->m_dim_sum);*/
            /*MSG_DEBUG("augmented dimensions " << m_this_matrix->_add_dim(m_this_matrix->m_dim_sum));*/
        }

        value_type get(size_t idx) { return get(m_this_matrix->ofs_to_idx(idx)); }

        value_type rec_get(std::vector<size_t>& backend_idx, size_t cur_dim)
        {
            /*MSG_DEBUG("rec_get(" << backend_idx << ", " << cur_dim << ')');*/
            MSG_QUEUE_FLUSH();
            size_t dim_i = m_this_matrix->m_dim_sum[cur_dim];
            size_t dim_size_i = m_this_matrix->m_dim_size[cur_dim];
            value_type accum = 0;
            if (cur_dim == m_this_matrix->m_dim_sum.size() - 1) {
                for (backend_idx[dim_i] = 0; backend_idx[dim_i] < dim_size_i; ++backend_idx[dim_i]) {
                    accum += ac.get(backend_idx);
                }
            } else {
                for (backend_idx[dim_i] = 0; backend_idx[dim_i] < dim_size_i; ++backend_idx[dim_i]) {
                    accum += rec_get(backend_idx, cur_dim + 1);
                }
            }
            return accum;
        }

        value_type get(const std::vector<size_t>& idx)
        {
            m_this_matrix->_add_dim(backend_idx, idx);
            /*backend_idx.resize(m_this_matrix->dimensions().size(), 0);*/
            /*for (size_t i = 0; i < backend_idx.size(); ++i) {*/
                /*if (*/
            /*}*/
            /*MSG_DEBUG("get(" << backend_idx << ")");*/
            return rec_get(backend_idx, 0);
        }
    };

    const_cached_access accessor() const { return {this}; }

    virtual nd_sparse_matrix as_sparse() const
    {
        nd_sparse_matrix ret(dimensions());
        auto ac = ret.accessor();
        auto my_ac = accessor();
        for (size_t i = 0; i < size(); ++i) {
            auto v = my_ac.get(i);
            if (v != 0) {
                ac.put(i, v);
            }
        }
        return ret;
    }

    virtual nd_dense_matrix as_dense() const
    {
        nd_dense_matrix ret(dimensions());
        auto ac = ret.accessor();
        auto my_ac = accessor();
        for (size_t i = 0; i < size(); ++i) {
            ac.put(i, my_ac.get(i));
        }
        return ret;
    }
};




template <typename BACKEND_TYPE>
extract_multiple_dimensions_type<BACKEND_TYPE>
sum_over(const BACKEND_TYPE& bt, const std::vector<size_t>& dim) { return {bt, dim}; }

template <typename BACKEND_TYPE>
extract_multiple_dimensions_type<BACKEND_TYPE>
sum_over(const BACKEND_TYPE& bt, size_t dim) { return {bt, {dim}}; }



/******************************************************************************
 ****************************************************************************
 *
 * RESHAPE TO 2D
 *
 **********************************************************************
 ********************************************************************
 */

#if 0
struct eigen_nd_matrix : public nd_matrix_base, public printable<eigen_nd_matrix> {
    typedef double value_type;
    Eigen::MatrixXd m_data;

    std::vector<size_t> join_vec(const std::vector<size_t>& v1, const std::vector<size_t>& v2)
    {
    }

    eigen_nd_matrix(const std::vector<size_t>& 
};
#endif

/******************************************************************************
 ****************************************************************************
 *
 * RESHAPE TO 2D
 *
 **********************************************************************
 ********************************************************************
 */

template <typename M1>
struct reshape_2D_type : public nd_matrix_base, public printable<reshape_2D_type<M1>> {
    typedef typename M1::value_type value_type;
    const M1 m_m1;
    std::vector<size_t> m_row;
    std::vector<size_t> m_col;
    std::vector<size_t> m_dim_row;
    std::vector<size_t> m_dim_col;
    std::vector<size_t> m_stride_row;
    std::vector<size_t> m_stride_col;

    reshape_2D_type(const reshape_2D_type& other)
        : nd_matrix_base(other)
        , m_m1(other.m_m1)
        , m_row(other.m_row)
        , m_col(other.m_col)
        , m_stride_row(other.m_stride_row)
        , m_stride_col(other.m_stride_col)
    {}

    static size_t muldim(const M1& m1, const std::vector<size_t>& dim)
    {
        size_t accum = 1;
        for (size_t d: dim) { accum *= m1.dimension(d); }
        return accum;
    }

    reshape_2D_type(const M1& m1, const std::vector<size_t>& as_row, const std::vector<size_t>& as_col)
        : nd_matrix_base({muldim(m1, as_row), muldim(m1, as_col)})
        , m_m1(m1)
        , m_row(as_row)
        , m_col(as_col)
        , m_dim_row()
        , m_dim_col()
        , m_stride_row()
        , m_stride_col()
    {
        m_dim_row.reserve(m_row.size());
        m_dim_col.reserve(m_col.size());
        for (size_t d: m_row) {
            m_dim_row.push_back(m1.dimension(d));
        }
        for (size_t d: m_col) {
            m_dim_col.push_back(m1.dimension(d));
        }
        compute_strides(m_stride_row, m_dim_row);
        compute_strides(m_stride_col, m_dim_col);
    }

    struct const_cached_access {
        typedef reshape_2D_type<M1>::value_type value_type;

        const reshape_2D_type<M1>* m_this_matrix;
        typename M1::const_cached_access ac;
        std::vector<size_t> row_idx;
        std::vector<size_t> col_idx;
        std::vector<size_t> m1_idx;

        const_cached_access(const reshape_2D_type<M1>* m)
            : m_this_matrix(m)
            , ac(m_this_matrix->m_m1.accessor())
            , row_idx()
            , col_idx()
            , m1_idx()
        {
            row_idx.resize(m_this_matrix->m_row.size());
            col_idx.resize(m_this_matrix->m_col.size());
            m1_idx.resize(m_this_matrix->m_m1.dimensions().size());
        }

        value_type get(size_t idx) { return get(m_this_matrix->ofs_to_idx(idx)); }

        value_type get(const std::vector<size_t>& idx)
        {
            row_idx = ofs_to_idx(m_this_matrix->m_dim_row, m_this_matrix->m_stride_row, idx[0]);
            col_idx = ofs_to_idx(m_this_matrix->m_dim_col, m_this_matrix->m_stride_col, idx[1]);
            for (size_t i = 0; i < row_idx.size(); ++i) {
                m1_idx[m_this_matrix->m_row[i]] = row_idx[i];
            }
            for (size_t i = 0; i < col_idx.size(); ++i) {
                m1_idx[m_this_matrix->m_col[i]] = col_idx[i];
            }
            return ac.get(m1_idx);
        }
    };

    const_cached_access accessor() const { return {this}; }

    virtual nd_sparse_matrix as_sparse() const;
    virtual nd_dense_matrix as_dense() const;
};

template <>
nd_sparse_matrix reshape_2D_type<nd_sparse_matrix>::as_sparse() const
{
    nd_sparse_matrix ret(dimensions());
    auto ac = ret.accessor();
    auto my_ac = accessor();
    for (const auto& iv: m_m1.m_data->m_data) {
        ac.put(iv.first, my_ac.get(iv.first));
    }
    return ret;
}

template <typename M1>
nd_sparse_matrix reshape_2D_type<M1>::as_sparse() const
{
    nd_sparse_matrix ret(dimensions());
    auto ac = ret.accessor();
    auto my_ac = accessor();
    for (size_t i = 0; i < size(); ++i) {
        auto v = my_ac.get(i);
        if (v != 0) {
            ac.put(i, v);
        }
    }
    return ret;
}

template <typename M1>
nd_dense_matrix reshape_2D_type<M1>::as_dense() const
{
    nd_dense_matrix ret(dimensions());
    auto ac = ret.accessor();
    auto my_ac = accessor();
    for (size_t i = 0; i < size(); ++i) {
        ac.put(i, my_ac.get(i));
    }
    return ret;
}

template <typename M1>
reshape_2D_type<M1>
reshape_2D(const M1& m1, const std::vector<size_t>& rows, const std::vector<size_t>& cols) { return {m1, rows, cols}; }


/******************************************************************************
 ****************************************************************************
 *
 * TRANSPOSITION
 *
 **********************************************************************
 ********************************************************************
 */

template <typename M1>
struct transposition_type : public nd_matrix_base, public printable<transposition_type<M1>> {
    typedef typename M1::value_type value_type;
    const M1 m_m1;
    std::vector<size_t> m_order;

    transposition_type(const transposition_type& other)
        : nd_matrix_base(other)
        , m_m1(other.m_m1)
        , m_order(other.m_order)
    {
        reshape();
    }

    transposition_type(const M1& m1, const std::vector<size_t>& order)
        : nd_matrix_base(m1.dimensions())
        , m_m1(m1)
        , m_order(order)
    {
        reshape();
    }

    void reshape()
    {
        for (size_t i = 0; i < m_order.size(); ++i) {
            m_dimensions[m_order[i]] = m_m1.m_dimensions[i];
            m_strides[m_order[i]] = m_m1.m_strides[i];
        }
    }

    struct const_cached_access {
        typedef transposition_type<M1>::value_type value_type;

        const transposition_type<M1>* m_this_matrix;
        typename M1::const_cached_access ac;
        std::vector<size_t> ord_idx;

        const_cached_access(const transposition_type<M1>* m)
            : m_this_matrix(m)
            , ac(m_this_matrix->m_m1.accessor())
            , ord_idx()
        {
            ord_idx.resize(m_this_matrix->m_order.size());
        }

        value_type get(size_t idx) { return get(m_this_matrix->ofs_to_idx(idx)); }

        value_type get(const std::vector<size_t>& idx)
        {
            for (size_t i = 0; i < idx.size(); ++i) {
                ord_idx[i] = idx[m_this_matrix->m_order[i]];
            }
            return ac.get(ord_idx);
        }
    };

    const_cached_access accessor() const { return {this}; }

    virtual nd_sparse_matrix as_sparse() const;
    virtual nd_dense_matrix as_dense() const;
};

template <>
nd_sparse_matrix transposition_type<nd_sparse_matrix>::as_sparse() const
{
    nd_sparse_matrix ret(dimensions());
    auto ac = ret.accessor();
    auto my_ac = accessor();
    for (const auto& iv: m_m1.m_data->m_data) {
        ac.put(iv.first, my_ac.get(iv.first));
    }
    return ret;
}

template <typename M1>
nd_sparse_matrix transposition_type<M1>::as_sparse() const
{
    nd_sparse_matrix ret(dimensions());
    auto ac = ret.accessor();
    auto my_ac = accessor();
    for (size_t i = 0; i < size(); ++i) {
        auto v = my_ac.get(i);
        if (v != 0) {
            ac.put(i, v);
        }
    }
    return ret;
}

template <typename M1>
nd_dense_matrix transposition_type<M1>::as_dense() const
{
    nd_dense_matrix ret(dimensions());
    auto ac = ret.accessor();
    auto my_ac = accessor();
    for (size_t i = 0; i < size(); ++i) {
        ac.put(i, my_ac.get(i));
    }
    return ret;
}

template <typename M1>
transposition_type<M1>
transpose(const M1& m1, const std::vector<size_t>& order) { return {m1, order}; }


/******************************************************************************
 ****************************************************************************
 *
 * NORMALIZATION
 *
 **********************************************************************
 ********************************************************************
 */

template <typename M1>
struct normalization_type : public nd_matrix_base, public printable<normalization_type<M1>> {
    typedef typename M1::value_type value_type;
    const M1 m_m1;
    std::vector<size_t> m_dim_sum;
    nd_dense_matrix m_sum;

    normalization_type(const normalization_type& other)
        : nd_matrix_base(other)
        , m_m1(other.m_m1)
        , m_dim_sum(other.m_dim_sum)
        , m_sum(other.m_sum)
    {}

    normalization_type(const M1& m1, const std::vector<size_t>& dim_sum)
        : nd_matrix_base(m1.dimensions())
        , m_m1(m1)
        , m_dim_sum()
        , m_sum(sum_over(m1, dim_sum).as_dense())
    {
        size_t i = (size_t)(-1);
        size_t n = dimensions().size();
        m_dim_sum.reserve(n - dim_sum.size());
        for (size_t dim: dim_sum) {
            for (++i; i < dim; ++i) { m_dim_sum.push_back(i); }
        }
        for (++i; i < n; ++i) { m_dim_sum.push_back(i); }

        auto ac = m_sum.accessor();
        for (size_t i = 0; i < m_sum.size(); ++i) {
            value_type v = ac.get(i);
            if (v != 0) { ac.put(i, 1./v); }
        }
    }

    struct const_cached_access {
        typedef normalization_type<M1>::value_type value_type;

        const normalization_type<M1>* m_this_matrix;
        typename M1::const_cached_access ac;
        nd_dense_matrix::const_cached_access ac_sum;
        std::vector<size_t> sum_idx;

        const_cached_access(const normalization_type<M1>* m)
            : m_this_matrix(m)
            , ac(m_this_matrix->m_m1.accessor())
            , ac_sum(m_this_matrix->m_sum.accessor())
            , sum_idx()
        {
            sum_idx.resize(m_this_matrix->m_dim_sum.size());
        }

        value_type get(size_t idx) { return get(m_this_matrix->ofs_to_idx(idx)); }

        value_type get(const std::vector<size_t>& idx)
        {
            for (size_t i = 0; i < m_this_matrix->m_dim_sum.size(); ++i) {
                sum_idx[i] = idx[m_this_matrix->m_dim_sum[i]];
            }
            return ac.get(idx) * ac_sum.get(sum_idx);
        }
    };

    const_cached_access accessor() const { return {this}; }

    virtual nd_sparse_matrix as_sparse() const;
    virtual nd_dense_matrix as_dense() const;
};

template <>
nd_sparse_matrix normalization_type<nd_sparse_matrix>::as_sparse() const
{
    nd_sparse_matrix ret(dimensions());
    auto ac = ret.accessor();
    auto my_ac = accessor();
    for (const auto& iv: m_m1.m_data->m_data) {
        ac.put(iv.first, my_ac.get(iv.first));
    }
    return ret;
}

template <typename M1>
nd_sparse_matrix normalization_type<M1>::as_sparse() const
{
    nd_sparse_matrix ret(dimensions());
    auto ac = ret.accessor();
    auto my_ac = accessor();
    for (size_t i = 0; i < size(); ++i) {
        auto v = my_ac.get(i);
        if (v != 0) {
            ac.put(i, v);
        }
    }
    return ret;
}

template <typename M1>
nd_dense_matrix normalization_type<M1>::as_dense() const
{
    nd_dense_matrix ret(dimensions());
    auto ac = ret.accessor();
    auto my_ac = accessor();
    for (size_t i = 0; i < size(); ++i) {
        ac.put(i, my_ac.get(i));
    }
    return ret;
}

template <typename M1>
normalization_type<M1>
normalize_over(const M1& m1, const std::vector<size_t>& dim) { return {m1, dim}; }


/******************************************************************************
 ****************************************************************************
 *
 * HADAMARD
 *
 **********************************************************************
 ********************************************************************
 */

template <typename M1, typename M2>
struct hadamard_product_type : public nd_matrix_base, public printable<hadamard_product_type<M1, M2>> {
    typedef typename M1::value_type value_type;

    const M1 m_m1;
    const M2 m_m2;
    std::vector<std::pair<size_t, size_t>> m_common_dim;

    hadamard_product_type(const hadamard_product_type<M1, M2>& other)
        : nd_matrix_base(other)
        , m_m1(other.m_m1)
        , m_m2(other.m_m2)
        , m_common_dim(other.m_common_dim)
    {}

    hadamard_product_type(const M1& m1, const M2& m2, const std::vector<std::pair<size_t, size_t>>& common_dim)
        : nd_matrix_base(m1.dimensions())
        , m_m1(m1)
        , m_m2(m2)
        , m_common_dim(common_dim)
    {
#ifndef NDEBUG
        if (m1.dimensions().size() < m2.dimensions().size()) {
            throw std::runtime_error(SPELL_STRING("Left matrix must have a greater dimensionality than right matrix"));
        }
        if (m2.dimensions().size() != common_dim.size()) {
            throw std::runtime_error(SPELL_STRING("All dimensions of right matrix must be mapped to dimensions of the left matrix"));
        }
        for (const auto& dim: m_common_dim) {
            if (dim.first > m1.dimensions().size()) {
                throw std::runtime_error(SPELL_STRING("Invalid dimension number for left matrix (" << dim.first << " > " << m1.dimensions().size() << ")"));
            }
            if (dim.second > m2.dimensions().size()) {
                throw std::runtime_error(SPELL_STRING("Invalid dimension number for right matrix (" << dim.second << " > " << m2.dimensions().size() << ")"));
            }
            if (m1.dimension(dim.first) != m2.dimension(dim.second)) {
                throw std::runtime_error(SPELL_STRING("Incompatible dimensions #" << dim.first << "[" << m1.dimension(dim.first) << "] and #" << dim.second << "[" << m1.dimension(dim.second) << "]"));
            }
        }
#endif
        /*MSG_DEBUG("EMD m_dim_sum=(" << m_dim_sum << ") m_dim_size=(" << m_dim_size << ") dimensions=(" << dimensions() << ")");*/
    }

    struct const_cached_access {
        typedef hadamard_product_type<M1, M2>::value_type value_type;

        const hadamard_product_type<M1, M2>* m_this_matrix;
        typename M1::const_cached_access ac1;
        typename M2::const_cached_access ac2;
        std::vector<size_t> idx2;


        const_cached_access(const hadamard_product_type<M1, M2>* m)
            : m_this_matrix(m)
            , ac1(m_this_matrix->m_m1.accessor())
            , ac2(m_this_matrix->m_m2.accessor())
            , idx2()
        { idx2.resize(m_this_matrix->m_m2.dimensions().size(), 0); }

        value_type get(size_t idx) { return get(m_this_matrix->ofs_to_idx(idx)); }

        value_type get(const std::vector<size_t>& idx)
        {
            for (const auto& dimpair: m_this_matrix->m_common_dim) {
                idx2[dimpair.second] = idx[dimpair.first];
            }
            return ac1.get(idx) * ac2.get(idx2);
        }
    };

    const_cached_access accessor() const { return {this}; }

    virtual nd_sparse_matrix as_sparse() const
    {
        nd_sparse_matrix ret(dimensions());
        auto ac = ret.accessor();
        auto my_ac = accessor();
        for (size_t i = 0; i < size(); ++i) {
            auto v = my_ac.get(i);
            if (v != 0) {
                ac.put(i, v);
            }
        }
        return ret;
    }

    virtual nd_dense_matrix as_dense() const
    {
        nd_dense_matrix ret(dimensions());
        auto ac = ret.accessor();
        auto my_ac = accessor();
        for (size_t i = 0; i < size(); ++i) {
            ac.put(i, my_ac.get(i));
        }
        return ret;
    }
};


template <typename M1, typename M2>
hadamard_product_type<M1, M2>
hadamard(const M1& m1, const M2& m2, const std::vector<std::pair<size_t, size_t>>& common_dim) { return {m1, m2, common_dim}; }


/******************************************************************************
 ****************************************************************************
 *
 * KRONECKER
 *
 **********************************************************************
 ********************************************************************
 */

template <typename M1, typename M2>
struct kronecker_product_type : public nd_matrix_base, public printable<kronecker_product_type<M1, M2>> {
    typedef typename M1::value_type value_type;

    const M1 m_m1;
    const M2 m_m2;

    std::vector<size_t> join_dim(const M1& m1, const M2& m2)
    {
        std::vector<size_t> ret;
        ret.reserve(m1.dimensions().size() + m2.dimensions().size());
        ret.insert(ret.end(), m1.dimensions().begin(), m1.dimensions().end());
        ret.insert(ret.end(), m2.dimensions().begin(), m2.dimensions().end());
        return ret;
    }

    kronecker_product_type(const kronecker_product_type<M1, M2>& other)
        : nd_matrix_base(other)
        , m_m1(other.m_m1)
        , m_m2(other.m_m2)
    {}

    kronecker_product_type(const M1& m1, const M2& m2)
        : nd_matrix_base(join_dim(m1, m2))
        , m_m1(m1)
        , m_m2(m2)
    {
        /*MSG_DEBUG("EMD m_dim_sum=(" << m_dim_sum << ") m_dim_size=(" << m_dim_size << ") dimensions=(" << dimensions() << ")");*/
    }

    struct const_cached_access {
        typedef kronecker_product_type<M1, M2>::value_type value_type;

        const kronecker_product_type<M1, M2>* m_this_matrix;
        typename M1::const_cached_access ac1;
        typename M2::const_cached_access ac2;
        std::vector<size_t> idx1;
        std::vector<size_t> idx2;


        const_cached_access(const kronecker_product_type<M1, M2>* m)
            : m_this_matrix(m)
            , ac1(m_this_matrix->m_m1.accessor())
            , ac2(m_this_matrix->m_m2.accessor())
            , idx1()
            , idx2()
        {
            idx1.resize(m_this_matrix->m_m1.dimensions().size(), 0);
            idx2.resize(m_this_matrix->m_m2.dimensions().size(), 0);
        }

        value_type get(size_t idx) { return get(m_this_matrix->ofs_to_idx(idx)); }

        value_type get(const std::vector<size_t>& idx)
        {
            auto b = idx.begin();
            decltype(b) e1 = b + idx1.size();
            decltype(b) e2 = idx.end();
            idx1.assign(b, e1);
            idx2.assign(e1, e2);
            return ac1.get(idx1) * ac2.get(idx2);
        }
    };

    const_cached_access accessor() const { return {this}; }

    virtual nd_sparse_matrix as_sparse() const
    {
        nd_sparse_matrix ret(dimensions());
        auto ac = ret.accessor();
        auto my_ac = accessor();
        for (size_t i = 0; i < size(); ++i) {
            auto v = my_ac.get(i);
            if (v != 0) {
                ac.put(i, v);
            }
        }
        return ret;
    }

    virtual nd_dense_matrix as_dense() const
    {
        nd_dense_matrix ret(dimensions());
        auto ac = ret.accessor();
        auto my_ac = accessor();
        for (size_t i = 0; i < size(); ++i) {
            ac.put(i, my_ac.get(i));
        }
        return ret;
    }
};




template <typename M1, typename M2>
kronecker_product_type<M1, M2>
kronecker(const M1& m1, const M2& m2) { return {m1, m2}; }



/******************************************************************************
 ****************************************************************************
 *
 * PRODUCT FRONT-END
 *
 **********************************************************************
 ********************************************************************
 */

template <typename M1, typename M2>
extract_multiple_dimensions_type<hadamard_product_type<M1, M2>>
hyper_product(const M1& m1, const M2& m2, const std::vector<std::pair<size_t, size_t>>& dim_map)
{
    std::vector<size_t> dim_sum;
    dim_sum.reserve(dim_map.size());
    for (const auto& p: dim_map) { dim_sum.push_back(p.first); }
    return sum_over(hadamard(m1, m2, dim_map), dim_sum);
}








/******************************************************************************
 ****************************************************************************
 *
 * OUTPUT
 *
 **********************************************************************
 ********************************************************************
 */


#define HORIZ_LINE0 "\u2550"
#define VERT_LINE0 "\u2551"
#define CROSS0 "\u256C"
#define HORIZ_LINE1 "\u2501"
#define VERT_LINE1 "\u2503"
#define CROSS1 "\u254B"
#define HORIZ_LINE2 "\u2500"
#define VERT_LINE2 "\u2502"
#define CROSS2 "\u253C"

#define LEVEL(_l) (mat.dimensions().size() - (_l))
#define SELECT(_what_, _l) (LEVEL(_l) < 6 ? _what_ ## 2 : LEVEL(_l) < 8 ? _what_ ## 1 : _what_ ## 0)

#define SEP_CROSS(_l) SELECT(HORIZ_LINE, _l) << SELECT(CROSS, _l) << SELECT(HORIZ_LINE, _l)
#define SEP_HORIZ_LONG(_l) SELECT(HORIZ_LINE, _l) << SELECT(HORIZ_LINE, _l) << SELECT(HORIZ_LINE, _l)
#define SEP_HORIZ(_l) SELECT(HORIZ_LINE, _l)
#define SEP_VERT(_l) ' ' << SELECT(VERT_LINE, _l) << ' '


#define COL_START 0
#define ROW_START (1 - COL_START)


template <typename MAT_TYPE>
void print_mat_col_sep2(std::ostream& os, const MAT_TYPE& mat, size_t current, size_t n, size_t width)
{
    /*MSG_DEBUG("print_mat_col_sep2 " << current << " " << mat.dimensions().size() << " " << n);*/
    if (n >= mat.dimensions().size()) {
        return;
    }
    size_t last_field = mat.dimension(n) - 1;
    /*MSG_DEBUG("print_mat_col_sep2 " << (last_field + 1) << " fields");*/
    /*char f = os.fill('-');*/
    for (size_t i = 0; i < last_field; ++i) {
        if (n >= mat.dimensions().size() - 2) {
            /*os << std::setw(width) << "";*/
            for (size_t i = 0; i < width; ++i) { os << SELECT(HORIZ_LINE, current); }
        } else {
            print_mat_col_sep2(os, mat, current, n + 2, width);
        }
        if (n == current) {
            os << SEP_CROSS(current);
        } else if (n < current) {
            os << SEP_VERT(n);
        } else if (n < (mat.dimensions().size() - 2)) {
            os << SEP_HORIZ_LONG(current);
        } else {
            os << SEP_HORIZ(current);
        }
    }
    if (n >= mat.dimensions().size() - 2) {
        /*os << std::setw(width) << "";*/
        for (size_t i = 0; i < width; ++i) { os << SELECT(HORIZ_LINE, current); }
    } else {
        print_mat_col_sep2(os, mat, current, n + 2, width);
    }
    if (n == ROW_START) {
        os << std::endl;
    }
    /*os.fill(f);*/
}



static inline void print_double(std::ostream& os, double v, size_t width)
{
    char c = os.fill(' ');
    os << std::setw(width) << v;
    os.fill(c);
}


template <typename MAT_TYPE>
void print_mat_row(std::ostream& os, const MAT_TYPE& mat, std::vector<size_t>& idx, size_t current, size_t width)
{
    if (current >= idx.size()) {
        return;
    }
    size_t n = mat.dimension(current) - 1;
    if (idx.size() < 2 || current >= idx.size() - 2) {
        auto acc = mat.accessor();
        for (idx[current] = 0; idx[current] < n; ++idx[current]) {
            print_double(os, acc.get(idx), width);
            os << ' ';
        }
        print_double(os, acc.get(idx), width);
    } else {
        for (idx[current] = 0; idx[current] < n; ++idx[current]) {
            print_mat_row(os, mat, idx, current + 2, width);
            os << SEP_VERT(current);
        }
        print_mat_row(os, mat, idx, current + 2, width);
    }
    if (current == ROW_START) {
        os << std::endl;
    }
}


template <typename MAT_TYPE>
void print_mat_col(std::ostream& os, const MAT_TYPE& mat, std::vector<size_t>& idx, size_t current, size_t width)
{
    if (current >= idx.size()) {
        print_mat_row(os, mat, idx, ROW_START, width);
        return;
    }
    size_t n = mat.dimension(current) - 1;
    for (idx[current] = 0; idx[current] < n; ++idx[current]) {
        print_mat_col(os, mat, idx, current + 2, width);
        if (current < mat.dimensions().size() - 2 + COL_START) {
            print_mat_col_sep2(os, mat, current - COL_START + ROW_START, ROW_START, width);
        }
    }
    print_mat_col(os, mat, idx, current + 2, width);
    /*os << std::endl;*/
}


template <typename MAT_TYPE>
void print_mat(std::ostream& os, const MAT_TYPE& mat)
{
    std::vector<size_t> indexes;
    size_t width = 0;
    auto ac = mat.accessor();
    for (size_t i = 0; i < mat.size(); ++i) {
        size_t tmp = SPELL_STRING(ac.get(i)).size();
        if (tmp > width) {
            width = tmp;
        }
    }
    /*MSG_DEBUG("dimensions.size=" << mat.dimensions().size());*/
    indexes.resize(mat.dimensions().size(), 0);
    if (indexes.size() == 0) {
        os << ac.get(0);
    } else if (indexes.size() == 1) {
        os << ac.get(0);
        for (size_t i = 1; i < mat.size(); ++i) {
            os << " " << ac.get(i);
        }
    } else {
        auto i = mat.dimensions().begin();
        auto j = mat.dimensions().end();
        if (i != j) {
            os << (*i);
            for (++i; i != j; ++i) {
                os << 'x' << (*i);
            }
        }
    }
    os << std::endl;
    print_mat_col(os, mat, indexes, COL_START, width);
}


#if 0
static inline std::ostream& operator << (std::ostream& os, const nd_sparse_matrix& mat)
{
    os << mat.m_data << std::endl;
    print_mat(os, mat);
    return os;
}

static inline std::ostream& operator << (std::ostream& os, const nd_dense_matrix& mat)
{
    print_mat(os, mat);
    return os;
}
#endif


template <typename BACKEND_TYPE>
std::ostream& operator << (std::ostream& os, const extract_multiple_dimensions_type<BACKEND_TYPE>& mat)
{
    if (mat.dimensions().size()) {
        print_mat(os, mat);
    } else {
        auto ac = mat.accessor();
        os << ac.get(0) << std::endl;
    }
    return os;
}


template <typename... T>
std::ostream& operator << (std::ostream& os, const printable<T...>& mat)
{
    print_mat(os, mat.to_type());
    return os;
}


/******************************************************************************
 ****************************************************************************
 *
 * EIGEN <-> ND_MATRIX CONVERSIONS
 *
 **********************************************************************
 ********************************************************************
 */

#include "eigen.h"


template <typename M>
const MatrixXd& operator >> (const MatrixXd& e_mat, printable<M>& nd_mat)
{
    const M& mat = nd_mat.to_type();
    if ((unsigned)e_mat.size() != mat.size()) {
        throw std::runtime_error("Incompatible matrix sizes in conversion");
    }
    const double* data = e_mat.data();
    auto ac = mat.accessor();
    for (size_t i = 0; i < mat.size(); ++i) { ac.put(i, data[i]); }
    return e_mat;
}


template <typename M>
MatrixXd& operator << (MatrixXd& e_mat, const printable<M>& nd_mat)
{
    const M& mat = nd_mat.to_type();
    if ((unsigned)e_mat.size() != mat.size()) {
        throw std::runtime_error("Incompatible matrix sizes in conversion");
    }
    double* data = e_mat.data();
    auto ac = mat.accessor();
    for (size_t i = 0; i < mat.size(); ++i) { data[i] = ac.get(i); }
    return e_mat;
}


template <>
MatrixXd& operator << (MatrixXd& e_mat, const printable<nd_sparse_matrix>& nd_mat)
{
    const nd_sparse_matrix& mat = nd_mat.to_type();
    if ((unsigned)e_mat.size() != mat.size()) {
        throw std::runtime_error("Incompatible matrix sizes in conversion");
    }
    e_mat = MatrixXd::Zero(e_mat.rows(), e_mat.cols());
    double* data = e_mat.data();
    for (const auto& iv: mat.m_data->m_data) {
        data[iv.first] = iv.second;
    }
    return e_mat;
}


#endif

