/*
 * Copyright (c) 2016 Damien Leroux, Sylvain Jasson, INRA
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _SPEL_BAYES_FACTORGRAPH_H_
#define _SPEL_BAYES_FACTORGRAPH_H_

#include "factor.h"


struct compute_labels {
    bn_label_type
        find_label(size_t n, const genotype_comb_type::element_type& labels)
        {
            auto it
                = std::find_if(labels.keys.begin(), labels.keys.end(),
                               [=] (const genotype_comb_type::key_type& k) { return k.parent == n; }
                  );
            if (it == labels.keys.end()) {
                MSG_ERROR("COULDN'T FIND LABEL FOR " << n << " IN " << labels, "");
                MSG_QUEUE_FLUSH();
                return {};
            }
            return it->state;
        }

    bn_label_type
        operator () (const pedigree_tree_type& tree, size_t n, const genotype_comb_type::element_type& labels, const std::vector<bool>& recompute)
        {
            if (tree.get_p2(n) == NONE) {
                /* gamete or ancestor */
                if (tree.get_p1(n) == NONE) {
                    /* ancestor */
                    return find_label(n, labels);
                } else {
                    auto gl = find_label(n, labels);
                    auto sub = operator () (tree, tree.get_p1(n), labels, recompute);
                    if (gl.first == GAMETE_L) {
                        return {sub.first, 0, sub.first_allele, 0};
                    } else {
                        return {sub.second, 0, sub.second_allele, 0};
                    }
                }
            } else  if (recompute[n]) {
                auto subl = operator () (tree, tree.get_p1(n), labels, recompute);
                auto subr = operator () (tree, tree.get_p2(n), labels, recompute);
                return {subl.first, subr.first, subl.first_allele, subr.first_allele};
            } else {
                return find_label(n, labels);
            }
        }

    std::vector<bn_label_type>
        operator () (const pedigree_tree_type& tree, size_t n, const genotype_comb_type& comb, const std::vector<bool>& recompute)
        {
            std::vector<bn_label_type> ret;
            ret.reserve(comb.m_combination.size());
            for (const auto& e: comb) {
                ret.emplace_back(operator () (tree, n, e, recompute));
            }
            return ret;
        }
};


struct factor_graph : public bn_graph_node {
    void
        add_ind_domain(size_t ind_node, const std::vector<bn_label_type>& table)
        {
            std::set<bn_label_type> uniq(table.begin(), table.end());
            m_variable_domains[ind_node].assign(uniq.begin(), uniq.end());
        }

    bn_factor
        create_factor(const pedigree_tree_type& T, size_t spawnling)
        {
            size_t g1 = T.get_p1(spawnling);
            size_t g2 = T.get_p2(spawnling);
            size_t p1 = T.get_p1(g1);
            size_t p2 = T.get_p1(g2);
            genotype_comb_type
                P1 = state_to_combination(p1, m_variable_domains[p1]);
            genotype_comb_type
                P2 = state_to_combination(p2, m_variable_domains[p2]);
            genotype_comb_type
                G1 = state_to_combination(g1, label_g) * .5;
            genotype_comb_type
                G2 = state_to_combination(g2, label_g)
                * (g1 != g2 ? .5 : 1);
            genotype_comb_type
                cross = kronecker(kronecker(P1, P2), kronecker(G1, G2));
            std::vector<bool> recompute(spawnling + 1, false);
            recompute.back() = true;
            auto label_per_state = compute_labels()(T, spawnling, cross, recompute);
            auto new_jp_table
                = fold(sum_over(hadamard(cross, state_to_combination(spawnling, label_per_state)), {g1, g2}));
            bn_factor ret = std::make_shared<bn_factor_type>(new_jp_table);
            ret->compute_io_sets(T);
            add_ind_domain(spawnling, label_per_state);

            m_factor_by_output[spawnling] = ret;

            return ret;
        }

























    factor_graph()
        : m_variable_domains(), m_factors(), m_n_alleles(0)
        , m_updaters(), m_extracters(), m_message_headers()
        , m_noise(0), m_updater_order()
    {}

    factor_graph(const pedigree_type& ped, size_t n_alleles, double noise=0)
        : m_variable_domains(), m_factors()
        , m_n_alleles(n_alleles)
        , m_updaters(), m_extracters(), m_message_headers()
        , m_noise(noise), m_updater_order()
    {
        /*compute_factors_and_domains(ped);*/
        /*compute_interfaces();*/
        build_factors(ped);
        compile(ped.tree);
    }


    struct factor_creation_list_type {
        struct factor_creation_op {
            std::vector<size_t> variables;
            /*std::vector<size_t> f1_vars, f2_vars;*/
            std::vector<std::vector<size_t>> interfaces;
            std::vector<size_t> progeny;

            void
                cross(const pedigree_tree_type& T, factor_creation_list_type& fcl, std::vector<bn_factor_ptr>& factors) const
                {
                    static std::vector<bn_label_type> label_g = {{GAMETE_L, 0, 0, 0}, {GAMETE_R, 0, 0, 0}};

                    auto get_factor
                        = [&] (const std::vector<size_t>& interface) -> genotype_comb_type
                        {
                            if (interface.size() == 1 && T[interface.front()].is_ancestor()) {
                                return state_to_combination(interface.front(), fcl.get_domain(interface.front()));
                            } else {
                                size_t comp_fac = fcl.find_compatible_factor(interface);
                                return factors[comp_fac]->project(interface);
                            }
                        };

                    /*MSG_DEBUG(std::endl << "cross(" << variables << ", " << f1_vars << ", " << f2_vars << ", " << ((int) progeny) << ')');*/
                    MSG_QUEUE_FLUSH();
                    auto i = interfaces.begin(), j = interfaces.end();
                    genotype_comb_type parents = get_factor(*i);
                    for (++i; i != j; ++i) {
                        parents = kronecker(parents, get_factor(*i));
                    }
                    /*MSG_DEBUG("COMPUTING CROSS. parents = " << parents);*/
                    /*MSG_QUEUE_FLUSH();*/
                    if (progeny.size()) {
                        std::vector<genotype_comb_type> crosses;
                        crosses.reserve(progeny.size());
                        for (size_t spawnling: progeny) {
                            genotype_comb_type
                                G1 = state_to_combination((size_t) T.get_p1(spawnling), label_g) * .5;
                            genotype_comb_type
                                G2 = state_to_combination((size_t) T.get_p2(spawnling), label_g)
                                * (T.get_p1(spawnling) != T.get_p2(spawnling) ? .5 : 1);
                            genotype_comb_type
                                unmarked_cross = kronecker(parents, kronecker(G1, G2));
                            /*MSG_DEBUG("unmarked_cross #" << spawnling << ' ' << unmarked_cross);*/
                            /*MSG_QUEUE_FLUSH();*/
                            std::vector<bool> recompute(spawnling + 1, false);
                            recompute.back() = true;
                            auto label_per_state = compute_labels()(T, spawnling, unmarked_cross, recompute);
                            auto new_jp_table
                                = fold(sum_over(hadamard(unmarked_cross, state_to_combination(spawnling, label_per_state)),
                                            {(size_t) T.get_p1(spawnling), (size_t) T.get_p2(spawnling)}));
                            crosses.emplace_back(new_jp_table);
                            fcl.add_ind_domain(spawnling, label_per_state);
                        }
                        for (size_t i = 1; i < crosses.size(); ++i) {
                            crosses[0] = kronecker(crosses[0], crosses[i]);
                        }
                        factors.emplace_back(std::make_shared<bn_factor_type>(crosses[0]));
                    } else {
                        factors.emplace_back(std::make_shared<bn_factor_type>(parents));
                    }
                    factors.back()->compute_io_sets(T);
                    /*MSG_DEBUG("DONE" << std::endl << (*factors.back()));*/
                }

            friend
                std::ostream&
                operator << (std::ostream& os, const factor_creation_op& op)
                {
                    std::stringstream ss;
                    auto i = op.interfaces.begin(), j = op.interfaces.end();
                    ss << '{';
                    if (i != j) {
                        ss << (*i);
                        for (++i; i != j; ++i) {
                            ss << "} ⨝ {" << (*i);
                        }
                    }
                    ss << '}';
                    if (op.progeny.size()) {
                        return os << '{' << op.variables << "}: {" << op.progeny << "} = " << ss.str();
                    } else {
                        return os << '{' << op.variables << "}: " << ss.str();
                    }
                }
        };

        const std::vector<bn_label_type>&
            get_domain(size_t n) const
            {
                static std::vector<bn_label_type> empty;
                auto it = variable_domains.find(n);
                return it == variable_domains.end() ? empty : it->second;
            }

        size_t
            find_compatible_factor(const std::vector<size_t>& interface) const
            {
                auto
                    ret = std::find_if(operations.begin(), operations.end(),
                            [&] (const factor_creation_op& fco)
                            {
                                return std::includes(fco.variables.begin(), fco.variables.end(),
                                                     interface.begin(), interface.end());
                            });
                if (ret == operations.end()) {
                    /*MSG_DEBUG("find_compatible_factor(" << interface << ") => not found");*/
                    return (size_t) -1;
                }
                /*MSG_DEBUG("find_compatible_factor(" << interface << ") => " << (ret - operations.begin()));*/
                return ret - operations.begin();
            }

        std::vector<size_t>
            joint_ancestors(const pedigree_tree_type& T, size_t node, const std::vector<size_t>& reent) const
            {
                auto p_anc = T.count_ancestors(node);
                std::vector<size_t> joint_reent;
                joint_reent.reserve(reent.size());
                for (size_t r: reent) {
                    if (p_anc.find(r) != p_anc.end()) {
                        joint_reent.push_back(r);
                    }
                }

                filter_unique(joint_reent);

                return joint_reent;
            }

        std::vector<size_t>
            unite(size_t n, const std::vector<size_t>& v1, const std::vector<size_t>& v2) const
            {
                std::vector<size_t> ret(v1.size() + v2.size() + 1);
                auto it = std::set_union(v1.begin(), v1.end(), v2.begin(), v2.end(), ret.begin());
                if (n != (size_t) -1) {
                    *it++ = n;
                }
                ret.resize(it - ret.begin());
                /*MSG_DEBUG("unite(" << ((long int) n) << ", " << v1 << ", " << v2 << ") = " << ret);*/
                /*MSG_QUEUE_FLUSH();*/
                return ret;
                /*std::set<size_t> tmp;*/
                /*if (n != (size_t) -1) {*/
                    /*tmp.insert(n);*/
                /*}*/
                /*tmp.insert(v1.begin(), v1.end());*/
                /*tmp.insert(v2.begin(), v2.end());*/
                /*return {tmp.begin(), tmp.end()};*/
            }

        void
            make_unique(std::vector<size_t>& vec)
            {
                std::sort(vec.begin(), vec.end());
                auto last = std::unique(vec.begin(), vec.end());
                vec.erase(last, vec.end());
            }

        size_t
            make_joint_factor_given(const pedigree_tree_type& T, const std::vector<size_t>& ind_vec, const std::vector<size_t>& given)
            {
                /*scoped_indent _(SPELL_STRING("[make_joint_factor_given {" << ind_vec << "} {" << given << "}] "));*/

                std::vector<std::vector<size_t>> given_per_ind;
                std::vector<size_t> joint_parents;
                std::vector<size_t> independent_parents;

                /*MSG_DEBUG("computing joint parents");*/
                MSG_QUEUE_FLUSH();
                for (size_t ind: ind_vec) {
                    size_t p = (size_t) T.get_p1(T.get_p1(ind));
                    auto ja = joint_ancestors(T, p, given);
                    /*MSG_DEBUG("parent " << ind << " has " << ja);*/
                    /*MSG_QUEUE_FLUSH();*/
                    if (ja.size()) {
                        joint_parents.push_back(p);
                        given_per_ind.push_back(ja);
                    } else {
                        independent_parents.push_back(p);
                    }
                    p = (size_t) T.get_p1(T.get_p2(ind));
                    ja = joint_ancestors(T, p, given);
                    /*MSG_DEBUG("parent " << ind << " has " << ja);*/
                    /*MSG_QUEUE_FLUSH();*/
                    if (ja.size()) {
                        joint_parents.push_back(p);
                        given_per_ind.push_back(ja);
                    } else {
                        independent_parents.push_back(p);
                    }
                }
                /*MSG_DEBUG("joint parents before make_unique " << joint_parents);*/
                /*MSG_QUEUE_FLUSH();*/
                make_unique(joint_parents);
                /*MSG_DEBUG("joint parents after make_unique " << joint_parents);*/
                /*MSG_QUEUE_FLUSH();*/

                if (joint_parents.size()) {
                    make_joint_factor_given(T, joint_parents, given);
                }

                operations.emplace_back();
                auto& op = operations.back();
#if 0
                if (joint_parents.size()) {
                    op.interfaces.push_back(joint_parents);
                    op.variables.insert(op.variables.end(), joint_parents.begin(), joint_parents.end());
                }
                for (size_t p: independent_parents) {
                    op.interfaces.emplace_back(std::vector<size_t>{p});
                    op.variables.push_back(p);
                }
#else
                for (size_t ind: ind_vec) {
                    size_t p1 = T.get_p1(T.get_p1(ind));
                    size_t p2 = T.get_p1(T.get_p2(ind));
                    if (std::find(joint_parents.begin(), joint_parents.end(), p1) != joint_parents.end()
                            || std::find(joint_parents.begin(), joint_parents.end(), p1) != joint_parents.end()) {
                        std::vector<size_t> itf = joint_parents;
                        itf.push_back(p1);
                        itf.push_back(p2);
                        make_unique(itf);
                        op.interfaces.emplace_back(itf);
                    } else {
                        if (p1 > p2) { p1 ^= p2; p2 ^= p1; p1 ^= p2; }
                        op.interfaces.emplace_back(std::vector<size_t>{p1, p2});
                    }
                    op.variables.push_back(ind);
                    op.variables.push_back(p1);
                    op.variables.push_back(p2);
                }
                op.progeny = ind_vec;
#endif
                make_unique(op.variables);
                /*MSG_DEBUG("created operation " << op);*/
                /*MSG_QUEUE_FLUSH();*/
                return operations.size() - 1;
            }

        void
            add_ind_domain(size_t ind_node, const std::vector<bn_label_type>& table)
            {
                std::set<bn_label_type> uniq(table.begin(), table.end());
                variable_domains[ind_node].assign(uniq.begin(), uniq.end());
            }

        void
            add_ind(const pedigree_type& ped, size_t ind_node, size_t n_alleles)
            {
                /*scoped_indent _(SPELL_STRING("[add_ind(" << ind_node << ")] "));*/
                if (ped.tree[ind_node].is_ancestor()) {
                    MSG_DEBUG("add_ind(" << ind_node << ") is ancestor");
                    /*MSG_DEBUG("... is ancestor");*/
                    std::vector<bn_label_type> labels;
                    char letter = ped.ancestor_letters.find(ind_node)->second;
                    for (size_t i = 0; i < n_alleles; ++i) {
                        labels.emplace_back(letter, letter, i, i);
                    }
                    variable_domains[ind_node] = labels;
                } else {
                    genotype_comb_type result;
                    std::vector<size_t> itf1, itf2;
                    size_t p1 = (size_t) ped.tree.get_p1(ped.tree.get_p1(ind_node));
                    size_t p2 = (size_t) ped.tree.get_p1(ped.tree.get_p2(ind_node));
                    MSG_DEBUG("add_ind(" << ind_node << ", " << p1 << ", " << p2 << ')');
                    auto reent = ped.tree.cleanup_reentrants(ind_node);
                    if (p1 != p2 && reent.size()) {
                        /*MSG_DEBUG("... has reentrants");*/
                        /*MSG_QUEUE_FLUSH();*/
                        std::vector<size_t> parents = p1 < p2 ? std::vector<size_t>{p1, p2} : std::vector<size_t>{p2, p1};
                        std::vector<size_t> R; // (reent.begin(), reent.end());
                        R.reserve(reent.size());
                        for (const auto& kv: reent) { R.push_back(kv.first); }
                        make_joint_factor_given(ped.tree, parents, R);
                        {
                            /* create factor with joint parents */
                            operations.emplace_back();
                            auto& op = operations.back();
                            op.variables = {p1 > p2 ? p2 : p1, p1 > p2 ? p1 : p2, ind_node};
                            op.progeny = {ind_node};
                            op.interfaces.emplace_back(std::vector<size_t>{p1 > p2 ? p2 : p1, p1 > p2 ? p1 : p2});
                            /*MSG_DEBUG("... result for #" << ind_node << ": " << op);*/
                        }
                    } else {
                        /*MSG_DEBUG("... simple cross");*/
                        operations.emplace_back();
                        auto& op = operations.back();
                        op.variables = {p1 < p2 ? p1 : p2, p1 < p2 ? p2 : p1, ind_node};
                        op.interfaces.emplace_back(std::vector<size_t>{p1});
                        op.interfaces.emplace_back(std::vector<size_t>{p2});
                        op.progeny = {ind_node};
                        /*MSG_DEBUG("... result for #" << ind_node << ": " << op);*/
                    }
                }
                /*MSG_DEBUG("");*/
            }

        void
            add_all(const pedigree_type& ped, size_t n_alleles)
            {
                for (size_t ind: ped.tree.m_ind_number_to_node_number) {
                    if (ind == (size_t) NONE) {
                        continue;
                    }
                    add_ind(ped, ind, n_alleles);
                }

                std::vector<bool> leaves(ped.tree.m_nodes.size(), true);
                for (const auto& n: ped.tree.m_nodes) {
                    if (n.p1 != NONE) { leaves[n.p1] = false; }
                    if (n.p2 != NONE) { leaves[n.p2] = false; }
                }

                auto l = leaves.begin();
                for (size_t i = 0; i < leaves.size(); ++i, ++l) {
                    if (*l) {
                        MSG_DEBUG("LEAF " << i);
                    }
                }

                for (const auto& op: operations) {
                    MSG_DEBUG("[OP] " << op);
                }
            }

        void
            cleanup()
            {
                std::vector<bool> included(operations.size(), false);
                size_t total = operations.size();
                for (size_t i1 = 0; i1 < operations.size(); ++i1) {
                    if (included[i1]) { continue; }
                    const auto& o1 = operations[i1];
                    for (size_t i2 = 0; i2 < operations.size(); ++i2) {
                        if (included[i2] || i1 == i2) { continue; }
                        const auto& o2 = operations[i2];
                        if (o1.variables == o2.variables) {
                            included[std::max(i1, i2)] = true;
                            continue;
                        }
                        if (std::includes(o2.variables.begin(), o2.variables.end(), o1.variables.begin(), o1.variables.end())) {
                            total -= !included[i1];
                            included[i1] = true;
                        }
                    }
                }
                std::vector<factor_creation_op> tmp;
                tmp.reserve(total);
                for (size_t i = 0; i < included.size(); ++i) {
                    if (!included[i]) {
                        tmp.emplace_back(operations[i]);
                    }
                }
                operations.swap(tmp);

                for (const auto& op: operations) {
                    MSG_DEBUG("[POST CLEANUP OP] " << op);
                }
            }

        void
            compute_factors(const pedigree_type& ped, std::vector<bn_factor_ptr>& factors)
            {
                for (const auto& op: operations) {
                    op.cross(ped.tree, *this, factors);
                }
            }

        void
            export_domains(std::map<size_t, std::vector<bn_label_type>>& domains)
            {
                variable_domains.swap(domains);
            }

    private:
        std::vector<factor_creation_op> operations;
        std::map<size_t, std::vector<bn_label_type>> variable_domains;
    };

    void
        build_factors(const pedigree_type& ped)
        {
            factor_creation_list_type factor_creation_operations;
            factor_creation_operations.add_all(ped, m_n_alleles);
            /*factor_creation_operations.cleanup();*/
            factor_creation_operations.compute_factors(ped, m_factors);
            cleanup_factor_list();
            factor_creation_operations.export_domains(m_variable_domains);
        }

    void
        init_variable_message(size_t var, const bn_message_type& ref, bn_message_type& vm) const
        {
            /*MSG_DEBUG("Using noise value = " << m_noise);*/
            double eps_2 = m_noise * .5;
            double noisy_one = 1. - m_noise;
            for (const bn_label_type& lab: m_variable_domains.find(var)->second) {
                genotype_comb_type::key_list kl = {{var, lab}};
                auto it = ref.find(kl);
                if (it == ref.end()) {
                    vm.force_set(kl, eps_2);
                } else {
                    vm.force_set(kl, eps_2 + noisy_one * ref[kl]);
                }
            }
        }

    friend
        std::ostream&
        operator << (std::ostream& os, const factor_graph& fg)
        {
            os << "FACTOR GRAPH @" << (&fg) << std::endl;
            os << "Variable domains:" << std::endl;
            for (const auto& kv: fg.m_variable_domains) {
                os << "  - " << kv.first << ": " << kv.second << std::endl;
            }
            for (const auto& f: fg.m_factors) {
                os << (*f) << std::endl;
            }
            /*os << "Ordered m_interfaces:" << std::endl;*/
            /*for (const auto& i: fg.m_interfaces) {*/
                /*os << (*i) << std::endl;*/
            /*}*/
            return os;
        }

    void
        cleanup_factor_list()
        {
            /* 
             * cleanup
             * - a factor is dismissed if its variables are all included in at least another factor's variables
             */
            /*MSG_DEBUG("BEFORE CLEANUP: " << m_factors.size() << " FACTORS.");*/
            /*for (const auto& f: m_factors) {*/
                /*MSG_DEBUG("" << (*f));*/
            /*}*/

            std::vector<bool> factor_included(m_factors.size(), false);
            size_t total = m_factors.size();
            for (size_t fsmall = 0; fsmall < m_factors.size(); ++fsmall) {
                if (factor_included[fsmall]) { continue; }
                for (size_t fbig = 0; fbig < m_factors.size(); ++fbig) {
                    if (factor_included[fbig]) { continue; }
                    if (fbig == fsmall) { continue; }
                    if (m_factors[fbig]->variables() == m_factors[fsmall]->variables()) {
                        factor_included[std::max(fsmall, fbig)] = true;
                        continue;
                    }
                    if (std::includes(m_factors[fbig]->variables().begin(), m_factors[fbig]->variables().end(),
                                      m_factors[fsmall]->variables().begin(), m_factors[fsmall]->variables().end())) {
                        total -= !factor_included[fsmall];  /* decrease if not already detected as an included-inside-another-factor factor. */
                        factor_included[fsmall] = true;
                        /*MSG_DEBUG("FACTOR #" << fsmall << ' ' << m_factors[fsmall]->variables() << " INCLUDED IN #" << fbig << ' ' << m_factors[fbig]->variables());*/
                    }
                }
            }
            std::vector<bn_factor_ptr> tmp_factors;
            tmp_factors.reserve(total);
            for (size_t i = 0; i < factor_included.size(); ++i) {
                if (!factor_included[i]) {
                    tmp_factors.emplace_back(m_factors[i]);
                }
            }
            m_factors.swap(tmp_factors);
            std::sort(m_factors.begin(), m_factors.end(), [](const bn_factor_ptr& fp1, const bn_factor_ptr& fp2) { return *fp1 < *fp2; });
            /*MSG_DEBUG("AFTER CLEANUP: " << m_factors.size() << " FACTORS.");*/
        }

    const std::vector<bn_label_type>&
        get_domain(size_t i) const
        {
            static std::vector<bn_label_type> empty;
            auto it = m_variable_domains.find(i);
            return it == m_variable_domains.end() ? empty : it->second;
        }

#if 0
    void
        clear_messages()
        {
            for (auto& i: m_interfaces) {
                i->clear();
            }
        }

    void
        reset()
        {
            for (auto& itf: m_interfaces) {
                itf->clear();
            }
        }
#endif

    friend struct instance_type;

    struct instance_type {
        instance_type(const factor_graph* _)
            : m_fg(_)
            , m_messages(_->m_message_headers.size())
            , m_front(false)
            , m_observations(1.) {}

        double delta() const { return m_messages.delta(); }

        bn_message_type&
            evidence() { return m_observations; }

        const bn_message_type&
            evidence() const { return m_observations; }

        instance_type&
            clear_evidence() { m_observations.clear(); return *this; }

        instance_type&
            evidence(size_t var, bn_label_type label, double prob)
            {
                m_observations.force_set({{var, label}}, m_fg->m_noise * .5 + (1. - m_fg->m_noise) * prob);
                return *this;
            }

        instance_type&
            evidence(const bn_message_type& buffer)
            {
                std::set<size_t> vars;
                for (const auto& kv: buffer) {
                    vars.insert(kv.first.begin()->parent);
                }
                for (size_t v: vars) {
                    m_fg->init_variable_message(v, buffer, m_observations);
                }
                /*for (const auto& kv: buffer) {*/
                    /*m_observations.set(kv.first, kv.second);*/
                /*}*/
                return *this;
            }

        double
            run(bn_message_type& marginals, size_t max_iterations=100, double convergence_threshold=1.e-10)
            {
                /*MSG_DEBUG("Running with observations:");*/
                /*MSG_DEBUG("" << m_observations.dump());*/
                double d = std::numeric_limits<double>::infinity();
                m_messages.clear();
                size_t i;
#if 0
                m_messages.double_buffering(true);
                for (i = 0; i < max_iterations && d > convergence_threshold;) {
                    d = update();
                    MSG_DEBUG("Iteration #" << (i++) << " delta=" << d);
                    dump_messages();
                }
#else
                m_messages.double_buffering(false);
                for (const auto& upd: m_fg->m_updaters) {
                    upd.update(m_messages, true, m_observations, m_messages);
                }
#endif
                /*MSG_DEBUG("Iteration #" << (i++));*/
                dump_messages();
                extract(marginals);
                return d;
            }

        void
            dump_messages() const
            {
                const auto& front = m_messages.get(m_front);
                const auto& back = m_messages.get(!m_front);
                auto fi = front.begin(), fj = front.end(), bi = back.begin();
                auto hi = m_fg->m_message_headers.begin();
                for (; fi != fj; ++fi, ++bi, ++hi) {
                    MSG_DEBUG("" << (*hi) << "    " << (*fi) << "    " << (*bi));
                }
            }

    protected:
        void
            extract(bn_message_type& marginals)
            {
                bn_message_buffer_type beliefs(m_fg->m_variable_domains.rbegin()->first + 1);
                beliefs.double_buffering(false);
                for (const auto& xtr: m_fg->m_extracters) {
                    xtr.update(m_messages, m_front, m_observations, beliefs);
                }
                marginals.clear();

                genotype_comb_type::key_list key;
                const auto& bebuf = beliefs.get(false);
                for (const auto& vd: m_fg->m_variable_domains) {
                    size_t var = vd.first;
                    const auto& domain = vd.second;
                    for (const auto& st: domain) {
                        key = {{var, st}};
                        marginals.set(key, bebuf[var][key]);
                    }
                }
            }

    private:
        const factor_graph* m_fg;
        bn_message_buffer_type m_messages;
        bool m_front;
        bn_message_type m_observations;
    };


    struct msg_descr {
        bn_factor_ptr emitter;
        std::vector<size_t> variables;
        /*msg_descr(bn_factor_ptr e, const std::vector<size_t>& v)*/
            /*: emitter(e)*/
            /*, variables(v)*/
        /*{}*/
        /*msg_descr& operator = (msg_descr& d) { emitter = d.emitter; variables = d.variables; return *this; }*/
        bool operator < (const msg_descr& other) const { return emitter->variables() < other.emitter->variables()
                                                             || (emitter->variables() == other.emitter->variables() && variables < other.variables); }
        friend
            std::ostream&
            operator << (std::ostream& os, const msg_descr& md) { return os << '{' << md.emitter->variables() << "}=>[" << md.variables << ']'; }
    };

    template <typename Coll>
        struct rev_struc {
            typedef typename Coll::reverse_iterator iterator;
            iterator _beg, _end;
            rev_struc(Coll& c) : _beg(c.rbegin()), _end(c.rend()) {}
            iterator begin() { return _beg; }
            iterator end() { return _end; }
        };

    template <typename Coll>
        rev_struc<Coll> reverse(Coll& c) { return {c}; }

    template <typename Coll>
        struct skip_struc {
            typedef typename Coll::iterator iterator;
            iterator _beg, _end;
            skip_struc(Coll& c, size_t skip) : _beg(c.begin() + skip), _end(c.end()) {}
            iterator begin() { return _beg; }
            iterator end() { return _end; }
        };

    template <typename Coll>
        skip_struc<Coll> skip(Coll& c, size_t s) { return {c, s}; }

    void
        compile(const pedigree_tree_type& T)
        {
            std::vector<bn_factor_ptr> factor_by_output_var(m_variable_domains.rbegin()->first + 1);

            std::vector<bool> ind_visited(T.size(), false);
            std::vector<std::shared_ptr<bn_factor_type>> factor_by_ind;

            std::map<bn_factor_ptr, std::vector<std::vector<size_t>>> factor_outputs;

            std::map<msg_descr, size_t> message_map;

            m_updaters.clear();
            m_updaters.reserve(2 * m_factors.size());

            m_extracters.clear();
            m_extracters.reserve(m_factors.size());
            m_updaters.clear();
            m_message_headers.clear();

            /* FORWARD */

            factor_outputs.clear();
            for (const auto& fp: m_factors) {
                for (size_t v: fp->outputs()) {
                    assert(!factor_by_output_var[v]);
                    factor_by_output_var[v] = fp;
                }
            }

            for (const auto& fp: m_factors) {
                m_updaters.emplace_back(fp);
                auto& fupd = m_updaters.back();
                std::map<bn_factor_ptr, std::vector<size_t>> input_interfaces;
                for (size_t v: fp->inputs()) {
                    auto src = factor_by_output_var[v];
                    input_interfaces[src].push_back(v);
                }
                for (const auto& fi: input_interfaces) {
                    msg_descr msg{fi.first, fi.second};
                    if (message_map.find(msg) == message_map.end()) {
                        factor_outputs[fi.first].push_back(fi.second);
                        size_t msg_index = message_map.size();
                        message_map[msg] = msg_index;
                    }
                    fupd.add_input(message_map[msg]);
                }
            }

            for (auto& fupd: m_updaters) {
                for (const auto& vars: factor_outputs[fupd.computer()]) {
                    msg_descr msg{fupd.computer(), vars};
                    fupd.add_output(message_map[msg], vars);
                }
            }

            /* BACKWARD */

            std::vector<std::vector<bn_factor_ptr>> factors_by_output_var(m_variable_domains.rbegin()->first + 1);

            size_t upd_skip = m_updaters.size();

            factor_outputs.clear();
            for (const auto& fp: reverse(m_factors)) {
                for (size_t v: fp->inputs()) {
                    factors_by_output_var[v].push_back(fp);
                }
            }

            for (const auto& fp: reverse(m_factors)) {
                m_updaters.emplace_back(fp);
                auto& fupd = m_updaters.back();
                std::map<bn_factor_ptr, std::vector<size_t>> input_interfaces;
                for (size_t v: fp->outputs()) {
                    for (const auto& src: factors_by_output_var[v]) {
                        input_interfaces[src].push_back(v);
                    }
                }
                for (const auto& fi: input_interfaces) {
                    msg_descr msg{fi.first, fi.second};
                    if (message_map.find(msg) == message_map.end()) {
                        factor_outputs[fi.first].push_back(fi.second);
                        size_t msg_index = message_map.size();
                        message_map[msg] = msg_index;
                    }
                    fupd.add_input(message_map[msg]);
                }
            }

            for (auto& fupd: skip(m_updaters, upd_skip)) {
                for (const auto& vars: factor_outputs[fupd.computer()]) {
                    msg_descr msg{fupd.computer(), vars};
                    fupd.add_output(message_map[msg], vars);
                }
            }

            MSG_DEBUG("message_map.size() = " << message_map.size());
            for (const auto& kv: message_map) {
                MSG_DEBUG(" * " << kv.first << " : " << kv.second);
            }

            m_message_headers.resize(message_map.size(), "");
            MSG_DEBUG("m_message_headers.size() = " << m_message_headers.size());
            for (const auto& descr_i: message_map) {
                MSG_DEBUG("setting descr " << descr_i.second << " with " << descr_i.first);
                MSG_QUEUE_FLUSH();
                m_message_headers[descr_i.second] = SPELL_STRING("" << descr_i.first);
            }

            for (const auto& upd: m_updaters) {
                std::stringstream ss;
                ss << "UPDATER";
                for (size_t i: upd.input_messages()) {
                    ss << ' ' << m_message_headers[i];
                }
                ss << " ## " << upd.computer()->variables() << " ##";
                for (const auto& kv: upd.output_messages()) {
                    ss << ' ' << m_message_headers[kv.second];
                }
                MSG_DEBUG(ss.str());
            }
            MSG_QUEUE_FLUSH();

            /* EXTRACTORS */
            auto fwupd = m_updaters.begin();
            auto bwupd = m_updaters.rbegin();
            for (const auto& fp: m_factors) {
                assert(fwupd->computer() == fp && bwupd->computer() == fp);
                m_extracters.emplace_back(fp);
                for (size_t in: fwupd->input_messages()) {
                    m_extracters.back().add_input(in);
                }
                for (size_t in: bwupd->input_messages()) {
                    m_extracters.back().add_input(in);
                }
                for (size_t v: fp->outputs()) {
                    m_extracters.back().add_output(v, {v});
                }
                ++fwupd;
                ++bwupd;
            }

            for (const auto& upd: m_extracters) {
                std::stringstream ss;
                ss << "EXTRACTER";
                for (size_t i: upd.input_messages()) {
                    ss << ' ' << m_message_headers[i];
                }
                ss << " ## " << upd.computer()->variables() << " ##";
                for (const auto& kv: upd.output_messages()) {
                    ss << ' ' << kv.second;
                }
                MSG_DEBUG(ss.str());
            }
            MSG_QUEUE_FLUSH();
        }

    instance_type
        instance() const
        {
            return {this};
        }

    template <typename V>
        void file_io_dic(ofile& ofs, const char* _4cc, std::vector<std::shared_ptr<V>>& data, std::map<ptrdiff_t, std::shared_ptr<V>>&)
        {
            rw_base rw;
            if (rw.fourcc(ofs, _4cc)) { return; }
            rw(ofs, data.size());
            for (const auto& p: data) {
                rw(ofs, (ptrdiff_t) p.get());
                p->file_io_common(ofs);
            }
        }

    template <typename V>
        void file_io_dic(ifile& ifs, const char* _4cc, std::vector<std::shared_ptr<V>>& data, std::map<ptrdiff_t, std::shared_ptr<V>>& dic)
        {
            rw_base rw;
            if (rw.fourcc(ifs, _4cc)) { return; }
            size_t sz;
            rw(ifs, sz);
            for (size_t i = 0; i < sz; ++i) {
                ptrdiff_t key;
                rw(ifs, key);
                 data.emplace_back(std::make_shared<V>());
                 dic[key] = data.back();
                data.back()->file_io_common(ifs);
            }
        }

    template <typename STREAM_TYPE>
        void file_io_factor_dic(STREAM_TYPE& fs, factor_dic_type& dic)
        {
            file_io_dic(fs, "FDIC", m_factors, dic);
        }

    template <typename STREAM_TYPE>
        void
        file_io(STREAM_TYPE& fs)
        {
            factor_dic_type factor_dic;
            rw_comb<size_t, bn_label_type> rw;
            if (rw.fourcc(fs, "FG__")) { return; }
            rw(fs, m_variable_domains);
            rw(fs, m_n_alleles);
            /* don't save observations, buffer index, or any message actually. */
            file_io_factor_dic(fs, factor_dic);
            /*file_io_interface_dic(fs, interface_dic);*/
            /* now both dictionaries are filled, we can read the interdependent data. */
            /*for (const auto& fp: m_factors) {*/
                /*fp->file_io_interfaces(fs, interface_dic);*/
            /*}*/
            /*for (const auto& ip: m_interfaces) {*/
                /*ip->file_io_factors(fs, factor_dic);*/
            /*}*/
            rw(fs, m_message_headers);
            auto mupd_rw = [&](STREAM_TYPE& fs, bn_message_updater_type& mupd) { mupd.file_io(fs, factor_dic); };
            rw(fs, m_updaters, mupd_rw);
            rw(fs, m_extracters, mupd_rw);
            /*MSG_DEBUG("file i/o m_noise before=" << m_noise);*/
            rw(fs, m_noise);
            /*MSG_DEBUG("file i/o m_noise after=" << m_noise);*/
            rw(fs, m_updater_order);
        }
    // AVOID A BUG on Windows: file_io does binary I/O operations => open in binary mode (not text mode)
    void save(const std::string& filename) { ofile ofs(filename, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary); file_io(ofs); }
    void load(const std::string& filename) { ifile ifs(filename, std::ios_base::in | std::ios_base::binary); file_io(ifs); }

    private:
        std::map<size_t, std::vector<bn_label_type>> m_variable_domains;
        std::vector<bn_factor> m_factors;
        /*size_t m_buffer_index;*/
        size_t m_n_alleles;
        std::vector<bn_message_updater_type> m_updaters;
        std::vector<bn_message_updater_type> m_extracters;
        std::vector<std::string> m_message_headers;
        double m_noise;
        std::vector<size_t> m_updater_order;
};




#endif

