/*
 * Copyright (c) 2016 Damien Leroux, Sylvain Jasson, INRA
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _SPEL_BAYES_COMPUTABLE_H_
#define _SPEL_BAYES_COMPUTABLE_H_

#include "factor.h"

struct generic_computer {
    virtual void apply_observations(const bn_message_type& observations);
    virtual void compute_state(bn_neighbour_iterator_type begin, bn_neighbour_iterator_type end,
                               const bn_message_type& observations,
                               bn_output_iterator_type out_begin, bn_output_iterator_type out_end) = 0;
};


struct factor_computer : public generic_computer {
    
};


#endif

