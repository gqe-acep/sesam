/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_BAYES_FACTOR_VAR_H_
#define _SPEL_BAYES_FACTOR_VAR_H_


/*#include "graph2.h"*/
#include "chrono.h"


#define FACTOR_CROSS_SPARSE


enum PropagationMode { PM_Sync, PM_Async };

struct double_buffer_type {
    std::vector<VectorXd> m_data[2];
    std::vector<VectorXd>* m_front;
    std::vector<VectorXd>* m_back;
    size_t m_current;
    PropagationMode m_pm;

    double_buffer_type(size_t res=0, PropagationMode pm=PM_Sync)
        : m_data()
        , m_current(0)
        , m_pm(pm)
    {
        m_data[0].reserve(res);
        m_data[1].reserve(res);
        flip();
    }

    double_buffer_type(const double_buffer_type& other)
        : m_data()
        , m_current(other.m_current)
        , m_pm(other.m_pm)
    {
        m_data[0] = other.m_data[0];
        m_data[1] = other.m_data[1];
    }

    size_t add(size_t size)
    {
        size_t ret = m_data[0].size();
        m_data[0].emplace_back(VectorXd::Ones(size));
        m_data[1].emplace_back(VectorXd::Ones(size));
        return ret;
    }

    void sync() { if (m_pm != PM_Sync) { m_pm = PM_Sync; flip(); } }
    void async() { if (m_pm != PM_Async) { m_pm = PM_Async; flip(); } }

    VectorXd& front(size_t i) { return (*m_front)[i]; }
    const VectorXd& front(size_t i) const { return (*m_front)[i]; }
    VectorXd& back(size_t i) { return (*m_back)[i]; }
    const VectorXd& back(size_t i) const { return (*m_back)[i]; }
    void flip()
    {
        switch (m_pm) {
            case PM_Sync:
                m_back = &m_data[m_current];
                m_current = 1 - m_current;
                m_front = &m_data[m_current];
                break;
            case PM_Async:
                m_current = 1 - m_current;
                m_data[m_current] = m_data[1 - m_current];
                m_front = m_back = &m_data[m_current];
                break;
        };
    }
    void copy() { m_data[1 - m_current] = m_data[m_current]; }
    double delta_max() const
    {
        double ret = 0;
        for (size_t i = 0; i < m_data[0].size(); ++i) {
            double tmp = (m_data[0][i] - m_data[1][i]).lpNorm<Eigen::Infinity>();
            ret = tmp > ret ? tmp : ret;
        }
        return ret;
    }
    size_t size() const { return m_data[0].size(); }
};


struct neighbourhood {
    std::vector<size_t> m_all_neighbours;
    std::map<size_t, std::vector<size_t>> m_all_but;

    neighbourhood()
        : m_all_neighbours()
        , m_all_but()
    {}

    void add_neighbour(size_t n)
    {
        for (auto& kv: m_all_but) {
            kv.second.push_back(n);
        }
        m_all_but.insert({n, m_all_neighbours});
        m_all_neighbours.push_back(n);
    }

    const std::vector<size_t>& neighbours() const { return m_all_neighbours; }
    const std::vector<size_t>& all_neighbours_but(size_t n) const { return m_all_but.find(n)->second; }
};



enum MessageType { MT_F2V, MT_V2F };




struct var_type : public neighbourhood {
    size_t m_label;
    size_t m_domain;
    bool m_observed;

    var_type(size_t label, size_t domain, bool o=false)
        : m_label(label), m_domain(domain), m_observed(o)
    {}

    var_type(size_t label, size_t n_par, size_t n_al, bool o=false)
        : m_label(label)
        , m_domain(n_par == 0
                   ? n_al * n_al
                   : n_al == 0
                     ? n_par * n_par
                     : n_par * n_par * n_al * n_al)
        , m_observed(o)
    {}

    size_t label() const { return m_label; }
    size_t domain() const { return m_domain; }
    bool observed() const { return m_observed; }
};


enum FactorType {
    FObs=0,
    FCross,
    FSelf,
    FAncestor,
    FDH,
    _FactorType_Count
};


struct factor_type {
    size_t m_label;
    FactorType m_type;
    size_t m_var1, m_var2, m_var3;
    factor_type(size_t l, FactorType t, size_t v1, size_t v2, size_t v3)
        : m_label(l), m_type(t), m_var1(v1), m_var2(v2), m_var3(v3)
    {}
    size_t label() const { return m_label; }
    FactorType type() const { return m_type; }
    size_t var1() const { return m_var1; }
    size_t var2() const { return m_var2; }
    size_t var3() const { return m_var3; }
};

static inline std::ostream& operator << (std::ostream& os, FactorType f)
{
    switch (f) {
        case FObs:      os << "Observation"; break;
        case FCross:    os << "Cross"; break;
        case FSelf:     os << "Self"; break;
        case FAncestor: os << "Ancestor"; break;
        case FDH:       os << "DH"; break;
        default:        os << "[custom]"; break;
    };
    return os;
}

extern size_t novar;

/*
namespace direction_data {
    extern std::vector<std::vector<size_t>>
        normalize;
    extern std::vector<std::vector<std::pair<size_t, size_t>>>
        product_mapping_binary,
        product_mapping_ternary;
    extern std::vector<std::vector<size_t>>
        sum_mapping_binary,
        sum_mapping_ternary;

}
*/


static inline void new_cross_prob2(size_t n_par, size_t n_al, MatrixXd& M1, MatrixXd& M2, MatrixXd& M3);
static inline void new_cross_prob2(size_t n_par, size_t n_al, SparseMatrix<double>& M1, SparseMatrix<double>& M2, SparseMatrix<double>& M3);
static inline void new_dh_prob2(size_t n_par, size_t n_al, MatrixXd& M1, MatrixXd& M2);
static inline void new_dh_prob2(size_t n_par, size_t n_al, SparseMatrix<double>& M1, SparseMatrix<double>& M2);
static inline void new_self_prob2(size_t n_par, size_t n_al, MatrixXd& M1, MatrixXd& M2);
static inline void new_self_prob2(size_t n_par, size_t n_al, SparseMatrix<double>& M1, SparseMatrix<double>& M2);
static inline void new_obs_prob2(size_t n_par, size_t n_al, double noise, MatrixXd& M1, MatrixXd& M2);
static inline void new_ancestor_prob2(size_t n_par, size_t n_al, MatrixXd& M1, MatrixXd& M2);


inline void normalize(VectorXd& v, bool to_one=false)
{
    double s = v.sum();
    if (s) {
        if (to_one) {
            v /= s;
        } else {
            v /= s / v.size();
        };
    }
}

struct var_message {
    size_t m_buf_idx;
    std::vector<size_t> m_incoming;
    /*VectorXd m_observation;*/
    bool m_evidence;
    size_t m_count;

    var_message(size_t bi, size_t n/*, size_t domain*/)
        : m_buf_idx(bi), m_incoming(), m_evidence(false), m_count(0)
    {
        m_incoming.reserve(n);
        /*m_observation.setOnes(domain);*/
    }

    bool is_evidence() const { return m_evidence; }
    void reset_count() { m_count = 0; }
    void incr_count() { ++m_count; }
    size_t count_target() const { return m_incoming.size(); }
    bool is_good() const { return m_count == count_target(); }

    void update(VectorXd& buffer, const VectorXd& observation, const double_buffer_type& factor_message_table) const
    {
        /*buffer.setOnes(buffer.size());*/
        buffer = observation;
        /*MSG_DEBUG("  buffer " << buffer.transpose());*/
        /*MSG_DEBUG("  observ " << observation.transpose());*/
        for (size_t inc: m_incoming) {
            /*MSG_DEBUG("  incoming " << factor_message_table.back(inc).transpose()); MSG_QUEUE_FLUSH();*/
            buffer.array() *= factor_message_table.back(inc).array();
        }
        /*MSG_DEBUG("update_V(" << m_incoming << ") " << buffer.transpose());*/
        /* optional normalization */
        normalize(buffer);
        /*MSG_DEBUG("  buffer = " << buffer.transpose()); MSG_QUEUE_FLUSH();*/
    }
};

struct matrix_wrapper {
    bool m_is_sparse;
    const MatrixXd* m_dense;
    const SparseMatrix<double>* m_sparse;

    matrix_wrapper(const MatrixXd* m) : m_is_sparse(false), m_dense(m), m_sparse(NULL) {}
    matrix_wrapper(const SparseMatrix<double>* m) : m_is_sparse(true), m_dense(NULL), m_sparse(m) {}

    template <typename V>
        VectorXd operator * (V&& v) const { if (m_is_sparse) { return (*m_sparse) * v; } else { return (*m_dense) * v; } }
};

struct factor_message {
    size_t m_buf_idx;
    FactorType m_factor;
    matrix_wrapper m_factor_matrix;
    //size_t m_var_index;  /* index of output variable */
    size_t m_msg1;       /* message index */
    size_t m_msg2;       /* message index */
    size_t m_count;

    template <typename DENSE_OR_SPARSE>
    factor_message(size_t bi, FactorType t, const DENSE_OR_SPARSE* mat, /*size_t vi,*/ size_t m1, size_t m2=novar)
        : m_buf_idx(bi), m_factor(t), /*m_var_index(vi),*/ m_factor_matrix(mat), m_msg1(m1), m_msg2(m2), m_count(0)
    {}

    void reset_count() { m_count = 0; }
    void incr_count() { ++m_count; }
    size_t count_target() const
    {
        switch (m_factor) {
            case FCross: return 2;
            default: return 1;
        };
    }
    bool is_good() const { return m_count == count_target(); }

    void update(VectorXd& buffer,
                /*const std::vector<std::vector<MatrixXd>>& factor_table,*/
                const double_buffer_type& variable_message_table) const
    {
        if (m_factor == FCross) {
            const VectorXd& m1 = variable_message_table.back(m_msg1);
            const VectorXd& m2 = variable_message_table.back(m_msg2);
            /*MSG_DEBUG("  factor m1 " << m1.transpose());*/
            /*MSG_DEBUG("  factor m2 " << m2.transpose());*/
            /*MSG_QUEUE_FLUSH();*/
            /*buffer = factor_table[m_factor][m_var_index] * kroneckerProduct(m2, m1);*/
            buffer = m_factor_matrix * kroneckerProduct(m2, m1);
        } else {
            const VectorXd& m1 = variable_message_table.back(m_msg1);
            /*buffer = factor_table[m_factor][m_var_index] * m1;*/
            buffer = m_factor_matrix * m1;
        }
        /* optional normalization */
        normalize(buffer);
        /*MSG_DEBUG("  buffer = " << buffer.transpose()); MSG_QUEUE_FLUSH();*/
    }
};


/*
static inline
bool
operator < (const std::pair<size_t, size_t>& p1,
            const std::pair<size_t, size_t>& p2)
{
    return p1.first < p2.first
        || (p1.first == p2.first && p1.second < p2.second);
}
*/

struct bayesian_network {
    /* pedigree parameters */
    size_t m_n_par, m_n_al;
    /* observational noise level */
    double m_noise;
    /* network setup */
    std::vector<var_type> m_variables;
    std::vector<std::string> m_variable_names;
    std::vector<factor_type> m_factors;
    /* message handling */
    std::vector<var_message> m_var_messages;
    std::vector<var_message> m_beliefs;
    std::vector<factor_message> m_factor_messages;
    /*double_buffer_type m_var_msg_buf;*/
    /*double_buffer_type m_factor_msg_buf;*/
    /* factored factors */
    std::vector<std::vector<MatrixXd>> m_factor_map;
#ifdef FACTOR_CROSS_SPARSE
    std::vector<SparseMatrix<double>> m_factor_cross;
    std::vector<SparseMatrix<double>> m_factor_self;
    std::vector<SparseMatrix<double>> m_factor_dh;
#else
    std::vector<MatrixXd> m_factor_cross;
    std::vector<MatrixXd> m_factor_self;
    std::vector<MatrixXd> m_factor_dh;
#endif

    /* evidence hack */
    std::map<size_t, size_t> m_evidence_table;
    /* ancestor hack */
    std::map<size_t, size_t> m_ancestor_table;

    /* lists of messages that a message may trigger in async mode */
    std::vector<std::pair<size_t, size_t>> m_corresp_F2V;
    std::vector<std::pair<size_t, size_t>> m_corresp_V2F;
    std::map<std::pair<size_t, size_t>, size_t> m_msg_buf_to_var;
    std::map<std::pair<size_t, size_t>, size_t> m_msg_buf_to_fac;
    /*boost::dynamic_bitset m_msg_fac_transmitted;*/
    /*boost::dynamic_bitset m_msg_var_transmitted;*/

    bayesian_network(size_t n_par, size_t n_al, double noise)
        : m_n_par(n_par), m_n_al(n_al), m_noise(noise)
        , m_variables(), m_factors()
        , m_var_messages(), m_beliefs()
        , m_factor_messages()
        /*, m_var_msg_buf(), m_factor_msg_buf()*/
        , m_factor_map()
#ifdef FACTOR_CROSS_SPARSE
        , m_factor_cross()
#endif
        , m_evidence_table()
    {
        /*MSG_DEBUG("Initializing factors...");*/
        chrono::start("init_factor_map");
        init_factor_map();
        chrono::stop("init_factor_map");
        chrono::display() = true;
        /*MSG_DEBUG("Done initializing factors.");*/

        /*MSG_DEBUG("Factor map size :");*/

        /*MSG_DEBUG("Obs " << m_factor_map[FObs].size());*/
        /*dump_factor(FObs, 0);*/
        /*dump_factor(FObs, 1);*/

#if 0
        MSG_DEBUG("Self " << m_factor_map[FSelf].size());
        dump_factor(FSelf, 0);
        dump_factor(FSelf, 1);

        MSG_DEBUG("DH " << m_factor_map[FDH].size());
        dump_factor(FDH, 0);
        dump_factor(FDH, 1);

        MSG_DEBUG("Cross " << m_factor_map[FCross].size());
        dump_factor(FCross, 0);
        dump_factor(FCross, 1);
        dump_factor(FCross, 2);

        MSG_DEBUG("Ancestor " << m_factor_map[FAncestor].size());
        dump_factor(FAncestor, 0);
        dump_factor(FAncestor, 1);
#endif
    }

    std::string& varname(size_t i) { return m_variable_names[i]; }
    std::vector<std::string>& varnames() { return m_variable_names; }

    void dump_factor(FactorType f, size_t i)
    {
        switch (f) {
            case FCross:
                MSG_DEBUG(m_factor_cross[i] << std::endl);
                break;
            case FSelf:
                MSG_DEBUG(m_factor_self[i] << std::endl);
                break;
            case FDH:
                MSG_DEBUG(m_factor_dh[i] << std::endl);
                break;
            default:;
                MSG_DEBUG(m_factor_map[f][i] << std::endl);
        };
    }

    size_t new_variable(size_t n_par, size_t n_al, bool observed=false)
    {
        m_variable_names.emplace_back(SPELL_STRING(m_variables.size()));
        size_t ret = m_variables.size();
        m_variables.emplace_back(ret, n_par, n_al, observed);
        return ret;
    }

    size_t new_factor(FactorType type, size_t var1, size_t var2, size_t var3=novar)
    {
        size_t ret = m_factors.size();
        m_factors.emplace_back(ret, type, var1, var2, var3);
        m_variables[var1].add_neighbour(ret);
        m_variables[var2].add_neighbour(ret);
        if (var3 != novar) {
            m_variables[var3].add_neighbour(ret);
        }
        return ret;
    }

    size_t ancestor()
    {
        size_t ret = new_variable(m_n_par, m_n_al);
        size_t ao = ancestor_obs(ret);
        size_t number = m_ancestor_table.size();
        m_ancestor_table[ao] = number;
        return ret;
    }

    size_t crossing(size_t p1, size_t p2)
    {
        size_t ret = new_variable(m_n_par, m_n_al);
        new_factor(FCross, p1, p2, ret);
        return ret;
    }

    size_t _new_binary(FactorType type, size_t p1)
    {
        size_t ret = new_variable(m_n_par, m_n_al);
        new_factor(type, p1, ret);
        return ret;
    }

    size_t selfing(size_t p1) { return _new_binary(FSelf, p1); }
    size_t dhing(size_t p1) { return _new_binary(FDH, p1); }

    size_t ancestor_obs(size_t v)
    {
        size_t ret = new_variable(m_n_par, 0, true);
        new_factor(FAncestor, v, ret);
        return ret;
    }

    size_t allele_obs(size_t v)
    {
        size_t ret = new_variable(0, m_n_al, true);
        new_factor(FObs, v, ret);
        return ret;
    }

    void init_messages()
    {
        m_msg_buf_to_var.clear();
        m_msg_buf_to_fac.clear();
        m_factor_messages.clear();
        m_var_messages.clear();
        m_beliefs.clear();
        m_evidence_table.clear();

        /* reserve vector sizes */
        size_t n_msg = 0;
        for (const auto& f: m_factors) {
            switch (f.type()) {
                case FCross: n_msg += 3; break;
                case FSelf:
                case FDH: n_msg += 2; break;
                default: ++n_msg;
            };
        }
        m_factor_messages.reserve(n_msg);
        m_corresp_F2V.resize(n_msg);
        n_msg = 0;
        for (const auto& v: m_variables) {
            n_msg += v.neighbours().size();
        }
        m_var_messages.reserve(n_msg);
        m_beliefs.reserve(m_variables.size());
        m_corresp_V2F.resize(n_msg);
        /* assign buffer numbers for ALL messages */
        size_t vbuf_idx = 0;
        for (const auto& f: m_factors) {
            m_msg_buf_to_var[{f.label(), f.var1()}] = vbuf_idx++; //m_factor_msg_buf.add(m_variables[f.var1()].domain());
            if (f.type() != FObs && f.type() != FAncestor) {
                m_msg_buf_to_var[{f.label(), f.var2()}] = vbuf_idx++; //m_factor_msg_buf.add(m_variables[f.var2()].domain());
                if (f.var3() != novar) {
                    m_msg_buf_to_var[{f.label(), f.var3()}] = vbuf_idx++; //m_factor_msg_buf.add(m_variables[f.var2()].domain());
                }
            }
            /*if (f.type() == FAncestor || f.type() == FObs) {*/
                /*m_evidence_table[f.var2()] = m_msg_buf_to_var[{f.label(), f.var1()}];*/
            /*}*/
        }
        size_t fbuf_idx = 0;
        for (const auto& v: m_variables) {
            for (size_t fi: v.neighbours()) {
                m_msg_buf_to_fac[{v.label(), fi}] = fbuf_idx++; //m_var_msg_buf.add(v.domain());
            }
        }
        /* set up reverse lookup for msg_buf_index->edge */
        for (const auto& kv: m_msg_buf_to_fac) {
            m_corresp_V2F[kv.second] = kv.first;
        }
        for (const auto& kv: m_msg_buf_to_var) {
            m_corresp_F2V[kv.second] = kv.first;
        }
        /* create messages */
        for (const auto& f: m_factors) {
            switch(f.type()) {
                case FCross:
                    m_factor_messages.emplace_back(
                            m_factor_messages.size(),
                            f.type(), &m_factor_cross[0],
                            m_msg_buf_to_fac[{f.var2(), f.label()}],
                            m_msg_buf_to_fac[{f.var3(), f.label()}]);
                    m_factor_messages.emplace_back(
                            m_factor_messages.size(),
                            f.type(), &m_factor_cross[1],
                            m_msg_buf_to_fac[{f.var1(), f.label()}],
                            m_msg_buf_to_fac[{f.var3(), f.label()}]);
                    m_factor_messages.emplace_back(
                            m_factor_messages.size(),
                            f.type(), &m_factor_cross[2],
                            m_msg_buf_to_fac[{f.var1(), f.label()}],
                            m_msg_buf_to_fac[{f.var2(), f.label()}]);
                    break;
                case FSelf:
                    m_factor_messages.emplace_back(
                            m_factor_messages.size(),
                            f.type(), &m_factor_self[0],
                            m_msg_buf_to_fac[{f.var2(), f.label()}]);
                    m_factor_messages.emplace_back(
                            m_factor_messages.size(),
                            f.type(), &m_factor_self[1],
                            m_msg_buf_to_fac[{f.var1(), f.label()}]);
                    break;
                case FDH:
                    m_factor_messages.emplace_back(
                            m_factor_messages.size(),
                            f.type(), &m_factor_dh[0],
                            m_msg_buf_to_fac[{f.var2(), f.label()}]);
                    m_factor_messages.emplace_back(
                            m_factor_messages.size(),
                            f.type(), &m_factor_dh[1],
                            m_msg_buf_to_fac[{f.var1(), f.label()}]);
                    break;
                case FObs:
                case FAncestor:
                    m_factor_messages.emplace_back(
                            m_factor_messages.size(),
                            f.type(), &m_factor_map[f.type()][0],
                            m_msg_buf_to_fac[{f.var2(), f.label()}]);
                    break;
                default:;
            };
        }
        for (const auto& v: m_variables) {
            size_t n = v.neighbours().size() - 1;
            for (size_t fi: v.neighbours()) {
                m_var_messages.emplace_back(m_var_messages.size(), n);
                for (size_t x: v.all_neighbours_but(fi)) {
                    m_var_messages.back().m_incoming.push_back(
                            m_msg_buf_to_var[{x, v.label()}]);
                }
                if (v.observed()) {
                    /*MSG_DEBUG("EVIDENCE " << v.label() << " " << m_var_messages.size());*/
                    m_evidence_table[v.label()] = m_var_messages.size() - 1;
                    m_var_messages[m_evidence_table[v.label()]].m_evidence = true;
                }
            }
            m_beliefs.emplace_back(m_beliefs.size(), n + 1);
            for (size_t fi: v.neighbours()) {
                m_beliefs.back().m_incoming.push_back(m_msg_buf_to_var[{fi, v.label()}]);
            }
        }
        /* initialize ancestor pseudo-observations */
        /*for (const auto& a_n: m_ancestor_table) {*/
            /*evidence(a_n.first) = VectorXd::Zero(m_n_par * m_n_par);*/
            /*evidence(a_n.first)(a_n.second * (m_n_par + 1)) = 1;*/
        /*}*/
    }

    std::string debug_var(size_t v) const
    {
        return m_variable_names[v];
    }

    std::string debug_fac(size_t f) const
    {
        const auto& F = m_factors[f];
        std::string msg1 = SPELL_STRING(F.type() << '[' << debug_var(F.var1()) << ", " << debug_var(F.var2()));
        std::string msg2;
        if (F.var3() != novar) {
            msg2 = SPELL_STRING(", " << debug_var(F.var3()));
        }
        return SPELL_STRING(msg1 << msg2 << ']');
    }

    std::string debug_message(MessageType t, size_t i, bool full=false, const double_buffer_type& msg_buf_table={}) const
    {
        size_t f, v;
        switch (t) {
            case MT_F2V:
                std::tie(f, v) = m_corresp_F2V[i];
                return SPELL_STRING(debug_fac(f) << " --> " << debug_var(v));
            case MT_V2F:
                std::tie(v, f) = m_corresp_V2F[i];
                if (full) {
                    std::vector<std::string> inc;
                    for (const auto& b: m_var_messages[i].m_incoming) {
                        inc.emplace_back(SPELL_STRING(std::endl << msg_buf_table.back(b).transpose()));
                    }
                    return SPELL_STRING(debug_var(v) << " --> " << debug_fac(f) << inc);
                } else {
                    return SPELL_STRING(debug_var(v) << " --> " << debug_fac(f));
                }
        };
        return "gcc is dumb";
    }

    void init_factor_map()
    {
        m_factor_map.resize(_FactorType_Count);
        /*chrono::start("init_factor Obs");*/
        {
            m_factor_map[FObs].resize(2);
            new_obs_prob2(m_n_par, m_n_al, m_noise, m_factor_map[FObs][0], m_factor_map[FObs][1]);
        }
        /*chrono::stop("init_factor Obs");*/
        /*chrono::start("init_factor Self");*/
        {
            /*m_factor_map[FSelf].resize(2);*/
            /*new_self_prob2(m_n_par, m_n_al, m_factor_map[FSelf][0], m_factor_map[FSelf][1]);*/
            m_factor_self.resize(2);
            new_self_prob2(m_n_par, m_n_al, m_factor_self[0], m_factor_self[1]);
        }
        /*chrono::stop("init_factor Self");*/
        /*chrono::start("init_factor DH");*/
        {
            /*m_factor_map[FDH].resize(2);*/
            /*new_dh_prob2(m_n_par, m_n_al, m_factor_map[FDH][0], m_factor_map[FDH][1]);*/
            m_factor_dh.resize(2);
            new_self_prob2(m_n_par, m_n_al, m_factor_dh[0], m_factor_dh[1]);
        }
        /*chrono::stop("init_factor DH");*/
        /*chrono::start("init_factor Cross");*/
        {
#ifdef FACTOR_CROSS_SPARSE
            m_factor_cross.resize(3);
            new_cross_prob2(m_n_par, m_n_al, m_factor_cross[0], m_factor_cross[1], m_factor_cross[2]);
#else
            m_factor_map[FCross].resize(3);
            new_cross_prob2(m_n_par, m_n_al, m_factor_map[FCross][0], m_factor_map[FCross][1], m_factor_map[FCross][2]);
#endif
        }
        /*chrono::stop("init_factor Cross");*/
        /*chrono::start("init_factor Ancestor");*/
        {
            m_factor_map[FAncestor].resize(2);
            new_ancestor_prob2(m_n_par, m_n_al, m_factor_map[FAncestor][0], m_factor_map[FAncestor][1]);
        }
        /*chrono::stop("init_factor Ancestor");*/
    }


    struct instance_type {
        const bayesian_network* m_bn;
        double_buffer_type m_var_msg_buf;
        double_buffer_type m_factor_msg_buf;
        std::vector<VectorXd> m_observation;
        std::vector<size_t> m_var_message_counters;
        std::vector<size_t> m_factor_message_counters;
        PropagationMode m_mode;

        bool m_exact;
        std::set<std::pair<MessageType, size_t>> m_inexact_messages;
        std::vector<size_t> m_inexact_var_message_counters;
        std::vector<size_t> m_inexact_factor_message_counters;

        instance_type(const bayesian_network* bn, PropagationMode mode=PM_Async)
            : m_bn(bn), m_var_msg_buf(bn->m_msg_buf_to_fac.size()), m_factor_msg_buf(bn->m_msg_buf_to_var.size())
            , m_var_message_counters(), m_factor_message_counters()
            , m_mode(mode)
        {
            m_var_message_counters.resize(bn->m_msg_buf_to_fac.size());
            m_observation.reserve(bn->m_msg_buf_to_fac.size());
            for (size_t i = 0; i < bn->m_msg_buf_to_fac.size(); ++i) {
                size_t dom = bn->m_variables[bn->m_corresp_V2F[i].first].domain();
                /*MSG_DEBUG("VAR MESSAGE #" << i << " " << bn->debug_message(MT_V2F, i) << " domain " << dom);*/
                /*MSG_QUEUE_FLUSH();*/
                m_observation.emplace_back(VectorXd::Ones(dom));
                m_var_msg_buf.add(dom);
            }

            m_factor_message_counters.resize(bn->m_factor_messages.size());
            for (size_t i = 0; i < bn->m_factor_messages.size(); ++i) {
                /*MSG_DEBUG(i << " vs " << bn->m_corresp_F2V.size());*/
                /*MSG_QUEUE_FLUSH();*/
                size_t dom = bn->m_variables[bn->m_corresp_F2V[i].second].domain();
                /*MSG_DEBUG("FACTOR MESSAGE #" << i << " " << bn->debug_message(MT_V2F, i) << " domain " << dom);*/
                /*MSG_QUEUE_FLUSH();*/
                m_factor_msg_buf.add(dom);
            }

            switch (mode) {
                case PM_Sync: sync(); break;
                case PM_Async: async(); break;
            };

            /* initialize ancestor pseudo-observations */
            for (const auto& a_n: m_bn->m_ancestor_table) {
                evidence(a_n.first) = VectorXd::Zero(m_bn->m_n_par * m_bn->m_n_par);
                evidence(a_n.first)(a_n.second * (m_bn->m_n_par + 1)) = 1;
            }
        }

        void dump_beliefs()
        {
            MSG_DEBUG("BELIEFS");
            for (size_t i = 0; i < m_bn->m_variables.size(); ++i) {
                MSG_DEBUG(m_bn->debug_var(i) << " [" << i << "] = " << belief(i).transpose());
            }
        }

        bool run(double threshold, size_t verbose=0)
        {
            if (verbose >= 1) { MSG_DEBUG("Running " << (m_mode == PM_Async ? "a" : "") << "synchronous convergence"); }
            if (verbose >= 2) {
                if (verbose >= 3) {
                    MSG_DEBUG("MESSAGES");
                    dump_messages();
                    dump_beliefs();
                }
            }
            m_exact = true;
            m_inexact_messages.clear();
            m_inexact_var_message_counters.clear();
            m_inexact_var_message_counters.resize(m_var_message_counters.size(), 0);
            m_inexact_factor_message_counters.clear();
            m_inexact_factor_message_counters.resize(m_factor_message_counters.size(), 0);
            bool exact = update_messages(verbose);
            size_t n_iter = 1;
            if (verbose >= 2) {
                if (verbose >= 3) {
                    MSG_DEBUG("MESSAGES");
                    dump_messages();
                    dump_beliefs();
                }
            }
            if (!exact) {
                do {
                    if (verbose >= 2) {
                        MSG_DEBUG("ITER " << n_iter << " delta=" << delta_max());
                    }
                    ++n_iter;
                    update_messages(verbose);
                    if (verbose >= 2) {
                        if (verbose >= 3) {
                            MSG_DEBUG("MESSAGES");
                            dump_messages();
                        }
                        dump_beliefs();
                    }
                } while (delta_max() > threshold);
            }
            if (verbose >= 1) {
                MSG_DEBUG("Converged after " << n_iter << " iterations (delta=" << delta_max() << ')');
            }
            return exact;
        }

        bool update_messages_async(size_t verbose=0)
        {
            bool exact = true;
            std::deque<std::pair<MessageType, size_t>> transmittable_messages;
            std::set<std::pair<MessageType, size_t>> pending_messages;
            /*transmittable_messages.reserve(m_var_messages.size() + m_factor_messages.size());*/
            /*pending_messages.reserve(m_var_messages.size() + m_factor_messages.size());*/

            m_var_msg_buf.flip();
            m_factor_msg_buf.flip();

            if (m_exact) {
                for (size_t i = 0; i < m_bn->m_var_messages.size(); ++i) {
                    if (m_bn->m_var_messages[i].is_evidence()
                            || m_bn->m_variables[m_bn->m_corresp_V2F[i].first].neighbours().size() == 1) {
                        transmittable_messages.emplace_back(MT_V2F, i);
                    } else {
                        pending_messages.insert({MT_V2F, i});
                    }
                    m_var_message_counters[i] = 0;
                }
                for (size_t i = 0; i < m_bn->m_factor_messages.size(); ++i) {
                    pending_messages.insert({MT_F2V, i});
                    m_factor_message_counters[i] = 0;
                }
            } else {
                pending_messages = m_inexact_messages;
                m_var_message_counters = m_inexact_var_message_counters;
                m_factor_message_counters = m_inexact_factor_message_counters;
                transmittable_messages.clear();
            }

            if (verbose >= 2) {
                MSG_DEBUG("NEW ITERATION " << pending_messages.size() << "P " << transmittable_messages.size() << "T");
            }

            MessageType type;
            size_t msg_i;
            size_t factor, var;

            while (pending_messages.size()) {
                while (transmittable_messages.size()) {
                    std::tie(type, msg_i) = transmittable_messages.front();
                    transmittable_messages.pop_front();
                    /*MSG_DEBUG("UPDATING " << m_bn->debug_message(type, msg_i));*/
                    /*MSG_QUEUE_FLUSH();*/
                    if (_async_update_message(type, msg_i)) {
                        /*MSG_DEBUG("ZERO " << m_bn->debug_message(type, msg_i, true, m_factor_msg_buf));*/
                    }
                    /*_async_message_transmitted(type, msg_i);*/
                    switch (type) {
                        case MT_F2V:
                            std::tie(factor, var) = m_bn->m_corresp_F2V[msg_i];
                            /*MSG_DEBUG("F" << factor << "->V" << var);*/
                            {
                                const std::vector<size_t>& neighbours = m_bn->m_variables[var].all_neighbours_but(factor);
                                for (size_t n : neighbours) {
                                    size_t new_msg = m_bn->m_msg_buf_to_fac.find({var, n})->second;
                                    /*m_var_messages[new_msg].incr_count();*/
                                    ++m_var_message_counters[new_msg];
                                    /*MSG_DEBUG("* incr(" << debug_message(MT_V2F, new_msg) << ") " << m_var_messages[new_msg].m_count << " / " << m_var_messages[new_msg].count_target());*/
                                    if (m_var_message_counters[new_msg] == m_bn->m_var_messages[new_msg].count_target()) {
                                        transmittable_messages.emplace_back(MT_V2F, new_msg);
                                        pending_messages.erase(transmittable_messages.back());
                                        /*MSG_DEBUG("FIRE V2F " << new_msg);*/
                                        /*} else {*/
                                        /*MSG_DEBUG("don't fire V2F " << new_msg);*/
                                    }
                                }
                            }
                            break;
                        case MT_V2F:
                            std::tie(var, factor) = m_bn->m_corresp_V2F[msg_i];
                            /*MSG_DEBUG("V" << var << "->F" << factor);*/
                            size_t var1 = m_bn->m_factors[factor].m_var1;
                            size_t var2 = m_bn->m_factors[factor].m_var2;
                            size_t var3 = m_bn->m_factors[factor].m_var3;
                            if (var1 != var) {
                                size_t new_msg = m_bn->m_msg_buf_to_var.find({factor, var1})->second;
                                /*auto& msg = m_bn->m_factor_messages[new_msg];*/
                                /*msg.incr_count();*/
                                /*MSG_DEBUG("* incr(" << debug_message(MT_F2V, new_msg) << ") " << m_factor_messages[new_msg].m_count << " / " << m_factor_messages[new_msg].count_target());*/
                                ++m_factor_message_counters[new_msg];
                                if (m_factor_message_counters[new_msg] == m_bn->m_factor_messages[new_msg].count_target()) {
                                    transmittable_messages.emplace_back(MT_F2V, new_msg);
                                    pending_messages.erase(transmittable_messages.back());
                                    /*MSG_DEBUG("FIRE F2V " << new_msg);*/
                                    /*} else {*/
                                    /*MSG_DEBUG("don't fire F2V " << new_msg);*/
                                }
                            }
                            if (var2 != var) {
                                size_t new_msg = m_bn->m_msg_buf_to_var.find({factor, var2})->second;
                                /*auto& msg = m_bn->m_factor_messages[new_msg];*/
                                /*msg.incr_count();*/
                                ++m_factor_message_counters[new_msg];
                                if (m_factor_message_counters[new_msg] == m_bn->m_factor_messages[new_msg].count_target()) {
                                    transmittable_messages.emplace_back(MT_F2V, new_msg);
                                    pending_messages.erase(transmittable_messages.back());
                                    /*MSG_DEBUG("FIRE F2V " << new_msg);*/
                                    /*} else {*/
                                    /*MSG_DEBUG("don't fire F2V " << new_msg);*/
                            }
                            }
                            if (var3 != var && var3 != novar) {
                                size_t new_msg = m_bn->m_msg_buf_to_var.find({factor, var3})->second;
                                /*auto& msg = m_factor_messages[new_msg];*/
                                /*msg.incr_count();*/
                                ++m_factor_message_counters[new_msg];
                                if (m_factor_message_counters[new_msg] == m_bn->m_factor_messages[new_msg].count_target()) {
                                    transmittable_messages.emplace_back(MT_F2V, new_msg);
                                    pending_messages.erase(transmittable_messages.back());
                                    /*MSG_DEBUG("FIRE F2V " << new_msg);*/
                                    /*} else {*/
                                    /*MSG_DEBUG("don't fire F2V " << new_msg);*/
                            }
                            }
                            break;
                    };
                }

                if (pending_messages.size()) {
                    if (m_exact) {
                        if (verbose >= 2) {
                            MSG_DEBUG("GOT " << pending_messages.size() << " PENDING SPELL_STRING(S)!");
                        }
                        m_exact = false;
                        m_inexact_messages = pending_messages;
                        m_inexact_var_message_counters = m_var_message_counters;
                        m_inexact_factor_message_counters = m_factor_message_counters;
                    }
                    exact = false;
                    auto b = pending_messages.begin();
                    transmittable_messages.push_back(*b);
                    pending_messages.erase(b);
                }
            }
            return exact;
        }

        bool update_messages_sync(size_t verbose=0)
        {
            for (const auto& m: m_bn->m_factor_messages) {
                /*m.update(m_factor_msg_buf.front(m.m_buf_idx), m_bn->m_factor_map, m_var_msg_buf);*/
                m.update(m_factor_msg_buf.front(m.m_buf_idx), m_var_msg_buf);
            }
            for (const auto& m: m_bn->m_var_messages) {
                m.update(m_var_msg_buf.front(m.m_buf_idx), m_observation[m.m_buf_idx], m_factor_msg_buf);
            }
            m_var_msg_buf.flip();
            m_factor_msg_buf.flip();
            return false;
            (void) verbose;
        }

        bool update_messages(size_t verbose=0)
        {
            switch (m_mode) {
                case PM_Sync:
                    return update_messages_sync(verbose);
                case PM_Async:
                    return update_messages_async(verbose);
                default:
                    return false;
            };
        }

        double delta_max()
        {
            double mf = m_factor_msg_buf.delta_max();
            double mv = m_var_msg_buf.delta_max();
            return mv > mf ? mv : mf;
        }

        VectorXd belief(size_t v)
        {
            if (m_bn->m_variables[v].observed()) {
                VectorXd ret = evidence(v);
                normalize(ret, true);
                return ret;
            } else {
                VectorXd ret(m_bn->m_variables[v].domain());
                VectorXd ones = VectorXd::Ones(m_bn->m_variables[v].domain());
                m_bn->m_beliefs[v].update(ret, ones, m_factor_msg_buf);
                normalize(ret, true);
                return ret;
            }
        }

        VectorXd parental_origin_belief(size_t v)
        {
            return m_bn->m_factor_map[FAncestor][1] * belief(v);
        }

        VectorXd& evidence(size_t v)
        {
            /*MSG_DEBUG("evidence for variable #" << v << " in message #" << m_evidence_table[v]);*/

            /*return m_var_messages[m_evidence_table[v]].m_observation;*/
            auto it = m_bn->m_evidence_table.find(v);
            if (it == m_bn->m_evidence_table.end()) {
                throw std::runtime_error(SPELL_STRING("Variable " << m_bn->debug_var(v) << " is not observed"));
            }
            return m_observation[it->second];
        }

        void sync()
        {
            m_var_msg_buf.sync();
            m_factor_msg_buf.sync();
            m_mode = PM_Sync;
        }

        void async()
        {
            m_var_msg_buf.async();
            m_factor_msg_buf.async();
            m_mode = PM_Async;
        }

        bool _async_update_message(MessageType t, size_t i)
        {
            size_t buf;
            switch (t) {
                case MT_V2F:
                    buf = m_bn->m_var_messages[i].m_buf_idx;
                    m_bn->m_var_messages[i].update(m_var_msg_buf.front(buf), m_observation[buf], m_factor_msg_buf);
                    return m_var_msg_buf.front(buf).sum() == 0;
                    break;
                case MT_F2V:
                    buf = m_bn->m_factor_messages[i].m_buf_idx;
                    /*m_bn->m_factor_messages[i].update(m_factor_msg_buf.front(buf), m_bn->m_factor_map, m_var_msg_buf);*/
                    m_bn->m_factor_messages[i].update(m_factor_msg_buf.front(buf), m_var_msg_buf);
                    return m_factor_msg_buf.front(buf).sum() == 0;
            };
            return false;
        }

        void dump_messages() const
        {
            for (size_t i = 0; i < m_factor_msg_buf.m_data[0].size(); ++i) {
                const VectorXd& v1 = m_factor_msg_buf.m_data[0][i];
                const VectorXd& v2 = m_factor_msg_buf.m_data[1][i];
                MSG_DEBUG(std::setw(30) << m_bn->debug_message(MT_F2V, i) << " " << std::setw(8) << v1.transpose() << " | " << std::setw(8) << v2.transpose());
            }
            for (size_t i = 0; i < m_var_msg_buf.m_data[0].size(); ++i) {
                const VectorXd& v1 = m_var_msg_buf.m_data[0][i];
                const VectorXd& v2 = m_var_msg_buf.m_data[1][i];
                MSG_DEBUG(std::setw(30) << m_bn->debug_message(MT_V2F, i) << " " << std::setw(8) << v1.transpose() << " | " << std::setw(8) << v2.transpose());
            }
        }
    };

    instance_type instance() const { return {this}; }
};






#define make_idx(_p1, _p2, _a1, _a2) ((_p1) + n_par * ((_p2) + n_par * ((_a1) + n_al * (_a2))))
#define make_idx_a(_a1, _a2) ((_a1) + n_al * (_a2))
#define make_idx_p(_p1, _p2) ((_p1) + n_par * (_p2))


    static inline
void new_ancestor_prob2(size_t n_par, size_t n_al, MatrixXd& M1, MatrixXd& M2)
{
    size_t dom1 = n_par * n_par * n_al * n_al;
    size_t dom2 = n_par * n_par;
    M1 = MatrixXd::Zero(dom1, dom2);
    M2 = MatrixXd::Zero(dom2, dom1);
    for (size_t a2 = 0; a2 < n_al; ++a2) {
        for (size_t a1 = 0; a1 < n_al; ++a1) {
            for (size_t p2 = 0; p2 < n_par; ++p2) {
                for (size_t p1 = 0; p1 < n_par; ++p1) {
                    size_t v = make_idx(p1, p2, a1, a2);
                    size_t a = make_idx_p(p1, p2);
                    M1(v, a) = 1;
                    M2(a, v) = 1;
                }
            }
        }
    }
}


    static inline
void new_obs_prob2(size_t n_par, size_t n_al, double noise, MatrixXd& M1, MatrixXd& M2)
{
    size_t dom1 = n_par * n_par * n_al * n_al;
    size_t dom2 = n_al * n_al;
    double obs_0 = noise / (dom2 - 1);
    double obs_1 = 1. - noise;
    M1.resize(dom1, dom2);
    M2.resize(dom2, dom1);
    for (size_t a2 = 0; a2 < n_al; ++a2) {
        for (size_t a1 = 0; a1 < n_al; ++a1) {
            for (size_t p2 = 0; p2 < n_par; ++p2) {
                for (size_t p1 = 0; p1 < n_par; ++p1) {
                    size_t v = make_idx(p1, p2, a1, a2);
                    for (size_t im = 0; im < n_al; ++im) {
                        for (size_t jm = 0; jm < n_al; ++jm) {
                            size_t a = make_idx_a(im, jm);
                            if (a1 == im && a2 == jm) {
                                M1(v, a) = obs_1;
                                M2(a, v) = obs_1;
                            } else {
                                M1(v, a) = obs_0;
                                M2(a, v) = obs_0;
                            }
                        }
                    }
                }
            }
        }
    }
}


    static inline
void new_cross_prob2(size_t n_par, size_t n_al, MatrixXd& M1, MatrixXd& M2, MatrixXd& M3)
{
    size_t dom = n_par * n_par * n_al * n_al;
    M1 = MatrixXd::Zero(dom, dom * dom);
    M2 = MatrixXd::Zero(dom, dom * dom);
    M3 = MatrixXd::Zero(dom, dom * dom);
    for (size_t pa2 = 0; pa2 < n_al; ++pa2) {
        for (size_t pa1 = 0; pa1 < n_al; ++pa1) {
            for (size_t pp2 = 0; pp2 < n_par; ++pp2) {
                for (size_t pp1 = 0; pp1 < n_par; ++pp1) {
                    size_t idxp = make_idx(pp1, pp2, pa1, pa2);
                    for (size_t ma2 = 0; ma2 < n_al; ++ma2) {
                        for (size_t ma1 = 0; ma1 < n_al; ++ma1) {
                            for (size_t mp2 = 0; mp2 < n_par; ++mp2) {
                                for (size_t mp1 = 0; mp1 < n_par; ++mp1) {
                                    size_t idxm = make_idx(mp1, mp2, ma1, ma2);

                                    size_t idx1 = make_idx(mp1, pp1, ma1, pa1);
                                    size_t idx2 = make_idx(mp1, pp2, ma1, pa2);
                                    size_t idx3 = make_idx(mp2, pp1, ma2, pa1);
                                    size_t idx4 = make_idx(mp2, pp2, ma2, pa2);

                                    M1(idxm, idxp + dom * idx1) += .25;
                                    M1(idxm, idxp + dom * idx2) += .25;
                                    M1(idxm, idxp + dom * idx3) += .25;
                                    M1(idxm, idxp + dom * idx4) += .25;

                                    M2(idxp, idxm + dom * idx1) += .25;
                                    M2(idxp, idxm + dom * idx2) += .25;
                                    M2(idxp, idxm + dom * idx3) += .25;
                                    M2(idxp, idxm + dom * idx4) += .25;

                                    M3(idx1, idxm + dom * idxp) += .25;
                                    M3(idx2, idxm + dom * idxp) += .25;
                                    M3(idx3, idxm + dom * idxp) += .25;
                                    M3(idx4, idxm + dom * idxp) += .25;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}


    static inline
void new_cross_prob2(size_t n_par, size_t n_al, SparseMatrix<double>& M1, SparseMatrix<double>& M2, SparseMatrix<double>& M3)
{
    size_t dom = n_par * n_par * n_al * n_al;
    M1.resize(dom, dom * dom);
    M2.resize(dom, dom * dom);
    M3.resize(dom, dom * dom);
    std::vector<Eigen::Triplet<double>> t1, t2, t3;
    t1.reserve(dom * dom * 4);
    t2.reserve(dom * dom * 4);
    t3.reserve(dom * dom * 4);
    for (size_t pa2 = 0; pa2 < n_al; ++pa2) {
        for (size_t pa1 = 0; pa1 < n_al; ++pa1) {
            for (size_t pp2 = 0; pp2 < n_par; ++pp2) {
                for (size_t pp1 = 0; pp1 < n_par; ++pp1) {
                    size_t idxp = make_idx(pp1, pp2, pa1, pa2);
                    for (size_t ma2 = 0; ma2 < n_al; ++ma2) {
                        for (size_t ma1 = 0; ma1 < n_al; ++ma1) {
                            for (size_t mp2 = 0; mp2 < n_par; ++mp2) {
                                for (size_t mp1 = 0; mp1 < n_par; ++mp1) {
                                    size_t idxm = make_idx(mp1, mp2, ma1, ma2);

                                    size_t idx1 = make_idx(mp1, pp1, ma1, pa1);
                                    size_t idx2 = make_idx(mp1, pp2, ma1, pa2);
                                    size_t idx3 = make_idx(mp2, pp1, ma2, pa1);
                                    size_t idx4 = make_idx(mp2, pp2, ma2, pa2);

                                    /* TODO: comment */
                                    t1.emplace_back(idxm, idxp + dom * idx1, .25);
                                    t1.emplace_back(idxm, idxp + dom * idx2, .25);
                                    t1.emplace_back(idxm, idxp + dom * idx3, .25);
                                    t1.emplace_back(idxm, idxp + dom * idx4, .25);

                                    t2.emplace_back(idxp, idxm + dom * idx1, .25);
                                    t2.emplace_back(idxp, idxm + dom * idx2, .25);
                                    t2.emplace_back(idxp, idxm + dom * idx3, .25);
                                    t2.emplace_back(idxp, idxm + dom * idx4, .25);

                                    t3.emplace_back(idx1, idxm + dom * idxp, .25);
                                    t3.emplace_back(idx2, idxm + dom * idxp, .25);
                                    t3.emplace_back(idx3, idxm + dom * idxp, .25);
                                    t3.emplace_back(idx4, idxm + dom * idxp, .25);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    M1.setFromTriplets(t1.begin(), t1.end());
    M2.setFromTriplets(t2.begin(), t2.end());
    M3.setFromTriplets(t3.begin(), t3.end());
}




    static inline
void new_dh_prob2(size_t n_par, size_t n_al, MatrixXd& M1, MatrixXd& M2)
{
    size_t dom = n_par * n_par * n_al * n_al;
    M1 = MatrixXd::Zero(dom, dom);

    for (size_t ma2 = 0; ma2 < n_al; ++ma2) {
        for (size_t ma1 = 0; ma1 < n_al; ++ma1) {
            for (size_t mp2 = 0; mp2 < n_par; ++mp2) {
                for (size_t mp1 = 0; mp1 < n_par; ++mp1) {
                    size_t i1 = make_idx(mp1,mp2, ma1,ma2);
                    size_t j1 = make_idx(mp1,mp1, ma1,ma1);
                    size_t i2 = make_idx(mp1,mp2, ma1,ma2);
                    size_t j2 = make_idx(mp2,mp2, ma2,ma2);
                    M1(i1, j1) += .5;
                    M1(i2, j2) += .5;
                }
            }
        }
    }
    /*M1 /= M1.sum();*/
    M2 = M1.transpose();
}


    static inline
void new_dh_prob2(size_t n_par, size_t n_al, SparseMatrix<double>& M1, SparseMatrix<double>& M2)
{
    size_t dom = n_par * n_par * n_al * n_al;

    M1.resize(dom, dom * dom);
    M2.resize(dom, dom * dom);

    std::vector<Eigen::Triplet<double>> t1, t2, t3;
    t1.reserve(dom * dom * 2);
    t2.reserve(dom * dom * 2);

    for (size_t ma2 = 0; ma2 < n_al; ++ma2) {
        for (size_t ma1 = 0; ma1 < n_al; ++ma1) {
            for (size_t mp2 = 0; mp2 < n_par; ++mp2) {
                for (size_t mp1 = 0; mp1 < n_par; ++mp1) {
                    size_t i1 = make_idx(mp1,mp2, ma1,ma2);
                    size_t j1 = make_idx(mp1,mp1, ma1,ma1);
                    size_t i2 = make_idx(mp1,mp2, ma1,ma2);
                    size_t j2 = make_idx(mp2,mp2, ma2,ma2);
                    t1.emplace_back(i1, j1, .5);
                    t1.emplace_back(i2, j2, .5);
                    t2.emplace_back(j1, i1, .5);
                    t2.emplace_back(j2, i2, .5);
                }
            }
        }
    }
    M1.setFromTriplets(t1.begin(), t1.end());
    M2.setFromTriplets(t2.begin(), t2.end());
}


    static inline
void new_self_prob2(size_t n_par, size_t n_al, SparseMatrix<double>& M1, SparseMatrix<double>& M2)
{
    size_t dom = n_par * n_par * n_al * n_al;

    M1.resize(dom, dom);
    M2.resize(dom, dom);

    std::vector<Eigen::Triplet<double>> t1, t2, t3;
    t1.reserve(dom * dom);
    t2.reserve(dom * dom);

    for (size_t g2a2 = 0; g2a2 < n_al; ++g2a2) {
        for (size_t g2a1 = 0; g2a1 < n_al; ++g2a1) {
            for (size_t g2p2 = 0; g2p2 < n_par; ++g2p2) {
                for (size_t g2p1 = 0; g2p1 < n_par; ++g2p1) {
                    size_t j = make_idx(g2p1, g2p2, g2a1, g2a2);
                    for (size_t g1a2 = 0; g1a2 < n_al; ++g1a2) {
                        for (size_t g1a1 = 0; g1a1 < n_al; ++g1a1) {
                            for (size_t g1p2 = 0; g1p2 < n_par; ++g1p2) {
                                for (size_t g1p1 = 0; g1p1 < n_par; ++g1p1) {  /* sic */
                                    size_t i = make_idx(g1p1, g1p2, g1a1, g1a2);
                                    if (g1p1 == g1p2 && g1a1 == g1a2 && g2p1 == g2p2 && g2a1 == g2a2 && g2p1 == g1p1 && g2a1 == g1a1) {
                                        t1.emplace_back(i, j, 1);
                                        t2.emplace_back(j, i, 1);
                                    } else if (g1p1 == g2p1 && g1a1 == g2a1 && g1p2 == g2p2 && g1a2 == g2a2) {
                                        t1.emplace_back(i, j, .25);
                                        t2.emplace_back(j, i, .25);
                                    } else if (g1p2 == g2p1 && g1a2 == g2a1 && g1p2 == g2p2 && g1a2 == g2a2) {
                                        t1.emplace_back(i, j, .25);
                                        t2.emplace_back(j, i, .25);
                                    } else if (g1p2 == g2p1 && g1a2 == g2a1 && g1p1 == g2p2 && g1a1 == g2a2) {
                                        t1.emplace_back(i, j, .25);
                                        t2.emplace_back(j, i, .25);
                                    } else if (g1p1 == g2p1 && g1a1 == g2a1 && g1p1 == g2p2 && g1a1 == g2a2) {
                                        t1.emplace_back(i, j, .25);
                                        t2.emplace_back(j, i, .25);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    M1.setFromTriplets(t1.begin(), t1.end());
    M2.setFromTriplets(t2.begin(), t2.end());
}

    static inline
void new_self_prob2(size_t n_par, size_t n_al, MatrixXd& M1, MatrixXd& M2)
{
    size_t dom = n_par * n_par * n_al * n_al;
    M1 = MatrixXd::Zero(dom, dom);

    for (size_t g2a2 = 0; g2a2 < n_al; ++g2a2) {
        for (size_t g2a1 = 0; g2a1 < n_al; ++g2a1) {
            for (size_t g2p2 = 0; g2p2 < n_par; ++g2p2) {
                for (size_t g2p1 = 0; g2p1 < n_par; ++g2p1) {
                    size_t j = make_idx(g2p1, g2p2, g2a1, g2a2);
                    for (size_t g1a2 = 0; g1a2 < n_al; ++g1a2) {
                        for (size_t g1a1 = 0; g1a1 < n_al; ++g1a1) {
                            for (size_t g1p2 = 0; g1p2 < n_par; ++g1p2) {
                                for (size_t g1p1 = 0; g1p1 < n_par; ++g1p1) {  /* sic */
                                    size_t i = make_idx(g1p1, g1p2, g1a1, g1a2);
                                    if (g1p1 == g1p2 && g1a1 == g1a2 && g2p1 == g2p2 && g2a1 == g2a2 && g2p1 == g1p1 && g2a1 == g1a1) {
                                        M1(i, j) = 1;
                                    } else if (g1p1 == g2p1 && g1a1 == g2a1 && g1p2 == g2p2 && g1a2 == g2a2) {
                                        M1(i, j) = .25;
                                    } else if (g1p2 == g2p1 && g1a2 == g2a1 && g1p2 == g2p2 && g1a2 == g2a2) {
                                        M1(i, j) = .25;
                                    } else if (g1p2 == g2p1 && g1a2 == g2a1 && g1p1 == g2p2 && g1a1 == g2a2) {
                                        M1(i, j) = .25;
                                    } else if (g1p1 == g2p1 && g1a1 == g2a1 && g1p1 == g2p2 && g1a1 == g2a2) {
                                        M1(i, j) = .25;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    M2 = M1.transpose();
}

#undef make_idx


#endif

