/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_BAYES_CLI_H_
#define _SPELL_BAYES_CLI_H_

#include "error.h"
#include <map>
#include <vector>
/*#include "generation_rs.h"*/
#include "commandline.h"
/*#include "bn.h"*/
/*#include "factor_var4.h"*/
#include "file.h"


enum JobDispatchScheme { JDS_None, JDS_MT, JDS_SGE, JDS_SSH };

void read_format(std::map<std::string, marker_observation_spec>& settings, const std::string& filename, file& is);

#define SPELL_CASTER ((size_t) -1)

struct bn_settings_t {
    /* Job control */
    JobDispatchScheme scheme;
    std::vector<std::string> command_line;
    std::vector<std::string> ssh_hosts;
    std::string qsub_opts;
    std::string prg_name;
    size_t n_threads;
    size_t job_start;
    size_t job_end;
    std::string job_name;
    std::string fifo_path;
    /* Configuration */
    std::string pop_name;
    std::string qtl_generation_name;
    std::string work_directory;
    std::map<std::string, marker_observation_spec> marker_observation_specs;
    std::vector<std::string> marker_observation_specs_filenames;
    std::map<std::string, population_marker_observation> observed_mark;
    double noise;
    double tolerance;
    /* Inputs */
    pedigree_type pedigree;
    std::string pedigree_filename;
    std::string design_filename;
    /* BN */
    /*pedigree_bayesian_network* bn;*/
    /* Management */
    std::vector<std::map<char, int>> alleles_per_marker;
    std::vector<size_t> unique_n_alleles;
    /*std::map<std::string, std::map<char, VectorXd>> compiled_obs_specs;*/
    std::vector<std::string> marker_names;
    std::map<std::string, size_t> marker_index;

    /* cleanup */
    std::set<std::string> job_filenames;
    std::mutex job_filenames_mutex;

    std::vector<std::string> input_generations;
    std::vector<std::string> output_generations;

    static const int OutputPopData = 1;
    static const int OutputOnePoint = 2;
    static const int OutputGametes = 4;

    int output_mode;
    std::vector<std::string> closing_messages;
    
    bn_settings_t()
        : scheme(JDS_None)
        , command_line()
        , ssh_hosts()
        , qsub_opts()
        , n_threads(0)
        , job_start(SPELL_CASTER)
        , job_end(SPELL_CASTER)
        , job_name()
        , fifo_path()
        , pop_name()
        , qtl_generation_name()
        , work_directory(".")
        , marker_observation_specs()
        , marker_observation_specs_filenames()
        , observed_mark()
        , noise(0.)
        , tolerance(1.e-10)
        , pedigree()
        , pedigree_filename()
        , design_filename()
        /*, bn(NULL)*/
        , alleles_per_marker()
        , marker_names()
        , marker_index()
        , job_filenames()
        , job_filenames_mutex()
        , input_generations()
        , output_generations()
        , output_mode(0)
        , closing_messages()
    {}

    size_t count_markers() const
    {
        return observed_mark.begin()->second.observations.data.size();
    }

    void post_init();

    bool is_master() const { return job_start == SPELL_CASTER; }

    static bn_settings_t* from_args(int argc, const char** argv);

    std::string
        job_filename(const char* job_name, size_t job)
        {
            /*bn_settings_t* unconst_this = const_cast<bn_settings_t*>(this);*/
            /*if (!unconst_this) {*/
                /*MSG_ERROR("Couldn't unconst the pointer to settings.", "Sacrifice a goat and hope for the best.");*/
            /*}*/
            std::string ret = SPELL_STRING(work_directory << '/' << job_name << '.' << job << ".data");
            std::lock_guard<std::mutex> lock(/*unconst_this->*/job_filenames_mutex);
            /*unconst_this->*/job_filenames.insert(ret);
            return ret;
        }

    void
        cleanup_job_files() const
        {
            for (const auto& path: job_filenames) {
                unlink(path.c_str());
            }
        }

    void
        load_full_pedigree_data()
        {
            /* NOT const. But called in a const context (job "collect-LV").
             * It is just not possible in EVERY case to handle this before the job is scheduled,
             * depending on which job dispatch method is used. Maybe there should be some
             * refactoring later with some settings->prepare_job(job_name) just before the job
             * is started.
             */
            /*const_cast<pedigree_type&>(pedigree).load(pedigree_filename, false);*/
            pedigree.load(pedigree_filename, false);
            for (const auto& pg: pedigree.generations) {
                if (pg) {
                    MSG_DEBUG("Generation " << pg->name << " has " << pg->labels);
                } else {
                    MSG_DEBUG("NULL pg");
                }
            }
        }
};

#endif

