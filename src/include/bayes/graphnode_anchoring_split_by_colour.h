/*
 * Copyright (c) 2016 Damien Leroux, Sylvain Jasson, INRA
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _SPEL_BAYES_GRAPH_NODE_H_
#define _SPEL_BAYES_GRAPH_NODE_H_

#include <memory>
#include <vector>
#include <algorithm>
#include <iostream>
#include <deque>
#include <list>
#include <map>
#include <cstdio>

/*#include "factor.h"*/
#include "../pedigree.h"

#include "../error.h"

#include "generalized_product.h"

template <typename V>
std::ostream&
operator << (std::ostream& os, const std::vector<std::vector<V>>& vv)
{
    auto i = vv.begin(), j = vv.end();
    if (i != j) {
        os << '{' << (*i) << '}';
        for (++i; i != j; ++i) {
            os << " {" << (*i) << '}';
        }
    }
    return os;
}

typedef int variable_index_type;
typedef size_t node_index_type;
typedef std::vector<node_index_type> node_vec;
typedef std::vector<variable_index_type> var_vec;
struct graph_type;
struct edge_type {
    const graph_type* graph;
    node_index_type first, second;

    edge_type() : graph(NULL), first(0), second(0) {}
    edge_type(const graph_type* g, node_index_type f, node_index_type s) : graph(g), first(f), second(s) {}

    bool
        operator < (const edge_type& other) const
        {
            return graph < other.graph
                || (graph == other.graph
                        && (first < other.first
                            || (first == other.first && second < other.second)));
        }

    void
        file_io(ifile& fs, const graph_type* g)
        {
            graph = g;
            rw_base rw;
            rw(fs, first);
            rw(fs, second);
        }

    void
        file_io(ofile& fs, const graph_type*)
        {
            rw_base rw;
            rw(fs, first);
            rw(fs, second);
        }

    friend
        std::ostream&
        operator << (std::ostream& os, const edge_type& e);
};



struct colour_proxy_impl;
typedef std::shared_ptr<colour_proxy_impl> colour_proxy;
struct colour_proxy_impl {
    colour_proxy proxy;
    std::weak_ptr<colour_proxy_impl> cache;

    friend
        colour_proxy
        get_colour_impl(colour_proxy col)
        {
            while (col->cache.lock()->proxy) { col->cache = col->cache.lock()->proxy; }
            return col->cache.lock();
        }

    friend
        colour_proxy
        assign_colour_impl(colour_proxy old_col, colour_proxy new_col)
        {
            auto oc = get_colour_impl(old_col);
            auto nc = get_colour_impl(new_col);
            if (oc != nc) {
                oc->proxy = nc;
            }
            return old_col;
        }

    friend
        bool
        colour_equal(colour_proxy c1, colour_proxy c2)
        {
            return get_colour_impl(c1) == get_colour_impl(c2);
        }
};

inline
colour_proxy
create_colour() { auto ret = std::make_shared<colour_proxy_impl>(); ret->cache = ret; return ret; }


template <typename V>
void
sort_and_unique(std::vector<V>& v)
{
    std::sort(v.begin(), v.end());
    v.erase(std::unique(v.begin(), v.end()), v.end());
}


enum node_type { Factor, Interface, Aggregate };


template <typename V>
std::ostream&
operator << (std::ostream& os, const std::list<V>& v)
{
    auto i = v.begin(), j = v.end();
    if (i != j) {
        os << (*i);
        for (++i; i != j; ++i) {
            os << " -- " << (*i);
        }
    }
    return os;
}


template <typename K, typename V>
std::ostream&
operator << (std::ostream& os, const std::map<K, V>& m)
{
    os << "{ ";
    for (const auto& kv: m) {
        os << kv.first << ": " << kv.second << ", ";
    }
    return os << '}';
}




typedef std::vector<genotype_comb_type> message_type;


inline
std::ostream&
operator << (std::ostream& os, const message_type& msg)
{
    auto i = msg.begin(), j = msg.end();
    if (i != j) {
        os << (*i);
        for (++i; i != j; ++i) {
            os << " | " << (*i);
        }
    }
    return os;
}


struct multiple_product_type {
    std::unordered_map<variable_index_type, colour_proxy> colours;
    std::vector<std::vector<var_vec>> vec_varsets;
    std::unordered_map<colour_proxy, std::vector<const genotype_comb_type*>> bins;
    std::unordered_map<colour_proxy, var_vec> varset_bins;
    std::vector<const message_type*> messages;

    void
        add(const message_type& msg)
        {
            size_t S = msg.size();
            vec_varsets.emplace_back();
            auto& varsets = vec_varsets.back();

            for (size_t i = 0; i < S; ++i) {
                varsets.emplace_back(get_parents(msg[i]));
                auto& varset = varsets.back();
                for (variable_index_type v: varset) {
                    auto& ptr = colours[v];
                    if (!ptr) {
                        ptr = create_colour();
                    }
                }
            }

            for (size_t i = 0; i < S; ++i) {
                if (varsets[i].size() == 0) { continue; }
                auto mcol = colours[varsets[i].front()];
                for (variable_index_type v: varsets[i]) {
                    auto& vcol = colours[v];
                    if (vcol && !colour_equal(vcol, mcol)) {
                        assign_colour_impl(vcol, mcol);
                    }
                }
            }
            messages.push_back(&msg);
        }

    void add(std::shared_ptr<message_type> ptr) { add(*ptr); }

    message_type
        compute(const var_vec& output, const std::map<var_vec, genotype_comb_type>& domains)
        {
            auto msg = messages.begin();
            for (const auto& varsets: vec_varsets) {
                for (size_t i = 0; i < varsets.size(); ++i) {
                    if (varsets[i].size() == 0) { continue; }
                    bins[get_colour_impl(colours[varsets[i].front()])].push_back(&(**msg)[i]);
                }
                for (const auto& kv: colours) {
                    varset_bins[get_colour_impl(kv.second)].push_back(kv.first);
                }
                ++msg;
            }

            for (auto& kv: varset_bins) {
                sort_and_unique(kv.second);
                /*MSG_DEBUG("varset " << kv.second << " assembles " << bins[kv.first].size() << " tables ");*/
                /*for (auto p: bins[kv.first]) { MSG_DEBUG(" * " << (*p)); }*/
            }

            message_type tmp;
            tmp.reserve(bins.size());
            for (const auto& kv: bins) {
                if ((output % varset_bins[kv.first]).size()) {
                    if (kv.second.size()) {
                        MSG_QUEUE_FLUSH();
                        if (kv.second.size() > 1) {
                            tmp.emplace_back(compute_product(kv.second.begin(), kv.second.end(), output, domains));
                        } else {
                            tmp.emplace_back(*kv.second.front());
                        }
                    } else {
                        tmp.push_back(*kv.second.front());
                    }
                }
            }
            /*MSG_DEBUG("result: " << tmp);*/
            return tmp;
        }
};


inline
message_type
product(const message_type& accum, const message_type& msg, const std::map<var_vec, genotype_comb_type>& domains)
{
    /*scoped_indent _(SPELL_STRING("[product] "));*/
    /*MSG_DEBUG("" << accum);*/
    /*MSG_DEBUG("" << msg);*/
    multiple_product_type mp;
    mp.add(accum);
    mp.add(msg);
    var_vec all;
    for (const auto& t: accum) { auto p = get_parents(t); all.insert(all.end(), p.begin(), p.end()); }
    for (const auto& t: msg) { auto p = get_parents(t); all.insert(all.end(), p.begin(), p.end()); }
    sort_and_unique(all);
    return mp.compute(all, domains);
#if 0
    message_type tmp;

    std::unordered_map<variable_index_type, colour_proxy> colours;

    std::vector<var_vec> accum_varsets, msg_varsets;

    for (size_t ai = 0; ai < accum.size(); ++ai) {
        accum_varsets.push_back(get_parents(accum[ai]));
        for (variable_index_type v: accum_varsets.back()) {
            auto& ptr = colours[v];
            if (!ptr) {
                ptr = create_colour();
            }
        }
    }
    for (size_t mi = 0; mi < msg.size(); ++mi) {
        msg_varsets.push_back(get_parents(msg[mi]));
        for (variable_index_type v: msg_varsets.back()) {
            auto& ptr = colours[v];
            if (!ptr) {
                ptr = create_colour();
            }
        }
    }

    for (size_t i = 0; i < accum.size(); ++i) {
        if (accum_varsets[i].size() == 0) { continue; }
        auto mcol = colours[accum_varsets[i].front()];
        for (variable_index_type v: accum_varsets[i]) {
            auto& vcol = colours[v];
            if (vcol && !colour_equal(vcol, mcol)) {
                assign_colour_impl(vcol, mcol);
            }
        }
    }

    for (size_t i = 0; i < msg.size(); ++i) {
        if (msg_varsets[i].size() == 0) { continue; }
        auto mcol = colours[msg_varsets[i].front()];
        for (variable_index_type v: msg_varsets[i]) {
            auto& vcol = colours[v];
            if (vcol && !colour_equal(vcol, mcol)) {
                assign_colour_impl(vcol, mcol);
            }
        }
    }

    std::unordered_map<colour_proxy, std::vector<const genotype_comb_type*>> bins;
    std::unordered_map<colour_proxy, var_vec> varsets;
    for (size_t i = 0; i < accum.size(); ++i) {
        if (accum_varsets[i].size() == 0) { continue; }
        bins[get_colour_impl(colours[accum_varsets[i].front()])].push_back(&accum[i]);
    }
    for (size_t i = 0; i < msg.size(); ++i) {
        if (msg_varsets[i].size() == 0) { continue; }
        bins[get_colour_impl(colours[msg_varsets[i].front()])].push_back(&msg[i]);
    }
    for (const auto& kv: colours) {
        varsets[get_colour_impl(kv.second)].push_back(kv.first);
    }

    tmp.reserve(bins.size());
    for (const auto& kv: bins) {
        tmp.emplace_back(compute_product(kv.second.begin(), kv.second.end(), varsets[kv.first], domains));
    }
    MSG_DEBUG("result: " << tmp);
    return tmp;

#if 0
    std::vector<colour_proxy> uniq;
    for (const auto& kv: colours) {
        uniq.push_back(get_colour_impl(kv.second));
    }
    sort_and_unique(uniq);

    std::vector<size_t> accum_dest, msg_dest;
    for (size_t i = 0; i < accum.size(); ++i) {
        if (accum_varsets[i].size() == 0) {
            accum_dest.push_back(0);
            continue;
        }
        accum_dest.push_back(std::find(uniq.begin(), uniq.end(), get_colour_impl(colours[accum_varsets[i].front()])) - uniq.begin());
    }
    for (size_t i = 0; i < msg.size(); ++i) {
        if (msg_varsets[i].size() == 0) {
            msg_dest.push_back(0);
            continue;
        }
        msg_dest.push_back(std::find(uniq.begin(), uniq.end(), get_colour_impl(colours[msg_varsets[i].front()])) - uniq.begin());
    }

    tmp.resize(uniq.size());

    for (size_t i = 0; i < accum.size(); ++i) {
        auto& table = tmp[accum_dest[i]];
        if (table.size()) {
            table = kronecker(table, accum[i]);
        } else {
            table = accum[i];
        }
    }
    for (size_t i = 0; i < msg.size(); ++i) {
        auto& table = tmp[msg_dest[i]];
        if (table.size()) {
            table = kronecker(table, msg[i]);
        } else {
            table = msg[i];
        }
    }

    return tmp;
#endif
#endif
}

inline
message_type&
accumulate(message_type& accum, const message_type& msg, const std::map<var_vec, genotype_comb_type>& domains)
{
    /*auto tmp = accum * msg;*/
    auto tmp = product(accum, msg, domains);
    accum.swap(tmp);
    return accum;
}

inline
std::shared_ptr<message_type>
accumulate(std::shared_ptr<message_type>& accum, const std::shared_ptr<message_type>& msg, const std::map<var_vec, genotype_comb_type>& domains)
{
    if (accum && accum->size()) {
        if (msg && msg->size()) {
            /**accum *= *msg;*/
            accumulate(*accum, *msg, domains);
        }
    } else {
        accum = msg;
    }
    return accum;
}

inline
std::shared_ptr<message_type>
product(std::shared_ptr<message_type> m1, std::shared_ptr<message_type> m2, const std::map<var_vec, genotype_comb_type>& domains)
{
    if (!m1) {
        return m2 ? std::make_shared<message_type>(*m2) : std::make_shared<message_type>();
    } else if (!m2) {
        return std::make_shared<message_type>(*m1);
    } else {
        /*return std::make_shared<message_type>(*m1 * *m2);*/
        return std::make_shared<message_type>(product(*m1, *m2, domains));
    }
}


inline
message_type
operator % (const message_type& msg, const var_vec& variables)
{
    message_type tmp;
    tmp.reserve(msg.size());
    for (const auto& table: msg) {
        auto varset = get_parents(table);
        if (varset == variables) {
            tmp.push_back(table);
        } else {
            auto proj = varset % variables;
            if (proj.size()) {
                /*var_vec norm = varset - proj;*/
                var_vec norm;
                tmp.push_back(project(table, proj, norm));
            }
        }
    }
    return tmp;
}


inline
std::shared_ptr<message_type>
operator % (std::shared_ptr<message_type> msg, const var_vec& variables)
{
    if (msg) {
        return std::make_shared<message_type>(*msg % variables);
    } else {
        return msg;
    }
}


inline
message_type&
operator %= (message_type& msg, const var_vec& variables)
{
    auto tmp = msg % variables;
    msg.swap(tmp);
    return msg;
}


inline
std::shared_ptr<message_type>
operator %= (std::shared_ptr<message_type> msg, const var_vec& variables)
{
    auto tmp = (*msg) % variables;
    msg->swap(tmp);
    return msg;
}


inline
genotype_comb_type&
accumulate(genotype_comb_type& table, const message_type& msg, const std::map<var_vec, genotype_comb_type>& domains)
{
    joint_variable_product_type jvp;
    jvp.add_table(table);
    for (const auto& mt: msg) {
        jvp.add_table(mt);
    }
    jvp.set_output(jvp.all_variable_names);
    jvp.compile(domains);
    table = jvp.compute();
    /*if (table.size()) {*/
        /*for (const auto& mt: msg) {*/
            /*table = kronecker(table, mt);*/
        /*}*/
    /*} else {*/
        /*auto i = msg.begin(), j = msg.end();*/
        /*if (i != j) {*/
            /*table = *i++;*/
        /*}*/
        /*for (; i != j; ++i) {*/
            /*table = kronecker(table, *i);*/
        /*}*/
    /*}*/
    return table;
}




inline
std::ostream&
operator << (std::ostream& os, std::shared_ptr<message_type> msg)
{
    if (msg) {
        return os << (*msg);
    } else {
        return os << "unif";
    }
}




inline
void
normalize(genotype_comb_type& table)
{
    double accum = 0;
    for (const auto& e: table) {
        accum += e.coef;
    }
    if (accum != 0) {
        accum = 1. / accum;
        for (auto& e: table) {
            e.coef *= accum;
        }
    }
}


inline void normalize(message_type& msg) { for (auto& table: msg) { normalize(table); } }
inline void normalize(std::shared_ptr<message_type> msg) { normalize(*msg); }


enum VariableIO { None=0, Input=1, Output=2 };
inline VariableIO operator | (VariableIO v1, VariableIO v2) { return (VariableIO) (((int) v1) | ((int) v2)); }
inline VariableIO& operator |= (VariableIO& v1, VariableIO v2) { v1 = (VariableIO) (((int) v1) | ((int) v2)); return v1; }

struct graph_type {

    struct query_op_atom_type {
        const graph_type* g;
        var_vec variables;
        node_index_type node;
        size_t n_operands;

        query_op_atom_type() : g(NULL), variables(), node(0), n_operands(0) {}

        query_op_atom_type(const graph_type* _g, const var_vec& vv, node_index_type n, size_t no)
            : g(_g), variables(vv), node(n), n_operands(no)
        {}

        friend
            std::ostream&
            operator << (std::ostream& os, const query_op_atom_type& qoa)
            {
                return os << "<op out=" << qoa.variables << " inner=" << (qoa.g->is_interface(qoa.node) ? 'I' : 'F') << qoa.g->variables_of(qoa.node) << " n_opd=" << qoa.n_operands << '>';
            }

        template <typename STREAM_TYPE>
            void
            file_io(STREAM_TYPE& fs)
            {
                rw_base rw;
                rw(fs, variables);
                rw(fs, node);
                rw(fs, n_operands);
            }

        void
            operator () (std::vector<std::shared_ptr<message_type>>& stack, std::vector<const var_vec*>& var_stack) const
            {
                /*bool itf = g->is_interface(node);*/
                std::shared_ptr<message_type> ret;
                /*if (!itf) {*/
                    ret = std::make_shared<message_type>(g->state[node]);
                /*} else {*/
                /*}*/
                multiple_product_type mp;
                for (size_t i = 0; i < n_operands; ++i) {
                    mp.add(stack.back());
                }
                ret = std::make_shared<message_type>(mp.compute(variables, g->domains));
                /*for (size_t i = 0; i < n_operands; ++i) {*/
                    /*auto op = stack.back();*/
                    /*stack.pop_back();*/
                    /*var_stack.pop_back();*/
                    /*accumulate(ret, op, g->domains);*/
                /*}*/
                /*if (!itf) {*/
                    ret %= variables;
                /*}*/
                stack.emplace_back(ret);
                var_stack.push_back(&variables);
            }
    };


    typedef std::vector<query_op_atom_type> query_operation_type;

    enum ComputeStateOperation { PushState, PushMessage, Accumulate, Project, Store };
#define CSO_TYPE_STR(_c) (_c == PushState ? "PushState" : _c == PushMessage ? "PushMessage" : _c == Accumulate ? "Accumulate" : _c == Project ? "Project" : "Store")
    struct compute_state_operation_type {
        const graph_type* g;
        ComputeStateOperation op_type;
        size_t n;
        edge_type e;
        var_vec v;
        std::vector<query_operation_type> op;
        size_t n_nei_sub;

        compute_state_operation_type() : g(NULL), op_type(PushState), n(0), e(), v(), op(), n_nei_sub(0) {}
        compute_state_operation_type(const graph_type* _g, ComputeStateOperation ot, size_t _n, const edge_type& _e, var_vec&& _v)
            : g(_g), op_type(ot), n(_n), e(_e), v(std::move(_v)), op()
        {}

        void
            operator () (std::map<edge_type, std::shared_ptr<message_type>>& messages, std::vector<std::shared_ptr<message_type>>& stack) const
            {
                /*MSG_DEBUG((*this));*/
                /*MSG_QUEUE_FLUSH();*/
                switch (op_type) {
                    case PushState:
                        if (g->is_aggregate(n) && !g->is_compound_interface(n)) {
                            std::map<edge_type, std::shared_ptr<message_type>> messages;
                            g->subgraphs[n]->compute_messages(stack.end() - n_nei_sub, stack.end(), messages);
                            /*MSG_QUEUE_FLUSH();*/
                            stack.resize(stack.size() - n_nei_sub);
                            /*MSG_QUEUE_FLUSH();*/
                            g->subgraphs[n]->compute_state(op, messages);
                            stack.emplace_back(std::make_shared<message_type>());
                            for (const auto& msg: g->subgraphs[n]->extract(op)) {
                                accumulate(stack.back(), msg, g->domains);
                            }
                            /*MSG_QUEUE_FLUSH();*/
                            /*stack.push_back(g->subgraphs[n]->extract(op));*/
                        } else {
                            stack.push_back(std::make_shared<message_type>(message_type{g->state[n]}));
                        }
                        break;
                    case PushMessage:
                        stack.push_back(messages[e]);
                        break;
                    case Accumulate:
                        {
                            multiple_product_type mp;
                            auto i = stack.rbegin();
                            auto j = i + n;
                            for (; i != j; ++i) {
                                mp.add(*i);
                                /*MSG_DEBUG((*i));*/
                            }
                            auto result = mp.compute(v, g->domains);
                            stack.resize(stack.size() - n);
                            stack.emplace_back(std::make_shared<message_type>(std::move(result)));
                            /*MSG_DEBUG(" => " << stack.back());*/
                        }
                        /*for (size_t i = 0; i < n; ++i) {*/
                            /*auto m2 = stack.back();*/
                            /*stack.pop_back();*/
                            /*accumulate(stack.back(), m2, g->domains);*/
                        /*}*/
                        break;
                    case Project:
                        stack.back() %= v;
                        normalize(stack.back());
                        break;
                    case Store:
                        if (stack.back()) {
                            messages[e] = std::make_shared<message_type>(*stack.back());
                        } else {
                            messages[e] = std::make_shared<message_type>();
                        }
                        stack.pop_back();
                        break;
                    default:;
                };
                /*MSG_DEBUG("STACK");*/
                /*for (const auto& s: stack) { MSG_DEBUG("  " << s); }*/
                /*MSG_DEBUG("MESSAGES");*/
                /*for (const auto& kv: messages) {*/
                    /*MSG_DEBUG(std::setw(20) << std::left << kv.first << " [" << kv.second << ']');*/
                /*}*/
            }

        void
            file_io_op(ifile& fs, const graph_type* grph)
            {
                size_t size;
                rw_base rw;
                rw(fs, size); op.clear(); op.resize(size);
                for (auto& v: op) {
                    rw(fs, size); v.resize(size);
                    for (auto& o: v) {
                        o.g = grph;
                        o.file_io(fs);
                    }
                }
                int ot;
                rw(fs, ot);
                op_type = (ComputeStateOperation) ot;
                g = grph;
            }

        void
            file_io_op(ofile& fs, const graph_type*)
            {
                rw_base rw;
                rw(fs, op.size());
                for (auto& v: op) {
                    rw(fs, v.size());
                    for (auto& o: v) {
                        o.file_io(fs);
                    }
                }
                rw(fs, (int) op_type);
            }

        template <typename STREAM_TYPE>
            void
            file_io(STREAM_TYPE& fs, const graph_type* grph)
            {
                rw_base rw;
                rw(fs, n);
                e.file_io(fs, grph);
                rw(fs, v);
                rw(fs, n_nei_sub);
                file_io_op(fs, grph);
            }

        friend
            std::ostream&
            operator << (std::ostream& os, const compute_state_operation_type& cso)
            {
                os << '<';
                switch (cso.op_type) {
                    case PushState:
                        os << "PushState " << (cso.g->is_interface(cso.n) ? 'I' : cso.g->is_aggregate(cso.n) ? 'A' : 'F') << cso.g->variables_of(cso.n);
                        if (cso.g->is_aggregate(cso.n)) {
                            os << " query=" << cso.op;
                        }
                        break;
                    case PushMessage:
                        os << "PushMessage " << cso.e;
                        break;
                    case Accumulate:
                        os << "Accumulate " << cso.n << " / " << cso.v;
                        break;
                    case Project:
                        os << "Project " << cso.v;
                        break;
                    case Store:
                        os << "Store " << cso.e;
                        break;
                };
                return os << '>';
            }
    };


    node_vec rank;
    node_vec represented_by;
    std::vector<node_type> type;
    std::vector<colour_proxy> colour;
    std::vector<node_vec> neighbours_in;
    std::vector<node_vec> neighbours_out;
    std::vector<node_vec> inner_nodes;
    std::vector<var_vec> rules;
    var_vec variables;
    std::vector<var_vec> node_variables;
    std::map<variable_index_type, VariableIO> io;

    std::map<variable_index_type, node_index_type> interface_to_node;
    std::map<node_index_type, variable_index_type> node_to_interface;

    std::vector<std::shared_ptr<graph_type>> subgraphs;

    std::vector<std::shared_ptr<message_type>> tables;
    std::vector<message_type> state;
    std::map<var_vec, genotype_comb_type> domains;
    /* TODO suppress joint_parent_domains and all afferent code */
    std::map<var_vec, genotype_comb_type> joint_parent_domains;
    std::map<variable_index_type, char> ancestor_letters;
    std::vector<compute_state_operation_type> compute_state_ops;

    const graph_type* parent;
    node_index_type index_in_parent;

    bool aggregate_cycles;
    bool generate_interfaces;

    size_t n_alleles;

    /* THIS DOESN'T HAVE  TO BE SAVED/LOADED. TEMPORARY STATE IN ORDER TO COMPUTE THE SEQUENCES OF OPERATIONS. */
    std::vector<var_vec> annotations;

    std::vector<bool> is_dh;

    graph_type& operator = (graph_type&& other) = delete;
#if 0
        {
            rank = other.rank;
            represented_by = std::move(other.represented_by);
            type = std::move(other.type);
            colour = std::move(other.colour);
            neighbours_in = std::move(other.neighbours_in);
            neighbours_out = std::move(other.neighbours_out);
            inner_nodes = std::move(other.inner_nodes);
            rules = std::move(other.rules);
            variables = std::move(other.variables);
            node_variables = std::move(other.node_variables);
            io = std::move(other.io);
            interface_to_node = std::move(other.interface_to_node);
            node_to_interface = std::move(other.node_to_interface);
            subgraphs = std::move(other.subgraphs);
            tables = std::move(other.tables);
            state = std::move(other.state);
            parent = std::move(other.parent);
            index_in_parent = std::move(other.index_in_parent);
            aggregate_cycles = std::move(other.aggregate_cycles);
            generate_interfaces = std::move(other.generate_interfaces);
            n_alleles = other.n_alleles;
            annotations = std::move(other.annotations);
            is_dh = std::move(other.is_dh);
            domains = std::move(other.domains);
            ancestor_letters = std::move(other.ancestor_letters);
            node_domains = std::move(other.node_domains);
            node_domain_computed = std::move(other.node_domain_computed);
            for (auto& s: subgraphs) {
                if (s) {
                    s->parent = this;
                }
            }
            return *this;
        }
#endif

    /* node index 0 is the trash bin */

    graph_type(const graph_type& other) = delete;
#if 0
        : rank(other.rank), represented_by(other.represented_by), type(other.type), colour(other.colour), neighbours_in(other.neighbours_in), neighbours_out(other.neighbours_out), inner_nodes(other.inner_nodes), rules(other.rules), variables(other.variables), node_variables(other.node_variables), io(), interface_to_node(), node_to_interface(), subgraphs(other.subgraphs), tables(other.tables), state(other.state), domains(other.domains), ancestor_letters(other.ancestor_letters), parent(other.parent), index_in_parent(other.index_in_parent),aggregate_cycles(other.aggregate_cycles), generate_interfaces(other.generate_interfaces), n_alleles(other.n_alleles), annotations(other.annotations), is_dh(other.is_dh)
    {
        for (auto& s: subgraphs) {
            if (s) {
                s->parent = this;
            }
        }
    }
#endif

    graph_type()
        : rank(1), represented_by(1), type(1), colour(1), neighbours_in(1), neighbours_out(1), inner_nodes(1), rules(1), variables(1), node_variables(1), io(), interface_to_node(), node_to_interface(), subgraphs(1), tables(1), state(1), parent(nullptr), index_in_parent(0),aggregate_cycles(true), generate_interfaces(true), n_alleles(1), annotations(1), is_dh(1, false)
    {}

    graph_type(size_t n_al)
        : rank(1), represented_by(1), type(1), colour(1), neighbours_in(1), neighbours_out(1), inner_nodes(1), rules(1), variables(1), node_variables(1), io(), interface_to_node(), node_to_interface(), subgraphs(1), tables(1), state(1), parent(nullptr), index_in_parent(0), aggregate_cycles(true), generate_interfaces(true), n_alleles(n_al), annotations(1), is_dh(1, false)
    {}

    void
        io_colours(ofile& fs)
        {
            rw_base rw;
            rw(fs, colour.size());
            for (const auto& c: colour) {
                rw(fs, (ptrdiff_t) &*get_colour_impl(c));
            }
        }

    void
        io_colours(ifile& fs)
        {
            rw_base rw;
            size_t sz;
            rw(fs, sz);
            colour.clear();
            colour.reserve(sz);
            std::map<ptrdiff_t, colour_proxy> dic;
            for (; sz; --sz) {
                ptrdiff_t value;
                rw(fs, value);
                auto it = dic.find(value);
                if (it == dic.end()) {
                    colour.emplace_back(create_colour());
                    dic[value] = colour.back();
                } else {
                    colour.emplace_back(it->second);
                }
            }
        }

    void
        io_type_subgraphs_tables(ofile& fs)
        {
            rw_comb<int, bn_label_type> rw;
            rw(fs, type.size());
            for (auto t: type) {
                rw(fs, (int) t);
            }
            rw(fs, io.size());
            for (const auto& kv: io) {
                rw(fs, kv.first);
                rw(fs, (variable_index_type) kv.second);
            }
            rw(fs, subgraphs.size());
            for (const auto& ptr: subgraphs) {
                rw(fs, !!ptr);
                if (!!ptr) { ptr->file_io(fs); }
            }
            rw(fs, tables.size());
            for (const auto& ptr: tables) {
                rw(fs, !!ptr);
                if (!!ptr) { rw(fs, *ptr); }
            }
            rw(fs, compute_state_ops.size());
            for (auto& cso: compute_state_ops) {
                cso.file_io(fs, this);
            }
        }

    void
        io_type_subgraphs_tables(ifile& fs)
        {
            size_t size;
            bool b;
            rw_comb<int, bn_label_type> rw;
            rw(fs, size); type.clear(); type.resize(size);
            for (auto& t: type) {
                int i;
                rw(fs, i);
                t = (node_type) i;
            }
            rw(fs, size);
            for (; size; --size) {
                variable_index_type k, v;
                rw(fs, k);
                rw(fs, v);
                io[k] = (VariableIO) v;
            }
            rw(fs, size); subgraphs.clear(); subgraphs.resize(size);
            for (auto& ptr: subgraphs) {
                rw(fs, b);
                if (b) {
                    ptr = std::make_shared<graph_type>();
                    ptr->file_io(fs);
                    ptr->parent = this;
                }
            }
            rw(fs, size); tables.clear(); tables.resize(size);
            for (auto& ptr: tables) {
                rw(fs, b);
                if (b) {
                    ptr = std::make_shared<message_type>();
                    rw(fs, *ptr);
                }
            }
            rw(fs, size); compute_state_ops.clear(); compute_state_ops.resize(size);
            for (auto& cso: compute_state_ops) {
                cso.file_io(fs, this);
            }
        }

    template <typename STREAM_TYPE>
        void
        file_io(STREAM_TYPE& fs)
        {
            rw_comb<int, bn_label_type> rw;
            if (rw.fourcc(fs, "GRPH")) { return; }
            rw(fs, rank);
            rw(fs, represented_by);
            /*rw(fs, colour);*/
            rw(fs, neighbours_in);
            rw(fs, neighbours_out);
            rw(fs, inner_nodes);
            rw(fs, rules);
            rw(fs, variables);
            rw(fs, annotations);
            rw(fs, interface_to_node);
            rw(fs, node_to_interface);
            rw(fs, index_in_parent);
            rw(fs, aggregate_cycles);
            rw(fs, generate_interfaces);
            rw(fs, n_alleles);
            rw(fs, state);
            rw(fs, domains);
            rw(fs, ancestor_letters);
            if (rw.fourcc(fs, "TBLS")) { return; }
            io_type_subgraphs_tables(fs);
            if (rw.fourcc(fs, "CLR_")) { return; }
            io_colours(fs);
        }

    void
        save(const std::string& filename)
        {
            // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
            ofile ofs(filename, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);
            file_io(ofs);
        }

    void
        load(const std::string& filename)
        {
            // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
            ifile ifs(filename, std::ios_base::in | std::ios_base::binary);
            file_io(ifs); //// file_io does binary I/O operations 
            parent = NULL;
        }

    bool is_aggregate(node_index_type node) const { return type[node] == Aggregate/*inner_nodes[node].size() > 1*/; }

    bool
        is_compound_interface(node_index_type node) const
        {
            if (is_aggregate(node)) {
                for (node_index_type n: inner_nodes[node]) {
                    if (type[n] != Interface) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

    bool
        is_interface(node_index_type node) const
        {
            if (is_aggregate(node)) {
                for (node_index_type n: inner_nodes[node]) {
                    if (type[n] != Interface) {
                        return false;
                    }
                }
                return true;
            }
            return type[node] == Interface;
        }

    bool
        is_computable(node_index_type node) const
        {
            return !is_interface(node);
        }

    size_t size() const { return rank.size(); }

    node_vec
        active_nodes() const
        {
            node_vec ret;
            ret.reserve(represented_by.size());
            for (node_index_type i = 1; i < represented_by.size(); ++i) {
                if (represented_by[i] == i) {
                    ret.push_back(i);
                }
            }
            return ret;
        }

    node_vec
        resolve_vector(const node_vec& vec) const
        {
            node_vec ret;
            ret.reserve(vec.size());
            for (node_index_type i: vec) {
                node_index_type r = resolve(i);
                if (r > 0) {
                    ret.push_back(r);
                }
            }
            sort_and_unique(ret);
            return ret;
        }

    var_vec
        interface_nodes(var_vec inputs) const
        {
            var_vec ret;
            for (variable_index_type v: inputs) {
                ret.push_back(resolve(interface_to_node.find(v)->second));
            }
            sort_and_unique(ret);
            return ret;
        }

    std::vector<edge_type>
        active_edges() const
        {
            std::vector<edge_type> ret;
            for (node_index_type n: active_nodes()) {
                for (node_index_type o: nei_out(n)) {
                    ret.emplace_back(this, n, o);
                }
            }
            return ret;
        }

    node_vec
        nei_in(node_index_type n) const
        {
            return resolve_vector(neighbours_in[n]);
        }

    node_vec
        nei_out(node_index_type n) const
        {
            return resolve_vector(neighbours_out[n]);
        }

    node_vec
        all_nei(node_index_type n) const
        {
            return nei_in(n) + nei_out(n);
        }

    void
        remove_link(node_index_type nin, node_index_type nout)
        {
            neighbours_out[nin] = resolve_vector(neighbours_out[nin]) - node_vec{nout};
            neighbours_in[nout] = resolve_vector(neighbours_in[nout]) - node_vec{nin};
        }

    void
        dump_node(node_index_type n)
        {
            MSG_DEBUG(
                   '[' << rank[n] << "] " << (is_interface(n) ? "INTERFACE " : (n == inner_nodes[n][0] ? "FACTOR " : "AGGREGATE ")) << n << std::endl
                << "  creation rule " << rules[n] << std::endl
                << "  represented by " << represented_by[n] << " (" << resolve(n) << ')' << std::endl
                << "  colour " << get_colour_impl(colour[n]) << std::endl
                << "  inputs " << neighbours_in[n] << " (" << resolve_vector(neighbours_in[n]) << ')' << std::endl
                << "  outputs " << neighbours_out[n] << " (" << resolve_vector(neighbours_out[n]) << ')' << std::endl
                << "  inner nodes " << inner_nodes[n] << std::endl
                << "  variable(s) " << variables_of(n) << std::endl
                );
            if (node_domains.size() == rank.size()) {
                MSG_DEBUG("  TABLE " << node_domains[n] << std::endl);
            }
            if (inner_nodes[n].size() > 1) {
#if 0
                for (node_index_type i: inner_nodes[n]) {
                    /*var_vec remaining, imported;*/
                    /*restrict_inputs(rules[i], inner_nodes[n], remaining, imported);*/
                    /*MSG_DEBUG("  * rule for " << i << ": " << remaining << " / " << imported);*/
                    MSG_DEBUG("  * rule for " << i << ": " << rules[i]);
                }
#else
                if (subgraphs[n]) {
                    scoped_indent _("  | ");
                    subgraphs[n]->dump_active();
                }
#endif
            }
            MSG_DEBUG("");
        }

#define DUMP_SZ(_x) << " * " #_x " " << _x.size() << std::endl

    void
        dump_sizes() const
        {
            MSG_DEBUG(""
                DUMP_SZ(rank)
                DUMP_SZ(type)
                DUMP_SZ(rules)
                DUMP_SZ(colour)
                DUMP_SZ(variables)
                DUMP_SZ(inner_nodes)
                DUMP_SZ(neighbours_in)
                DUMP_SZ(neighbours_out)
                DUMP_SZ(represented_by)
                );
        }

    void
        dump()
        {
            MSG_DEBUG("ALL NODES");
            /*dump_sizes();*/
            for (node_index_type i = 0; i < rank.size(); ++i) {
                dump_node(i);
            }
        }

    void
        dump_active()
        {
            MSG_DEBUG("ACTIVE NODES");
            /*dump_sizes();*/
            for (node_index_type i: active_nodes()) {
                dump_node(i);
            }
        }

    void
        compute_ranks()
        {
            std::vector<bool> visited(rank.size(), false);
            compute_ranks(active_nodes(), visited);
        }

    void
        compute_ranks(const node_vec& nodes, std::vector<bool>& visited)
        {
            for (node_index_type n: nodes) {
                if (visited[n]) { continue; }
                visited[n] = true;
                auto nin = nei_in(n);
                compute_ranks(nin, visited);
                rank[n] = 0;
                if (nin.size()) {
                    for (node_index_type i: nin) {
                        if (rank[n] < rank[i]) {
                            rank[n] = rank[n];
                        }
                    }
                    ++rank[n];
                }
            }
        }

    typedef std::pair<size_t, var_vec> emitter_and_interface_type;
    struct compare_eai {
        bool operator () (const emitter_and_interface_type& e1, const emitter_and_interface_type& e2) const { return e1.first < e2.first || (e1.first == e2.first && e1.second < e2.second); }
    };

    typedef std::map<emitter_and_interface_type, size_t, compare_eai> interface_map_type;

    node_index_type
        create_interface(const var_vec& varset)
        {
            node_index_type i;
            if (varset.size() == 1) {
                i = add_interface(node_vec{}, varset.front());
                colour[i] = create_colour();
            } else {
                node_vec iv;
                iv.reserve(varset.size());
                for (variable_index_type v: varset) {
                    iv.push_back(add_interface(node_vec{}, v));
                }
                i = add_node(node_vec{}, node_vec{}, var_vec{}, create_colour(), Aggregate, iv, -1);
                for (node_index_type ni: iv) {
                    represented_by[ni] = i;
                    colour[ni] = colour[i];
                }
            }
            return i;
        }

    node_index_type
        create_interface_between(node_index_type n1, node_index_type n2)
        {
            auto varset = variables_of(n1) % variables_of(n2);
            node_index_type i = create_interface(varset);
            colour[i] = get_colour_impl(colour[n1]);
            neighbours_in[i].push_back(n1);
            neighbours_out[i].push_back(n2);
            return i;
        }

    void
        rebuild_interface_between(node_index_type n1, node_index_type n2, interface_map_type& interface_map)
        {
#if 0
            auto varset = variables_of(n1) % variables_of(n2);
            /*MSG_DEBUG("edge between factor(graph)s " << n1 << " and " << n2 << " carries variables " << varset);*/
            node_index_type& i = interface_map[{n1, varset}];
            if (i == 0) {
                if (varset.size() == 1) {
                    i = add_interface(node_vec{n1}, varset.front());
                    colour[i] = get_colour_impl(colour[n1]);
                    neighbours_out[i].push_back(n2);
                } else {
                    node_vec iv;
                    iv.reserve(varset.size());
                    for (variable_index_type v: varset) {
                        iv.push_back(add_interface(node_vec{}, v));
                    }
                    i = add_node(node_vec{n1}, node_vec{n2}, var_vec{}, get_colour_impl(colour[n1]), Aggregate, iv, -1);
                    for (node_index_type ni: iv) {
                        represented_by[ni] = i;
                    }
                }
            } else {
                neighbours_out[i].push_back(n2);
            }
#else
            auto varset = variables_of(n1) % variables_of(n2);
            node_index_type& i = interface_map[{n1, varset}];
            if (i == 0) {
                i = create_interface_between(n1, n2);
            } else {
                neighbours_out[i] = neighbours_out[i] + node_vec{n2};
            }
            neighbours_in[n2] = nei_in(n2) + node_vec{i} - nei_in(i);
            neighbours_out[n1] = nei_out(n1) + node_vec{i} - nei_out(i);
            remove_link(n1, n2);
#endif
            /*filter_out_and_replace_by(neighbours_out[n1], node_vec{n2}, i);*/
            /*filter_out_and_replace_by(neighbours_in[n2], node_vec{n1}, i);*/
            /*MSG_DEBUG("REBUILD " << n1 << ' ' << i << ' ' << n2);*/
            /*dump_node(n1);*/
            /*dump_node(i);*/
            /*dump_node(n2);*/
        }

    void
        remove_nei(node_vec& neighbours, node_index_type n)
        {
            node_vec new_nei;
            new_nei.reserve(neighbours.size());
            auto N = resolve(n);
            for (auto x: resolve_vector(neighbours)) {
                if (x != N) {
                    new_nei.push_back(x);
                }
            }
            new_nei.swap(neighbours);
        }

    bool
        suppress_all_cycles()
        {
            bool redo;
            bool redone = false;
            do {
                redo = false;
                for (node_index_type n: active_nodes()) {
#if 0
#else
                    auto nei = nei_in(n);
                    /*MSG_DEBUG("LOOK FOR CYCLES FROM NEIGHBOURS OF " << n << "   " << nei);*/
                    for (auto i = nei.begin(), j = nei.end(); i != j; ++i) {
                        std::list<node_index_type> path;
                        for (auto k = i + 1; k != j; ++k) {
                            path = find_path_between_parents(*i, *k, n);
                            if (0 && path.size()) {
                                /*MSG_DEBUG("FOUND BETWEEN " << (*i) << " AND " << (*k) << ": " << path);*/
                                for (node_index_type x: path) {
                                    /*dump_node(x);*/
                                }
                                break;
                            }
                        }
                        if (path.size()) {
                            MSG_DEBUG("FOUND CYCLE ENDING ON " << n << " === " << path);
                            path.push_back(n);
                            MSG_DEBUG("path to aggregate " << path);
                            aggregate_path(path);
                            redo = true;
                            redone = true;
                            break;
                        }
                    }
#endif
                }
            } while (redo);
            return redone;
        }

    void
        finalize_stage1(bool reconstruct_itf=true)
        {
            /*check_neighbours();*/
            debug_graph("finalize", 10);
            scoped_indent _("[stage1] ");
            /* search for cycles and aggregate them */
            interface_map_type interface_map;
            if (reconstruct_itf) {
                /*dump_active();*/
                auto edges = active_edges();
                for (const auto& e: edges) {
                    if (is_interface(e.first) ^ !is_interface(e.second)) {
                        /* By construction, it's a factor->factor edge, never an interface->interface edge */
                        rebuild_interface_between(e.first, e.second, interface_map);
                    }
                }
            }
            /*check_neighbours();*/
            debug_graph("finalize", 11);
            suppress_all_cycles();
            /*check_neighbours();*/
            debug_graph("finalize", 12);

            if (size()) {
                node_vec A = active_nodes();

                for (node_index_type n: A) {
                    if (is_aggregate(n) && !is_interface(n)) {
                        /*MSG_DEBUG("subgraphs.size() = " << subgraphs.size() << " n = " << n);*/
                        subgraphs[n] = subgraph(n);
                        /* protect all interfaced variables with this level */
                        var_vec var;
                        for (node_index_type ni: nei_in(n)) {
                            auto nv = variables_of(ni);
                            var.insert(var.end(), nv.begin(), nv.end());
                        }
                        for (node_index_type no: nei_out(n)) {
                            auto nv = variables_of(no);
                            var.insert(var.end(), nv.begin(), nv.end());
                        }
                        sort_and_unique(var);
                        for (auto v: var) {
                            subgraphs[n]->io[v] = Input | Output;
                        }
                    }
                    /* then optimize */
                    /*subgraphs[n]->optimize();*/
                }
                debug_graph("finalize", 13);
            }
        }

    void
        finalize_stage2(bool reconstruct_itf)
        {
            if (reconstruct_itf) {
                /* Create interfaces for all variables used in this layer not already represented in a top-level interface */
                var_vec all_variables;
                std::vector<bool> var_represented(1 + *std::max_element(variables.begin(), variables.end()), false);
                node_vec A = active_nodes();
                for (node_index_type n: A) {
                    if (is_interface(n)) {
                        for (variable_index_type v: variables_of(n)) {
                            var_represented[v] = true;
                        }
                    } else if (is_aggregate(n) && nei_in(n).size() == 0) {
                        var_vec vv;
                        for (node_index_type subn: subgraphs[n]->active_nodes()) {
                            if (subgraphs[n]->is_interface(subn) && subgraphs[n]->nei_in(subn).size() == 0) {
                                vv = vv + subgraphs[n]->variables_of(subn);
                            }
                        }
                        if (vv.size()) {
                            node_index_type i = create_interface(vv);
                            neighbours_out[i].push_back(n);
                            neighbours_in[n] = nei_in(n) + node_vec{i};
                            colour[i] = get_colour_impl(colour[n]);
                            for (variable_index_type v: vv) {
                                var_represented[v] = true;
                            }
                        }
                    }
                }
                debug_graph("finalize", 21);

                /* Discover input interfaces in order to merge distinct trees */
                /* Actually we need to discover their neighbours so we can sort-unique them by colours */
                if (parent != NULL) {
                    node_vec itf_nei;
                    for (node_index_type n: A) {
                        if (is_interface(n) && nei_in(n).size() == 0) {
                            itf_nei = itf_nei + nei_out(n);
                        } else if (!is_interface(n)) {
                            for (variable_index_type v: variables_of(n)) {
                                if (var_represented[v]) { continue; }
                                node_index_type i = add_interface(node_vec{n}, v);
                                nei_out(n).push_back(i);
                            }
                        }
                    }
                    std::sort(itf_nei.begin(), itf_nei.end(), [this] (node_index_type a, node_index_type b) { return colour[a] < colour[b]; });
                    itf_nei.erase(std::unique(itf_nei.begin(), itf_nei.end(), [this] (node_index_type a, node_index_type b) { return colour_equal(colour[a], colour[b]); }), itf_nei.end());

                    node_vec itf;
                    node_vec out, inner;
                    for (node_index_type nei: itf_nei) {
                        for (node_index_type n: nei_in(nei)) {
                            if (nei_in(n).size() == 0) {
                                itf.push_back(n);
                                out = out + nei_out(n);
                                inner = inner + inner_nodes[n];
                            }
                        }
                    }

                    /* Merge inputs of distinct trees to make one single tree */
#if 1
                    /*MSG_DEBUG("Input interfaces " << itf);*/
                    /*for (auto i: itf) {*/
                        /*MSG_DEBUG(" * " << i << " has colour " << get_colour_impl(colour[i]));*/
                    /*}*/
                    /*std::sort(itf.begin(), itf.end(), [this] (node_index_type a, node_index_type b) { return colour[a] < colour[b]; });*/
                    /*itf.erase(std::unique(itf.begin(), itf.end(), [this] (node_index_type a, node_index_type b) { return colour_equal(colour[a], colour[b]); }), itf.end());*/
                    /*MSG_DEBUG("Colour unique input interfaces " << itf);*/
                    /*for (auto i: itf) {*/
                        /*MSG_DEBUG(" * " << i << " has colour " << get_colour_impl(colour[i]));*/
                    /*}*/
                    if (itf.size() > 1) {
                        /* FIXME merge interface IIF factor colours are different. Otherwise this creates a cycle. */
                        node_index_type agr = add_node(node_vec{}, out, var_vec{}, create_colour(), Aggregate, inner, -1);
                        node_vec va = {agr};
                        for (node_index_type i: itf) {
                            represented_by[i] = agr;
                        }
                        for (node_index_type n: out) {
                            neighbours_in[n] = nei_in(n) - itf + va;
                            if (!colour_equal(colour[n], colour[agr])) {
                                assign_colour_impl(colour[n], colour[agr]);
                            }
                        }
                    }
                    debug_graph("finalize", 22);
#endif
                } else {
                    for (node_index_type n: A) {
                        if (!is_interface(n)) {
                            var_vec all_output;
                            for (node_index_type o: nei_out(n)) {
                                all_output = all_output + variables_of(o);
                            }
                            for (variable_index_type v: variables_of(n)) {
                                if (var_represented[v] || std::find(all_output.begin(), all_output.end(), v) == all_output.end()) { continue; }
                                node_index_type i = add_interface(node_vec{n}, v);
                                neighbours_out[n].push_back(i);
                                colour[i] = colour[n];
                            }
                        }
                    }
                    debug_graph("finalize", 23);
                }
            }
            /* unbox single-factor layers */
            for (node_index_type n: active_nodes()) {
                for (node_index_type o: nei_out(n)) {
                    if (!colour_equal(colour[n], colour[o])) {
                        assign_colour_impl(colour[o], colour[n]);
                    }
                }
            }
#if 0
            for (node_index_type n: active_nodes()) {
                if (type[n] == Aggregate && !is_interface(n)) {
                    size_t count_factors = 0;
                    node_index_type factor_node = (node_index_type) -1;
                    for (node_index_type i: inner_nodes[n]) {
                        if (type[i] == Factor) {
                            ++count_factors;
                            factor_node = i;
                        }
                    }
                    MSG_DEBUG("Aggregate #" << n << " has " << count_factors << " factors");
                    if (count_factors == 1) {
                        represented_by[factor_node] = factor_node;
                        neighbours_in[factor_node] = nei_in(n);
                        neighbours_out[factor_node] = nei_out(n);
                        represented_by[n] = factor_node;
                        subgraphs[n].reset();
                    }
                }
            }
#endif
        }

    void
        finalize_import_inputs()
        {
            for (node_index_type n: active_nodes()) {
                if (is_aggregate(n) && !is_interface(n)) {
                    std::vector<var_vec> inputs;
                    for (node_index_type i: nei_in(n)) {
                        inputs.push_back(variables_of(i));
                    }
                }
            }
        }

    void
        finalize(bool reconstruct_itf=true)
        {
            scoped_indent _("[finalize] ");
            finalize_stage1(reconstruct_itf);
            finalize_stage2(reconstruct_itf);
            update_all_ranks();
            /*if (reconstruct_itf) {*/
                /*finalize_import_inputs();*/
            /*}*/
        }

    node_index_type
        add_node(const node_vec& in, const node_vec& out,
                 const var_vec& rule, colour_proxy col, node_type t,
                 const node_vec& inner, variable_index_type var)
        {
            node_index_type ret = rank.size();
            /*MSG_DEBUG("adding node " << ret);*/
            neighbours_out.emplace_back(out);
            neighbours_in.emplace_back(in);
            rules.emplace_back(rule);
            colour.emplace_back(col);
            if (in.size()) {
                size_t r = 0;
                for (node_index_type i: in) {
                    r = std::max(r, rank[i]);
                }
                rank.push_back(r + 1);
            } else {
                rank.push_back(0);
            }
            /*MSG_DEBUG("rank=" << rank.back());*/
            type.push_back(t);
            variables.push_back(var);
            node_variables.emplace_back();
            represented_by.push_back(ret);
            subgraphs.emplace_back();
            tables.emplace_back();
            state.emplace_back();
            annotations.emplace_back();
            is_dh.push_back(false);
            if (inner.size()) {
                inner_nodes.emplace_back(inner);
            } else {
                inner_nodes.emplace_back(node_vec{ret});
            }
            /*dump();*/
            return ret;
        }

    node_index_type
        resolve_interface(variable_index_type var)
        {
            auto it = interface_to_node.find(var);
            if (it == interface_to_node.end()) {
                /*MSG_DEBUG("resolve_interface(" << var << ") => new interface");*/
                if (generate_interfaces) {
                    return add_interface(node_vec{}, var);
                } else {
                    return (node_index_type) -1;
                }
            }
            /*MSG_DEBUG("resolve_interface(" << var << ") => resolve(" << it->second << ") = " << resolve(it->second));*/
            return resolve(it->second);
        }

    node_index_type
        add_interface(const node_vec& producer, variable_index_type var, bool force=false)
        {
            node_index_type ret = add_node(producer, node_vec{}, var_vec{}, create_colour(), Interface, node_vec{}, var);
            if (force || interface_to_node.find(var) == interface_to_node.end()) {
                interface_to_node[var] = ret;
                node_to_interface[ret] = var;
            }
            for (node_index_type p: producer) {
                neighbours_out[p].push_back(ret);
            }
            return ret;
        }

    node_index_type
        add_factor(const var_vec& rule, colour_proxy col,
                 variable_index_type var)
        {
            node_vec in, out;
            if (generate_interfaces) {
                for (variable_index_type v: rule) {
                    in.push_back(resolve_interface(v));
                }
            } else {
                for (variable_index_type v: rule) {
                    auto it = interface_to_node.find(v);
                    if (it != interface_to_node.end()) {
                        in.push_back(resolve(it->second));
                    }
                }
            }
            sort_and_unique(in);
            node_index_type ret = add_node(in, node_vec{}, rule, col, Factor, node_vec{}, var);
            for (node_index_type n: in) {
                neighbours_out[n].push_back(ret);
            }
            if (generate_interfaces) {
                node_index_type i = add_node(node_vec{ret}, node_vec{}, var_vec{}, colour[ret], Interface, node_vec{}, var);
                interface_to_node[var] = i;
                node_to_interface[i] = var;
                neighbours_out[ret].push_back(i);
            } else {
                interface_to_node[var] = ret;
                node_to_interface[ret] = var;
            }
            return ret;
        }

    node_index_type
        add_factor(variable_index_type var)
        {
            /*MSG_DEBUG("add_factor(" << var << ')');*/
            node_index_type ret = add_factor(var_vec{}, create_colour(), var);
            /*dump();*/
            return ret;
        }

    node_index_type
        resolve(node_index_type n) const
        {
            while (n != represented_by[n]) { n = represented_by[n]; }
            return n;
        }

    node_index_type
        add_factor(variable_index_type v1, variable_index_type var)
        {
            /*MSG_DEBUG("add_factor(" << v1 << ", " << var << ')');*/
            node_index_type p1r;
            if (!generate_interfaces) {
                auto it = interface_to_node.find(v1);
                if (it == interface_to_node.end()) {
                    auto ret = add_factor(var);
                    rules.back() = {v1};
                    return ret;
                } else {
                    p1r = resolve(it->second);
                }
            } else {
                p1r = resolve_interface(v1);
            }
            node_index_type ret = add_factor(var_vec{v1}, colour[p1r], var);
            /*compute_ranks();*/
            /*dump();*/
            return ret;
        }

    void
        aggregate_path(std::list<node_index_type>& path)
        {
            parents_of_max_rank aggr_first;
            while (path.size() > 2 && ((aggr_first = find_parents_of_max_rank(path)), !aggregate(*aggr_first.p1, *aggr_first.p2, *aggr_first.child, path, aggr_first.p1))) {
                path.erase(aggr_first.p1);
                path.erase(aggr_first.p2);
                path.erase(aggr_first.child);
            }
        }

    node_index_type
        add_factor(variable_index_type v1,variable_index_type v2, variable_index_type var)
        {
            /*MSG_DEBUG("add_factor(" << v1 << ", " << v2 << ", " << var << ')');*/
            node_index_type p1r, p2r;
            if (!generate_interfaces) {
                auto it = interface_to_node.find(v1);
                if (it == interface_to_node.end()) {
                    node_index_type ret = add_factor(v2, var);
                    rules.back() = {v1, v2};
                    return ret;
                } else {
                    p1r = resolve(it->second);
                }
                it = interface_to_node.find(v2);
                if (it == interface_to_node.end()) {
                    node_index_type ret = add_factor(v1, var);
                    rules.back() = {v1, v2};
                    return ret;
                } else {
                    p2r = resolve(it->second);
                }
            } else {
                /* first, search for a factor/aggregate containing both parents */
                var_vec sorted_rule = v1 < v2 ? var_vec{v1, v2} : var_vec{v2, v1};
                bool found = false;
                node_index_type common = 0;
                for (auto n: active_nodes()) {
                    if (type[n] != Factor) {
                        continue;
                    }
                    auto varset = variables_of(n);
                    auto result = sorted_rule % varset;
                    /*MSG_DEBUG("searching for rule " << sorted_rule << " in #" << n << ' ' << varset << " => " << result);*/
                    if (result == sorted_rule) {
                        found = true;
                        common = n;
                        break;
                    }
                }
                if (found) {
                    if (generate_interfaces) {
                        found = false;
                        node_index_type agr;
                        for (auto n: nei_out(common)) {
                            if (sorted_rule % variables_of(n) == sorted_rule) {
                                agr = n;
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            node_vec common_fac = {common};
                            node_index_type i1, i2;
                            i1 = add_interface(common_fac, v1);
                            i2 = add_interface(common_fac, v2);
                            agr = add_node(common_fac, node_vec{}, var_vec{}, colour[common_fac.front()], Aggregate, node_vec{i1, i2}, -1);
                            represented_by[i1] = represented_by[i2] = agr;
                        }
                        p1r = p2r = agr;
                    } else {
                        p1r = p2r = common;
                    }
                } else {
                    p1r = resolve_interface(v1);
                    p2r = resolve_interface(v2);
                }

                /*p1r = resolve_interface(v1);*/
                /*p2r = resolve_interface(v2);*/
            }

            if (p1r > p2r) {
                p1r ^= p2r;
                p2r ^= p1r;
                p1r ^= p2r;
            }

            node_index_type ret;
            
            if (p1r != (node_index_type) -1) {
                ret = add_factor(var_vec{v1, v2}, colour[p1r], var);

                bool cycle = false;
                if (p1r != p2r) {
                    if (colour_equal(colour[p1r], colour[p2r])) {
                        /* cycle! */
                        /*MSG_DEBUG("cycle!");*/
                        cycle = true;
                    } else {
                        /*MSG_DEBUG("no cycle.");*/
                        assign_colour_impl(colour[p1r], colour[p2r]);
                    }
                }

                if (cycle && aggregate_cycles) {
                    /*MSG_DEBUG("search a path between " << p1r << " and " << p2r);*/
                    std::vector<bool> visited(rank.size(), false);
                    visited[ret] = true;
                    auto path = find_shortest_path(node_vec{p1r, p2r},
                            [&, this](node_index_type n) { return nei_in(n) + nei_out(n); },
                            [&] (node_index_type n) { return visited[n]; },
                            [&] (node_index_type n) { visited[n] = true; });
                    path.push_back(ret);
                    /*MSG_DEBUG("New algo path: " << path);*/
                    path = find_path_between_parents(p1r, p2r, ret);
                    /*MSG_DEBUG("Old algo path: " << path);*/
                    /*dump_active();*/
                    /*MSG_DEBUG("Found path: "; for (size_t n: path) { std::cout << ' ' << n; } std::cout);*/
                    /*size_t min_rank = 1 + rank[*std::min_element(path.begin(), path.end(), [this](node_index_type i1, node_index_type i2) { return rank[i1] < rank[i2]; })];*/
                    aggregate_path(path);
                }
            } else {
                ret = add_factor(var_vec{v1, v2}, create_colour(), var);
            }

            /*compute_ranks();*/
            /*dump();*/
            return ret;
        }

    void
        add_ancestor(variable_index_type id)
        {
            auto& domain = domains[{id}];
            char letter = ancestor_letters.size() + 'a';
            ancestor_letters[id] = letter;
            for (char al = 0; al < (char) n_alleles; ++al) {
                domain.m_combination.emplace_back(genotype_comb_type::element_type{{{id, {letter, letter, al, al}}}, 1.});
            }
            /*MSG_DEBUG("Domains " << domains);*/
        }

    void
        compute_labels(genotype_comb_type& table, variable_index_type spawnling)
        {
            auto& domain = domains[{spawnling}];
            domain.m_combination.clear();
            domain.m_combination.reserve(table.m_combination.size());
            for (auto& state: table) {
                auto& keys = state.keys.keys;
                bn_label_type G = keys.front().state;
                const auto& p1 = keys[G.first_allele];
                const auto& p2 = keys[G.second_allele];
                bool f1 = G.first == GAMETE_L;
                bool f2 = G.second == GAMETE_L;
                /*MSG_DEBUG("keys " << keys << " G " << G << " p1 " << p1 << " p2 " << p2 << " f1 " << f1 << " f2 " << f2);*/
                genotype_comb_type::key_type key = {
                    spawnling, {f1 ? p1.state.first : p1.state.second,
                                f2 ? p2.state.first : p2.state.second,
                                f1 ? p1.state.first_allele : p1.state.second_allele,
                                f2 ? p2.state.first_allele : p2.state.second_allele}
                };
                domain.m_combination.emplace_back(genotype_comb_type::element_type{{key}, 1.});
                keys.emplace_back(key);
            }
            sort_and_unique(domain.m_combination);
        }

    std::shared_ptr<message_type>
        compute_factor_table(variable_index_type spawnling, const var_vec& parents, bool dh)
        {
            return std::make_shared<message_type>(message_type(1));
            /*scoped_indent _(SPELL_STRING("[compute_factor_table " << parents << " -> " << spawnling << "] "));*/
            /*return std::make_shared<message_type>();*/
            var_vec sorted_parents(parents);
            std::sort(sorted_parents.begin(), sorted_parents.end());
            auto& jpar_dom = joint_parent_domains[sorted_parents];
            if (!jpar_dom.size()) {
                auto op = build_query_operation(sorted_parents);
                std::vector<std::shared_ptr<message_type>> storage;  /* maintain shared pointers alive */
                joint_variable_product_type jvp;
                for (const auto& msg: extract(op)) {
                    /*accumulate(jpar_dom, *msg, domains);*/
                    storage.push_back(msg);
                    /*MSG_DEBUG("JPAR MSG " << (*msg));*/
                    MSG_QUEUE_FLUSH();
                    for (const auto& t: *msg) {
                        if (t.size()) {
                            jvp.add_table(t);
                        }
                    }
                }
                jvp.set_output(sorted_parents);
                jvp.compile(domains);
                jpar_dom = jvp.compute();
                for (auto& e: jpar_dom.m_combination) {
                    e.coef = 1;
                }
            }
            /*MSG_DEBUG("jpar_dom " << jpar_dom);*/
            /*MSG_QUEUE_FLUSH();*/
            auto ret = std::make_shared<message_type>();
            auto fac = generate_factor(parents, dh, spawnling, jpar_dom, domains);
            ret->emplace_back(fac);
            /*MSG_DEBUG("Generated factor #" << fac.size());*/
            /*MSG_QUEUE_FLUSH();*/
            extract_domain(ret->back(), {spawnling}, domains);
            return ret;
#if 0
            MSG_DEBUG("Computed parent probability table " << jpar_dom);

            std::vector<bn_label_type> label_g;
            if (parents.size() == 2) {
                label_g = {
                    {GAMETE_L, GAMETE_L, 1, 2},
                    {GAMETE_L, GAMETE_R, 1, 2},
                    {GAMETE_R, GAMETE_L, 1, 2},
                    {GAMETE_R, GAMETE_R, 1, 2}
                };
            } else if (dh) {
                label_g = {
                    {GAMETE_L, GAMETE_L, 1, 1},
                    {GAMETE_R, GAMETE_L, 1, 1},
                };
            } else {
                label_g = {
                    {GAMETE_L, GAMETE_L, 1, 1},
                    {GAMETE_L, GAMETE_R, 1, 1},
                    {GAMETE_R, GAMETE_L, 1, 1},
                    {GAMETE_R, GAMETE_R, 1, 1}
                };
            }

            genotype_comb_type
                G = state_to_combination(-1, label_g) * (1. / label_g.size());

            auto jpar = kronecker(jpar_dom, G);
            MSG_DEBUG("Computed parent probability table with gametes " << jpar);
            compute_labels(jpar, spawnling);
            MSG_DEBUG("Computed factor table with gametes " << jpar);
            MSG_DEBUG("Computed domain for #" << spawnling << ": " << domains[{spawnling}]);
            auto ret = std::make_shared<message_type>();
            ret->emplace_back(fold(sum_over(jpar, {-1})));
            std::sort(ret->back().begin(), ret->back().end(),
                    [](const genotype_comb_type::element_type& e1, const genotype_comb_type::element_type& e2) { return e1.keys < e2.keys; });

            {
                auto tmp = ret->back();
                for (variable_index_type v: parents) {
                    MSG_DEBUG("FACTOR TRANSPOSED FOR OUTPUT ON " << v);
                    tmp = express_conditional_probability(tmp, {v});
                    MSG_DEBUG("" << tmp);
                }
                tmp = express_conditional_probability(tmp, {spawnling});
                MSG_DEBUG("SHOULD BE THE ORIGINAL FACTOR NOW WITH OUTPUT ON " << spawnling);
                MSG_DEBUG("" << tmp);
                MSG_DEBUG("" << (tmp == ret->back()));
            }

            return ret;
#endif
        }

    node_index_type
        add_cross(variable_index_type p1, variable_index_type p2, variable_index_type id)
        {
            /*MSG_DEBUG("################ ADD FACTOR " << p1 << " " << p2 << " " << id);*/
            auto factor = compute_factor_table(id, var_vec{p1, p2}, false);
            node_index_type ret = add_factor(p1, p2, id);
            tables[ret] = factor;
            state[ret] = *tables[ret];
            /*MSG_DEBUG("Computed factor for #" << id << ": " << (*tables[ret]));*/
            /*operations.push_back(op);*/
            return ret;
        }

    node_index_type
        add_dh(variable_index_type p1, variable_index_type id)
        {
            auto factor = compute_factor_table(id, var_vec{p1}, true);
            node_index_type ret = add_factor(p1, id);
            tables[ret] = factor;
            state[ret] = *tables[ret];
            is_dh[ret] = true;
            /*MSG_DEBUG("Computed factor for #" << ret << ": " << (*tables[ret]));*/
            /*operations.push_back(op);*/
            return ret;
        }

    node_index_type
        add_selfing(variable_index_type p1, variable_index_type id)
        {
            auto factor = compute_factor_table(id, var_vec{p1}, false);
            node_index_type ret = add_factor(p1, id);
            tables[ret] = factor;
            state[ret] = *tables[ret];
            /*MSG_DEBUG("Computed factor for #" << ret << ": " << (*tables[ret]));*/
            /*operations.push_back(op);*/
            return ret;
        }

    bool
        find_ascending_path(node_index_type p1, node_index_type p2, node_vec& path, std::vector<bool>& visited)
        {
            /*MSG_DEBUG("path from " << p1 << " to " << p2);*/
            if (p1 == p2) {
                return true;
            }
            for (node_index_type i: nei_in(p1)) {
                i = resolve(i);
                if (visited[i]) {
                    return false;
                }
                visited[i] = true;
                if (find_ascending_path(i, p2, path, visited)) {
                    path.push_back(i);
                    return true;
                }
            }
            return false;
        }

    std::vector<bool>
        create_visited()
        {
            return std::vector<bool>(rank.size(), false);
        }

    node_vec
        find_aggregate_chain(node_index_type p1, node_index_type p2)
        {
            node_vec path;
            auto vis1 = create_visited(), vis2 = create_visited();

            if (rank[p1] > rank[p2] && find_ascending_path(p1, p2, path, vis1)) {
                /*MSG_DEBUG("found ascending path from " << p1 << " to " << p2);*/
                path.push_back(p1);
            } else if (rank[p2] > rank[p1] && find_ascending_path(p2, p1, path, vis2)) {
                /*MSG_DEBUG("found ascending path from " << p2 << " to " << p1);*/
                path.push_back(p2);
            } else {
                /*MSG_DEBUG("no ascending path between parents");*/
                path = {p1, p2};
            }
            return path;
        }

    bool
        filter_out(node_vec& vec, const node_vec& aggr)
        {
            node_vec tmp(vec.size());
            auto it = std::set_difference(vec.begin(), vec.end(), aggr.begin(), aggr.end(), tmp.begin());
            tmp.resize(it - tmp.begin());
            tmp.swap(vec);
            return tmp.size() != vec.size();
        }

    void
        filter_out_and_replace_by(node_vec& vec, const node_vec& aggr, node_index_type new_node)
        {
            /*MSG_DEBUG("filter_out_and_replace_by(" << vec << ", " << aggr << ", " << new_node << ')');*/
            /*if (filter_out(vec, aggr)) {*/
                /*vec.push_back(new_node);*/
            /*}*/
            node_vec tmp;
            tmp.reserve(vec.size());
            for (auto n: vec) {
                node_index_type nr = resolve(n);
                /*MSG_DEBUG("" << nr << " vs " << aggr);*/
                if (std::find(aggr.begin(), aggr.end(), nr) == aggr.end()) {
                    tmp.push_back(n);
                }
            }
            sort_and_unique(tmp);
            if (tmp.size() != vec.size()) {
                tmp.push_back(new_node);
            }
            vec.swap(tmp);
            /*MSG_DEBUG(" => " << vec);*/
        }

    bool
        update_rank(node_index_type node)
        {
            node_index_type r = 0;
            if (nei_in(node).size()) {
                for (node_index_type i: nei_in(node)) {
                    if (rank[i] > r) {
                        r = rank[i];
                    }
                }
                ++r;
            }
            if (rank[node] != r) {
                rank[node] = r;
                return true;
            }
            return false;
        }

    void
        propagate_update_rank(node_index_type source)
        {
            auto nout = nei_out(source);
            std::deque<node_index_type> stack(nout.begin(), nout.end());
            while (stack.size()) {
                node_index_type n = stack.front();
                stack.pop_front();
                if (update_rank(n)) {
                    nout = nei_out(n);
                    stack.insert(stack.end(), nout.begin(), nout.end());
                }
            }
        }

    void
        update_all_ranks()
        {
            std::deque<node_index_type> stack;
            std::vector<bool> visited(rank.size(), false);
            for (node_index_type n: active_nodes()) {
                if (nei_in(n).size() == 0) {
                    rank[n] = 0;
                    auto out = nei_out(n);
                    stack.insert(stack.end(), out.begin(), out.end());
                    visited[n] = true;
                }
            }
            while (stack.size()) {
                node_index_type n = stack.front();
                stack.pop_front();
                if (!visited[n]) {
                    update_rank(n);
                    auto out = nei_out(n);
                    stack.insert(stack.end(), out.begin(), out.end());
                    visited[n] = true;
                }
            }
        }

    void
        check_neighbours()
        {
            for (node_index_type i : active_nodes()) {
                for (node_index_type n: nei_in(i)) {
                    if (represented_by[n] != n) {
                        MSG_DEBUG("error neighbour_in[" << i << "] " << n << " is represented by " << represented_by[n]);
                    }
                    auto tmp = nei_out(n);
                    if (std::find(tmp.begin(), tmp.end(), i) == tmp.end()) {
                        MSG_DEBUG("error neighbour_in[" << i << "] " << n << " doesn't have " << i << " as a neighbour_out " << tmp);
                    }
                }
                for (node_index_type n: nei_out(i)) {
                    if (represented_by[n] != n) {
                        MSG_DEBUG("error neighbour_out[" << i << "] " << n << " is represented by " << represented_by[n]);
                    }
                    auto tmp = nei_in(n);
                    if (std::find(tmp.begin(), tmp.end(), i) == tmp.end()) {
                        MSG_DEBUG("error neighbour_out[" << i << "] " << n << " doesn't have " << i << " as a neighbour_in " << tmp);
                    }
                }
            }
        }

    bool
        recursive_path_finder(node_index_type node, node_index_type goal, std::list<node_index_type>& path, std::vector<bool>& visited) const
        {
            /*MSG_DEBUG("recursive_path_finder(" << node << ", " << goal << ')');*/
            if (node == goal) {
                return true;
            }
            visited[node] = true;
            for (node_index_type n: nei_in(node)) {
                if (!visited[n] && recursive_path_finder(n, goal, path, visited)) {
                    path.push_back(n);
                    return true;
                }
            }
            for (node_index_type n: nei_out(node)) {
                if (!visited[n] && recursive_path_finder(n, goal, path, visited)) {
                    path.push_back(n);
                    return true;
                }
            }
            return false;
        }

    bool
        recursive_path_finder_a(node_index_type node, node_index_type goal, std::list<node_index_type>& path, std::vector<bool>& visited) const
        {
            /*MSG_DEBUG("recursive_path_finder(" << node << ", " << goal << ')');*/
            if (node == goal) {
                return true;
            }
            visited[node] = true;
            for (node_index_type n: nei_in(node)) {
                if (!visited[n] && (recursive_path_finder_a(n, goal, path, visited) || recursive_path_finder_d(n, goal, path, visited))) {
                    path.push_back(n);
                    return true;
                }
            }
            return false;
        }

    bool
        recursive_path_finder_d(node_index_type node, node_index_type goal, std::list<node_index_type>& path, std::vector<bool>& visited) const
        {
            /*MSG_DEBUG("recursive_path_finder(" << node << ", " << goal << ')');*/
            if (node == goal) {
                return true;
            }
            visited[node] = true;
            for (node_index_type n: nei_out(node)) {
                if (!visited[n] && recursive_path_finder_d(n, goal, path, visited)) {
                    path.push_back(n);
                    return true;
                }
            }
            return false;
        }

    std::list<node_index_type>
        find_path_between_parents(node_index_type p1, node_index_type p2, node_index_type child)
        {
            std::vector<bool> visited(rank.size(), false);
            visited[child] = true;
            std::list<node_index_type> path;
            if (recursive_path_finder(p1, p2, path, visited)) {
                path.push_back(p1);
                path.push_back(child);
            }
            return path;
        }

    template <typename GET_NEIGHBOURS, typename VISITED, typename VISIT>
    std::list<node_index_type>
        find_shortest_path(const node_vec& between, GET_NEIGHBOURS&& get_neighbours, VISITED&& visited, VISIT&& visit)
        {
            /*static constexpr node_index_type msb1 = 1 << (8 * sizeof(node_index_type) - 1);*/
            /*static constexpr node_index_type msb2 = msb1 >> 1;*/
            /*static constexpr node_index_type uninitialized = msb1;*/
            /*static constexpr node_index_type starting_point = msb2;*/
#define all_bits ((node_index_type) -1)
#define uninitialized (all_bits ^ (all_bits >> 1))
#define starting_point (uninitialized >> 1)
#define node_is_parent(_n) (from[_n] & starting_point)
#define node_is_initialized(_n) (!(from[_n] & uninitialized))
            /*scoped_indent _(SPELL_STRING("[breadth-first " << between << "] "));*/
            /*MSG_DEBUG("uninitialized " << uninitialized << " starting_point " << starting_point);*/
            node_vec from(rank.size(), uninitialized);
            std::deque<node_index_type> stack;
            for (node_index_type n: between) {
                /*stack.insert(stack.end(), direction[n].begin(), direction[n].end());*/
                stack.push_back(n);
                from[n] = starting_point;
            }
            if (0) {
                std::stringstream ss;
                ss << "from";
                size_t i = 0;
                for (node_index_type f: from) {
                    ss << ' ' << i << ':';
                    if (node_is_parent(f)) { ss << 'P'; } else if (node_is_initialized(i)) { ss << f; } else { ss << '-'; }
                }
                MSG_DEBUG(ss.str());
            }
            while (stack.size()) {
                node_index_type this_node = stack.front();
                stack.pop_front();
                if (visited(this_node)) {
                    continue;
                }
                visit(this_node);
                auto neighbours = get_neighbours(this_node);
                if (0) {
                    std::stringstream ss;
                    ss << "ON " << this_node << ':';
                    if (node_is_parent(this_node)) { ss << 'P'; } else if (node_is_initialized(this_node)) { ss << from[this_node]; } else { ss << '-'; }
                    ss << " neighbours " << neighbours;
                    MSG_DEBUG(ss.str());
                }
                for (node_index_type n: neighbours) {
                    if (visited(n)) {
                        continue;
                    }
                    /*MSG_DEBUG("neighbour " << n);*/
                    if (node_is_parent(n)) {
                        std::list<node_index_type> ret;
                        ret.push_back(n);
                        node_index_type p = this_node;
                        while (!node_is_parent(p)) {
                            ret.push_back(p);
                            /*MSG_DEBUG("building path (1) " << ret);*/
                            p = from[p];
                        }
                        /*MSG_DEBUG("end branch");*/
                        ret.push_back(p);
                        /*MSG_DEBUG("complete path " << ret);*/
                        return ret;
                    } else if (node_is_initialized(n)) {
                        std::list<node_index_type> ret;
                        /* compute path and return */
                        node_index_type p = n;
                        while (!node_is_parent(p)) {
                            ret.push_back(p);
                            /*MSG_DEBUG("building path (1) " << ret);*/
                            p = from[p];
                        }
                        ret.push_back(p);
                        /*MSG_DEBUG("building path (2) " << ret);*/
                        p = this_node;
                        while (!node_is_parent(p)) {
                            ret.push_front(p);
                            /*MSG_DEBUG("building path (3) " << ret);*/
                            p = from[p];
                        }
                        ret.push_front(p);

                        /*MSG_DEBUG("complete path " << ret);*/
                        return ret;
                    }
                    stack.push_back(n);
                    from[n] = this_node;
                }
                if (0) {
                    std::stringstream ss;
                    ss << "from";
                    size_t i = 0;
                    for (node_index_type f: from) {
                        ss << ' ' << i << ':';
                        if (node_is_parent(i)) { ss << 'P'; } else if (node_is_initialized(i)) { ss << f; } else { ss << '-'; }
                        ++i;
                    }
                    MSG_DEBUG(ss.str());
                }
            }
#undef node_is_parent
#undef node_is_initialized
#undef uninitialized
#undef starting_point
            /*MSG_DEBUG("did not find v-path");*/
            return {};
        }

    std::list<node_index_type>
        find_path(node_index_type p1, node_index_type p2) const
        {
            /*scoped_indent _(SPELL_STRING("[find_path " << p1 << ' ' << p2 << "] "));*/
            std::vector<bool> visited(rank.size(), false);
            std::list<node_index_type> path;
            if (recursive_path_finder(p1, p2, path, visited)) {
                path.push_back(p1);
            }
            /*MSG_DEBUG("PATH " << path);*/
            return path;
        }

    node_vec
        find_all_ancestors(node_index_type p, const std::vector<bool>& other_branch, std::vector<bool>& visited) const
        {
            node_vec ret;
            ret.reserve(rank.size());
            node_vec stack(nei_in(p));
            while (stack.size()) {
                node_index_type n = stack.back();
                stack.pop_back();
                if (!visited[n]) {
                    ret.push_back(n);
                    visited[n] = true;
                    if (other_branch[n]) {
                        return ret;
                    }
                    auto nei = nei_in(n);
                    stack.insert(stack.end(), nei.begin(), nei.end());
                }
            }
            return ret;
        }

    node_vec
        find_all_descendants(node_index_type p, const std::vector<bool>& other_branch, std::vector<bool>& visited) const
        {
            node_vec ret;
            ret.reserve(rank.size());
            node_vec stack(nei_out(p));
            while (stack.size()) {
                node_index_type n = stack.back();
                stack.pop_back();
                if (!visited[n]) {
                    ret.push_back(n);
                    visited[n] = true;
                    if (other_branch[n]) {
                        return ret;
                    }
                    auto nei = nei_out(n);
                    stack.insert(stack.end(), nei.begin(), nei.end());
                }
            }
            return ret;
        }

    std::list<node_index_type>
        find_path1(node_index_type p1, node_index_type p2) const
        {
            /*scoped_indent _(SPELL_STRING("[find_path " << p1 << ' ' << p2 << "] "));*/
            std::vector<bool> anc_visited1(rank.size(), false), anc_visited2(rank.size(), false);
            auto a1 = find_all_ancestors(p1, anc_visited2, anc_visited1);
            auto a2 = find_all_ancestors(p2, anc_visited1, anc_visited2);
            /*MSG_DEBUG("a1 " << a1);*/
            /*MSG_DEBUG("a2 " << a2);*/
            auto common = a1 % a2;
            /*MSG_DEBUG("common " << common);*/
            std::list<node_index_type> path;
            if (common.size()) {
                std::vector<bool> visited(rank.size(), true);
                std::vector<bool> targets(rank.size(), true);
                for (node_index_type n: a1 + a2) {
                    visited[n] = false;
                }
                for (node_index_type n: common) {
                    targets[n] = true;
                }
                recursive_path_finder(common.front(), p2, path, visited);
                recursive_path_finder(p1, common.front(), path, visited);
                path.push_back(p1);
                /*MSG_DEBUG("PATH " << path);*/
            }
            return path;
        }

    std::list<node_index_type>
        find_path2(node_index_type p1, node_index_type p2) const
        {
            /*scoped_indent _(SPELL_STRING("[find_path " << p1 << ' ' << p2 << "] "));*/
            std::vector<bool> visited(rank.size(), false);
            std::list<node_index_type> path;
            if (recursive_path_finder_a(p1, p2, path, visited) || recursive_path_finder_d(p1, p2, path, visited)) {
                path.push_back(p1);
            }
            /*MSG_DEBUG("PATH " << path);*/
            return path;
        }

    struct parents_of_max_rank {
        std::list<node_index_type>::iterator p1, child, p2;
    };

    parents_of_max_rank
        find_parents_of_max_rank(std::list<node_index_type>& path)
        {
            auto child = std::max_element(path.begin(), path.end(), [this](node_index_type n1, node_index_type n2) { return rank[n1] < rank[n2]; });
            auto p1 = child, p2 = child;
            if (child == path.begin()) {
                p1 = path.end();
            }
            --p1;
            ++p2;
            if (p2 == path.end()) {
                p2 = path.begin();
            }
            return {p1, child, p2};
        }

    const var_vec&
        variables_of(node_index_type n) const
        {
            var_vec& ret = const_cast<graph_type*>(this)->node_variables[n];
            if (ret.size() == 0) {
                for (node_index_type i: inner_nodes[resolve(n)]) {
                    ret.insert(ret.end(), rules[i].begin(), rules[i].end());
                    if (variables[i] != -1) {
                        ret.push_back(variables[i]);
                    }
                    ret.insert(ret.end(), annotations[i].begin(), annotations[i].end());
                }
                sort_and_unique(ret);
            }
            return node_variables[n];
        }


    bool
        aggregate(node_index_type p1, node_index_type p2, node_index_type new_node, std::list<node_index_type>& path, std::list<node_index_type>::iterator pos)
        {
            p1 = resolve(p1);
            p2 = resolve(p2);
            node_index_type ret = rank.size();

            node_vec aggr = find_aggregate_chain(p1, p2);
            sort_and_unique(aggr);

            node_vec all_nodes, nin, nout;
            for (node_index_type i: aggr) {
                all_nodes = all_nodes + inner_nodes[i];
                nin = nin + nei_in(i);
                nout = nout + nei_out(i);
            }

            nin = nin - all_nodes - aggr;
            nout = nout - all_nodes - aggr;

            if (aggr.size() == 1) {
                ret = aggr.front();
            } else {
                /*MSG_DEBUG("-----------------------------------------------------------------------------");*/
                /*MSG_DEBUG("adding aggregate " << ret);*/
                colour.emplace_back(colour[p1]);
                rank.push_back(1 + std::min(rank[p1], rank[p2]));
                annotations.emplace_back();
                is_dh.push_back(false);
                represented_by.push_back(ret);
                /*MSG_DEBUG("initial aggr " << aggr);*/

                /*MSG_DEBUG("ready to aggregate nodes " << all_nodes << std::endl*/
                          /*<< "             aggr " << aggr << std::endl*/
                          /*<< "    neighbours in " << nin << std::endl*/
                          /*<< "   neighbours out " << nout);*/

                inner_nodes.emplace_back();
                neighbours_in.emplace_back();
                neighbours_out.emplace_back();
                rules.emplace_back();
                subgraphs.emplace_back();
                type.push_back(Aggregate);
                variables.push_back(-1);
                node_variables.emplace_back();
                tables.emplace_back();
                state.emplace_back();

                for (node_index_type i: aggr) {
                    represented_by[i] = ret;
                }
            }

            neighbours_in[new_node] = nei_in(new_node) - aggr + node_vec{ret};

#if 0
            for (node_index_type n: all_nodes) {
                for (node_index_type x: nei_out(n)) {
                    neighbours_in[x] = nei_in(x);
                }
                for (node_index_type x: nei_in(n)) {
                    neighbours_out[x] = nei_out(x);
                }
                neighbours_in[n].clear();
                neighbours_out[n].clear();
            }
#endif

            inner_nodes[ret].swap(all_nodes);
            neighbours_in[ret].swap(nin);
            neighbours_out[ret].swap(nout);

            update_rank(ret);
            propagate_update_rank(ret);

            path.insert(pos, ret);

            /*check_neighbours();*/

            return aggr.size() > 2;
        }


    std::list<node_index_type>
        find_vpath(variable_index_type v1, variable_index_type v2) const
        {
            node_vec nv1, nv2;
            std::list<node_index_type> ret;
            if (v1 > v2) {
                v1 ^= v2;
                v2 ^= v1;
                v1 ^= v2;
            }
            var_vec V = {v1, v2};
            /*MSG_DEBUG("v1 " << v1 << " v2 " << v2);*/
            for (node_index_type n: active_nodes()) {
                if (is_interface(n)) {
                    for (variable_index_type v: V % variables_of(n)) {
                        (v == v1 ? nv1 : nv2).push_back(n);
                    }
                }
            }
            /*MSG_DEBUG("nv1 " << nv1 << " nv2 " << nv2);*/
            auto joint_itf = nv1 % nv2;
            if (joint_itf.size()) {
                return {*std::min_element(joint_itf.begin(), joint_itf.end(), [this](node_index_type n1, node_index_type n2) { return variables_of(n1).size() < variables_of(n2).size(); })};
            }
            size_t path_size = (size_t) -1;
            for (auto n1: nv1) {
                for (auto n2: nv2) {
                    if (!colour_equal(colour[n1], colour[n2])) {
                        return {};
                    }
                    auto path = find_path2(n1, n2);
                    /*MSG_DEBUG("n1 " << n1 << " n2 " << n2 << " path " << path);*/
                    size_t psz = path.size();
                    if (psz && psz < path_size) {
                        path_size = psz;
                        path.swap(ret);
                    }
                }
            }
            return ret;
        }

    node_vec
        var_to_nodes(variable_index_type v) const
        {
            node_vec nv;
            for (node_index_type n: active_nodes()) {
                if (is_interface(n) || type[n] == Aggregate) {
                    auto varz = variables_of(n);
                    if (std::find(varz.begin(), varz.end(), v) != varz.end()) {
                        nv.push_back(n);
                    }
                }
            }
            return nv;
        }

    /*std::map<colour_proxy, std::vector<std::list<node_index_type>>>*/
    std::vector<std::vector<std::list<node_index_type>>>
        find_vpath(var_vec vv) const
        {
            /*std::map<colour_proxy, std::vector<std::list<node_index_type>>> ret;*/
            std::map<variable_index_type, std::vector<node_vec>> nv;
            std::map<colour_proxy, var_vec> by_colour;
            for (auto v: vv) {
                auto nodes = var_to_nodes(v);
                /*MSG_DEBUG("variable " << v << " nodes " << nodes);*/
                /*for (auto n: nodes) {*/
                    /*MSG_DEBUG("  node colour " << get_colour_impl(colour[n]));*/
                /*}*/
                auto col = get_colour_impl(colour[nodes.front()]);
                nv[v].push_back(nodes);
                by_colour[col].push_back(v);
            }
            std::vector<var_vec> cliques;
            std::map<std::pair<variable_index_type, variable_index_type>, std::list<node_index_type>> paths;
            std::vector<std::vector<std::list<node_index_type>>> paths_by_clique;
            std::map<variable_index_type, size_t> var_clique;
            for (auto& kv: by_colour) {
                /*MSG_DEBUG("In component " << kv.second);*/
                if (kv.second.size() > 1) {
                    auto i = kv.second.begin(), j = kv.second.end();
                    for (; i != j; ++i) {
                        if (var_clique.find(*i) == var_clique.end()) {
                            var_clique[*i] = cliques.size();
                            cliques.emplace_back();
                            cliques.back().push_back(*i);
                            paths_by_clique.emplace_back();
                            /*MSG_DEBUG("Variable " << (*i) << " is in a new clique");*/
                        }
                        for (auto k = i + 1; k != j; ++k) {
                            /*MSG_DEBUG("Find path between " << (*i) << " and " << (*k));*/
                            auto path = find_vpath(*i, *k);
                            if (path.size()) {
                                paths[{*i, *k}] = path;
                                paths_by_clique[var_clique[*i]].emplace_back(path);
                                cliques[var_clique[*i]].push_back(*k);
                                var_clique[*k] = var_clique[*i];
                            }
                        }
                        if (paths_by_clique[var_clique[*i]].size() == 0) {
                            paths_by_clique[var_clique[*i]].emplace_back();
                            /*paths_by_clique[var_clique[*i]].back().emplace_back(*i);*/
                            paths_by_clique[var_clique[*i]].back().emplace_back(nv[*i].front().front());
                        }
                    }
                    /*MSG_DEBUG("var_clique " << var_clique);*/
                } else {
                    auto var = kv.second.front();
                    /*MSG_DEBUG("var is single " << var);*/
                    var_clique[var] = cliques.size();
                    cliques.emplace_back();
                    cliques.back().push_back(var);
                    paths_by_clique.emplace_back();
                    paths_by_clique.back().emplace_back();
                    /*paths_by_clique.back().emplace_back(var);*/
                    /*paths_by_clique.back().emplace_back(find_vpath(var, var));*/
                    paths_by_clique.back().back().emplace_back(nv[var].front().front());
                }
            }
            /*MSG_DEBUG("cliques " << cliques);*/
            /*MSG_DEBUG("paths " << paths);*/
            /*MSG_DEBUG("paths_by_clique " << paths_by_clique);*/

            return paths_by_clique;
#if 0
            for (auto& kv: by_colour) {
                if (kv.second.size() > 1) {
                    auto i = kv.second.begin(), j = kv.second.end();
                    for (; i != j; ++i) {
                        for (auto k = i + 1; k != j; ++k) {
                            auto path = find_vpath(*i, *k);
                            if (path.size()) {
                                ret[kv.first].push_back(path);
                            }
                        }
                    }
                } else {
                    MSG_DEBUG("variable " << kv.second.front() << " is lonely " << nv[kv.first].front());
                    ret[kv.first].emplace_back();
                    ret[kv.first].back().push_back(nv[kv.first].front().front());
                }
            }
            return ret;
#endif
        }

    std::shared_ptr<graph_type>
        subgraph(node_index_type index, bool fin=true)
        {
            std::shared_ptr<graph_type> ret = std::make_shared<graph_type>(n_alleles);
            ret->generate_interfaces = generate_interfaces;
            ret->aggregate_cycles = aggregate_cycles;
            ret->parent = this;
            ret->index_in_parent = index;
            var_vec remaining, imported;
            /*MSG_DEBUG("CREATING SUBGRAPH " << inner_nodes[index]);*/
            /*MSG_DEBUG("index " << index);*/
            /*MSG_DEBUG("this = " << ret.get());*/
            /*MSG_DEBUG("parent " << this);*/
            for (node_index_type i: nei_in(index)) {
                if (is_interface(i)) {
                    ret->create_interface(variables_of(i));
                } else {
                    ret->create_interface(variables_of(i) % variables_of(index));
                }
            }
            for (node_index_type i: inner_nodes[index]) {
                const auto& rule = rules[i];
                auto var = variables[i];
                /*MSG_DEBUG("subgraph " << i << "  " << rule << " " << var);*/
                if (type[i] == Factor) {
                    node_index_type n;
                    switch (rule.size()) {
                        case 0: n = ret->add_factor(var);
                                break;
                        case 1: n = ret->add_factor(rule[0], var);
                                break;
                        case 2: n = ret->add_factor(rule[0], rule[1], var);
                                break;
                    };
                    ret->tables[n] = tables[i];
                } else {
                    if (ret->interface_to_node.find(var) == ret->interface_to_node.end()) {
                        ret->add_interface(node_vec{}, var);
                    }
                }
                /*for (variable_index_type v: variables_of(i)) {*/
                    /*auto& domain = ret->domains[{v}];*/
                    /*if (!domain.size()) {*/
                        /*domain = domains[{v}];*/
                    /*}*/
                /*}*/
            }
            if (fin) {
                ret->finalize();
            } else {
                for (node_index_type n: ret->active_nodes()) {
                    if (ret->is_aggregate(n) && !ret->is_interface(n)) {
                        ret->subgraphs[n] = ret->subgraph(n);
                    }
                }
            }
            return ret;
        }

    VariableIO
        node_io(node_index_type n) const
        {
            VariableIO ret = None;
            if (type[n] == Factor) {
                return io.find(variables[n])->second;
            }
            for (variable_index_type v: variables_of(n)) {
                ret |= io.find(v)->second;
            }
            return ret;
        }

    std::string
        to_image(const std::string& prefix, const std::string& format) const
        {
            auto A = active_nodes();
            std::string dotfilename = SPELL_STRING("/tmp/prout" << prefix); //std::tempnam(nullptr);
            std::vector<std::string> sub_filenames;
            {
                std::ofstream dotfile(dotfilename);
                dotfile << "digraph spell {rankdir=LR;" << std::endl;
                if (parent != NULL) {
                    dotfile << "bgcolor=transparent;" << std::endl;
                }
                for (node_index_type n: A) {
                    std::string col;
                    auto io_val = node_io(n);
                    if (io_val == (Input | Output)) {
                        col = ", style=filled, fillcolor=mistyrose2";
                    } else if (io_val == Input) {
                        col = ", style=filled, fillcolor=lightsteelblue1";
                    } else if (io_val == Output) {
                        col = ", style=filled, fillcolor=darkseagreen1";
                    } else {
                        col = ", style=filled, fillcolor=white";
                    }
                    dotfile << n << "[";
                    if (is_interface(n)) {
                        dotfile << "shape=diamond, label=\"" << n  << " [" << inner_nodes[n] << "] " << variables_of(n) << "\"";
                    } else if (is_aggregate(n)) {
                        std::string filename = subgraphs[n]->to_image(SPELL_STRING("_sub_" << prefix << '_' << n), format);
                        dotfile << "shape=box,labeljust=\"l\", labelloc=\"t\", label=\"" << n << " [" << inner_nodes[n] << "]\",image=\"" << filename << "\"";
                        sub_filenames.push_back(filename);
                    } else {
                        dotfile << "shape=box,label=\"" << n << ' ' << variables_of(n) << "\"";
                    }
                    dotfile << col << "];" << std::endl;
                }
                for (node_index_type h: A) {
                    for (node_index_type t: nei_out(h)) {
                        dotfile << h << " -> " << t << std::endl;
                    }
                }
                dotfile << '}' << std::endl;
            }
            std::string ret = SPELL_STRING(prefix << '.' << format);
            std::string cmd = SPELL_STRING("dot -T" << format << ' ' << dotfilename << " -o " << ret);
            if (system(cmd.c_str())) {
                MSG_ERROR("Dot failed " << strerror(errno), "");
            }
            for (const auto& s: sub_filenames) { unlink(s.c_str()); }
            return ret;
        }

    void
        build_compute_message(const edge_type& edge, std::map<edge_type, bool>& visited, std::vector<compute_state_operation_type>& ops)
        {
            MSG_DEBUG("build_compute_message " << edge << " " << visited[edge]);
            MSG_DEBUG_INDENT;
            node_index_type emitter = edge.first, receiver = edge.second;
            /*const graph_type* edge_graph = edge.graph;*/
            auto& v = visited[edge];
            if (!v) {
                visited[edge] = true;
                node_vec incoming = nei_in(emitter) + nei_out(emitter) - node_vec{receiver};
                size_t N = incoming.size();
                edge_type in_msg{this, 0, emitter};
                bool emit_from_itf = is_interface(emitter);
                for (node_index_type emt: incoming) {
                    in_msg.first = emt;
                    /*if (visited[in_msg]) {*/
                    /*ops.emplace_back(*this, PushMessage, emt, in_msg, variables_of(emt));*/
                    /*} else {*/
                    build_compute_message(in_msg, visited, ops);
                    ops.emplace_back(this, PushMessage, emt, in_msg, var_vec{});
                    /*}*/
                    /*ops.emplace_back(*this, PushMessage, emt, in_msg, variables_of(emt));*/
                }
                if (is_aggregate(emitter) && !is_interface(emitter)) {
                    auto varset = variables_of(receiver);
                    ops.emplace_back(this, PushState, emitter, edge, var_vec{});
                    ops.back().op = subgraphs[emitter]->build_query_operation(varset);
                    ops.back().n_nei_sub = N;
                    ++N;
                } else {
                    ops.emplace_back(this, PushState, emitter, edge, var_vec{});
                    ++N;
                }
                if (N > 0 && (emit_from_itf || !is_aggregate(emitter))) {
                    var_vec target_varset = variables_of(emit_from_itf ? emitter : receiver);
                    ops.emplace_back(this, Accumulate, N, edge, std::move(target_varset));
                }
                /*if (!is_aggregate(emitter)) {*/
                    /*var_vec target_varset = variables_of(emit_from_itf ? emitter : receiver);*/
                    /*if (target_varset != variables_of(emitter)) {*/
                        /*ops.emplace_back(this, Project, 1, edge, std::move(target_varset));*/
                    /*}*/
                /*}*/
                ops.emplace_back(this, Store, 1, edge, var_vec{});
            }
            MSG_DEBUG_DEDENT;
        }

    void
        build_compute_graph()
        {
            for (node_index_type n: active_nodes()) {
                if (is_aggregate(n) && !is_interface(n)) {
                    subgraphs[n]->build_compute_graph();
                }
            }

            std::map<edge_type, bool> visited;
            std::vector<edge_type> edges = active_edges();
            for (const edge_type& e: edges) {
                bool& v = visited[e];
                if (!v) {
                    /*build_compute_message(e, visited, compute_state_ops);*/
                }
            }
            for (edge_type& e: edges) {
                std::swap(e.first, e.second);
                bool& v = visited[e];
                if (!v) {
                    /*build_compute_message(e, visited, compute_state_ops);*/
                }
            }
            MSG_DEBUG(compute_state_ops);
        }

    void
        compute_messages(std::vector<std::shared_ptr<message_type>>::const_iterator inputs_begin,
                         std::vector<std::shared_ptr<message_type>>::const_iterator inputs_end,
                         std::map<edge_type, std::shared_ptr<message_type>>& messages)
        {
            /*scoped_indent _(SPELL_STRING('[' << this << " compute_messages] "));*/
            for (node_index_type n: active_nodes()) {
                if (is_interface(n)) {
                    state[n].clear();
                    auto varset = variables_of(n);
                    for (auto i = inputs_begin; i != inputs_end; ++i) {
                        accumulate(state[n], (**i) % varset, domains);
                    }
                    if (state[n].size() == 0) {
                        for (variable_index_type v: varset) {
                            state[n].emplace_back(domains[{v}]);
                        }
                    }
                } else if (!is_aggregate(n)) {
                    state[n] = *tables[n];
                }
            }
            MSG_DEBUG("INITIAL STATE");
            for (node_index_type n: active_nodes()) {
                MSG_DEBUG((is_interface(n) ? 'I' : is_aggregate(n) ? 'A' : 'F') << variables_of(n) << "  " << state[n]);
            }
            MSG_DEBUG("OPERATIONS " << compute_state_ops);

            std::vector<std::shared_ptr<message_type>> stack;
            for (const auto& o: compute_state_ops) { o(messages, stack); }
            MSG_DEBUG("MESSAGES");
            for (const auto& kv: messages) {
                MSG_DEBUG(std::setw(20) << std::left << kv.first << " [" << kv.second << ']');
            }
        }

    void
        compute_state(const std::vector<query_operation_type>& query, std::map<edge_type, std::shared_ptr<message_type>>& messages)
        {
            /*scoped_indent _(SPELL_STRING('[' << this << " compute_state] "));*/
            for (const auto& qlist: query) {
                MSG_DEBUG("query " << qlist);
                if (qlist.size() == 1) {
                    const auto& q = qlist.front();
                    node_vec incoming = nei_in(q.node) + nei_out(q.node);
                    MSG_DEBUG("incoming " << incoming);
                    edge_type e{this, 0, q.node};
                    MSG_DEBUG("initial state " << state[q.node]);
                    for (node_index_type n: incoming) {
                        e.first = n;
                        if (messages[e]) {
                            accumulate(state[q.node], *messages[e], domains);
                        }
                    }
                } else {
                    for (const auto& q: qlist) {
                        if (type[q.node] == Factor) {
                            node_vec incoming = nei_in(q.node) + nei_out(q.node);
                            MSG_DEBUG("incoming " << incoming);
                            state[q.node] = *tables[q.node];
                            MSG_DEBUG("initial state " << state[q.node]);
                            edge_type e{this, 0, q.node};
                            for (node_index_type n: incoming) {
                                e.first = n;
                                accumulate(state[q.node], *messages[e], domains);
                            }
                        }
                    }
                }
            }
        }

    void
        compute_full_factor_state(std::map<edge_type, std::shared_ptr<message_type>>& messages,
                                  std::map<variable_index_type, genotype_comb_type>& output)
        {
            /*scoped_indent _(SPELL_STRING('[' << this << " compute_state] "));*/
            for (node_index_type node: active_nodes()) {
                if (type[node] == Factor) {
                    node_vec incoming = nei_in(node) + nei_out(node);
                    MSG_DEBUG("incoming " << incoming);
                    state[node] = *tables[node];
                    MSG_DEBUG("initial state " << state[node]);
                    edge_type e{this, 0, node};
                    for (node_index_type n: incoming) {
                        e.first = n;
                        accumulate(state[node], *messages[e], domains);
                    }
                    const auto& table = state[node].front();
                    for (variable_index_type v: get_parents(table)) {
                        if (output.find(v) == output.end()) {
                            output[v] = project(table, {v}, {});;
                        }
                    }
                } else if (is_aggregate(node) && !is_interface(node)) {
                    std::vector<std::shared_ptr<message_type>> inputs;
                    node_vec incoming = nei_in(node) + nei_out(node);
                    edge_type e{this, 0, node};
                    for (node_index_type nei: incoming) {
                        e.first = nei;
                        inputs.push_back(messages[e]);
                    }
                    std::map<edge_type, std::shared_ptr<message_type>> sub_messages;
                    subgraphs[node]->compute_messages(inputs.begin(), inputs.end(), sub_messages);
                    subgraphs[node]->compute_full_factor_state(sub_messages, output);
                }
            }
        }

    std::vector<std::shared_ptr<message_type>>
        extract(const std::vector<query_operation_type>& query)
        {
            std::vector<std::shared_ptr<message_type>> ret;
            for (const auto& q: query) {
                ret.emplace_back(extract(q));
                normalize(ret.back());
            }
            return ret;
        }

    /* TODO: refaire les queries en utilisant les opérations de calcul d'état en ne calculant que les messages nécessaires et en conservant les variables de la requête dans les projections */
    std::shared_ptr<message_type>
        extract(const query_operation_type& query)
        {
            MSG_DEBUG("extracting " << query);
            auto ret = std::make_shared<message_type>();
            ret->reserve(query.size());
            std::vector<std::shared_ptr<message_type>> stack;
            std::vector<const var_vec*> var_stack;
            for (const auto& op: query) {
                op(stack, var_stack);
            }
            for (size_t i = 0; i < stack.size(); ++i) {
                if (!(stack[i] && stack[i]->size())) {
                    auto vi = var_stack[i]->begin(), vj = var_stack[i]->end();
                    stack[i] = std::make_shared<message_type>(message_type{domains[{*vi}]});
                    for (++vi; vi != vj; ++vi) {
                        const auto& domain = domains[{*vi}];
                        accumulate(*stack[i], message_type{domain * (1. / domain.size())}, domains);
                    }
                }
            }
            for (const auto& mptr: stack) {
                accumulate(*ret, *mptr, domains);
            }
            return ret;
        }

    var_vec
        rec_tree(const var_vec& q, size_t node, const std::map<size_t, std::vector<size_t>>& neighbours, std::vector<bool>& visited, query_operation_type& ret) const
        {
            static size_t indent = 0;
            visited[node] = true;
            size_t N = 0;
            var_vec propag;
            const auto& this_neighbours = neighbours.find(node)->second;
            for (size_t nei: this_neighbours) {
                if (!visited[nei]) {
                    indent += 3;
                    propag = propag + rec_tree(variables_of(node) + q, nei, neighbours, visited, ret);
                    indent -= 3;
                    ++N; // += !(is_interface(nei) && neighbours.find(nei)->second.size() == 1);
                }
            }
            var_vec var = (variables_of(node) + propag) % q;
            MSG_DEBUG(std::setw(indent) << "" << (is_interface(node) ? " I " : " F ") << node << " [" << N << "] " << variables_of(node) << ' ' << var);
            if (!(N == 1 && ret.back().variables == var)) {
                ret.emplace_back(this, var, node, N);
            }
            return var;
        }


    std::vector<query_operation_type>
        build_query_operation(const var_vec& q)
        {
            /*scoped_indent _(SPELL_STRING("[build_Q " << q << "] "));*/
            std::vector<query_operation_type> ret;

            auto path = find_vpath(q);

            MSG_DEBUG("path " << path);

            for (const auto& clique_paths: path) {
                size_t max_index = 0;
                std::map<size_t, std::vector<size_t>> neighbours;
                for (const auto& p: clique_paths) {
                    MSG_DEBUG("on path " << p);
                    auto i = p.begin(), j = p.end();
                    size_t prev = *i;
                    max_index = std::max(max_index, prev);
                    ++i;
                    if (i == j) {
                        neighbours[prev].push_back(prev);
                    } else {
                        for (; i != j; ++i) {
                            neighbours[prev].push_back(*i);
                            neighbours[*i].push_back(prev);
                            prev = *i;
                            max_index = std::max(max_index, prev);
                        }
                    }
                }

                MSG_DEBUG("neighbours " << neighbours);

                if (neighbours.size() == 0) {
                    return {};
                }

                std::vector<bool> visited(max_index + 1, false);

                node_index_type anchor = (node_index_type) -1;
                size_t max_degree = 0;
                for (auto& kv: neighbours) {
                    sort_and_unique(kv.second);
                    MSG_DEBUG(kv.first << ' ' << kv.second.size());
                    if (kv.second.size() > max_degree) {
                        max_degree = kv.second.size();
                        anchor = kv.first;
                    }
                }

                ret.emplace_back();
                rec_tree(q, anchor, neighbours, visited, ret.back());
                MSG_DEBUG("expression " << ret.back());
            }
            return ret;
        }

    genotype_comb_type
        build_evidence(variable_index_type id, const std::map<bn_label_type, double>& obs) const
        {
            genotype_comb_type tmp;
            for (const auto& kv: obs) {
                tmp.m_combination.emplace_back(genotype_comb_type::element_type{{{id, kv.first}}, kv.second});
            }
            return tmp;
            /*return kronecker(tmp, domains.find({id})->second);*/
            /*genotype_comb_type ret = domains.find({id})->second;*/
            /*for (auto& e: ret) {*/
                /*auto it = obs.find(e.key);*/
                /*if (it != obs.end()) {*/
                    /*ret.m_combination.emplace_back(genotype_comb_type::key_list{{id, label}}, it->second);*/
                /*}*/
            /*}*/
            /*return ret;*/
        }

    bool
        var_is_protected(variable_index_type var)
        {
            return io[var] != None;
        }

    static
        std::unique_ptr<graph_type>
        from_pedigree(const std::vector<pedigree_item>& ped, size_t n_al, const std::vector<std::string>& input_gen, const std::vector<std::string>& output_gen, const std::string& debug_prefix = "")
        {
            std::unique_ptr<graph_type> g(new graph_type(n_al));
            g->aggregate_cycles = true;
            g->generate_interfaces = false;
            /*system("rm -vf test_*.png");*/
            for (const auto& p: ped) {
                /*MSG_DEBUG("-------------------------------------------------------------------------------");*/
                /*MSG_DEBUG("-------------------------------------------------------------------------------");*/
                /*MSG_DEBUG("-------------------------------------------------------------------------------");*/
                VariableIO io = None;
                if (std::find(input_gen.begin(), input_gen.end(), p.gen_name) != input_gen.end()) {
                    io |= Input;
                }
                if (std::find(output_gen.begin(), output_gen.end(), p.gen_name) != output_gen.end()) {
                    io |= Output;
                }
                g->io[p.id] = io;
                if (p.is_ancestor()) {
                    g->add_interface(node_vec{}, p.id);
                    /*g->add_factor(p.id);*/
                    g->add_ancestor(p.id);
                } else if (p.is_cross()) {
                    g->add_cross(p.p1, p.p2, p.id);
                } else if (p.is_dh()) {
                    g->add_dh(p.p1, p.id);
                } else if (p.is_self()) {
                    g->add_selfing(p.p1, p.id);
                }
                /*if (io) {*/
                    /*MSG_DEBUG("IO for var #" << p.id << ": " << io);*/
                /*}*/
#if 0
                if (debug_prefix.size()) {
                    auto tmp = g;
                    for (node_index_type n: tmp.active_nodes()) {
                        if (tmp.is_aggregate(n) && !tmp.is_interface(n)) {
                            tmp.subgraphs[n] = tmp.subgraph(n, false);
                        }
                    }
                    /*tmp.finalize();*/
                    tmp.to_image(SPELL_STRING(debug_prefix << '_' << std::setw(4) << std::setfill('0') << p.id << "_fg"), "png");
                }
#endif
            }
            g->finalize();
            g->compute_domains_and_factors();
            return g;
        }

    std::vector<bn_label_type>
        get_domain(variable_index_type var)
        {
            std::vector<bn_label_type> ret;
            const auto& dom = domains[{var}];
            ret.reserve(dom.size());
            for (const auto& e: dom) {
                ret.push_back(e.keys.keys.front().state);
            }
            return ret;
        }

#if 0
    void
        extract_marginals(std::map<int, std::map<bn_label_type, double>>& marginals)
        {
            std::map<int, std::map<bn_label_type, double>> ret;
            for (size_t n = 0; n < rank.size(); ++n) {
                if (type[n] == Factor && represented_by[n] == n) {
                    const genotype_comb_type& S = state[n].front();
                    auto varset = get_parents(S);
                    for (auto v: varset) {
                        if (ret.find(v) == ret.end()) {
                            joint_variable_product_type jvp;
                            jvp.add_table(S);
                            jvp.set_output({v});
                            jvp.compile(domains);
                            auto& M = marginals[v];
                            for (const auto& e: jvp.compute()) {
                                M[e.keys.keys[0].state] = e.coef;
                            }
                        }
                    }
                } else if (subgraphs[n]) {
                    subgraphs[n]->extract_marginals(marginals);
                }
            }
        }
#endif

    bool
        variables_are_independent(variable_index_type v1, variable_index_type v2, const std::list<node_index_type>& path) const
        {
            var_vec V = v1 < v2 ? var_vec{v1, v2} : var_vec{v2, v1};
            for (node_index_type n: path) {
                if (!(variables_of(n) % V).size()) {
                    return true;
                }
            }
            return false;
        }

    std::map<variable_index_type, node_index_type>
        get_output_interfaces(const var_vec& V) const
        {
            std::map<variable_index_type, node_index_type> ret;
            std::map<variable_index_type, node_vec> candidates;
            std::map<node_index_type, size_t> candidate_scores;
            for (node_index_type n: active_nodes()) {
                if (!is_interface(n)) {
                    continue;
                }
                auto vn = variables_of(n) % V;
                if (vn.size()) {
                    candidate_scores[n] += vn.size();
                    for (auto v: vn) {
                        candidates[v].push_back(n);
                    }
                }
            }

            for (auto& vn: candidates) {
                std::sort(vn.second.begin(), vn.second.end(), [&](node_index_type n1, node_index_type n2) { return candidate_scores[n1] > candidate_scores[n2]; });
                ret[vn.first] = vn.second.front();
            }

            return ret;
        }

    node_index_type
        find_anchor(const var_vec& V)
        {
            std::map<node_index_type, size_t> candidate_scores;
            for (node_index_type n: active_nodes()) {
                auto vn = variables_of(n) % V;
                if (vn.size()) {
                    candidate_scores[n] = vn.size();
                }
            }
            size_t max = 0;
            node_index_type best = (node_index_type) -1;
            for (const auto& kv: candidate_scores) {
                if (kv.second > max) {
                    max = kv.second;
                    best = kv.first;
                }
            }
            return best;
        }

    bool
        recursive_find_path_to_var(node_index_type node, variable_index_type v, std::list<node_index_type>& path, std::vector<bool>& visited) const
        {
            auto V = variables_of(node);
            if (std::find(V.begin(), V.end(), v) != V.end()) {
                /*path.push_back(node);*/
                return true;
            }
            visited[node] = true;
            for (node_index_type n: nei_in(node)) {
                n = resolve(n);
                if (!visited[n] && recursive_find_path_to_var(n, v, path, visited)) {
                    path.push_back(n);
                    return true;
                }
            }
            for (node_index_type n: nei_out(node)) {
                n = resolve(n);
                if (!visited[n] && recursive_find_path_to_var(n, v, path, visited)) {
                    path.push_back(n);
                    return true;
                }
            }
            return false;
        }

    std::list<node_index_type>
        find_path_to_var(node_index_type start, variable_index_type v)
        {
            std::vector<bool> visited(rank.size(), false);
            visited[start] = true;
            std::list<node_index_type> path;
            recursive_find_path_to_var(start, v, path, visited);
            return path;
        }

    void
        annotate(node_index_type anchor, const var_vec& V)
        {
            for (variable_index_type v: V) {
                for (node_index_type n: find_path_to_var(anchor, v)) {
                    annotations[n].push_back(v);
                }
            }
        }

    void
        sort_annotations()
        {
            for (auto& annot: annotations) {
                std::sort(annot.begin(), annot.end());
            }
        }

    void
        reset_annotations()
        {
            annotations.resize(rank.size());
            clear_annotations();
        }

    void
        clear_annotations()
        {
            for (auto& annot: annotations) {
                annot.clear();
            }
        }

    std::map<colour_proxy, std::map<size_t, node_index_type>>
        anchor_incoming(const std::vector<var_vec>& incoming)
        {
            std::map<colour_proxy, std::map<size_t, node_index_type>> ret;
            std::vector<size_t> indices(incoming.size());
            size_t index;
            std::generate(indices.begin(), indices.end(), [&] () { return index++; });
            std::sort(indices.begin(), indices.end(), [&] (size_t i, size_t j) { return incoming[i].size() > incoming[j].size(); });
            for (size_t i: indices) {
                node_index_type anchor = find_anchor(incoming[i]);
                annotate(anchor, incoming[i]);
                ret[get_colour_impl(colour[anchor])][i] = anchor;
            }
            sort_annotations();
            return ret;
        }


    std::vector<message_type> node_domains;
    std::vector<bool> node_domain_computed;

    message_type
        get_joint_domain(const var_vec& varset)
        {
            MSG_QUEUE_FLUSH();
            /*scoped_indent _(SPELL_STRING("[get_joint_domain " << varset << "] "));*/
            auto path = find_vpath(varset);
            message_type ret;
            for (const auto& cliq: path) {
                ret.emplace_back();
                message_type accum;
                for (const auto& path: cliq) {
                    message_type tmp;
                    for (node_index_type n: path) {
                        accumulate(tmp, get_node_domain(n), domains);
                        tmp %= varset + variables_of(n);
                        /*MSG_DEBUG("tmp " << tmp);*/
                    }
                    accumulate(accum, tmp, domains);
                    accum %= varset;
                }
                accumulate(ret, accum, domains);
            }
            return ret;
        }

    message_type
        get_node_domain(node_index_type n)
        {
            if (is_aggregate(n) && !is_interface(n)) {
                if (!node_domain_computed[n]) {
                    subgraphs[n]->compute_domains_and_factors();
                    node_domain_computed[n] = true;
                }
                return subgraphs[n]->get_joint_domain(variables_of(n));
            } else {
                if (!node_domain_computed[n]) {
                    compute_node_domain(n);
                }
                return node_domains[n];
            }
        }

    message_type
        get_node_domain(node_index_type n, const var_vec& vv)
        {
            /*MSG_DEBUG("get_node_domain(" << n << ", " << vv << ')');*/
            if (is_aggregate(n) && !is_interface(n)) {
                if (!node_domain_computed[n]) {
                    subgraphs[n]->compute_domains_and_factors();
                    node_domain_computed[n] = true;
                }
                return subgraphs[n]->get_joint_domain(vv);
            } else {
                if (!node_domain_computed[n]) {
                    compute_node_domain(n);
                }
                return node_domains[n] % vv;
            }
        }

    void
        propagate_spawnling_domain_rec(const var_vec& V, const genotype_comb_type& dom, std::map<const graph_type*, bool>& visited)
        {
            /*if (visited[this]) {*/
                /*return;*/
            /*}*/
            visited[this] = true;
            domains[V] = dom;
            /*MSG_DEBUG("Domains " << (parent ? SPELL_STRING(parent << "->" << index_in_parent) : std::string("top-level")) << ' ' << domains);*/
            if (parent && !visited[parent]) {
                const_cast<graph_type*>(parent)->propagate_spawnling_domain(V, dom);
            }
            for (auto& sub: subgraphs) {
                if (sub && !visited[sub.get()]) {
                    sub->propagate_spawnling_domain_rec(V, dom, visited);
                }
            }
        }

    void
        propagate_spawnling_domain(const var_vec& V, const genotype_comb_type& dom)
        {
            std::map<const graph_type*, bool> visited;
            propagate_spawnling_domain_rec(V, dom, visited);
        }

    void
        compute_node_domain(node_index_type n)
        {
            /*scoped_indent _(SPELL_STRING("[compute_node_domain #" << n << "] "));*/
            auto inputs = nei_in(n);
            auto vv = variables_of(n);
            if (is_interface(n)) {
                multiple_product_type mp;
                std::vector<message_type> stack;
                for (node_index_type i: inputs) {
                    stack.emplace_back(get_node_domain(i, vv));
                }
                for (const auto& m: stack) {
                    mp.add(m);
                }
                node_domains[n] = mp.compute(vv, domains);
                for (auto& dom: node_domains[n]) {
                    for (auto& e: dom) {
                        e.coef = 1;
                    }
                }
            } else if (is_aggregate(n)) {
                subgraphs[n]->compute_domains_and_factors();
            } else /* factor */ {
                /*scoped_indent _(SPELL_STRING("[compute_factor {" << rules[n] << "} => " << variables[n] << "] "));*/
                /*MSG_DEBUG(" ### ### COMPUTING FACTOR {" << rules[n] << "} => " << variables[n] << " ### ###");*/
                /*MSG_QUEUE_FLUSH();*/
                joint_variable_product_type jvp;

                var_vec varset = rules[n] + var_vec{variables[n]};
                std::sort(varset.begin(), varset.end());
                /*for (node_index_type o: nei_out(n)) {*/
                    /*varset = varset + variables_of(o);*/
                /*}*/
                message_type stack;
                stack.reserve(64);  /* FIXME arbitrary value */
                for (node_index_type i: inputs) {
                    /*MSG_DEBUG("[jpar_dom] varset " << varset);*/
                    auto idom = get_node_domain(i, varset);
                    /*MSG_DEBUG("[jpar_dom] from domain " << idom);*/
                    for (const auto& t: idom) {
                        stack.emplace_back(t);
                    }
                }

                            /*jvp.add_table(stack.back());*/
                            /*MSG_DEBUG(" * ADDING " << stack.back());*/
                        /*}*/
                    /*}*/
                /*}*/
                /*jvp.set_output(varset);*/
                /*jvp.compile(domains);*/
                /*auto jpar_dom = jvp.compute();*/
                genotype_comb_type jpar_dom;
                MSG_QUEUE_FLUSH();
                if (stack.size() > 1) {
                    /*MSG_DEBUG("Domains " << (parent ? SPELL_STRING(parent << "->" << index_in_parent) : std::string("top-level")) << ' ' << domains);*/
                    jpar_dom = compute_product(stack.begin(), stack.end(), varset, domains);
                } else {
                    jpar_dom = stack.front();
                }
                /*MSG_DEBUG("[jpar_dom] stack " << stack);*/
                /*MSG_DEBUG("[jpar_dom] result " << jpar_dom);*/
                node_domains[n].emplace_back(generate_factor(rules[n], is_dh[n], variables[n], jpar_dom, domains));
                /* Add domain for spawnling */
                variable_index_type spawnling = variables[n];
                auto dom = (node_domains[n] % var_vec{spawnling}).front();
                for (auto& e: dom) {
                    e.coef = 1;
                }
                propagate_spawnling_domain({spawnling}, dom);
                /*MSG_DEBUG("Domain for spawnling #" << spawnling << ": " << domains[{spawnling}]);*/
            }
            /*MSG_DEBUG("COMPUTED DOMAIN " << node_domains[n]);*/
            /*dump_node(n);*/
            node_domain_computed[n] = true;
        }

    void
        compute_domains_and_factors()
        {
            /*scoped_indent _(SPELL_STRING("[compute_domains_and_factors " << (parent ? SPELL_STRING(parent << "->" << index_in_parent) : std::string("top-level")) << "] "));*/
            /*MSG_DEBUG("n_alleles = " << n_alleles);*/
            if (parent) {
                domains = parent->domains;
            }
            node_domains.clear();
            node_domains.resize(rank.size());
            node_domain_computed.clear();
            node_domain_computed.resize(rank.size());
            /*MSG_DEBUG("Domains " << domains);*/

            auto active = active_nodes();

            for (node_index_type n: active) {
                if (is_interface(n) && nei_in(n).size() == 0) {
                    auto vv = variables_of(n);
                    if (parent == NULL) {
                        /* ancestor(s) ! */
                        for (variable_index_type v: vv) {
                            node_domains[n].emplace_back(domains[{v}]);
                        }
                        node_domain_computed[n] = true;
                        /*MSG_DEBUG("Pre-filled ancestor interface #" << n << " domain " << node_domains[n]);*/
                    } else {
                        multiple_product_type mp;
                        std::vector<message_type> stack;
                        for (node_index_type i: parent->resolve_vector(parent->nei_in(index_in_parent))) {
                            /*MSG_DEBUG("SUBGRAPH EXTERNAL INPUT #" << i);*/
                            stack.emplace_back(const_cast<graph_type*>(parent)->get_node_domain(i, vv));
                            /*MSG_DEBUG("Have domain " << stack.back());*/
                        }
                        for (const auto& m: stack) {
                            mp.add(m);
                        }
                        node_domains[n] = mp.compute(vv, domains);
                        node_domain_computed[n] = true;
                        /*MSG_DEBUG("HAVE DOMAIN FOR INPUT #" << n << ' ' << node_domains[n]);*/
                    }
                }
            }
            for (node_index_type n: active) {
                if (!node_domain_computed[n]) {
                    MSG_QUEUE_FLUSH();
                    compute_node_domain(n);
                }
            }

            /*MSG_DEBUG("domains computes " << node_domain_computed);*/

            /*for (node_index_type n: active) {*/
                /*dump_node(n);*/
                /*MSG_DEBUG("=========================================================");*/
            /*}*/
        }

    void
        delete_node(node_index_type n)
        {
            represented_by[n] = 0;
            const node_vec& N = inner_nodes[n] + node_vec{n};
            for (auto i: nei_in(n)) {
                neighbours_out[i] = nei_out(i) - N;
            }
            for (auto o: nei_out(n)) {
                neighbours_in[o] = nei_in(o) - N;
            }
        }

#define foreach_node_in(_var, _set) for (node_index_type _var: _set)
#define foreach_node_in_if(_var, _set, _pred) for (node_index_type _var: _set) if (_pred)
#define has_single_input(_n) nei_in(_n).size() == 1
#define has_single_output(_n) nei_out(_n).size() == 1
#define has_no_output(_n) nei_out(_n).size() == 0

    bool is_factor(node_index_type n) const { return type[n] == Factor; }
    bool is_unprotected(node_index_type n) const { for (node_index_type i: inner_nodes[n]) { if (io.find(variables[i])->second != None) { return false; } } return true; }

    size_t
        get_node_domain_size(node_index_type n)
        {
            auto dom = get_node_domain(n);
            if (dom.size() == 0) {
                return 0;
            }
            size_t accum = 1;
            for (const auto& t: dom) {
                accum *= t.size();
            }
            return accum;
        }

    void
        squeeze_factor(const node_vec& f1in, node_index_type f1, node_index_type i1, node_index_type f2)
        {
            var_vec var_f1in;
            for (node_index_type i: f1in) {
                var_f1in = var_f1in + variables_of(i);
            }
            auto in_f2_squeeze = variables_of(i1);
            foreach_node_in_if(if2, nei_in(f2), if2 != i1) {
                in_f2_squeeze = in_f2_squeeze - variables_of(if2);
            }
            auto final_varset = var_f1in + variables_of(f2) - in_f2_squeeze;
            multiple_product_type mp;
            message_type f1_subdom = get_node_domain(f1, var_f1in + in_f2_squeeze);
            mp.add(f1_subdom);
            mp.add(node_domains[f2]);
            node_domains[f2] = mp.compute(final_varset, domains); //product(node_domains[f1], node_domains[f2], domains) % varset;
            inner_nodes[f2].push_back(f1);
            neighbours_in[f2] = nei_in(f1) + nei_in(f2) - node_vec{i1};
            neighbours_out[i1] = nei_out(i1) - node_vec{f2};
            foreach_node_in(if1, f1in) {
                neighbours_out[if1] = nei_out(if1) + node_vec{f2};
            }
            if (has_no_output(i1)) {
                delete_node(i1);
            }
            node_variables[f2] = final_varset;
        }

    std::unique_ptr<graph_type>
        clone() const
        {
            std::unique_ptr<graph_type> ret(new graph_type());

            ret->rank = rank;
            ret->represented_by = represented_by;
            ret->type = type;
            ret->colour = colour;
            ret->neighbours_in = neighbours_in;
            ret->neighbours_out = neighbours_out;
            ret->inner_nodes = inner_nodes;
            ret->rules = rules;
            ret->variables = variables;
            ret->node_variables = node_variables;
            ret->io = io;
            ret->interface_to_node = interface_to_node;
            ret->node_to_interface = node_to_interface;
            ret->subgraphs.resize(rank.size());
            ret->tables = tables;
            ret->state = state;
            ret->parent = parent;
            ret->index_in_parent = index_in_parent;
            ret->aggregate_cycles = aggregate_cycles;
            ret->generate_interfaces = generate_interfaces;
            ret->n_alleles = n_alleles;
            ret->annotations = annotations;
            ret->is_dh = is_dh;
            ret->domains = domains;
            ret->ancestor_letters = ancestor_letters;
            ret->node_domains = node_domains;
            ret->node_domain_computed = node_domain_computed;
            return ret;
        }

    void
        debug_graph(const std::string& pfx, size_t x)
        {
            if (parent != NULL) {
                return;
            }
            auto tmp = clone();
            for (node_index_type n: tmp->active_nodes()) {
                if (tmp->is_aggregate(n) && !tmp->is_interface(n) && !tmp->subgraphs[n]) {
                    tmp->subgraphs[n] = tmp->subgraph(n, false);
                }
            }
            /*tmp->finalize();*/
            tmp->to_image(SPELL_STRING(pfx << '_' << this << '_' << std::setw(4) << std::setfill('0') << x), "png");
        }

    void
        optimize()
        {
            debug_graph("optimize", 0);
            /*scoped_indent _("[optimize] ");*/
            foreach_node_in_if(n, active_nodes(), subgraphs[n]) {
                subgraphs[n]->optimize();
            }
            debug_graph("optimize", 1);

            /*MSG_DEBUG("Squeeze intermediate subgraphs");*/
            foreach_node_in_if(f1, active_nodes(), is_aggregate(f1) && !is_interface(f1) && has_single_input(f1) && is_unprotected(f1)) {
                /*MSG_DEBUG("On subgraph#1 " << f1 << ' ' << variables_of(f1));*/
                auto f1in = nei_in(f1);
                foreach_node_in_if(i1, nei_out(f1), is_interface(i1)) {
                    /*MSG_DEBUG("On interface#1 " << i1 << ' ' << variables_of(i1) << " / " << nei_out(i1));*/
                    if (has_single_output(i1)) {
                    foreach_node_in_if(f2, nei_out(i1), type[f2] == Factor) {
                        /*MSG_DEBUG("On factor#2 " << f2 << ' ' << variables_of(f2));*/
                        squeeze_factor(f1in, f1, i1, f2);
                    }
                    }
                }
            }
            debug_graph("optimize", 2);

            /*MSG_DEBUG("Squeeze intermediate factors");*/
            foreach_node_in_if(f1, active_nodes(), is_factor(f1) && has_single_output(f1) && is_unprotected(f1)) {
                /*MSG_DEBUG("On factor#1 " << f1 << ' ' << variables_of(f1));*/
                auto f1in = nei_in(f1);
                foreach_node_in_if(i1, nei_out(f1), is_interface(i1) && has_single_output(i1)) {
                    /*MSG_DEBUG("On interface#1 " << i1 << ' ' << variables_of(i1));*/
                    foreach_node_in_if(f2, nei_out(i1), type[f2] == Factor) {
                        /*MSG_DEBUG("On factor#2 " << f2 << ' ' << variables_of(f2));*/
                        squeeze_factor(f1in, f1, i1, f2);
                    }
                }
            }
            debug_graph("optimize", 3);

            /* remove dangling interfaces that have no I/O */
            /*MSG_DEBUG("Purging dangling interfaces");*/
            foreach_node_in_if(n, active_nodes(), has_no_output(n) && is_unprotected(n)) {
                delete_node(n);
            }
            debug_graph("optimize", 4);
        }

    struct message_compute_operation_type {
        std::vector<size_t> incoming_messages;
        var_vec output;
        node_index_type emitter;
        node_index_type receiver;
    };

    struct computation_variant_type {
        std::vector<size_t> outer_inputs;
        /* FIXME vec<vec<>> takes into account that anchor* WILL return a list of anchors by colour */
        std::vector<std::vector<node_index_type>> anchors;
        std::vector<std::vector<size_t>> operations;
        /* FIXME must be a vector, in case of joint probabilities along a path, or multiple ones */
        message_compute_operation_type output_operation;
    };

    struct instance_type {
        std::vector<message_type> tables;
        std::vector<message_type> evidence;
        std::unordered_map<variable_index_type, node_index_type> evidence_affectations;
        std::vector<std::unique_ptr<instance_type>> sub_instances;
        const instance_type* parent;
        std::unordered_map<node_index_type, computation_variant_type> variants; /* computations for the given output (as key) */
        std::map<std::pair<node_index_type, node_index_type>, size_t> message_index;
        std::vector<message_type> messages; /* internal messages */
        std::vector<message_compute_operation_type> operations;

        instance_type() : tables(), evidence(), evidence_affectations(), sub_instances(), parent(nullptr) {}
        instance_type(graph_type* g)
            : tables(g->node_domains), evidence(g->node_domains.size()), sub_instances(g->node_domains.size()), parent(nullptr)
        {
            for (node_index_type n: g->active_nodes()) {
                if (g->subgraphs[n]) {
                    sub_instances[n].reset(new instance_type(g->subgraphs[n].get()));
                }
                for (variable_index_type v: g->variables_of(n)) {
                    if (evidence_affectations.find(v) == evidence_affectations.end()) {
                        evidence_affectations[v] = n;
                    }
                }
                size_t mi = 0;
                for (node_index_type o: g->nei_out(n)) {
                    message_index[{n, o}] = mi++;
                    message_index[{o, n}] = mi++;
                }
                messages.resize(mi);
                operations.resize(mi);
                node_index_type emitter, receiver;
                for (const auto& er_idx: message_index) {
                    auto& op = operations[er_idx.second];
                    std::tie(op.emitter, op.receiver) = er_idx.first;
                    op.output = g->variables_of(emitter) % g->variables_of(receiver);
                    auto nei = g->all_nei(emitter) - node_vec{receiver};
                    op.incoming_messages.reserve(nei.size());
                    for (node_index_type i: nei) {
                        op.incoming_messages.push_back(message_index[{i, emitter}]);
                    }
                }
            }
            build_variants(g);
        }

        void
            add_evidence(variable_index_type v, const genotype_comb_type& obs)
            {
                node_index_type index = evidence_affectations[v];
                if (sub_instances[index]) {
                    sub_instances[index]->add_evidence(v, obs);
                } else {
                    evidence[index].push_back(obs);
                }
            }

        void
            clear_evidence()
            {
                evidence.clear();
                evidence.resize(tables.size());
            }

        message_type
            compute_message(const message_compute_operation_type& op, const std::map<var_vec, genotype_comb_type>& domains)
            {
                if (sub_instances[op.emitter]) {
                    return sub_instances[op.emitter]->compute(op.receiver, domains);
                }
                multiple_product_type mp;
                mp.add(evidence[op.emitter]);
                mp.add(tables[op.emitter]);
                for (size_t inc: op.incoming_messages) {
                    mp.add(messages[inc]);
                }
                return mp.compute(op.output, domains);
            }

        message_type
            compute(node_index_type n, const std::map<var_vec, genotype_comb_type>& domains)  /* compute an external message (this -> external node #n) */
            {
                const auto& variant = variants[n];
                clear_evidence();
                for (size_t i = 0; i < variant.outer_inputs.size(); ++i) {
                    evidence[variant.anchors[i]] = parent->messages[variant.outer_inputs[i]];
                }
                for (const auto& op: variant.operations) {
                    compute_message(op, domains);
                }
                return compute_message(variant.output_operation, domains);
            }

        void
            get_message_order_rec(size_t final_message, std::vector<bool>& message_visited, std::vector<size_t>& ret)
            {
                for (size_t inc: operations[final_message].incoming_messages) {
                    if (!message_visited[inc]) {
                        get_message_order_rec(inc, message_visited, ret);
                    }
                }
                message_visited[final_message] = true;
                ret.push_back(final_message);
            }

        std::vector<size_t>
            get_message_order(size_t final_message)
            {
                std::vector<bool> message_visited(operations.size());
                std::vector<size_t> order;
                get_message_order_rec(final_message, message_visited, order);
                return order;
            }

        void
            build_variant(node_index_type towards, graph_type* g, const std::map<size_t, node_index_type>& incoming, const var_vec& output)
            {
                /* incoming is [external message index] => internal anchor node */
                auto& V = variants[towards];
                V
            }

        void
            build_variants(graph_type* g)
            {
                std::map<node_index_type, node_index_type> neighbours;
                if (g->parent) {
                    auto nei_nodes = g->nei_in(g->index_in_parent) + g->nei_out(g->index_in_parent);
                    std::vector<var_vec> all_nei_vars;
                    all_nei_vars.reserve(nei_nodes.size());
                    for (node_index_type n: nei_nodes) {
                        all_nei_vars.push_back(g->parent->variables_of(n) % g->parent->variables_of(g->index_in_parent));
                    }
                    g->reset_annotations();
                    auto anchors = g->anchor_incoming(all_nei_vars);
                    for (const auto& c_ia: anchors) {
                        /*neighbours[parent->message_index[{nei_nodes[ia.first], g->index_in_parent}]] = ia.second;*/
                        for (const auto& ia: c_ia.second) {
                            neighbours[nei_nodes[ia.first]] = ia.second;
                        }
                    }
                    for (size_t i = 0; i < nei_nodes.size(); ++i) {
#if 0
                        g->reset_annotations();
                        std::vector<var_vec> nei_vars;
                        nei_vars.reserve(all_nei_vars.size() - 1);
                        nei_vars.insert(nei_vars.end(), all_nei_vars.begin(), all_nei_vars.begin() + i);
                        nei_vars.insert(nei_vars.end(), all_nei_vars.begin() + i + 1, all_nei_vars.end());
                        g->reset_annotations();
                        auto anchors = g->anchor_incoming(nei_vars);
                        std::map<size_t, node_index_type> incoming;
                        for (const auto& ia: anchors) {
                            incoming[parent->message_index[{nei_nodes[ia.first], g->index_in_parent}]] = ia.second;
                        }
#endif
                        build_variant(nei_nodes[i], g, incoming, all_nei_vars[i]);
                    }
                }
                build_variant(0, g, neighbours, {});  /* to compute full state later, when all messages are computed. */
            }
    };
};

inline
message_type
graph_type::computation_variant_type::compute(graph_type::instance_type* I)
{
    I->messages.clear();
    I->messages.resize(I->message_index.size());

    /* install input messages as evidence */
    I->clear_evidence();
    for (

}

#if 0
struct subgraph_type {
    graph_type graph;

    subgraph_type(const graph_type source, const std::vector<size_t>& subset)
        : graph()
    {
        var_vec remaining, imported;
        MSG_DEBUG("CREATING SUBGRAPH " << subset);
        for (size_t i: subset) {
            /*const auto& rule = source.restrict_inputs(source.rules[i], subset);*/
            /*source.restrict_inputs(source.rules[i], subset, remaining, imported);*/
            /*switch (remaining.size()) {*/
            const auto& rule = source.rules[i];
            auto var = source.variables[i];
            /*MSG_DEBUG("subgraph " << i << "  " << rule << " " << var);*/
            if (source.type[i] == Factor) {
                switch (rule.size()) {
                    case 0: graph.add_factor(var);
                            break;
                    case 1: graph.add_factor(rule[0], var);
                            break;
                    case 2: graph.add_factor(rule[0], rule[1], var);
                            break;
                };
            } else {
                if (graph.interface_to_node.find(var) == graph.interface_to_node.end()) {
                    graph.add_interface(node_vec{}, var);
                }
            }
        }
        /*graph.finalize();*/
        /*for (auto& inn: graph.inner_nodes) {*/
            /*for (size_t& i: inn) {*/
                /*i = source.inner_nodes[to_old_index[i]][0];*/
            /*}*/
        /*}*/
    }
};
#endif

inline
std::ostream&
operator << (std::ostream& os, const edge_type& e)
{
    std::stringstream tmp;
    tmp << (e.graph->is_interface(e.first) ? 'I' : 'F') << e.graph->variables_of(e.first)
        << "->"
        << (e.graph->is_interface(e.second) ? 'I' : 'F') << e.graph->variables_of(e.second);
    return os << tmp.str();
}




#endif

