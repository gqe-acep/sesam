/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_BAYES_FACTOR_H_
#define _SPEL_BAYES_FACTOR_H_

#include "message.h"

template <typename V>
std::vector<V>
operator + (const std::vector<V>& u, const std::vector<V>& v)
{
    std::vector<V> ret(u.size() + v.size());
    auto it = std::set_union(u.begin(), u.end(), v.begin(), v.end(), ret.begin());
    ret.resize(it - ret.begin());
    return ret;
}


template <typename V>
std::vector<V>
operator - (const std::vector<V>& u, const std::vector<V>& v)
{
    std::vector<V> ret(u.size() + v.size());
    auto it = std::set_difference(u.begin(), u.end(), v.begin(), v.end(), ret.begin());
    ret.resize(it - ret.begin());
    return ret;
}





struct compute_labels {
    bn_label_type
        find_label(size_t n, const genotype_comb_type::element_type& labels)
        {
            auto it
                = std::find_if(labels.keys.begin(), labels.keys.end(),
                               [=] (const genotype_comb_type::key_type& k) { return k.parent == n; }
                  );
            if (it == labels.keys.end()) {
                MSG_ERROR("COULDN'T FIND LABEL FOR " << n << " IN " << labels, "");
                MSG_QUEUE_FLUSH();
                return {};
            }
            return it->state;
        }

    bn_label_type
        operator () (const pedigree_tree_type& tree, size_t n, const genotype_comb_type::element_type& labels, const std::vector<bool>& recompute)
        {
            if (tree.get_p2(n) == NONE) {
                /* gamete or ancestor */
                if (tree.get_p1(n) == NONE) {
                    /* ancestor */
                    return find_label(n, labels);
                } else {
                    auto gl = find_label(n, labels);
                    auto sub = operator () (tree, tree.get_p1(n), labels, recompute);
                    if (gl.first == GAMETE_L) {
                        return {sub.first, 0, sub.first_allele, 0};
                    } else {
                        return {sub.second, 0, sub.second_allele, 0};
                    }
                }
            } else  if (recompute[n]) {
                auto subl = operator () (tree, tree.get_p1(n), labels, recompute);
                auto subr = operator () (tree, tree.get_p2(n), labels, recompute);
                return {subl.first, subr.first, subl.first_allele, subr.first_allele};
            } else {
                return find_label(n, labels);
            }
        }

    std::vector<bn_label_type>
        operator () (const pedigree_tree_type& tree, size_t n, const genotype_comb_type& comb, const std::vector<bool>& recompute)
        {
            std::vector<bn_label_type> ret;
            ret.reserve(comb.m_combination.size());
            for (const auto& e: comb) {
                ret.emplace_back(operator () (tree, n, e, recompute));
            }
            return ret;
        }
};


#if 0
struct factor_set_type {
    size_t n_alleles;
    std::map<size_t, genotype_comb_type>& joint_prob_tables;
    std::map<size_t, std::vector<bn_label_type>>& variable_domains;

    

    void
        compute_factor(const graph_type& G, size_t id, size_t p1, size_t p2)
        {
            if (p1 == 0 && p2 == 0) {
                /* ancestor */
                std::vector<bn_label_type> labels;
                char letter = ped.ancestor_letters.find(ind_node)->second;
                for (size_t i = 0; i < n_alleles; ++i) {
                    labels.emplace_back(letter, letter, i, i);
                }
                variable_domains[ind_node] = labels;
            } else if (p1 == p2) {
                /* selfing */
            } else if (p2 == 0) {
                /* doubled haploid */
            } else {
                /* standard cross */
            }
        }

};
#endif







struct bayesian_node_type;
typedef std::shared_ptr<bayesian_node_type> bayesian_node;
struct bn_interface_type;
typedef std::shared_ptr<bn_interface_type> bn_interface;


struct bn_interface_type {
    std::vector<bayesian_node> neighbours;
    std::vector<bn_message_type> incoming;
    std::vector<size_t> variables;

    struct iterator {
        const bn_interface_type& interface;
        const bayesian_node_type* target;
        size_t source_idx;

        bool operator != (const iterator& other) const { return source_idx != other.source_idx; }
        iterator&
            operator ++ ()
            {
                ++source_idx;
                if (source_idx < interface.neighbours.size() && interface.neighbours[source_idx].get() == target) {
                    ++source_idx;
                }
                return *this;
            }

        const bn_message_type& operator * () const { return interface.incoming[source_idx]; }
        const bn_message_type* operator -> () const { return &interface.incoming[source_idx]; }
    };

    iterator
        begin(const bayesian_node_type* factor) const
        {
            return {*this, factor, factor == neighbours.front().get() ? (size_t) 1 : (size_t) 0};
        }
    iterator end() const { return {*this, NULL, incoming.size()}; }

    void
        apply(const std::vector<size_t>& variables, const bayesian_node_type* factor, genotype_comb_type& state)
        {
            auto i = begin(factor), j = end();
            for (; i != j; ++i) {
                i->apply(variables, state);
            }
        }

    void
        absorb(const bn_interface_type& other)
        {
            variables = variables + other.variables;
            neighbours = neighbours + other.neighbours;
            incoming.resize(neighbours.size());
        }
};


struct bayesian_node_type {
    virtual
        void
        compute(const std::vector<bn_interface>& inputs, const std::vector<size_t>& output_variables, bn_message_type& outputs) = 0;

    virtual
        void
        compute_marginals(const bn_message_type& inputs, bn_message_type& outputs) = 0;

    virtual ~bayesian_node_type() {}

    bayesian_node_type(const std::vector<size_t>& variables) : m_variables(variables) {}

    const std::vector<size_t>& variables() const { return m_variables; }

private:
    std::vector<size_t> m_variables;
};

struct bn_factor_type : public bayesian_node_type {
    bn_factor_type(const std::vector<size_t>& variables, genotype_comb_type table)
        : bayesian_node_type(variables), m_table(table)
    {}

    void
        compute(const std::vector<bn_interface>& inputs, const std::vector<size_t>& output_variables, bn_message_type& outputs)
        {
            std::vector<size_t> subvarset = output_variables % variables();
            if (subvarset.size()) {
                genotype_comb_type state(m_table);
                for (const auto& i: inputs) {
                    i->apply(variables(), this, state);
                }

                bn_single_message_type output;

                for (const auto& e: state) {
                    output.accumulate(e.keys % subvarset, e.coef);
                }
                outputs.append(output);
            }
        }

    genotype_comb_type m_table;

};

typedef std::shared_ptr<bn_factor_type> bn_factor;


struct bayesian_message_type;
typedef std::shared_ptr<bayesian_message_type> bayesian_message;

struct bayesian_message_type {
    std::vector<bayesian_message> inputs;
    bayesian_node computer;
    std::vector<size_t> variables;
    bn_message_type output;
};


struct compare_message {
    bool operator () (bayesian_message m1, bayesian_message m2) const
    {
        std::deque<bayesian_message> stack(m2->inputs.begin(), m2->inputs.end());
        while (stack.size()) {
            if (m1 == stack.front()) {
                return true;
            }
            stack.insert(stack.end(), stack.front()->inputs.begin(), stack.front()->inputs.end());
            stack.pop_front();
        }
        return false;
    }
};

inline
void
sort_messages(std::list<bayesian_message>& messages)
{
    /*std::sort(messages.begin(), messages.end(), compare_message());*/
    messages.sort(compare_message());
}


struct bn_factor_graph_type : public bayesian_node_type {
    bn_factor_graph_type(const std::vector<size_t>& variables)
        : bayesian_node_type(variables)
    {}

    void
        compute(const std::vector<bn_interface>& inputs, const std::vector<size_t>& output_variables, bn_message_type& outputs)
        {
            (void) inputs;
            (void) output_variables;
            (void) outputs;
        }

    bn_single_message_type
        marginals() const
        {
            bn_single_message_type ret;
            return ret;
        }

    size_t
        add_node(bayesian_node node)
        {
            size_t ret = m_nodes.size();
            m_nodes.emplace_back(node);
            m_neighbours.emplace_back();
            return ret;
        }

    void
        set_neighbours(size_t node, std::vector<size_t>& nei)
        {
            m_neighbours[node].swap(nei);
        }

    void
        create_messages()
        {
            std::map<bayesian_node, std::map<bayesian_node, bayesian_message>> msg_map;
            for (size_t node = 0; node < m_nodes.size(); ++node) {
                for (size_t neighbour: m_neighbours[node]) {
                    bayesian_message msg = std::make_shared<bayesian_message_type>();
                    msg_map[m_nodes[node]][m_nodes[neighbour]] = msg;
                    msg->computer = m_nodes[node];
                    msg->variables = m_nodes[node]->variables() % m_nodes[neighbour]->variables();
                }
            }
            for (size_t node = 0; node < m_nodes.size(); ++node) {
                for (size_t neighbour: m_neighbours[node]) {
                    bayesian_message msg = msg_map[m_nodes[node]][m_nodes[neighbour]];
                    m_messages.emplace_back(msg);
                    msg->inputs.resize(m_neighbours[node].size() - 1);
                    size_t i;
                    for (i = 0; i < neighbour; ++i) {
                        msg->inputs[i] = msg_map[m_nodes[m_neighbours[node][i]]][m_nodes[node]];
                    }
                    for (++i; i < m_neighbours[node].size(); ++i) {
                        msg->inputs[i - 1] = msg_map[m_nodes[m_neighbours[node][i]]][m_nodes[node]];
                    }
                }
            }
            /*std::sort(m_messages.begin(), m_messages.end(), compare_message());*/
            sort_messages(m_messages);
        }

private:
    std::vector<bayesian_node> m_nodes;
    std::vector<std::vector<size_t>> m_neighbours;
    std::list<bayesian_message> m_messages;
    std::list<bayesian_message> m_messages_marginals;
};

typedef std::shared_ptr<bn_factor_graph_type> bn_factor_graph;



#if 0

struct bn_factor_type {
    bn_factor_type() : m_variables(), m_joint_prob_table(), m_inputs() {}
    bn_factor_type(const genotype_comb_type& joint)
        : m_variables(), m_joint_prob_table(joint), m_inputs()
    {
        m_variables = get_parents(m_joint_prob_table);
    }

    bn_factor_type(const bn_factor_type& other)
        : m_variables(other.m_variables), m_joint_prob_table(other.m_joint_prob_table), m_inputs(other.m_inputs)
    {}

    bn_factor_type(bn_factor_type&& other)
        : m_variables(std::move(other.m_variables)), m_joint_prob_table(std::move(other.m_joint_prob_table)),
          m_inputs(std::move(other.m_inputs))
    {}

    bn_factor_type(genotype_comb_type&& joint)
        : m_variables(), m_joint_prob_table(std::move(joint)), m_inputs()
    {
        m_variables = get_parents(m_joint_prob_table);
    }

    void
        compute_io_sets(const pedigree_tree_type& T)
        {
            m_inputs.reserve(m_variables.size() - 1);
            m_outputs.reserve(m_variables.size() - 1);
            for (size_t v: m_variables) {
                auto anc = T.count_ancestors(v);
                bool add = !T[v].is_ancestor();  /* ancestors are treated like output variables. They come from nowhere. */
                for (const auto& kv: anc) {
                    if (std::find(m_variables.begin(), m_variables.end(), kv.first) != m_variables.end()) {
                        add = false;
                        m_ancestors[v].push_back((size_t) kv.first);
                    }
                }
                if (add) {
                    m_inputs.push_back(v);
                } else {
                    m_outputs.push_back(v);
                }
            }
        }

    genotype_comb_type
        project(const std::vector<size_t>& project_variables)
        {
            if (project_variables == m_variables) {
                return m_joint_prob_table;
            }
            std::vector<size_t> norm_variables;
            norm_variables.reserve(m_variables.size());
            for (const auto& kv: m_ancestors) {
                bool is_leaf = true;
                for (const auto& a: kv.second) {
                    if (std::find(project_variables.begin(), project_variables.end(), a) != project_variables.end()) {
                        is_leaf = false;
                        break;
                    }
                }
                if (is_leaf) {
                    norm_variables.push_back(kv.first);
                }
            }
            /*std::vector<size_t> norm_variables(m_inputs.size());*/
            /*auto it = std::set_difference(m_inputs.begin(), m_inputs.end(),*/
                                          /*project_variables.begin(), project_variables.end(),*/
                                          /*norm_variables.begin());*/
            /*norm_variables.resize(it - norm_variables.begin());*/
            return ::project(m_joint_prob_table, project_variables, norm_variables);
        }


    std::vector<size_t>
        common_variables(const bn_factor_type& other) const
        {
            return intersection(m_variables, other.m_variables);
        }

    friend
        std::ostream&
        operator << (std::ostream& os, const bn_factor_type& factor)
        {
            os << "FACTOR @" << (&factor) << " on variables {" << factor.m_variables << '}' << std::endl;
            os << "joint prob. table: " << factor.m_joint_prob_table << std::endl;
            return os;
        }

    friend
        std::ostream&
        operator << (std::ostream& os, bn_factor_ptr factor)
        {
            return os << '{' << factor->variables() << '}';
        }

    const std::vector<size_t>&
        variables() const { return m_variables; }

    const std::vector<size_t>&
        inputs() const { return m_inputs; }

    const std::vector<size_t>&
        outputs() const { return m_outputs; }

    bool operator < (const bn_factor_type& other) const { return intersection(m_outputs, other.m_inputs).size() > 0; }

    const genotype_comb_type&
        table() const { return m_joint_prob_table; }

    template <typename STREAM_TYPE>
        void
        file_io_common(STREAM_TYPE& fs)
        {
            rw_comb<size_t, bn_label_type> rw;
            if (rw.fourcc(fs, "FACT")) {
                return;
            }
            rw(fs, m_variables);
            rw(fs, m_inputs);
            rw(fs, m_ancestors);
            rw(fs, m_joint_prob_table);
        }

private:
    std::vector<size_t> m_variables;
    genotype_comb_type m_joint_prob_table;
    std::vector<size_t> m_inputs;
    std::vector<size_t> m_outputs;
    std::map<size_t, std::vector<size_t>> m_ancestors;
};



struct bn_factor_computer {
    bn_factor_computer(bn_factor f) : m_factor(f), m_state() {}

    void
        compute_state(bn_neighbour_iterator_type begin, bn_neighbour_iterator_type end, const bn_message_type& observations, bn_output_iterator_type out_begin, bn_output_iterator_type out_end) const
        {
            /*MSG_DEBUG("compute_factor_message, output over " << variables << ", have " << (end - begin) << " inputs");*/
            genotype_comb_type tmp_joint[2];
            size_t tmp_index = 0;
#define FRONT tmp_joint[tmp_index]
#define BACK tmp_joint[1 - tmp_index]
#define SWAP_JOINT() do { tmp_index ^= 1; } while (0)

            FRONT.m_combination.reserve(m_joint_prob_table.size());
            BACK.m_combination.reserve(m_joint_prob_table.size());

            double biggest;

            auto scale_and_swap
                = [&] ()
                {
                    MSG_DEBUG("front before cleanup " << FRONT);
                    if (biggest) {
                        double lowest = biggest * DBL_EPSILON;
                        int exponent = std::ilogb(biggest);
                        double scale = std::scalbn(1, -exponent);
                        BACK.m_combination.clear();
                        for (const auto& e: FRONT) {
                            /*if (e.coef > lowest) {*/
                            if (e.coef > lowest) {
                                BACK.m_combination.emplace_back(e.keys, e.coef * scale);
                            } else {
                                MSG_DEBUG("omitting small value " << e);
                            }
                        }
                        SWAP_JOINT();
                    }
                };

            /* Init joint prob table with observations */
            biggest = 0;
            for (const auto& e: m_joint_prob_table) {
                double prob = e.coef;
                for (const auto& key: e.keys) {
                    prob *= observations[key];
                }
                if (prob > 0) {
                    biggest = std::max(prob, biggest);
                    FRONT.m_combination.emplace_back(e.keys, prob);
                }
            }
            if (biggest == 0) {
                /*MSG_DEBUG("NO BIGGIE " << FRONT);*/
            }

            scale_and_swap();
            MSG_DEBUG("  FRONT " << FRONT);

            for (bn_neighbour_iterator_type i = begin; i != end; ++i) {
                biggest = 0;
                MSG_DEBUG("accumulating message " << (*i));
                for (auto& e: FRONT) {
                    genotype_comb_type::key_list interface_key = e.keys % i->variables();
                    e.coef *= (*i)[interface_key];
                    biggest = std::max(e.coef, biggest);
                }
                if (biggest == 0) {
                    /*MSG_DEBUG("NO BIGGIE " << FRONT);*/
                }
                scale_and_swap();
                MSG_DEBUG("  FRONT " << FRONT);
            }

            for (bn_output_iterator_type i = out_begin; i != out_end; ++i) {
                auto& output = *i;
                output.reset(0);
                const auto& variables = i.variables();
                for (const auto& e: FRONT) {
                    genotype_comb_type::key_list output_key;
                    genotype_comb_type::element_type sub_element;
                    std::tie(output_key, sub_element) = e.extract(variables);
                    output.accumulate(output_key, e.coef);
                }

                double accum = 0;

                for (auto& kv: output) { accum += kv.second; }
                if (accum != 0) {
                    accum = 1. / accum;
                    for (auto& kv: output) { kv.second *= accum; }
                }

                MSG_DEBUG("factor message " << output);

                if (output.norm() == 0) {
                    exit(-23);
                }
            }

            m_state.swap(FRONT);
        }

private:
    bn_factor m_factor;
    genotype_comb_type m_state;
};

#endif

#endif

