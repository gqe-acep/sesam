/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_BAYES_message_H_
#define _SPEL_BAYES_message_H_

#include <map>
#include <vector>
/*#include "../linear_combination.h"*/
#include "../pedigree.h"

template <typename V>
std::vector<V>
operator % (const std::vector<V>& u, const std::vector<V>& v)
{
    std::vector<V> ret(std::min(u.size(), v.size()));
    auto it = std::set_intersection(u.begin(), u.end(), v.begin(), v.end(), ret.begin());
    ret.resize(it - ret.begin());
    return ret;
}



struct bn_single_message_type {
    std::vector<size_t> m_variables;
    std::map<genotype_comb_type::key_list, double> m_map;

    void
        set(const genotype_comb_type::key_list& keys, double d)
        {
            /*if (d != m_default_val) {*/
                m_map[keys] = d;
            /*}*/
        }

    void
        accumulate(const genotype_comb_type::key_list& keys, double d)
        {
            auto it = m_map.find(keys);
            if (it == m_map.end()) {
                m_map.emplace(keys, d);
            } else {
                it->second += d;
            }
        }

    double
        operator [] (const genotype_comb_type::key_list& keys) const
        {
            auto it = m_map.find(keys);
            if (it == m_map.end()) {
                return 0;
            }
            return it->second;
        }

    bn_single_message_type
        project(const std::vector<size_t>& varset) const
        {
            bn_single_message_type ret;
            ret.m_variables = varset;
            for (const auto& kv: m_map) {
                ret.accumulate(kv.first % varset, kv.second);
            }
            return ret;
        }

    void
        swap(bn_single_message_type& other)
        {
            m_map.swap(other.m_map);
            m_variables.swap(other.m_variables);
        }
};


struct bn_message_type {
    std::vector<size_t> m_variables;
    std::vector<bn_single_message_type> m_indep;

    std::vector<bn_single_message_type>::const_iterator
        begin() const { return m_indep.begin(); }

    std::vector<bn_single_message_type>::const_iterator
        end() const { return m_indep.end(); }

    bn_message_type
        project(const std::vector<size_t>& varset) const
        {
            bn_message_type ret;
            for (const auto& indep: m_indep) {
                std::vector<size_t> subvarset = indep.m_variables % varset;
                if (subvarset.size()) {
                    ret.m_indep.push_back(indep.project(subvarset));
                }
            }
            return ret;
        }

    void
        apply(const std::vector<size_t>& variables, genotype_comb_type& table) const
        {
            bn_message_type interface = project(variables);
            genotype_comb_type result;
            result.m_combination.reserve(table.m_combination.size());
            double biggest = 0;
            for (const auto& e: table.m_combination) {
                double coef = e.coef;
                for (const auto& i: interface) {
                    auto keys = e.keys % i.m_variables;
                    coef *= i[keys];
                }
                if (coef) {
                    if (coef > biggest) { biggest = coef; }
                    result.m_combination.emplace_back(e.keys, coef);
                }
            }
            if (biggest) {
                double lowest = biggest * DBL_EPSILON;
                int exponent = std::ilogb(biggest);
                double scale = std::scalbn(1, -exponent);
                auto i = table.m_combination.begin();
                for (auto& e: result.m_combination) {
                    if (e.coef > lowest) {
                        i->coef = e.coef * scale;
                        i->keys.keys.swap(e.keys.keys);
                        ++i;
                    }
                }
                table.m_combination.resize(i - table.m_combination.begin());
            } else {
                table.m_combination.swap(result.m_combination);
            }
        }

    bn_message_type&
        append(const bn_message_type& other)
        {
            m_indep.insert(m_indep.end(), other.m_indep.begin(), other.m_indep.end());
            return *this;
        }

    bn_message_type&
        append(bn_single_message_type& other)
        {
            m_indep.emplace_back();
            m_indep.back().swap(other);
            return *this;
        }
};



#endif

