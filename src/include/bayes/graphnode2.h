/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_BAYES_GRAPH_NODE_H_
#define _SPEL_BAYES_GRAPH_NODE_H_

#include "graphnode_base.h"

#include "../pedigree.h"

struct graph_type : public graph_base_type {
    graph_type() : graph_base_type() {}

    void
        add_variable(const var_vec& rule, variable_index_type new_variable)
        {
            /*scoped_indent _(SPELL_STRING("[add_variable " << rule << ' ' << new_variable << "] "));*/
            if (rule.size() > 0) {
                node_vec parents;
                if (rule.size() == 2) {
                    parents = variables_to_nodes(rule);
                } else {
                    parents = {node_by_var(rule.front())};
                }
                node_index_type newfac = add_factor(rule, new_variable);
                path_type path;
                foreach_in(parents, [&, this] (node_index_type p) {
                        if (add_edge(p, newfac, path)) {
                            aggregate_path(path);
                        }
                        });
            } else {
                add_interface(new_variable);
                char letter = m_ancestor_letters.size() + 'a';
                m_ancestor_letters[new_variable] = letter;
            }
            /*MSG_DEBUG("===================================");*/
            /*dump();*/
            /*MSG_DEBUG("===================================");*/
        }

    char
        ancestor_letter(variable_index_type v) const
        {
            return m_ancestor_letters.find(v)->second;
        }

    void
        postprocess()
        override
        {
            graph_base_type::postprocess();
            /**/
        }

    template <typename Action>
        void
        foreach_ancestor(Action&& action)
        {
            for (const auto& kv: m_ancestor_letters) {
                action(kv.first, kv.second);
            }
        }

private:
    std::map<variable_index_type, char> m_ancestor_letters;

    struct parents_of_max_rank {
        std::list<node_index_type>::iterator p1, child, p2;
    };

    parents_of_max_rank
        find_parents_of_max_rank(std::list<node_index_type>& path)
        {
            auto child = std::max_element(path.begin(), path.end(), [this](node_index_type n1, node_index_type n2) { return rank(n1) < rank(n2); });
            auto p1 = child, p2 = child;
            if (child == path.begin()) {
                p1 = path.end();
            }
            --p1;
            ++p2;
            if (p2 == path.end()) {
                p2 = path.begin();
            }
            return {p1, child, p2};
        }

    void
        aggregate_path(std::list<node_index_type>& path)
        {
            parents_of_max_rank aggr_first;
            while (path.size() > 2 && ((aggr_first = find_parents_of_max_rank(path)), !aggregate_part(*aggr_first.p1, *aggr_first.p2, path, aggr_first.p1))) {
                path.erase(aggr_first.p1);
                path.erase(aggr_first.p2);
                path.erase(aggr_first.child);
            }
        }

    bool
        aggregate_part(node_index_type p1, node_index_type p2, path_type& path, path_type::iterator pos)
        {
            node_vec aggr = find_aggregate_chain(p1, p2);
            sort_and_unique(aggr);
            node_index_type ret = aggregate(aggr);
            path.insert(pos, ret);
            return aggr.size() > 2;
        }

    node_vec
        variables_to_nodes(const var_vec& rule)
        {
            auto sorted_rule = rule;
            std::sort(sorted_rule.begin(), sorted_rule.end());
            auto find_rule = [&,this](node_index_type n) { return variables_of(n) % sorted_rule == sorted_rule; };
            node_index_type common = find_in(nodes(), find_rule);
            if (common == (node_index_type) -1) {
                /*MSG_DEBUG("common not found.");*/
                node_vec ret;
                for (variable_index_type v: rule) {
                    node_index_type n = node_by_var(v);
                    if (n != (node_index_type) -1) {
                        ret.push_back(n);
                    }
                }
                sort_and_unique(ret);
                /*MSG_DEBUG("returning parents " << ret);*/
                return ret;
            } else {
                /*MSG_DEBUG("Initial common " << common);*/
                node_index_type parent = -1;
                while ((parent = find_in(nei_in(common), find_rule)) != (node_index_type) -1) {
                    common = parent;
                    /*MSG_DEBUG("New common " << common);*/
                }
                node_vec parents;
                foreach_in_if(nei_in(common),
                    [&,this] (node_index_type n) { return sorted_rule.size() && !!(variables_of(n) % sorted_rule).size(); },
                    [&,this] (node_index_type n) {
                        sorted_rule -= variables_of(n);
                        parents.push_back(n);
                        /*MSG_DEBUG("Found parent " << n << " now sorted_rule is " << sorted_rule);*/
                    });
                /*MSG_DEBUG("remaining sorted_rule " << sorted_rule);*/
                if (!sorted_rule.size()) {
                    /*MSG_DEBUG("=> parents " << parents);*/
                    return parents;
                } else {
                    /*MSG_DEBUG("=> common " << common);*/
                    return {common};
                }
            }
        }

    bool
        find_ascending_path(node_index_type p1, node_index_type p2, node_vec& path, std::vector<bool>& visited)
        {
            /*MSG_DEBUG("path from " << p1 << " to " << p2);*/
            if (p1 == p2) {
                return true;
            }
            for (node_index_type i: nei_in(p1)) {
                if (visited[i]) {
                    return false;
                }
                visited[i] = true;
                if (find_ascending_path(i, p2, path, visited)) {
                    path.push_back(i);
                    return true;
                }
            }
            return false;
        }

    node_vec
        find_aggregate_chain(node_index_type p1, node_index_type p2)
        {
            node_vec path;
            std::vector<bool> vis1(size(), false), vis2(size(), false);

            if (rank(p1) > rank(p2) && find_ascending_path(p1, p2, path, vis1)) {
                /*MSG_DEBUG("found ascending path from " << p1 << " to " << p2);*/
                path.push_back(p1);
            } else if (rank(p2) > rank(p1) && find_ascending_path(p2, p1, path, vis2)) {
                /*MSG_DEBUG("found ascending path from " << p2 << " to " << p1);*/
                path.push_back(p2);
            } else {
                /*MSG_DEBUG("no ascending path between parents");*/
                path = {p1, p2};
            }
            return path;
        }

};


template <typename Derived>
struct recursive_graph_type : public graph_type {
    recursive_graph_type() : graph_type(), m_subgraphs(), m_parent(nullptr), m_index_in_parent(-1) {}

    void
        dump()
        {
            foreach_in(nodes(), [this] (node_index_type n) {
                dump_node(n);
                if (node_is_subgraph(n)) {
                    MSG_DEBUG_INDENT_EXPR("      ");
                    subgraph(n)->dump();
                    MSG_DEBUG_DEDENT;
                }
            });
        }

    void
        build_subgraphs()
        {
            m_subgraphs.clear();
            m_subgraphs.resize(size());
            foreach_in_if(nodes(),
                    [this] (node_index_type n) { return node_is_subgraph(n); },
                    [this] (node_index_type n) {
                        m_subgraphs[n] = make_subgraph(n);
                        m_subgraphs[n]->build_subgraphs();
                    });
        }

    void
        postprocess()
        override
        {
            graph_type::postprocess();
            add_missing_interfaces();
            join_trees();
            foreach_in_if(nodes(),
                    [this] (node_index_type n) { return node_is_subgraph(n); },
                    [this] (node_index_type n) { m_subgraphs[n]->postprocess(); });
        }

    node_index_type
        index_in_parent() const { return m_index_in_parent; }

    const Derived*
        parent() const { return m_parent; }

    std::shared_ptr<Derived>
        subgraph(node_index_type n) const { return m_subgraphs[n]; }

protected:
    node_index_type
        create_interface(const var_vec& vv)
        {
            if (vv.size() == 1) {
                return add_interface(vv.front());
            } else {
                return add_interface(vv);
            }
        }

private:
    void
        set_parent(const Derived* p, node_index_type i)
        {
            m_parent = p;
            m_index_in_parent = i;
        }

    std::shared_ptr<Derived>
        make_subgraph(node_index_type n)
        {
            /*scoped_indent _(SPELL_STRING("[subgraph " << n << "] "));*/
            /*MSG_DEBUG("Creating subgraph...");*/
            std::shared_ptr<Derived> ret = dynamic_cast<Derived*>(this)->create_subgraph(n);
            foreach_in_if(inner_nodes(n),
                    [&,this] (node_index_type i) { return node_is_factor(i); },
                    [&,this] (node_index_type i) {
                        dynamic_cast<Derived*>(this)->add_node_to_subgraph(ret, i);
                    });
            ret->set_parent(dynamic_cast<const Derived*>(this), n);
            return ret;
        }

    void
        add_interface_from(node_index_type from, const var_vec& vv)
        {
            path_type dont_care;
            add_edge(from, create_interface(vv), dont_care);
        }

    void
        add_interface_to(const var_vec& vv, node_index_type to)
        {
            path_type dont_care;
            add_edge(create_interface(vv), to, dont_care);
        }

    void
        create_interfaces_between_factors()
        {
            auto not_interface = [this] (node_index_type n) { return !node_is_interface(n); };
            /* create interfaces between factors/subgraphs */
            foreach_in_if(nodes(), not_interface,
                [&,this] (node_index_type from) {
                    auto nout = nei_out(from);
                    foreach_in_if(nout, not_interface,
                        [&,this] (node_index_type to) {
                            insert_interface_between(from, to);
                        });
                });
        }

    void
        export_ancestral_interfaces()
        {
            /* export ancestral interfaces from subgraphs if missing */
            foreach_in_if(nodes(),
                [this] (node_index_type n) { return node_is_subgraph(n); },
                [this] (node_index_type to) {
                    auto in_var = variables_of(nei_in(to));
                    subgraph(to)->foreach_in_if(subgraph(to)->nodes(),
                        [&,this] (node_index_type in) { return subgraph(to)->nei_in(in).size() == 0; },
                        [&,this] (node_index_type in) {
                            auto vv = subgraph(to)->variables_of(in) - in_var;
                            if (vv.size() > 0) {
                                for (variable_index_type v: vv) {
                                    add_interface_to({v}, to);
                                }
                            }
                        });
                });
        }

    void
        create_output_interfaces()
        {
            /* create output interfaces from factors */
            foreach_in_if(nodes(),
                [this] (node_index_type n) { return node_is_factor(n); },
                [this] (node_index_type from) {
                    var_vec out = var_vec{own_variable_of(from)} - variables_of(nei_out(from));
                    if (out.size()) {
                        add_interface_from(from, out);
                    }
                });
        }

    void
        add_missing_interfaces()
        {
            create_interfaces_between_factors();
            export_ancestral_interfaces();
            create_output_interfaces();
        }

    void
        join_trees()
        {
            std::map<colour_proxy, node_vec> tree_roots;
            foreach_in_if(nodes(),
                    [this] (node_index_type n) { return node_is_interface(n) && nei_out(n).size() == 0; },
                    [&,this] (node_index_type n) { tree_roots[colour_of(n)].push_back(n); });
            /*MSG_DEBUG("tree roots " << tree_roots);*/
            if (tree_roots.size() > 1) {
                node_vec nn;
                /*for (const auto& kv: tree_roots) { nn.insert(nn.end(), kv.second.begin(), kv.second.end()); }*/
                for (const auto& kv: tree_roots) { nn.push_back(kv.second.front()); }
                aggregate(nn);
            }
        }

    std::vector<std::shared_ptr<Derived>> m_subgraphs;
    const Derived* m_parent;
    node_index_type m_index_in_parent;
};




inline
std::vector<pedigree_item>
filter_pedigree(const std::vector<pedigree_item>& full_pedigree, const std::vector<std::string>& inputs, const std::vector<std::string>& outputs)
{
    std::vector<pedigree_item> ret;
    std::map<int, pedigree_item> items;
    std::map<int, bool> protect;
    for (const auto& p: full_pedigree) {
        protect[p.id] = std::find(inputs.begin(), inputs.end(), p.gen_name) != inputs.end()
                     || std::find(outputs.begin(), outputs.end(), p.gen_name) != outputs.end();
    }
    auto pi = full_pedigree.rbegin(), pj = full_pedigree.rend();
    for (; pi != pj; ++pi) {
        if (protect[pi->id]) {
            if (pi->p1) { protect[pi->p1] = true; }
            if (pi->p2) { protect[pi->p2] = true; }
        }
    }
    ret.reserve(full_pedigree.size());
    for (const auto& p: full_pedigree) {
        if (protect[p.id]) {
            ret.emplace_back(p);
        }
    }
    return ret;
}





struct factor_graph_type : public recursive_graph_type<factor_graph_type> {
    std::map<variable_index_type, VariableIO> io;
    std::map<var_vec, genotype_comb_type> domains;
    /*const factor_graph_type* parent;*/
    /*node_index_type index_in_parent;*/
    size_t n_alleles;
    std::map<node_index_type, pedigree_item> pedigree_items;
    std::map<variable_index_type, bool> is_dh;
    std::vector<message_type> node_domains;
    std::map<node_index_type, node_index_type> anchor_points;
    std::vector<bool> node_domain_computed;
    bool keep_gametes;
    std::vector<int> gametes;

    graph_type& operator = (graph_type&& other) = delete;
    factor_graph_type(const graph_type& other) = delete;

    factor_graph_type(bool keepgam, size_t n_al=1)
        : io(), domains(), /*parent(nullptr), index_in_parent(0),*/ n_alleles(n_al), is_dh(), node_domains(), anchor_points(), node_domain_computed(), keep_gametes(keepgam)
    {}

    std::shared_ptr<factor_graph_type>
        create_subgraph(node_index_type n)
        {
            auto ret = std::make_shared<factor_graph_type>(n_alleles);
            ret->io = io;
            for (variable_index_type v: variables_of(nei_out(n))) {
                ret->io[v] |= Output;
            }
            foreach_in(nei_in(n),
                    [&,this] (node_index_type i) {
                        var_vec V = variables_of(i) % variables_of(n);
                        for (variable_index_type v: V) {
                            ret->io[v] |= Input;
                        }
                        ret->create_interface(V);
                    });
            /*for (variable_index_type v: variables_of(nei_in(n)) % variables_of(n)) {*/
                /*ret->io[v] |= Input;*/
                /*ret->add_interface(v);*/
            /*}*/
            return ret;
        }

    void
        add_node_to_subgraph(std::shared_ptr<factor_graph_type> sub, node_index_type n)
        {
            /*scoped_indent _(SPELL_STRING("[add_node_to_subgraph " << n << "] "));*/
            variable_index_type v = own_variable_of(n);
            sub->add_variable(rule(n), v);
            sub->is_dh[v] = is_dh[v];
        }

    static
        std::unique_ptr<factor_graph_type>
        from_pedigree(const std::vector<pedigree_item>& ped, size_t n_al, bool keep_gametes, const std::vector<std::string>& input_gen, const std::vector<std::string>& output_gen/*, const std::string& debug_prefix = ""*/)
        {
            std::unique_ptr<factor_graph_type> g(new factor_graph_type(keep_gametes, n_al));
            /*system("rm -vf test_*.png");*/
            for (const auto& p: filter_pedigree(ped, input_gen, output_gen)) {
                /*MSG_DEBUG("-------------------------------------------------------------------------------");*/
                /*MSG_DEBUG("-------------------------------------------------------------------------------");*/
                /*MSG_DEBUG("-------------------------------------------------------------------------------");*/
                VariableIO io = None;
                if (std::find(input_gen.begin(), input_gen.end(), p.gen_name) != input_gen.end()) {
                    io |= Input;
                }
                if (output_gen.size() == 0 || std::find(output_gen.begin(), output_gen.end(), p.gen_name) != output_gen.end()) {
                    io |= Output;
                }
                g->io[p.id] = io;
                g->is_dh[p.id] = p.is_dh();
                if (p.is_ancestor()) {
                    g->add_variable(var_vec{}, p.id);
                } else if (p.is_cross()) {
                    g->add_variable({p.p1, p.p2}, p.id);
                } else if (p.is_dh()) {
                    g->add_variable({p.p1}, p.id);
                } else if (p.is_self()) {
                    g->add_variable({p.p1}, p.id);
                }
                /*if (io) {*/
                    /*MSG_DEBUG("IO for var #" << p.id << ": " << io);*/
                /*}*/
            }
            /*g->finalize();*/
            /*g->compute_domains_and_factors();*/
            g->postprocess();
            g->finalize();
            return g;
        }

    VariableIO
        node_io(node_index_type n) const
        {
            VariableIO ret = None;
            if (node_is_factor(n)) {
                return io.find(own_variable_of(n))->second;
            }
            for (variable_index_type v: variables_of(n)) {
                ret |= io.find(v)->second;
            }
            return ret;
        }

    std::string
        to_image(const std::string& prefix, const std::string& format) const
        {
            auto A = nodes();
            std::string dotfilename = SPELL_STRING("/tmp/prout" << prefix); //std::tempnam(nullptr);
            std::vector<std::string> sub_filenames;
            {
                std::ofstream dotfile(dotfilename);
                dotfile << "digraph spell {rankdir=LR;" << std::endl;
                if (parent() != NULL) {
                    dotfile << "bgcolor=transparent;" << std::endl;
                }
                for (node_index_type n: A) {
                    std::string col;
                    auto io_val = node_io(n);
                    if (io_val == (Input | Output)) {
                        col = ", style=filled, fillcolor=mistyrose2";
                    } else if (io_val == Input) {
                        col = ", style=filled, fillcolor=lightsteelblue1";
                    } else if (io_val == Output) {
                        col = ", style=filled, fillcolor=darkseagreen1";
                    } else {
                        col = ", style=filled, fillcolor=white";
                    }
                    dotfile << n << "[";
                    if (node_is_interface(n)) {
                        dotfile << "shape=diamond, label=\"" << n  << " [" << inner_nodes(n) << "]   " << variables_of(n) << "\"";
                    } else if (node_is_subgraph(n)) {
                        std::string filename = subgraph(n)->to_image(SPELL_STRING("_sub_" << prefix << '_' << n), format);
                        dotfile << "shape=box,labeljust=\"l\", labelloc=\"t\", label=\"" << n << " [" << inner_nodes(n) << "]\",image=\"" << filename << "\"";
                        sub_filenames.push_back(filename);
                    } else {
                        dotfile << "shape=box,label=\"" << n << "   " << variables_of(n) << "\"";
                    }
                    dotfile << col << "];" << std::endl;
                }
                for (node_index_type h: A) {
                    for (node_index_type t: nei_out(h)) {
                        dotfile << h << " -> " << t << std::endl;
                    }
                }
                dotfile << '}' << std::endl;
            }
            std::string ret = SPELL_STRING(prefix << '.' << format);
            std::string cmd = SPELL_STRING("dot -T" << format << ' ' << dotfilename << " -o " << ret);
            if (system(cmd.c_str())) {
                MSG_ERROR("Dot failed " << strerror(errno), "");
            }
            for (const auto& s: sub_filenames) { unlink(s.c_str()); }
            return ret;
        }

    void
        dump_node(node_index_type n)
        override
        {
            graph_base_type::dump_node(n);
            if (node_domains.size()) {
                MSG_DEBUG("      Domain " << node_domains[n]);
            }
        }

    void
        postprocess()
        override
        {
            scoped_indent _("[postprocess] ");
            MSG_DEBUG("BUILD SUBGRAPHS");
            build_subgraphs();
            MSG_DEBUG("RECURSE THE POSTPROCESSING");
            recursive_graph_type<factor_graph_type>::postprocess();
            MSG_DEBUG("CREATE ANCESTOR DOMAINS");
            create_ancestor_domains();
            MSG_DEBUG("DONE.");
        }

    void
        finalize()
        {
            scoped_indent _(SPELL_STRING("[finalize] "));
//            to_image("pre-optim", "png");
            MSG_DEBUG("DOMAINS");
            compute_domains_and_factors();
            MSG_DEBUG("OPTIMIZE");
            optimize();
            MSG_DEBUG("RUNNING INTERSECTIONS");
            running_intersections();
            MSG_DEBUG("DONE.");
        }

    void
        running_intersections()
        {
            if (parent()) {
                std::vector<var_vec> all_nei_vars;
                node_vec nei_nodes;
                nei_nodes = parent()->nei_in(index_in_parent()) + parent()->nei_out(index_in_parent());
                all_nei_vars.reserve(nei_nodes.size());
                /*MSG_DEBUG("NEI NODES " << nei_nodes);*/
                for (node_index_type n: nei_nodes) {
                    all_nei_vars.push_back(parent()->variables_of(n) % parent()->variables_of(index_in_parent()));
                }
                /*MSG_DEBUG("ALL NEI VARS " << all_nei_vars);*/
                /*reset_annotations();*/
                for (const auto& ia: anchor_incoming(all_nei_vars)) {
                    anchor_points[nei_nodes[ia.first]] = ia.second;
                }
            }
            foreach_in_if(nodes(),
                    [this] (node_index_type n) { return node_is_subgraph(n); },
                    [this] (node_index_type n) { subgraph(n)->running_intersections(); });
        }


    bool
        var_is_output(variable_index_type var) const
        {
            auto mode = io.find(var)->second;
            return mode & Output;
        }

    std::vector<bn_label_type>
        get_domain(variable_index_type var)
        {
            std::vector<bn_label_type> ret;
            const auto& dom = domains[{var}];
            ret.reserve(dom.size());
            for (const auto& e: dom) {
                ret.push_back(e.keys.keys.front().state);
            }
            return ret;
        }

    bool
        variables_are_independent(variable_index_type v1, variable_index_type v2, const std::list<node_index_type>& path) const
        {
            var_vec V = v1 < v2 ? var_vec{v1, v2} : var_vec{v2, v1};
            for (node_index_type n: path) {
                if (!(variables_of(n) % V).size()) {
                    return true;
                }
            }
            return false;
        }

    std::map<variable_index_type, node_index_type>
        get_output_interfaces(const var_vec& V) const
        {
            std::map<variable_index_type, node_index_type> ret;
            std::map<variable_index_type, node_vec> candidates;
            std::map<node_index_type, size_t> candidate_scores;
            foreach_in_if(nodes(),
                [this] (node_index_type n) { return !node_is_interface(n); },
                [&,this] (node_index_type n) {
                    auto vn = variables_of(n) % V;
                    if (vn.size()) {
                        candidate_scores[n] += vn.size();
                        for (auto v: vn) {
                            candidates[v].push_back(n);
                        }
                    }
                });

            for (auto& vn: candidates) {
                std::sort(vn.second.begin(), vn.second.end(), [&](node_index_type n1, node_index_type n2) { return candidate_scores[n1] > candidate_scores[n2]; });
                ret[vn.first] = vn.second.front();
            }

            return ret;
        }

    node_index_type
        find_anchor(const var_vec& V)
        {
            std::map<node_index_type, size_t> candidate_scores;
            foreach_in_if(nodes(),
                [this] (node_index_type n) { return node_is_interface(n); },
                [&,this] (node_index_type n) {
                    auto vn = variables_of(n) % V;
                    if (vn.size()) {
                        candidate_scores[n] = vn.size();
                    }
                });
            size_t max = 0;
            node_index_type best = (node_index_type) -1;
            for (const auto& kv: candidate_scores) {
                if (kv.second > max) {
                    max = kv.second;
                    best = kv.first;
                }
            }
            return best;
        }

    bool
        recursive_find_path_to_var(node_index_type node, variable_index_type v, path_type& path, std::vector<bool>& visited) const
        {
            auto V = variables_of(node);
            if (std::find(V.begin(), V.end(), v) != V.end()) {
                /*path.push_back(node);*/
                return true;
            }
            visited[node] = true;
            for (node_index_type n: nei_in(node)) {
                if (!visited[n] && recursive_find_path_to_var(n, v, path, visited)) {
                    path.push_back(n);
                    return true;
                }
            }
            for (node_index_type n: nei_out(node)) {
                if (!visited[n] && recursive_find_path_to_var(n, v, path, visited)) {
                    path.push_back(n);
                    return true;
                }
            }
            return false;
        }

    std::list<node_index_type>
        find_path_to_var(node_index_type start, variable_index_type v)
        {
            std::vector<bool> visited(size(), false);
            visited[start] = true;
            std::list<node_index_type> path;
            recursive_find_path_to_var(start, v, path, visited);
            return path;
        }

    void
        annotate(node_index_type anchor, const var_vec& V)
        {
            for (variable_index_type v: V) {
                for (node_index_type n: find_path_to_var(anchor, v)) {
                    /*annotations[n].push_back(v);*/
                    add_variables_to_node(n, {v});
                }
            }
            add_variables_to_node(anchor, V);
        }

    std::map<size_t, node_index_type>
        anchor_incoming(const std::vector<var_vec>& incoming)
        {
            std::map<size_t, node_index_type> ret;
            std::vector<size_t> indices(incoming.size());
            size_t index = 0;
            std::generate(indices.begin(), indices.end(), [&] () { return index++; });
            std::sort(indices.begin(), indices.end(), [&] (size_t i, size_t j) { return incoming[i].size() > incoming[j].size(); });
            for (size_t i: indices) {
                node_index_type anchor = find_anchor(incoming[i]);
                annotate(anchor, incoming[i]);
                ret[i] = anchor;
            }
            /*sort_annotations();*/
            /*MSG_DEBUG("ANCHORS " << ret);*/
            return ret;
        }


    std::list<node_index_type>
        find_vpath(variable_index_type v1, variable_index_type v2) const
        {
            node_vec nv1, nv2;
            std::list<node_index_type> ret;
            if (v1 > v2) {
                v1 ^= v2;
                v2 ^= v1;
                v1 ^= v2;
            }
            var_vec V = {v1, v2};
            /*MSG_DEBUG("v1 " << v1 << " v2 " << v2);*/
            foreach_in_if(nodes(),
                [this] (node_index_type n) { return node_is_factor(n); },
                [&,this] (node_index_type n) {
                    for (variable_index_type v: V % variables_of(n)) {
                        (v == v1 ? nv1 : nv2).push_back(n);
                    }
                });
            /*MSG_DEBUG("nv1 " << nv1 << " nv2 " << nv2);*/
            auto joint_itf = nv1 % nv2;
            if (joint_itf.size()) {
                return {*std::min_element(joint_itf.begin(), joint_itf.end(), [this](node_index_type n1, node_index_type n2) { return variables_of(n1).size() < variables_of(n2).size(); })};
            }
            size_t path_size = (size_t) -1;
            for (auto n1: nv1) {
                for (auto n2: nv2) {
                    if (!colour_equal(colour_of(n1), colour_of(n2))) {
                        return {};
                    }
                    auto path = find_path(n1, n2);
                    /*MSG_DEBUG("n1 " << n1 << " n2 " << n2 << " path " << path);*/
                    size_t psz = path.size();
                    if (psz && psz < path_size) {
                        path_size = psz;
                        path.swap(ret);
                    }
                }
            }
            return ret;
        }

    /*std::map<colour_proxy, std::vector<std::list<node_index_type>>>*/
    std::vector<std::vector<std::list<node_index_type>>>
        find_vpath(var_vec vv) const
        {
            /*std::map<colour_proxy, std::vector<std::list<node_index_type>>> ret;*/
            std::map<variable_index_type, std::vector<node_vec>> nv;
            std::map<colour_proxy, var_vec> by_colour;
            for (auto v: vv) {
                auto nn = filter(nodes(), [&] (node_index_type n) { auto vv = variables_of(n); return std::find(vv.begin(), vv.end(), v) != vv.end(); });
                /*MSG_DEBUG("variable " << v << " nodes " << nn);*/
//                for (auto n: nn) {
//                    MSG_DEBUG("  node colour " << colour_of(n));
//                }
                if (nn.size()) {
                    auto col = colour_of(nn.front());
                    nv[v].push_back(nn);
                    by_colour[col].push_back(v);
                }
            }
            std::vector<var_vec> cliques;
            std::map<std::pair<variable_index_type, variable_index_type>, std::list<node_index_type>> paths;
            std::vector<std::vector<std::list<node_index_type>>> paths_by_clique;
            std::map<variable_index_type, size_t> var_clique;
            for (auto& kv: by_colour) {
                /*MSG_DEBUG("In component " << kv.second);*/
                if (kv.second.size() > 1) {
                    auto i = kv.second.begin(), j = kv.second.end();
                    for (; i != j; ++i) {
                        if (var_clique.find(*i) == var_clique.end()) {
                            var_clique[*i] = cliques.size();
                            cliques.emplace_back();
                            cliques.back().push_back(*i);
                            paths_by_clique.emplace_back();
                            /*MSG_DEBUG("Variable " << (*i) << " is in a new clique");*/
                        }
                        for (auto k = i + 1; k != j; ++k) {
                            /*MSG_DEBUG("Find path between " << (*i) << " and " << (*k));*/
                            auto path = find_vpath(*i, *k);
                            if (path.size()) {
                                paths[{*i, *k}] = path;
                                paths_by_clique[var_clique[*i]].emplace_back(path);
                                cliques[var_clique[*i]].push_back(*k);
                                var_clique[*k] = var_clique[*i];
                            }
                        }
                        if (paths_by_clique[var_clique[*i]].size() == 0) {
                            paths_by_clique[var_clique[*i]].emplace_back();
                            /*paths_by_clique[var_clique[*i]].back().emplace_back(*i);*/
                            paths_by_clique[var_clique[*i]].back().emplace_back(nv[*i].front().front());
                        }
                    }
                    /*MSG_DEBUG("var_clique " << var_clique);*/
                } else {
                    auto var = kv.second.front();
                    /*MSG_DEBUG("var is single " << var);*/
                    var_clique[var] = cliques.size();
                    cliques.emplace_back();
                    cliques.back().push_back(var);
                    paths_by_clique.emplace_back();
                    paths_by_clique.back().emplace_back();
                    /*paths_by_clique.back().emplace_back(var);*/
                    /*paths_by_clique.back().emplace_back(find_vpath(var, var));*/
                    paths_by_clique.back().back().emplace_back(nv[var].front().front());
                }
            }
            /*MSG_DEBUG("cliques " << cliques);*/
            /*MSG_DEBUG("paths " << paths);*/
            /*MSG_DEBUG("paths_by_clique " << paths_by_clique);*/

            return paths_by_clique;
        }



    message_type
        get_joint_domain(const var_vec& varset)
        {
            /*MSG_QUEUE_FLUSH();*/
            /*scoped_indent _(SPELL_STRING("[get_joint_domain " << varset << "] "));*/
            /*MSG_DEBUG("[get_joint_domain " << varset << "] ");*/
            auto path = find_vpath(varset);
            message_type ret;
            for (const auto& cliq: path) {
                ret.emplace_back();
                message_type accum;
                for (const auto& path: cliq) {
                    message_type tmp;
                    for (node_index_type n: path) {
                        accumulate(tmp, get_node_domain(n), domains);
                        tmp %= varset + variables_of(n);
                        /*MSG_DEBUG("tmp " << tmp);*/
                    }
                    accumulate(accum, tmp, domains);
                    accum %= varset;
                }
                accumulate(ret, accum, domains);
            }
            return ret;
        }

    message_type
        get_node_domain(node_index_type n)
        {
            if (node_is_subgraph(n)) {
                if (!node_domain_computed[n]) {
                    subgraph(n)->compute_domains_and_factors();
                    node_domain_computed[n] = true;
                }
                return subgraph(n)->get_joint_domain(variables_of(n));
            } else {
                if (!node_domain_computed[n]) {
                    compute_node_domain(n);
                }
                return node_domains[n];
            }
        }

    message_type
        get_node_domain(node_index_type n, const var_vec& vv)
        {
            /*MSG_DEBUG("get_node_domain(" << n << ", " << vv << ") " << variables_of(n));*/
            if (node_is_subgraph(n)) {
                if (!node_domain_computed[n]) {
                    subgraph(n)->compute_domains_and_factors();
                    node_domain_computed[n] = true;
                }
                return subgraph(n)->get_joint_domain(vv);
            } else {
                if (!node_domain_computed[n]) {
                    compute_node_domain(n);
                }
                var_vec vo = variables_of(n);
                if (vv % vo == vo) {
                    return node_domains[n];
                }
                return node_domains[n] % vv;
            }
        }

    void
        propagate_spawnling_domain_rec(const var_vec& V, const genotype_comb_type& dom, std::map<const graph_type*, bool>& visited)
        {
            /*if (visited[this]) {*/
                /*return;*/
            /*}*/
            visited[this] = true;
            domains[V] = dom;
            /*MSG_DEBUG("Domains " << (parent() ? SPELL_STRING(parent() << "->" << index_in_parent()) : std::string("top-level")) << ' ' << domains);*/
            if (parent() && !visited[parent()]) {
                const_cast<factor_graph_type*>(parent())->propagate_spawnling_domain(V, dom);
            }
            foreach_in_if(nodes(),
                    [&,this] (node_index_type n) { return node_is_subgraph(n) && !visited[subgraph(n).get()]; },
                    [&,this] (node_index_type n) {
                        subgraph(n)->propagate_spawnling_domain_rec(V, dom, visited);
                    });
        }

    void
        propagate_spawnling_domain(const var_vec& V, const genotype_comb_type& dom)
        {
            std::map<const graph_type*, bool> visited;
            propagate_spawnling_domain_rec(V, dom, visited);
        }

    void
        create_ancestor_domains()
        {
            foreach_ancestor([this] (variable_index_type id, char letter) {
                auto& domain = domains[{id}];
                for (char al = 0; al < (char) n_alleles; ++al) {
                    domain.m_combination.emplace_back(genotype_comb_type::element_type{{{id, {letter, letter, al, al}}}, 1.});
                }
            });
        }

    void
        compute_node_domain(node_index_type n)
        {
            /*scoped_indent _(SPELL_STRING("[compute_node_domain #" << n << "] "));*/
            auto inputs = nei_in(n);
            auto vv = variables_of(n);
            if (node_is_interface(n)) {
                multiple_product_type mp;
                std::vector<message_type> stack;
                for (node_index_type i: inputs) {
                    stack.emplace_back(get_node_domain(i, vv));
                }
                for (const auto& m: stack) {
                    mp.add(m);
                }
                node_domains[n] = mp.compute(vv, domains);
                for (auto& dom: node_domains[n]) {
                    for (auto& e: dom) {
                        e.coef = 1;
                    }
                }
            } else if (node_is_subgraph(n)) {
                subgraph(n)->compute_domains_and_factors();
            } else /* factor */ {
                //// DEBUG : quick (and dirty) workaround to deal with clang++ issue on macos
                ////scoped_indent _(SPELL_STRING("[compute_factor {" << rule_of(n) << "} => " << own_variable_of(n) << "] "));
                scoped_indent _(SPELL_STRING("[compute_factor {" << vec_to_spell_string(rule_of(n)) << "} => " << own_variable_of(n) << "] "));
                MSG_DEBUG(" ### ### COMPUTING FACTOR {" << rule_of(n) << "} => " << own_variable_of(n) << " ### ###");
                MSG_QUEUE_FLUSH();
                joint_variable_product_type jvp;

                variable_index_type spawnling = own_variable_of(n);
                var_vec varset = variables_of(n);
                std::sort(varset.begin(), varset.end());
                varset = varset - var_vec{spawnling};
                /*for (node_index_type o: nei_out(n)) {*/
                    /*varset = varset + variables_of(o);*/
                /*}*/
                message_type stack;
                stack.reserve(64);  /* FIXME arbitrary value */
                for (node_index_type i: inputs) {
                    MSG_DEBUG("[jpar_dom] varset " << varset);
                    auto idom = get_node_domain(i, varset);
                    MSG_DEBUG("[jpar_dom] from domain " << idom);
                    for (const auto& t: idom) {
                        stack.emplace_back(t);
                    }
                }

                            /*jvp.add_table(stack.back());*/
                            /*MSG_DEBUG(" * ADDING " << stack.back());*/
                        /*}*/
                    /*}*/
                /*}*/
                /*jvp.set_output(varset);*/
                /*jvp.compile(domains);*/
                /*auto jpar_dom = jvp.compute();*/
                genotype_comb_type jpar_dom;
                /*MSG_QUEUE_FLUSH();*/
                if (stack.size() > 1) {
                    MSG_DEBUG("Domains " << (parent() ? SPELL_STRING(parent() << "->" << index_in_parent()) : std::string("top-level")) << ' ' << domains);
                    jpar_dom = compute_product(stack.begin(), stack.end(), varset, domains);
                } else {
                    jpar_dom = stack.front();
                }
                MSG_DEBUG("[jpar_dom] stack " << stack);
                MSG_DEBUG("[jpar_dom] result " << jpar_dom);
                node_domains[n].emplace_back(generate_factor(rule_of(n), is_dh[n], spawnling, jpar_dom, domains, keep_gametes));
                MSG_DEBUG("keep_gametes " << std::boolalpha << keep_gametes);
                if (keep_gametes) {
                    gametes.emplace_back(-spawnling);
                    io[-spawnling] = Output;
                    add_variables_to_node(n, {-spawnling});
                }
                /* Add domain for spawnling */
                MSG_DEBUG("node_domains[n] " << node_domains[n]);
                MSG_DEBUG("spawnling vec " << var_vec{spawnling});
                {
                    auto msg = (node_domains[n] % var_vec{spawnling});
                    auto dom = msg.front();
                    for (auto& e: dom) {
                        e.coef = 1;
                    }
                    propagate_spawnling_domain({spawnling}, dom);
                }
                if (keep_gametes)
                {
                    auto msg = (node_domains[n] % var_vec{-spawnling});
                    auto dom = msg.front();
                    for (auto& e: dom) {
                        e.coef = 1;
                    }
                    propagate_spawnling_domain({-spawnling}, dom);
                }
                MSG_DEBUG("Domain for spawnling #" << spawnling << ": " << domains[{spawnling}]);
                MSG_DEBUG("Variables for corresponding node #" << n << " are " << variables_of(n));
            }
            /*MSG_DEBUG("COMPUTED DOMAIN " << node_domains[n]);*/
            dump_node(n);
            node_domain_computed[n] = true;
        }

    void
        compute_domains_and_factors()
        {
            /*scoped_indent _(SPELL_STRING("[compute_domains_and_factors " << (parent() ? SPELL_STRING(parent() << "->" << index_in_parent()) : std::string("top-level")) << "] "));*/
            /*MSG_DEBUG("n_alleles = " << n_alleles);*/
            if (parent()) {
                domains = parent()->domains;
            }
            node_domains.clear();
            node_domains.resize(size());
            node_domain_computed.clear();
            node_domain_computed.resize(size());
            /*MSG_DEBUG("Domains " << domains);*/

            auto active = nodes();

            foreach_in_if(active,
                [this] (node_index_type n) { return node_is_interface(n) && nei_in(n).size() == 0; },
                [&,this] (node_index_type n) {
                    auto vv = variables_of(n);
                    if (parent() == NULL) {
                        /* ancestor(s) ! */
                        for (variable_index_type v: vv) {
                            node_domains[n].emplace_back(domains[{v}]);
                        }
                        node_domain_computed[n] = true;
                        /*MSG_DEBUG("Pre-filled ancestor interface #" << n << " domain " << node_domains[n]);*/
                    } else {
                        multiple_product_type mp;
                        std::vector<message_type> stack;
                        foreach_in(parent()->nei_in(index_in_parent()),
                            [&,this] (node_index_type i) {
                                /*MSG_DEBUG("SUBGRAPH EXTERNAL INPUT #" << i);*/
                                stack.emplace_back(const_cast<factor_graph_type*>(parent())->get_node_domain(i, vv));
                                /*MSG_DEBUG("Have domain " << stack.back());*/
                            });
                        for (const auto& m: stack) {
                            mp.add(m);
                        }
                        node_domains[n] = mp.compute(vv, domains);
                        node_domain_computed[n] = true;
                        /*MSG_DEBUG("HAVE DOMAIN FOR INPUT #" << n << ' ' << node_domains[n]);*/
                    }
                });

            for (node_index_type n: active) {
                if (!node_domain_computed[n]) {
                    /*MSG_QUEUE_FLUSH();*/
                    compute_node_domain(n);
                }
            }
            
            std::sort(gametes.begin(), gametes.end());

            /*MSG_DEBUG("domains computes " << node_domain_computed);*/

            /*for (node_index_type n: active) {*/
                /*dump_node(n);*/
                /*MSG_DEBUG("=========================================================");*/
            /*}*/
        }

    bool
        is_unprotected(node_index_type n) const
        {
            /*return node_io(n) != None;*/
            for (variable_index_type v: variables_of(n)) {
                if (io.find(v)->second != None) { return false; }
            }
            return true;
        }

    bool
        has_single_input(node_index_type n) const
        {
            return nei_in(n).size() == 1;
        }

    bool
        has_single_output(node_index_type n) const
        {
            return nei_out(n).size() == 1;
        }

    bool
        has_no_output(node_index_type n) const
        {
            return nei_out(n).size() == 0;
        }

    void
        squeeze_factor(const node_vec& f1in, node_index_type f1, node_index_type i1, node_index_type f2)
        {
            /*MSG_DEBUG("squeeze_factor f1in " << f1in << " f1 " << f1 << " i1 " << i1 << " f2 " << f2);*/
            var_vec var_f1in;
            for (node_index_type i: f1in) {
                var_f1in = var_f1in + variables_of(i);
            }
            /*MSG_DEBUG("var_f1in " << var_f1in);*/
            var_vec var_f2out;
            for (node_index_type i: nei_out(f2)) {
                var_f2out = var_f2out + variables_of(i);
            }
            /*MSG_DEBUG("var_f2out " << var_f2out);*/
            var_vec var_fac = variables_of(f1) + variables_of(f2);
            auto final_varset = (var_f1in + var_f2out) % var_fac;
            /*auto in_f2_squeeze = variables_of(i1);*/
            /*foreach_node_in_if(if2, nei_in(f2), if2 != i1) {*/
                /*in_f2_squeeze = in_f2_squeeze - variables_of(if2);*/
            /*}*/
            /*auto final_varset = var_f1in + variables_of(f2) - in_f2_squeeze;*/
            /*MSG_DEBUG("computing product");*/
            multiple_product_type mp;
            message_type f1_subdom = get_node_domain(f1, var_f1in + variables_of(i1) + final_varset);
            message_type f2_subdom = get_node_domain(f2, var_f2out + variables_of(i1) + final_varset);
            /*MSG_DEBUG("have subdomains");*/
            mp.add(f1_subdom);
            mp.add(f2_subdom);
            node_domains[f2] = mp.compute(final_varset, domains); //product(node_domains[f1], node_domains[f2], domains) % varset;
            /*MSG_DEBUG("computed product");*/
            path_type dont_care;
            /*MSG_DEBUG("adding edges");*/
            foreach_in(f1in,
                [&,this] (node_index_type in) {
                    add_edge(in, f2, dont_care);
                });
            /*MSG_DEBUG("added edges");*/
            /*inner_nodes[f2].push_back(f1);*/
            /*neighbours_in[f2] = nei_in(f1) + nei_in(f2) - node_vec{i1};*/
            /*neighbours_out[i1] = nei_out(i1) - node_vec{f2};*/
            /*MSG_DEBUG("removing edge");*/
            remove_edge(i1, f2);
            if (has_no_output(i1)) {
                /*MSG_DEBUG("deleting node");*/
                delete_node(i1);
            }
            /*MSG_DEBUG("setting variables for f2");*/
            set_variables_for_node(f2, final_varset);
        }

    void
        optimize()
        {
            /*debug_graph("optimize", 0);*/
            scoped_indent _("[optimize] ");
            foreach_in_if(nodes(),
                [this] (node_index_type n) { return node_is_subgraph(n); },
                [this] (node_index_type n) { subgraph(n)->optimize(); });
            /*debug_graph("optimize", 1);*/
#if 1
            MSG_DEBUG("Squeeze intermediate subgraphs");
            foreach_in_if(nodes(),
                [this] (node_index_type f1) { return node_is_subgraph(f1) && has_single_output(f1) && is_unprotected(f1); },
                [this] (node_index_type f1) {
                    auto f1in = nei_in(f1);
                    auto of1 = nei_out(f1);
                    foreach_in_if(of1,
                        [this] (node_index_type i1) { return node_is_interface(i1) && has_single_output(i1); },
                        [&,this] (node_index_type i1) {
                            auto oi1 = nei_out(i1);
                            foreach_in_if(oi1,
                                [this] (node_index_type f2) { return node_is_factor(f2); },
                                [&,this] (node_index_type f2) {
                                    squeeze_factor(f1in, f1, i1, f2);
                                });
                        });
                });

            MSG_DEBUG("Squeeze intermediate factors");
            foreach_in_if(nodes(),
                [this] (node_index_type f1) { return node_is_factor(f1) && has_single_output(f1) && is_unprotected(f1); },
                [this] (node_index_type f1) {
                    auto f1in = nei_in(f1);
                    auto of1 = nei_out(f1);
                    foreach_in_if(of1,
                        [this] (node_index_type i1) { return node_is_interface(i1) && has_single_output(i1); },
                        [&,this] (node_index_type i1) {
                            auto oi1 = nei_out(i1);
                            foreach_in_if(oi1,
                                [this] (node_index_type f2) { return node_is_factor(f2); },
                                [&,this] (node_index_type f2) {
                                    squeeze_factor(f1in, f1, i1, f2);
                                });
                        });
                });
#endif
#if 1
            /* remove dangling interfaces that have no I/O */
            MSG_DEBUG("Purging dangling interfaces");
            foreach_in_if(nodes(),
                [this] (node_index_type n) { return has_no_output(n) && is_unprotected(n); },
                [this] (node_index_type n) { delete_node(n); });
            /*debug_graph("optimize", 4);*/
#endif
        }
};


struct message_compute_operation_type {
    std::vector<size_t> incoming_messages;
    std::vector<var_vec> incoming_domains;
    var_vec output;
    node_index_type emitter;
    node_index_type receiver;

    friend
        inline
        std::ostream&
        operator << (std::ostream& os, const message_compute_operation_type& mco)
        {
            return os << "{(" << mco.incoming_messages << " % " << mco.incoming_domains << ") x " << mco.emitter << " ==" << mco.output << "=> " << mco.receiver << '}';
        }

    template <typename IO>
        void
        file_io(IO& f)
        {
            rw_base rw;
            if (rw.fourcc(f, "MCpO")) { return; }
            rw(f, incoming_messages);
            rw(f, incoming_domains);
            rw(f, output);
            rw(f, emitter);
            rw(f, receiver);
        }
};

struct computation_variant_type {
    std::vector<size_t> outer_inputs;
    std::vector<node_index_type> anchors;
    std::vector<size_t> operations;
    /* FIXME must be a vector, in case of joint probabilities along a path, or multiple ones */
    std::vector<message_compute_operation_type> output_operations;

    template <typename IO>
        void
        file_io(IO& f)
        {
            rw_base rw;
            if (rw.fourcc(f, "CVar")) { return; }
            rw(f, outer_inputs);
            rw(f, anchors);
            rw(f, operations);
            rw(f, output_operations, [](IO& f, message_compute_operation_type& mco) { mco.file_io(f); });
        }

    friend
        inline
        std::ostream&
        operator << (std::ostream& os, const computation_variant_type& cv)
        {
            return os << "<variant outer=" << cv.outer_inputs << " anchors=" << cv.anchors << " ops=" << cv.operations << " output=" << cv.output_operations << ">";
        }
};

struct instance_type {
    std::vector<message_type> tables;
    std::vector<message_type> evidence;
    std::vector<message_type> internal_evidence;
    std::unordered_map<variable_index_type, node_index_type> evidence_affectations;
    std::vector<std::unique_ptr<instance_type>> sub_instances;
    const instance_type* parent;
    std::unordered_map<node_index_type, computation_variant_type> variants; /* computations for the given output (as key) */
    std::map<std::pair<node_index_type, node_index_type>, size_t> message_index;
    std::vector<message_type> messages; /* internal messages */
    std::vector<message_compute_operation_type> operations;

    template <typename IO>
        void
        file_io(IO& f)
        {
            rw_comb<int, bn_label_type> rw;
            if (rw.fourcc(f, "BN_I")) { return; }
            rw(f, tables);
            rw(f, evidence_affectations);
            /*rw(f, variants, [](IO& f, computation_variant_type& cv) { cv.file_io(f); });*/
            rw(f, variants);
            rw(f, message_index);
            /*rw(f, operations, [](IO& f, message_compute_operation_type& mco) { mco.file_io(f); });*/
            rw(f, operations);
            rw_sub_instances(f);
        }

    void
        rw_sub_instances(ifile& ifs)
        {
            rw_base rw;
            parent = nullptr;
            size_t n, i;
            rw(ifs, n);
            sub_instances.clear();
            sub_instances.resize(tables.size());
            for (; n; --n) {
                rw(ifs, i);
                auto& sub = sub_instances[i];
                sub.reset(new instance_type());
                sub->file_io(ifs);
                sub->parent = this;
            }
            messages.resize(message_index.size());
            clear_internal_evidence();
            evidence.resize(internal_evidence.size());
        }

    void
        rw_sub_instances(ofile& ofs)
        {
            rw_base rw;
            size_t n = 0;
            for (const auto& sub: sub_instances) if (sub) { ++n; }
            rw(ofs, n);
            for (size_t i = 0; i < sub_instances.size(); ++i) if (sub_instances[i]) {
                rw(ofs, i);
                sub_instances[i]->file_io(ofs);
            }
        }

    instance_type() : tables(), evidence(), evidence_affectations(), sub_instances(), parent(nullptr) {}
    instance_type(factor_graph_type* g, instance_type* par=nullptr)
        : tables(g->node_domains.size()), evidence(g->node_domains.size()), sub_instances(g->node_domains.size()), parent(par)
    {
        var_vec varz; for (node_index_type n: g->nodes()) { varz = varz + g->variables_of(n); }
        /*scoped_indent _(SPELL_STRING("[instance " << varz << "] "));*/
        size_t mi = 0;
        for (node_index_type n: g->nodes()) {
            if (g->node_is_interface(n) || g->node_is_factor(n)) {
                tables[n] = g->node_domains[n];
            }
            if (g->node_is_interface(n)) {
                for (variable_index_type v: g->variables_of(n)) {
                    if (evidence_affectations.find(v) == evidence_affectations.end()) {
                        evidence_affectations[v] = n;
                    }
                }
            }
            for (node_index_type o: g->nei_out(n)) {
                message_index[{n, o}] = mi++;
                message_index[{o, n}] = mi++;
            }
        }
        messages.resize(mi);
        operations.resize(mi);
        for (const auto& er_idx: message_index) {
            auto& op = operations[er_idx.second];
            std::tie(op.emitter, op.receiver) = er_idx.first;
            op.output = g->variables_of(op.emitter) % g->variables_of(op.receiver);
            auto nei = g->all_nei(op.emitter) - node_vec{op.receiver};
            op.incoming_messages.reserve(nei.size());
            for (node_index_type i: nei) {
                op.incoming_messages.push_back(get_message_index(i, op.emitter));
            }
        }
        for (const auto& er_idx: message_index) {
            compile_incoming_message_domains(operations[er_idx.second]);
            /*MSG_DEBUG(std::setw(4) << er_idx.second << "   " << operations[er_idx.second]);*/
        }
        build_variants(g);
        g->foreach_in_if(g->nodes(),
            [&] (node_index_type n) { return g->node_is_subgraph(n); },
            [&,this] (node_index_type n) {
                sub_instances[n].reset(new instance_type(g->subgraph(n).get(), this));
            });
    }

    void
        compile_incoming_message_domains(message_compute_operation_type& op)
        {
            op.incoming_domains.clear();
            op.incoming_domains.reserve(op.incoming_messages.size());
            for (size_t msg: op.incoming_messages) {
                var_vec other_dom = op.output;
                for (size_t other: op.incoming_messages) if (other != msg) {
                    other_dom = other_dom + operations[other].output;
                }
                op.incoming_domains.push_back(operations[msg].output % other_dom);
            }
        }

    size_t
        get_message_index(node_index_type emitter, node_index_type receiver) const
        {
            auto it = message_index.find({emitter, receiver});
            if (it == message_index.end()) {
                throw 0;
            }
            return it->second;
        }

    void
        add_evidence(variable_index_type v, const genotype_comb_type& obs)
        {
            node_index_type index = evidence_affectations[v];
            if (sub_instances[index]) {
                sub_instances[index]->add_evidence(v, obs);
            } else {
                /*MSG_DEBUG("evidence affectation for variable " << v << " is " << index);*/
                evidence[index].push_back(obs);
                /*MSG_DEBUG("evidence for node " << index << " is " << evidence[index]);*/
            }
        }

    void
        clear_internal_evidence()
        {
            /*MSG_DEBUG("CLEAR EVIDENCE");*/
            internal_evidence.clear();
            internal_evidence.resize(tables.size());
        }

    message_type
        compute_message(const message_compute_operation_type& op, const std::map<var_vec, genotype_comb_type>& domains)
        {
            //// DEBUG : quick (and dirty) workaround to deal with clang++ issue on macos
            ////scoped_indent _(SPELL_STRING("[compute message " << op.emitter << " % " << op.output << " -> " << op.receiver << "] "));
            scoped_indent _(SPELL_STRING("[compute message " << op.emitter << " % " << vec_to_spell_string(op.output) << " -> " << op.receiver << "] "));
            MSG_DEBUG("" << op);
            if (sub_instances[op.emitter]) {
                return sub_instances[op.emitter]->compute(op.receiver, domains);
            }
            multiple_product_type mp;
            MSG_DEBUG("evidence: " << evidence[op.emitter]);
            mp.add(evidence[op.emitter]);
            MSG_DEBUG("internal evidence: " << internal_evidence[op.emitter]);
            mp.add(internal_evidence[op.emitter]);
            MSG_DEBUG("table: " << tables[op.emitter]);
            MSG_DEBUG("incoming domains: " << op.incoming_domains);
            MSG_DEBUG("Add emitter");
            mp.add(tables[op.emitter]);
            auto inci = op.incoming_messages.begin(), incj = op.incoming_messages.end();
            auto domi = op.incoming_domains.begin();
            std::vector<message_type> msgs;
            msgs.reserve(op.incoming_messages.size());
            for (; inci != incj; ++inci, ++domi) {
                MSG_DEBUG("Add incoming");
                msgs.emplace_back(messages[*inci] % *domi);
                /*MSG_DEBUG("input #" << (*inci) << ": raw " << messages[*inci]);*/
                /*MSG_DEBUG("input #" << (*inci) << ": dom " << (*domi));*/
                /*MSG_DEBUG("input #" << (*inci) << ": " << msgs.back());*/
                /*mp.add(msgs.back());*/
                mp.add(messages[*inci]);
            }
            MSG_DEBUG("Compute.");
            auto ret = mp.compute(op.output, domains);
            MSG_DEBUG("" << ret);
            MSG_DEBUG("");
            return ret;
        }

    message_type
        compute(node_index_type n, const std::map<var_vec, genotype_comb_type>& domains)  /* compute an external message (this -> external node #n) */
        {
            const auto& variant = variants[n];
            /*scoped_indent _(SPELL_STRING("[compute " << n << "] "));*/
            /*MSG_DEBUG("" << variant);*/
            clear_internal_evidence();
            for (size_t i = 0; i < variant.outer_inputs.size(); ++i) {
                internal_evidence[variant.anchors[i]] = parent->messages[variant.outer_inputs[i]];
                /*MSG_DEBUG("Adding evidence at anchor #" << variant.anchors[i] <<": " << internal_evidence[variant.anchors[i]]);*/
            }
            for (size_t o: variant.operations) {
                const auto& op = operations[o];
                scoped_indent _(SPELL_STRING("[" << get_message_index(op.emitter, op.receiver) << "] "));
                messages[get_message_index(op.emitter, op.receiver)] = compute_message(op, domains);
                MSG_DEBUG("MESSAGES");
#ifndef SPELL_UNSAFE_OUTPUT // when SPELL_UNSAFE_OUTPUT not defined, MSG_DEBUG expands to nothing => we get "unused variable warnings"
                for (const auto& kv: message_index) {
                    MSG_DEBUG(std::setw(4) << kv.first.first << " -> " << std::setw(4) << kv.first.second << "  ==" << kv.second << "==  " << messages[kv.second]);
                }
#endif
            }
            message_type ret;
            for (const auto& op: variant.output_operations) {
                auto tmp = compute_message(op, domains);
                ret.insert(ret.end(), tmp.begin(), tmp.end());
            }
            /*MSG_DEBUG("MESSAGES");*/
            /*for (const auto& kv: message_index) {*/
                /*MSG_DEBUG(std::setw(4) << kv.first.first << " -> " << std::setw(4) << kv.first.second << "  ==" << kv.second << "==  " << messages[kv.second] << std::endl);*/
            /*}*/
            /*MSG_DEBUG("OUTPUT");*/
            /*for (const auto& t: ret) {*/
                /*MSG_DEBUG("" << t << std::endl);*/
            /*}*/
            return ret;
        }

    void
        get_message_order_rec(size_t final_message, std::vector<bool>& message_visited, std::vector<size_t>& ret)
        {
            for (size_t inc: operations[final_message].incoming_messages) {
                if (!message_visited[inc]) {
                    get_message_order_rec(inc, message_visited, ret);
                }
            }
            message_visited[final_message] = true;
            ret.push_back(final_message);
        }

    std::vector<size_t>
        get_message_order(size_t final_message)
        {
            std::vector<bool> message_visited(operations.size());
            std::vector<size_t> order;
            get_message_order_rec(final_message, message_visited, order);
            return order;
        }

    void
        build_variant(node_index_type towards, factor_graph_type* g, const std::map<size_t, node_index_type>& incoming, node_index_type output)
        {
            /* incoming is [external message index] => internal anchor node */
            auto& V = variants[towards];
            V.outer_inputs.clear();
            V.outer_inputs.reserve(incoming.size());
            V.anchors.clear();
            V.anchors.reserve(incoming.size());
            for (const auto& msg_anchor: incoming) {
                V.outer_inputs.push_back(msg_anchor.first);
                V.anchors.push_back(msg_anchor.second);
            }
            std::vector<bool> message_visited(operations.size());
            if (output != 0) {
                V.output_operations.emplace_back();
                auto& output_operation = V.output_operations.back();
                /*output_operation.output = g->variables_of(output);*/
                /*for (variable_index_type v: g->variables_of(output)) {*/
                    /*if (g->var_is_output(v)) {*/
                        /*output_operation.output.push_back(v);*/
                    /*}*/
                /*}*/
                output_operation.output = g->parent()->variables_of(towards);
                output_operation.receiver = 0;
                output_operation.emitter = output;
                for (node_index_type i: g->all_nei(output)) {
                    size_t m = get_message_index(i, output);
                    get_message_order_rec(m, message_visited, V.operations);
                    output_operation.incoming_messages.push_back(m);
                }
                compile_incoming_message_domains(output_operation);
            } else {
                for (node_index_type output: g->nodes()) {
                    MSG_DEBUG("active node " << output << " has io " << g->node_io(output) << " and variables " << g->variables_of(output));
                    if (g->node_is_interface(output)) {
                        if (g->node_io(output) & Output) {
                            V.output_operations.emplace_back();
                            auto& output_operation = V.output_operations.back();
                            /*output_operation.output = g->variables_of(output);*/
                            for (variable_index_type v: g->variables_of(output)) {
                                MSG_DEBUG("Output variable " << v << "?");
                                if (g->var_is_output(v)) {
                                    output_operation.output.push_back(v);
                                }
                            }
                            output_operation.receiver = 0;
                            output_operation.emitter = output;
                            for (node_index_type i: g->all_nei(output)) {
                                size_t m = get_message_index(i, output);
                                get_message_order_rec(m, message_visited, V.operations);
                                output_operation.incoming_messages.push_back(m);
                            }
                            compile_incoming_message_domains(output_operation);
                        }
                    } else {
                        MSG_DEBUG("node variables " << g->variables_of(output));
                        MSG_DEBUG("gametes " << g->gametes);
                        auto gam = g->variables_of(output) % g->gametes;
                        if (gam.size()) {
                            MSG_DEBUG("node has gametes " << gam);
                            for (variable_index_type v: gam) {
                                V.output_operations.emplace_back();
                                auto& output_operation = V.output_operations.back();
                                output_operation.output.push_back(v);
                                output_operation.receiver = 0;
                                output_operation.emitter = output;
                                for (node_index_type i: g->all_nei(output)) {
                                    size_t m = get_message_index(i, output);
                                    get_message_order_rec(m, message_visited, V.operations);
                                    output_operation.incoming_messages.push_back(m);
                                }
                                compile_incoming_message_domains(output_operation);
                            }
                        }
                    }
                }
            }
            /*MSG_DEBUG(this << ' ' << std::setw(4) << towards << ": " << V);*/
        }

    void
        build_variants(factor_graph_type* g)
        {
            /* FIXME: anchoring should account for forests. Maybe return a map [colour] => { [size_t] => node_index_type } */
            std::map<size_t, node_index_type> all_incoming;
            /*MSG_DEBUG("Anchor points " << g->anchor_points);*/
            for (const auto& ext_anchor: g->anchor_points) {
                node_index_type external = ext_anchor.first;
                if (!g->parent()->node_is_deleted(external)) {
                    /*MSG_DEBUG("ext " << ext_anchor.first << " anchor " << ext_anchor.second);*/
                    /*MSG_QUEUE_FLUSH();*/
                    all_incoming[parent->get_message_index(external, g->index_in_parent())] = ext_anchor.second;
                }
            }
            /*MSG_DEBUG("ALL INCOMING " << all_incoming);*/
            if (g->parent()) {
                auto nei_nodes = g->parent()->all_nei(g->index_in_parent());
                for (size_t i = 0; i < nei_nodes.size(); ++i) {
                    std::map<size_t, node_index_type> incoming;
                    node_index_type output = 0;
                    for (const auto& ext_anchor: g->anchor_points) {
                        node_index_type external = ext_anchor.first;
                        if (!g->parent()->node_is_deleted(external) && external != nei_nodes[i]) {
                            incoming[parent->get_message_index(external, g->index_in_parent())] = ext_anchor.second;
                        } else {
                            output = ext_anchor.second;
                        }
                    }
                    build_variant(nei_nodes[i], g, incoming, output);
                }
            }
            build_variant(0, g, all_incoming, 0);  /* to compute full state later, when all messages are computed. */
        }
};


inline
std::unique_ptr<instance_type>
instance(std::unique_ptr<factor_graph_type>& g)
{
    return std::unique_ptr<instance_type>(new instance_type(g.get()));
}

#if 0
inline
std::ostream&
operator << (std::ostream& os, const edge_type& e)
{
    std::stringstream tmp;
    tmp << (e.graph->is_interface(e.first) ? 'I' : 'F') << e.graph->variables_of(e.first)
        << "->"
        << (e.graph->is_interface(e.second) ? 'I' : 'F') << e.graph->variables_of(e.second);
    return os << tmp.str();
}
#endif



#endif

