/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_DISK_HASHTABLE_H_
#define _SPELL_DISK_HASHTABLE_H_

#include <iostream>
#include "basic_file_checks.h"
#include "cache/file.h"
#include "error.h"
#include "settings.h"

#include <cstdint> // defines PTRDIFF_MAX
#include <limits>  // defines INT_MAX
#include <sys/types.h>
#include <dirent.h>
#include <shared_mutex>


/*
 * http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2406.html#shared_mutex_imp
 */

#if 0

class shared_mutex
{
    std::mutex mut_;
    std::condition_variable gate1_;
    std::condition_variable gate2_;
    unsigned state_;

    static const unsigned write_entered_ = 1U << (sizeof(unsigned)*CHAR_BIT - 1);
    static const unsigned n_readers_ = ~write_entered_;

public:

    shared_mutex() : state_(0) {}

// Exclusive ownership

    inline void lock();
    inline bool try_lock();
//     bool timed_lock(nanoseconds rel_time);
    inline void unlock();

// Shared ownership

    inline void lock_shared();
    inline bool try_lock_shared();
//     bool timed_lock_shared(nanoseconds rel_time);
    inline void unlock_shared();
};

// Exclusive ownership

void
shared_mutex::lock()
{
//     std::this_thread::disable_interruption _;
    std::unique_lock<std::mutex> lk(mut_);
    while (state_ & write_entered_)
        gate1_.wait(lk);
    state_ |= write_entered_;
    while (state_ & n_readers_)
        gate2_.wait(lk);
}

bool
shared_mutex::try_lock()
{
    std::unique_lock<std::mutex> lk(mut_, std::try_to_lock);
    if (lk.owns_lock() && state_ == 0)
    {
        state_ = write_entered_;
        return true;
    }
    return false;
}

void
shared_mutex::unlock()
{
//     {
    std::unique_lock<std::mutex> _(mut_);
    state_ = 0;
//     }
    gate1_.notify_all();
}

// Shared ownership

void
shared_mutex::lock_shared()
{
//     std::this_thread::disable_interruption _;
    std::unique_lock<std::mutex> lk(mut_);
    while ((state_ & write_entered_) || (state_ & n_readers_) == n_readers_)
        gate1_.wait(lk);
    unsigned num_readers = (state_ & n_readers_) + 1;
    state_ &= ~n_readers_;
    state_ |= num_readers;
}

bool
shared_mutex::try_lock_shared()
{
    std::unique_lock<std::mutex> lk(mut_, std::try_to_lock);
    unsigned num_readers = state_ & n_readers_;
    if (lk.owns_lock() && !(state_ & write_entered_) && num_readers != n_readers_)
    {
        ++num_readers;
        state_ &= ~n_readers_;
        state_ |= num_readers;
        return true;
    }
    return false;
}

void
shared_mutex::unlock_shared()
{
    std::unique_lock<std::mutex> _(mut_);
    unsigned num_readers = (state_ & n_readers_) - 1;
    state_ &= ~n_readers_;
    state_ |= num_readers;
    if (state_ & write_entered_)
    {
        if (num_readers == 0)
            gate2_.notify_one();
    }
    else
    {
        if (num_readers == n_readers_ - 1)
            gate1_.notify_one();
    }
}



struct reader_lock {
    reader_lock(shared_mutex& _) : m(&_) { m->lock_shared(); }
    ~reader_lock() { m->unlock_shared(); }
private:
    shared_mutex* m;
};


struct writer_lock {
    writer_lock(shared_mutex& _) : m(&_) { m->lock(); }
    ~writer_lock() { m->unlock(); }
private:
    shared_mutex* m;
};

#endif


typedef std::shared_timed_mutex shared_mutex;
typedef std::shared_lock<shared_mutex> reader_lock;
typedef std::unique_lock<shared_mutex> writer_lock;











template <typename Type> struct dht_argmatch;
template <typename Type> struct dht_argput;

template <typename IntegralType>
bool
match_integral(std::istream& is, IntegralType i)
{
    IntegralType tmp;
    is.read((char*) &tmp, sizeof(IntegralType));
    return tmp == i;
}

// By default, Eigen::Index is defined as EIGEN_DEFAULT_DENSE_INDEX_TYPE, i.e std::ptrdiff_t.
// On some 32 bits system (not all of them), ptrdiff_t may be a typedef of int
// => this lead to compilation error: redefinition of arg_match(ifile& ifs, int& i)
// DEBUG: define arg_match using type Eigen::Index only on 64 bits system (PTRDIFF_MAX != INT_MAX)
#ifdef PTRDIFF_MAX
#if PTRDIFF_MAX != INT_MAX
// 64 bits system
inline bool arg_match(std::istream& is, Eigen::Index i) { return match_integral(is, i); }
#else
// 32 bits system, PTRDIFF_MAX == INT_MAX == LONG_MAX
// ptrdiff_t is a typedef of either int or long
// => we have to define arg_match using type long
inline bool arg_match(std::istream& is, long i) { return match_integral(is, i); }
#endif
#endif
inline bool arg_match(std::istream& is, int i) { return match_integral(is, i); }
inline bool arg_match(std::istream& is, size_t i) { return match_integral(is, i); }
inline bool arg_match(std::istream& is, double i) { return match_integral(is, i); }

inline bool arg_match(std::istream& is, const label_type& l) { return match_integral(is, l.compact()); }

inline bool
    arg_match(std::istream& is, const std::string& s)
    {
        if (!arg_match(is, s.size())) {
            return false;
        }
        std::vector<char> buffer(s.size());
        is.read(buffer.data(), s.size());
        return std::string(buffer.begin(), buffer.end()) == s;
    }

template <typename X>
inline bool
    arg_match(std::istream& is, const std::vector<X>& vec)
    {
        if (!arg_match(is, vec.size())) {
            return false;
        }
        for (X i: vec) {
            if (!arg_match(is, i)) {
                return false;
            }
        }
        return true;
    }

template <typename Scalar, int... Opts>
bool
    arg_match(std::istream& is, const Eigen::Matrix<Scalar, Opts...>& mat)
    {
        if (!(arg_match(is, mat.rows()) && arg_match(is, mat.cols()))) {
            return false;
        }
        for (int i = 0; i < mat.rows(); ++i) {
            for (int j = 0; j < mat.cols(); ++j) {
                if (!arg_match(is, mat(i, j))) {
                    return false;
                }
            }
        }
        return true;
    }

template <typename C, typename R, typename M>
bool
    arg_match(std::istream& is, const labelled_matrix<C, R, M>& lm)
    {
        return arg_match(is, lm.column_labels) && arg_match(is, lm.row_labels) && arg_match(is, lm.data);
    }

inline bool
    arg_match(std::istream& is, const geno_matrix* mat)
    {
        return arg_match(is, mat->name)
            && arg_match(is, mat->labels)
            && arg_match(is, mat->diag);
    }

inline bool
    arg_match(std::istream& is, const std::vector<double>& vec)
    {
        if (!arg_match(is, vec.size())) {
            return false;
        }
        for (double d: vec) {
            if (!arg_match(is, d)) {
                return false;
            }
        }
        return true;
    }

inline bool
    arg_match(std::istream& is, const qtl_chromosome* qc)
    {
        if (qc == NULL) {
            return arg_match(is, (size_t)0);
        }
        return arg_match(is, qc->chr->name) && arg_match(is, qc->qtl);
    }

inline bool
    arg_match(std::istream& is, const context_key& ck)
    {
        static std::string null("null");
        return ((ck->pop ? arg_match(is, ck->pop->name) && arg_match(is, ck->pop->qtl_generation_name) && arg_match(is, ck->pop->indices)
                         : arg_match(is, null))
            && (ck->gen ? arg_match(is, ck->gen) : arg_match(is, null))
            && (ck->chr ? arg_match(is, ck->chr->name) : arg_match(is, null))
            && arg_match(is, ck->loci));
    }

inline bool
    arg_match(std::istream& is, const locus_key& lk)
    {
        static std::string end("LKend");
        if (lk) {
            auto vec = lk->to_vec();
            /*MSG_DEBUG("arg_match lk->to_vec " << vec);*/
            if (!arg_match(is, vec)) {
                return false;
            }
        }
        return arg_match(is, end);
#if 0
        static std::string end("LKend");
        locus_key tmp = lk;
        while (tmp) {
            if (!arg_match(is, tmp->locus)) {
                return false;
            }
            tmp = tmp->parent;
        }
        return arg_match(is, end);
        return true;
#endif
    }

template <typename... Args> struct arg_matcher;
template <typename Arg> struct arg_matcher<Arg> {
    bool operator () (std::istream& is, Arg&& arg) const { return arg_match(is, arg); }
};
template <typename Arg, typename... Args> struct arg_matcher<Arg, Args...> {
    bool operator () (std::istream& is, Arg&& arg, Args... other) const
    {
        return arg_match(is, arg) && arg_matcher<Args...>()(is, std::forward<Args>(other)...);
    }
};



template <typename IntegralType>
    inline void write_integral(std::ostream& os, IntegralType i) { os.write((char*) &i, sizeof(IntegralType)); }

// Eigen::Index is defined as EIGEN_DEFAULT_DENSE_INDEX_TYPE, i.e std::ptrdiff_t.
// On some 32 bits system (not all of them), ptrdiff_t may be a typedef of int
// => this lead to compilation error: redefinition of arg_write(ifile& ifs, int& i)
// DEBUG: define arg_write using type Eigen::Index only on 64 bits system (PTRDIFF_MAX != INT_MAX) 
#ifdef PTRDIFF_MAX
#if PTRDIFF_MAX != INT_MAX
// 64 bits system
inline void arg_write(std::ostream& os, Eigen::Index i) { write_integral(os, i); }
#else
// 32 bits system, PTRDIFF_MAX == INT_MAX == LONG_MAX
// ptrdiff_t is a typedef of either int or long
// => we have to define arg_match using type long
inline void arg_write(std::ostream& os, long i) { write_integral(os, i); }
#endif
#endif
inline void arg_write(std::ostream& os, int i) { write_integral(os, i); }
inline void arg_write(std::ostream& os, size_t i) { write_integral(os, i); }
inline void arg_write(std::ostream& os, double i) { write_integral(os, i); }

inline void arg_write(std::ostream& os, const label_type& l) { write_integral(os, l.compact()); }

inline void arg_write(std::ostream& os, const std::string& s) { arg_write(os, s.size()); os.write(s.data(), s.size()); }

template <typename X>
    inline void arg_write(std::ostream& os, const std::vector<X>& vec)
    {
        arg_write(os, vec.size());
        for (auto x: vec) { arg_write(os, x); }
    }

template <typename Scalar, int... Opts>
    inline void
    arg_write(std::ostream& os, const Eigen::Matrix<Scalar, Opts...>& mat)
    {
        arg_write(os, mat.rows());
        arg_write(os, mat.cols());
        for (int i = 0; i < mat.rows(); ++i) {
            for (int j = 0; j < mat.cols(); ++j) {
                arg_write(os, mat(i, j));
            }
        }
    }

template <typename R, typename C, typename M>
    inline void
    arg_write(std::ostream& os, const labelled_matrix<R, C, M>& lm)
    {
        arg_write(os, lm.column_labels);
        arg_write(os, lm.row_labels);
        arg_write(os, lm.data);
    }

inline void
    arg_write(std::ostream& os, const geno_matrix* mat)
    {
        arg_write(os, mat->name);
        arg_write(os, mat->labels);
        arg_write(os, mat->diag);
    }

inline void
    arg_write(std::ostream& os, const std::vector<double>& vec)
    {
        arg_write(os, vec.size());
        for (double d: vec) {
            arg_write(os, d);
        }
    }

inline void
    arg_write(std::ostream& os, const qtl_chromosome* qc)
    {
        if (qc == NULL) {
            arg_write(os, (size_t) 0);
        } else {
            arg_write(os, qc->chr->name);
            arg_write(os, qc->qtl);
        }
    }

inline void
    arg_write(std::ostream& os, const context_key& ck)
    {
        static std::string null("null");
        if (ck->pop) {
            arg_write(os, ck->pop->name);
            arg_write(os, ck->pop->qtl_generation_name);
            arg_write(os, ck->pop->indices);
        } else {
            arg_write(os, null);
        }
        if (ck->gen) {
            arg_write(os, ck->gen);
        } else {
            arg_write(os, null);
        }
        arg_write(os, ck->chr ? ck->chr->name : null);
        arg_write(os, ck->loci);
    }

inline void
    arg_write(std::ostream& os, const locus_key& lk)
    {
        static std::string end("LKend");
        if (lk) {
            auto vec = lk->to_vec();
            /*MSG_DEBUG("arg_write lk->to_vec " << vec);*/
            arg_write(os, vec);
        }
        arg_write(os, end);
#if 0
        static std::string end("LKend");
        locus_key tmp = lk;
        while (tmp) {
            arg_write(os, tmp->locus);
            tmp = tmp->parent;
        }
        arg_write(os, end);
#endif
    }


template <typename... Args> struct arg_writer;
template <typename Arg> struct arg_writer<Arg> {
    void operator () (std::ostream& os, Arg&& arg) const { arg_write(os, arg); }
};
template <typename Arg, typename... Args> struct arg_writer<Arg, Args...> {
    void operator () (std::ostream& os, Arg&& arg, Args... other) const
    {
        arg_write(os, arg);
        arg_writer<Args...>()(os, std::forward<Args>(other)...);
    }
};


namespace std {
template <typename V, typename A>
    struct hash<std::vector<V, A>> {
        size_t operator () (const std::vector<V, A>& vec) const
        {
            size_t accum = (1 + vec.size());
            for (const V& x: vec) {
                accum = (accum * 39916801) ^ hash<V>()(x);
            }
            return accum;
        }
    };

    template <typename X>
        struct hash<const X&> { size_t operator () (const X& x) const { return hash<X>()(x); } };
}


template <typename... Args> struct arg_hasher;
template <typename Arg> struct arg_hasher<Arg> {
    /*size_t operator () (Arg&& arg) const { return 0xdeadbeef ^ (39916801 * std::hash<Arg>()(arg)); }*/
    size_t operator () (Arg&& arg) const { return std::hash<Arg>()(std::forward<Arg>(arg)); }
};
template <typename Arg, typename... Args> struct arg_hasher<Arg, Args...> {
    size_t operator () (Arg&& arg, Args... other) const
    {
        return std::hash<Arg>()(std::forward<Arg>(arg)) ^ (arg_hasher<Args...>()(std::forward<Args>(other)...) * 39916801);
    }
};



template <typename Func> struct disk_hashtable;


template <typename ValueType, typename... AllArgs>
struct disk_hashtable<ValueType(AllArgs...)> {
    typedef ValueType value_type;
    typedef disk_hashtable<ValueType(AllArgs...)> this_type;

    disk_hashtable(const std::string& base_path)
        : m_base_path()
        , m_mutex()
        /*: m_base_path(SPELL_STRING(active_settings->work_directory << '/' << base_name))*/
    {
        writer_lock lock(m_mutex);
        m_base_path = base_path;
        ensure_directories_exist(m_base_path);
    }

    std::string
        hash(AllArgs&&... args) const
        {
            static char hex[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
            size_t h = arg_hasher<AllArgs...>()(std::forward<AllArgs>(args)...);
            std::string ret(11, '/');
            static size_t ret_ofs[8] = {0, 1, 3, 4, 6, 7, 9, 10};
            for (int i = 7; i >= 0; --i) {
                ret[ret_ofs[i]] = hex[h & 0xF];
                h >>= 4;
            }
            return ret;
        }

    /* 
     * List all existing entries
     */
    std::vector<std::string>
        get_entries(const std::string& path) const
        {
            std::vector<std::string> ret;
            DIR *dir;
            struct dirent *ent;
            if ((dir = opendir (path.c_str())) != NULL) {
                /* print all the files and directories within directory */
                while ((ent = readdir (dir)) != NULL) {
                    /*printf ("%s\n", ent->d_name);*/
                    /*ret.push_back(SPELL_STRING(path << '/' << ent->d_name));*/
                    if (check_file(SPELL_STRING(path << '/' << ent->d_name), false, true, false)) {
                        ret.push_back(ent->d_name);
                    }
                }
                closedir (dir);
            }
            return ret;
        }

    bool
        match_key(file& entry, AllArgs&&... args) const
        {
            return arg_matcher<AllArgs...>()(entry, std::forward<AllArgs>(args)...);
        }

    bool
        find(std::function<void(file&)> process_result, AllArgs&&... args) const
        {
            std::string hashed_key = hash(std::forward<AllArgs>(args)...);
            std::string path = SPELL_STRING(m_base_path << '/' << hashed_key);
            std::vector<std::string> entries = get_entries(path);
            return find(path, entries, process_result, std::forward<AllArgs>(args)...);
        }

    bool
        find(const std::string& path, const std::vector<std::string>& entries,
                std::function<void(file&)> process_result, AllArgs&&... args) const
        {
            for (const auto& ent: entries) {
                file entf(SPELL_STRING(path << '/' << ent), std::fstream::in | std::fstream::binary);
                if (match_key(entf, std::forward<AllArgs>(args)...)) {
                    process_result(entf);
                    entf.close();
                    return true;
                }
                entf.close();
            }
            return false;
        }

    std::pair<bool, value_type>
        get(AllArgs&&... args) const
        {
            static std::string end("end");
            std::pair<bool, value_type> ret;
            try {
                ret.first = find([&] (file& ifs) { cache_input ci(ifs); ci & ret.second; if (!arg_match(ifs, end)) { throw file::error(""); } },
                                 std::forward<AllArgs>(args)...);
            } catch(file::error& fuuuuu) {
                ret.first = false;
            }
            return ret;
        }

    void
        put(value_type& value, AllArgs&&... args)
        {
            std::string hashed_key = hash(std::forward<AllArgs>(args)...);
            std::string path = SPELL_STRING(m_base_path << '/' << hashed_key);
            ensure_directories_exist(path);
            std::vector<std::string> entries = get_entries(path);
            put(path, entries, value, std::forward<AllArgs>(args)...);
        }

    void
        put(const std::string& path, const std::vector<std::string>& entries,
                value_type& value, AllArgs&&... args)
        {
            static std::string end("end");
            writer_lock lock(m_mutex);
            bool exists = find(path, entries, [&] (file& ofs) { cache_output co(ofs); co & value; arg_write(ofs, end); },
                               std::forward<AllArgs>(args)...);
            /*MSG_DEBUG("entries " << entries);*/
            if (!exists) {
                std::string new_entry = SPELL_STRING(path << '/' << entries.size());
                /*MSG_DEBUG("new_entry " << new_entry);*/
                file ofs(new_entry, std::fstream::out | std::fstream::binary);
                /*if (ofs.rdstate() & std::ios_base::failbit) {*/
                    /*MSG_DEBUG("open failed " << new_entry);*/
                /*}*/
                arg_writer<AllArgs...>()(ofs, std::forward<AllArgs>(args)...);
                cache_output co(ofs);
                co & value;
                arg_write(ofs, end);
                ofs.close();
            }
        }

    value_type
        get_or_compute(std::function<value_type(AllArgs...)> func, AllArgs&&... args)
        {
            std::string hashed_key = hash(std::forward<AllArgs>(args)...);
            std::string path;
            std::pair<bool, value_type> ret;
            std::vector<std::string> entries;
            {
                reader_lock lock(m_mutex);
                path = SPELL_STRING(m_base_path << '/' << hashed_key);
                ensure_directories_exist(path);
                entries = get_entries(path);
                ret.first = find([&] (file& ifs) { cache_input ci(ifs); ci & ret.second; },
//                                 args...);
                                std::forward<AllArgs>(args)...);
            }
            if (!ret.first) {
                ret.second = func(std::forward<AllArgs>(args)...);
                put(path, entries, ret.second, std::forward<AllArgs>(args)...);
//                ret.second = func(args...);
//                put(path, entries, ret.second, args...);
            }
            return ret.second;
        }

private:
    std::string m_base_path;
    mutable shared_mutex m_mutex;
};



#endif

