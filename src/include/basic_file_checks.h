/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_BASIC_FILE_CHECKS_H_
#define _SPELL_BASIC_FILE_CHECKS_H_

#ifdef _WIN32
extern "C" {
//#include <io.h>     // _chmod
#include <direct.h> // _mkdir
}
#endif

extern "C" {
#include <sys/stat.h>
};

struct file_stat {
    bool exists;
    int err;
    bool is_file;
    bool is_dir;
    bool writable;
    bool readable;
    file_stat(const std::string& path)
    {
        struct stat st;
        if (stat(path.c_str(), &st)) {
            exists = false;
            err = errno;
        } else {
            exists = true;
            err = 0;
        }
        if (exists) {
            is_file = !!S_ISREG(st.st_mode);
            is_dir = !!S_ISDIR(st.st_mode);
            writable = !!(st.st_mode & (S_IWUSR | S_IWGRP | S_IWOTH));
            readable = !!(st.st_mode & (S_IRUSR | S_IRGRP | S_IROTH));
        } else {
            is_file = is_dir = writable = readable = false;
        }
    }
};


static inline
bool check_file(const std::string& path, bool req_directory, bool req_writable, bool display=true)
{
    file_stat fs(path);
    if (fs.err != 0) {
        if (display) {
            MSG_ERROR("Path " << path << " is invalid: " << strerror(errno), "Check whether " << path << " exists and is accessible");
        }
        return false;
    }

    if (req_directory) {
        if (!fs.is_dir) {
            if (display) {
                MSG_ERROR(path << " is not a directory", "Specify the path to directory, not a file [" << path << ']');
            }
            return false;
        }
    } else if (!fs.is_file) {
        if (display) {
            MSG_ERROR(path << " is not a regular file or a symbolic link to a regular file", "Specify the path to a file, not a directory [" << path << ']');
        }
        return false;
    }

    if (!fs.readable) {
        if (display) {
             MSG_ERROR(path << " is not readable", "Verify the permissions of " << path);
        }
        return false;
    }
    if (req_writable && !fs.writable) {
        if (display) {
             MSG_ERROR(path << " is not writable", "Verify the permissions of " << path);
        }
        return false;
    }
    return true;
}

static inline
bool ensure_directory_exists(const std::string& path)
{
#ifdef _WIN32
    return check_file(path, true, true, false)
        || _mkdir(path.c_str()) != -1;
#else
    return check_file(path, true, true, false)
        || mkdir(path.c_str(), 0770) != -1;
#endif
}


#if 0
static inline
bool ensure_directories_exist(const std::string& path)
{
    auto i = path.begin(), j = path.end();
    bool ok = true;
    /*if (*i == '/') { ++i; }*/
    do {
        if (i != j) {
            ++i;
        }
        while (i != j && *i != '/') { ++i; }
        std::string sub(path.begin(), i);
        /*MSG_INFO("ensure_directory_exists " << sub);*/
        ok = ensure_directory_exists(sub);
    } while (ok && i != j);
    return ok;
}

#elif 0

static inline
bool ensure_directories_exist(const std::string& path)
{
    if (path.size() == 0) {
        /*MSG_DEBUG("ensure_directories_exist(" << path << ") OK");*/
        return true;
    }
    size_t i = path.size() - 1;
    while (i > 0 && path[i] != '/') { --i; }
    std::string parent(path.begin(), path.begin() + i);
    if (check_file(parent, true, true, false) || ensure_directories_exist(parent)) {
        /*MSG_DEBUG("ensure_directories_exist(" << path << ") OK");*/
        return mkdir(path.c_str(), 0770) != -1;
    }
    /*MSG_DEBUG("ensure_directories_exist(" << path << ") bad.");*/
    return false;
}

#else
//extern "C" {
/*
@(#)File:           $RCSfile: mkpath.c,v $
@(#)Version:        $Revision: 1.13 $
@(#)Last changed:   $Date: 2012/07/15 00:40:37 $
@(#)Purpose:        Create all directories in path
@(#)Author:         J Leffler
@(#)Copyright:      (C) JLSS 1990-91,1997-98,2001,2005,2008,2012
*/

/*TABSTOP=4*/

#include <errno.h>
#include <unistd.h>
#include <string.h>

typedef struct stat Stat;

#if 0  /* FIXME: move ID string to static_data */
#ifndef lint
/* Prevent over-aggressive optimizers from eliminating ID string */
const char jlss_id_mkpath_c[] = "@(#)$Id: mkpath.c,v 1.13 2012/07/15 00:40:37 jleffler Exp $";
#endif /* lint */
#endif

static inline int do_mkdir(const char *path, mode_t mode)
{
    Stat            st;
    int             status = 0;

    if (stat(path, &st) != 0)
    {
        /* Directory does not exist. EEXIST for race condition */
#ifdef _WIN32
        if (_mkdir(path) != 0 && errno != EEXIST) status = -1;
#else
        if (mkdir(path, mode) != 0 && errno != EEXIST) status = -1;
#endif
    }
    else if (!S_ISDIR(st.st_mode))
    {
        errno = ENOTDIR;
        status = -1;
    }

    return(status);
}

/**
** mkpath - ensure all directories in path exist
** Algorithm takes the pessimistic view and works top-down to ensure
** each directory in path exists, rather than optimistically creating
** the last element and working backwards.
*/
static inline int mkpath(const char *path, mode_t mode)
{
    char           *pp;
    char           *sp;
    int             status;
char           *copypath = strdup(path);

    status = 0;
    pp = copypath;
#ifdef _WIN32
    // file separator on windows can be either '/' or '\'
    while (status == 0 && (sp = strpbrk(pp, "/\\")) != 0)
#else
    while (status == 0 && (sp = strchr(pp, '/')) != 0)
#endif
    {
        if (sp != pp)
        {
            /* Neither root nor double slash in path */
            *sp = '\0';
            status = do_mkdir(copypath, mode);
            *sp = '/';
        }
        pp = sp + 1;
    }
    if (status == 0)
        status = do_mkdir(path, mode);
    free(copypath);
    return (status);
}

#ifdef TEST

#include <stdio.h>

/*
** Stress test with parallel running of mkpath() function.
** Before the EEXIST test, code would fail.
** With the EEXIST test, code does not fail.
**
** Test shell script
** PREFIX=mkpath.$$
** NAME=./$PREFIX/sa/32/ad/13/23/13/12/13/sd/ds/ww/qq/ss/dd/zz/xx/dd/rr/ff/ff/ss/ss/ss/ss/ss/ss/ss/ss
** : ${MKPATH:=mkpath}
** ./$MKPATH $NAME &
** [...repeat a dozen times or so...]
** ./$MKPATH $NAME &
** wait
** rm -fr ./$PREFIX/
*/

int main(int argc, char **argv)
{
    int             i;

    for (i = 1; i < argc; i++)
    {
        for (int j = 0; j < 20; j++)
        {
            if (fork() == 0)
            {
                int rc = mkpath(argv[i], 0777);
                if (rc != 0)
                    fprintf(stderr, "%d: failed to create (%d: %s): %s\n",
                            (int)getpid(), errno, strerror(errno), argv[i]);
                exit(rc == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
            }
        }
        int status;
        int fail = 0;
        while (wait(&status) != -1)
        {
            if (WEXITSTATUS(status) != 0)
                fail = 1;
        }
        if (fail == 0)
            printf("created: %s\n", argv[i]);
    }
    return(0);
}

//} /* extern "C" */
#endif /* TEST */

static inline
bool ensure_directories_exist(const std::string& path)
{
    return mkpath(path.c_str(), 0700) == 0;
}

#endif

#endif

