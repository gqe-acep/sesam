/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_TENSOR_H_
#define _SPELL_TENSOR_H_

#include "eigen.h"
#include <vector>
#include <array>
#include <memory>
#include <iostream>


template <typename Scalar, int N_Dim>
struct tensor_view {
    typedef Eigen::Map<Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>, 0, Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic>> matrix_view_type;

    tensor_view(std::shared_ptr<std::vector<Scalar>> dat, const std::array<size_t, N_Dim>& sizes, const std::array<size_t, N_Dim + 1>& strides, size_t offset)
        : m_dimensions(sizes), m_strides(strides), m_offset(offset), m_data(dat)
    {}

    tensor_view(const std::array<size_t, N_Dim>& sizes)
        : m_dimensions(sizes), m_strides(), m_offset(0), m_data(std::make_shared<std::vector<Scalar>>())
    {
        compute_strides();
        m_data->resize(size());
    }

    std::shared_ptr<std::vector<Scalar>>
        data() { return m_data; }

    Scalar&
        operator () (std::initializer_list<size_t> indices)
        {
            return (*m_data)[compute_offset(std::forward<std::initializer_list<size_t>>(indices))];
        }

    Scalar
        operator () (std::initializer_list<size_t> indices) const
        {
            return (*m_data)[compute_offset(std::forward<std::initializer_list<size_t>>(indices))];
        }

    template <int N_Skip_Dim>
    tensor_view<Scalar, N_Dim - N_Skip_Dim>
    chip(const std::array<size_t, N_Skip_Dim>& dimensions, const std::array<size_t, N_Skip_Dim>& indices) const
    {
        typedef std::array<size_t, N_Dim - N_Skip_Dim + 1> new_strides_type;
        typedef std::array<size_t, N_Dim - N_Skip_Dim> new_sizes_type;
        new_strides_type new_strides;
        new_sizes_type new_dimensions;
        size_t new_size = 1;
        size_t offset = 0;

        size_t i_skip = 0, i_dim = 0;
        size_t i_nd = 0;
        for (i_skip = 0; i_skip < N_Skip_Dim; ++i_skip) {
            for (; i_dim < dimensions[i_skip] && i_dim < N_Dim; ++i_dim) {
                /*std::cout << "keep dimension #" << i_dim << " dimension=" << m_dimensions[i_dim] << " stride=" << m_strides[i_dim] << std::endl;*/
                new_strides[i_nd] = m_strides[i_dim];
                new_dimensions[i_nd] = m_dimensions[i_dim];
                new_size *= m_dimensions[i_dim];
                ++i_nd;
            }
            offset += indices[i_skip] * m_strides[dimensions[i_skip]];
            std::cout << "new offset = " << offset << std::endl;
            ++i_dim;
        }
        for (; i_dim < N_Dim; ++i_dim) {
            /*std::cout << "keep dimension #" << i_dim << " dimension=" << m_dimensions[i_dim] << " stride=" << m_strides[i_dim] << std::endl;*/
            new_strides[i_nd] = m_strides[i_dim];
            new_dimensions[i_nd] = m_dimensions[i_dim];
            ++i_nd;
            new_size *= m_dimensions[i_dim];
        }
        new_strides.back() = new_size;
        return {m_data, new_dimensions, new_strides, offset};
    }

    tensor_view<Scalar, N_Dim - 1>
    chip(size_t dimension, size_t indice) const
    {
        return chip<1>(std::array<size_t, 1>{{dimension}}, std::array<size_t, 1>{{indice}});
    }

    matrix_view_type
        as_matrix()
        {
            if (N_Dim == 2) {
                return matrix_view_type{
                            &(*m_data)[m_offset],
                            (Eigen::Index) m_dimensions[0], (Eigen::Index) m_dimensions[1],
                            Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic>(m_strides[1], m_strides[0])};
            }
            return matrix_view_type(NULL, 0, 0, Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic>(0, 0));
        }


    friend
        std::ostream&
        operator << (std::ostream& os, const tensor_view<Scalar, N_Dim>& tensor)
        {
            os << "Tensor " << N_Dim << 'D' << std::endl;
            os << "  dimensions";
            for (size_t d: tensor.m_dimensions) { os << ' ' << d; }
            os << std::endl << "  strides";
            for (size_t d: tensor.m_strides) { os << ' ' << d; }
            os << std::endl << "  offset " << tensor.m_offset;
            os << std::endl << "data";
            for (Scalar d: *tensor.m_data) { os << ' ' << d; }
            os << std::endl;
            return os;
        }

private:
    size_t compute_offset(std::initializer_list<size_t> indices)
    {
        assert(indices.size() == N_Dim);
        size_t offset = m_offset;
        auto i = indices.begin();
        auto j = indices.end();
        auto s = m_strides.begin();
        for (; i != j; ++i, ++s) {
            offset += *i * *s;
        }
        /*std::cout << "compute_offset(";*/
        /*i = indices.begin();*/
        /*std::cout << *i;*/
        /*for (++i; i != j; ++i) { std::cout << ", " << *i; }*/
        /*std::cout << ") = " << offset << std::endl;*/
        return offset;
    }
   size_t size() const { return m_strides.back(); }

   void
       compute_strides()
       {
           size_t i = 1;
           m_strides[0] = 1;
           for (; i <= N_Dim; ++i) {
               m_strides[i] = m_dimensions[i - 1] * m_strides[i - 1];
           }
       }

    std::array<size_t, N_Dim> m_dimensions;
    std::array<size_t, N_Dim + 1> m_strides;
    size_t m_offset;
    std::shared_ptr<std::vector<Scalar>> m_data;
};

#endif

