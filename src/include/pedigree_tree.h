/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_PEDIGREE_TREE_H_
#define _SPELL_PEDIGREE_TREE_H_
#include <map>
#include <vector>
#include <set>
#include <unordered_set>
#include "permutation.h"


typedef std::map<int, int> ancestor_node_list_type;


inline
ancestor_node_list_type reentrants(const ancestor_node_list_type& a)
{
    ancestor_node_list_type ret;
    for (const auto& kv: a) {
        if (kv.second > 1) {
            ret.emplace(kv);
        }
    }
    return ret;
}


inline
ancestor_node_list_type operator + (const ancestor_node_list_type& a1, const ancestor_node_list_type& a2)
{
    ancestor_node_list_type ret(a1);
    for (const auto& kv: a2) {
        ret[kv.first] += kv.second;
    }
    return ret;
}


inline
ancestor_node_list_type operator / (const ancestor_node_list_type& a, const ancestor_node_list_type& restr)
{
    ancestor_node_list_type ret;
    for (const auto& kv: a) {
        auto i = restr.find(kv.first);
        if (i != restr.end()) {
            ret.emplace(kv.first, std::min(kv.second, i->second));
        }
    }
    return ret;
}


inline
ancestor_node_list_type operator % (const ancestor_node_list_type& a, const ancestor_node_list_type& restr)
{
    ancestor_node_list_type ret;
    for (const auto& kv: a) {
        if (restr.find(kv.first) != restr.end()) {
            ret.emplace(kv);
        }
    }
    return ret;
}


inline
ancestor_node_list_type operator - (const ancestor_node_list_type& a, const ancestor_node_list_type& restr)
{
    ancestor_node_list_type ret;
    for (const auto& kv: a) {
        auto it = restr.find(kv.first);
        if (it == restr.end()) {
            ret.emplace(kv);
        } else if (kv.second > it->second) {
            ret.emplace(kv.first, kv.second - it->second);
        }
    }
    return ret;
}


inline
ancestor_node_list_type operator * (const ancestor_node_list_type& a, int weight)
{
    ancestor_node_list_type ret;
    for (const auto& kv: a) {
        ret.emplace(kv.first, kv.second * weight);
    }
    return ret;
}


inline
std::ostream& operator << (std::ostream& os, const ancestor_node_list_type& a)
{
    auto i = a.begin();
    auto j = a.end();
    if (i != j) {
        os << i->first << ':' << i->second;
        for (++i; i != j; ++i) {
            os << ' ' << i->first << ':' << i->second;
        }
    } else {
        os << "empty";
    }
    return os;
}



struct pedigree_tree_type {
#define NONE ((int) -1)
    typedef size_t individual_index_type;

    struct node_type {
        int p1, p2;
        node_type() : p1(NONE), p2(NONE) {}
        node_type(int p) : p1(p), p2(NONE) {}
        node_type(int a, int b) : p1(a), p2(b) {}
        node_type(const node_type& other) : p1(other.p1), p2(other.p2) {}
        bool operator == (const node_type& other) const { return p1 == other.p1 && p2 == other.p2; }
        bool operator < (const node_type& other) const { return p1 < other.p1 || (p1 == other.p1 && p2 < other.p2); }
        node_type& operator = (const node_type& other) { p1 = other.p1; p2 = other.p2; return *this; }
        bool is_ancestor() const { return p1 == NONE && p2 == NONE; }
        bool is_gamete() const { return p1 != NONE && p2 == NONE; }
        bool is_crossing() const { return p1 != NONE && p2 != NONE; }
        bool is_genotype() const { return p2 != NONE || p1 == NONE; }
        bool has_p1() const { return p1 != NONE; }
        bool has_p2() const { return p2 != NONE; }
    };

    std::vector<int> m_leaves;
    std::unordered_set<int> m_roots;

    std::vector<node_type> m_nodes;
    std::vector<std::vector<bool>> m_must_recompute;

    std::map<int, individual_index_type> m_node_number_to_ind_number;
    std::vector<int> m_ind_number_to_node_number;
    std::vector<int> m_original_ordering;

    pedigree_tree_type()
        : m_leaves(), m_roots(), m_nodes(), m_must_recompute(), m_node_number_to_ind_number(), m_ind_number_to_node_number(1, NONE)
    {}

    bool
        node_is_clone(int n) const
        {
            const auto& N = m_nodes[n];
            return N.is_ancestor()
                || (N.is_genotype() && m_nodes[get_p1(N.p1)].is_ancestor() && m_nodes[get_p1(N.p2)].is_ancestor());
        }

    int get_p1(int n) const { return m_nodes[n].p1; }
    int get_p2(int n) const { return m_nodes[n].p2; }

    const node_type& operator [] (int n) const { return m_nodes[n]; }

    size_t size() const { return m_nodes.size(); }
    const std::vector<bool>& get_recompute_vec(int n) const { return m_must_recompute[n]; }

    /* building helpers */

    int ancestor() { return add_node(); }
    int gamete(int p) { return add_node(p); }
    int crossing(int p1, int p2) { return add_node(gamete(p1), gamete(p2)); }
    int dh(int p1) { int g = gamete(p1); return add_node(g, g); }
    int selfing(int p1) { return crossing(p1, p1); }

    /* expression helpers */

    pedigree_tree_type
        extract(const std::vector<bool>& include, std::vector<int>& inputs, std::vector<int>& outputs) const
        {
            pedigree_tree_type ret;
            if (include.size() != m_nodes.size()) {
                return {};
            }
            std::vector<int> translate(1 + m_nodes.size(), NONE);
            std::vector<int> inv_translate(1 + m_nodes.size(), NONE);
            const int sz = (int) m_nodes.size();
            for (int i = 0; i < sz; ++i) {
                if (include[i]) {
                    translate[1 + i] = ret.add_node(translate[1 + m_nodes[i].p1], translate[1 + m_nodes[i].p2]);
                    inv_translate[translate[1 + i]] = i;
                    ret.m_original_ordering.back() = m_original_ordering[i];
                }
            }
            inputs.clear();
            inputs.reserve(ret.m_leaves.size());
            for (size_t i = 0; i < ret.m_leaves.size(); ++i) {
                inputs.push_back(inv_translate[ret.m_leaves[i]]);
            }
            outputs.clear();
            outputs.reserve(ret.m_roots.size());
            for (int r: ret.m_roots) {
                outputs.push_back(inv_translate[r]);
            }
            return ret;
        }

    int original_node_number(int i) const { return m_original_ordering[i]; }

    pedigree_tree_type
        extract_expression(int n, std::vector<int>& inputs, std::vector<int>& outputs)
        {
            std::vector<bool> include(m_nodes.size(), false);
            std::vector<int> stack(1, n);
            const auto& recompute = m_must_recompute[n];
            /* what to include?
             * - any must_recompute node
             * - any parent of gamete (single-parent) node
             */
            while (stack.size() != 0) {
                n = stack.back();
                stack.pop_back();
                include[n] = true;
                if (recompute[n] || m_nodes[n].is_gamete()) {
                    if (!include[m_nodes[n].p1]) {
                        stack.push_back(m_nodes[n].p1);
                    }
                    if (m_nodes[n].has_p2() && !include[m_nodes[n].p2]) {
                        stack.push_back(m_nodes[n].p2);
                    }
                }
            }
            return extract(include, inputs, outputs);
        }

    std::vector<bool>
        get_deep_recompute_vec(int n) const
        {
            std::vector<bool> ret(m_must_recompute[n]);
            for (int i = n; i >= 0; --i) {
                if (!ret[i]) {
                    const auto& sub_recompute = m_must_recompute[i];
                    for (int sub = 0; sub < i; ++sub) {
                        ret[sub] = ret[sub] | sub_recompute[sub];
                    }
                }
            }
            ret[n] = false;
            return ret;
        }

    pedigree_tree_type
        extract_subtree(int n)
        {
            std::vector<bool> include(m_nodes.size(), false);
            std::vector<int> stack(1, n);

            while (stack.size() != 0) {
                n = stack.back();
                stack.pop_back();
                include[n] = true;
                if (m_nodes[n].has_p1() && !include[m_nodes[n].p1]) {
                    stack.push_back(m_nodes[n].p1);
                }
                if (m_nodes[n].has_p2() && !include[m_nodes[n].p2]) {
                    stack.push_back(m_nodes[n].p2);
                }
            }
            std::vector<int> discard_inputs, discard_outputs;
            return extract(include, discard_inputs, discard_outputs);
        }

    int
        add_node(int p1=NONE, int p2=NONE)
        {
            int ret = (int) m_nodes.size();
            m_nodes.emplace_back(p1, p2);
            m_roots.erase(p1);
            m_roots.erase(p2);
            m_roots.insert(ret);
            m_must_recompute.emplace_back(m_nodes.size(), false);
            std::vector<bool>& recompute = m_must_recompute.back();
            if (p2 != NONE || p1 == NONE) { /* descendant or ancestor, not gamete */
                m_node_number_to_ind_number[ret] = m_ind_number_to_node_number.size();
                m_ind_number_to_node_number.push_back(ret);
            }
            if (p1 != NONE && p2 != NONE) {
                auto tmp_reent = cleanup_reentrants(ret);
                for (size_t i = 0; i < m_nodes.size(); ++i) {
                    recompute[i] = (tmp_reent % count_ancestors(i)).size() > 0;
                    /*MSG_DEBUG("must_recompute " << make_node_label(i) << " = " << recompute[i]);*/
                }
            } else if (p1 == NONE && p2 == NONE) {
                m_leaves.push_back(ret);
            }
            recompute.back() = true;
            m_original_ordering.push_back(m_original_ordering.size());
            return ret;
        }

    ancestor_node_list_type
        cleanup_reentrants(int node) const
        {
#if 0
            auto A = count_ancestors(node);
            /*auto Ap1 = count_ancestors(m_nodes[node].p1);*/
            /*auto Ap2 = count_ancestors(m_nodes[node].p2);*/

            auto R = reentrants(A);
            /*auto Rp1 = reentrants(Ap1);*/
            /*auto Rp2 = reentrants(Ap2);*/

            MSG_DEBUG_INDENT_EXPR("[cleanup_reentrants] ");
            MSG_DEBUG("A: " << A);
            /*MSG_DEBUG("Ap1: " << Ap1);*/
            /*MSG_DEBUG("Ap2: " << Ap2);*/
            MSG_DEBUG("R: " << R);
            /*MSG_DEBUG("Rp1: " << Rp1);*/
            /*MSG_DEBUG("Rp2: " << Rp2);*/

            /*R = R - Rp1 - Rp2;*/

            ancestor_node_list_type ret = R;
            auto i = R.rbegin();
            auto j = R.rend();
            for (; i != j; ++i) {
                /*MSG_DEBUG("cleaning from #" << i->first << " (x" << i->second << ')');*/
                auto sub_re = reentrants(count_ancestors(i->first) * i->second);
                /*MSG_DEBUG(" sub reentrants = " << sub_re);*/
                ret = ret - sub_re;
                /*MSG_DEBUG(" current list = " << ret);*/
            }
            MSG_DEBUG("final R: " << ret);
            MSG_DEBUG_DEDENT;
#else
            /* Merci à Alexandre Heurteau pour l'idée de la recherche de cycle */
            /*scoped_indent _(SPELL_STRING("[reentrants(" << node << ")] "));*/
            std::vector<bool> visited(m_nodes.size(), false);

            std::vector<int> left_ancestors, right_ancestors;
            left_ancestors.reserve(m_nodes.size());
            right_ancestors.reserve(m_nodes.size());
            std::vector<int> stack;
            stack.reserve(m_nodes.size());

            auto ancestor_list
                = [&] (int n0, std::vector<int>& anc)
                {
                    stack.clear();
                    stack.push_back(n0);
                    while (stack.size()) {
                        int n = stack.back();
                        stack.pop_back();
                        /*MSG_DEBUG("ancestor_list on " << n);*/
                        if (!visited[n]) {
                            if (m_nodes[n].has_p1()) {
                                stack.push_back(m_nodes[n].p1);
                            }
                            if (m_nodes[n].has_p2()) {
                                stack.push_back(m_nodes[n].p2);
                            }
                        }
                        anc.push_back(n);
                        visited[n] = true;
                    }
                    std::sort(anc.begin(), anc.end());
                    auto last = std::unique(anc.begin(), anc.end());
                    anc.erase(last, anc.end()); 
                };

            ancestor_list(m_nodes[node].p1, left_ancestors);
            /*MSG_DEBUG("left ancestors " << left_ancestors);*/
            /*MSG_DEBUG("visited " << visited);*/

            ancestor_list(m_nodes[node].p2, right_ancestors);
            /*MSG_DEBUG("right ancestors " << right_ancestors);*/
            /*MSG_DEBUG("visited " << visited);*/

            std::vector<int> intersect(std::min(left_ancestors.size(), right_ancestors.size()), -1);

            auto it = std::set_intersection(left_ancestors.begin(), left_ancestors.end(), right_ancestors.begin(), right_ancestors.end(), intersect.begin());
            intersect.resize(it - intersect.begin());

            ancestor_node_list_type ret;
            for (int n: intersect) {
                ret[n] = 1;
            }

            /*MSG_DEBUG("reentrants = " << ret);*/
#endif
            return ret;
        }

    ancestor_node_list_type
        count_ancestors(int node) const
        {
            ancestor_node_list_type ret;
            if (node == NONE) {
                return ret;
            }
            std::vector<int> stack;
            stack.reserve(m_nodes.size());
            /*stack.push_back(node);*/
            if (m_nodes[node].has_p1()) {
                stack.push_back(m_nodes[node].p1);
            }
            if (m_nodes[node].has_p2()) {
                stack.push_back(m_nodes[node].p2);
            }
            while (stack.size()) {
                int n = stack.back();
                stack.pop_back();
                /*if (node_is_clone(n)) { continue; }*/
                if (m_nodes[n].has_p1()) {
                    stack.push_back(m_nodes[n].p1);
                }
                if (m_nodes[n].has_p2()) {
                    stack.push_back(m_nodes[n].p2);
                }
                ++ret[n];
            }
            /*ret[node] = 1;*/
            return ret;
        }

    std::string
        make_node_label(int ni) const
        {
            const node_type& n = m_nodes[ni];
            std::string sub;
            std::string p1;
            std::stringstream l;
            l <<  '(' << ni << ") ";
            if (n.has_p2() || !n.has_p1()) {
                l << m_node_number_to_ind_number.find(ni)->second;
            } else {
                l << "G(" << m_node_number_to_ind_number.find(n.p1)->second << ')';
            }
            return l.str();
        }

    individual_index_type
        node2ind(int node) const
        {
            auto it = m_node_number_to_ind_number.find(node);
            if (it != m_node_number_to_ind_number.end()) { return it->second; }
            return NONE;
        }

    int
        ind2node(individual_index_type ind) const
        {
            return m_ind_number_to_node_number[ind];
        }

    int next_ind_idx() const { return m_ind_number_to_node_number.size(); }

    void
        render_tree_stream(std::string& path) const
        {
            ofile tmp("/tmp/pedigree_tree"); // text mode OK
            std::vector<std::string> labels;
            labels.reserve(m_nodes.size());
            for (size_t ni = 0; ni < m_nodes.size(); ++ni) {
                labels.emplace_back(make_node_label(ni));
            }
            size_t ni = 0;
            for (const auto& n: m_nodes) {
                if (n.has_p1()) {
                    tmp << '[' << labels[n.p1] << "]-->[" << labels[ni] << ']' << std::endl;
                }
                if (n.has_p2()) {
                    tmp << '[' << labels[n.p2] << "]-->[" << labels[ni] << ']' << std::endl;
                }
                ++ni;
            }
            tmp.close();
            { auto ret = system("graph-easy /tmp/pedigree_tree --as_boxart /tmp/pedigree_tree.txt"); (void) ret; }
            path = "/tmp/pedigree_tree.txt";
        }

    std::string
        render_tree() const
        {
            std::string path;
            render_tree_stream(path);
            ifile out(path); // text mode OK
            char c;
            std::stringstream converter;
            while (!out.get(c).eof()) {
                converter << c;
            }
            return converter.str();
        }

    bool
        operator == (const pedigree_tree_type& other) const
        {
            return m_nodes == other.m_nodes;
        }

    bool
        operator < (const pedigree_tree_type& other) const
        {
            return m_nodes < other.m_nodes;
        }

    friend
        std::ostream&
        operator << (std::ostream& os, const pedigree_tree_type& tree)
        {
            std::string path;
            tree.render_tree_stream(path);
            ifile out(path); // text mode OK
            char c;
            while (!out.get(c).eof()) {
                os << c;
            }
            return os;
        }

    struct tree_descr_struc;
    typedef std::shared_ptr<tree_descr_struc> tree_descr_type;

    /*struct tree_descr_comp {*/
        /*bool operator () (const tree_descr_struc* a, const tree_descr_struc* b) const { return *a < *b; }*/
    /*};*/

    /*typedef std::map<tree_descr_struc*, bool, tree_descr_comp> tree_descr_bool_map_type;*/
    typedef std::map<tree_descr_struc*, bool> tree_descr_bool_map_type;

    struct tree_descr_struc {
        size_t count;
        tree_descr_type sub1, sub2;
        bool LR;
        bool RL;

        tree_descr_struc(size_t c, tree_descr_type s1, tree_descr_type s2)
            : count(c), sub1(), sub2(), LR(false), RL(false)
        {
            if ((*s1) == (*s2)) {
                LR = RL = true;
                sub1 = s1;
                sub2 = s2;
            } else if ((*s1) < (*s2)) {
                LR = true;
                sub1 = s1;
                sub2 = s2;
            } else {
                RL = true;
                sub1 = s2;
                sub2 = s1;
            }
        }

        tree_descr_struc(size_t c, tree_descr_type s1)
            : count(c), sub1(s1), sub2(), LR(true), RL(false)
        {}

        tree_descr_struc(size_t c)
            : count(c), sub1(), sub2(), LR(true), RL(false)
        {}

        bool is_null() const { return static_cast<const void*>(this) == NULL; }

        bool operator == (const tree_descr_struc& other) const
        {
            if (is_null()) {
                return other.is_null();
            }
            if (other.is_null()) {
                return is_null();
            }
            return count == other.count
                && (sub1 == other.sub1 || *sub1 == *other.sub1)
                && (sub2 == other.sub2 || *sub2 == *other.sub2);
        }

        bool operator < (const tree_descr_struc& other) const
        {
            if (!(this - static_cast<const tree_descr_struc*>(0ULL))) {
                return !!(&other - static_cast<const tree_descr_struc*>(0ULL));
            }
            if (!(&other - static_cast<const tree_descr_struc*>(0ULL))) {
                return false;
            }
            return count < other.count
                || (count == other.count
                    && (*sub1 < *other.sub1
                        || (*sub1 == *other.sub1 && *sub2 < *other.sub2)));
        }

        void reset_cursor(tree_descr_bool_map_type& cursors)
        {
            cursors[this] = !LR;
            /*MSG_DEBUG_INDENT_EXPR("[RC " << LR << RL << "] ");*/
            /*MSG_DEBUG(cursors[this]);*/
            if (sub1) {
                sub1->reset_cursor(cursors);
            }
            if (sub2) {
                sub2->reset_cursor(cursors);
            }
            /*MSG_DEBUG_DEDENT;*/
        }

        void reset_visited(tree_descr_bool_map_type& visited)
        {
            visited[this] = false;
            if (sub1) { sub1->reset_visited(visited); }
            if (sub2) { sub2->reset_visited(visited); }
        }

        bool next(tree_descr_bool_map_type& cursors, tree_descr_bool_map_type& visited)
        {
            if (!(this - static_cast<const tree_descr_struc*>(0ULL)) || visited[this]) {
                return true;
            }
            visited[this] = true;
            bool carry = true;
            if (LR && RL) {
                if (sub2->next(cursors, visited)) {
                    if (sub1->next(cursors, visited)) {
                        carry = cursors[this];
                        cursors[this] = !cursors[this];
                    } else {
                        carry = false;
                    }
                } else {
                    carry = false;
                }
            } else if (sub2) {
                carry = sub2->next(cursors, visited);
                carry &= sub1->next(cursors, visited);
            } else if (sub1) {
                carry = sub1->next(cursors, visited);
            }
            return carry;
        }

        size_t count_permutations(tree_descr_bool_map_type& visited)
        {
            if (visited[this]) {
                return 1;
            }
            visited[this] = true;
            if (sub1) {
                if (sub2) {
                    size_t count = LR + RL;
                    return count * sub1->count_permutations(visited) * sub2->count_permutations(visited);
                }
                return sub1->count_permutations(visited);
            }
            return 1;
        }

        template <typename FBefore, typename FAfter>
            void
            visit(int node, const pedigree_tree_type& tree,
                  const tree_descr_bool_map_type& cursors, tree_descr_bool_map_type& visited,
                  FBefore before, FAfter after)
            {
                /*MSG_DEBUG("visiting " << (*this) << " node " << node);*/
                /*MSG_QUEUE_FLUSH();*/
                bool has_p1 = tree.m_nodes[node].has_p1();
                bool has_p2 = tree.m_nodes[node].has_p2();
                bool v = visited[this];
                visited[this] = true;
                bool cursor = cursors.find(this)->second;
                if (!before(node, has_p1, has_p2, cursor, v)) {
                    return;
                }
                if (cursor) {
                    /*MSG_DEBUG("cursor TRUE");*/
                    /* R-to-L.
                     * p2 then p1
                     * if LR is set then: use sub2/p2, sub1/p1
                     * else: use sub1/p2, sub2/p1
                     */
                    if (LR) {
                        /*MSG_DEBUG("LR TRUE");*/
                        /*MSG_QUEUE_FLUSH();*/
                        sub2->visit(tree.m_nodes[node].p2, tree, cursors, visited, before, after);
                        sub1->visit(tree.m_nodes[node].p1, tree, cursors, visited, before, after);
                    } else {
                        /*MSG_DEBUG("LR FALSE");*/
                        /*MSG_QUEUE_FLUSH();*/
                        sub1->visit(tree.m_nodes[node].p2, tree, cursors, visited, before, after);
                        sub2->visit(tree.m_nodes[node].p1, tree, cursors, visited, before, after);
                    }
                } else {
                    /*MSG_DEBUG("cursor FALSE");*/
                    /*MSG_QUEUE_FLUSH();*/
                    /* L-to-R.
                     * p1 then p2, and sub1 is p1.
                     */
                    if (has_p1) {
                        /*MSG_DEBUG("HAS P1");*/
                        /*MSG_QUEUE_FLUSH();*/
                        sub1->visit(tree.m_nodes[node].p1, tree, cursors, visited, before, after);
                    }
                    if (has_p2) {
                        /*MSG_DEBUG("HAS P2");*/
                        /*MSG_QUEUE_FLUSH();*/
                        sub2->visit(tree.m_nodes[node].p2, tree, cursors, visited, before, after);
                    }
                }
                after(node, has_p1, has_p2, cursor);
            }

        friend
            std::ostream& operator << (std::ostream& os, const tree_descr_struc& td)
            {
                static int recurse = 0;
                if (!((&td) - static_cast<const tree_descr_struc*>(0ULL))) {
                    return os;
                }
                os << td.count;
                if (td.sub1) {
                    ++recurse;
                    os << (*td.sub1);
                    --recurse;
                }
                if (td.sub2) {
                    ++recurse;
                    os << (*td.sub2);
                    --recurse;
                }
                os << '0';
                if (td.LR && !recurse) {
                    os << " [LR]";
                }
                if (td.RL && !recurse) {
                    os << " [RL]";
                }
                return os;
            }

        friend
            std::ostream& operator << (std::ostream& os, const tree_descr_type& td)
            {
                if (!td || td.get() == NULL) {
                    return os;
                }
                return os << (*td);
            }
    };

    typedef std::map<size_t, tree_descr_type> tree_descr_map_type;

    tree_descr_map_type
        compute_descr(int node) const
        {
            std::map<size_t, tree_descr_type> ret;
            auto all_counts = count_ancestors(node);
            for (const auto& kv: all_counts) {
                if (m_nodes[kv.first].has_p2()) {
                    ret[kv.first] = std::make_shared<tree_descr_struc>(kv.second, ret[m_nodes[kv.first].p1], ret[m_nodes[kv.first].p2]);
                } else if (m_nodes[kv.first].has_p1()) {
                    ret[kv.first] = std::make_shared<tree_descr_struc>(kv.second, ret[m_nodes[kv.first].p1]);
                } else {
                    ret[kv.first] = std::make_shared<tree_descr_struc>(kv.second);
                }
                /*MSG_DEBUG("computed descr for #" << kv.first << ": " << ret[kv.first]);*/
            }
            if (m_nodes[node].has_p2()) {
                ret[node] = std::make_shared<tree_descr_struc>(1, ret[m_nodes[node].p1], ret[m_nodes[node].p2]);
            } else if (m_nodes[node].has_p1()) {
                ret[node] = std::make_shared<tree_descr_struc>(1, ret[m_nodes[node].p1]);
            } else {
                ret[node] = std::make_shared<tree_descr_struc>(1);
            }
            return ret;
        }

    struct subtree_configuration_iterator {
        tree_descr_bool_map_type curs;
        const pedigree_tree_type* tree;
        tree_descr_type descr;
        int node;

        subtree_configuration_iterator()
            : curs(), tree(NULL), descr(), node(NONE)
        {}

        subtree_configuration_iterator&
            operator = (const subtree_configuration_iterator& other)
            {
                tree = other.tree;
                descr = other.descr;
                node = other.node;
                curs.clear();
                descr->reset_cursor(curs);
                return *this;
            }

        subtree_configuration_iterator(const pedigree_tree_type* t, int n, tree_descr_type d)
            : curs(), tree(t), descr(d), node(n)
        {
            descr->reset_cursor(curs);
        }

        subtree_configuration_iterator(const pedigree_tree_type* t, int n, tree_descr_type d, const tree_descr_bool_map_type& cursors_)
            : curs(cursors_), tree(t), descr(d), node(n)
        {}

        const std::vector<bool>
            cursors() const
            {
                std::vector<bool> ret(curs.size(), false);
                tree_descr_bool_map_type vis;
                descr->reset_visited(vis);
                descr->visit(tree->root(), *tree, curs, vis,
                        [] (int, bool, bool, bool, bool v) { return !v; },
                        [&] (int n, bool, bool, bool c) { ret[n] = c; }
                        );
                return ret;
            }

        std::vector<int>
            node_order(bool p_before=false, bool once=false) const
            {
                std::vector<int> ret;
                ret.reserve(tree->m_nodes.size());
                tree_descr_bool_map_type vis;
                descr->reset_visited(vis);
                if (p_before) {
                    descr->visit(node, *tree, curs, vis,
                                 [&] (int, bool, bool, bool, bool v) { return !(v && once); },
                                 [&](int node, bool, bool, bool) { ret.push_back(node); });
                } else {
                    descr->visit(node, *tree, curs, vis,
                                 [&] (int node, bool, bool, bool, bool v) { if (v && once) { return false; } ret.push_back(node); return true; },
                                 [&](int, bool, bool, bool) {});
                }
                return ret;
            }

        std::vector<int>
            leaf_order(bool once=false) const
            {
                std::vector<int> ret;
                ret.reserve(tree->m_nodes.size());
                tree_descr_bool_map_type vis;
                descr->reset_visited(vis);
                descr->visit(node, *tree, curs, vis,
                             [&] (int, bool, bool, bool, bool v) { return !(v && once); },
                             [&](int node, bool, bool, bool) { ret.push_back(node); });
                return ret;
            }

        bool
            next()
            {
                tree_descr_bool_map_type vis;
                descr->reset_visited(vis);
                return descr->next(curs, vis);
            }

        size_t
            size() const
            {
                tree_descr_bool_map_type vis;
                descr->reset_visited(vis);
                return descr->count_permutations(vis);
            }
    };

    int root() const { return *m_roots.begin(); }

    subtree_configuration_iterator
        configuration_iterator() const
        {
            int node = root();
            return {this, node, compute_descr(node)[node]};
        }

    subtree_configuration_iterator
        configuration_iterator(const std::vector<bool>& cursors) const;

    /* Only if there is exactly one root... */
    tree_descr_type root_descr() const
    {
        int r = root();
        return compute_descr(r)[r];
    }
};


inline
pedigree_tree_type::tree_descr_bool_map_type
make_cursors(const pedigree_tree_type::tree_descr_map_type& descr, const std::vector<bool>& cursors)
{
    pedigree_tree_type::tree_descr_bool_map_type ret;
    MSG_DEBUG("make_cursors... descr.size=" << descr.size() << " cursors.size=" << cursors.size());
    MSG_QUEUE_FLUSH();
    for (int i = 0; i < (int) descr.size(); ++i) {
        ret[descr.find(i)->second.get()] = cursors[i];
    }
    return ret;
}


inline
pedigree_tree_type::subtree_configuration_iterator
pedigree_tree_type::configuration_iterator(const std::vector<bool>& cursors) const
{
    int node = root();
    auto descr = compute_descr(node);
    tree_descr_bool_map_type curs = make_cursors(descr, cursors);
    return {this, node, descr[node], curs};
}


inline
std::map<int, int>
compute_tree_mapping(const std::vector<int>& o1, const std::vector<int>& o2)
{
    if (o1.size() != o2.size()) {
        MSG_DEBUG("NODE ORDER SIZES DON'T MATCH");
        return {};
    }
    std::map<int, int> ret;
    MSG_DEBUG("o1 " << o1);
    MSG_DEBUG("o2 " << o2);
    for (size_t i = 0; i < o1.size(); ++i) {
        auto it = ret.find(o1[i]);
        if (it == ret.end()) {
            ret[o1[i]] = o2[i];
        } else if (it->second != o2[i]) {
            MSG_DEBUG("MAPPING FAILED");
            return {};
        }
    }
    return ret;
}

#if 0
struct expression_symmetries_type {
//     std::vector<std::vector<bool>> symmetry_cursors;
//     std::vector<std::vector<bool>> latent_symmetry_cursors;

    std::vector<std::vector<std::vector<int>>> input_cliques;
    std::vector<std::vector<std::vector<int>>> latent_input_cliques;

    const pedigree_tree_type* tree;

    expression_symmetries_type(const pedigree_tree_type& t)
        : /*symmetry_cursors(), latent_symmetry_cursors(),*/ input_cliques(), latent_input_cliques(), tree(&t)
    {
        MSG_DEBUG_INDENT_EXPR("[EXP SYM TYPE] ");
        auto descr = tree->root_descr();
        auto spi = tree->configuration_iterator();
        auto ref = spi.node_order();
        std::map<int, size_t> leaf_idx;
        for (int i: tree->m_leaves) {
            size_t sz = leaf_idx.size();
            leaf_idx[i] = sz;
        }
        do {
            MSG_DEBUG("testing order");
            MSG_DEBUG("" << spi.node_order());
            MSG_DEBUG("" << ref);
            auto mapping = compute_tree_mapping(ref, spi.node_order());
            if (mapping.size()) {
                MSG_DEBUG("mapping: " << mapping);
                auto cursors = spi.cursors();

                /* determine cliques */
                std::vector<std::vector<int>> cliques;
                std::vector<bool> visited(tree->m_leaves.size(), false);
                for (int l: tree->m_leaves) {
                    if (visited[leaf_idx[l]]) { continue; }
                    cliques.emplace_back(1, l);
                    visited[leaf_idx[l]] = true;
                    while (!visited[leaf_idx[(l = mapping[l])]]) {
                        cliques.back().push_back(l);
                        visited[leaf_idx[l]] = true;
                    }
                }
                MSG_DEBUG("input cliques: ");
                for (const auto& c: cliques) {
                    MSG_DEBUG("  " << c);
                }

                if (cursors.back() && descr->LR) {
                    /* latent! */
                    latent_symmetry_cursors.push_back(cursors);
                    latent_input_cliques.push_back(cliques);
                } else {
                    symmetry_cursors.push_back(cursors);
                    input_cliques.push_back(cliques);
                }
            }
        } while (!spi.next());
        MSG_DEBUG_DEDENT;
    }

    std::vector<bool>
        test_input_permutability_in_symmetries(const std::vector<pedigree_tree_type::tree_descr_type>& input_descriptors)
        {
            return test_input_permutability_in_(input_cliques, input_descriptors);
        }

    std::vector<bool>
        test_input_permutability_in_latent_symmetries(const std::vector<pedigree_tree_type::tree_descr_type>& input_descriptors)
        {
            return test_input_permutability_in_(latent_input_cliques, input_descriptors);
        }

    std::vector<bool>
        test_input_permutability_in_(const std::vector<std::vector<std::vector<int>>>& all_cliques,
                                     const std::vector<pedigree_tree_type::tree_descr_type>& input_descriptors)
        {
            std::vector<bool> ret;

            for (const auto& cliques: all_cliques) {
                bool ok = true;
                for (const auto& clique: cliques) {
                    auto d1 = input_descriptors[clique[0]];
                    for (size_t i = 1; i < clique.size(); ++i) {
                        ok &= (*d1 == *input_descriptors[clique[i]]);
                    }
                }
                ret.push_back(ok);
            }

            return ret;
        }
};



struct expression_mapping_type {
    std::map<int, int> mapping;
    std::vector<bool> cursors;
private:
    const pedigree_tree_type* tree;
    const pedigree_tree_type* ref;
    pedigree_tree_type::subtree_configuration_iterator spi;

    expression_mapping_type()
        : mapping(), cursors(), tree(NULL), ref(NULL), spi()
    {}

    expression_mapping_type(const pedigree_tree_type* r, const pedigree_tree_type* t)
        : mapping(), cursors(), tree(t), ref(r), spi()
    {
        auto ref_order = ref->configuration_iterator().node_order();
        spi = tree->configuration_iterator();
        do {
            mapping = compute_tree_mapping(ref_order, spi.node_order());
        } while (!mapping.size() && !spi.next());
        cursors = spi.cursors();
    }

public:

    friend
    expression_mapping_type
    create_mapping(const pedigree_tree_type& ref, const pedigree_tree_type& tree);
};

inline
expression_mapping_type
create_mapping(const pedigree_tree_type& ref, const pedigree_tree_type& tree)
{
    int rr = *ref.m_roots.begin();
    int tr = *tree.m_roots.begin();
    auto descr_ref = ref.compute_descr(rr)[rr];
    auto descr_tree = tree.compute_descr(tr)[tr];
    if (!(*descr_ref == *descr_tree)) {
        MSG_DEBUG("DESCRIPTORS DON'T MATCH");
        MSG_DEBUG("ref  " << descr_ref);
        MSG_DEBUG("tree " << descr_tree);
        return {};
    }
    return {&ref, &tree};
}


struct iterative_lumper {
    std::vector<int> bins;
    size_t input_size, output_size;

    iterative_lumper()
        : bins(), input_size(0), output_size(0)
    {}

    iterative_lumper(const iterative_lumper& other)
        : bins(other.bins), input_size(other.input_size), output_size(other.output_size)
    {}

    iterative_lumper(const std::set<subset>& P0)
        : bins(), input_size(0), output_size(P0.size())
    {
        for (const auto& S: P0) { input_size += S.size(); }
        bins.resize(input_size + 1, -1);
        int b = 0;
        for (const auto& S: P0) {
            for (int s: S) {
                bins[s] = b;
            }
            ++b;
        }
        bins[input_size] = output_size;
    }

    iterative_lumper(const MatrixXb& collect)
        : bins(collect.cols() ? collect.cols() + 1 : 0), input_size(collect.cols()), output_size(collect.rows())
    {
        for (int i = 0; i < collect.cols(); ++i) {
            int r;
            collect.col(i).maxCoeff(&r);
            bins[i] = r;
        }
        if (collect.cols()) {
            bins[collect.cols()] = collect.rows();
        }
        /*MSG_DEBUG("ITERATIVE LUMPER FROM MATRIX");*/
        /*MSG_DEBUG(collect);*/
        /*MSG_DEBUG("" << bins);*/
        /*MSG_DEBUG("input size " << input_size);*/
        /*MSG_DEBUG("output size " << output_size);*/
    }

    iterative_lumper&
        operator = (iterative_lumper&& other)
        {
            bins.swap(other.bins);
            input_size = other.input_size;
            output_size = other.output_size;
            return *this;
        }

    int operator [] (size_t i) const
    {
        /*MSG_DEBUG("REQUEST " << i << " in " << bins);*/
        /*MSG_DEBUG("input size " << input_size);*/
        /*MSG_DEBUG("output size " << output_size);*/
        return bins.size() ? bins[i] : i;
    }
};


struct extreme_lozenge_generator {
    typedef std::function<void(const iterative_lumper&)> op_func_type;

    std::vector<int> ref_order;
    std::vector<int> disorder;
    std::vector<int> sizes;
    std::vector<permutation_type> permutations;
    std::vector<int> iterators;
    std::vector<bool> rotations;
    std::vector<int> iterator_stack;
    std::vector<int> size_stack;
    std::vector<op_func_type> node_ops_ref, node_ops_dis;
    std::vector<char> node_types;
    std::vector<bool> visited;
    std::vector<std::vector<int>> gametes;
    std::vector<iterative_lumper> ref_collects, dis_collects;
    std::vector<bool> recompute_vec;
    std::vector<int> original_order;
    std::vector<bool> default_cursors;

    extreme_lozenge_generator(const pedigree_tree_type& tree, const std::vector<bool>& cursors)
        : ref_order(tree.configuration_iterator().node_order(true, false))
        , disorder(tree.configuration_iterator(cursors).node_order(true, false))
        , sizes(tree.m_nodes.size(), 1)
        , permutations(tree.m_nodes.size(), permutation_type{0})
        , iterators(tree.m_nodes.size(), 0)
        , rotations(tree.m_nodes.size(), 0)
        , iterator_stack()
        , size_stack()
        , node_ops_ref()
        , node_ops_dis()
        , node_types(tree.m_nodes.size(), 0)
        , visited()
        , original_order(tree.m_original_ordering)
        , default_cursors(tree.configuration_iterator().cursors())
    {
        pedigree_tree_type::tree_descr_bool_map_type vis;
        /*auto default_cursors = tree.configuration_iterator().cursors();*/
        MSG_DEBUG("default_cursors: " << default_cursors);
        MSG_DEBUG("cursors: " << cursors);
        auto descr = tree.compute_descr(tree.root());
        auto root_descr = descr[tree.root()];
        root_descr->reset_visited(vis);
        root_descr->visit(tree.root(), tree, make_cursors(descr, cursors), vis,
                [] (int, bool, bool, bool, bool v) { return !v; },
                [&, this] (int n, bool p1, bool p2, bool curs)
                {
                    if (p1 && !p2) {
                        /* On a gamete, propagate rotation of parent */
                        rotations[n] = rotations[tree.m_nodes[n].p1];
                    } else {
                        /* On a genotype, there is a rotation IIF the cursor is set AND LR is possible.
                         * There is no rotation if the cursor is set but only RL is set. */
                        MSG_DEBUG("rotation on node " << n << " curs=" << curs << " descr: " << descr[n]);
                        rotations[n] = curs & descr[n]->LR;
                    }
                });

        for (size_t n = 0; n < tree.m_nodes.size(); ++n) {
            if (tree.m_nodes[n].is_gamete()) {
                sizes[n] = 2;
                /*permutations[n] = permutation_type::identity(2);*/
                node_types[n] = 'g';
            } else if (tree.m_nodes[n].has_p2()) {
                node_types[n] = 'c';
            } else {
                node_types[n] = 'l';
            }
        }

        gametes.resize(tree.m_nodes.size(), {});
        for (size_t l = 0; l < tree.m_nodes.size(); ++l) {
            if (node_types[l] == 'g') {
                continue;
            }
            for (size_t n = l + 1; n < tree.m_nodes.size(); ++n) {
                if (tree.m_nodes[n].has_p2()) {
                    continue;
                }
                if (tree.m_nodes[n].p1 == (int) l) {
                    gametes[l].push_back(n);
                }
            }
        }

        init_op_list();
        ref_collects.resize(node_types.size());
        dis_collects.resize(node_types.size());
        recompute_vec = tree.get_deep_recompute_vec(tree.root());
    }


    template <typename PermutationGetter, typename RotationGetter>
        void
        init(PermutationGetter get_leaf_permut, RotationGetter get_leaf_rot)
        {
            for (size_t i = 0; i < node_types.size(); ++i) {
                if (node_types[i] == 'l') {
                    permutations[i] = get_leaf_permut(i);
                    MSG_DEBUG("permutation for leaf " << i);
                    MSG_DEBUG(permutations[i]);
                    sizes[i] = permutations[i].size();
                    rotations[i] = get_leaf_rot(i);
                    for (int g: gametes[i]) {
                        rotations[g] = rotations[i];
                    }
                    MSG_DEBUG("permutation is a " << (rotations[i] ? "latent " : "") << "symmetry");
                }
            }
        }

    void
        init_inner_rot(const std::vector<bool>& ref_cursors)
        {
            for (size_t i = 0; i < node_types.size(); ++i) {
                if (node_types[i] == 'c') {
                    rotations[i] = ref_cursors[i] ^ default_cursors[i];
                    for (int g: gametes[i]) {
                        rotations[g] = rotations[i];
                    }
                    MSG_DEBUG("permutation is a " << (rotations[i] ? "latent " : "") << "symmetry");
                }
            }
        }

    std::vector<int>
        compute()
        {
            std::vector<int> x, y;
            iterators.clear();
            iterators.resize(node_types.size(), 0);
            do {
                /*MSG_DEBUG("NEW ITERATION " << iterators);*/
                /*MSG_DEBUG_INDENT_EXPR("[ref_order] ");*/
                x.push_back(compute_index(ref_collects, node_ops_ref, ref_order));
                /*MSG_DEBUG_DEDENT;*/
                /*MSG_DEBUG_INDENT_EXPR("[disorder] ");*/
                y.push_back(compute_index(dis_collects, node_ops_dis, disorder));
                /*MSG_DEBUG_DEDENT;*/
            } while (!next());
            MSG_DEBUG("GLZ " << x);
            MSG_DEBUG("GLZ " << y);
            /* check x[i] == i for every i in [0;x.size()] */
            /*for (size_t i = 0; i < x.size(); ++i) {*/
                /*if (x[i] != (int) i) {*/
                    /*MSG_DEBUG("BAD INDEX AT #" << i << ": " << x[i]);*/
                    /*return {};*/
                /*}*/
            /*}*/
            std::vector<int> perm(1 + *std::max_element(x.begin(), x.end()), 0);
            for (size_t i = 0; i < x.size(); ++i) {
                perm[x[i]] = y[i];
            }
            MSG_DEBUG_INDENT_EXPR("[RESULT PERMUTATION] ");
            MSG_DEBUG("" << perm);
            MSG_DEBUG_DEDENT;
            return perm;
        }

    template <typename PermutationGetter, typename RotationGetter>
        permutation_type
        operator () (PermutationGetter get_leaf_permut, RotationGetter get_leaf_rot)
        {
            init(get_leaf_permut, get_leaf_rot);
            return permutation_type{compute()};
        }

    template <typename CollectGetter>
        void
        init_lumpers(const std::vector<int>& order, std::vector<iterative_lumper>& collects, CollectGetter get_node_lumper)
        {
            std::vector<bool> visited(node_types.size(), false);
            collects.clear();
            collects.resize(node_types.size());
            for (int n: order) {
                if (visited[n]) {
                    continue;
                }
                visited[n] = true;
                if (!recompute_vec[n]) {
                    collects[n] = iterative_lumper(get_node_lumper(original_order[n]));
                    MSG_DEBUG("collect[" << n << "] = " << collects[n].bins);
                }
            }
        }

    template <typename PermutationGetter, typename RotationGetter, typename CollectGetter>
        permutation_type
        operator () (PermutationGetter get_leaf_permut, RotationGetter get_leaf_rot, CollectGetter get_node_lumper)
        {
            init(get_leaf_permut, get_leaf_rot);
            init_lumpers(ref_order, ref_collects, get_node_lumper);
            init_lumpers(disorder, dis_collects, get_node_lumper);
            return permutation_type{compute()}; //.lump(get_node_lumper(original_order.back()));
        }

    bool
        next()
        {
            std::vector<bool> visited(ref_order.size(), false);
            bool carry = true;
            /*for (size_t i = 0; carry && i < ref_order.size(); ++i) {*/
            for (int i = ref_order.size() - 1; carry && i >= 0; --i) {
                int n = ref_order[i];
                if (visited[n]) {
                    continue;
                }
                visited[n] = true;
                ++iterators[n];
                carry = (iterators[n] == sizes[n]);
                if (carry) {
                    iterators[n] = 0;
                }
            }
            return carry;
        }

    int
        compute_index(const std::vector<iterative_lumper>& lumpers, const std::vector<std::function<void(const iterative_lumper&)>>& node_ops, const std::vector<int>& order)
        {
            visited.clear();
            visited.resize(node_types.size(), false);
            iterator_stack.clear();
            size_stack.clear();
            /*MSG_DEBUG("########################################");*/
            /*MSG_DEBUG("iterators " << iterators);*/
            for (size_t i = 0; i < order.size(); ++i) {
                int n = order[i];
                /*MSG_DEBUG("iterator stack " << iterator_stack);*/
                /*MSG_DEBUG("size stack " << size_stack);*/
                /*MSG_DEBUG("--- " << std::setw(3) << i << " - [" << node_types[n] << "] ----------------- I " << std::setw(50) << SPELL_STRING("" << iterator_stack) << " S " << std::setw(50) << SPELL_STRING("" << size_stack) << " node #" << n << " L " << lumpers[i].bins << ',' << lumpers[i].input_size << ',' << lumpers[i].output_size);*/
                /*MSG_QUEUE_FLUSH();*/
                node_ops[n](lumpers[n]);
                /*MSG_DEBUG("    " << std::setw(3) << i << " ----------------------- I " << std::setw(50) << SPELL_STRING("" << iterator_stack) << " S " << std::setw(50) << SPELL_STRING("" << size_stack));*/
                /*MSG_QUEUE_FLUSH();*/
            }
            /*MSG_DEBUG("------------------------------- I " << iterator_stack << " S " << size_stack);*/
#if 0
            int i_accum = 0;
            int s_accum = 1;
            int n = 0;
            for (size_t i = 0; i < iterator_stack.size(); ++i) {
                if (lumpers[n].bins.size()) {
                    MSG_DEBUG("LUMPING with " << lumpers[n].bins << " => i=" << lumpers[n][i_accum] << " s=" << lumpers[n][s_accum]);
                    MSG_QUEUE_FLUSH();
                    i_accum = lumpers[n][i_accum];
                    s_accum = lumpers[n][s_accum];
                    if (node_types[n] == 'c') {
                        ++n;
                    }
                }
                MSG_DEBUG("i_accum=" << i_accum << " s_accum=" << s_accum);
                MSG_QUEUE_FLUSH();
                i_accum = (i_accum * size_stack[i]) + iterator_stack[i];
                s_accum *= size_stack[i];
                ++n;
                /*if (node_types[n] == 'c') {*/
            }
            MSG_DEBUG("i_accum=" << i_accum << " s_accum=" << s_accum << " n=" << n);
            return i_accum;
#else
            /*MSG_DEBUG("final iterator stack " << iterator_stack);*/
            /*MSG_DEBUG("final size stack " << size_stack);*/
            /*MSG_QUEUE_FLUSH();*/
            return iterator_stack.back();
#endif
        }

    void
        geno_op(const iterative_lumper& lumper)
        {
            /*return;*/
#if 0
            int i_accum = 0;
            int s_accum = 1;
            while (size_stack.size()) {
                i_accum = (i_accum * size_stack.back()) + iterator_stack.back();
                s_accum *= size_stack.back();
                iterator_stack.pop_back();
                size_stack.pop_back();
            }
            iterator_stack.push_back(lumper[i_accum]);
            size_stack.push_back(lumper[s_accum]);
            return;
#endif

            int sz2 = size_stack.back();
            size_stack.pop_back();
            int sz1 = size_stack.back();
            size_stack.pop_back();

            int i2 = iterator_stack.back();
            iterator_stack.pop_back();
            int i1 = iterator_stack.back();
            iterator_stack.pop_back();

            /*MSG_DEBUG(" index " << (i1 * sz2 + i2) << " lumped into " << lumper[i1 * sz2 + i2] << " by " << lumper.bins << ',' << lumper.input_size << ',' << lumper.output_size);*/
            /*MSG_DEBUG(" pushing size " << lumper[sz1 * sz2]);*/
            iterator_stack.push_back(lumper[i1 * sz2 + i2]);
            size_stack.push_back(lumper[sz1 * sz2]);
        }

    bool
        not_visited(int i)
        {
            if (visited[i]) {
                /*size_stack.push_back(1);*/
                /*iterator_stack.push_back(0);*/
                /*MSG_DEBUG("ALREADY VISITED");*/
                return false;
            }
            visited[i] = true;
            return true;
        }

    op_func_type
        op_dynamic_gamete(size_t i)
        {
            return [i, this] (const iterative_lumper&)
            {
                if (not_visited(i)) {
                    iterator_stack.push_back(iterators[i] ^ rotations[i]);
                    size_stack.push_back(2);
                    geno_op({});
                } else {
                    iterator_stack.push_back(0);
                    size_stack.push_back(1);
                    geno_op({});
                }
            };
        }

    op_func_type
        op_static_gamete(size_t i)
        {
            return [i, this] (const iterative_lumper&)
            {
                if (not_visited(i)) {
                    iterator_stack.push_back(iterators[i]);
                    size_stack.push_back(2);
                    geno_op({});
                } else {
                    iterator_stack.push_back(0);
                    size_stack.push_back(1);
                    geno_op({});
                }
            };
        }

    op_func_type
        op_dynamic_leaf(size_t i)
        {
            return [i, this] (const iterative_lumper&)
            {
                if (not_visited(i)) {
                    iterator_stack.push_back(permutations[i][iterators[i]]);
                    size_stack.push_back(sizes[i]);
                } else {
                    iterator_stack.push_back(0);
                    size_stack.push_back(1);
                }
            };
        }

    op_func_type
        op_static_leaf(size_t i)
        {
            return [i, this] (const iterative_lumper&)
            {
                if (not_visited(i)) {
                    iterator_stack.push_back(iterators[i]);
                    size_stack.push_back(sizes[i]);
                } else {
                    iterator_stack.push_back(0);
                    size_stack.push_back(1);
                }
            };
        }

    op_func_type
        op_cross(size_t i)
        {
            return [i, this] (const iterative_lumper& lumper)
            {
                if (not_visited(i)) {
                    geno_op(lumper);
                } else {
                    geno_op({});
                }
            };
        }

    void
        init_op_list()
        {
            for (size_t i = 0; i < node_types.size(); ++i) {
                switch (node_types[i]) {
                    case 'l':
                        node_ops_ref.push_back(op_static_leaf(i));
                        node_ops_dis.push_back(op_dynamic_leaf(i));
                        break;
                    case 'g':
                        node_ops_dis.push_back(op_dynamic_gamete(i));
                        node_ops_ref.push_back(op_static_gamete(i));
                        break;
                    case 'c':
                        node_ops_ref.push_back(op_cross(i));
                        node_ops_dis.push_back(op_cross(i));
                        break;
                    default:
                        MSG_DEBUG("BAD NODE TYPE " << node_types[i]);
                };
            }
        }
};


template <typename CollectGetter>
permutation_type
align_tree(const pedigree_tree_type& ref, const pedigree_tree_type& tree, CollectGetter get_node_lumper)
{
    MSG_DEBUG("ALIGN TREE");
    MSG_DEBUG(ref);
    MSG_DEBUG(tree);
    auto mapping = create_mapping(ref, tree);
    extreme_lozenge_generator elg(tree, mapping.cursors);
    elg.init_inner_rot(ref.configuration_iterator().cursors());
    return elg([] (int) { return permutation_type::identity(1); },
               [] (int) { return false; },
               get_node_lumper); //.lump(get_node_lumper(tree.original_node_number(tree.root())));
}


struct symmetry_propagator {
    expression_symmetries_type exp_sym;
    std::vector<extreme_lozenge_generator> sym_propagators;
    std::vector<extreme_lozenge_generator> lat_sym_propagators;
    int input_count;
    int root;

    struct group_iterator {
        symmetry_group_type::const_iterator b, i, e, lb, le;
        bool latent;
        group_iterator(const symmetry_group_type& g, const symmetry_group_type& lg)
            : b(g.begin()), i(g.begin()), e(g.end()), lb(lg.begin()), le(lg.end()), latent(false)
        {
            MSG_DEBUG("NEW GROUP ITERATOR");
            MSG_DEBUG(g);
            MSG_DEBUG(lg);
            MSG_QUEUE_FLUSH();
        }
        bool operator ++ ()
        {
            ++i;
            if (i == e) {
                i = lb;
                latent = true;
            }
            if (i == le) {
                i = b;
                latent = false;
                return true;
            }
            return false;
        }
    };

    symmetry_propagator(const pedigree_tree_type& expr)
        : exp_sym(expr), sym_propagators(), lat_sym_propagators(), root(expr.original_node_number(expr.root()))
    {
        MSG_DEBUG_INDENT_EXPR("[SYM. PROPAG.] ");
        sym_propagators.reserve(exp_sym.symmetry_cursors.size());
        lat_sym_propagators.reserve(exp_sym.latent_symmetry_cursors.size());
        for (const auto& curz: exp_sym.symmetry_cursors) {
            sym_propagators.emplace_back(expr, curz);
        }
        for (const auto& curz: exp_sym.latent_symmetry_cursors) {
            lat_sym_propagators.emplace_back(expr, curz);
        }
        input_count = (int) expr.m_leaves.size();
        MSG_DEBUG_DEDENT;
    }

    int get_input_node(int i) const { return exp_sym.tree->original_node_number(exp_sym.tree->m_leaves[i]); }

    template <typename CollectGetter>
    permutation_type
        compute_propagated_permutation(
                const std::vector<symmetry_table_type>& inputs,
                const std::vector<bool>& is_latent,
                extreme_lozenge_generator& propagator,
                CollectGetter get_node_lumper)
        {
            return propagator([&] (int i) { return inputs[i].table; }, [&] (int i) { return is_latent[i]; }, get_node_lumper);
        }

    bool
        next_sym_vec(std::vector<group_iterator>& iterators)
        {
            for (auto& i: iterators) {
                if (!++i) {
                    return false;
                }
            }
            return true;
        }

    std::vector<symmetry_table_type>
        get_sym_vec(const std::vector<group_iterator>& iterators)
        {
            std::vector<symmetry_table_type> ret;
            ret.reserve(iterators.size());
            for (const auto& it: iterators) {
                ret.emplace_back(*it.i);
            }
            return ret;
        }

    std::vector<bool>
        get_is_latent_vec(const std::vector<group_iterator>& iterators)
        {
            std::vector<bool> ret;
            ret.reserve(iterators.size());
            for (const auto& it: iterators) {
                ret.emplace_back(it.latent);
            }
            return ret;
        }

    /*template <typename DescrGetter, typename SymmetryGroupGetter, typename LatentSymmetryGroupGetter>*/
    template <typename SubtreeGetter, typename SymmetryGroupGetter, typename LatentSymmetryGroupGetter, typename CollectGetter>
    symmetry_group_type
        compute_propagated_(
            bool is_latent,
            const std::vector<std::vector<std::vector<int>>>& cliques,
            std::vector<extreme_lozenge_generator>& propagators,
            /*DescrGetter get_input_descr,*/
            SubtreeGetter get_input_subtree,
            SymmetryGroupGetter get_sym_group,
            LatentSymmetryGroupGetter get_lat_sym_group,
            const std::vector<label_type>& labels,
            const MatrixXd inf_mat,
            CollectGetter get_node_lumper
            )
        {
            symmetry_group_type ret;
            std::vector<pedigree_tree_type::tree_descr_type> input_descriptors;
            input_descriptors.reserve(input_count);
            for (int i = 0; i < input_count; ++i) {
                /*input_descriptors.emplace_back(get_input_descr(i));*/
                input_descriptors.emplace_back(get_input_subtree(i).root_descr());
                MSG_DEBUG("DESCR[" << i << "] = " << input_descriptors.back());
            }

            std::vector<bool> unbroken_syms = exp_sym.test_input_permutability_in_(cliques, input_descriptors);

            MSG_DEBUG("GOING FOR " << propagators.size() << " PROPAGATORS");
            for (size_t p = 0; p < propagators.size(); ++p) {
                if (!unbroken_syms[p]) {
                    MSG_DEBUG("** SYMMETRY #" << p << " IS BROKEN **");
                    continue;
                }

                /* TODO: align inputs
                 * find rotation for each full input subtree to match everything in each input clique
                 */
                std::vector<permutation_type> alignment(input_count);
                for (const auto& clique: cliques[p]) {
                    const auto& ref = get_input_subtree(clique[0]);
                    MSG_DEBUG("ref for clique");
                    MSG_DEBUG(ref);
                    for (int i: clique) {
                        alignment[i] = align_tree(ref, get_input_subtree(i), get_node_lumper);
                        MSG_DEBUG("alignment[" << i << "] = ");
                        MSG_DEBUG(alignment[i]);
                        /* FIXME: IL FAUT LUMPER ! */
                        MSG_QUEUE_FLUSH();
                    }
                }

                std::vector<symmetry_group_type> input_groups;
                std::vector<symmetry_group_type> lat_input_groups;
                std::vector<group_iterator> sym_iterators;
                input_groups.reserve(input_count);
                lat_input_groups.reserve(input_count);
                sym_iterators.reserve(input_count);
                for (int i = 0; i < input_count; ++i) {
                    input_groups.push_back(get_sym_group(get_input_node(i)).permute(alignment[i]));  /* alignment should be factored in here */
                    lat_input_groups.push_back(get_lat_sym_group(get_input_node(i)).permute(alignment[i]));  /* alignment should be factored in here */
                    sym_iterators.emplace_back(input_groups.back(), lat_input_groups.back());
                }

                do {
                    auto sv = get_sym_vec(sym_iterators);
                    letter_permutation_type l = sv.front().letters;
                    bool ok = true;
                    for (size_t i = 1; ok && i < sv.size(); ++i) {
                        letter_permutation_type tmp;
                        ok = l.combine(sv[i].letters, tmp);
                        l = tmp;
                    }
                    if (!ok) {
                        continue;
                    }

                    auto sv_latent = get_is_latent_vec(sym_iterators);

                    auto result = symmetry_table_type::build(compute_propagated_permutation(sv, sv_latent, propagators[p], get_node_lumper), labels, inf_mat, is_latent);

                    if (result.first) {
                        ret.insert(result.second);
                    }

                } while (!next_sym_vec(sym_iterators));
            }

            return ret;
        }

    template <typename DescrGetter, typename SymmetryGroupGetter, typename LatentSymmetryGroupGetter, typename CollectGetter>
    symmetry_group_type
        compute_propagated_symmetries(DescrGetter get_input_descr, SymmetryGroupGetter get_sym_group, LatentSymmetryGroupGetter get_lat_sym_group,
                                      const std::vector<label_type>& labels, const MatrixXd& inf_mat, CollectGetter get_node_lumper)
        {
            return compute_propagated_(false, exp_sym.input_cliques, sym_propagators, get_input_descr, get_sym_group, get_lat_sym_group, labels, inf_mat, get_node_lumper);
        }

    template <typename DescrGetter, typename SymmetryGroupGetter, typename LatentSymmetryGroupGetter, typename CollectGetter>
    symmetry_group_type
        compute_propagated_latent_symmetries(DescrGetter get_input_descr, SymmetryGroupGetter get_sym_group, LatentSymmetryGroupGetter get_lat_sym_group,
                                             const std::vector<label_type>& labels, const MatrixXd& inf_mat, CollectGetter get_node_lumper)
        {
            return compute_propagated_(true, exp_sym.latent_input_cliques, lat_sym_propagators, get_input_descr, get_sym_group, get_lat_sym_group, labels, inf_mat, get_node_lumper);
        }
};
#endif


#if 0
struct pedigree_tree_layout {
    const pedigree_tree_type& tree;
    
    std::map<int, std::pair<int, int>> child_of;
    std::multimap<int, std::set<int>> partner_of;
    std::set<std::pair<int, int>> partners;
    std::map<std::pair<int, int>, std::set<int>> children;
    std::vector<int> rank;
    std::vector<int> column;

    std::set<subset> node_lists;

    std::pair<int, int>
        get_parents(int node)
        {
            return {tree.get_p1(tree.get_p1(node)), tree.get_p1(tree.get_p2(node))};
        }

    pedigree_tree_layout(const pedigree_tree_type& t)
        : tree(t), child_of(), partner_of(), partners(), children(), rank(t.size(), 0), node_lists()
    {
        for (size_t n = 0; n < tree.size(); ++n) {
            if (tree[n].is_crossing()) {
                auto parents = get_parents(n);
                partner_of[parents.first].insert(parents.second);
                partner_of[parents.second].insert(parents.first);
                partners.insert(parents);
                children[parents].insert(n);
                child_of[n] = parents;
                rank[n] = std::max(rank[parents.first], rank[parents.second]) + 1;
            }
        }

        for (const auto& p: partners) {
            subset s;
            s.push_back(p.first);
            s.push_back(p.second);
            s.insert(s.end(), children[p].begin(), children[p].end());
            node_lists.insert(s);
            auto i = s.begin();
            auto j = s.end();
            column[*i++] = (s.size() - 2) / 2;
            column[*i++] = 1 + (s.size() - 2) / 2;
            int x = 0;
            for (; i != j; ++i) {
                column[*i] = x++;
            }
        }
    }
};
#endif



#endif

