//
// Created by daleroux on 20/04/17.
//

#ifndef SPELL_QTL_TASK_POOL_H
#define SPELL_QTL_TASK_POOL_H


#include <atomic>
#include <functional>
#include <deque>
#include <future>


/* TODO remove Task class and handle it through a simple std::thread in TaskPool. And implement Task::wait() inside TaskPool. */

struct Task {
    virtual std::thread::id run() = 0;
    virtual bool has_run() const = 0;
};

struct TaskPool {
    static void take_slot() { instance()._take_slot(); }
    static void release_slot() { instance()._release_slot(); }

    static void init(unsigned int slots) { instance().m_free_slots.store(slots); }
    static TaskPool& instance() { static TaskPool _; return _; }
    static void join();

    static bool is_task(std::thread::id id)
    {
//        auto T = lock_instance();
//        return T->m_running_tasks.find(id) != T->m_running_tasks.end();
        auto& T = instance();
        return id != T.m_main_thread_id && id != T.m_task_runner.get_id();
    }

    static bool is_task()
    {
        return is_task(std::this_thread::get_id());
    }

    struct lock_instance_type {
        TaskPool* that;
        lock_instance_type() : that(&instance()) { that->lock(); }
        ~lock_instance_type() { that->unlock(); }
        TaskPool* operator -> () { return that; }
    };

    static lock_instance_type lock_instance() { return {}; }

    void lock() { m_slots_mutex.lock(); }
    void unlock() { m_slots_mutex.unlock(); }

    // add new work item to the pool
    static
    void enqueue(Task* task)
    {
        auto& T = instance();
        T.lock();
        T.m_pending_tasks.push_back(task);
        T.m_slots_cv.notify_all();
        T.unlock();
    }


    static void task_runner()
    {
        auto& T = instance();
        while (!T.m_quit) {
//            MSG_DEBUG("*** WAITING FOR A SLOT TO BE AVAILABLE");
//            std::function<void()> task;
//            std::thread* thread;
            Task* task;
            {
                std::unique_lock<std::recursive_mutex> lock(T.m_slots_mutex);
                T.m_slots_cv.wait(lock, [&]() { return (T.m_free_slots > 0 && T.m_pending_tasks.size() > 0) || T.m_quit; });
                if (T.m_quit) {
                    return;
                }
//                if (T.m_pending_tasks.size()) {
//                 T.m_slots_cv.wait(lock, [&]() { return T.m_free_slots > 0 || T.m_quit; });
//                 if (T.m_quit) {
//                     return;
//                 }
//                MSG_DEBUG(" ** HAVE A SLOT AND A TASK");
//                std::tie(task, thread) = T.m_pending_tasks.front();
                task = T.m_pending_tasks.front();
                T.m_pending_tasks.pop_front();
//                    MSG_DEBUG("thread " << thread << " should not be joinable: " << std::boolalpha << thread->joinable());
//                    MSG_QUEUE_FLUSH();
//                MSG_DEBUG(" ** There are still " << T.m_pending_tasks.size() << " tasks pending.");
                T.m_free_slots--;
            }

//            std::thread::id id = task->run();
            task->run();

//            T.lock();
//                    task.reset();
//            thread->get_id();
//            T.m_running_tasks.emplace(id);
//            MSG_DEBUG(" ** HAVE " << T.m_running_tasks.size() << " RUNNING TASKS AND " << T.m_free_slots.load() << " SLOTS.");
//            T.unlock();
//                }
        }
    }

    static void remove_task(std::thread::id id)
    {
        lock_instance()->m_running_tasks.erase(id);
    }

    void quit() { m_quit = true; m_slots_cv.notify_all(); m_task_runner.join(); }

    struct wait_guard {
        wait_guard()
        {
//            MSG_DEBUG("Task waiting on sub-task. Releasing slot.");
            if (TaskPool::is_task()) {
                TaskPool::release_slot();
            }
        }
        ~wait_guard()
        {
//            MSG_DEBUG("Task done waiting on sub-task. Acquiring slot.");
            if (TaskPool::is_task()) {
                TaskPool::take_slot();
            }
//            MSG_DEBUG("Task re-acquired slot.");
        }
    };

    static
    void
    wait(std::function<void()> f)
    {
//        if (is_task()) {
            wait_guard guard;
//            MSG_DEBUG("*** TASK IS WAITING ON SUB-TASK.")
            return f();
//        }
//        return f();
    }


    void
    _take_slot()
    {
        if (!is_task()) {
            return;
        }
        std::unique_lock<std::recursive_mutex> lock(m_slots_mutex);
//        MSG_DEBUG("… Waiting for a slot …");
        if (m_free_slots == 0) {
            m_slots_cv.wait(lock, [this]() { return m_free_slots > 0 || m_quit; });
        }
        m_free_slots--;
//        MSG_DEBUG("*** TOOK ONE SLOT. " << m_free_slots.load() << " LEFT.")
    }

    void
    _release_slot()
    {
        if (!is_task()) {
            return;
        }
//         {
            std::unique_lock<std::recursive_mutex> lock(m_slots_mutex);
            m_free_slots++;
//            MSG_DEBUG("*** FREED ONE SLOT. " << m_free_slots.load() << " LEFT.")
//         }
        m_slots_cv.notify_all();
    }

    void
    _set_slot_count(unsigned int n)
    {
        lock();
//        MSG_DEBUG("SET SLOT COUNT " << n);
        m_free_slots.store(m_free_slots.load() + n - m_total_slots.load());
        m_total_slots.store(n);
//        MSG_DEBUG("  Now slot count = " << m_free_slots.load());
        m_slots_cv.notify_all();
        unlock();
    }

    static void set_slot_count(unsigned int n) { instance()._set_slot_count(n); }

private:
    TaskPool(unsigned int slots=1)
            : m_free_slots(slots), m_total_slots(slots), m_pending_tasks(), m_slots_cv(), m_slots_mutex(), m_quit(false), m_running_tasks(), m_task_runner(task_runner), m_main_thread_id(std::this_thread::get_id())
    {}

    ~TaskPool() { quit(); }

    std::atomic_int m_free_slots, m_total_slots;
    std::deque<Task*> m_pending_tasks;
    std::condition_variable_any m_slots_cv;
    std::recursive_mutex m_slots_mutex;
    std::atomic_bool m_quit;
    std::unordered_set<std::thread::id> m_running_tasks;
    std::thread m_task_runner;
    std::thread::id m_main_thread_id;
};



#endif //SPELL_QTL_TASK_POOL_H
