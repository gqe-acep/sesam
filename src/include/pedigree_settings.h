/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_PEDIGREE_SETTINGS_H_
#define _SPELL_PEDIGREE_SETTINGS_H_

#include "error.h"
#include "commandline.h"
#include "pedigree.h"

struct ped_settings_t {
    std::string prg_name;
    std::vector<std::string> command_line;
    std::string pedigree_filename;
    char csv_sep;
    std::string work_directory;
    size_t max_states;
    bool with_LC;

    std::string pop_name;

    ped_settings_t()
        : prg_name()
        , command_line()
        , pedigree_filename()
        , csv_sep(';')
        , work_directory()
        , max_states(std::numeric_limits<decltype(max_states)>::max())
        , with_LC(true)
        , pop_name()
    {}

    static ped_settings_t* from_args(int argc, const char** argv);
};

#endif

