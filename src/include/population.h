/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_POP_H_
#define _SPEL_POP_H_

#include "polynom.h"
/*#include <Eigen/Core>*/
#include "eigen.h"
#include <sstream>
#include "file.h"
#include "fmin.h"
#include <set>

typedef std::pair<char, char> haplo_type;

struct gamete {
    haplo_type haplotype;
    polynom P;
    size_t i, j;
};



struct kern {
    bool f1;
    bool f2;
    polynom P;
    haplo_type operator () (const haplo_type& h1, const haplo_type& h2) { return { f1 ? h1.second : h1.first, f2 ? h2.second : h2.first }; }
};


extern const std::vector<kern> Kernel;


typedef labelled_matrix<PolynomMatrix, haplo_type> Generation;

inline Generation Homozygous(char symbol)
{
    Generation ret({{symbol, symbol}});
    ret.data(0, 0) = 1;
    return ret;
}


inline std::vector<gamete> Gametes(Generation P1)
{
    std::vector<gamete> G;

    Eigen::VectorXd genotype_weight = eval_poly(P1.data, .5).col(0);

    size_t i = 0;
    size_t j;
    polynom P = {0};
    for (auto h1: P1.row_labels) {
        j = 0;
        for (auto h2: P1.column_labels) {
            for (auto k: Kernel) {
                G.push_back({ k(h1, h2), k.P * P1.data(i, j) * genotype_weight(i), i, j });
                P += G.back().P;
            }
            ++j;
        }
        ++i;
    }
    /*std::cout << "Gametes probability check : " << P << std::endl;*/
    return G;
}


inline std::vector<std::vector<gamete>> SelfGametes(Generation P1)
{
    std::vector<std::vector<gamete>> GG;

    Eigen::VectorXd genotype_weight = eval_poly(P1.data, .5).col(0);

    /*std::cout << "Genotype weights" << std::endl << genotype_weight << std::endl;*/

    size_t i = 0;
    size_t j;
    polynom P = {0};
    for (auto h1: P1.row_labels) {
        j = 0;
        polynom R = {0};
        for (auto h2: P1.column_labels) {
            GG.push_back(std::vector<gamete>());
            polynom Q = {0};
            for (auto k: Kernel) {
                GG.back().push_back({ k(h1, h2), k.P, i, j });
                P += GG.back().back().P;
                Q += GG.back().back().P;
            }
            R += Q;
            /*std::cout << "SelfGametes[" << i << ',' << j << "] probability check : " << Q << std::endl;*/
            ++j;
        }
        /*std::cout << "SelfGametes[" << i << "] probability check : " << R << (R == polynom({genotype_weight(i)}) ? " (correct)": " (incorrect)") << ' ' << (R[0] - genotype_weight(i)) << std::endl;*/
        ++i;
    }
    /*std::cout << "SelfGametes probability check : " << P << std::endl;*/
    return GG;
}


inline Generation Cross(Generation P1, Generation P2)
{
    std::vector<gamete> G1 = Gametes(P1);
    std::vector<gamete> G2 = Gametes(P2);

    std::set<haplo_type> row_labels, column_labels;

    for (auto g1: G1) {
        for (auto g2: G2) {
            row_labels.insert({ g1.haplotype.first, g2.haplotype.first });
            column_labels.insert({ g1.haplotype.second, g2.haplotype.second });
        }
    }
    labelled_matrix<PolynomMatrix, haplo_type> C(row_labels.begin(), row_labels.end(),
                                                 column_labels.begin(), column_labels.end());

    for (auto g1: G1) {
        for (auto g2: G2) {
            haplo_type hrow = { g1.haplotype.first, g2.haplotype.first };
            haplo_type hcol = { g1.haplotype.second, g2.haplotype.second };
            C(hrow, hcol) += g1.P * g2.P;
        }
    }
    PolynomMatrix pm(C.innerSize(), C.outerSize());
    /*std::cout << "EVAL_POLY(C, 0)" << std::endl << eval_poly(C, 0) << std::endl;*/
    for (int i = 0; i < pm.innerSize(); ++i) {
        double ct = C.data(i, i)[0];
        pm(i, i) = { ct == 0 ? 0 : 1 / ct };
    }
    C.data *= pm;
    return C;
}


inline Generation Self(Generation P1)
{
    std::vector<std::vector<gamete>> GG = SelfGametes(P1);

    Eigen::VectorXd genotype_weight = eval_poly(P1.data, .5).col(0);

    std::set<haplo_type> row_labels, column_labels;

    for (auto G: GG) {
        for (auto g1: G) {
            for (auto g2: G) {
                row_labels.insert({ g1.haplotype.first, g2.haplotype.first });
                column_labels.insert({ g1.haplotype.second, g2.haplotype.second });
            }
        }
    }
    labelled_matrix<PolynomMatrix, haplo_type> C(row_labels.begin(), row_labels.end(),
                                                 column_labels.begin(), column_labels.end());

    for (auto G: GG) {
        for (auto g1: G) {
            for (auto g2: G) {
                haplo_type hrow = { g1.haplotype.first, g2.haplotype.first };
                haplo_type hcol = { g1.haplotype.second, g2.haplotype.second };
                C(hrow, hcol) += g1.P * g2.P * P1.data(g1.i, g1.j) * genotype_weight(g1.j);
            }
        }
    }
    PolynomMatrix pm(C.innerSize(), C.outerSize());
    /*std::cout << "EVAL_POLY(C, 0)" << std::endl << eval_poly(C, 0) << std::endl;*/
    for (int i = 0; i < pm.innerSize(); ++i) {
        double ct = C.data(i, i)[0];
        pm(i, i) = { ct == 0 ? 0 : 1 / ct };
    }
    C.data *= pm;
    return C;
}

#endif

