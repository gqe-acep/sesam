/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_CACHE_2_H_
#define _SPEL_CACHE_2_H_

#include "cache/base.h"
#include "disk_hashtable.h"
#include "task_pool.h"
#include "stl_output.h"

template <typename Ret, typename... Args>
computation_registry<value<Ret>, Ret (*) (Args...), value<typename clean_type<Args>::type>...>&
__get_registry()
{
    static computation_registry<value<Ret>, Ret (*) (Args...), value<typename clean_type<Args>::type>...> _reg_;
    /*MSG_DEBUG("Registry at " << (&_reg_));*/
    return _reg_;
}


template <typename Ret, typename... Args>
    struct async_computation<Ret(Args...)>;

template <typename Ret, typename... Args>
computation_registry<std::shared_ptr<async_computation<Ret(Args...)>>,
                     Ret (*) (Args...),
                     value<typename clean_type<Args>::type>...>&
__get_in_progress_registry()
{
    static computation_registry<std::shared_ptr<async_computation<Ret(Args...)>>,
                                Ret (*) (Args...),
                                value<typename clean_type<Args>::type>...> _reg_;
    /*MSG_DEBUG("Registry at " << (&_reg_));*/
    return _reg_;
}


template <typename Ret, typename... Args>
std::mutex& __get_in_progress_mutex() { static std::mutex _; return _; }

template <typename Ret, typename... Args>
void
unregister_task_in_progress(Ret (*f) (Args...),
                            const value<typename clean_type<Args>::type>&... args)
{
    __get_in_progress_mutex<Ret, Args...>().lock();
    __get_in_progress_registry<Ret, Args...>().remove(f, args...);
    __get_in_progress_mutex<Ret, Args...>().unlock();
}


template <typename Ret, typename... Args>
std::shared_ptr<async_computation<Ret(Args...)>>
register_task_in_progress(std::shared_ptr<async_computation<Ret(Args...)>> v,
                          Ret (*f) (Args...),
                          const value<typename clean_type<Args>::type>&... args)
{
    // __get_in_progress_mutex<Ret, Args...>().lock(); // Frank Gauthier 26/11/2020
    __get_in_progress_registry<Ret, Args...>().get(f, args...) = v;
    // __get_in_progress_mutex<Ret, Args...>().unlock(); // Frank Gauthier 26/11/2020

    return v;
}


template <typename Ret, typename... Args>
struct lock_in_progress_mutex {
    lock_in_progress_mutex() { __get_in_progress_mutex<Ret, Args...>().lock(); }
    ~lock_in_progress_mutex() { __get_in_progress_mutex<Ret, Args...>().unlock(); }
};


template <typename Ret, typename... Args>
    struct async_computation<Ret(Args...)> : public Task {
        typedef async_computation<Ret(Args...)> this_type;
        typedef Ret value_type;
        typedef Ret (*computation_function_pointer_type) (Args...);
        typedef std::packaged_task<Ret(Args...)> task_type;

        async_computation(const async_computation&) = delete;
        async_computation() = delete;

//        ~async_computation() { if (m_thread.joinable()) { m_thread.join(); } }

        async_computation(CachingPolicy _Sync, computation_function_pointer_type func,
                          const value<typename clean_type<Args>::type>&... args)
            : dependencies(args...)
            , m_storage_init(false)
            , m_storage()
            , m_future()
            , m_compute([=] () {
                {
                    std::unique_lock<std::mutex> lock(mutex);
                    m_started = true; //// DEBUG false positive ???? helgrind warning: Possible data race during write of size 1 by thread #4. This conflicts with a previous read of size 1 by thread #1. see line 282
                    m_started_cv.notify_all();
                }
/*
                    bool previous = m_started.exchange(true);
                    if (previous) {
                        MSG_DEBUG("Task " << get_func_name(func) << " already run. Returning.");
//                        unregister_task_in_progress(func, {*args}...);
                        TaskPool::release_slot();
                        TaskPool::remove_task(m_thread.get_id());
                        return;
                    }
*/
                    std::string func_name = get_func_name(func);
                    chrono_trace _(func_name);
                    if (0) {
//                        msg_handler_t::cout() << "INVOKING " << func_name << "(…)" << std::endl;
                        std::stringstream ss;
                        ss << "INVOKING " << func_name << "(" << std::endl;
                        std::vector<std::string> avec;
                        do_with_arg_pack(avec.push_back(SPELL_STRING(args)));
                        for (size_t i = 0; i < avec.size() - 1; ++i) {
                            ss << avec[i] << ", ";
                        }
                        ss << ')' << std::endl;
                        msg_handler_t::cout() << ss.str();
                    }
//                    std::thread::id this_id = std::this_thread::get_id();
//                                active_settings->thread_stacks[this_id].push_back(func_name);
//                    MSG_DEBUG("[" << this_id << ',' << (this_id == active_settings->main_thread) << "] ENTER " << func_name);
//                     msg_handler_t::run_hooks();

                    auto ret = func(*args...);
//                                active_settings->thread_stacks[this_id].pop_back();
//                    msg_handler_t::run_hooks();
                    unregister_task_in_progress(func, {*args}...);
                    TaskPool::release_slot();
//                    TaskPool::remove_task(m_thread->get_id());
//                    MSG_DEBUG("[" << this_id << ',' << (this_id == active_settings->main_thread) << "] LEAVE " << func_name);
//                     mutex.lock();
//                     m_started_cv.notify_all();
//                     mutex.unlock();
                    return ret;
                })
            , mutex()
            , m_started(false)
            , m_started_cv()
            , m_storage_waiting(false)
            , m_storage_waiting_cv()
        {
//            MSG_DEBUG("In constructor, thread " << (&m_thread) << " should not be joinable: " << std::boolalpha << m_thread.joinable());
//             std::unique_lock<std::mutex> lock(mutex);
//            if (!m_started) {
//                m_started.store(true);
                TaskPool::enqueue(this);
//            }
        }

        async_computation(CachingPolicy _Sync, std::function<Ret(Args...)>& proxy,
                          computation_function_pointer_type func,
                          const value<typename clean_type<Args>::type>&... args)
            : dependencies(args...)
            , m_storage_init(false)
//            , m_thread()
            , m_storage()
            /*, m_future(active_settings->enqueue(_Sync, func, *args...))*/
//            , m_promise()
            , m_future()
            , m_compute([=] () {
                {
                    std::unique_lock<std::mutex> lock(mutex);
                    m_started = true;
                    m_started_cv.notify_all();
                }

//                     {
//                         std::unique_lock<std::mutex> lock(mutex);
//                         m_started_cv.notify_all();
//                     }
/*
                    bool previous = m_started.exchange(true);
                    if (previous) {
                        MSG_DEBUG("Task " << get_func_name(func) << " already run. Returning.");
//                        unregister_task_in_progress(func, {*args}...);
                        TaskPool::release_slot();
                        TaskPool::remove_task(m_thread.get_id());
                        return;
                    }
*/
                    std::string func_name = get_func_name(func);
                    chrono_trace _(func_name);
                    if (0) {
//                        msg_handler_t::cout() << "INVOKING " << func_name << "(…)" << std::endl;
                        std::stringstream ss;
                        ss << "INVOKING " << func_name << "(" << std::endl;
                        std::vector<std::string> avec;
                        do_with_arg_pack(avec.push_back(SPELL_STRING(args)));
                        for (size_t i = 0; i < avec.size() - 1; ++i) {
                            ss << avec[i] << ", ";
                        }
                        ss << ')' << std::endl;
                        msg_handler_t::cout() << ss.str();
                    }
//                    std::thread::id this_id = std::this_thread::get_id();
//                                active_settings->thread_stacks[this_id].push_back(func_name);
//                    MSG_DEBUG("[" << this_id << ',' << (this_id == active_settings->main_thread) << "] p ENTER " << func_name);
//                    msg_handler_t::run_hooks();
//                    m_promise.set_value(proxy(*args...));
//                     mutex.lock();
                    auto ret = proxy(*args...);
//                     mutex.unlock();
//                    MSG_DEBUG("[" << this_id << ',' << (this_id == active_settings->main_thread) << "] p LEAVE " << func_name);
//                                active_settings->thread_stacks[this_id].pop_back();
//                    msg_handler_t::run_hooks();
                    unregister_task_in_progress(func, {*args}...);
                    TaskPool::release_slot();
//                    TaskPool::remove_task(m_thread->get_id());

//                     mutex.lock();
//                     m_started_cv.notify_all();
//                     mutex.unlock();

                    return ret;
                })
            /*, m_future(std::async(func, *args...))*/
            , mutex()
            , m_started(false)
            , m_started_cv()
            , m_storage_waiting(false)
            , m_storage_waiting_cv()
        {
//            MSG_DEBUG("In constructor (w/ proxy), thread " << (&m_thread) << " should not be joinable: " << std::boolalpha << m_thread.joinable());
//             std::unique_lock<std::mutex> lock(mutex);
//            if (!m_started) {
//                m_started.store(true);
                TaskPool::enqueue(this);
//            }
        }

        bool has_run() const override { return m_started; }

        std::thread::id run() override
        {
            if (m_started) {
                MSG_ERROR("Task has already run.", "");
                return {};
            } else {
                std::unique_lock<std::mutex> lock(mutex); //// DEBUG false positive ???? helgrind says: Lock at 0x9D3E378 was first observed here, and 0x9D3E378 is inside a block alloc'd line 347 (new())
                m_future = std::async(std::launch::async, m_compute);
//                m_thread.reset(new std::thread(std::move(m_compute)));
//                m_thread = new std::thread(std::move(m_compute));
//                m_thread.detach();
//                return m_thread->get_id();
                return {};
            }
        }

        value_type& __get_noconst()
        {
            /*std::lock_guard<decltype(mutex)> read_guard(mutex);*/
            /*if (m_future.valid()) {*/
                /*return m_storage = m_future.get();*/
            /*}*/
//            mutex.lock();
//            if (!m_storage_init && m_future.valid()) {
//                mutex.unlock();
            if (m_storage_init) {
                return m_storage;
            }
//             TaskPool::wait([&lock, this]() {
//                    mutex.lock();
            TaskPool::release_slot();
            {
                if (!m_started.load()) { //// DEBUG false positive ???? helgrind warning: Possible data race during read by thread #1 but locks held: none (see line 109)
                    std::unique_lock<std::mutex> lock(mutex);
                    m_started_cv.wait(lock, [this]() { return m_started.load(); });
                }
//                 if (!m_storage_init && m_future.valid()) {
                    bool waiting_for_storage = m_storage_waiting.exchange(true);
                    if (waiting_for_storage) {
                        std::unique_lock<std::mutex> lock(mutex);
                        m_storage_waiting_cv.wait(lock, [this] () -> bool { return m_storage_init; });
                    } else {
//                         mutex.unlock(); //// DEBUG m_furure is read: m_future.get() but not locked
                        m_storage = m_future.get(); //// DEBUG false positive ???? helgrind warning: Possible data race during read of size 8 by thread #1, this conflicts with a previous write of size 8 by thread #2 (see line 257)
                        std::unique_lock<std::mutex> lock(mutex);
//                         mutex.lock();
                        m_storage_init.store(true);
                        m_storage_waiting_cv.notify_all();
//                         mutex.unlock();
                    }
//                 }
            }
            TaskPool::take_slot();
//                    mutex.unlock();
//             });
//                mutex.lock();
//            }
//            mutex.unlock();
            return m_storage;
        }

        const value_type& __get_const() const
        {
            return const_cast<this_type*>(this)->__get_noconst();
        }

    protected:
        std::tuple<value<typename clean_type<Args>::type>...> dependencies;
        mutable std::atomic_bool m_storage_init;
//        std::unique_ptr<std::thread> m_thread;
        mutable value_type m_storage;
        mutable std::future<Ret> m_future;
        std::function<Ret()> m_compute;
        mutable std::mutex mutex;
        mutable std::atomic_bool m_started;
        mutable std::condition_variable m_started_cv;
        mutable std::atomic_bool m_storage_waiting;
        mutable std::condition_variable m_storage_waiting_cv;
    };

template <typename Ret, typename... Args>
std::shared_ptr<async_computation<Ret(Args...)>>
    make_async_computation(CachingPolicy& _Sync,
                           Ret (*func) (Args...),
                           const value<typename clean_type<Args>::type>&... args)
    {
        /*struct _mac_guard {*/
            /*_mac_guard() { __get_in_progress_mutex<Ret, Args...>().lock(); }*/
            /*~_mac_guard() { __get_in_progress_mutex<Ret, Args...>().unlock(); }*/
        /*} _;*/
//         std::lock_guard<std::mutex> scope_lock(__get_in_progress_mutex<Ret, Args...>());
        __get_in_progress_mutex<Ret, Args...>().lock(); //// DEBUG line added 2020-12-11
        auto& r = __get_in_progress_registry<Ret, Args...>();
        auto exists = r.find(func, args...);
        __get_in_progress_mutex<Ret, Args...>().unlock(); //// DEBUG line added 2020-12-11
        if (exists) {
            return *exists;
        } else {
            std::shared_ptr<async_computation<Ret(Args...)>>
                ac(new async_computation<Ret(Args...)>(_Sync, func, args...));
            lock_in_progress_mutex<Ret, Args...> _; //// DEBUG this lock is ok : it is unlocked when leaving this else block via its destructor.
            return r.get(func, args...) = register_task_in_progress(ac, func, args...);
        }
    }


template <typename Ret, typename... Args>
std::shared_ptr<async_computation<Ret(Args...)>>
    make_async_computation(CachingPolicy& _Sync,
                           Ret (*func) (Args...),
                           std::function<Ret(Args...)>& proxy,
                           const value<typename clean_type<Args>::type>&... args)
    {
        /*struct _mac_guard {*/
            /*_mac_guard() { __get_in_progress_mutex<Ret, Args...>().lock(); }*/
            /*~_mac_guard() { __get_in_progress_mutex<Ret, Args...>().unlock(); }*/
        /*} _;*/
//         std::lock_guard<std::mutex> scope_lock(__get_in_progress_mutex<Ret, Args...>());
        __get_in_progress_mutex<Ret, Args...>().lock();
        auto& r = __get_in_progress_registry<Ret, Args...>();
        auto exists = r.find(func, args...);
        __get_in_progress_mutex<Ret, Args...>().unlock();
        if (exists) {
            return *exists;
        } else {
            std::shared_ptr<async_computation<Ret(Args...)>>
                ac(new async_computation<Ret(Args...)>(_Sync, proxy, func, args...));
            lock_in_progress_mutex<Ret, Args...> _;
            return r.get(func, args...) = register_task_in_progress(ac, func, args...);
        }
    }


#ifndef NDEBUG
#define THIS_ARG_MAY_BE_UNUSED(x) x
#else
#define THIS_ARG_MAY_BE_UNUSED(x)
#endif

template <typename Ret, typename... Args>
    struct cached_computation<Ret(Args...)> {
        typedef Ret value_type;

        cached_computation(const std::string& name, Ret (*f) (Args...), const value<typename clean_type<Args>::type>&... THIS_ARG_MAY_BE_UNUSED(args))
            : m_name(name)
            , m_func(f)
#ifndef NDEBUG
            , dump_call()
#endif
        {
#ifndef NDEBUG
            dump_call = [=] () {
                std::stringstream ss;
                ss << m_name << '(';
                std::vector<std::string> avec;
                do_with_arg_pack(avec.push_back(SPELL_STRING(args)));
                for (size_t i = 0; i < avec.size() - 1; ++i) {
                    ss << avec[i] << ", ";
                }
                ss << avec.back() << ')';
                return ss.str();
            };
#endif
            /*MSG_DEBUG("NEW CACHED_COMPUTATION WITH ARGS " << dump());*/
            /*MSG_DEBUG("MD5 (accum) = " << m_md5_hash.accum);*/
            /*MSG_DEBUG("MD5 (append) = " << m_md5_hash.append);*/
            /*std::cout << "NEW CACHED_COMPUTATION " << m_md5_hash.accum << ' ' << m_md5_hash.append << std::endl;*/
        }

        std::string
            dump()
            {
#ifndef NDEBUG
                return dump_call();
#else
                return m_name;
#endif
            }

        Ret operator () (Args... x)
        {
            static disk_hashtable<Ret(Args...)> dht(SPELL_STRING(active_settings->work_directory << '/' << active_settings->name << ".cache/" << m_name));
            return dht.get_or_compute(m_func, std::forward<Args>(x)...);
        }

        std::string m_name;
        Ret (*m_func) (Args...);

#ifndef NDEBUG
        std::function<std::string()> dump_call;
#endif
    };


template <typename Ret, typename... Args>
    struct computed_value<Ret(Args...)> : generic_value_interface<Ret> {
        typedef Ret value_type;
        typedef std::function<Ret(Args...)> computation_type;

        computed_value(CachingPolicy _Sync, Ret (*func) (Args...), const value<typename clean_type<Args>::type>&... args)
            : m_hash(compute_hash(args...))
            , m_task(make_async_computation<Ret, Args...>(_Sync, func, args...))
        {}

        virtual
            value_type& operator * ()
            override { return m_task->__get_noconst(); }
        virtual
            value_type* operator -> ()
            override { return &m_task->__get_noconst(); }
        virtual
            const value_type& operator * () const
            override { return m_task->__get_const(); }
        virtual
            const value_type* operator -> () const
            override { return &m_task->__get_const(); }

        virtual
            size_t hash() const
            override { return m_hash; }
        virtual
            md5_digest& md5(md5_digest& md) const
            override { return md; }

    /*protected:*/
        size_t m_hash;
        std::shared_ptr<async_computation<Ret(Args...)>> m_task;
    };

template <typename Ret, typename... Args>
    struct cached_computed_value<Ret(Args...)> : generic_value_interface<Ret> {
        typedef Ret value_type;
        typedef std::function<Ret(Args...)> computation_type;

        cached_computed_value(CachingPolicy _Sync, Ret (*func) (Args...), const value<typename clean_type<Args>::type>&... args)
            : m_comp(get_func_name(func), func, args...)
            , m_comp_proxy([this](Args... x) { return m_comp(x...); })
            , m_task(make_async_computation<Ret, Args...>(_Sync, func, m_comp_proxy, args...))
        {}
            /*m_hash = m_comp.m_md5_hash.md5.context;*/
        /*}*/

        virtual
            value_type& operator * ()
            override { return m_task->__get_noconst(); }
        virtual
            value_type* operator -> ()
            override { return &m_task->__get_noconst(); }
        virtual
            const value_type& operator * () const
            override { return m_task->__get_const(); }
        virtual
            const value_type* operator -> () const
            override { return &m_task->__get_const(); }

        virtual
            size_t hash() const override
            /*override { return m_hash; }*/
            {
                (void)m_task->__get_const();
                MSG_ERROR("hash is not implemented for cached_computed_value", "");
                return 0;
                /*return m_comp.m_md5_hash.md5.context;*/
            }
        virtual
            md5_digest& md5(md5_digest& md) const override
            {
                (void)m_task->__get_const();
                /*return md.blend(m_comp.m_md5_hash.md5.context);*/
                MSG_ERROR("md5 is not implemented for cached_computed_value", "");
                /*return md.blend(m_comp.m_md5_hash.md5.context);*/
                return md;
            }
            /*override { return md; }*/

    /*protected:*/
        cached_computation<Ret(Args...)> m_comp;
        std::function<Ret(Args...)> m_comp_proxy;
        std::shared_ptr<async_computation<Ret(Args...)>> m_task;
        /*size_t m_hash;*/
    };


/*template <typename X>*/
/*value<X> as_value(X&& x) { return {x}; }*/

#if 0
template <typename ValueType, typename... AllArgs> struct registry_impl_type;
template <typename ValueType>
    struct registry_impl_type<ValueType> { typedef ValueType type; };
template <typename ValueType, typename Arg0, typename... OtherArgs>
    struct registry_impl_type<ValueType, Arg0, OtherArgs...> {
        typedef std::unordered_map<Arg0, registry_impl_type<ValueType, OtherArgs...>::type> type;
    };
#endif


#if 0
static inline
std::mutex& __get_lock(void* fptr)
{
    struct m_wrap { std::mutex mutex; m_wrap() : mutex() {} m_wrap(const m_wrap&) : mutex() {} };
    static std::unordered_map<void*, m_wrap> _;
    return _[fptr].mutex;
    /*auto it = _.find(fptr);*/
    /*if (it == _.end()) {*/
        /*bool discard;*/
        /*std::tie(it, discard) = _.insert({fptr, {}});*/
    /*}*/
    /*return it->second;*/
}
#endif


template <typename Ret, typename... Args>
struct with_disk_cache_traits {
	typedef cached_computed_value<Ret(Args...)> type;
};

template <typename Ret, typename... Args>
struct without_disk_cache_traits {
	typedef computed_value<Ret(Args...)> type;
};

template <int _Policy, typename Ret, typename... Args>
struct disk_cache_traits;

template <typename Ret, typename... Args>
struct disk_cache_traits<Oneshot, Ret, Args...> : without_disk_cache_traits<Ret, Args...> {};

template <typename Ret, typename... Args>
struct disk_cache_traits<Disk, Ret, Args...> : with_disk_cache_traits<Ret, Args...> {};

template <typename Ret, typename... Args>
struct with_mem_cache_traits {
	typedef value<Ret>& return_type;

	template <typename _Maker>
		static
		return_type
		create(CachingPolicy _Sync, Ret (&f) (Args...), const clean_value_type<Args>&... x)
		{
            static std::recursive_mutex _;
            std::lock_guard<decltype(_)> lock_guard(_);

            /*value<Ret>* ret_in_progress = __get_in_progress_registry<Ret, Args...>().find(&f, x...);*/

            /*if (ret_in_progress) {*/
                /*return *ret_in_progress;*/
            /*}*/

			/*MSG_DEBUG("new value with mem cache");*/
			return_type ret = __get_registry<Ret, Args...>().get(&f, x...);
			if (!ret.valid()) {
				ret = new _Maker(_Sync, f, x...);
			}
			return ret;
		}
};

template <typename Ret, typename... Args>
struct without_mem_cache_traits {
	typedef value<Ret> return_type;

	template <typename _Maker>
		static
		return_type
		create(CachingPolicy _Sync, Ret (&f) (Args...), const clean_value_type<Args>&... x)
		{
            static std::recursive_mutex _;
            std::lock_guard<decltype(_)> lock_guard(_);
            /*value<Ret>* ret_in_progress = __get_in_progress_registry<Ret, Args...>().find(&f, x...);*/

            /*if (ret_in_progress) {*/
                /*return *ret_in_progress;*/
            /*}*/

			/*MSG_DEBUG("new value without mem cache");*/
			return_type ret = new _Maker(_Sync, f, x...);
			return ret;
		}
};

template <int _Policy, typename Ret, typename... Args>
struct mem_cache_traits;

template <typename Ret, typename... Args>
struct mem_cache_traits<Oneshot, Ret, Args...> : without_mem_cache_traits<Ret, Args...> {};

template <typename Ret, typename... Args>
struct mem_cache_traits<Mem, Ret, Args...> : with_mem_cache_traits<Ret, Args...> {};

template <CachingPolicy _Policy = Oneshot, typename Ret, typename... Args>
typename mem_cache_traits<_Policy & Mem, Ret, Args...>::return_type
make_value(Ret (&f) (Args...), const clean_value_type<Args>&... x)
{
	typedef mem_cache_traits<_Policy & Mem, Ret, Args...> mem_policy;
	typedef disk_cache_traits<_Policy & Disk, Ret, Args...> disk_policy;
	return mem_policy::template create<typename disk_policy::type>(_Policy, f, x...);
}


template <typename... X> struct tuple;

template <CachingPolicy C, typename F, typename R, typename P> struct make_coll_impl;

template <typename A, typename B> struct types_must_be_identical : std::integral_constant<bool, false> {};
template <> struct types_must_be_identical<int, double> : std::integral_constant<bool, false> {};
template <> struct types_must_be_identical<double, int> : std::integral_constant<bool, false> {};
template <typename A> struct types_must_be_identical<A, A> : std::integral_constant<bool, true> {};
template <typename A> struct types_must_be_identical<const A&, A> : std::integral_constant<bool, true> {};
template <typename A> struct types_must_be_identical<A&, A> : std::integral_constant<bool, true> {};
template <typename A> struct types_must_be_identical<const A, A> : std::integral_constant<bool, true> {};

template <typename LA, typename LB> struct type_lists_must_be_identical;
template <> struct type_lists_must_be_identical<tuple<>, tuple<>> {};

// FRANCK GAUTHIER: problems on cygwin only (a numeric constant named B0 is already defined in <sys/termios.h>)
// => replace every occurrence of B0 with _B0, in cache2.h
template <typename A0, typename... A, typename _B0, typename... B>
    struct type_lists_must_be_identical<tuple<A0, A...>, tuple<_B0, B...>>
            : type_lists_must_be_identical<tuple<A...>, tuple<B...>> {
        static_assert(types_must_be_identical<A0, _B0>::value, "INVALID PARAMETER TYPE.");
    };

template <CachingPolicy _Policy, typename Ret, typename... FuncArgs, typename... ErrArgs>
struct make_coll_impl<_Policy, Ret(FuncArgs...), tuple<>, tuple<ErrArgs...>> {
    void operator () (collection<Ret>& coll,
                      Ret (&f) (FuncArgs...),
                      ErrArgs... errargs)
    {
        type_lists_must_be_identical<tuple<FuncArgs...>, tuple<typename ErrArgs::value_type...>>();
        coll.push_back(make_value<_Policy>(f, errargs...));
    }
};

template <CachingPolicy _Policy, typename Ret, typename... FuncArgs>
struct make_coll_impl<_Policy, Ret(FuncArgs...), tuple<>, tuple<clean_value_type<FuncArgs>...>> {
    void operator () (collection<Ret>& coll,
                      Ret (&f) (FuncArgs...),
                      const value<typename clean_type<FuncArgs>::type>&... pargs)
    {
        coll.push_back(make_value<_Policy>(f, pargs...));
    }
};


template <CachingPolicy _Policy, typename Ret, typename... FuncArgs, typename VT, typename... ArgsRemaining, typename... PreviousArgs>
struct make_coll_impl<_Policy, Ret(FuncArgs...), tuple<value<VT>, ArgsRemaining...>, tuple<PreviousArgs...>> {
    void operator () (collection<Ret>& coll,
                      Ret (&f) (FuncArgs...),
                      const value<VT>& car,
                      const ArgsRemaining&... cdr,
                      const PreviousArgs&... pargs)
    {
        make_coll_impl<
			_Policy,
            Ret(FuncArgs...),
            tuple<ArgsRemaining...>,
            tuple<PreviousArgs..., value<VT>>>() (coll, f, cdr..., pargs..., car);
    }
};

template <CachingPolicy _Policy, typename Ret, typename... FuncArgs, typename T, typename... ArgsRemaining, typename... PreviousArgs>
struct make_coll_impl<_Policy, Ret(FuncArgs...), tuple<range<T>, ArgsRemaining...>, tuple<PreviousArgs...>> {
    void operator () (collection<Ret>& coll,
                      Ret (&f) (FuncArgs...),
                      const range<T>& car,
                      const ArgsRemaining&... cdr,
                      const PreviousArgs&... pargs)
    {
        typedef value<T> vtype;
        for (const vtype& v: car) {
            make_coll_impl<
				_Policy,
                Ret(FuncArgs...),
                tuple<ArgsRemaining...>,
                tuple<PreviousArgs..., vtype>>() (coll, f, cdr..., pargs..., v);
        }
    }
};

template <CachingPolicy _Policy, typename Ret, typename... FuncArgs, typename T, typename... ArgsRemaining, typename... PreviousArgs>
struct make_coll_impl<_Policy, Ret(FuncArgs...), tuple<collection<T>, ArgsRemaining...>, tuple<PreviousArgs...>> {
    void operator () (collection<Ret>& coll,
                      Ret (&f) (FuncArgs...),
                      const collection<T>& car,
                      const ArgsRemaining&... cdr,
                      const PreviousArgs&... pargs)
    {
        typedef value<T> vtype;
        for (const vtype& v: car) {
            make_coll_impl<
				_Policy,
                Ret(FuncArgs...),
                tuple<ArgsRemaining...>,
                tuple<PreviousArgs..., vtype>>() (coll, f, cdr..., pargs..., v);
        }
    }
};

template <CachingPolicy _Policy, typename Ret, typename... FuncArgs, typename T, typename... ArgsRemaining, typename... PreviousArgs>
struct make_coll_impl<_Policy, Ret(FuncArgs...), tuple<as_collection<T>, ArgsRemaining...>, tuple<PreviousArgs...>> {
    void operator () (collection<Ret>& coll,
                      Ret (&f) (FuncArgs...),
                      const as_collection<T>& car,
                      const ArgsRemaining&... cdr,
                      const PreviousArgs&... pargs)
    {
        typedef value<typename T::value_type> vtype;
        for (const vtype& v: car) {
            make_coll_impl<
				_Policy,
                Ret(FuncArgs...),
                tuple<ArgsRemaining...>,
                tuple<PreviousArgs..., vtype>>() (coll, f, cdr..., pargs..., v);
        }
    }
};



template <CachingPolicy _Policy = Oneshot, typename Ret, typename... Args, typename... CollArgs>
collection<Ret>
make_collection(Ret (&f) (Args...), CollArgs... args)
{
    collection<Ret> ret;
    make_coll_impl<_Policy, Ret(Args...), tuple<CollArgs...>, tuple<>>() (ret, f, args...);
    return ret;
}



//template <typename T1, typename T2>
//std::ostream& operator << (std::ostream& os, const std::pair<T1, T2>& p) { return os << p.first << ':' << p.second; }
//
//template <typename T1, typename T2>
//std::ostream& operator << (std::ostream& os, const std::map<T1, T2>& m)
//{
//    auto i = m.begin(), j = m.end();
//    os << '{';
//    if (i != j) {
//        os << (*i);
//        for (++i; i != j; ++i) {
//            os << ' ' << (*i);
//        }
//    }
//    return os << '}';
//}



template <typename T>
std::ostream&
operator << (std::ostream& os, const collection<T>& coll)
{
    for (const auto& k: coll) {
        os << '\t' << k << std::endl;
    }
#if 0
    auto i = coll.begin(), j = coll.end();
    if (i == j) {
        return os;
    }
    os << (**i);
    for (++i; i != j; ++i) {
        os << ' ' << (**i);
    }
#endif
    return os;
}

#endif

