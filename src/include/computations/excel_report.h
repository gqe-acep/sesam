#ifndef _SPELL_EXCEL_REPORT_H_
#define _SPELL_EXCEL_REPORT_H_

#include "labelled_matrix.h"
#include "excel.h"
#include "model/print.h"


#define SGNF_0 "sgnf0"
#define SGNF_1 "sgnf1"
#define SGNF_2 "sgnf2"
#define SGNF_3 "sgnf3"
#define SGNF_4 "sgnf4"

namespace excel_report {
    inline
    void
    create_signif_styles(excel::stream& s)
    {
        xlnt::font w, b;
        w.color(xlnt::color::white());
        b.color(xlnt::color::black());
        xlnt::color c0(xlnt::indexed_color(11));
        xlnt::color c1(xlnt::indexed_color(13));
        xlnt::color c2(xlnt::indexed_color(55));
        xlnt::color c3(xlnt::indexed_color(53));
        xlnt::color c4(xlnt::indexed_color(10));
        // Significance codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
        s << excel::style(SGNF_0, [&](xlnt::style& sty) { sty.fill(xlnt::fill().solid(c0)); })
          << excel::style(SGNF_1, [&](xlnt::style& sty) { sty.fill(xlnt::fill().solid(c1)); })
          << excel::style(SGNF_2, [&](xlnt::style& sty) { sty.fill(xlnt::fill().solid(c2)); })
          << excel::style(SGNF_3, [&](xlnt::style& sty) { sty.fill(xlnt::fill().solid(c3)); })
          << excel::style(SGNF_4, [&](xlnt::style& sty) { sty.fill(xlnt::fill().solid(c4)); })
        ;
    }
    
    inline
    std::string
    get_signif_style(double v)
    {
        if (v < .001) { return SGNF_0; }
        if (v < .01) { return SGNF_1; }
        if (v < .05) { return SGNF_2; }
        if (v < .1) { return SGNF_3; }
        return SGNF_4;
    }
}


inline excel::stream& operator << (excel::stream& s, const std::vector<char>& vc) { std::string str(vc.begin(), vc.end()); return s << str; }
inline excel::stream& operator << (excel::stream& s, const model_block_key& mbk) { return s << SPELL_STRING(mbk); }

namespace excel {
template <typename RS, typename RF, typename CS, typename CF, typename M>
manipulator
matrix_with_sections(std::string title, const model_print::matrix_with_sections<RS, RF, CS, CF, M>& mws, std::function<void(int, int, xlnt::cell)> formatter = [] (int, int, xlnt::cell) {})
{
    static auto set_mat_title = excel::merge(2, 2) << /*excel::align([] (xlnt::alignment& al) {
        al.horizontal(xlnt::horizontal_alignment::center).vertical(xlnt::vertical_alignment::center);
    }) << */ excel::cell_format([] (xlnt::format& fmt) {
              xlnt::alignment al;
              al.horizontal(xlnt::horizontal_alignment::center).vertical(xlnt::vertical_alignment::center);
              fmt.alignment(al);
              xlnt::font font;
              font.bold(true).italic(true);
              fmt.font(font);
          })

    << excel::border_right(xlnt::color::black(), xlnt::border_style::thin)
    << excel::border_bottom(xlnt::color::black(), xlnt::border_style::thin)
    ;

    static excel::manipulator vertical_label = excel::align([](xlnt::alignment& al) {
        al.rotation(90).horizontal(xlnt::horizontal_alignment::center).vertical(xlnt::vertical_alignment::center);
    });
    static excel::manipulator horizontal_label = excel::align([](xlnt::alignment& al) {
        al.horizontal(xlnt::horizontal_alignment::center).vertical(xlnt::vertical_alignment::center);
    });
    xlnt::color border_color = xlnt::color::black();
    xlnt::border_style border_style = xlnt::border_style::thin;
    static excel::manipulator BL = excel::border_left(border_color, border_style);
    static excel::manipulator BT = excel::border_top(border_color, border_style);
    static excel::manipulator BR = excel::border_right(border_color, border_style);
    static excel::manipulator BB = excel::border_bottom(border_color, border_style);
    return [&] (stream& s) {
        xlnt::cell_reference origin = s.origin;

        excel::push(s);
        
        /* column section labels */
        s << excel::move(origin.make_offset(2, 0));
        for (const auto& sec: mws.m_column_sections) {
            s << sec.label() << horizontal_label << excel::merge(sec.n_fields(), 1) << BL << BT << excel::next_col;
        }
        s << BL;
        /* column field labels */
        s << excel::move(origin.make_offset(2, 1));
        for (const auto& sec: mws.m_column_sections) {
            s << BL;
            for (const auto& l: sec.field_labels()) {
                s << l << horizontal_label << BB << excel::next_col;
            }
        }
        s << excel::prev_col << BR;

        /* row section labels */
        s << excel::move(origin.make_offset(0, 1));
        for (const auto& sec: mws.m_row_sections) {
            s << excel::next_row << excel::merge(1, sec.n_fields()) << sec.label() << vertical_label << BL << BT;
        }
        s << BB;
        /* row field labels */
        s << excel::move(origin.make_offset(1, 2));
        int rborder = 1;
        for (const auto& sec: mws.m_row_sections) {
            s << BT;
            for (const auto& l: sec.field_labels()) {
                s << l << vertical_label << BR << excel::next_row;
            }
            rborder += sec.n_fields();
            s << excel::push << excel::move(origin.make_offset(1, rborder)) << BB << BR << excel::pop;
        }

        /* data */
        auto& mat = mws.m_matrix;
        int r0 = 0, r1 = 0;
        for (const auto& rs: mws.m_row_sections) {
            r1 += rs.n_fields();
            int c0 = 0, c1 = 0;
            for (const auto& cs: mws.m_column_sections) {
                c1 += cs.n_fields();
                s << excel::move(origin.make_offset(2 + c0, 2 + r0));
                int r, c;
                for (r = r0; r < r1 - 1; ++r) {
                    for (c = c0; c < c1 - 1; ++c) {
//                         MSG_DEBUG('(' << r << ", " << c << ')');
                        s << mat(r, c);
                        formatter(r, c, s.sheet[s.cursor]);
                        s << excel::next_col;
                    }
                    s << mat(r, c);
                    formatter(r, c, s.sheet[s.cursor]);
                    s << BR << excel::next_row;
                }
                for (c = c0; c < c1 - 1; ++c) {
//                     MSG_DEBUG('(' << r << ", " << c << ')');
                    s << mat(r, c) << BB;
                    formatter(r, c, s.sheet[s.cursor]);
                    s << excel::next_col;
//                     s << mat(r, c) << BB << excel::next_col;
                }
                s << mat(r, c) << BB << BR;
                formatter(r, c, s.sheet[s.cursor]);
                s << excel::next_row;
//                 s << mat(r, c) << BB << BR << excel::next_row;
                c0 += cs.n_fields();
            }
            s << excel::next_row;
            r0 += rs.n_fields();
        }
//         for (int r = 0; r < mat.rows(); ++r) {
//             for (int c = 0; c < mat.cols(); ++c) {
//                 s << mat(r, c) << excel::next_col;
//             }
//             s << excel::next_row;
//         }
        
        excel::pop(s);
        s.cursor = s.origin;
        s << title << set_mat_title;
        s.cursor = origin.make_offset(1 + mat.cols(), 1 + mat.rows());
        return s;
    };
}

template <typename A, int R, int C>
manipulator
matrix(const Eigen::Matrix<A, R, C>& mat)
{
    return [&] (stream& s) {
        for (int r = 0; r < mat.rows(); ++r) {
            for (int c = 0; c < mat.cols(); ++c) {
                s << mat(r, c) << excel::next_col;
            }
            s << excel::next_row;
        }
        return s;
    };
}

// inline
// stream&
// autosize(stream& s) {
//     auto dimensions = s.sheet.calculate_dimension();
//     for (xlnt::column_t::index_t c = dimensions.top_left().column_index(); c <= dimensions.top_right().column_index(); ++c) {
//         MSG_DEBUG("Computed width " << s.sheet.column_width(c));
//     }
//     for (xlnt::row_t r = dimensions.top_left().row(); r <= dimensions.bottom_left().row(); ++r) {
//         MSG_DEBUG("Computed height " << s.sheet.row_height(r));
//     }
//     return s;
// }

} /* namespace excel */

#endif
