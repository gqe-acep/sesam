/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_COMPUTATIONS_MODEL_H_
#define _SPEL_COMPUTATIONS_MODEL_H_

#ifndef _SPEL_COMPUTATIONS_BASE_H_
#include "computations/base.h"
#endif

#include "../model.h"

#ifndef _SPEL_COMPUTATIONS_BASIC_DATA_H_
#include "computations/basic_data.h"
#endif

#ifndef _SPEL_COMPUTATIONS_PROBABILITIES_H_
#include "computations/probabilities.h"
#endif

#include "linear_combination.h"

template <typename L>
struct block_builder {
	struct element {
		int first_row;
        int n_rows;
		std::vector<int> columns;
	};
	std::vector<element> elements;
	typedef typename std::vector<element>::const_iterator element_iterator_type;
	std::vector<L> labels;
	int n_rows;
	int n_columns;

	block_builder&
		operator = (block_builder&& bb)
		{
			elements.swap(bb.elements);
			labels.swap(bb.labels);
			n_rows = bb.n_rows;
			n_columns = bb.n_columns;
			return *this;
		}

    template <typename MATRIX>
	struct iterator {
		element_iterator_type ei;
		MATRIX* output;

        /* assemble */
		iterator&
			operator += (const MATRIX& block)
			{
                if (ei->n_rows != block.rows() || ei->columns.size() != ((size_t) block.cols())) {
                    MSG_ERROR("Invalid part size in model block. Have " << MATRIX_SIZE(block) << " but expected (" << ei->n_rows << ", " << ei->columns.size() << ')', "Fix the code");
                    abort();
                }
                /*MSG_DEBUG("output " << output);*/
                /*MSG_DEBUG(output->innerSize() << ',' << output->outerSize());*/
                /*MSG_DEBUG("first_row " << ei->first_row);*/
				/*MSG_DEBUG("block(" << block.innerSize() << ',' << block.outerSize() << ')');*/
				auto slice = output->middleRows(ei->first_row, ei->n_rows);
				/*MSG_DEBUG("slice(" << slice.innerSize() << ',' << slice.outerSize() << ')');*/
				for (int i = 0; i < block.outerSize(); ++i) {
					slice.col(ei->columns[i]) = block.col(i);
				}
				++ei;
				return *this;
			}
	};

    template <typename MATRIX>
	struct disasm_iterator {
		element_iterator_type ei;
		MATRIX* output;

        /* disassemble */
		disasm_iterator&
			operator -= (MATRIX& block)
			{
                /*MSG_DEBUG("output " << output);*/
                /*MSG_DEBUG(output->innerSize() << ',' << output->outerSize());*/
                /*MSG_DEBUG("first_row " << ei->first_row);*/
				/*MSG_DEBUG("block(" << block.innerSize() << ',' << block.outerSize() << ')');*/
				/*MSG_DEBUG("slice(" << slice.innerSize() << ',' << slice.outerSize() << ')');*/
				auto slice = output->middleRows(ei->first_row, ei->n_rows);
                size_t ncol = ei->columns.size();
                block.resize(ei->n_rows, ncol);
				for (size_t i = 0; i < ncol; ++i) {
					block.col(i) = slice.col(ei->columns[i]);
				}
				++ei;
				return *this;
			}
	};

    template <typename MATRIX>
	iterator<MATRIX> begin(MATRIX& output) const
	{
		/*output.resize(n_rows, n_columns);*/
		output = MATRIX::Zero(n_rows, n_columns);
		return {elements.begin(), &output};
	}

    template <typename MATRIX>
	disasm_iterator<MATRIX> disasm_begin(MATRIX& output) const
	{
		return {elements.begin(), &output};
	}
};


static inline
model extend(const model& M, const model_block_key& k, const value<model_block_type>& bloc)
{
    return {const_cast<model*>(&M)->extend(k, bloc)};
}

MatrixXd
traits_to_matrix();

const MatrixXd&
trait_by_name(population_value pop, const std::string& name);

value<MatrixXd>
trait_permutations(const std::string& trait_name, int n);

value<MatrixXd>
trait_matrix(const std::string& trait_name, const collection<population_value>& all_pops);

value<MatrixXd>
residuals_permutations(const model& M, int n);

VectorXd
f_tester(const model& M, const model& M0, const parental_origin_per_locus_type& popl);

model_block_type
cross_indicator(const std::string& trait_name);

value<model_block_type>
make_covariables_block(const std::string& trait, const std::vector<std::string>& covar_names);

model
basic_model();

MatrixXd
f_test_along_chromosome(const value<model>& Mcurrent, const value<model>& M0,
                        const collection<parental_origin_per_locus_type>& popl);

static inline
int max_col(const MatrixXd& mat)
{
    VectorXd norms = mat.colwise().norm();
    int ret;
    norms.maxCoeff(&ret);
    return ret;
}

model
init_model(const value<MatrixXd>& Y, const value<model_block_type>& indicator, double threshold);

VectorXd
row_max(const MatrixXd& mat);

std::vector<double>
get_quantiles(const VectorXd& values, const std::vector<double>& quantiles);


template <typename L>
void
init_connected_block_builder(
		block_builder<L>& ret,
		std::function<int (const qtl_pop_type&)> get_pop_size,
		std::function<std::vector<L> (const qtl_pop_type&)> get_labels,
        const collection<population_value>& all_pops)
{
    std::set<L> uniq;
    std::map<L, int> index;

	for (auto& pop: all_pops) {
		for (const L& l: get_labels(**pop)) {
            uniq.insert(l);
        }
    }
    for (const L& l: uniq) {
        int sz = index.size();
        index.insert({l, sz});
	}
#if 0
//	MSG_DEBUG("index size " << index.size());
	std::stringstream s;
	s << '{';
	for (auto& kv: index) {
		s << " (" << kv.first << ')';
	}
	s << " }";
//	MSG_DEBUG("index: " << s.str());
#endif

	ret.n_rows = 0;
	ret.n_columns = index.size();
	ret.elements.reserve(all_pops.size());
	/*ret.labels.reserve(ret.n_columns);*/
	/*for (auto& kv: index) { ret.labels.push_back(kv.first); }*/
    ret.labels.assign(uniq.begin(), uniq.end());

	for (auto& kv: all_pops) {
		ret.elements.emplace_back();
		auto& e = ret.elements.back();
		const auto& lv = get_labels(**kv);
		e.first_row = ret.n_rows;
        e.n_rows = get_pop_size(**kv);
		e.columns.reserve(lv.size());
		for (const L& l: lv) {
			e.columns.push_back(index[l]);
		}
		ret.n_rows += e.n_rows;
	}
}

template <typename L>
void
init_disconnected_block_builder(
		block_builder<L>& ret,
		std::function<int (const qtl_pop_type&)> get_pop_size,
		std::function<std::vector<L>(const qtl_pop_type&)> get_labels,
        const collection<population_value>& all_pops)
{
    ret.n_rows = 0;
	int col_id = 0;

	ret.n_columns = 0;
	for (auto& kv: all_pops) {
		ret.n_columns += get_labels(**kv).size();
	}
	ret.labels.reserve(ret.n_columns);
	ret.elements.reserve(all_pops.size());

	for (auto& kv: all_pops) {
		ret.elements.emplace_back();
		auto& e = ret.elements.back();
		const auto& lv = get_labels(**kv);
		e.first_row = ret.n_rows;
        e.n_rows = get_pop_size(**kv);
		e.columns.reserve(lv.size());
		for (const L& l: lv) {
			e.columns.push_back(col_id++);
			ret.labels.push_back(l);
		}
		ret.n_rows += e.n_rows;
	}
}


collection<model_block_type>
compute_parental_origins_multipop(const collection<population_value>& all_pops,
                                  const value<chromosome_value>& chr,
                                  const value<locus_key>& lk,
                                  const value<std::vector<double>>& loci,
                                  const std::vector<double>& test_loci);

collection<model_block_type>
compute_dominance_multipop(const collection<population_value>& all_pops,
                           const value<chromosome_value>& chr,
                           const value<locus_key>& lk,
                           const value<std::vector<double>>& loci,
                           const std::vector<double>& test_loci);

collection<model_block_type>
compute_parental_origins_multipop(const collection<population_value>& all_pops,
                                  const value<chromosome_value>& chr,
                                  const std::vector<locus_key>& lk,
                                  const value<std::vector<double>>& loci,
                                  const std::vector<double>& test_loci);

collection<model_block_type>
compute_dominance_multipop(const collection<population_value>& all_pops,
                           const value<chromosome_value>& chr,
                           const std::vector<locus_key>& lk,
                           const value<std::vector<double>>& loci,
                           const std::vector<double>& test_loci);

/*collection<std::vector<char>>*/
/*contrast_groups_connected();*/

MatrixXd
contrast_groups(const collection<population_value>& all_pops, const locus_key& lk);

VectorXd
test_with_permutations(const std::string &trait, int n_permut, size_t y_block_size, chromosome_value chr, const locus_key &lk);


double
qtl_threshold(const std::string& trait_name, chromosome_value chr, const locus_key& lk, double quantile, int n_permut, size_t y_block_size);


double
qtl_threshold_all_chromosomes(const std::string& trait_name, size_t y_block_size, double quantile, int n_permut);


double
qtl_threshold_all_chromosomes_for_model(const std::string& trait_name, double quantile, int n_permut, size_t y_block_size, const model& Mperm);

std::vector<double>
compute_effective_loci(const locus_key& lk, const std::vector<double>& loci);

collection<model_block_type>
assemble_parental_origins_multipop(
        const value<chromosome_value>& chr,
        const value<locus_key>& lk,
        const std::vector<collection<parental_origin_per_locus_type>>& all_popl,
        const collection<population_value>& all_pops,
        bool block_is_POP);

collection<parental_origin_per_locus_type>
disassemble_parental_origins_multipop(
        const chromosome_value& chr,
        const locus_key& lk,
        model_block_type& block,
        const collection<population_value>& all_pops);

struct computation_along_chromosome {
    MatrixXd coefficients;
    MatrixXd residuals;
    VectorXd rank;
    MatrixXd rss;
    MatrixXd ftest_pvalue;
    MatrixXd ftest_lod;
    MatrixXd r2;
    MatrixXd chi2;
    MatrixXd chi2_lod;
    MatrixXd mahalanobis;

    computation_along_chromosome()
        : coefficients(), residuals(), rank(), rss(), ftest_pvalue(), ftest_lod(), r2(), chi2(), chi2_lod(), mahalanobis()
    {}

    friend
        md5_digest& operator << (md5_digest& md5, const computation_along_chromosome&)
        {
            return md5;
        }
};


inline void arg_write(std::ostream&, const computation_along_chromosome*) {}
inline bool arg_match(std::istream&, const computation_along_chromosome*) { return true; }
namespace std { template<> struct hash<computation_along_chromosome*> { bool operator () (const computation_along_chromosome*) const { return 1; } }; }


namespace std {
    template <>
    struct hash<computation_along_chromosome> {
        size_t operator () (const computation_along_chromosome&)
        {
            return 0;
        }
    };

    template <>
        struct hash<ComputationType> : hash<int> {};

    template <>
        struct hash<ComputationResults> : hash<int> {};
}

/*void*/
/*compute_along_chromosome(computation_along_chromosome& ret,*/
                         /*ComputationType ct, ComputationResults cr,*/
                         /*const value<model>& Mcurrent, const value<model>& M0,*/
                         /*const collection<parental_origin_per_locus_type>& popl);*/

int
compute_one(int i, computation_along_chromosome* ret,
            ComputationType ct, ComputationResults cr,
            size_t y_block_size,
            const model& Mcurrent, const model& M0,
            const model_block_key& k,
            const parental_origin_per_locus_type& popl);

template <CachingPolicy _Policy = CachingPolicy::Oneshot>
void
compute_along_chromosome(computation_along_chromosome& ret,
                         ComputationType ct, ComputationResults cr,
                         const value<model>& Mcurrent, const value<model>& M0,
                         size_t y_block_size,
                         const locus_key& base_key,
                         chromosome_value chr,
                         const std::vector<double>& loci,
                         const collection<parental_origin_per_locus_type>& popl)
{
//    MSG_DEBUG("INIT COMPUTATION ct=" << ct << " cr=" << cr);
    if (cr & RSS) {
        ret.rss.resize(Mcurrent->Y().outerSize(), popl.size());
    }
    if (cr & Residuals) {
        ret.residuals.resize(Mcurrent->Y().innerSize(), Mcurrent->Y().outerSize() * popl.size());
    }
    if (cr & Coefficients) {
        ret.residuals.resize(Mcurrent->Y().innerSize(), Mcurrent->Y().outerSize() * popl.size());
    }
    if (cr & Rank) {
        ret.rank.resize(popl.size());
    }
    if (ct & FTest) {
            ret.ftest_pvalue.resize(Mcurrent->Y().outerSize(), popl.size());
    }
    if (ct & FTestLOD) {
            ret.ftest_lod.resize(Mcurrent->Y().outerSize(), popl.size());
    }
    if (ct & Chi2) {
//        MSG_DEBUG("INIT CHI2");
//        MSG_DEBUG("Y.cols = " << Mcurrent->Y().cols() << " y_block_size = " << y_block_size << " popl.size = " << popl.size());
        MSG_QUEUE_FLUSH();
        ret.chi2.resize(Mcurrent->Y().cols() / y_block_size, popl.size());
    }
    if (ct & Chi2LOD) {
//        MSG_DEBUG("INIT CHI2 LOD");
//        MSG_DEBUG("Y.cols = " << Mcurrent->Y().cols() << " y_block_size = " << y_block_size << " popl.size = " << popl.size());
        MSG_QUEUE_FLUSH();
        ret.chi2_lod.resize(Mcurrent->Y().cols() / y_block_size, popl.size());
    }
    if (ct & Mahalanobis) {
            ret.mahalanobis.resize(popl.size(), 1);
    }
    if (ct & R2) {
            ret.r2.resize(Mcurrent->Y().outerSize(), popl.size());
    }

    value<ComputationType> vct = ct;
    value<ComputationResults> vcr = cr;
    value<computation_along_chromosome*> pcac = &ret;

    /*DUMP_FILE_LINE();*/
    collection<int> joiner;
    joiner.reserve(popl.size());

    /* FIXME: create correct keys? FIXED */
    for (auto i: range<int>(0, popl.size(), 1)) {
        /*DUMP_FILE_LINE();*/
        /*value<model_block_key> vmbk = model_block_key(base_key);*/
        /**vmbk += std::make_pair(chr, loci[*i]);*/
        /*(*vmbk)->loci = (*vmbk)->loci + loci[*i];*/
        value<model_block_key> vmbk = model_block_key_struc::pop(chr, base_key + loci[*i]);
        /*MSG_DEBUG("new block key " << vmbk);*/
        joiner.push_back(make_value<_Policy>(compute_one,
                                             i, pcac, /* flower */
                                             vct, vcr,
                                             as_value(y_block_size),
                                             Mcurrent, M0,
                                             vmbk,
                                             /*as_value(base_key + std::make_pair(chr, loci[i])),*/
                                             popl[i]));
    }

    for (auto& k: joiner) {
        /*DUMP_FILE_LINE();*/
        (void) *k;  /* join them all */
    }
}


inline
void
init_computation(computation_along_chromosome& ret, ComputationType ct, ComputationResults cr, size_t n_steps, size_t Y_rows, size_t Y_cols)
{
    if (cr & RSS) {
        ret.rss = MatrixXd::Zero(Y_cols, n_steps);
    }
    if (cr & Residuals) {
        ret.residuals = MatrixXd::Zero(Y_rows, Y_cols * n_steps);
    }
    if (cr & Coefficients) {
        ret.residuals = MatrixXd::Zero(Y_rows, Y_cols * n_steps);
    }
    if (cr & Rank) {
        ret.rank = VectorXd::Zero(n_steps);
    }
    if (ct & FTest) {
            ret.ftest_pvalue = MatrixXd::Zero(Y_cols, n_steps);
    }
    if (ct & FTestLOD) {
            ret.ftest_lod = MatrixXd::Zero(Y_cols, n_steps);
    }
    if (ct & Chi2) {
        ret.chi2 = MatrixXd::Zero(1, n_steps);
    }
    if (ct & Chi2LOD) {
        ret.chi2_lod = MatrixXd::Zero(1, n_steps);
    }
    if (ct & Mahalanobis) {
            ret.mahalanobis = MatrixXd::Zero(n_steps, 1);
    }
    if (ct & R2) {
            ret.r2 = MatrixXd::Zero(Y_cols, n_steps);
    }

}

template <CachingPolicy _Policy = CachingPolicy::Oneshot>
void
compute_along_interval(int i0, computation_along_chromosome& ret,
                       value<ComputationType> vct, value<ComputationResults> vcr,
                       size_t y_block_cols,
                       const value<model>& Mcurrent, const value<model>& M0,
                       const locus_key& base_key,
                       chromosome_value chr,
                       const std::vector<double>& loci,
                       const collection<parental_origin_per_locus_type>& popl)
{
    value<computation_along_chromosome*> pcac = &ret;

    /*DUMP_FILE_LINE();*/
    collection<int> joiner;
    joiner.reserve(popl.size());

    /* FIXME: create correct keys? FIXED */
    for (auto i: range<int>(i0, i0 + popl.size(), 1)) {
        /*DUMP_FILE_LINE();*/
        /*value<model_block_key> vmbk = model_block_key(base_key);*/
        /**vmbk += std::make_pair(chr, loci[*i]);*/
        value<model_block_key> vmbk = model_block_key_struc::pop(chr, base_key + loci[*i]);
        /*MSG_DEBUG("new block key " << vmbk);*/
        joiner.push_back(make_value<_Policy>(compute_one,
                                             i, pcac, /* flower */
                                             vct, vcr,
                                             as_value(y_block_cols),
                                             Mcurrent, M0,
                                             vmbk,
                /*as_value(base_key + std::make_pair(chr, loci[i])),*/
                                             popl[i]));
    }

    for (auto& k: joiner) {
//        DUMP_FILE_LINE();
        (void) *k;  /* join them all */
//        MSG_DEBUG("Block" << std::endl << (*k));
//        MSG_QUEUE_FLUSH();
    }
}


template <CachingPolicy _Policy = CachingPolicy::Oneshot>
void
compute_along_interval(int i0, computation_along_chromosome& ret,
                       value<ComputationType> vct, value<ComputationResults> vcr,
                       size_t y_block_cols,
                       const value<model>& Mcurrent, const value<model>& M0,
                       const std::vector<locus_key>& base_key,
                       chromosome_value chr,
                       const std::vector<double>& loci,
                       const collection<parental_origin_per_locus_type>& popl)
{
    value<computation_along_chromosome*> pcac = &ret;

    /*DUMP_FILE_LINE();*/
    collection<int> joiner;
    joiner.reserve(popl.size());

    /* FIXME: create correct keys? FIXED */
    for (auto i: range<int>(i0, i0 + popl.size(), 1)) {
        /*DUMP_FILE_LINE();*/
        /*value<model_block_key> vmbk = model_block_key(base_key);*/
        /**vmbk += std::make_pair(chr, loci[*i]);*/
        value<model_block_key> vmbk = model_block_key_struc::pop(chr, base_key[*i] + loci[*i]);
        /*MSG_DEBUG("new block key " << vmbk);*/
        joiner.push_back(make_value<_Policy>(compute_one,
                                             i, pcac, /* flower */
                                             vct, vcr,
                                             as_value(y_block_cols),
                                             Mcurrent, M0,
                                             vmbk,
                /*as_value(base_key + std::make_pair(chr, loci[i])),*/
                                             popl[i]));
    }

    for (auto& k: joiner) {
//        DUMP_FILE_LINE();
        (void) *k;  /* join them all */
//        MSG_DEBUG("Block" << std::endl << (*k));
//        MSG_QUEUE_FLUSH();
    }
}


collection<model_block_type>
compute_parental_origins_multipop_extension(
        const collection<population_value>& all_pop,
        const value<qtl_chromosome_value>& qtl_chr,
        const value<std::vector<double>>& loci,
        const value<model_block_type>& source,
        int sz);

#endif


