/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_COMPUTATIONS_TO_MD5_H_
#define _SPEL_COMPUTATIONS_TO_MD5_H_

static inline
md5_digest& operator << (md5_digest& md5, const label_type& aa)
{
    return md5.update(aa.first()).update(aa.second());
}


static inline
md5_digest& operator << (md5_digest& md5, const chromosome& chr)
{
    return md5 << chr.name << chr.raw.marker_name.size();
}

static inline
md5_digest& operator << (md5_digest& md5, const qtl_chromosome& chr)
{
    return md5 << (*chr.chr) << chr.qtl.size();
}

static inline
md5_digest& operator << (md5_digest& md5, const qtl_pop_type& pop)
{
    /*return md5 << pop.qtl_generation_name << pop.observed_traits_filename << pop.observed_mark.size() << pop.noise;*/
    MSG_DEBUG("QTL_POP MD5 using pop_name='" << pop.name << "' and qtl_gen_name='" << pop.qtl_generation_name << "'");
    return md5 << pop.name << pop.qtl_generation_name;
}

static inline
md5_digest& operator << (md5_digest& md5, const geno_matrix& gen)
{
    return md5 << gen.name;
}


static inline
md5_digest& operator << (md5_digest& md5, const context_key& ck)
{
    return md5 << ck->pop->name << ck->pop->gen->name << ck->loci;
}


/*static inline*/
/*md5_digest& operator << (md5_digest& md5, const model& m)*/
/*{*/
    /*return md5 << m.keys();*/
/*}*/

#if 0
static inline
md5_digest& operator << (md5_digest& md5, const impl::generation_rs::segment_computer_t& sc)
{
    return md5 << sc.g_this << sc.steps << sc.noise;
}
#endif

#endif

