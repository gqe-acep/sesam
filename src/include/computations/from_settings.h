/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_COMPUTATIONS_FROM_SETTINGS_H_
#define _SPEL_COMPUTATIONS_FROM_SETTINGS_H_

typedef cache::value<const ::generation_rs*> generation_value;
typedef cache::value<const ::population*> population_value;
typedef cache::value<const ::chromosome*> chromosome_value;
typedef cache::value<const ::qtl_chromosome*> qtl_chromosome_value;



extern settings_t* active_settings;

static inline
::chromosome* _find_chromosome(const std::string& name)
{
    for (auto& chr: active_settings->map) {
        if (chr.name == name) {
            return &chr;
        }
    }
    return NULL;
}

static inline
::qtl_chromosome* _find_qtl_chromosome(const std::string& name)
{
    for (auto& chr: active_settings->working_set) {
        if (chr.chr->name == name) {
            return &chr;
        }
    }
    return NULL;
}

template <typename CACHE_DIRECTION>
cache_file<CACHE_DIRECTION>& operator & (cache_file<CACHE_DIRECTION>& cf, allele_pair& ap)
{
    return cf & ap.first & ap.second;
}

static inline
cache_input& operator & (cache_input& ci, const generation_rs*& g)
{
    std::string s;
    ci & s;
    g = active_settings->design->generation[s];
    return ci;
}

static inline
cache_input& operator & (cache_input& ci, const impl::generation_rs*& g)
{
    std::string s;
    ci & s;
    g = active_settings->design->generation[s];
    return ci;
}

static inline
cache_output& operator & (cache_output& ci, const generation_rs*& g)
{
    return ci & g->name;
}

static inline
cache_output& operator & (cache_output& ci, const impl::generation_rs*& g)
{
    return ci & g->name;
}

static inline
cache_input& operator & (cache_input& ci, const ::qtl_chromosome*& chr)
{
    std::string s;
    ci & s;
    chr = _find_qtl_chromosome(s);
    return ci;
}

static inline
cache_output& operator & (cache_output& co, const ::qtl_chromosome*& chr)
{
    return co & chr->chr->name;
}

static inline
cache_input& operator & (cache_input& ci, const ::chromosome*& chr)
{
    std::string s;
    ci & s;
    chr = _find_chromosome(s);
    return ci;
}

static inline
cache_output& operator & (cache_output& co, const ::chromosome*& chr)
{
    return co & chr->name;
}


static inline
cache_input& operator & (cache_input& ci, const ::population*& pop)
{
    std::string pname;
    ci & pname;
    pop = &active_settings->populations[pname];
    return ci;
}


static inline
cache_output& operator & (cache_output& co, const ::population*& pop)
{
    return co & pop->name;
}

static inline
cache_output& operator & (cache_output& ci, impl::generation_rs::segment_computer_t& sc)
{
    return ci & sc.g_this & sc.steps & sc.noise & sc.chr;
}
static inline
cache_input& operator & (cache_input& ci, impl::generation_rs::segment_computer_t& sc)
{
    std::string chr_name;
    ci & sc.g_this & sc.steps & sc.noise & sc.chr;
    sc.init();
    return ci;
}

template <typename CD, typename V>
cache_file<CD>& operator & (cache_file<CD>& cf, cache::value<V>& v) { return v.file_io(cf); }


namespace computations {

struct generation : value<const generation_rs*> {
    generation() : value<const generation_rs*>() {}
    generation(const generation& g) : value<const generation_rs*>(g) {}
    generation(generation&& g) : value<const generation_rs*>(g) {}
    generation(const value<std::string>& name)
        : value<const generation_rs*>{active_settings->design->generation[name]}
    {}
    generation(const std::tuple<value<std::string>>& name)
        : value<const generation_rs*>{active_settings->design->generation[std::get<0>(name)]}
    {}
};
}

namespace std {
    template <>
        struct hash<computations::generation> {
            size_t operator () (const computations::generation& g) const
            {
                return hash<void*>()(reinterpret_cast<void*>(const_cast<generation_rs*>(g.v())));
            }
        };
}




namespace computations {
struct chromosome : chromosome_value {
    chromosome(const std::string& name)
        : chromosome_value{_find_chromosome(name)}
    {}

    chromosome(const ::chromosome* chr)
        : chromosome_value{chr}
    {}
};

struct qtl_chromosome : qtl_chromosome_value {
    qtl_chromosome(const std::string& name)
        : qtl_chromosome_value{_find_qtl_chromosome(name)}
    {}

    qtl_chromosome(const ::qtl_chromosome* chr)
        : qtl_chromosome_value{chr}
    {}
};


struct selected_qtls : value<std::vector<std::pair<const ::chromosome*, double>>> {
    selected_qtls()
        : value<std::vector<std::pair<const ::chromosome*, double>>>()
    {
        for (auto& qc: active_settings->working_set) {
            for (auto& l: qc.qtl) {
                v().push_back({qc.chr, l});
            }
        }
    }

    static const bool composite = false;
};

struct selected_qtls_on_chromosome : value<std::vector<double>> {
    selected_qtls_on_chromosome(const qtl_chromosome_value& qc)
        : value<std::vector<double>>(qc.v()->qtl)
    {}

    static const bool composite = false;
};


struct population : population_value {
    population(const value<std::string>& name)
        : population_value{&active_settings->populations[name]}
    {}
};


struct qtl_generation : generation {
    qtl_generation(const population_value& pop)
        : generation(value<std::string>{pop.v()->qtl_generation_name})
    {}
    qtl_generation(const population& pop)
        : generation(value<std::string>{pop.v()->qtl_generation_name})
    {}
};


struct all_observed_generation_names : all_keys<decltype(::population::observed_mark)> {
    all_observed_generation_names(const population_value& pop)
        : all_keys<decltype(::population::observed_mark)>(pop.v()->observed_mark)
    {}
    all_observed_generation_names(const population& pop)
        : all_keys<decltype(::population::observed_mark)>(pop.v()->observed_mark)
    {}
    using all_keys<decltype(::population::observed_mark)>::size;
    using all_keys<decltype(::population::observed_mark)>::operator [];
};

struct marker_observations : value<std::vector<char>> {
    marker_observations(const population_value& pop, const chromosome_value& chr, const value<std::string>& gen, const value<int>& num)
        : value<std::vector<char>>{
                pop.v()->get_observed_mark(gen)
                         .observations
                         .get_obs(chr.v()->marker_name.begin(), chr.v()->marker_name.end(), num)}
    {}
    marker_observations(const population& pop, const chromosome& chr, const value<std::string>& gen, const value<int>& num)
        : value<std::vector<char>>{
                pop.v()->get_observed_mark(gen)
                         .observations
                         .get_obs(chr.v()->marker_name.begin(), chr.v()->marker_name.end(), num)}
    {}
};

struct test_loci : value<std::vector<double>> {
    test_loci(const chromosome& chr)
        : value<std::vector<double>>{compute_steps(chr.v()->marker_locus, active_settings->step)}
    {}
};


struct genoprob_computer : value<generation_rs::segment_computer_t> {
    genoprob_computer(const population& pop, const generation& gen, const qtl_chromosome& chr)
        : value<generation_rs::segment_computer_t>{
            gen.v()->segment_computer(chr.v(), active_settings->step, pop.v()->noise)}
    {}
};


} /* namespace computations */

static inline
md5_digest& operator << (md5_digest& md5, const computations::genoprob_computer& gc)
{
    return md5 << gc.v().g_this->name << gc.v().steps << gc.v().noise;
}

namespace computations {

struct marker_observation_spec : value<::marker_observation_spec> {
    marker_observation_spec(const generation& gen)
        : value<::marker_observation_spec>{
                active_settings->marker_observation_specs
                               ->map[gen.v()->name]}
    {}
};



} /* namespace computations */

#endif

