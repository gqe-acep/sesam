/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_COMPUTATIONS_BASIC_DATA_H_
#define _SPEL_COMPUTATIONS_BASIC_DATA_H_

#ifndef _SPEL_COMPUTATIONS_BASE_H_
#include "base.h"
#endif

typedef const geno_matrix* generation_value;
typedef const qtl_pop_type* population_value;
typedef const chromosome* chromosome_value;
typedef const qtl_chromosome* qtl_chromosome_value;

typedef std::vector<double> selected_qtls_on_chromosome_type;
typedef std::map<char, VectorXb> observation_vectors_type;
typedef labelled_matrix<MatrixXd, label_type, double> locus_probabilities_type;
typedef labelled_matrix<MatrixXd, label_type, int> geno_prob_type;
typedef labelled_matrix<MatrixXd, std::vector<char>, label_type> state_to_parental_origin_matrix_type;
typedef labelled_matrix<MatrixXd, std::vector<char>, double> parental_origin_type;
typedef labelled_matrix<MatrixXd, int, std::vector<char>> parental_origin_per_locus_type;
typedef Eigen::Matrix<bool, 1, Eigen::Dynamic> bool_row_type;

struct compare_row {
    bool operator () (const bool_row_type& r1, const bool_row_type& r2) const
    {
        for (int i = 0; i < r1.innerSize(); ++i) {
            if (r1(i) > r2(i)) {
                return true;
            } else if (r1(i) < r2(i)) {
                return false;
            }
        }
        return false;
    }
};

typedef std::set<bool_row_type, compare_row> bool_row_set;

std::vector<std::pair<const chromosome*, double>>
selected_qtls();

std::vector<double>
selected_qtls_on_chromosome(qtl_chromosome_value);

generation_value
generation_by_name(const population_value&, const std::string&, int);

generation_value
qtl_generation(const population_value&, int);

/*generation_value*/
/*generation_by_name(const std::string&);*/

/*generation_value*/
/*qtl_generation(const population_value&);*/

population_value
population_by_name(const std::string&);

collection<population_value>
all_populations();

chromosome_value
chromosome_by_name(const std::string&);

qtl_chromosome_value
qtl_chromosome_by_name(const std::string&);

collection<std::string>
all_observed_generation_names(population_value);

/*std::vector<char>*/
/*marker_observations(population_value, chromosome_value, generation_value, int);*/

/*collection<double>*/
value<std::vector<double>>
test_loci(chromosome_value);

/*generation_rs::segment_computer_t*/
/*genoprob_computer(population_value, generation_value, qtl_chromosome_value);*/

/*marker_observation_spec*/
/*marker_observation_specifications(generation_value);*/

labelled_matrix<MatrixXd, std::vector<char>, label_type>
compute_state_to_parental_origin(const context_key& ck, const locus_key& lk);

labelled_matrix<MatrixXd, std::vector<char>, label_type>
compute_state_to_dominance(const context_key& ck, const locus_key& lk);


MatrixXb
get_contrast_groups(generation_value gen, const locus_key& lk);

/*observation_vectors_type*/
/*observation_vectors(const generation_rs*);*/

/*pedigree_type*/
/*pedigree(population_value, generation_value, int);*/

range<int>
individual_range(population_value);

/*observation_vectors_type*/
/*get_observation_vectors(generation_value gen);*/

#if 0
struct pop_mgo_data {
    generation_value qtl_gen;
    collection<std::string> aogn;
    collection<generation_value> aog;
    /*collection<observation_vectors_type> aov;*/
    pop_mgo_data(population_value pop)
        : qtl_gen(generation_by_name(pop->qtl_generation_name))
        , aogn(all_observed_generation_names(pop))
        , aog(make_collection<Sync>(generation_by_name, aogn))
        /*, aov(make_collection<Sync>(get_observation_vectors, aog))*/
    {
        /*MSG_DEBUG("new pop_mgo_data");*/
    }
};
#endif

struct stpom_data {
    std::vector<std::vector<char>> row_labels;
    std::vector<label_type> col_labels;
    MatrixXb haplo1;
    MatrixXb haplo2;

    friend
        std::ostream&
        operator << (std::ostream& os, const stpom_data& sd)
        {
            os << "Row labels: " << sd.row_labels << std::endl;
            os << "Col labels: " << sd.col_labels << std::endl;
            os << "Haplo1: " << std::endl << sd.haplo1 << std::endl;
            os << "Haplo2: " << std::endl << sd.haplo2 << std::endl;
            return os;
        }
};

value<stpom_data>
get_stpom_data(const context_key& ck, const locus_key& lk);

template <typename CACHE_DIRECTION>
CACHE_DIRECTION& operator & (CACHE_DIRECTION& cd, stpom_data& sd)
{
    return cd & sd.row_labels & sd.col_labels & sd.haplo1 & sd.haplo2;
}

static inline md5_digest& operator << (md5_digest& md5, const stpom_data& sd)
{
    return md5 << sd.haplo1 << sd.haplo2;
}

/*static inline md5_digest& operator << (md5_digest& md5, const pop_mgo_data&)*/
/*{*/
    /*return md5;*/
/*}*/

MatrixXd
population_marker_obs(const context_key&, int);

collection<std::string>
all_traits();

#if 0
static inline
cache_input& operator & (cache_input& ci, const qtl_pop_type*& pop)
{
    std::string name;
    ci & name;
    pop = population_by_name(name);
    return ci;
}


static inline
cache_output& operator & (cache_output& co, const qtl_pop_type*& pop)
{
    return co & pop->name;
}
#endif // 0

#endif

