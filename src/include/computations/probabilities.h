/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_COMPUTATIONS_PROBABILITIES_H_
#define _SPEL_COMPUTATIONS_PROBABILITIES_H_

#ifndef _SPEL_COMPUTATIONS_BASE_H_
#include "base.h"
#endif

#ifndef _SPEL_COMPUTATIONS_BASIC_DATA_H_
#include "basic_data.h"
#endif

/*locus_probabilities_type*/
/*locus_probabilities(qtl_chromosome_value, population_value,*/
                    /*const multi_generation_observations&,*/
                    /*const selected_qtls_on_chromosome_type&);*/

MatrixXd permutation_matrix(size_t n_parents, size_t n_loci, size_t order);

/*locus_probabilities_type*/
/*locus_probabilities(const context_key&, const locus_key&,*/
                    /*const multi_generation_observations&,*/
                    /*const std::vector<double>&);*/

state_to_parental_origin_matrix_type
state_to_parental_origin_matrix(generation_value, int);

parental_origin_type
parental_origin(const locus_probabilities_type&, generation_value, const selected_qtls_on_chromosome_type&);

parental_origin_per_locus_type
joint_parental_origin_at_locus(const context_key& ck, const locus_key& lk);

parental_origin_per_locus_type
joint_dominance_at_locus(const context_key& ck, const locus_key& lk, bool);

collection<parental_origin_per_locus_type>
parental_origin_per_locus(const collection<parental_origin_type>&);

/*
collection<parental_origin_per_locus_type>
compute_parental_origins(const value<population_value>& pop,
                         const value<chromosome_value>& chr,
                         const value<std::vector<double>>& loci);
*/

#endif

