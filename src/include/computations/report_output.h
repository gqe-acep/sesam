/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// #ifndef _SPEL_REPORT_OUTPUT_H_
// #define _SPEL_REPORT_OUTPUT_H_

#ifndef _SPEL_REPORT_OUTPUT_H_
#define _SPEL_REPORT_OUTPUT_H_

#include "settings.h"
#include "../model/model.h"
#include "excel_report.h"
#include <memory>


extern "C" {
#   include <libgen.h>
}

typedef std::map<std::string, std::map<double, std::string>> point_of_interest_type;
typedef std::map<std::string, std::map<double, std::pair<double, double>>> region_of_interest_type;

struct test_result;

struct significance_legend_type {
    std::vector<double> thresholds;
    std::vector<std::string> labels;
};

inline
const significance_legend_type&
significance_legend()
{
//     static std::string _ = "Significance codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1";
    static significance_legend_type _ = {
        {0, 0.001, 0.01, 0.05, 0.1, 1},
        {"***", "**", "*", ".", " ", "", ""}
    };
    return _;
}


struct qtl_description {
    std::string chromosome;
    double position;
    double inner_interval_start;
    double inner_interval_end;
    double outer_interval_start;
    double outer_interval_end;
};


struct contrast_description {
//     std::string chromosome;
//     double qtl_locus;
    std::string key;
    std::string contrast_group;
    std::string contrast_pair;
    double value;
    double significance_value;
    int significance_level;
};


typedef model_print::matrix_with_sections<std::string, void, std::string, std::vector<char>> mws_result;
typedef model_print::matrix_with_sections<void, std::vector<char>, model_block_key, std::vector<char>> mws_contrast;
typedef model_print::matrix_with_sections<model_block_key, std::vector<char>, model_block_key, std::vector<char>> mws_model_xtx;
typedef model_print::matrix_with_sections<std::string, void, model_block_key, std::vector<char>> mws_result_by_block;
enum ReportMode { Main=0, AlgoPhase, AlgoSel, FullMap, Trait, LOD, Model, Legend };


inline
void
ensure_path_exists(std::string path)
{
    char* buf = strdup(path.c_str());
    char* dir = dirname(buf);
    ensure_directories_exist(dir);
    free((void*) buf);            
}

inline
matrix_with_sections<std::string, void, std::string, std::string, Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic>>
qtl_descr_to_mat(const std::vector<qtl_description>& descr)
{
    Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic> mat(descr.size(), 6);
    int r = 0;
    for (const auto& d: descr) {
        mat(r, 0) = d.chromosome;
        mat(r, 1) = SPELL_STRING(d.position);
        mat(r, 2) = SPELL_STRING(d.inner_interval_start);
        mat(r, 3) = SPELL_STRING(d.inner_interval_end);
        mat(r, 4) = SPELL_STRING(d.outer_interval_start);
        mat(r, 5) = SPELL_STRING(d.outer_interval_end);
        ++r;
    }
//         MSG_DEBUG("QTL REPORT" << std::endl << mat);
    matrix_with_sections<std::string, void, std::string, std::string, Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic>> mws(mat);
    mws.add_column_section("Chromosome", std::vector<std::string>{"Name"});
    mws.add_column_section("Locus", std::vector<std::string>{"(cM)"});
    mws.add_column_section("Inner confidence interval", std::vector<std::string>{"start", "end"});
    mws.add_column_section("Outer confidence interval", std::vector<std::string>{"start", "end"});
    mws.add_row_section("", descr.size());
//         MSG_DEBUG("QTL REPORT with headers" << std::endl << mws);
    return mws;
}


struct report_stream_base {
    report_stream_base() : m_enabled(true), m_trait_name("undefined") {}

    const std::string trait_name() { return m_trait_name; }
    
    virtual ~report_stream_base() {}

    virtual bool check_open() const = 0;
    virtual void init_selection_report_impl(const std::vector<std::string>& headers) = 0;
    virtual void line_impl(std::string line) = 0;
    virtual void line_impl() = 0;
    virtual void line_impl(const std::vector<std::string>&) = 0;
    virtual void selection_impl(int index, std::string chrom, std::string line, double score, double locus, double threshold) = 0;
    virtual void algo_phase_impl(std::string descr) = 0;
    virtual void select_book_impl(std::string name, bool exists) = 0;
    virtual void select_sheet_impl(std::string name, bool exists) = 0;
    virtual void header_impl(std::string text) = 0;
    virtual void matrix_impl(const MatrixXd& mat, const std::vector<std::string> colnames) = 0;
    virtual void matrix_impl(const Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic>& mat) = 0;
    virtual void matrix_impl(std::string title, const mws_model_xtx& mat) = 0;
    virtual void matrix_impl(std::string title, const mws_result& mat) = 0;
    virtual void matrix_impl(std::string title, const mws_result_by_block& mat) = 0;
    virtual void matrix_impl(std::string title, const mws_contrast& mat, const MatrixXi& styles) = 0;
    virtual void legend_impl() = 0;
    virtual void qtl_impl(const std::vector<qtl_description>&) = 0;
    virtual void full_map_impl(region_of_interest_type&, point_of_interest_type&) = 0;
    virtual void contrasts_impl(const std::vector<contrast_description>&) = 0;

    void init_selection_report(const std::vector<std::string>& headers) { check_open(); init_selection_report_impl(headers); }
    void line(const std::vector<std::string>& line) { check_open(); line_impl(line); }
    void line(std::string line) { check_open(); line_impl(line); }
    void line() { check_open(); line_impl(); }
    void selection(int index, std::string chrom, std::string line, double score, double locus, double threshold) { check_open(); selection_impl(index, chrom, line, score, locus, threshold); }
    void algo_phase(const std::string descr) { check_open(); algo_phase_impl(descr); }
    void select_book(std::string name, bool exists) { select_book_impl(name, exists); }
    void select_sheet(std::string name, bool exists) { select_sheet_impl(name, exists); }
    void header(std::string text) { check_open(); header_impl(text); }
    void matrix(const Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic>& mat) { check_open(); matrix_impl(mat); }
    void matrix(const MatrixXd& mat, const std::vector<std::string> colnames={}) { check_open(); matrix_impl(mat, colnames); }
    void matrix(std::string title, const mws_result& mat) { check_open(); matrix_impl(title, mat); }
    void matrix(std::string title, const mws_result_by_block& mat) { check_open(); matrix_impl(title, mat); }
    void matrix(std::string title, const mws_model_xtx& mat) { check_open(); matrix_impl(title, mat); }
    void matrix(std::string title, const mws_contrast& mat, const MatrixXi& styles) { check_open(); matrix_impl(title, mat, styles); }
    void legend() { check_open(); legend_impl(); }
    void qtl(const std::vector<qtl_description>& descr) { check_open(); qtl_impl(descr); }
    void full_map(region_of_interest_type& roi, point_of_interest_type& poi) { check_open(); full_map_impl(roi, poi); }
    void contrasts(const std::vector<contrast_description>& c) { check_open(); contrasts_impl(c); }

    void mode(ReportMode rm) { m_enabled = behaviour(rm); }
    void trait(std::string tn) { m_trait_name = tn; }

protected:
    virtual bool behaviour(ReportMode) = 0;

private:
    bool m_enabled;
    std::string m_trait_name;
};


inline
std::string
make_report_path(const std::string& trait_name, const std::string& type, const std::string& book)
{
    return SPELL_STRING(active_settings->work_directory << '/' << active_settings->name << ".report/" << type << '/' << trait_name << '/' << book);
}

inline
std::string
make_report_path(const std::string& trait_name, const std::string& type, const std::string& book, const std::string& sheet, const std::string& ext)
{
    return SPELL_STRING(make_report_path(trait_name, type, book) << '/' << sheet << '.' << ext);
}

inline
std::string
make_report_path(const std::string& trait_name, const std::string& type, const std::string& book, const std::string& sheet, const std::string& title, const std::string& ext)
{
    return SPELL_STRING(make_report_path(trait_name, type, book) << '/' << sheet << '/' << title << '.' << ext);
}

inline
std::string
make_report_path(const std::string& trait_name, const std::string& type, const std::string& book, const std::string& ext)
{
    return SPELL_STRING(make_report_path(trait_name, type, book) << '.' << ext);
}


struct report_stream_filesystem : public report_stream_base {
    report_stream_filesystem(std::string type, std::string ext) : m_type(type), m_ext(ext) {}

    void select_book_impl(std::string name, bool exists) override
    {
        close();
        m_book = name;
    }

    void select_sheet_impl(std::string name, bool exists) override
    {
        close();
        m_sheet = name;
        ensure_directories_exist(make_report_path(trait_name(), m_type, m_book));
        open(make_report_path(trait_name(), m_type, m_book, m_sheet, m_ext), exists);
    }

    bool check_open() const override
    {
        if (!m_file.is_open()) {
            MSG_ERROR("Attempting to write to an unopen file (book=" << m_book << " and sheet=" << m_sheet << ')', "");
            return false;
        }
        return true;
    }

protected:
    static Eigen::IOFormat model_format() {
//         static Eigen::IOFormat f(Eigen::FullPrecision, Eigen::DontAlignCols, "\t", "\n", "", "", "", "");
        static Eigen::IOFormat f(Eigen::FullPrecision, 0, " ", "\n", "", "", "", "");
        return f;
    }

    void close() { if (m_file.is_open()) { m_file.close(); } }
    void open(const std::string& path, bool exists)
    {
//         MSG_DEBUG("[REPORT] Opening " << path << " exists? " << std::boolalpha << exists);
        m_full_path = path; m_file.open(m_full_path, exists ? std::ofstream::out | std::ofstream::app : std::ofstream::out);
        
    }

    std::ofstream m_file;

private:
    std::string m_type, m_ext;
    std::string m_book;
    std::string m_sheet;
    std::string m_full_path;
};



struct report_stream_text : public report_stream_filesystem {
    report_stream_text() : report_stream_filesystem("text", "txt"), m_selections() {}
    
    ~report_stream_text()
    {
        if (m_selections.size()) {
            Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic> mat(m_selections.size(), m_selections.front().size());
            int i = -1, j;
            for (const auto& row: m_selections) {
                ++i;
                j = 0;
                for (auto cell = row.begin(), end = row.end(); cell != end; ++cell, ++j) {
                    mat(i, j) = *cell;
                }
            }
            select_book("algo_trace", false);
            select_sheet("Computations", false);
            matrix_impl(mat);
        }
    }

    void init_selection_report_impl(const std::vector<std::string>& headers) override
    {
        if (m_selections.size() == 0) {
            m_selections.push_back(headers);
        }
    }
    
    void algo_phase_impl(std::string descr) override
    {
        m_selections.emplace_back(std::vector<std::string>{"", "", descr});
    }

    void selection_impl(int index, std::string chrom, std::string line, double score, double locus, double threshold) override
    {
//         check_open();
//         m_file << std::setw(5) << std::right << index << std::setw(10) << std::right << chrom << "   " << line << std::endl;
//         MSG_DEBUG("index " << index);
//         MSG_DEBUG("chrom " << chrom);
//         MSG_DEBUG("line " << line);
//         MSG_DEBUG("score " << score);
//         MSG_DEBUG("locus " << locus);
//         MSG_DEBUG("threshold " << threshold);
        m_selections.emplace_back(std::vector<std::string>{SPELL_STRING(index), chrom, line, SPELL_STRING(score), SPELL_STRING(locus), SPELL_STRING(threshold)});
    }

    bool behaviour(ReportMode) override { return true; }

    void line_impl(const std::vector<std::string>& line) override
    {
        check_open();
        auto i = line.begin(), j = line.end();
        if (i != j) {
            m_file << (*i++);
            while (i != j) {
                m_file << '\t' << (*i++);
            }
        }
        m_file << std::endl;
    }
    void line_impl(std::string line) override
    {
        check_open();
        m_file << line << std::endl;
    }
    void line_impl() override
    {
        check_open();
        m_file << std::endl;
    }
    void header_impl(std::string text) override
    {
        check_open();
        m_file
        << "=================================================================================================================" << std::endl
        << ' ' << text << std::endl
        << "-----------------------------------------------------------------------------------------------------------------" << std::endl << std::endl;
    }
    void matrix_impl(const MatrixXd& mat, const std::vector<std::string> colnames) override
    {
//         MSG_DEBUG("mat" << std::endl << mat);
        if (colnames.size()) {
            line_impl(colnames);
        }
        m_file << mat.format(model_format());
    }
    void matrix_impl(const Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic>& mat) override
    {
        m_file << mat.format(model_format());
    }
    void matrix_impl(std::string title, const mws_model_xtx& mat) override
    {
        header(title);
        m_file << mat << std::endl;
    }
    void matrix_impl(std::string title, const mws_result& mat) override
    {
        header(title);
        for (const auto& sec: mat.m_column_sections) {
            MSG_DEBUG("[matrix_impl keys] " << sec.field_labels());
        }
        m_file << mat << std::endl;
    }
    void matrix_impl(std::string title, const mws_result_by_block& mat) override
    {
        header(title);
        m_file << mat << std::endl;
    }
    void matrix_impl(std::string title, const mws_contrast& mat, const MatrixXi& styles) override
    {
        const auto& L = significance_legend();
        auto& m = mat.m_matrix;
        Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic> tmp(m.rows(), m.cols());
        for (int r = 0; r < m.rows(); ++r) {
            for (int c = 0; c < m.cols(); ++c) {
                if (r != c) {
                    tmp(r, c) = SPELL_STRING(m(r, c) << ' ' << std::setw(3) << std::right << L.labels[styles(r, c)]);
                }
            }
        }
        header(title);
        model_print::matrix_with_sections<void, std::vector<char>, model_block_key, std::vector<char>, Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic>> mws(tmp);
        for (const auto& s: mat.m_column_sections) {
            mws.add_column_section(s.label(), s.field_labels());
            mws.add_row_section(s.field_labels());
        }
        m_file << mws << std::endl;
    }
    
    void contrasts_impl(const std::vector<contrast_description>& cvec) override
    {
            static auto L = significance_legend();
            Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic> mat;
            mat.resize(1 + cvec.size(), 6);
            int r = 1;
            mat(0, 0) = "Key";
            mat(0, 1) = "Contrast Group";
            mat(0, 2) = "Contrast Pair";
            mat(0, 3) = "Contrast";
            mat(0, 4) = "Significance";
            mat(0, 5) = "Signif. code";
            for (const auto& descr: cvec) {
                mat(r, 0) = descr.key;
                mat(r, 1) = descr.contrast_group;
                mat(r, 2) = descr.contrast_pair;
                mat(r, 3) = SPELL_STRING(descr.value);
                mat(r, 4) = SPELL_STRING(descr.significance_value);
                mat(r, 5) = L.labels[descr.significance_level];
                ++r;
            }
            header_impl("Contrasts");
            matrix_impl(mat);
    }

    
    void legend_impl() override
    {
        const auto& L = significance_legend();
        auto ti = L.thresholds.begin(), tj = L.thresholds.end();
        auto li = L.labels.begin();
        m_file << "Significance codes:  " << (*ti);
        for (++ti; ti != tj; ++ti, ++li) {
            m_file << " '" << (*li) << "' " << (*ti);
        }
        m_file << std::endl;
    }

    void full_map_impl(region_of_interest_type& roi, point_of_interest_type& poi) override
    {
        for (const auto& chr: active_settings->map) {
            m_file << chr.pretty_print(200, poi[chr.name], roi[chr.name]) << std::endl;
        }
    }

    void qtl_impl(const std::vector<qtl_description>& descr) override
    {
        m_file << "Detected QTLs" << std::endl << std::endl;
        m_file << qtl_descr_to_mat(descr);
    }

private:
    std::vector<std::vector<std::string>> m_selections;

    inline
    void header(const std::string& str)
    {
        size_t w = str.size() + 1;
        auto fill = [&, this] (char c) -> std::ostream& { char prev = m_file.fill(c); m_file << std::setw(w) << ""; m_file.fill(prev); return m_file; };
        m_file << std::endl;
        fill(' ') << '|' << std::endl
          << str << " |" << std::endl;
        fill('_') << '|' << std::endl << std::endl;
    }
};



struct full_map_item_type {
    std::string group;
    double locus;
    std::string feature_type;
    std::string feature_name;
    double ici_start, ici_end, oci_start, oci_end;
};


inline
std::vector<full_map_item_type>
make_full_map_item_list(const point_of_interest_type& poi, const region_of_interest_type& roi)
{
    static std::map<double, std::string> nothing_poi;
    static std::map<double, std::pair<double, double>> nothing_roi;
    std::vector<full_map_item_type> ret;
    for (const auto& chrom: active_settings->map) {
        const std::string& name = chrom.name;
        auto r_chrom_i = roi.find(name);
        auto p_chrom_i = poi.find(name);
        const auto& R = r_chrom_i == roi.end() ? nothing_roi : r_chrom_i->second;
        const auto& P = p_chrom_i == poi.end() ? nothing_poi : p_chrom_i->second;
        auto ri = R.begin(), rj = R.end();
        auto pi = P.begin(), pj = P.end();
        auto li = chrom.raw.marker_locus.begin(), lj = chrom.raw.marker_locus.end();
        auto ni = chrom.raw.marker_name.begin()/*, nj = chrom.raw.marker_name.end()*/;
        for (; li != lj; ++li, ++ni) {
            double l = *li;
            while (pi != pj && pi->first < l) {
                double icis = -1, icie = -1, ocis = -1, ocie = -1;
                if (ri != rj && ri->first == pi->first) {
                    std::tie(ocis, ocie) = ri->second;
                    ++ri;
                }
                ret.emplace_back(full_map_item_type{name, pi->first, std::string{"QTL"}, pi->second, icis, icie, ocis, ocie});
                ++pi;
            }
            ret.emplace_back(full_map_item_type{name, l, std::string{"MARK"}, *ni, -1., -1., -1., -1.});
        }
    }
    return ret;
}


struct report_stream_excel : public report_stream_base {
    bool behaviour(ReportMode) override { return true; }

    std::string
    workbook_path(std::string name) { return make_report_path(trait_name(), "excel", name, "xlsx"); }
    
#if 0
    void save_if_open()
    {
        if (m_stream.is_open()) {
            MSG_DEBUG("[XLS] Save workbook");
            std::string path = workbook_path(m_wb_name);
            char* buf = strdup(path.c_str());
            char* dir = dirname(buf);
            ensure_directories_exist(dir);
            free((void*) buf);
            m_wb.save(path);
        }
    }
#endif

    void backup()
    {
        m_backup_sheet_name = m_sheet_name;
        m_backup_wb_name = m_wb_name;
//         MSG_DEBUG("[XLS] Backup");
        
    }
    void restore()
    {
        if (m_backup_sheet_name != "") {
            select_book_impl(m_backup_wb_name, true);
            select_sheet_impl(m_backup_sheet_name, true);
//         } else {
//             m_stream.reset();
        }
        m_wb_name = m_backup_wb_name;
        m_sheet_name = m_backup_sheet_name;
        m_backup_sheet_name = m_backup_wb_name = "";
//         MSG_DEBUG("[XLS] Restore");
    }
    
    void select_comp_report()
    {
        select_book_impl("algo_trace", false);
        select_sheet_impl("Computations", false);
    }
    
    void init_selection_report_impl(const std::vector<std::string>& headers) override
    {
        auto box = current_stream().sheet.calculate_dimension();
        if (box.height() == 0) {
            backup();
            select_comp_report();
            line_impl(headers);
            restore();
        }
    }

    void select_book_impl(std::string name, bool exists) override
    {
//         MSG_DEBUG("[XLS] Select book <" << name << "> exists=" << std::boolalpha << exists);
        m_backup_wb_name = m_backup_sheet_name = m_sheet_name = "";
        m_wb_name = name;
        m_sheet_name = "";
        auto& curwb = current_workbook();
        m_on_first_sheet = curwb.sheet_by_index(0).title() == "Sheet1";
        if (!current_stream().is_open()) {
            current_stream().reset(&current_workbook());
            current_stream() << excel::style("header", [] (xlnt::style& sty) {
              xlnt::alignment al;
              al.horizontal(xlnt::horizontal_alignment::center).vertical(xlnt::vertical_alignment::center);
              sty.alignment(al);
              xlnt::font font;
              font.bold(true).size(18);
              sty.font(font);
            });
        }
    }

    void select_sheet_impl(std::string name, bool exists) override
    {
        auto& curwb = current_workbook();
        auto& stream = current_stream();
//         MSG_DEBUG("[XLS] Select sheet <" << name << "> exists=" << std::boolalpha << exists);
//         {
//             std::stringstream sheets;
//             sheets << "[XLS] In workbook with sheets:";
//             for (const auto& s: *m_stream.bookptr) {
//                 sheets << " \"" << s.title() << '"';
//             }
//             MSG_DEBUG(sheets.str());
//         }
        if (m_on_first_sheet) {
            curwb.sheet_by_index(0).title(name);
            m_on_first_sheet = false;
        }
        if (curwb.contains(name)) {
            stream << excel::select_worksheet(name);
        } else {
            stream << excel::new_worksheet(name);
        }
        m_sheet_name = name;
    }

    void line_impl(const std::vector<std::string>& cells) override
    {
        auto& stream = current_stream();
        for (const auto& s: cells) {
            stream << s << excel::next_col;
        }
        stream << excel::next_row;
    }

    void line_impl(std::string text) override
    {
        current_stream() << text << excel::next_row;
    }

    void line_impl() override
    {
        current_stream() << excel::next_row;
    }

    void selection_impl(int index, std::string chrom, std::string line, double score, double locus, double threshold) override
    {
        backup();
        select_comp_report();
        current_stream()
//             << excel::move(1, m_n_computation_rows)
            << index << excel::next_col
            << chrom << excel::next_col
            << line << excel::next_col
            << score << excel::next_col
            << locus << excel::next_col
            << threshold << excel::next_row;
//         ++m_n_computation_rows;
        restore();
    }

    void algo_phase_impl(std::string descr) override
    {
        backup();
        select_comp_report();
        current_stream() << excel::next_col << excel::next_col << descr << excel::next_row;
        ++m_n_computation_rows;
        restore();
    }
    void header_impl(std::string text) override
    {
        auto& s = current_stream();
        s << text
          << excel::merge(10, 2)
//           << excel::style("header")
          << excel::cell_format([] (xlnt::format& fmt) {
              xlnt::alignment al;
              al.horizontal(xlnt::horizontal_alignment::center).vertical(xlnt::vertical_alignment::center);
              fmt.alignment(al);
              xlnt::font font;
              font.bold(true).size(18);
              fmt.font(font);
          })
          << excel::next_row << excel::next_row;
        
        
//         line_impl(text);
    }
    void matrix_impl(const MatrixXd& mat, const std::vector<std::string> colnames) override
    {
        if (colnames.size()) {
            line_impl(colnames);
        }
        current_stream() << excel::matrix(mat) << excel::next_row << m_matrix_done;
    }
    void matrix_impl(const Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic>& mat) override
    {
        current_stream() << excel::matrix(mat) << excel::next_row << m_matrix_done;
    }
    void matrix_impl(std::string title, const mws_model_xtx& mat) override
    {
        current_stream() << excel::matrix_with_sections(title, mat) << excel::next_row << m_matrix_done;
    }
    void matrix_impl(std::string title, const mws_result& mat) override
    {
        current_stream() << excel::matrix_with_sections(title, mat) << excel::next_row << m_matrix_done;
    }
    void matrix_impl(std::string title, const mws_result_by_block& mat) override
    {
        current_stream() << excel::matrix_with_sections(title, mat) << excel::next_row << m_matrix_done;
    }
    void matrix_impl(std::string title, const mws_contrast& mat, const MatrixXi& styles) override
    {
        if (std::find(m_wb_with_styles.begin(), m_wb_with_styles.end(), m_wb_name) == m_wb_with_styles.end()) {
            current_stream() << excel_report::create_signif_styles;
            m_wb_with_styles.push_back(m_wb_name);
        }
        current_stream()
            << excel::matrix_with_sections(title, mat, [&] (int r, int c, xlnt::cell cell) {
                cell.style(excel_report::get_signif_style(mat.m_matrix(r, c)));
            })
            << excel::next_row << m_matrix_done;
    }
    void legend_impl() override
    {
        auto& s = current_stream();
        static auto R = excel::align([] (xlnt::alignment& al) { al.horizontal(xlnt::horizontal_alignment::right); });
        s << excel::push << excel::set_origin;
        auto tl = s.origin;
        s << "Significance codes" << excel::merge(3, 1) << excel::align([] (xlnt::alignment& al) {         al.horizontal(xlnt::horizontal_alignment::center); })  << excel::next_row
            <<    "0 <=" << R << excel::next_col << "***" << excel::style(SGNF_0) << excel::next_col << "< .001" << excel::next_row
            << ".001 <=" << R << excel::next_col << "**" << excel::style(SGNF_1) << excel::next_col << "< .01" << excel::next_row
            <<  ".01 <=" << R << excel::next_col << "*" << excel::style(SGNF_2) << excel::next_col << "< .05" << excel::next_row
            <<  ".05 <=" << R << excel::next_col << "." << excel::style(SGNF_3) << excel::next_col << "< .1" << excel::next_row
            <<   ".1 <=" << R << excel::next_col << excel::style(SGNF_4) << excel::next_col << "< 1.0";
        auto br = s.cursor;
        s << excel::pop << excel::border_box(tl, br, {{xlnt::color::black(), xlnt::border_style::thin}});
    }
    void full_map_impl(region_of_interest_type& roi, point_of_interest_type& poi) override
    {
        auto items = make_full_map_item_list(poi, roi);
        line_impl(std::vector<std::string>{
            "Chromosome", "Locus", "Feature type", "Feature name",
            "Inner interval start", "Inner interval end",
            "Outer interval start", "Outer interval end"
        });
        auto& s = current_stream();
        for (const auto& item: items) {
            s << excel::next_row
              << item.group << excel::next_col
              << item.locus << excel::next_col
              << item.feature_type << excel::next_col
              << item.feature_name << excel::next_col;
            if (item.ici_start != -1) { s << item.ici_start; } s << excel::next_col;
            if (item.ici_end != -1) { s << item.ici_end; } s << excel::next_col;
            if (item.oci_start != -1) { s << item.oci_start; } s << excel::next_col;
            if (item.oci_end != -1) { s << item.oci_end; }
        }
    }

    void qtl_impl(const std::vector<qtl_description>& descr) override
    {
        auto& stream = current_stream();
        stream
            << excel::set_origin
            << excel::matrix_with_sections("Detected QTLs", qtl_descr_to_mat(descr))
            << excel::next_row << excel::next_row;
//         for (const auto& d: descr) {
//             stream
//                 << d.position << excel::next_col
//                 << d.inner_interval_start << excel::next_col
//                 << d.inner_interval_end << excel::next_col
//                 << d.outer_interval_start << excel::next_col
//                 << d.outer_interval_end << excel::next_row;
//         }
    }

    void contrasts_impl(const std::vector<contrast_description>& cvec) override
    {
            static auto L = significance_legend();
            static xlnt::color border_color = xlnt::color::black();
            static xlnt::border_style border_style = xlnt::border_style::thin;
            static excel::manipulator BL = excel::border_left(border_color, border_style);
            static excel::manipulator BT = excel::border_top(border_color, border_style);
            static excel::manipulator BR = excel::border_right(border_color, border_style);
            static excel::manipulator BB = excel::border_bottom(border_color, border_style);
            static auto B1 = BL << BT << BB;
            static auto B2 = BR << BT << BB;
            auto& stream = current_stream();
            stream
                << excel::next_row;
            header_impl("Contrasts");
            stream
                << excel::set_origin
                << "Key" << B1 << excel::next_col
                << "Contrast Group" << B1 << excel::next_col
                << "Contrast Pair" << B1 << excel::next_col
                << "Contrast" << B1 << excel::next_col
                << "Significance" << B1 << excel::next_col
                << "Signif. code" << B2
                << excel_report::create_signif_styles;
            std::string prev_key, prev_group;
            for (const auto& descr: cvec) {
                std::string sty = excel_report::get_signif_style(descr.significance_value);
                std::vector<excel::manipulator> border = {BL};
                if (descr.key != prev_key || descr.contrast_group != prev_group) {
                    prev_key = descr.key;
                    prev_group = descr.contrast_group;
                    border.push_back(BT);
                }
                stream
                    << excel::next_row
                    << descr.key << border << excel::next_col
                    << descr.contrast_group << border << excel::next_col
                    << descr.contrast_pair << excel::style(sty) << border << excel::next_col
                    << descr.value << excel::style(sty) << border << excel::next_col
                    << descr.significance_value << excel::style(sty) << border << excel::next_col
                    << L.labels[descr.significance_level] << excel::style(sty) << BR << border;
            }
            auto topright = (stream.origin, stream.cursor).top_right();
            stream
                << BB << excel::prev_col
                << BB << excel::prev_col
                << BB << excel::prev_col
                << BB << excel::prev_col
                << BB << excel::prev_col
                << BB << excel::move(topright) << excel::next_col << excel::next_col;
            legend_impl();
                
    }

    bool check_open() const override
    {
        return current_stream().is_open() && m_sheet_name != "";
    }

    report_stream_excel()
        : m_matrix_done(excel::to_bottomleft << excel::next_row << excel::set_origin)
        , m_wb(), m_stream(), m_on_first_sheet(false)
        , m_n_computation_rows(1)
        , m_sheet_name(), m_wb_name()
        , m_backup_sheet_name(), m_backup_wb_name()
    {}
    
    ~report_stream_excel()
    {
        for (const auto& kv: m_wb) {
//             MSG_DEBUG("[XLS] Save workbook " << kv.first);
            std::string path = workbook_path(kv.first);
            ensure_path_exists(path);
            kv.second.save(path);
        }
    }
    
private:
    std::vector<excel::manipulator> m_matrix_done;
    xlnt::workbook& current_workbook() { return m_wb[m_wb_name]; }
    excel::stream& current_stream() { return m_stream[m_wb_name]; }
    const excel::stream& current_stream() const { return m_stream.find(m_wb_name)->second; }
    
    std::map<std::string, xlnt::workbook> m_wb;
    std::map<std::string, excel::stream> m_stream;
    bool m_on_first_sheet;
    int m_n_computation_rows;
    std::string m_sheet_name;
    std::string m_wb_name;
    std::string m_backup_sheet_name;
    std::string m_backup_wb_name;
    std::vector<std::string> m_wb_with_styles;
};


struct report_stream_r : public report_stream_base {
    bool behaviour(ReportMode rm) override {
        return true;
        switch (rm) {
            case ReportMode::Legend:
                return false;
            case ReportMode::AlgoSel:
            case ReportMode::AlgoPhase:
            case ReportMode::Main:
            case ReportMode::FullMap:
            case ReportMode::LOD:
            case ReportMode::Trait:
            case ReportMode::Model:
                return true;
        };
        return false;
    }
    
    void
    open(std::string title="")
    {
        close();
        std::string path;
        if (title != "") {
            path = make_report_path(trait_name(), "R", m_book, m_sheet, title, "txt");
        } else {
            path = make_report_path(trait_name(), "R", m_book, m_sheet, "txt");
        }
//         MSG_DEBUG("[R] opening " << path);
        ensure_path_exists(path);
        m_file.open(path);
    }
    
    void
    close()
    {
        if (m_file.is_open()) {
            m_file.close();
        }
    }

    template <typename MWS>
    void
    output_colnames(const MWS& mws)
    {
        bool notfirst = false;
        for (const auto& section: mws.m_column_sections) {
            for (const auto& f: section.field_labels()) {
                if (notfirst) {
                    m_file << "\t";
                }
                m_file << '"' << section.label();
                if (f.size() > 1 || (f.size() && f[0] != ' ')) {
                    m_file << '.' << f;
                }
                m_file << '"';
                notfirst = true;
            }
        }
        m_file << std::endl;
    }

    template <typename MAT>
    void
    output_matrix(const MAT& mat)
    {
        for (int r = 0; r < mat.rows(); ++r) {
            m_file << mat(r, 0);
            for (int c = 1; c < mat.cols(); ++c) {
                m_file << '\t' << mat(r, c);
            }
            m_file << std::endl;
        }
    }

    void init_selection_report_impl(const std::vector<std::string>& headers) override {}
    void select_book_impl(std::string name, bool exists) override { m_book = name; }
    void select_sheet_impl(std::string name, bool exists) override { m_sheet = name; }
    void line_impl(const std::vector<std::string>& line) override
    {
        auto i = line.begin(), j = line.end();
        if (i != j) {
            m_file << *i;
            for (++i; i != j; ++i) {
                m_file << '\t' << *i;
            }
            m_file << std::endl;
        }
    }
    void line_impl(std::string line) override {}
    void line_impl() override {}
    void selection_impl(int index, std::string chrom, std::string line, double score, double locus, double threshold) override
    {
        
    }
    void algo_phase_impl(std::string descr) override
    {
        
    }
    void header_impl(std::string text) override {}
    void matrix_impl(const Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic>& mat) override
    {
        open();
        output_matrix(mat);
        close();
    }
    void matrix_impl(const MatrixXd& mat, const std::vector<std::string> colnames) override
    {
        open();
        if (colnames.size()) {
            line_impl(colnames);
        }
        output_matrix(mat);
        close();
    }
    void matrix_impl(std::string title, const mws_model_xtx& mat) override
    {
        open(title);
        output_colnames(mat);
        output_matrix(mat.m_matrix);
        close();
    }
    void matrix_impl(std::string title, const mws_result& mat) override
    {
        open(title);
        output_colnames(mat);
        output_matrix(mat.m_matrix);
        close();
    }

    void matrix_impl(std::string title, const mws_result_by_block& mat) override
    {
        open(title);
        output_colnames(mat);
        output_matrix(mat.m_matrix);
        close();
    }

    void matrix_impl(std::string title, const mws_contrast& mat, const MatrixXi& styles) override
    {
        open(title);
        output_colnames(mat);
        output_matrix(mat.m_matrix);
        close();
    }

    void legend_impl() override {}
    void full_map_impl(region_of_interest_type& roi, point_of_interest_type& poi) override
    {
    }
    void qtl_impl(const std::vector<qtl_description>& descr) override
    {
        open("Detected QTLs");
        m_file << "chromosome.name\tlocus\tCI.inner.start\tCI.inner.end\tCI.outer.start\tCI.outer.end" << std::endl;
        for (const auto& d: descr) {
            m_file << d.chromosome << '\t' << d.position << '\t' << d.inner_interval_start << '\t' << d.inner_interval_end << '\t' << d.outer_interval_start << '\t' << d.outer_interval_end << std::endl;
        }
        close();
    }

    void contrasts_impl(const std::vector<contrast_description>& cvec) override
    {
    }

    bool check_open() const override
    {
        return true;
    }
    
private:
    std::ofstream m_file;
    
     std::string m_book, m_sheet, m_mat;   
};


struct report_stream_multiplexer {
    void
    trait(std::string name) { for (auto& sptr: m_streams) { sptr->trait(name); } }
    
    template <class REPORT_STREAM_TYPE, typename... Args>
    void
    add(Args&&... args) {
        m_streams.emplace_back(new REPORT_STREAM_TYPE(std::forward<Args>(args)...));
    }

    void
    terminate()
    {
        MSG_DEBUG("TERMINATE REPORT STREAMS");
        m_streams.clear();
    }
    
    report_stream_multiplexer&
    mode(ReportMode rm)
    {
        for (auto& sptr: m_streams) { sptr->mode(rm); }
        return *this;
    }

    report_stream_multiplexer&
    init_selection_report(const std::vector<std::string>& headers)
    {
        for (auto& sptr: m_streams) { sptr->init_selection_report(headers); }
        return *this;
    }

    report_stream_multiplexer&
    select_book(std::string name, bool exists)
    {
        for (auto& sptr: m_streams) { sptr->select_book(name, exists); }
        return *this;
    }

    report_stream_multiplexer&
    line(const std::vector<std::string>& text)
    {
        for (auto& sptr: m_streams) { sptr->line(text); }
        return *this;
    }

    report_stream_multiplexer&
    line(std::string text)
    {
        for (auto& sptr: m_streams) { sptr->line(text); }
        return *this;
    }

    report_stream_multiplexer&
    line()
    {
        for (auto& sptr: m_streams) { sptr->line(); }
        return *this;
    }

    report_stream_multiplexer&
    algo_phase(std::string descr)
    {
        for (auto& sptr: m_streams) { sptr->algo_phase(descr); }
        return *this;
    }

    report_stream_multiplexer&
    selection(int index, std::string chrom, std::string text, double score, double locus, double threshold)
    {
        for (auto& sptr: m_streams) { sptr->selection(index, chrom, text, score, locus, threshold); }
        return *this;
    }

    report_stream_multiplexer&
    full_map(region_of_interest_type& roi, point_of_interest_type& poi)
    {
        for (auto& sptr: m_streams) { sptr->full_map(roi, poi); }
        return *this;
    }

    report_stream_multiplexer&
    select_sheet(std::string name, bool exists)
    {
        for (auto& sptr: m_streams) { sptr->select_sheet(name, exists); }
        return *this;
    }

    report_stream_multiplexer&
    header(std::string text)
    {
        for (auto& sptr: m_streams) { sptr->header(text); }
        return *this;
    }

    report_stream_multiplexer&
    matrix(std::string title, const mws_result& mat)
    {
        for (auto& sptr: m_streams) { sptr->matrix(title, mat); }
        return *this;
    }

    report_stream_multiplexer&
    matrix(std::string title, const mws_result_by_block& mat)
    {
        for (auto& sptr: m_streams) { sptr->matrix(title, mat); }
        return *this;
    }

    report_stream_multiplexer&
    matrix(const MatrixXd& mat, const std::vector<std::string>& colnames={})
    {
        for (auto& sptr: m_streams) { sptr->matrix(mat, colnames); }
        return *this;
    }

    report_stream_multiplexer&
    matrix(std::string title, const mws_model_xtx& mat)
    {
        for (auto& sptr: m_streams) { sptr->matrix(title, mat); }
        return *this;
    }

    report_stream_multiplexer&
    matrix(std::string title, const mws_contrast& mat, const MatrixXi& styles)
    {
        for (auto& sptr: m_streams) { sptr->matrix(title, mat, styles); }
        return *this;
    }

    report_stream_multiplexer&
    legend()
    {
        for (auto& sptr: m_streams) { sptr->legend(); }
        return *this;
    }

    report_stream_multiplexer&
    qtls(const std::vector<qtl_description>& descr)
    {
        for (auto& sptr: m_streams) { sptr->qtl(descr); }
        return *this;
    }

    report_stream_multiplexer&
    contrasts(const std::vector<contrast_description>& cvec)
    {
        for (auto& sptr: m_streams) { sptr->contrasts(cvec); }
        return *this;
    }

    report_stream_multiplexer()
        : m_streams()
    {
        add<report_stream_text>();
    }

    report_stream_multiplexer(std::string formats)
        : m_streams()
    {
        auto i = formats.cbegin(), j = formats.cend(), k = i;
//         size_t cursor = 0, end = formats.size();
        MSG_DEBUG("Initializing report formats <" << formats << '>');
        while (i != j) {
            k = i;
            while (k != j && *k != ',') { ++k; }
            std::string token(i, k);
//             MSG_DEBUG("Have token " << token);
            i = k + (k != j);
            if (token == "text") {
                add<report_stream_text>();
            } else if (token == "r") {
                add<report_stream_r>();
            } else if (token == "excel") {
                add<report_stream_excel>();
            } else {
                MSG_ERROR("Unknown report format '" << token << '\'', "Allowed report formats are 'text', 'r', and 'excel'. The list must be comma-separated without spaces.");
            }
        }
//         while (cursor < end) {
//             size_t tokenpos = formats.find_first_of(',', cursor);
//             size_t incr;
//             if (tokenpos == std::string::npos) {
//                 tokenpos = end;
//                 incr = tokenpos;
//             } else {
//                 incr = tokenpos + 1;
//             }
//             std::string token(i + cursor , i + tokenpos);
//             MSG_DEBUG(" - " << token);
//             i += cursor + (cursor != std::string::npos);  /* skip comma if not at the end of string */
//             cursor = incr;
//             if (token == "text") {
//                 add<report_stream_text>();
//             } else if (token == "r") {
//                 add<report_stream_r>();
//             } else if (token == "excel") {
//                 add<report_stream_excel>();
//             } else {
//                 MSG_ERROR("Unknown report format '" << token << '\'', "Allowed report formats are 'text', 'r', and 'excel'. The list must be comma-separated without spaces.");
//             }
//             i += cursor;
//         }
    }

private:
    std::vector<std::unique_ptr<report_stream_base>> m_streams;
};

#endif  // defined(_SPEL_REPORT_OUTPUT_H_)

// #endif

