/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_COMPUTATIONS_GENETIC_H_
#define _SPEL_COMPUTATIONS_GENETIC_H_

namespace computations {


/** unique_unphased_locus_vectors
 * vector of bool Vectors corresponding to the phase-free locus vectors for a given generation (e.g. for F2 : 1000, 0110, 0001)
 */
#if 0
struct unique_unphased_locus_vectors : computed_value<unique_unphased_locus_vectors,
                                                      std::vector<VectorXb>,
                                                      generation_value> {
    using computed_value<unique_unphased_locus_vectors, std::vector<VectorXb>, generation_value>::computed_value;

    static bool allele_pairs_equal(const allele_pair& a, const allele_pair& b)
    {
        return (a.first == b.first && a.second == b.second)
            || (a.first == b.second && a.second == b.first);
    }

    static
        std::vector<VectorXb>
        do_compute(const generation_value& gen)
        {
            const auto& process = gen.v()->main_process();
            for (const auto& ap: process.row_labels) {
                bool found = false;
                for (auto u: uniq) {
                    found |= allele_pairs_equal(ap, u);
                }
                if (!found) {
                    uniq.push_back(ap);
                }
            }
            std::vector<VectorXb> ret(uniq.size());
            for (size_t i = 0; i < uniq.size(); ++i) {
                for (size_t j = 0; j < process.row_labels.size(); ++j) {
                    ret[i](j) = allele_pairs_equal(uniq[i], process.row_labels[j]);
                }
            }
            return ret;
        }

    static const bool composite = true;
};
#endif

/* TODO: something to add a vector of QTL loci (see uulv above) to a (chromosome+mgo) to compute multi-point probabilities */


/** observation_vectors
 * Map of observation_symbol -> equivalent_state_vector
 */
struct observation_vectors
        : computed_value<observation_vectors,
                         std::map<char, VectorXb>,
                         generation> {
    typedef computed_value<observation_vectors, std::map<char, VectorXb>, generation> base;
    using base::computed_value;
    using base::v;
    static std::map<char, VectorXb>
        do_compute(const generation& gen)
        {
            marker_observation_spec mos(gen);
            std::map<char, VectorXb> ret = gen.v()->observation_vectors(mos);
            return ret;
        }
};

/** all_observation_vectors
 * Vector of observation_vectors for each observed generation
 */
struct all_observation_vectors
        : computed_collection<observation_vectors> {
    using computed_collection<observation_vectors>::computed_collection;
    using computed_collection<observation_vectors>::size;
    using computed_collection<observation_vectors>::operator [];
};

/** all_observed_generations
 * Vector of generation for each observed generation
 */
struct all_observed_generations
        : computed_collection<generation> {
    using computed_collection<generation>::computed_collection;
    using computed_collection<generation>::size;
    using computed_collection<generation>::operator [];
};


/** pedigree
 * Vector of individual/ancestor indices for each observed generation
 * for a given individual.
 *
 * For instance, pedigree of #1 in F3 may be [1, 0] if it was spawned from
 * individual #0 in F2 and observed generations are [F3, F2].
 */
struct pedigree : value<std::vector<int>> {
    pedigree() : value<std::vector<int>>() {}
    pedigree(const population_value& pop, const generation_value& gen, const value<int>& ind)
        : value<std::vector<int>>{pop.v()->get_pedigree(gen.v(), ind)}
    {}
    pedigree(const std::tuple<population_value, generation_value, value<int>>& t)
        : value<std::vector<int>>{std::get<0>(t).v()->get_pedigree(std::get<1>(t).v(), std::get<2>(t))}
    {}
};

/** individual_range
 * Range of all valid individual indices for a given generation in a given population
 */
struct individual_range : range<int> {
    int _get_size(const ::population& pop)
    {
        auto it = pop.observed_mark.find(pop.qtl_generation_name);
        if (it == pop.observed_mark.end()) {
            return 0;
        }
        return it->second.observations.n_obs;
    }

    individual_range(const ::population& pop)
        : range<int>(0, _get_size(pop))
    {}
};

/** all_qtl_pedigrees
 * Vector of pedigrees for all individuals in the QTL generation for a population
 */
struct all_qtl_pedigrees : computed_collection<pedigree> {
    all_qtl_pedigrees(const population_value& pop)
        : computed_collection<pedigree>(pop, generation(pop.v()->qtl_generation_name), individual_range(*pop.v()))
    {}
};

/** population_marker_obs
 * All locus vectors for all generations for a given pedigree
 */
struct population_marker_obs
        : computed_value<population_marker_obs,
                         multi_generation_observations,
                         population_value, chromosome_value, pedigree> {
    using computed_value<population_marker_obs, multi_generation_observations, population_value, chromosome_value, pedigree>::computed_value;
    static multi_generation_observations do_compute(const population_value& pop, const chromosome_value& chr, const pedigree& ped)
    {
        qtl_generation qtl_gen(pop);
        all_observed_generation_names aogn(pop);
        all_observed_generations aog(aogn);
        static_assert(all_observed_generations::composite, "all_observed_generations is not composite");
        multi_generation_observations mgo(chr.v()->marker_locus.size(), qtl_gen.v());
        for (size_t i = 0; i < aogn.size(); ++i) {
            observation_vectors ov(aog[i]);
            MatrixXb tmp = aog[i].v()->raw_observations(ov.v(), marker_observations(pop, chr, aogn[i], ped[i]).v());
            mgo[aog[i].v()] = tmp;
        }
        qtl_gen.v()->update_locus_vectors(mgo);
        return mgo;
    }
};

/** all_population_marker_obs
 * All population_marker_obs for a given chromosome for each pedigree in the QTL generation for a given population
 */
struct all_population_marker_obs : computed_collection<population_marker_obs> {
    all_population_marker_obs(const population_value& pop, const chromosome_value& chr)
        : computed_collection<population_marker_obs>(
                pop, chr, all_qtl_pedigrees(pop))
    {}
};

/** locus_probabilities
 * State probabilities for a given individual for a given generation
 */
struct locus_probabilities : computed_value<locus_probabilities,
                                            labelled_matrix<MatrixXd, allele_pair, double>,
                                            value<impl::generation_rs::segment_computer_t>,
                                            value<const generation_rs*>, population_marker_obs,
                                            value<std::vector<double>>> {
    using computed_value<locus_probabilities,
                         labelled_matrix<MatrixXd, allele_pair, double>,
                         value<impl::generation_rs::segment_computer_t>,
                         value<const generation_rs*>, population_marker_obs,
                         value<std::vector<double>>>::computed_value;

    static labelled_matrix<MatrixXd, allele_pair, double>
        do_compute(const value<impl::generation_rs::segment_computer_t>& gc,
                   const value<const generation_rs*>& gen, const population_marker_obs& pmo,
                   const value<std::vector<double>>& sqoc)
        {
            auto it = pmo.v().find(gen.v());
            if (it == pmo.v().end()) {
                return {};
            }
            return const_cast<impl::generation_rs::segment_computer_t*>(&gc.v())->compute(it->second);
            (void)sqoc;
        }
};


#if 0
struct extended_chromosome
        : computed_value<extended_chromosome,
                         chromosome_value::value_type,
                         chromosome_value, value<double>> {
    ::chromosome chr;

    static
        chromosome_value::value_type
        do_compute(const chromosome_value& original, const value<double>& qtl_pos)
        {
            chr.name = original.name;
            chr.marker_name = original.marker_name;
            chr.marker_locus.reserve(original.marker_locus.size() + 1);
            size_t insertion_point;
            double qtl = qtl_pos.v();
            auto li = original.marker_locus.begin();
            auto lj = original.marker_locus.end();
            while (li != lj) {
                if (*li < qtl) {
                    chr.marker_locus.push_back(*li);
                } else if (*li == qtl) {
                    insertion_point = chr.marker_locus.size();
                    chr.marker_locus.insert(chr.marker_locus.end(), li, lj);
                    break;
                } else {
                    chr.marker_name.insert(chr.marker_name.begin() + chr.marker_locus.size(), "");
                    chr.marker_locus.push_back(qtl);
                    chr.marker_locus.insert(chr.marker_locus.end(), li, lj);
                    break;
                }
            }
            return &chr;
        }
};
                     

struct joint_locus_probabilities
        : computed_value<locus_probabilities,
                         labelled_matrix<MatrixXd, allele_pair, double>,
                         value<impl::generation_rs::segment_computer_t>, value<const generation_rs*>, population_marker_obs,
                         value<double>, value<Eigen::VectorXb>> {
};

struct single_joint_locus_probabilities
        : computed_value<joint_locus_probabilities,
                         labelled_matrix<MatrixXd, allele_pair, double>,
                         population_value, generation_value, chromosome_value,
                         value<labelled_matrix<MatrixXd, allele_pair, double>>,
                         value<double>, value<Eigen::VectorXb>> {
    static
        labelled_matrix<MatrixXd, allele_pair, double>
        do_compute(const population& pop, const generation& gen, const chromosome& chr,
                   const value<labelled_matrix<MatrixXd, allele_pair, double>& prob,
                   const value<double> qtl, const value<Eigen::VectorXb>& qtl_state)
        {
        }
};
#endif


/** all_locus_probabilities
 * State probabilities for all individuals in a given generation
 */
struct all_locus_probabilities : computed_collection<locus_probabilities> {
    using computed_collection<locus_probabilities>::computed_collection;
    using computed_collection<locus_probabilities>::size;
    using computed_collection<locus_probabilities>::operator [];
};

/** state_to_first_order_parental_origin_matrix
 * Matrix to transform locus_probabilities into simple parental origin
 */

template <typename T>
struct cartesian_product {
    typedef typename T::iterator value_iterator;
    typedef typename T::value_type value_type;
    std::vector<T> sets;

    void push_back(const T& x) { sets.push_back(x); }
    void push_back(T&& x) { sets.push_back(x); }

    struct iterator {
        bool carry;
        std::vector<value_iterator> components;
        cartesian_product<T>* cp;

        iterator(cartesian_product<T>* _cp_, bool c)
            : carry(c), components(), cp(_cp_)
        {
            if (!carry) {
                typename std::vector<T>::iterator i = cp->sets.begin(), j = cp->sets.end();
                for (; i != j; ++i) {
                    components.push_back(i->begin());
                    /*std::cout << "iterator stored " << (*(i->begin())) << std::endl;*/
                }
                /*for (auto s: cp->sets) {*/
                    /*ret.components.push_back({ s.begin(), s.end(), s.begin() });*/
                    /*components.push_back(s.begin());*/
                /*}*/
            }
        }

        std::vector<value_type> operator * () const
        {
            std::vector<value_type> ret;
            for (auto x: components) {
                ret.push_back(*x);
            }
            return ret;
        }

        template <typename Function>
            void apply(Function f)
            {
                size_t i = 0;
                for (auto x: components) {
                    if (!f(i++, *x)) {
                        break;
                    }
                }
            }

        iterator& operator ++ ()
        {
            auto rbeg = components.rbegin();
            auto rend = components.rend();
            auto s = cp->sets.rbegin();
            bool c = true;
            for (; c && rbeg != rend; ++rbeg, ++s) {
                c = (++(*rbeg) == s->end());
                if (c) {
                    /*std::cout << "carry!" << std::endl;*/
                    *rbeg = s->begin();
                } else {
                    break;
                }
            }
            if (rbeg == rend) {
                carry = true;
            }
            return *this;
        }

        bool operator != (const iterator&)
        {
            /* only used to test against end, so only check carry. */
            return !carry;
        }

        bool operator == (const iterator&)
        {
            /* only used to test against end, so only check carry. */
            return carry;
        }
    };

    size_t size() const
    {
        size_t ret = 1;
        for (auto& s: sets) {
            ret *= s.size();
        }
        return ret;
    }

    iterator begin()
    {
        return iterator(this, false);
    }

    iterator end()
    {
        return iterator(this, true);
    }
};

struct state_to_first_order_parental_origin_matrix : computed_value<state_to_first_order_parental_origin_matrix,
                                                         labelled_matrix<MatrixXd, std::vector<char>, allele_pair>,
                                                         generation, qtl_chromosome, selected_qtls_on_chromosome> {
    using computed_value<state_to_first_order_parental_origin_matrix, labelled_matrix<MatrixXd, std::vector<char>, allele_pair>, generation, qtl_chromosome, selected_qtls_on_chromosome>::computed_value;
    static labelled_matrix<MatrixXd, std::vector<char>, allele_pair>
        do_compute(const generation& gen, const qtl_chromosome& qtl_chr, const selected_qtls_on_chromosome&)
        {
            /*MSG_DEBUG("StFOPOM gen @ " << ((void*)gen.v()));*/
            std::set<char> parents;
            /*::qtl_chromosome tmp_qc(*qtl_chr.v());*/
            /*tmp_qc.add_qtl({1.e6});*/
            const std::vector<allele_pair>& labels = gen.v()->main_process().column_labels;
            /*MSG_DEBUG("labels " << labels);*/
            for (const allele_pair& ap: labels) {
                parents.insert(ap.first);
                parents.insert(ap.second);
            }
            std::map<char, int> parent_index;
            for (char c: parents) {
                size_t sz = parent_index.size();
                parent_index[c] = sz;
            }
            std::vector<char> parent_symbol(parents.begin(), parents.end());
            auto qsi = qtl_chr.v()->qtl_state_iterator(gen.v()->unphased_LV());
            std::vector<std::vector<char>> parents_labels;
            cartesian_product<std::set<char>> parents_labels_product;
            int qtl_sz = qtl_chr.v()->qtl.size();
            for (int i = 0; i <= qtl_sz; ++i) {
                parents_labels_product.push_back(parents);
            }
            parents_labels.reserve(parents_labels_product.size());
            /*parents_labels.insert(parents_labels.end(), parents_labels_product.begin(), parents_labels_product.end());*/
            for (auto p: parents_labels_product) {
                parents_labels.push_back(p);
            }
            /*do {*/
                /*parents_labels.emplace_back();*/
                /*parents_labels.back().reserve(qtl_chr.v()->qtl.size());*/
                /*for (int qs: qsi.qtl_state) {*/
                    /*parents_labels.back().push_back(parent_symbol[qs]);*/
                /*}*/
            /*} while (!qsi.next());*/

            /*MSG_DEBUG("parent_index " << parent_index);*/
            std::vector<allele_pair> full_labels;
            do {
                full_labels.insert(full_labels.end(), labels.begin(), labels.end());
            } while (!qsi.next());
            labelled_matrix<MatrixXd, std::vector<char>, allele_pair> ret(parents_labels.begin(), parents_labels.end(), full_labels.begin(), full_labels.end());
            cartesian_product<std::vector<allele_pair>> qtl_state_product;
            for (int i = 0; i <= qtl_sz; ++i) {
                qtl_state_product.push_back(labels);
            }
            int col = 0;
            std::vector<char> l1, l2;
            l1.reserve(1 + qtl_sz);
            l2.reserve(1 + qtl_sz);
            int row1, row2;
            for (std::vector<allele_pair> haploz: qtl_state_product) {
                l1.clear();
                l2.clear();
                for (auto h: haploz) {
                    l1.push_back(h.first);
                    l2.push_back(h.second);
                }
                /*MSG_DEBUG("l1: " << l1);*/
                /*MSG_DEBUG("l2: " << l2);*/
                row1 = std::find(parents_labels.begin(), parents_labels.end(), l1) - parents_labels.begin();
                row2 = std::find(parents_labels.begin(), parents_labels.end(), l2) - parents_labels.begin();
                ret.data(row1, col) += 1;
                ret.data(row2, col) += 1;
                ++col;
            }
#if 0
            MatrixXd core1 = MatrixXd::Zero(parent_index.size(), labels.size());
            MatrixXd core2 = MatrixXd::Zero(parent_index.size(), labels.size());
            for (size_t i = 0; i < labels.size(); ++i) {
                core1(parent_index[labels[i].first], i) = 1;
                core2(parent_index[labels[i].second], i) = 1;
            }
            /*MatrixXb bcore = (core.array() != 0.).select(MatrixXb::Ones(core.innerSize(), core.outerSize()), MatrixXb::Zero(core.innerSize(), core.outerSize()));*/
            /*MSG_DEBUG("core" << std::endl << core);*/
            /*MatrixXb bcore = core.cast<bool>();*/
            /*MSG_DEBUG("bcore" << std::endl << bcore);*/
            MatrixXd kron1 = MatrixXd::Ones(1, 1);
            MatrixXd kron2 = MatrixXd::Ones(1, 1);
            for (size_t i = 0; i <= qtl_chr.v()->qtl.size(); ++i) {
                MatrixXd tmp = kroneckerProduct(kron1, core1);
                kron1 = tmp;
                tmp = kroneckerProduct(kron2, core2);
                kron2 = tmp;
            }
            MSG_DEBUG((kron2 + kron1 - ret.data));
            /*ret.data = kroneckerProduct(core, kron.cast<double>());*/
            /*ret.data = kron;*/
#endif
            auto kron = const_cast<generation_rs*>(gen.v())->compute_state_to_parental_origin(qtl_chr.v()->qtl.size());
            /*MSG_DEBUG(kron.data - ret.data);*/
            MSG_DEBUG("Eq: " << ((kron.data - ret.data).array() != 0).any());
            return ret;
        }
};


static inline
MatrixXd concat_right(const std::vector<const MatrixXd*>& mat_vec)
{
    size_t full_size = 0;
    MatrixXd ret;
    for (auto m: mat_vec) {
        full_size += m->outerSize();
        /*MSG_DEBUG("preparing concat_right with matrix(" << m->innerSize() << ',' << m->outerSize() << ')');*/
    }
    ret.resize(mat_vec.front()->innerSize(), full_size);
    full_size = 0;
    for (auto m: mat_vec) {
        /*MSG_DEBUG("concat_right in M(" << ret.innerSize() << ',' << ret.outerSize() << ") at col " << full_size << "matrix(" << m->innerSize() << ',' << m->outerSize() << ')');*/
        /*ret.block(0, full_size, ret.innerSize(), m->outerSize()) = *m;*/
        ret.middleCols(full_size, m->outerSize()) = *m;
        full_size += m->outerSize();
    }
    return ret;
}

/** parental_origin
 * Transform locus_probabilities into simple parental origin
 */
struct parental_origin : computed_value<parental_origin,
                                        labelled_matrix<MatrixXd, std::vector<char>, double>,
                                        locus_probabilities, generation,
                                        selected_qtls_on_chromosome> {
    using computed_value<parental_origin,
                         labelled_matrix<MatrixXd, std::vector<char>, double>,
                         locus_probabilities, generation,
                         selected_qtls_on_chromosome>::computed_value;
    static labelled_matrix<MatrixXd, std::vector<char>, double>
        do_compute(const locus_probabilities& lp, const generation& gen, const selected_qtls_on_chromosome& sqoc)
        {
            /*MSG_DEBUG(__FILE__ << ':' << __LINE__);*/
            /*auto& s = stfopom.v();*/
            auto& s = const_cast<generation_rs*>(gen.v())->compute_state_to_parental_origin(sqoc->size());
            auto& l = lp.v();
            /*MSG_DEBUG("stfopom(" << s.innerSize() << ',' << s.outerSize() << ") lp(" << l.innerSize() << ',' << l.outerSize() << ')');*/
            return s * l;
            /*labelled_matrix<MatrixXd, char, double> ret;*/
            /*ret.reserve(1);*/
            /*MatrixXd x = concat_right(lp.v());*/
            /*ret.emplace_back(stfopom.v() * x);*/
            /*ret.reserve(lp.v().size());*/
            /*for (auto& m: lp.v()) {*/
                /*ret.emplace_back(stfopom.v() * m);*/
            /*}*/
            /*return ret;*/
        }
};



/** all_parental_origins
 * Vector of parental_origin for each individual in the qtl_generation for a given population
 */
struct all_parental_origins : computed_collection<parental_origin> {
    /*all_parental_origins(const genoprob_computer& gc, const population& pop, const chromosome& chr, const state_to_first_order_parental_origin_matrix& stfopom)*/
        /*: computed_collection<parental_origin>(*/
                /*all_locus_probabilities(gc,*/
                                        /*generation(pop.v()->qtl_generation_name),*/
                                        /*all_population_marker_obs(pop, chr),*/
                                        /*selected_qtls_on_chromosome(qtl_chromosome(chr.v()->name))),*/
                /*stfopom)*/
    /*{}*/
    using computed_collection<parental_origin>::computed_collection;
    using computed_collection<parental_origin>::size;
    using computed_collection<parental_origin>::operator [];
};


/** parental_origins_per_locus
 * map of [locus] -> parental_origin_matrix (parent X individuals)
 * transposition of all_parental_origins
 */
struct parental_origins_per_locus : computed_value<parental_origins_per_locus,
                                                   std::vector<labelled_matrix<MatrixXd, int, std::vector<char>>>,
                                                   all_parental_origins> {
    using computed_value<parental_origins_per_locus, std::vector<labelled_matrix<MatrixXd, int, std::vector<char>>>, all_parental_origins>::computed_value;

    static
    std::vector<labelled_matrix<MatrixXd, int, std::vector<char>>>
    do_compute(const all_parental_origins& apo)
    {
        auto& tmp = apo[0].v();
        size_t n_ind = apo.size();
        size_t n_par = tmp.innerSize();
        size_t n_loc = tmp.outerSize();
        MSG_DEBUG("n_ind " << n_ind);
        MSG_DEBUG("n_par " << n_par);
        MSG_DEBUG("n_loc " << n_loc);

        /* labels for individuals */
        std::vector<int> individuals;
        individuals.resize(n_ind);
        size_t index = 0;
        std::generate(individuals.begin(), individuals.end(), [&] () { return index++; });
        /* labels for parents */
        std::vector<std::vector<char>> parents = tmp.row_labels;
        /* loci */
        std::vector<double> loci = tmp.column_labels;
        /* return value */
        std::vector<labelled_matrix<MatrixXd, int, std::vector<char>>> ret(loci.size());

        /* first round : init matrices */
        for (size_t l = 0; l < n_loc; ++l) {
            /*labelled_matrix<MatrixXd, int, std::vector<char>>& lm = ret[loci[l]];*/
            labelled_matrix<MatrixXd, int, std::vector<char>>& lm = ret[l];
            lm.row_labels = individuals;
            lm.column_labels = parents;
            lm.data.resize(n_ind, n_par);
            for (int ind: individuals) {
                lm.data.row(ind) = apo[ind].v().data.col(l);
            }
        }
        return ret;
    }

    static const bool composite = true;
};

#if 0
struct parental_origins_per_locus : computed_value<parental_origins_per_locus,
                                                   std::map<double, labelled_matrix<MatrixXd, int, std::vector<char>>>,
                                                   all_parental_origins> {
    using computed_value<parental_origins_per_locus, std::map<double, labelled_matrix<MatrixXd, int, std::vector<char>>>, all_parental_origins>::computed_value;

    static
    std::map<double, labelled_matrix<MatrixXd, int, std::vector<char>>>
    do_compute(const all_parental_origins& apo)
    {
        auto& tmp = apo[0].v();
        size_t n_ind = apo.size();
        size_t n_par = tmp.innerSize();
        size_t n_loc = tmp.outerSize();
        MSG_DEBUG("n_ind " << n_ind);
        MSG_DEBUG("n_par " << n_par);
        MSG_DEBUG("n_loc " << n_loc);

        /* labels for individuals */
        std::vector<int> individuals;
        individuals.resize(n_ind);
        size_t index = 0;
        std::generate(individuals.begin(), individuals.end(), [&] () { return index++; });
        /* labels for parents */
        std::vector<std::vector<char>> parents = tmp.row_labels;
        /* loci */
        std::vector<double> loci = tmp.column_labels;
        /* return value */
        std::map<double, labelled_matrix<MatrixXd, int, std::vector<char>>> ret;

        /* first round : init matrices */
        for (size_t l = 0; l < n_loc; ++l) {
            labelled_matrix<MatrixXd, int, std::vector<char>>& lm = ret[loci[l]];
            lm.row_labels = individuals;
            lm.column_labels = parents;
            lm.data.resize(n_ind, n_par);
            for (int ind: individuals) {
                lm.data.row(ind) = apo[ind].v().data.col(l);
            }
        }
        return ret;
    }

    static const bool composite = true;
};
#endif

typedef parental_origins_per_locus::value_type::value_type popl_value_type;

}

#endif

