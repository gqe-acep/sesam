/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_COMPUTATIONS_BASE_H_
#define _SPEL_COMPUTATIONS_BASE_H_

#include "error.h"
#include "input.h"
#include "labelled_matrix.h"
#include "settings.h"
#include "cache2.h"
#include "computations/to_md5.h"

namespace std {
    template <typename T>
        struct dummy_hash { size_t operator () (const T& x) { MSG_ERROR("Dummy hash! " << typeid(T).name(), "Fix the code"); throw 0; return 0; (void)x; } };

    /*template <typename R, typename L, typename M>*/
        /*struct hash<labelled_matrix<M, R, L>>*/
            /*: dummy_hash<labelled_matrix<M, R, L>> {};*/

    template <>
        struct hash<const char*> {
            size_t operator () (const char* s) {
                std::hash<std::string> h;
                return h(s);
            }
        };

#if 0
    template <>
        struct hash<impl::generation_rs::segment_computer_t> {
            size_t operator () (const impl::generation_rs::segment_computer_t& sc)
            {
                hash<std::string> hs;
                hash<std::vector<double>> hvd;
                hash<double> hd;
                return hs(sc.g_this->name) ^ hvd(sc.steps) ^ hd(sc.noise);
            }
        };
#endif

/*template <typename _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>*/
        /*struct hash<Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols>>*/
            /*: dummy_hash<Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols>> {};*/

    template <typename F, typename S>
        struct hash<pair<F, S>> {
            size_t operator () (const pair<F, S>& p) const
            {
                hash<F> hf;
                hash<S> hs;
                return hf(p.first) ^ hs(p.second);
            }
        };
}

#endif

