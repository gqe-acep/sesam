/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_REPORT_H_DECL
#define _SPEL_REPORT_H_DECL

#include "report_output.h"

enum class AR: int { RSS=1, Rank=2, Test=4, Model=8, All=0xFF };

inline
bool
operator & (AR a, AR b) { return !!(((int) a) & ((int) b)); }


struct analysis_report_computations {
    analysis_report_computations(model_manager& _);
    
    void
    check_constraints();

    void
    prepare_computations();
    
//     model_print::matrix_with_sections<std::string, void, model_block_key, std::vector<char>>
    mws_result_by_block
    R2(int i);

    int
    significance_level(double q);

    std::vector<qtl_description>
    prepare_qtls(std::vector<QTL> &qtls, ComputationType lod_test_type);
  
    std::pair<std::vector<size_t>, std::vector<std::pair<model_block_key, std::vector<std::vector<char>>>>>
    filter_columns(const MatrixXd& conrow, const model_block_collection& blocks);

    std::vector<contrast_description>
    contrasts(const MatrixXd& conmat, const MatrixXd& vcov, const MatrixXd& coef);

    std::vector<std::tuple<std::string, MatrixXd, MatrixXd, MatrixXi>>
    contrast_matrices(const std::vector<contrast_description>& descr);

    const trait_metadata_type&
    trait_md() const { return TM; }
    
    const MatrixXd&
    coefs() const { return all_coefficients; }

    const VectorXd&
    pseudo_var() const { return pseudo_variances; }

protected:
    model_manager& mm;
    point_of_interest_type poi;
    region_of_interest_type roi;

private:
    MatrixXd X;
    trait_metadata_type TM;

    MatrixXd Pt;
    MatrixXd all_coefficients;
    MatrixXd all_residuals;
    VectorXd all_rss;
    VectorXd pseudo_variances;
};


struct test_result;

struct analysis_report : public analysis_report_computations {
    analysis_report(model_manager& _)
        : analysis_report_computations(_)
        , m_stream(active_settings->report_format_string)
        , m_selection_index(0)
        , m_books()
        , m_previous_book()
        , m_current_book()
        , m_sheets(nullptr)
    {}
    
    void terminate() { m_stream.terminate(); }
    
    analysis_report&
    select_book(std::string n)
    {
        bool exists = m_books.find(n) != m_books.end();
        m_stream.select_book(n, exists);
        m_sheets = &m_books[n];
        m_previous_book = m_current_book;
        m_current_book = n;
        return *this;
    }
    
    analysis_report&
    select_previous_book()
    {
        if (m_previous_book != "") {
            m_current_book = m_previous_book;
            m_stream.select_book(m_previous_book, true).select_sheet(m_previous_sheet, true);
            m_previous_book = "";
            m_previous_sheet = "";
        }
        return *this;
    }
    
    analysis_report&
    select_sheet(std::string n)
    {
        if (!m_sheets) {
            MSG_ERROR("No workbook selected. A workbook must be selected before selecting a worksheet.", "Go have coffee and come back when you feel better.");
        }
        bool exists = std::find(m_sheets->begin(), m_sheets->end(), n) != m_sheets->end();
        m_stream.select_sheet(n, exists);
        if (!exists) {
            m_sheets->push_back(n);
        }
        m_previous_sheet = m_current_sheet;
        m_current_sheet = n;
        return *this;
    }
    
    analysis_report&
    report_algo_phase(std::string descr)
    {
        
        m_stream.mode(ReportMode::AlgoPhase);
        select_book("algo_trace").select_sheet("Computations");
//         m_stream.line(SPELL_STRING("[ALGO] " << descr));
        m_stream.algo_phase(descr);
        return *this;
    }

    analysis_report&
    report_algo_selection(model& Mbase, std::string chrom, double score, double locus, double threshold);

    analysis_report&
    report_full_map(region_of_interest_type& R, point_of_interest_type& P)
    {
        m_stream.mode(ReportMode::FullMap);
        select_book("report").select_sheet("Full Map");
        m_stream.full_map(R, P);
        return *this;
    }
    
    analysis_report&
    report_trait(std::string name, const MatrixXd& values)
    {
        m_stream.mode(ReportMode::Trait);
        select_book("report").select_sheet(SPELL_STRING("trait_" << name));
        m_stream.matrix(values);
        return *this;
    }
    
    template <typename MAT_OR_VEC, MAT_OR_VEC computation_along_chromosome::* FIELD, bool TRANSPOSE>
    static
    void
    include_data(int& c, std::vector<std::string>& colnames, MatrixXd& values, std::string colname, const computation_along_chromosome& cac)
    {
        const MAT_OR_VEC& mat = cac.*FIELD;
        int ncols;
        if (TRANSPOSE) {
            ncols = mat.rows();
        } else {
            ncols = mat.cols();
        }
        MSG_DEBUG("include_data values.cols()=" << values.cols() << " c=" << c << " ncols=" << ncols);
        if (ncols) {
            colnames.insert(colnames.end(), ncols, colname);
            if (TRANSPOSE) {
                values.middleCols(c, ncols) = mat.transpose();
            } else {
                values.middleCols(c, ncols) = mat;
            }
            c += ncols;
        }
    }
    
    analysis_report&
    report_computation(model& Mbase, std::string chrom, std::string trait_name, std::vector<double>& loci, const computation_along_chromosome& cac, const test_result& tr);
    analysis_report&
    report_model(const model& Mcurrent);
    analysis_report&
    report_legend()
    {
        m_stream
        .mode(ReportMode::Legend)
        .legend();
        return *this;
    }
    
    analysis_report&
    report_qtls();
 
    analysis_report&
    trait(std::string name) { m_stream.trait(name); return *this; }
    
//     analysis_report&
//     line() { m_stream.line(); return *this; }

    template <typename... Args>
    analysis_report&
    line(Args&&... args) { m_stream.line(std::forward<Args>(args)...); return *this; }
    
    template <typename... Args>
    analysis_report&
    matrix(Args&&... args) { m_stream.matrix(std::forward<Args>(args)...); return *this; }
    
    analysis_report&
    header(std::string str) { m_stream.header(str); return *this; }

    analysis_report&
    report_contrasts(const MatrixXd& conmat, const MatrixXd& vcov, const MatrixXd& coef)
    {
        auto c = contrasts(conmat, vcov, coef);
        m_stream.contrasts(c);
        return *this;
    }
    
    void init_selection_report();

private:
    report_stream_multiplexer m_stream;
    int m_selection_index;
    std::map<std::string, std::vector<std::string>> m_books;
    std::string m_previous_book, m_current_book, m_previous_sheet, m_current_sheet;
    std::vector<std::string>* m_sheets;
};


void init_analysis_report(model_manager& mm);

void make_report(analysis_report& report, model_manager& mm);
void report_lod(analysis_report&, model_manager&);
void report_full_map();

#endif  // defined(_SPEL_REPORT_H_DECL)
