/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_SCRIPT_PARSER_H_
#define _SPELL_SCRIPT_PARSER_H_

#include "tokenizer.h"
#include "error.h"
#include "script.h"
#include <exception>

namespace script {

struct SyntaxError : public std::runtime_error {
    size_t offset;
    SyntaxError(size_t ofs)
        : std::runtime_error(SPELL_STRING("Syntax error at offset " << ofs)), offset(ofs)
    {}
};


struct ParseFinished {};

enum token_class {
    Whitespace,
    KeyWord,
    TypeName,
    SourceName,
    PluralSourceName,
    TestName,
    Punctuation,
    OpenPar,
    ClosePar,
    Plus,
    Minus,
    Identifier,
    Equal,
    Number,
    Printable,
    String,
    Keys,
    EOS
};


inline
std::ostream& operator << (std::ostream& os, token_class tc)
{
#define DUMP(x) case x: os << #x; break
    switch(tc) {
        DUMP(Whitespace);
        DUMP(KeyWord);
        DUMP(TypeName);
        DUMP(SourceName);
        DUMP(PluralSourceName);
        DUMP(TestName);
        DUMP(Punctuation);
        DUMP(OpenPar);
        DUMP(ClosePar);
        DUMP(Plus);
        DUMP(Minus);
        DUMP(Identifier);
        DUMP(Equal);
        DUMP(Number);
        DUMP(Printable);
        DUMP(String);
        DUMP(Keys);
        DUMP(EOS);
    };
#undef DUMP
    return os;
}

typedef tokenizer<token_class> script_tokenizer;

struct UnexpectedToken : public std::runtime_error {
    UnexpectedToken(script_tokenizer::tokenizer_state::iterator tok_i, const char* msg)
        : std::runtime_error(SPELL_STRING("Syntax error on token " << (*tok_i) << ":" << std::endl << "Expected " << msg))
    {}
};


std::string unescape(std::string s)
{
    std::string ret;
    if (s.size() < 2 || s.front() != '"' || s.back() != '"') {
        return s;
    }
    auto i = s.begin();
    auto j = s.end();
    --j;
    ++i;
    ret.reserve(s.size());
    while (i != j) {
        if (*i == '\\') {
            switch (*++i) {
                case 't': ret += '\t'; break;
                case 'r': ret += '\r'; break;
                case 'n': ret += '\n'; break;
                case '"': ret += '"'; break;
                default: ret += '\\'; ret += *i;
            };
        } else {
            ret += *i;
        }
        ++i;
    }
    return ret;
}


struct script_parser {
    script_tokenizer m_toker;

    typedef typename script_tokenizer::token_type token_type;

    script_parser()
        : m_toker
          {
              {Whitespace, "^[ \r\n\t]+"},
              {KeyWord, "^\\b(for|as|with|along|from|to|and|set|if|select|deselect|above|next|all|max|compute|output|file|data|append)\\b"},
              {TypeName, "^\\b(filename|model|chromosome|locus|result|selection|test)\\b"},
              {SourceName, "^\\b(each-chromosome|each-selected-locus)\\b"},
              {PluralSourceName, "^\\b(chromosomes|selected_loci)\\b"},
              {TestName, "^\\b(FTest)\\b"},
              {Punctuation, "^[;,.]"},
              {OpenPar, "^[(]"},
              {ClosePar, "^[)]"},
              {Plus, "^[+]"},
              {Minus, "^[-]"},
              {Equal, "^[=]"},
              {Number, "^[0-9]+[.][0-9]*|[.][0-9]+"},
              {Printable, "^\\b(loci|scores|X|XtX|XtXinv|coefficients|residuals|rank)\\b"},
              {String, "^\"([^\\\"]|\\\\[\"trn])*\""},
              {Keys, "^\\bkeys\\b"},
              {Identifier, "^[a-zA-Z_][a-zA-Z0-9_]*"},
              {EOS, "^$"}
          }
    { m_toker.discard.insert(Whitespace); }

    /* parsing context */
    script_tokenizer::tokenizer_state::iterator tok_i, tok_end;

    bool at_end() const { return tok_i == tok_end; }

    bool accept(token_class tc) { if (tok_i->cls == tc && tok_i != tok_end) { ++tok_i; return true; } return false; }
    void expect(token_class tc) { if (tok_i->cls != tc && tok_i != tok_end) { throw SyntaxError(tok_i->ofs); } ++tok_i; }

    bool accept(token_class tc, const std::string& text) { if (tok_i->cls == tc && tok_i->text == text && tok_i != tok_end) { ++tok_i; return true; } return false; }
    void expect(token_class tc, const std::string& text) { if (tok_i->cls != tc || (tok_i->text != text && tok_i != tok_end)) { throw SyntaxError(tok_i->ofs); } ++tok_i; }

    template <typename T, typename... Args>
        std::shared_ptr<T> alloc(Args&&... a) { return std::shared_ptr<T>(new T(std::forward<Args>(a)...)); }

    void dump_error(const std::string& text, size_t offset)
    {
        auto start = text.rbegin() + (text.size() - offset);
        size_t line_start = (text.rend() - std::find(start, text.rend(), '\n'));
        auto line_end = std::find(text.begin() + line_start, text.end(), '\n');
        if (line_start) { ++line_start; }  /* skip the \n if any */
        if (line_end != text.end()) { --line_end; }  /* skip the \n if any */
        std::string line(text.begin() + line_start, line_end);
        size_t line_no = 1 + std::count(text.begin(), text.begin() + line_start, '\n');
        size_t column_no = offset - line_start;
        MSG_ERROR("Error at line " << line_no << " column " << column_no << " (at token " << (*tok_i) << "):", "");
        MSG_ERROR(line, "");
        std::string padding;
        padding.reserve(line.size());
        for (size_t i = 0; i < column_no; ++i) {
            char c = line[i];
            if (c == '\t') {
                padding += '\t';
            } else {
                padding += ' ';
            }
        }
        padding += '^';
        MSG_ERROR(padding, "");
    }

    std::shared_ptr<Block> parse(const std::string& text, model_manager* mm)
    {
        auto state = m_toker(text);
        tok_i = std::begin(state);
        tok_end = std::end(state);
        std::shared_ptr<Block> ret = alloc<Block>();
        ret->create(type_model_manager, "");
        ret->create(type_ct, "Last computation");
        ret->create(type_ct, "Last computation max");
        ret->resolve("") = mm;
        /* FIXME! it's a double, not exactly a locus */
        ret->create(type_locus, "threshold");
        ret->resolve("threshold") = mm->threshold;
        try {
            parse_block(ret);
        }
        catch (ParseFinished) {}
        catch (SyntaxError se) {
            dump_error(text, se.offset);
            ret->statements.clear();
        }
        
        return ret;
    }

    ComputationType parse_test()
    { 
        std::string test_name = tok_i->text;
        expect(TestName);
        return str_to_test(test_name);
    }

    std::string parse_var_name()
    { 
        std::string var_name = tok_i->text;
        expect(Identifier);
        return var_name;
    }

    void parse_block(std::shared_ptr<Block> block)
    {
        while (parse_statement(block));
    }

    std::shared_ptr<Expression> parse_operand()
    {
        std::string tok = tok_i->text;
        if (accept(Number)) {
            std::stringstream ss(tok);
            double d;
            ss >> d;
            return alloc<Locus>(d);
        } else if (accept(Identifier)) {
            return alloc<SingleVar>(tok);
        } else if (accept(KeyWord, "max")) {
            expect(OpenPar);
            auto tn = parse_test();
            expect(ClosePar);
            return alloc<MaxResult>(tn);
        }
        throw 0;
    }

    std::shared_ptr<Expression> parse_expr()
    {
        std::shared_ptr<Expression> e;
        std::shared_ptr<Expression> op1, op2;
        op1 = parse_operand();
        if (accept(Plus)) {
            op2 = parse_operand();
            return alloc<LocusAdd>(op1, op2);
        } else if (accept(Minus)) {
            op2 = parse_operand();
            return alloc<LocusSub>(op1, op2);
        } else if (accept(KeyWord, "above")) {
            op2 = parse_operand();
            return alloc<Above>(op1, op2);
        }
        return op1;
    }

    bool parse_opt_if(bool ret, std::shared_ptr<Block> block)
    {
        if (accept(KeyWord, "if")) {
            std::shared_ptr<Expression> cond = parse_expr();
            std::shared_ptr<Statement> then = block->statements.back();
            block->statements.back() = alloc<If>(cond, then);
            ret = true;
        }
        expect(Punctuation);
        return ret;
    }

    void parse_output(std::shared_ptr<Output> o)
    {
        while (true) {
            std::string p = tok_i->text;
            if (accept(String)) {
                o->to_print.push_back(alloc<PrintString>(unescape(p)));
            } else if (accept(Keys)) {
                o->to_print.push_back(alloc<PrintKeys>());
            } else if (!accept(Printable)) {
                break;
            } else {
                if (p == "loci") { o->to_print.push_back(alloc<PrintLoci>());
                } else if (p == "scores") { o->to_print.push_back(alloc<PrintScores>());
                } else if (p == "X") { o->to_print.push_back(alloc<PrintX>());
                } else if (p == "XtX") { o->to_print.push_back(alloc<PrintXtX>());
                } else if (p == "XtXinv") { o->to_print.push_back(alloc<PrintXtX_inv>());
                } else if (p == "coefficients") { o->to_print.push_back(alloc<PrintCoefficients>());
                } else if (p == "residuals") { o->to_print.push_back(alloc<PrintResiduals>());
                } else if (p == "rank") { o->to_print.push_back(alloc<PrintRank>());
                }
            }
        }
    }

    bool parse_statement(std::shared_ptr<Block> block)
    {
        if (at_end()) {
            //throw ParseFinished();
            return false;
        } else if (accept(KeyWord, "for")) {
            std::string source = tok_i->text;
            expect(SourceName);
            expect(KeyWord, "as");
            std::string var_name = parse_var_name();
            std::shared_ptr<Source> src;
            if (source == "each-chromosome") { src = alloc<Chromosome>(); }
            else if (source == "each-selected-locus") { src = alloc<SelectedLocus>(); }
            std::shared_ptr<Block> inner_for = alloc<For>(block, src, var_name);
            expect(Punctuation);
            parse_block(inner_for);
            block->statements.push_back(inner_for);
            return true;
        } else if (accept(KeyWord, "with")) {
            std::string typ = tok_i->text;
            expect(TypeName);
            std::string name = parse_var_name();
            block->create(typ, name);
            if (accept(Equal)) {
                Variable& v = block->resolve(name);
                std::string text = tok_i->text;
                std::stringstream ss(text);
                switch (v.get_type()) {
                    case type_bool:
                        expect(Number);
                        ss >> v.data.b;
                        break;
                    case type_locus:
                        expect(Number);
                        ss >> v.data.loc;
                        break;
                    case type_chromosome:
                        expect(Identifier);
                        v.data.chrom = active_settings->find_chromosome(text);
                        break;
                    case type_filename:
                        {
                            filename_stream filename;
                            while (true) {
                                if (accept(String)) {
                                    filename << unescape(text);
                                } else if (accept(Keys)) {
                                    const model_manager& mm = block->resolve("");
                                    filename << mm.Mcurrent.keys();
                                } else {
                                    break;
                                }
                                text = tok_i->text;
                            }
                            v.reset(filename);
                        }
                        break;
                    default:
                        throw 0;
                };
            }
            return parse_opt_if(true, block);
        } else if (accept(KeyWord, "compute")) {
            ComputationType ct = parse_test();
            expect(KeyWord, "along");
            std::string chrom_name = tok_i->text;
            expect(Identifier);
            std::shared_ptr<Variable> vchrom = alloc<Variable>();
            if (!block->has(chrom_name)) {
                block->create(type_chromosome, chrom_name) = active_settings->find_chromosome(chrom_name);
            }
            std::shared_ptr<Expression> e1, e2;
            if (accept(KeyWord, "from")) {
                e1 = parse_expr();
                expect(KeyWord, "to");
                e2 = parse_expr();
            }
            block->statements.emplace_back(new Compute(ct, chrom_name, e1, e2));
            return parse_opt_if(true, block);
        } else if (accept(KeyWord, "set")) {
            std::string var_name = parse_var_name();
            expect(KeyWord, "to");
            std::shared_ptr<Expression> e = parse_expr();
            std::shared_ptr<Statement> st = alloc<Assign>(var_name, e);
            block->statements.push_back(st);
            return parse_opt_if(true, block);
        } else if (accept(KeyWord, "deselect")) {
            std::shared_ptr<Expression> e = parse_expr();
            std::shared_ptr<Statement> st = alloc<SelectionSub>(e);
            block->statements.push_back(st);
            return parse_opt_if(true, block);
        } else if (accept(KeyWord, "select")) {
            std::shared_ptr<Expression> e = parse_expr();
            std::shared_ptr<Statement> st = alloc<SelectionAdd>(e);
            block->statements.push_back(st);
            return parse_opt_if(true, block);
        } else if (accept(KeyWord, "next")) {
            expect(Identifier);
            return parse_opt_if(false, block);
        } else if (accept(KeyWord, "output")) {
            std::vector<std::string> to_print;
            if (accept(KeyWord, "file")) {
                /* R table mode */
                std::shared_ptr<TableOutput> to = alloc<TableOutput>();
                to->filename_var = parse_var_name();
                if (accept(KeyWord, "append")) {
                    to->append = true;
                } else {
                    to->append = false;
                }
                expect(KeyWord, "data");
                parse_output(to);
                block->statements.push_back(to);
            } else {
                /* stdout mode */
                std::shared_ptr<Print> o = alloc<Print>();
                parse_output(o);
                block->statements.push_back(o);
            }
            return parse_opt_if(true, block);
        }
        throw UnexpectedToken(tok_i, "for, with, compute, set, deselect, select, next, output.");
    }
};

} /* namespace script */

#endif

