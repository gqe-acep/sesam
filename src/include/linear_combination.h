/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_LINEAR_COMBINATION_H_
#define _SPEL_LINEAR_COMBINATION_H_

#include <list>
#include "eigen.h"
#include <unordered_map>

#define _EPSILON 1.e-10

template <typename V>
std::vector<V>
operator + (const std::vector<V>& u, const std::vector<V>& v)
{
    std::vector<V> ret(u.size() + v.size());
    auto it = std::set_union(u.begin(), u.end(), v.begin(), v.end(), ret.begin());
    ret.resize(it - ret.begin());
    return ret;
}


template <typename V>
std::vector<V>
operator - (const std::vector<V>& u, const std::vector<V>& v)
{
    std::vector<V> ret(u.size() + v.size());
    auto it = std::set_difference(u.begin(), u.end(), v.begin(), v.end(), ret.begin());
    ret.resize(it - ret.begin());
    return ret;
}


template <typename V>
std::vector<V>
operator % (const std::vector<V>& u, const std::vector<V>& v)
{
    std::vector<V> ret(std::min(u.size(), v.size()));
    auto it = std::set_intersection(u.begin(), u.end(), v.begin(), v.end(), ret.begin());
    ret.resize(it - ret.begin());
    return ret;
}




template <typename PARENT_TYPE, typename STATE_TYPE=size_t>
struct combination_type {
    static const size_t nopar = (size_t) -1;

    typedef combination_type<PARENT_TYPE, STATE_TYPE> this_type;

    typedef Eigen::Matrix<this_type, Eigen::Dynamic, 1> Vector;

    typedef std::map<PARENT_TYPE, Vector> expansion_map_type;

    struct key_type {
        /* TODO: parent MUST be const generation_rs* */
        PARENT_TYPE parent;
        STATE_TYPE state;

        key_type(PARENT_TYPE p, STATE_TYPE s) : parent(p), state(s) {}
        key_type() : parent(0), state(0) {}
        key_type& operator = (const key_type& k) { parent = k.parent; state = k.state; return *this; }
        bool operator == (const key_type& other) const { return parent == other.parent && state == other.state; }
        bool operator == (PARENT_TYPE x) const { return parent == x; }
        bool operator != (const key_type& other) const { return parent != other.parent || state != other.state; }
        bool operator != (PARENT_TYPE x) const { return parent != x; }
        bool operator < (const key_type& other) const { return parent < other.parent || (parent == other.parent && state < other.state); }

        friend std::ostream& operator << (std::ostream& os, const key_type& k)
        {
            return os << '{' << k.parent << ':' << k.state << '}';
        }
    };

    struct key_list {
        /* not sure about vector. maybe multiset? multimap<generation_rs, size_t>? */
        std::vector<key_type> keys;
        typedef typename std::vector<key_type>::iterator iterator;
        typedef typename std::vector<key_type>::const_iterator const_iterator;
        key_list(key_type&& k) : keys({std::move(k)}) {}
        key_list(const key_type& k) : keys({k}) {}
        key_list(size_t n) : keys(n) {}
        key_list(key_type&& k1, key_type&& k2) : keys({std::move(k1), std::move(k2)}) { std::sort(keys.begin(), keys.end()); }
        key_list(std::initializer_list<key_type>&& kl) : keys(kl) { std::sort(keys.begin(), keys.end()); }
        key_list() : keys() {}

        iterator begin() { return keys.begin(); }
        iterator end() { return keys.end(); }
        const_iterator begin() const { return keys.begin(); }
        const_iterator end() const { return keys.end(); }

        size_t size() const { return keys.size(); }

        bool
            operator == (const key_list& other) const
            {
                auto ai = keys.begin();
                auto aj = keys.end();
                auto bi = other.keys.begin();
                auto bj = other.keys.end();
                if (keys.size() != other.keys.size()) {
                    return false;
                }
                while (ai != aj && bi != bj && *ai == *bi) { ++ai; ++bi; }
                return ai == aj && bi == bj;
            }

        bool
            operator != (const key_list& other) const { return !((*this) == other); }

        bool
            operator < (const key_list& other) const
            {
                auto ai = keys.begin();
                auto aj = keys.end();
                auto bi = other.keys.begin();
                auto bj = other.keys.end();
                while (ai != aj && bi != bj && *ai == *bi) { ++ai; ++bi; }
                if (ai == aj) {
                    return bi != bj;
                } else {
                    if (bi == bj) {
                        return false;
                    }
                    return *ai < *bi;
                }
            }

        friend
            std::ostream&
            operator << (std::ostream& os, const key_list& kl)
            {
                auto ai = kl.keys.begin();
                auto aj = kl.keys.end();
                if (ai != aj) {
                    os << (*ai);
                    for(++ai; ai != aj; ++ai) {
                        os << '*' << (*ai);
                    }
                }
                return os;
            }

        friend
            key_list
            operator + (const key_list& k1, const key_list& k2)
            {
                key_list ret(k1.keys.size() + k2.keys.size());
                auto it = std::set_union(k1.keys.begin(), k1.keys.end(), k2.keys.begin(), k2.keys.end(), ret.keys.begin());
                ret.keys.resize(it - ret.keys.begin());
                return ret;
            }

        friend
            key_list
            operator - (const key_list& k1, const key_type& k_remove)
            {
                key_list ret;
                ret.keys.reserve(k1.keys.size() - 1);
                for (const auto& k: k1) {
                    if (k != k_remove) {
                        ret.keys.push_back(k);
                    }
                }
                return ret;
            }

        friend
            key_list
            operator % (const key_list& k1, const std::vector<PARENT_TYPE>& keep)
            {
                key_list ret;
                ret.keys.reserve(keep.size());
                for (const auto& k: k1) {
                    if (std::find(keep.begin(), keep.end(), k.parent) != keep.end()) {
                        ret.keys.push_back(k);
                    }
                }
                return ret;
            }

        friend
            key_list
            operator / (const key_list& k1, const std::vector<PARENT_TYPE>& discard)
            {
                key_list ret;
                ret.keys.reserve(k1.keys.size() - discard.size());
                auto ki = k1.begin();
                auto kj = k1.end();
                auto di = discard.begin();
                auto dj = discard.end();
                for (; ki != kj && di != dj; ++ki) {
                    if (ki->parent == *di) {
                        ++di;
                    } else if (ki->parent > *di) {
                        ++di;
                    } else {
                        ret.keys.push_back(*ki);
                    }
                }
                ret.keys.insert(ret.keys.end(), ki, kj);
                return ret;
            }

        std::pair<key_list, key_list>
            extract(const std::vector<PARENT_TYPE>& parents) const
            {
                std::pair<key_list, key_list> ret;
                ret.first.keys.reserve(parents.size());
                ret.second.keys.reserve(keys.size() - parents.size());
                for (const auto& k: keys) {
                    if (std::find(parents.begin(), parents.end(), k.parent) != parents.end()) {
                        ret.first.keys.push_back(k);
                    } else {
                        ret.second.keys.push_back(k);
                    }
                }
                return ret;
            }

        bool
            is_consistent() const
            {
                auto i1 = keys.begin(), i2 = i1, j = keys.end();
                for (; i1 != j; ++i1) {
                    i2 = i1;
                    for (++i2; i2 != j && i2->parent == i1->parent && i2->state == i1->state; ++i2);
                    if (i2 != j && i2->parent == i1->parent) {
                        MSG_DEBUG("INCONSISTENT KEYS " << keys);
                        return false;
                    }
                }
                return true;
            }
    };

    struct element_type {
        key_list keys;
        double coef;
        element_type(const key_type& k1, double c) : keys({k1}), coef(c) {}
        element_type(const key_list& k, double c) : keys(k), coef(c) {}
        element_type(key_list&& k, double c) : keys(std::move(k)), coef(c) {}
        element_type(const key_type& k1, const key_type& k2, double c) : keys({k1, k2}), coef(c) {}
        element_type(element_type&& e) : keys(std::move(e.keys)), coef(e.coef) {}
        element_type(const element_type& e) : keys(e.keys), coef(e.coef) {}
        element_type(double c) : keys(), coef(c) {}
        element_type() : keys(), coef(1) {}
        element_type& operator = (const element_type& e) { keys = e.keys; coef = e.coef; return *this; }
        element_type& operator = (element_type&& e) { keys = std::move(e.keys); coef = e.coef; return *this; }
        bool operator == (const element_type& other) const { return keys == other.keys && fabs(coef - other.coef) < _EPSILON; }
        bool operator != (const element_type& other) const { return keys != other.keys || coef != other.coef; }
        bool operator < (const element_type& other) const { return keys < other.keys; }

        friend
            std::ostream& operator << (std::ostream& os, const element_type& e)
            {
                os << e.coef;
                if (e.keys.keys.size()) {
                    os << '*' << e.keys;
                }
                return os;
            }

        friend
            element_type operator + (const element_type& e1, double coef)
            {
                return {e1.keys, e1.coef + coef};
            }

        friend
            element_type operator * (const element_type& e1, double coef)
            {
                return {e1.keys, e1.coef * coef};
            }

        friend
            element_type operator * (const element_type& e1, const element_type& e2)
            {
                element_type ret(e1.keys + e2.keys, e1.coef * e2.coef);
                /*if (!ret.keys.is_consistent()) {*/
                    /*ret.keys.keys.clear();*/
                    /*ret.coef = 0;*/
                /*}*/
                return ret;
            }

        element_type& operator *= (double d) { coef *= d; return *this; }

        std::pair<key_list, element_type>
            extract(const std::vector<PARENT_TYPE>& parents) const
            {
                auto kk = keys.extract(parents);
                return {kk.first, {kk.second, coef}};
            }

        bool is_consistent() const
        {
            return keys.is_consistent();
        }

        this_type operator | (const std::map<PARENT_TYPE, Vector>& expansions) const
        {
            MSG_DEBUG("================================================");
            MSG_DEBUG("expanding " << (*this));
            std::vector<key_type> common_parents;
            common_parents.reserve(keys.size());
            for (const auto& k: keys) {
                if (expansions.find(k.parent) != expansions.end()) {
                    common_parents.push_back(k);
                }
            }
            MSG_DEBUG("common parents " << common_parents);
            key_list tmp_key(keys.size() - common_parents.size());
            /*auto it = keys.begin();*/
            /*for (const auto& k: common_parents) {*/
                /*while (it->parent != k.parent) {*/
                    /*tmp_key.keys.push_back(k);*/
                    /*++it;*/
                /*}*/
            /*}*/
            std::set_difference(keys.begin(), keys.end(), common_parents.begin(), common_parents.end(), tmp_key.begin());
            this_type ret;
            ret.m_combination.emplace_back(tmp_key, coef);
            MSG_DEBUG("tmp_key " << tmp_key);
            for (const auto& p: common_parents) {
                const auto& lc = expansions.find(p.parent)->second(p.state);
                MSG_DEBUG("lc = " << lc);
                ret *= lc;
                /*this_type tmp;*/
                /*for (const auto& e: lc) {*/
                    /*tmp.m_combination.emplace_back(tmp_key + e.keys, coef * e.coef);*/
                /*}*/
                /*ret *= tmp;*/
            }
            std::sort(ret.begin(), ret.end());
            MSG_DEBUG("expansion " << ret);
            return ret;
        }
    };

    typedef std::vector<element_type> sum_type;
    typedef typename std::vector<element_type>::iterator sum_iterator_type;
    typedef typename std::vector<element_type>::const_iterator sum_const_iterator_type;

    sum_type m_combination;

    friend std::ostream& operator << (std::ostream& os, const combination_type<PARENT_TYPE, STATE_TYPE>& c)
    {
        auto i = c.begin();
        auto j = c.end();
        if (i != j) {
            os << (*i++);
            for (; i != j; ++i) {
                os << " + " << (*i);
            }
        }
        return os;
    }

    combination_type()
        : m_combination()
    {}

    combination_type(const this_type& other)
        : m_combination(other.m_combination)
    {}

    combination_type(this_type&& other)
        : m_combination(std::move(other.m_combination))
    {}

    this_type&
        operator = (const this_type& other)
        {
            m_combination = other.m_combination;
            return *this;
        }

    this_type&
        operator = (this_type&& other)
        {
            m_combination = std::move(other.m_combination);
            return *this;
        }

    combination_type(PARENT_TYPE par, size_t n_states)
        : m_combination()
    {
        for (size_t i = 0; i < n_states; ++i) {
            m_combination.emplace_back(key_type{par, i}, 1.);
        }
    }

    combination_type(double coef)
        : m_combination()
    {
        m_combination.emplace_back(coef);
    }

    element_type&
        operator [] (size_t i) { return m_combination[i]; }

    const element_type&
        operator [] (size_t i) const { return m_combination[i]; }

    sum_iterator_type begin() { return m_combination.begin(); }
    sum_iterator_type end() { return m_combination.end(); }
    sum_const_iterator_type begin() const { return m_combination.begin(); }
    sum_const_iterator_type end() const { return m_combination.end(); }

    size_t size() const { return m_combination.size(); }

    bool not_empty() const { return m_combination.size() > 0; }

    bool is_consistent() const
    {
        for (const auto& e: m_combination) {
            if (!e.is_consistent()) {
                return false;
            }
        }
        return true;
    }

    this_type&
        operator *= (double d)
        {
            for (auto& e: m_combination) {
                e.coef *= d;
            }
            return *this;
        }

    this_type operator + (const this_type& c) const
    {
        this_type ret;
        auto ai = m_combination.cbegin();
        auto aj = m_combination.cend();
        auto bi = c.m_combination.cbegin();
        auto bj = c.m_combination.cend();
        while (true) {
            if (bi == bj || ai == aj) { break; }
            /*MSG_DEBUG("a=" << (*ai) << " b=" << (*bi));*/
            /*if (ai->parent == bi->parent && ai->state != bi->state) {
                ret.m_combination = {*ai, *bi};
                break;
            } else*/
            if (*ai < *bi) { /*MSG_DEBUG("a<b");*/ if (ai->coef > _EPSILON || ai->coef < -_EPSILON) { ret.m_combination.push_back(*ai); } ++ai; }
            else if (*bi < *ai) { /*MSG_DEBUG("b<a");*/ if (bi->coef > _EPSILON || ai->coef < -_EPSILON) { ret.m_combination.push_back(*bi); } ++bi; }
            else {
                /*MSG_DEBUG("a=b");*/
                auto tmp = (*ai) + bi->coef;
                if (tmp.coef > _EPSILON || tmp.coef < -_EPSILON) {
                    ret.m_combination.emplace_back(tmp);
                }
                ++ai;
                ++bi;
            }
        }
        for (; ai != aj; ++ai) { if (ai->coef) { ret.m_combination.push_back(*ai); } }
        for (; bi != bj; ++bi) { if (bi->coef) { ret.m_combination.push_back(*bi); } }
        return ret;
    }

    this_type operator * (const this_type& c) const
    {
        this_type ret;
        for (const auto& e1: *this) {
            for (const auto& e2: c) {
                double c = e1.coef * e2.coef;
                if (!c) { continue; }
                ret.m_combination.emplace_back(e1 * e2);
            }
        }
        return ret;
    }

    this_type& operator += (const element_type& c)
    {
        this_type cc;
        cc.m_combination.emplace_back(c);
        return *this += cc;
    }

    this_type& operator += (const this_type& c)
    {
        this_type tmp = (*this) + c;
        m_combination.swap(tmp.m_combination);
        return *this;
    }

    this_type& operator *= (const this_type& c)
    {
        this_type tmp = (*this) * c;
        m_combination.swap(tmp.m_combination);
        return *this;
    }

    this_type
        operator | (const expansion_map_type& expansions) const
        {
            this_type ret;
            /*bool modified;*/
            /*do {*/
                /*modified = false;*/
                for (const auto& e: m_combination) {
                    this_type tmp = e | expansions;
                    /*modified |= (tmp.size() != 1 || tmp.m_combination.front() != e);*/
                    ret += tmp;
                    /*MSG_DEBUG("tmp=" << tmp);*/
                    /*MSG_DEBUG("e=" << e);*/
                    /*MSG_DEBUG("ret=" << ret);*/
                }
                /*MSG_DEBUG("modified=" << modified);*/
            /*} while (modified);*/
            return ret;
        }

    friend
        this_type operator * (const element_type& e, const this_type& lc)
        {
            this_type ret;
            ret.reserve(lc.m_combination.size());
            for (const auto& e2: lc.m_combination) {
                ret.emplace_back(e * e2);
            }
            return ret;
        }

    double apply(const std::map<PARENT_TYPE, VectorXd>& parent_states) const
    {
        double accum = 0;
        for (const auto& e: m_combination) {
            double tmp = e.coef;
            for (const key_type& k: e.keys) {
                MSG_DEBUG("k.parent=" << k.parent << " k.state=" << k.state);
                MSG_QUEUE_FLUSH();
                tmp *= parent_states.find(k.parent)->second(k.state);
            }
            accum += tmp;
        }
        return accum;
    }

    bool operator == (const combination_type<PARENT_TYPE, STATE_TYPE>& other) const { return m_combination == other.m_combination; }
};

template <typename PARENT_TYPE, typename STATE_TYPE>
typename combination_type<PARENT_TYPE, STATE_TYPE>::Vector
operator | (const typename combination_type<PARENT_TYPE, STATE_TYPE>::Vector& V,
            const std::map<PARENT_TYPE, typename combination_type<PARENT_TYPE, STATE_TYPE>::Vector>& expansions)
{
    typename combination_type<PARENT_TYPE, STATE_TYPE>::Vector ret(V.size());
    for (int i = 0; i < V.size(); ++i) {
        ret(i) = V(i) | expansions;
    }
    return ret;
}




template <typename PARENT_TYPE, typename STATE_TYPE>
std::vector<PARENT_TYPE>
get_parents(const combination_type<PARENT_TYPE, STATE_TYPE>& comb)
{
    std::vector<PARENT_TYPE> ret;
    if (!comb.m_combination.size()) {
        return {};
    }
    for (const auto& k: comb.begin()->keys) {
        ret.push_back(k.parent);
    }
    return ret;
}


template <typename PARENT_TYPE, typename STATE_TYPE>
combination_type<PARENT_TYPE, STATE_TYPE>
state_to_combination(PARENT_TYPE parent, const std::vector<STATE_TYPE>& states)
{
    typedef typename combination_type<PARENT_TYPE, STATE_TYPE>::key_type key_type;
    combination_type<PARENT_TYPE, STATE_TYPE> ret;
    ret.m_combination.reserve(states.size());
    for (const auto& s: states) {
        ret.m_combination.emplace_back(key_type{parent, s}, 1.);
    }
    return ret;
}


template <typename PARENT_TYPE, typename STATE_TYPE>
combination_type<PARENT_TYPE, STATE_TYPE>
hadamard(const combination_type<PARENT_TYPE, STATE_TYPE>& c1, const combination_type<PARENT_TYPE, STATE_TYPE>& c2)
{
    auto i1 = c1.m_combination.begin();
    auto i2 = c2.m_combination.begin();
    auto j = c1.m_combination.end();
    /* assume c1.size() == c2.size() */
    combination_type<PARENT_TYPE, STATE_TYPE> ret;
    ret.m_combination.reserve(c1.m_combination.size());
    for (; i1 != j; ++i1, ++i2) {
        ret.m_combination.emplace_back((*i1) * (*i2));
    }
    return ret;
}


template <typename PARENT_TYPE, typename STATE_TYPE>
std::map<typename combination_type<PARENT_TYPE, STATE_TYPE>::key_list, combination_type<PARENT_TYPE, STATE_TYPE>>
sum_over(const combination_type<PARENT_TYPE, STATE_TYPE>& comb, const std::vector<PARENT_TYPE>& parents)
{
    std::map<typename combination_type<PARENT_TYPE, STATE_TYPE>::key_list, combination_type<PARENT_TYPE, STATE_TYPE>> ret;

    for (const auto& e: comb) {
        auto ke = e.extract(parents);
        ret[ke.first] += ke.second;
    }

    return ret;
}


template <typename PARENT_TYPE, typename STATE_TYPE>
combination_type<PARENT_TYPE, STATE_TYPE>
normalize_over(const combination_type<PARENT_TYPE, STATE_TYPE>& comb, const std::vector<PARENT_TYPE>& variables)
{
    typedef typename combination_type<PARENT_TYPE, STATE_TYPE>::key_list key_list;
    combination_type<PARENT_TYPE, STATE_TYPE> ret;
    std::unordered_map<key_list, double> norm_coef;
    for (const auto& e: comb) {
        auto k = e.keys % variables;
        norm_coef[k] += e.coef;
    }
    /*MSG_DEBUG("NORM FACTORS: " << norm_coef);*/
    for (auto& kv: norm_coef) { kv.second = 1. / kv.second; }
    for (const auto& e: comb) {
        auto k = e.keys % variables;
        ret.m_combination.emplace_back(e.keys, e.coef * norm_coef[k]);
    }
    return ret;
}


template <typename PARENT_TYPE, typename STATE_TYPE>
std::map<typename combination_type<PARENT_TYPE, STATE_TYPE>::key_list, double>
extract_probability_of(const combination_type<PARENT_TYPE, STATE_TYPE>& table, const std::vector<PARENT_TYPE>& queried)
{
    std::map<typename combination_type<PARENT_TYPE, STATE_TYPE>::key_list, double> ret;
    for (const auto& e: table) {
        ret[e.keys % queried] += e.coef;
    }
    return ret;
}


template <typename PARENT_TYPE, typename STATE_TYPE>
combination_type<PARENT_TYPE, STATE_TYPE>
express_conditional_probability(const combination_type<PARENT_TYPE, STATE_TYPE>& table, const std::vector<PARENT_TYPE>& expressed)
{
    /*
     * Compute P(expressed / other) = P(other / expressed) * P(expressed) / P(other)
     * Assuming the table is P(other / expressed).
     * If the table is P(other, expressed), TODO check math
     * If the table is P(something / something else), TODO check math
     */
    typedef typename combination_type<PARENT_TYPE, STATE_TYPE>::key_list key_list;
    auto condition = get_parents(table) - expressed;
    /*std::map<key_list, double> prob_expressed = extract_probability_of(table, expressed);*/
    std::map<key_list, double> prob_condition = extract_probability_of(table, condition);
    for (auto& kv: prob_condition) { kv.second = 1. / kv.second; }
//     double n = 0;
    /*for (const auto& kv: prob_expressed) { n += kv.second; }*/
    /*n = 1. / n;*/
    /*for (auto& kv: prob_expressed) { kv.second *= n; }*/
    combination_type<PARENT_TYPE, STATE_TYPE> output;
    output.m_combination.reserve(table.size());
    for (const auto& e: table) {
        /*output.m_combination.emplace_back(e.keys, e.coef * prob_expressed[e.keys % expressed] * prob_condition[e.keys % condition]);*/
        output.m_combination.emplace_back(e.keys, e.coef * prob_condition[e.keys % condition]);
    }
    return output;
}


template <typename PARENT_TYPE, typename STATE_TYPE>
std::map<typename combination_type<PARENT_TYPE, STATE_TYPE>::key_list, combination_type<PARENT_TYPE, STATE_TYPE>>
sum_over_dual(const combination_type<PARENT_TYPE, STATE_TYPE>& comb, const std::vector<PARENT_TYPE>& parents)
{
    std::map<typename combination_type<PARENT_TYPE, STATE_TYPE>::key_list, combination_type<PARENT_TYPE, STATE_TYPE>> ret;

    for (const auto& e: comb) {
        auto ke = e.extract(parents);
        /*ret[ke.first] += ke.second;*/
        ret[ke.second.keys] += {ke.first, ke.second.coef};
    }

    /*MSG_DEBUG("sum_over_dual(" << comb << ", " << parents << ") => " << ret);*/

    return ret;
}


#if 0
template <typename PARENT_TYPE, typename STATE_TYPE>
std::map<typename combination_type<PARENT_TYPE, STATE_TYPE>::key_list, combination_type<PARENT_TYPE, STATE_TYPE>>
sum_over_and_normalize(const combination_type<PARENT_TYPE, STATE_TYPE>& comb, const std::vector<PARENT_TYPE>& parents)
{
    std::map<typename combination_type<PARENT_TYPE, STATE_TYPE>::key_list, combination_type<PARENT_TYPE, STATE_TYPE>> ret;

    std::map<typename combination_type<PARENT_TYPE, STATE_TYPE>::key_list, double> norm;

    for (const auto& e: comb) {
        auto ke = e.extract(parents);
        norm[ke.second.keys] += e.coef;
    }

    for (auto& kv: norm) { kv.second = 1. / kv.second; }

    for (const auto& e: comb) {
        auto ke = e.extract(parents);
        ret[ke.first] += ke.second * norm[ke.second.keys];
    }

    return ret;
}
#endif

template <typename PARENT_TYPE, typename STATE_TYPE>
combination_type<PARENT_TYPE, STATE_TYPE>
fold(const std::map<typename combination_type<PARENT_TYPE, STATE_TYPE>::key_list, combination_type<PARENT_TYPE, STATE_TYPE>>& comb_map)
{
    combination_type<PARENT_TYPE, STATE_TYPE> ret;
    for (const auto& kv: comb_map) {
        ret += kv.second;
    }
    return ret;
}


template <typename PARENT_TYPE, typename STATE_TYPE>
combination_type<PARENT_TYPE, STATE_TYPE>
project(const combination_type<PARENT_TYPE, STATE_TYPE>& comb, const std::vector<PARENT_TYPE>& project_variables, const std::vector<PARENT_TYPE>& norm_variables)
{
    if (norm_variables.size()) {
        return fold(sum_over_dual(normalize_over(comb, norm_variables), project_variables));
    } else {
        return fold(sum_over_dual(comb, project_variables));
    }
}


template <typename PARENT_TYPE, typename STATE_TYPE>
combination_type<PARENT_TYPE, STATE_TYPE>
project_dual(const combination_type<PARENT_TYPE, STATE_TYPE>& comb, const std::vector<PARENT_TYPE>& project_variables, const std::vector<PARENT_TYPE>& norm_variables)
{
    return fold(sum_over_dual(normalize_over(comb, norm_variables), project_variables));
}



template <typename PARENT_TYPE, typename STATE_TYPE>
combination_type<PARENT_TYPE, STATE_TYPE>
kronecker(const combination_type<PARENT_TYPE, STATE_TYPE>& c1, const combination_type<PARENT_TYPE, STATE_TYPE>& c2)
{
    /*typedef typename combination_type<PARENT_TYPE, STATE_TYPE>::key_type key_type;*/
    typedef typename combination_type<PARENT_TYPE, STATE_TYPE>::key_list key_list;
    typedef typename combination_type<PARENT_TYPE, STATE_TYPE>::element_type element_type;
    combination_type<PARENT_TYPE, STATE_TYPE> ret;

#if 1
    std::vector<PARENT_TYPE> I1, I2, inter, dangling;

    /*I1.reserve(c1.m_combination.front().keys.size());*/
    /*I2.reserve(c2.m_combination.front().keys.size());*/
    /*inter.resize(std::min(c1.m_combination.front().keys.size(), c2.m_combination.front().keys.size()));*/
    /*dangling.resize(c2.m_combination.front().keys.size());*/

    /* compute I1, I2, and I1 n I2 */
    I1 = get_parents(c1);
    I2 = get_parents(c2);
    inter = I1 % I2;
    dangling = I2 - I1;

    auto prob_dangling = extract_probability_of(c2, dangling);
    auto c2_inner = sum_over(c2, dangling);

    struct sub_kron_right_operand {
        key_list dangling;
        double dangling_coef;
        std::vector<element_type> tables;
        combination_type<PARENT_TYPE, STATE_TYPE> raw_result;
    };

    /* precompute kronecker_delta ... */
    std::map<key_list, std::pair<std::vector<element_type>, std::map<key_list, std::vector<element_type>>>> sub_kron;
    /*std::map<key_list, double> common_prob;*/
    /*std::map<key_list, double> common_prob2;*/
    /*double norm1, norm2;*/

    key_list common_key, dangling_key;

    /* ... on c1 */
    for (const auto& e: c1) {
        element_type sub(e.coef);
        sub.keys.keys.reserve(e.keys.keys.size() - inter.size());
        common_key.keys.clear();
        common_key.keys.reserve(inter.size());
        for (const auto& k: e.keys.keys) {
            if (std::find(inter.begin(), inter.end(), k.parent) != inter.end()) {
                common_key.keys.emplace_back(k);
            } else {
                sub.keys.keys.emplace_back(k);
            }
        }
        /*common_prob[common_key] += e.coef;*/
        sub_kron[common_key].first.emplace_back(sub);
    }

    /* ... on c2 */
    for (const auto& e: c2) {
        element_type sub(e.coef);
        sub.keys.keys.reserve(e.keys.keys.size() - inter.size());
        common_key.keys.clear();
        dangling_key.keys.clear();
        common_key.keys.reserve(inter.size());
        dangling_key.keys.reserve(dangling.size());
        for (const auto& k: e.keys.keys) {
            if (std::find(inter.begin(), inter.end(), k.parent) != inter.end()) {
                common_key.keys.emplace_back(k);
            } else if (std::find(dangling.begin(), dangling.end(), k.parent) != dangling.end()) {
                dangling_key.keys.emplace_back(k);
                sub.keys.keys.emplace_back(k);
            } else {
                sub.keys.keys.emplace_back(k);
            }
        }
        sub.coef /= prob_dangling[dangling_key];
        /*common_prob2[common_key] += e.coef;*/
        sub_kron[common_key].second[dangling_key].emplace_back(sub);
    }

    /*norm1 = 0; for (const auto& kv: common_prob) { norm1 += kv.second; }*/
    /*norm2 = 0; for (const auto& kv: common_prob2) { norm2 += kv.second; }*/

    /* engage paranoid mode */
    /* {
        auto i1 = common_prob.begin(), j1 = common_prob.end();
        auto i2 = common_prob2.begin(), j2 = common_prob2.end();
        for (; i1 != j1 && i2 != j2 && fabs(i1->second / norm1 - i2->second / norm2) < _EPSILON; ++i1, ++i2);
        if (i1 != j1 || i2 != j2) {
            MSG_DEBUG("Dissimilar probabilities in common variables " << common_prob << " vs " << common_prob2);
        }
        i1 = common_prob.begin();
        auto ski = sub_kron.begin(), skj = sub_kron.end();
        for (; ski != skj && i1->first == ski->first; ++ski, ++i1);
        if (ski != skj) {
            MSG_DEBUG("Dissimilar keys in common stuff");
        }
    }

    {
        auto i = sub_kron.begin(), j = sub_kron.end();
        auto norm = common_prob.begin();
        for (; i != j; ++i, ++norm) {
            double n1 = 1. / (norm1 * common_prob[i->first]);
            double n2 = 1. / (norm2 * common_prob2[i->first]);
            for (auto& t: i->second.first) { t *= n1; }
            for (auto& t: i->second.second) { t *= n2; }
        }
    } */

    size_t total_size = 0;

    for (const auto& kv: sub_kron) {
        total_size += kv.second.first.size() * kv.second.second.size();
    }

    /* perform kronecker per subset */

    ret.m_combination.reserve(total_size);
    for (const auto& kv: sub_kron) {
        element_type e0(1.);
        e0.keys = kv.first;
        /*double norm = common_prob[kv.first];*/
        for (const auto& e1: kv.second.first) {
            for (const auto& dang: kv.second.second) {
                for (const auto& e2: dang.second) {
                    ret.m_combination.emplace_back(e0 * e1 * e2 * prob_dangling[dang.first]);
                    /*ret.m_combination.emplace_back(e0 * e1 * e2 * norm);*/
                    /*ret += e0 * e1 * e2 * norm;*/
                }
            }
        }
    }

    std::sort(ret.begin(), ret.end());

    /*MSG_DEBUG("kronecker");*/
    /*MSG_DEBUG("  c1    " << c1);*/
    /*MSG_DEBUG("x c2    " << c2);*/
    /*MSG_DEBUG("   =    " << ret);*/
#endif

    return ret;
}




namespace Eigen {
    template<typename PARENT_TYPE>
    struct NumTraits<combination_type<PARENT_TYPE>> {
        typedef combination_type<PARENT_TYPE> Real;
        typedef combination_type<PARENT_TYPE> NonInteger;
        typedef combination_type<PARENT_TYPE> Nested;
        typedef combination_type<PARENT_TYPE> Literal;
        enum {
            IsComplex = 0,
            IsInteger = 1,
            ReadCost = 10,
            WriteCost = 10,
            MulCost = 100,
            AddCost = 1000,
            IsSigned = 0,
            RequireInitialization = 1
        };
        static Real epsilon() { return {0.}; }
        static Real dummy_precision() { return {0.}; }
        static combination_type<PARENT_TYPE> highest() { return {1.e308}; }
        static combination_type<PARENT_TYPE> lowest() { return {0.}; }
    };
}


/*namespace impl { struct generation_rs; }*/

typedef combination_type<size_t> gencomb_type;


typedef gencomb_type::Vector VectorLC;

inline
VectorLC
make_parent_comb(size_t par, size_t n_states)
{
    VectorLC ret(n_states);
    for (size_t i = 0; i < n_states; ++i) {
        ret(i, 0).m_combination.emplace_back(gencomb_type::key_type{par, i}, 1.);
    }
    /*MSG_DEBUG("make_parent_comb");*/
    /*MSG_DEBUG(ret);*/
    return ret;
}

inline
std::vector<gencomb_type>
make_parent_comb_vec(size_t par, size_t n_states)
{
    std::vector<gencomb_type> ret(n_states);
    for (size_t i = 0; i < n_states; ++i) {
        ret[i].m_combination.emplace_back(gencomb_type::key_type{par, i}, 1.);
    }
    /*MSG_DEBUG("make_parent_comb");*/
    /*MSG_DEBUG(ret);*/
    return ret;
}

/*inline*/
/*VectorLC*/
/*expand(const VectorLC& V, */

#endif

