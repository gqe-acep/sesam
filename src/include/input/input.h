/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_INPUT_BASE_H_
#define _SPEL_INPUT_BASE_H_

#include <exception>
#include <sstream>
#include "file.h"

struct input_exception : public file::error {
    std::string msg;
#if 0
    input_exception(std::istream& is, const std::string& w)
        : file::error()
    {
        int L = 1;
        int C = 1;
        long ofs = is.tellg();
        long last_line_ofs = 0;
        is.seekg(0, is.beg);
        while (is.tellg() != ofs) {
            switch(is.get()) {
                /* FIXME : for a mac, it's \r. For unix and windows, \n is enough. */
                case '\n':
                    ++L;
                    last_line_ofs = is.tellg();
                    break;
                default:;
            };
        }
        C = ofs - last_line_ofs;
        std::stringstream buf;
        buf << "at line " << L << " column " << C << ": " << w;
        msg = buf.str();
    }
#endif
    input_exception(file& is, const std::string& w)
        : file::error()
    {
        int L = 1;
        int C = 1;
        long ofs = is.tellg();
        long last_line_ofs = 0;
        is.seekg(0, is.beg);
        while (is.tellg() != ofs) {
            switch(is.get()) {
                /* FIXME : for a mac, it's \r. For unix and windows, \n is enough. */
                case '\n':
                    ++L;
                    last_line_ofs = is.tellg();
                    break;
                default:;
            };
        }
        C = ofs - last_line_ofs;
        std::stringstream buf;
        buf << "While reading file '" << is.m_path << "': ";
        buf << "at line " << L << " column " << C << ": " << w;
        msg = buf.str();
    }
    ~input_exception() throw() {}
    const char* what() const throw()
    {
        return msg.c_str();
    }
};

namespace read_data {

inline void ws_nonl(file& is)
{
    while (true) {
        switch(is.peek()) {
            case ' ':
            case '\t':
                (void)is.get();
                break;
            case '\r':
            case '\n':
                throw input_exception(is, "unexpected end of line");
            default:
                return;
        };
    }
}


inline void ws_nl(file& is)
{
    bool nl_ok = false;
    while (true) {
        switch(is.peek()) {
            case '\r':
            case '\n':
                nl_ok = true;
                (void)is.get();
                break;
            case ' ':
            case '\t':
                (void)is.get();
                break;
            default:
                if (!nl_ok) {
                    throw input_exception(is, "expected end of line");
                }
                return;
        };
    }
}


inline bool ws_opt_nl(file& is)
{
    bool nl_ok = false;
    while (!is.eof()) {
        switch(is.peek()) {
            case '\r':
            case '\n':
            case EOF:
                nl_ok = true;
            case ' ':
            case '\t':
                (void)is.get();
                break;
            default:
                return nl_ok;
        };
    }
    return nl_ok;
}


inline std::string read_star_name(file& is) {
    std::string ret;
    if (is.eof()) {
        throw input_exception(is, "unexpected end of file");
    }
    char c = is.get();
    if (c == '#') {
        while (is.get() != '\n');
        if (is.eof()) {
            throw input_exception(is, "unexpected end of file");
        }
        c = is.get();
    }
    if (c != '*') {
        throw input_exception(is, "expected a *");
    }
    is >> ret;
    if (ret.size() == 0) {
        throw input_exception(is, "expected a name after *");
    }
    return ret;
}

}

#endif

