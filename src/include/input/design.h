/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_DESIGN_H_
#define _SPEL_DESIGN_H_

#include <map>
#include <vector>
#include <set>
#include "geno_matrix.h"
#include "file.h"

struct ancestor_profile {
    std::vector<std::pair<char, char>> genotype_tags;
};

struct design_type {
    std::string filename;
    std::map<std::string, std::shared_ptr<geno_matrix>> generation;
    std::map<std::string, ancestor_profile> profiles;
    design_type()
        : filename(), generation(), profiles()
    { __current() = this; }

    static design_type*& __current() { static design_type* cur = NULL; return cur; }

    std::vector<std::string> ancestor_names() const
    {
        std::vector<std::string> ret;
        /*for (const auto& kv: generation) {*/
            /* FIXME */
            /*if (kv.second->design->is_ancestor) {*/
                /*ret.push_back(kv.first);*/
            /*}*/
        /*}*/
        return ret;
    }
};

design_type* read_design(file& is);

inline std::ostream& operator << (std::ostream& os, const design_type& d)
{
    os << "Design (" << d.filename << ')' << std::endl;
    auto g = d.generation.begin();
    auto gend = d.generation.end();
    for (; g != gend; ++g) {
        os << (*g->second) << std::endl;
    }
    return os;
}

#endif

