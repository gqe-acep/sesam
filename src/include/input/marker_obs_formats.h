/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//
// Created by daleroux on 22/11/16.
//

#ifndef SPELL_QTL_MARKER_OBS_FORMATS_H_H
#define SPELL_QTL_MARKER_OBS_FORMATS_H_H

#include <string>
#include <fstream>
#include <pedigree.h>
#include <regex>
#include <commandline.h>
#include "settings.h"
#include "../../3rd-party/json.hpp"
#include "error.h"
#include "observations.h"

/* NOTE POUR SJ : LES FORMATS SONT ICI */

struct marker_obs_formats {
    static
    nlohmann::json&
    get_internal_formats_json() {
        static std::string formats_str = R"(
{
    "02": {
        "domain": "allele",
        "alphabet_from": "01",
        "scores": {
            "0": ["00"],
            "1": ["01", "10"],
            "2": ["11"],
            "-": ["00", "01", "10", "11"]
        }
    },
    "ABHCD": {
        "domain": "ancestor",
        "alphabet_from": "ab",
        "scores": {
            "A": ["aa"],
            "H": ["ab", "ba"],
            "B": ["bb"],
            "-": ["aa", "ab", "ba", "bb"],
            "C": ["ab", "ba", "bb"],
            "D": ["aa", "ab", "ba"]
        }
    },
    "CP": {
        "domain": "ancestor",
        "alphabet_from": "abcd",
        "scores": {
            "0": ["ac", "ad", "bc", "bd"],
            "1": ["ac"],
            "2": ["ad"],
            "3": ["ac", "ad"],
            "4": ["bc"],
            "5": ["ac", "bc"],
            "6": ["ad", "bc"],
            "7": ["ac, ad", "bc"],
            "8": ["bd"],
            "9": ["ac", "bd"],
            "A": ["ad", "bd"],
            "B": ["ac", "ad", "bd"],
            "C": ["bc", "bd"],
            "D": ["ac", "bc", "bd"],
            "E": ["ad", "bc", "bd"],
            "F": ["ac", "ad", "bc", "bd"],
            "a": ["ad", "bd"],
            "b": ["ac", "ad", "bd"],
            "c": ["bc", "bd"],
            "d": ["ac", "bc", "bd"],
            "e": ["ad", "bc", "bd"],
            "f": ["ac", "ad", "bc", "bd"],
            "-": ["ac", "ad", "bc", "bd"]
        }
    }
})";
        static nlohmann::json formats = nlohmann::json::parse(formats_str);
        return formats;
    }


    static
    std::string
    build_all_internal_format_names()
    {
        std::vector<std::string> tmp;
        const auto& json = get_internal_formats_json();
        for (auto it = json.begin(); it != json.end(); ++it) {
            tmp.push_back(it.key());
        }
        std::stringstream ss;
        auto i = tmp.begin(), j = tmp.end();
        ss << (*i++);
        for (; i != j; ++i) {
            ss << ' ' << (*i);
        }
        return ss.str();
    }

    static
    void
    add_format(std::ifstream& ifs)
    {
        nlohmann::json fmt = nlohmann::json::parse(ifs);
        nlohmann::json& internal = get_internal_formats_json();
        MSG_DEBUG("INTERNAL " << internal.dump());
        MSG_QUEUE_FLUSH();

        for (auto it = fmt.begin(); it != fmt.end(); ++it) {
            std::string key = it.key();
            if (internal.count(key) == 0) {
                internal[key] = it.value();
//                internal += {it.key(), it.value()};
            } else {
                MSG_ERROR("Format " << it.key() << " is already defined.", "");
            }
        }
    }

    static
    const std::string&
    all_internal_format_names()
    {
        static std::string ret(build_all_internal_format_names());
        return ret;
    }

    static
    nlohmann::json
    get_format_json(const std::string& key)
    {
        const nlohmann::json& formats = get_internal_formats_json();
        auto it = formats.find(key);
        if (it == formats.end()) {
            MSG_ERROR("Marker observation format is not defined: " << key << ". Use one of the predefined formats " << build_all_internal_format_names() << " or add a -mos <format-filename> argument to the commandline.", "");
//             MSG_ERROR("Marker observation format is not defined: " << key, SPELL_STRING("Use one of the predefined formats " << build_all_internal_format_names() << " or add a -mos <format-filename> argument to the commandline."));
            return {};
        }
        return it.value();
    }

    static
    marker_observation_spec
    make_observation_spec(nlohmann::json format) {
        marker_observation_spec ret;
        ret.domain = format["domain"] == "ancestor" ? ODAncestor : ODAllele;
        ret.domain_size = format["alphabet_from"].size();
        const auto& fmt_scores = format["scores"];
        for (auto it = fmt_scores.begin(); it != fmt_scores.end(); ++it) {
            ret.scores.emplace_back();
            auto& score = ret.scores.back();
            score.first = it.key()[0];
//             MSG_INFO("score " << score.first);
            for (const auto& sc: it.value()) {
                std::string s = sc;
                score.second.emplace_back(s[0], s[1]);
            }
    //        MSG_INFO("score " << score.first << " // " << score.second);
        }
//         MSG_INFO(ret);
        return ret;
    }

    static
    marker_observation_spec
    get_format(const pedigree_type& ped, const std::string& format_name)
    {
//        MSG_DEBUG("Requested format " << format_name);
//        MSG_QUEUE_FLUSH();
        auto sep = format_name.find('/');
        if (sep != std::string::npos) {
            std::smatch m;
            int i1, i2;
            char l1, l2;
            if (std::regex_match(format_name, m, std::regex("^([0-9]+)/([0-9]+)$"))) {
                /* fetch letters by ancestor id number */
                int v1 = to<int>(m[1].str());
                int v2 = to<int>(m[2].str());
//                MSG_DEBUG("Matched ancestor IDs " << v1 << " and " << v2);
                i1 = (int) (std::find_if(ped.items.begin(), ped.items.end(), [&] (const pedigree_item& i) { return i.id == v1; }) - ped.items.begin());
                i2 = (int) (std::find_if(ped.items.begin(), ped.items.end(), [&] (const pedigree_item& i) { return i.id == v2; }) - ped.items.begin());

            } else {
                std::string name1(format_name.begin(), format_name.begin() + sep);
                std::string name2(format_name.begin() + sep + 1, format_name.end());
//                MSG_DEBUG("Looking for ancestors named " << name1 << " and " << name2);
                i1 = (int) (std::find_if(ped.items.begin(), ped.items.end(), [&] (const pedigree_item& i) { return i.gen_name == name1; }) - ped.items.begin());
                i2 = (int) (std::find_if(ped.items.begin(), ped.items.end(), [&] (const pedigree_item& i) { return i.gen_name == name2; }) - ped.items.begin());
            }
//            MSG_DEBUG("Have ancestor numbers " << i1 << " and " << i2);
//            for (const auto& kv: ped.ancestor_letters) {
//                MSG_DEBUG(kv.first << ':' << kv.second);
//            }
            l1 = ped.ancestor_letters.find(i1)->second;
            l2 = ped.ancestor_letters.find(i2)->second;
            nlohmann::json json, src = get_format_json("ABHCD");
            json["domain"] = src["domain"];
            json["alphabet_from"] = (l1 < l2 ? SPELL_STRING(l1 << l2) : SPELL_STRING(l2 << l1));
            json["scores"] = nullptr;
            const auto& fmt_scores = src["scores"];
            for (auto it = fmt_scores.begin(); it != fmt_scores.end(); ++it) {
                json["scores"][it.key()] = nlohmann::json::array();
                for (auto& geno: it.value()) {
                    std::string s = geno;
                    s[0] = (s[0] == 'a' ? l1 : l2);
                    s[1] = (s[1] == 'a' ? l1 : l2);
                    json["scores"][it.key()] += s;
                }
            }
            MSG_DEBUG("Created marker observation spec " << json.dump());
            return make_observation_spec(json);
        }
        return make_observation_spec(get_format_json(format_name));
    }
};



#endif //SPELL_QTL_MARKER_OBS_FORMATS_H_H
