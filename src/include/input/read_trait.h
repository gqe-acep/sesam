/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_TRAIT_READER_H_
#define _SPEL_TRAIT_READER_H_

#include <iostream>
#include <vector>
#include "file.h"

//#ifndef MATRIX_SIZE
#define dim_matrix(_x_) '(' << _x_.rows() << ", " << _x_.cols() << ')'
//#endif


inline
MatrixXd
reduce_with_good_indices(const MatrixXd& data, const std::vector<size_t>& source_indices, const std::vector<size_t>& indices)
{
    /* REQUIRES that indices is a subset of source_indices */
    MatrixXd ret(indices.size(), data.cols());
    auto sii = source_indices.begin();
    for (int i = 0; i < (int) indices.size(); ++i) {
        while (*sii < indices[i]) { ++sii; }
        ret.row(i) = data.row(sii - source_indices.begin());
    }
    return ret;
}



template <typename T>
std::vector<T>
reduce_with_good_indices(const std::vector<T>& data, const std::vector<size_t>& source_indices, const std::vector<size_t>& indices)
{
    /* REQUIRES that indices is a subset of source_indices */
    std::vector<T> ret;
    ret.reserve(indices.size());
    auto sii = source_indices.begin();
    for (size_t i: indices) {
        while (*sii < i) { ++sii; }
        ret.push_back(data[sii - source_indices.begin()]);
    }
    return ret;

}



struct trait {
    std::string name;
    std::vector<std::string> dim_names;
    std::vector<size_t> good_indices;
    /* traits in columns */
    MatrixXd values;
    MatrixXd raw;
    MatrixXd P;

    trait()
        : name(), dim_names(), good_indices(), values(), raw(), P(MatrixXd::Ones(1, 1))
    {}

    trait(const trait& other)
        : name(other.name), dim_names(other.dim_names), good_indices(other.good_indices), values(other.values), raw(other.raw), P(other.P)
    {}

    trait(const std::vector<trait>& traits, const std::vector<std::string>& names, const std::string& new_name, double threshold=1.e-3)
        : raw(), P()
    {
        name = new_name;
        std::vector<size_t> indices;
        for (const auto& n: names) {
            auto it = std::find_if(traits.begin(), traits.end(), [&](const trait& t) { return t.name == n; });
            if (it == traits.end()) {
                /* WHINE */
                MSG_WARNING("Didn't find trait " << n << " in trait list.");
                continue;
            }
            if (indices.size() == 0) {
                good_indices = it->good_indices;
            } else {
                std::vector<size_t> tmp = good_indices;
                auto last = std::set_intersection(tmp.begin(), tmp.end(), it->good_indices.begin(), it->good_indices.end(), good_indices.begin());
                good_indices.resize(last - good_indices.begin());
            }
            indices.push_back(it - traits.begin());
        }
        if (good_indices.size() == 0) {
            /* WHINE */
        } else {
            int ncols = 0;
            for (auto i: indices) {
                ncols += traits[i].raw.cols();
            }
            raw.resize(good_indices.size(), ncols);
            ncols = 0;
            for (size_t j = 0; j < indices.size(); ++j) {
                const auto& t = traits[indices[j]];
                dim_names.insert(dim_names.end(), t.dim_names.begin(), t.dim_names.end());
                auto t_gi = t.good_indices.begin();
                int t_value_i = 0;
                for (size_t i = 0; i < good_indices.size(); ++i) {
                    while (good_indices[i] != *t_gi) {
                        ++t_gi;
                        ++t_value_i;
                    }
//                    raw(i, j) = t.values(t_value_i, 0);
                    raw.block(i, ncols, 1, t.raw.cols()) = t.raw.block(t_value_i, 0, 1, t.raw.cols());
                }
                ncols += t.raw.cols();
            }
            P = MatrixXd::Identity(raw.cols(), raw.cols());
            values = raw;
        }
    }

    MatrixXd
    XtX() const
    {
        MatrixXd centered = raw.rowwise() - raw.colwise().mean();
        return centered.adjoint() * centered;
    }

    static
    MatrixXd
    do_PCA(const MatrixXd xtx, double threshold)  /* returns P */
    {
        SelfAdjointEigenSolver<MatrixXd> eig(xtx);
        VectorXd eival = eig.eigenvalues();
        MatrixXd eivec = eig.eigenvectors();

        double min = eival(eival.size() - 1) * threshold;
        int i = eival.size() - 1;
        while (i > 0 && eival(i - 1) > min) { --i; }

        return eivec.rightCols(eival.size() - i);
    }

    void
    setP(const MatrixXd& p)
    {
        P = p;

        values = raw * P;
    }

    void
    PCA(double threshold)
    {
        MatrixXd centered = raw.rowwise() - raw.colwise().mean();
        MatrixXd cov = centered.adjoint() * centered;
//        MSG_DEBUG("DIM NAMES " << dim_names);
        MSG_DEBUG("RAW" << std::endl << raw.transpose());
        MSG_DEBUG("COV" << std::endl << cov);
        SelfAdjointEigenSolver<MatrixXd> eig(cov);
//        MSG_DEBUG("EIGENVALUES" << std::endl << eig.eigenvalues());
//        MSG_DEBUG("EIGENVECTORS" << std::endl << eig.eigenvectors());
        MatrixXd test = centered * eig.eigenvectors();
        MSG_DEBUG("test " << test.col(0).dot(test.col(1)));

        VectorXd eival = eig.eigenvalues();
        MatrixXd eivec = eig.eigenvectors();

        double min = eival(eival.size() - 1) * threshold;
        int i = eival.size() - 1;
        while (i > 0 && eival(i - 1) > min) { --i; }
//        MSG_DEBUG("min=" << min << " i=" << i);

        P = eivec.rightCols(eival.size() - i);

        MSG_DEBUG("P" << std::endl << P);

        values = raw * P;

        MSG_DEBUG("projected " << dim_matrix(values));
    }

    trait operator % (const trait& other) const { return this->operator % (other.good_indices); }

    trait
    operator % (const std::vector<size_t>& other_indices) const
    {
        trait ret;
        ret.name = name;
        ret.dim_names = dim_names;
        ret.good_indices.resize(good_indices.size());
        auto it = std::set_intersection(good_indices.begin(), good_indices.end(), other_indices.begin(), other_indices.end(), ret.good_indices.begin());
        ret.good_indices.resize(it - ret.good_indices.begin());
        ret.raw = reduce_with_good_indices(raw, good_indices, ret.good_indices);
        ret.values = reduce_with_good_indices(values, good_indices, ret.good_indices);
        ret.P = P;
        return ret;
    }
};



namespace read_data {
    std::vector<trait> read_trait(file&, const std::vector<std::string>&);
}

inline std::ostream& operator << (std::ostream& os, const trait& t)
{
    os << "<single_trait:" << t.name << ' ' << t.values.cols() << " dimensions, " << t.dim_names << ", " << t.values.rows() << " values, ";
//    for (double v: t.values) {
//        os << ' ' << v;
//    }
    /*auto val = t.values.begin();*/
    /*auto end = t.values.end();*/
    /*for (; val != end; ++val) {*/
        /*os << ' ' << (*val);*/
    /*}*/

//    os << " indices:";
//    for (size_t i: t.good_indices) {
//        os << ' ' << i;
//    }
    return os << '>';
}

inline std::ostream& operator << (std::ostream& os, const std::vector<trait>& map)
{
    auto t = map.cbegin();
    auto end = map.cend();
    for (; t != end; ++t) {
        os << (*t) << std::endl;
    }
    return os;
}

#endif

