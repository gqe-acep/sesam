/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_READ_MARK_H_
#define _SPEL_READ_MARK_H_

#include <unordered_map>
#include <iostream>
#include <vector>
#include <string>
#include "file.h"
/*#include "markov_population.h"*/

struct marker_data {
    std::string data_type;
    std::unordered_map<std::string, std::string> data;
    size_t n_obs;
    size_t n_mark;

    char __get_marker_obs(const std::string& marker_name, int i) const
    {
        auto it = data.find(marker_name);
        if (it == data.end()) {
            return 0;
        }
        return it->second[i];
    }

    template <typename InputIterator>
    std::vector<char>
    get_obs(InputIterator marker_name_begin, InputIterator marker_name_end,
            size_t ind) const
    {
        std::vector<char> ret;
        if (data.size()) {
            ret.reserve(marker_name_end - marker_name_begin);
            for (; marker_name_begin != marker_name_end; ++marker_name_begin) {
                ret.push_back(__get_marker_obs(*marker_name_begin, ind));
            }
        }
        return ret;
    }
};

inline std::ostream& operator << (std::ostream& os, const marker_data& md)
{
    os << "Marker data (" << md.n_mark << " markers, " << md.n_obs << " observations)" << std::endl;
    auto i = md.data.begin();
    auto j = md.data.end();
    for (; i != j; ++i) {
        os << i->first << ": " << i->second << std::endl;
    }
    return os;
}

namespace read_data {
    marker_data read_marker(file& is, int first=-1, int last=-1);
}

#endif

