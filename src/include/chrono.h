/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_CHRONO_H_
#define _SPEL_CHRONO_H_

#include <map>
#include <sys/time.h>
#include "error.h"

struct chrono {
    struct chrono_map : public std::map<std::string, chrono> {
        ~chrono_map()
        {
            if (!chrono::display()) {
                return;
            }
            msg_handler_t::cerr() << msg_handler_t::n() << "Timers:" << std::endl;
            for (auto& kv: *this) {
                if (kv.second.accum != 0.) {
                    msg_handler_t::cerr() << kv.first << ": " << kv.second.accum << " seconds." << std::endl;
                }
            }
        }
    };

    struct counter_map : public std::map<std::string, int64_t> {
        ~counter_map()
        {
            if (!chrono::display()) {
                return;
            }
            msg_handler_t::cerr() << msg_handler_t::n() << "Counters:" << std::endl;
            for (auto& kv: *this) {
                msg_handler_t::cerr() << kv.first << ": " << kv.second << std::endl;
            }
        }
    };

    static chrono_map& chronos()
    {
        static chrono_map ret;
        return ret;
    }

    static counter_map& counters()
    {
        static counter_map ret;
        return ret;
    }

    static bool& display()
    {
        static bool _ = false;
        return _;
    }

    double t0;
    double accum;

    static std::mutex& mutex() { static std::mutex _; return _; }

    static double time_()
    {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        return tv.tv_sec + tv.tv_usec * .000001;
    }

    chrono() : t0(0), accum(0) {}

    void start() { t0 = time_(); }
    void stop() { accum += time_() - t0; }

    static void start(const std::string& s) { std::unique_lock<std::mutex> _(mutex()); chronos()[s].start(); }
    static void stop(const std::string& s) { std::unique_lock<std::mutex> _(mutex()); chronos()[s].stop(); }

    static double get_t0() { return time_(); }
    static void stop(const std::string& s, double t0) { std::unique_lock<std::mutex> _(mutex()); chronos()[s].accum += time_() - t0; }


    static void increment(const std::string& s) { std::unique_lock<std::mutex> _(mutex()); counters()[s]++; }
};


struct scope_chrono {
    std::string name;
    scope_chrono(const std::string& n) : name(n) { chrono::start(name); }
    ~scope_chrono() { chrono::stop(name); }
};

#endif

