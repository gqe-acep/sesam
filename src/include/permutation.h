/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_PERMUT_H_
#define _SPEL_PERMUT_H_

#include <map>
#include <iostream>
#include "eigen.h"
#include "braille_plot.h"
#include "lumping2.h"

struct permutation_type;
struct transposed_permutation_type;

template <typename TYPE> struct transposition_result;
template <> struct transposition_result<permutation_type> {
    typedef transposed_permutation_type type;
    typedef transposed_permutation_type return_type;
    static return_type result(const permutation_type* p);
};
template <> struct transposition_result<transposed_permutation_type> {
    typedef permutation_type type;
    typedef const permutation_type& return_type;
    static return_type result(const transposed_permutation_type* p);
};

template <typename DERIVED>
struct permutation_base {
    typedef DERIVED derived_type;
    virtual const std::vector<int>& map() const = 0;
    virtual const std::vector<int>& transposed_map() const = 0;

    struct const_iterator {
        std::vector<int>::const_iterator m_i, m_beg;
        typedef std::pair<int, int> value_type;
        value_type tmp;
        const_iterator(std::vector<int>::const_iterator m, std::vector<int>::const_iterator b) : m_i(m), m_beg(b), tmp() {}
        bool operator == (const_iterator c) { return m_i == c.m_i; }
        bool operator != (const_iterator c) { return m_i != c.m_i; }
        bool operator < (const_iterator c) { return m_i < c.m_i; }
        value_type operator * () const { return {m_i - m_beg, *m_i}; }
        const_iterator& operator ++ () { ++m_i; return *this; }
        const_iterator& operator ++ (int) { ++m_i; return *this; }
        const value_type* operator -> () { tmp = **this; return &tmp; }
    };

    const_iterator begin() const { return {map().begin(), map().begin()}; }
    const_iterator end() const { return {map().end(), map().begin()}; }

    typename transposition_result<DERIVED>::return_type transpose() const;

    size_t size() const { return map().size(); }
    size_t cols() const { return size(); }
    size_t rows() const { return size(); }
    size_t innerSize() const { return size(); }
    size_t outerSize() const { return size(); }

    template <typename OTHER_DERIVED>
    permutation_type operator * (const permutation_base<OTHER_DERIVED>& other) const;

    const DERIVED* derived() const { return dynamic_cast<const DERIVED*>(this); }

    template <typename _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>
    Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols>
    operator % (const Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols>& other) const
    {
        Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols> ret(other.rows(), other.cols());
        const std::vector<int>& m_map = map();
        const std::vector<int>& m_transposed = transposed_map();
        for (int i = 0; i < other.rows(); ++i) {
            for (int j = 0; j < other.cols(); ++j) {
                /*ret(m_transposed[i], m_map[j]) = other(i, j);*/
                ret(i, m_map[j]) = other(m_transposed[i], j);
            }
        }
        return ret;
    }

    template <typename VALUE_TYPE>
    std::vector<VALUE_TYPE>
    operator * (const std::vector<VALUE_TYPE>& vec) const
    {
        std::vector<VALUE_TYPE> ret;
        ret.reserve(vec.size());
        const std::vector<int>& m_transposed = transposed_map();
        for (size_t i = 0; i < vec.size(); ++i) {
            ret.push_back(vec[m_transposed[i]]);
        }
        return ret;
    }

    template <typename Scalar>
    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> matrix() const
    {
        const std::vector<int>& m_map = map();
        Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> ret = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>::Zero(m_map.size(), m_map.size());
        for (size_t i = 0; i < m_map.size(); ++i) {
            ret(m_map[i], i) = 1;
        }
        return ret;
    }

    template <typename OTHER_DERIVED>
    bool operator == (const permutation_base<OTHER_DERIVED>& other) const
    {
        return map() == other.map();
    }

    template <typename OTHER_DERIVED>
    bool operator != (const permutation_base<OTHER_DERIVED>& other) const
    {
        return map() != other.map();
    }

    template <typename OTHER_DERIVED>
    bool operator < (const permutation_base<OTHER_DERIVED>& other) const
    {
        return map() < other.map();
    }

    friend
        std::ostream& operator << (std::ostream& os, const permutation_base<DERIVED>& perm)
        {
            /*return os << ('.' + ('@' - '.') * perm.matrix<char>().array());*/
            braille_grid grid(perm.size(), perm.size());
            grid.set_background(32, 20, 32);
            const auto& map = perm.map();
            for (size_t i = 0; i < map.size(); ++i) {
                grid.put_pixel(i, map[i]);
            }
            return os << grid;
        }

    int operator [] (int state) const { return map()[state]; }
};


struct permutation_type : public permutation_base<permutation_type> {
    std::vector<int> m_map;
    std::vector<int> m_transposed;

    static
    permutation_type identity(int n)
    {
        permutation_type ret;
        ret.m_map.reserve(n);
        ret.m_transposed.reserve(n);
        for (int i = 0; i < n; ++i) {
            ret.m_map.push_back(i);
            ret.m_transposed.push_back(i);
        }
        return ret;
    }

    static
    permutation_type anti_identity(int n)
    {
        permutation_type ret;
        ret.m_map.reserve(n);
        ret.m_transposed.reserve(n);
        for (int i = 0; i < n; ++i) {
            ret.m_map.push_back(n - 1 - i);
            ret.m_transposed.push_back(n - 1 - i);
        }
        return ret;
    }

    static
    permutation_type lozenge(size_t n0, size_t n1)
    {
        int N = n0 * n1;
        permutation_type ret;
        ret.m_map.resize(N);
        ret.m_transposed.resize(N);
        for (size_t i0 = 0; i0 < n0; ++i0) {
            for (size_t i1 = 0; i1 < n1; ++i1) {
                int i = i1 * n0 + i0;
                int j = i0 * n1 + i1;
                ret.m_map[j] = i;
                ret.m_transposed[i] = j;
            }
        }
        return ret;
    }

    template <typename DERIVED>
    static
    permutation_type lozenge(size_t n0, size_t n1, const permutation_base<DERIVED>& permut)
    {
        int N = n0 * n1;
        /*if (N == 2) {*/
            /*return (MatrixXb::Ones(2, 2) - MatrixXb::Identity(2, 2));*/
        /*}*/

        /*std::vector<int> permut_col(permut.cols());*/
        /*std::vector<int> permut_row(permut.rows());*/
        const std::vector<int>& permut_col = permut.transposed_map();
        const std::vector<int>& permut_row = permut.map();

        permutation_type ret;
        ret.m_map.resize(N);
        ret.m_transposed.resize(N);
        for (size_t i0 = 0; i0 < n0; ++i0) {
            for (size_t i1 = 0; i1 < n1; ++i1) {
                int i = i1 * n0 + i0;
                int j = i0 * n1 + i1;
                /*ret(permut_col[i], permut_row[j]) = 1;*/
                /*ret(permut_row[i], permut_col[j]) = 1;*/
                ret.m_map[permut_col[j]] = permut_row[i];
                ret.m_transposed[permut_row[i]] = permut_col[j];
            }
        }
        /*MSG_DEBUG("lozenge(" << n0 << ", " << n1 << ',' << (antidiag ? "anti" : "diag") << ')');*/
        /*MSG_DEBUG(ret);*/
        /*MSG_DEBUG("-- self-adjoint?");*/
        /*MSG_DEBUG((ret - ret.transpose()));*/
        /*MSG_DEBUG("-- self-inverse?");*/
        /*MSG_DEBUG((ret * ret));*/
        return ret;
    }


    permutation_type() : m_map(), m_transposed() {}
    permutation_type(const permutation_type& p) : m_map(p.m_map), m_transposed(p.m_transposed) {}
    permutation_type(permutation_type&& p) : m_map(std::move(p.m_map)), m_transposed(std::move(p.m_transposed)) {}
    explicit permutation_type(const std::vector<int>& m) : m_map(m), m_transposed() { compute_transposed(); }
    explicit permutation_type(std::vector<int>&& m) : m_map(std::move(m)), m_transposed() { compute_transposed(); }
    explicit permutation_type(std::initializer_list<int> m) : m_map(std::move(m)), m_transposed() { compute_transposed(); }
    permutation_type(const std::vector<int>& m, const std::vector<int>& t) : m_map(m), m_transposed(t) {}
    template <typename DERIVED>
    permutation_type(const permutation_base<DERIVED>& p) : m_map(p.map()), m_transposed(p.transposed_map()) {}
    template <typename Scalar>
    explicit permutation_type(const Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>& mat)
        : m_map(mat.rows(), -1), m_transposed(mat.rows(), -1)
    {
        assert(mat.rows() == mat.cols());

        for (int col = 0; col < mat.cols(); ++col) {
            int row;
            mat.col(col).maxCoeff(&row);
            m_map[col] = row;
            m_transposed[row] = col;
        }
    }
    template <typename Alloc>
    permutation_type(const std::map<int, int, Alloc>& map)
        : m_map(map.size(), -1), m_transposed(map.size(), -1)
    {
        for (const auto& kv: map) {
            m_map[kv.first] = kv.second;
            m_transposed[kv.second] = kv.first;
        }
    }

    const std::vector<int>& map() const { return m_map; }
    const std::vector<int>& transposed_map() const { return m_transposed; }

    permutation_type& operator = (const permutation_type& p) { m_map = p.map(); m_transposed = p.transposed_map(); return *this; }
    permutation_type& operator = (const transposed_permutation_type& p);
    permutation_type& operator = (permutation_type&& p) { m_map.swap(p.m_map); m_transposed.swap(p.m_transposed); return *this; }

    permutation_type& compute_transposed()
    {
        m_transposed.clear();
        m_transposed.resize(m_map.size());
        for (size_t i = 0; i < m_map.size(); ++i) {
            m_transposed[m_map[i]] = i;
        }
        return *this;
    }

    permutation_type lump(const std::set<subset>& P0) const
    {
        std::map<int, int> bins;
        int b = 0;
        for (const auto& S: P0) {
            for (int s: S) {
                bins[s] = b;
            }
            ++b;
        }
        /* check consistency */
        for (const auto& S: P0) {
            auto i = S.begin(), j = S.end();
            b = bins[m_map[*i]];
            for (++i; i != j; ++i) {
                if (b != bins[m_map[*i]]) {
                    /* whine */
//                    MSG_DEBUG("CAN'T LUMP BECAUSE OF CONFLICT");
//                    MSG_DEBUG("PERMUTATION");
//                    MSG_DEBUG("" << (*this));
//                    MSG_DEBUG("PARTITION");
//                    MSG_DEBUG("" << P0);
//                    MSG_QUEUE_FLUSH();
                    return {};
                }
            }
        }
        /* perform lump */
        permutation_type ret;
        for (const auto& S: P0) {
            ret.m_map.push_back(bins[m_map[*S.begin()]]);
        }
        ret.compute_transposed();
//        MSG_DEBUG("LUMP");
//        MSG_DEBUG("" << (*this));
//        MSG_DEBUG("INTO");
//        MSG_DEBUG("" << ret);
        return ret;
    }

    permutation_type lump(const Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic>& mat) const
    {
        std::set<subset> P;
        for (int r = 0; r < mat.rows(); ++r) {
            subset S;
            for (int c = 0; c < mat.cols(); ++c) {
                if (mat(r, c)) {
                    S.push_back(c);
                }
            }
            P.insert(S);
        }
        return lump(P);
    }
};



struct transposed_permutation_type : public permutation_base<transposed_permutation_type> {
    const permutation_type* ptr;
    transposed_permutation_type(const permutation_type* viewed) : ptr(viewed) {}

    const std::vector<int>& map() const { return ptr->m_transposed; }
    const std::vector<int>& transposed_map() const { return ptr->m_map; }
};

inline
permutation_type& permutation_type::operator = (const transposed_permutation_type& p) { m_map = p.map(); m_transposed = p.transposed_map(); return *this; }



/*static*/
inline
    typename transposition_result<permutation_type>::return_type
    transposition_result<permutation_type>::
    result(const permutation_type* p) { return {p}; }

/*static*/
inline
    typename transposition_result<transposed_permutation_type>::return_type
    transposition_result<transposed_permutation_type>::
    result(const transposed_permutation_type* p) { return *p->ptr; }

template <typename DERIVED>
    typename transposition_result<DERIVED>::return_type
    permutation_base<DERIVED>::
    transpose() const
    {
        return transposition_result<DERIVED>::result(derived());
    }


template <typename DERIVED>
template <typename OTHER_DERIVED>
    permutation_type
    permutation_base<DERIVED>::
    operator * (const permutation_base<OTHER_DERIVED>& other) const
    {
        permutation_type ret;
        /*MSG_DEBUG_INDENT_EXPR("[product A] ");*/
        /*MSG_DEBUG((*this));*/
        /*MSG_DEBUG_DEDENT;*/
        /*MSG_DEBUG_INDENT_EXPR("[product B] ");*/
        /*MSG_DEBUG(other);*/
        /*MSG_DEBUG_DEDENT;*/
        /*MSG_QUEUE_FLUSH();*/
        if (size() != other.size()) {
            MSG_DEBUG("SIZE MISMATCH " << size() << " vs " << other.size());
            MSG_DEBUG("this:");
            MSG_DEBUG("" << (*derived()));
            MSG_DEBUG("other:");
            MSG_DEBUG("" << (*other.derived()));
            MSG_QUEUE_FLUSH();
            assert(size() == other.size());
        }
        ret.m_map.resize(size(), -1);
        ret.m_transposed.resize(size(), -1);
        const std::vector<int>& m_map = map();
        const std::vector<int>& m_transposed = transposed_map();
        const std::vector<int>& other_map = other.map();
        const std::vector<int>& other_transposed = other.transposed_map();
        for (size_t i = 0; i < other.size(); ++i) {
            ret.m_map[i] = m_map[other_map[i]];
            ret.m_transposed[i] = other_transposed[m_transposed[i]];
        }

#ifndef NDEBUG
        for (size_t i = 0; i < other.size(); ++i) {
            assert(ret.m_transposed[m_map[other_map[i]]] == (int) i);
        }
#endif

        /*MSG_DEBUG_INDENT_EXPR("[product R] ");*/
        /*MSG_DEBUG(ret);*/
        /*MSG_DEBUG_DEDENT;*/

        return ret;
        /*return ret.compute_transposed();*/
    }

template <typename DERIVED1, typename DERIVED2>
    permutation_type
    kronecker(const permutation_base<DERIVED1>& p1, const permutation_base<DERIVED2>& p2)
    {
        permutation_type ret;
        size_t sz1 = p1.size();
        size_t sz2 = p2.size();
        auto compute_state = [&] (int st1, int st2) { return st1 * sz2 + st2; };
        ret.m_map.resize(p1.size() * sz2);
        ret.m_transposed.resize(sz1 * sz2);
        size_t s = 0;
        for (size_t s1 = 0; s1 < sz1; ++s1) {
            for (size_t s2 = 0; s2 < sz2; ++s2, ++s) {
                size_t t = compute_state(p1[s1], p2[s2]);
                ret.m_map[s] = t;
                ret.m_transposed[t] = s;
            }
        }
        return ret;
    }

#if 0
inline
    permutation_type
    kronecker(const std::vector<permutation_type>& pvec, const std::vector<int>& refs)
    {
        std::vector<size_t> iterators(pvec.size(), 0);
        std::vector<size_t> sizes(pvec.size(), 1);

        for (size_t i = 0; i < pvec.size(); ++i) {
            if (refs[i] != -1) {
                sizes[i] = pvec[i].size();
            }
        }

        auto get_i =
            [&] (size_t i)
            {
                if (refs[i] == -1) {
                    return iterators[i];
                } else {
                    return 0;
                }
            };

        auto get_pi =
            [&] (size_t i)
            {
                if (refs[i] == -1) {
                    return pvec[i].m_map[iterators[i]];
                } else {
                    return pvec[i].m_map[iterators[refs[i]]];
                }
            };


        auto next =
            [&] ()
            {
                size_t i = 0;
                while (i < iterators.size()) {
                    if (refs[i] == -1) {
                        ++iterators[i];
                        if (iterators[i] == sizes[i]) {
                            iterators[i] = 0;
                            ++i;
                        } else {
                            break;
                        }
                    } else {
                        ++i;
                    }
                }
                return i != iterators.size();
            };

        auto get_col =
            [&] ()
            {
            };
    }
#endif

#endif
