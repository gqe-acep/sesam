/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_FILE_H_
#define _SPELL_FILE_H_

#ifndef MSG_DEBUG
#include <error.h>
#endif

extern "C" {
#include <string.h>   /* for strerror */
}

#include <fstream>
#include <map>  /* for debug */
#include <atomic>

struct file {
    static constexpr std::ios_base::seekdir beg = std::ios::beg;
    static constexpr std::ios_base::seekdir cur = std::ios::cur;
    static constexpr std::ios_base::seekdir end = std::ios::end;
    static std::atomic<size_t> open_count;

    std::fstream m_impl;
    std::string m_path;
    static std::map<std::string, size_t> debug_open_files;
    static std::mutex dof_mutex;

    static void add_file(const std::string& path) { std::unique_lock<std::mutex> _(dof_mutex); ++debug_open_files[path]; }
    static void remove_file(const std::string& path) { std::unique_lock<std::mutex> _(dof_mutex); --debug_open_files[path]; }

    struct error : public std::ios_base::failure {
        error() : std::ios_base::failure("") {}
        error(const std::string& msg) : std::ios_base::failure(msg) {}
    };

    file() : m_impl(), m_path("<undefined>") {}

    file(const std::string& path, std::ios_base::openmode mode)
        : m_impl(), m_path()
    {
        open(path, mode);
    }

    ~file() { m_impl.close(); }

    bool good() const { return m_impl.good(); }
    bool fail() const { return m_impl.fail(); }
    bool eof() const { return m_impl.eof(); }
    bool bad() const { return m_impl.bad(); }

    operator bool () const { return good(); }
    bool operator ! () const { return !good(); }

    operator std::fstream& () { return m_impl; }
    operator std::ostream& () { return m_impl; }

    void
        close() { if (m_impl.is_open()) { m_impl.close(); remove_file(m_path); m_path = "<undefined>"; open_count--; } }

    void
        open(const std::string& path, std::ios_base::openmode mode)
        {
            m_impl.close();
            m_path = path;
            m_impl.open(path, mode);
            if (m_impl.is_open()) {
                check_state();
                open_count++;
                add_file(path);
            } else {
                throw file::error(SPELL_STRING("Couldn't open file " << path << " (" << open_count << " already open files). Error message: " << strerror(errno)));
            }
        }

    std::streampos tellg() { return m_impl.tellg(); }
    std::streampos tellp() { return m_impl.tellp(); }
    file& seekg(std::streamoff off) { m_impl.seekg(off); return *this; }
    file& seekg(std::streamoff off, std::ios_base::seekdir way) { m_impl.seekg(off, way); return *this; }
    int peek() { return m_impl.peek(); }

    void
        check_state() const
        {
            if (m_impl.bad()) {
                throw file::error(SPELL_STRING("Corrupted stream. Can't I/O file " << m_path));
            }
            if (m_impl.fail()) {
                throw file::error(SPELL_STRING("Couldn't I/O file " << m_path));
            }
            /*if (m_impl.eof()) {*/
                /*throw file::error(SPELL_STRING("Reached EOF while reading file " << m_path));*/
            /*}*/
        }

    template <typename ARG>
        friend
        file& operator << (file& f, ARG&& arg);

    template <typename ARG>
        friend
        file& operator >> (file& f, ARG&& arg);

    friend
        file& operator << (file& f, std::ostream& (&func) (std::ostream&));

    file&
        write(const char* s, std::streamsize n)
        {
            m_impl.write(s, n);
            check_state();
            return *this;
        }

   file&
       read(char* s, std::streamsize n)
       {
           check_state();
           m_impl.read(s, n);
           return *this;
       }

   file&
       put(char c)
       {
           m_impl.put(c);
           check_state();
           return *this;
       }

   int
       get()
       {
           check_state();
           int ret = m_impl.get();
           return ret;
       }

   template <typename Arg0, typename... Args>
       file&
       get(Arg0& arg, Args&... args)
       {
           m_impl.get(arg, args...);
           check_state();
           return *this;
       }
};


struct ifile : public file {
    // AVOID A BUG on Windows => add parameter mode to allow binary mode
    ifile(const std::string& path, std::ios_base::openmode mode = std::ios_base::in): file(path, mode) {}

    using file::get;
};


struct ofile : public file {
    ofile(const std::string& path, std::ios_base::openmode mode = std::ios_base::out | std::ios_base::trunc)
        : file(path, mode)
    {}
};


namespace std {
    inline
    void getline(file& f, std::string& s) { getline(f.m_impl, s); }
}


#endif

