/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_CACHE_FILE_H_
#define _SPEL_CACHE_FILE_H_

#include <iostream>
#include <type_traits>
#include "labelled_matrix.h"

struct cache_input_traits {
    typedef std::istream stream_type;
    static void op(stream_type& st, char* b, size_t sz) { st.read(b, sz); }
};

struct cache_output_traits {
    typedef std::ostream stream_type;
    static void op(stream_type& st, char* b, size_t sz) { st.write(b, sz); }
};

template <typename CACHE_DIRECTION>
struct cache_file {
    typename CACHE_DIRECTION::stream_type& f;

    cache_file(typename CACHE_DIRECTION::stream_type& _) : f(_) {}
    operator typename CACHE_DIRECTION::stream_type& () { return f; }
    cache_file<CACHE_DIRECTION>& op(char* b, size_t sz)
    {
        CACHE_DIRECTION::op(f, b, sz);
        return *this;
    }
};

typedef cache_file<cache_input_traits> cache_input;
typedef cache_file<cache_output_traits> cache_output;


template <typename CACHE_DIRECTION, typename X>
    typename std::enable_if<std::is_arithmetic<X>::value, cache_file<CACHE_DIRECTION>>::type&
    operator & (cache_file<CACHE_DIRECTION>& c, X& x)
    {
        return c.op(const_cast<char*>(reinterpret_cast<const char*>(&x)), sizeof(X));
    }

#if 0
template <typename CACHE_DIRECTION, typename X>
    typename std::enable_if<!std::is_arithmetic<X>::value,
        cache_file<CACHE_DIRECTION>>::type&
    operator & (cache_file<CACHE_DIRECTION>& c, const X& x)
    {
        return c.op(reinterpret_cast<char*>(&x), sizeof(X));
    }


template <typename CACHE_DIRECTION, typename X>
    typename std::enable_if<std::is_arithmetic<X>::value,
        cache_file<CACHE_DIRECTION>>::type&
    operator & (cache_file<CACHE_DIRECTION>& c, X x)
    {
        CACHE_DIRECTION::op(c, reinterpret_cast<char*>(&x), sizeof(X));
    }
#endif

static inline
cache_output& operator & (cache_output& c, const std::string& s)
{
    size_t sz = s.size();
    c & sz;
    return c.op(const_cast<char*>(s.c_str()), s.size());
}

static inline
cache_input& operator & (cache_input& c, std::string& s)
{
    size_t sz = 0;
    c & sz;
    s.resize(sz, ' ');
    return c.op(const_cast<char*>(s.c_str()), sz);
}



template <int I, int N, typename CACHE_DIRECTION, typename... Elems>
typename std::enable_if<I != N, cache_file<CACHE_DIRECTION>>::type&
operator & (cache_file<CACHE_DIRECTION>& c, std::tuple<Elems...>& t)
{
    c & std::get<I>(t);
    return operator &<I + 1, N, CACHE_DIRECTION, Elems...> (c, t);
}

template <int I, int N, typename CACHE_DIRECTION, typename... Elems>
typename std::enable_if<I == N, cache_file<CACHE_DIRECTION>>::type&
operator & (cache_file<CACHE_DIRECTION>& c, std::tuple<Elems...>&)
{
    return c;
}

template <typename CACHE_DIRECTION, typename... Elems>
cache_file<CACHE_DIRECTION>&
operator & (cache_file<CACHE_DIRECTION>& c, std::tuple<Elems...>& t)
{
    return operator &<0, sizeof...(Elems), CACHE_DIRECTION, Elems...> (c, t);
}

template <typename V, typename A>
cache_output&
operator & (cache_output& c, std::vector<V, A>& v)
{
    size_t sz = v.size();
    c & sz;
    for (auto& x: v) { c & x; }
    return c;
}

template <typename V, typename A>
cache_input&
operator & (cache_input& c, std::vector<V, A>& v)
{
    size_t sz;
    c & sz;
    v.clear();
    v.reserve(sz);
    V tmp;
    for (; sz > 0; --sz) { c & tmp; v.push_back(tmp); }
    return c;
}

template <typename K, typename V, typename A, typename C>
cache_output&
operator & (cache_output& c, std::map<K, V, A, C>& v)
{
    size_t sz = v.size();
    K key;
    V value;
    c & sz;
    for (auto& x: v) { key = x.first; value = x.second; c & key & value; }
    return c;
}

template <typename K, typename V, typename A, typename C>
cache_input&
operator & (cache_input& c, std::map<K, V, A, C>& m)
{
    size_t sz;
    K key;
    V value;
    c & sz;
    m.clear();
    for (; sz > 0; --sz) { c & key & value; m[key] = value; }
    return c;
}



template <typename _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>
cache_output&
operator & (cache_output& c, Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols>& mat)
{
    int sz;
    sz = mat.innerSize();
    /*MSG_DEBUG("inner size " << sz);*/
    c & sz;
    sz = mat.outerSize();
    /*MSG_DEBUG("outer size " << sz);*/
    c & sz;
    _Scalar* data = mat.data();
    for (int i = 0; i < mat.size(); ++i) {
        c & data[i];
    }
#if 0
    for (int j = 0; j < mat.outerSize(); ++j) {
        for (int i = 0; i < mat.innerSize(); ++i) {
            /*MSG_DEBUG('(' << i << ',' << j << ") " << mat(i, j));*/
            c & mat(i, j);
        }
    }
#endif
    return c;
}

template <typename _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>
cache_input&
operator & (cache_input& c, Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols>& mat)
{
    int sz1, sz2;
    c & sz1;
    c & sz2;
    /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << '\t' << __func__);*/
    mat.resize(sz1, sz2);
    _Scalar* data = mat.data();
    for (int i = 0; i < mat.size(); ++i) {
        c & data[i];
    }
#if 0
    _Scalar s;
    /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << '\t' << __func__);*/
    for (int j = 0; j < mat.outerSize(); ++j) {
        for (int i = 0; i < mat.innerSize(); ++i) {
            c & s;
            /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << '\t' << __func__);*/
            mat(i, j) = s;
        }
    }
#endif
    return c;
}


#if 0
template <typename Class, typename Type, typename... Dependencies>
cache_input&
operator & (cache_input& c, cache::computed_value<Class, Type, Dependencies...>& cv)
{
    std::tuple<Dependencies...> dep;
    c & dep;
    cv.__init(dep);
    return c;
}

template <typename Class, typename Type, typename... Dependencies>
cache_output&
operator & (cache_output& c, cache::computed_value<Class, Type, Dependencies...>& cv)
{
    c & cv.task->dependencies;
    return c;
}
#endif

template <typename DIRECTION, typename M, typename R, typename C>
cache_file<DIRECTION>& operator & (cache_file<DIRECTION>& cf, labelled_matrix<M, R, C>& lm)
{
    return cf & lm.row_labels & lm.column_labels & lm.data;
}



inline
cache_input& operator & (cache_input& cf, label_type& a)
{
    return cf & a.first() & a.second();
}

inline
cache_output& operator & (cache_output& cf, const label_type& a)
{
    return cf & a.first() & a.second();
}



#endif

