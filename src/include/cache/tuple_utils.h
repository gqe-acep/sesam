/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_CACHE_TUPLE_UTILS_H_
#define _SPEL_CACHE_TUPLE_UTILS_H_

#include <tuple>
#include <iostream>

namespace cache {

template <int... X> struct reverse;
template <typename Reverse, int... X> struct reverse_impl;
template <int... REV, int CAR, int... CDR>
    struct reverse_impl<reverse<REV...>, CAR, CDR...> {
        typedef typename reverse_impl<reverse<CAR, REV...>, CDR...>::type type;
    };
template <int... REV>
    struct reverse_impl<reverse<REV...>> { typedef reverse<REV...> type; };
template <int... X>
    struct make_reverse { typedef typename reverse_impl<reverse<>, X...>::type type; };


    template <template <typename X> class PREDICATE, typename... T> struct if_any_type;

    template <template <typename X> class PREDICATE, typename X>
        struct if_any_type<PREDICATE, X> {
            static const bool value = PREDICATE<X>::value;
        };

    template <template <typename X> class PREDICATE, typename CAR, typename... CDR>
        struct if_any_type<PREDICATE, CAR, CDR...> {
            static const bool value = PREDICATE<CAR>::value || if_any_type<PREDICATE, CDR...>::value;
        };

    template <int... Indices> struct sequence {
        template <template <typename X> class PROCESSOR, typename... TupleElems>
            struct with_processor {
                typedef std::tuple<typename PROCESSOR<TupleElems>::return_type...> result_type;

                static result_type process(const std::tuple<TupleElems...>& x)
                {
                    return std::make_tuple(PROCESSOR<TupleElems>()(std::get<Indices>(x))...);
                }

                static result_type process(std::tuple<TupleElems...>& x)
                {
                    return std::make_tuple(PROCESSOR<TupleElems>()(std::get<Indices>(x))...);
                }
            };

        template <template <typename X> class PROCESSOR, typename... TupleElems>
            struct with_processor<PROCESSOR, std::tuple<TupleElems...>> : public with_processor<PROCESSOR, TupleElems...> {};

        template <template <typename X> class PROCESSOR, typename... TupleElems>
            struct with_backward_recursion {
                typedef bool result_type;
                typedef typename make_reverse<Indices...>::type backward_indices;

                template <typename REV, typename... ELEMENTS> struct backward;
                template <typename REV> struct backward_tuple;

                template <int I, int... BackwardIndices, typename Elem, typename... Elems>
                    struct backward<reverse<I, BackwardIndices...>, Elem, Elems...> {
                        static bool process(Elem& car, Elems&... x)
                        {
                            if (PROCESSOR<Elem>()(car)) {
                                return backward<reverse<BackwardIndices...>, Elems...>::process(x...);
                            }
                            return false;
                        }
                    };

                template <int I, typename Elem>
                    struct backward<reverse<I>, Elem> {
                        static bool process(Elem& car)
                        {
                            return PROCESSOR<Elem>()(car);
                        }
                    };

                template <int I, int... BackwardIndices>
                    struct backward_tuple<reverse<I, BackwardIndices...>> {
                        static bool process(std::tuple<TupleElems...>& x)
                        {
                            if (PROCESSOR<typename std::tuple_element<I, std::tuple<TupleElems...>>::type>()(std::get<I>(x))) {
                                return backward_tuple<reverse<BackwardIndices...>>::process(x);
                            }
                            return false;
                        }
                    };

                template <int I>
                    struct backward_tuple<reverse<I>> {
                        static bool process(std::tuple<TupleElems...>& x)
                        {
                            return PROCESSOR<typename std::tuple_element<I, std::tuple<TupleElems...>>::type>()(std::get<I>(x));
                        }
                    };

                static bool process(TupleElems&... x)
                {
                    return backward<backward_indices, TupleElems...>::process(x...);
                }

                static bool process(std::tuple<TupleElems...>& x)
                {
                    return backward_tuple<backward_indices>::process(x);
                }
            };
    };

    template <int N, int Last, int... Indexes>
        struct make_sequence_impl {
            typedef typename std::enable_if<N != Last,
                                            typename make_sequence_impl<N, Last + 1, Indexes..., Last>::type
                                           >::type type;
    };
    template <int N, int... Indexes>
        struct make_sequence_impl<N, N, Indexes...> {
            typedef sequence<Indexes...> type;
        };

    template <int N> struct make_sequence { typedef typename make_sequence_impl<N, 0>::type type; };

    template <template <typename ITEM> class TRANSFORM, typename... TupleElems>
        struct transform {
            typedef typename make_sequence<sizeof...(TupleElems)>::type indices;
            typedef typename indices::template with_processor<TRANSFORM, TupleElems...> processor;
            typedef typename processor::result_type type;
            type operator () (TupleElems... x) const
            {
                return processor::process(x...);
            }
        };

}

template <typename Elem>
void _print_elem(std::ostream& os, const Elem& e) { os << e; }

template <typename Elem>
void _print_elem(std::ostream& os, const Elem* e) { os << "&(" << e << ')'; }

template <int I, typename... TElems>
typename std::enable_if<I == sizeof...(TElems), std::ostream>::type&
_print_tuple(std::ostream& os, const std::tuple<TElems...>& t)
{
    return os << ')';
}

template <int I, typename... TElems>
typename std::enable_if<(I > 0 && I < sizeof...(TElems)), std::ostream>::type&
_print_tuple(std::ostream& os, const std::tuple<TElems...>& t)
{
    os << ", ";
    _print_elem(os, std::get<I>(t));
    return _print_tuple<I + 1, TElems...>(os, t);
}

template <int I, typename... TElems>
typename std::enable_if<I == 0, std::ostream>::type&
_print_tuple(std::ostream& os, const std::tuple<TElems...>& t)
{
    _print_elem(os, std::get<I>(t));
    return _print_tuple<I + 1, TElems...>(os, t);
}

template <typename... TElems>
std::ostream& _print_tuple(std::ostream& os, const std::tuple<TElems...>& t)
{
    os << '(';
    return _print_tuple<0, TElems...>(os, t);
}


template <typename... TElems>
std::ostream&
operator << (std::ostream& os, const std::tuple<TElems...>& t)
{
    return _print_tuple(os, t);
}

#endif

