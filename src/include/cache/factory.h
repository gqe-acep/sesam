/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_CACHE_FACTORY_H_
#define _SPEL_CACHE_FACTORY_H_

#include <vector>
#include <memory>
#include <iostream>
#include <future>
#include <typeinfo>

#include "md5.h"
#include "input.h"

/*#include "call_tuple.h"*/
#include "tuple_utils.h"
#include "value.h"
#include "cartesian_product.h"
#include "file.h"

template <typename T>
using only_if_derives_from_wof = typename std::enable_if<std::is_base_of<cache::with_output_func, T>::value, T>::type;

namespace std {
    template <> struct hash<const double> : hash<double> {};

    template <typename... Elem>
        struct hash<std::tuple<Elem...>> {
            typedef std::tuple<Elem...> TupleType;
            template <int I, int N>
                struct hash_elem {
                    size_t operator () (const TupleType& t) const
                    {
                        /*std::hash<typename std::tuple_element<I, TupleType>::type> h;*/
                        return std::get<I>(t).hash() ^ hash_elem<I + 1, N>()(t);
                    }
                };
            template <int N>
                struct hash_elem<N, N> {
                    size_t operator () (const TupleType&) const
                    {
                        return 0;
                    }
                };
            size_t operator () (const TupleType& t) const
            {
                return hash_elem<0, sizeof...(Elem)>()(t);
            }
        };

    /*template <typename T>*/
        /*struct hash<only_if_derives_from_wof<T>> : hash<cache::with_output_func> {};*/
}



/*msg_handler_t::lock_type msg_handler_t::mutex;*/

namespace cache {
namespace predicate {
    template <typename T> struct is_composite
        : std::integral_constant<bool, T::composite> {};
#if 0
    template <typename T> struct is_composite;
    template <typename T> struct is_composite<value<T>>
        : std::integral_constant<bool, false> {};
    template <typename T> struct is_composite<range<T>>
        : std::integral_constant<bool, true> {};
    template <typename T> struct is_composite<collection<T>>
        : std::integral_constant<bool, true> {};

    template <typename X, typename T>
        struct derived_from
            : std::integral_constant<bool, std::is_same<X, T>::value
                                           || std::is_base_of<T, X>::value> {};

    template <typename V, template <typename X> class T>
        struct is_composite<T<V>>
            : std::integral_constant<bool, derived_from<T<V>, range<V>>::value
                                           || derived_from<T<V>, collection<V>>::value> {};
#endif
}
}

namespace detail {
    /* FIXME : rather overload operator << (md5, x<V>) */
    template <typename V>
        struct is_container<cache::value<V>>
            : std::integral_constant<bool, false> {};
    template <typename V>
        struct is_container<cache::range<V>>
            : std::integral_constant<bool, false> {};
    template <typename V>
        struct is_container<cache::collection<V>>
            : std::integral_constant<bool, true> {};
}


namespace cache {


template <typename Class, typename Type, typename... Dependencies>
struct computed_value : value<Type> {
    typedef Type value_type;
    typedef std::tuple<Dependencies...> dependencies_type;
    typedef async_computed_value<Class, Type, Dependencies...> task_type;
    using value<Type>::output;
    using value<Type>::val;
    task_type* task;
    /*computed_value(Dependencies&&... d)*/
        /*: value<Type>(),  task(task_type::get(d...))*/
    /*{}*/
    computed_value(const Dependencies&... d)
        : value<Type>(),  task(task_type::get(d...))
    {}
    /*computed_value(std::tuple<Dependencies...>&& t)*/
        /*: value<Type>(),  task(task_type::get(t))*/
    /*{}*/
    computed_value(const std::tuple<Dependencies...>& t)
        : value<Type>(),  task(task_type::get(t))
    {}
    computed_value(const computed_value<Class, Type, Dependencies...>& cv)
        : value<Type>(),  task(cv.task)
    {}
    /*computed_value(computed_value<Class, Type, Dependencies...>&& cv)*/
        /*: value<Type>(),  task(cv.task)*/
    /*{}*/

    template <typename... DerDep>
        computed_value(const std::tuple<DerDep...>& ddt)
            : value<Type>(), task(task_type::get(std::tuple<Dependencies...>(ddt)))
        {}

    template <typename... DerDep>
        computed_value(const DerDep&... dd)
            : value<Type>(), task(task_type::get(((const Dependencies&)dd)...))
        {}

    computed_value() : value<Type>(), task() {}

    void __init(const std::tuple<Dependencies...>& d)
    {
        task = task_type::get(d);
    }

    Class& operator = (const computed_value<Class, Type, Dependencies...>& c)
    {
        /*if (task) {*/
            /*delete task;*/
        /*}*/
        task = c.task;
        return *static_cast<Class*>(this);
    }

#if 1
    Class& operator = (computed_value<Class, Type, Dependencies...>&& c)
    {
        /*if (task) {*/
            /*delete task;*/
        /*}*/
        task = c.task;
        return *static_cast<Class*>(this);
    }
#endif

    operator const Type& () const
    {
        return *task;
    }

    /*Type& v() const { return *task; }*/
    const Type& v() const { return *task; }

    static const bool composite = false;

    virtual
    std::ostream&
    output(std::ostream& os) const override
    {
        return os << v();
    }

    virtual
        md5_digest&
        to_md5(md5_digest& md5) const override
        {
            return task->to_md5(md5);
        }

    virtual
        size_t hash() const
        {
            std::hash<dependencies_type> h;
            return h(task->dependencies);
        }

    bool operator == (const Class& v) const
    {
        return task == v.task;
    }

    struct iterator {
        /*typedef computed_value<Class, Type, Dependencies...> value_type;*/
        typedef const Class value_type;
        /*computed_value<Class, Type, Dependencies...> val;*/
        const Class* val;
        bool end;
        iterator(const value_type& v) : val(&v) {}
        iterator() : val(), end(true) {}
        iterator(const iterator& i) : val(i.val) {}
        const value_type& operator * () const { return *val; }
        iterator& operator ++ () { val = NULL; return *this; }
        iterator& operator ++ (int) { val = NULL; return *this; }
        bool operator != (const iterator& i) const { return val != i.val; }
        bool operator == (const iterator& i) const { return val == i.val; }
        iterator& operator = (const iterator& i) { val = i.val; return *this; }
    };
    iterator begin() const { return {*this}; }
    iterator end() const { return {}; }
    typedef iterator const_iterator;

    /*static const bool skip_cache = false;*/
    /*static const bool skip_registry = false;*/
    static const bool skip_cache = true;
    static const bool skip_registry = true;
};


#if 0
template <typename Class, typename Type, typename... Dependencies>
struct async_computed_value : value<computed_type<Type>> {
    /*virtual Type do_compute(const Dependencies&...) = 0;*/
    typedef typename make_sequence<sizeof...(Dependencies)>::type indices;
    using value<computed_type<Type>>::output;
    typedef async_computed_value<Class, Type, Dependencies...> task_type;

    std::tuple<Dependencies...> dependencies;
    Type val_cache;
    bool fetched;

    async_computed_value(const std::tuple<Dependencies...>& dep)
        : dependencies(dep), val_cache(), fetched(false)
    { val = async_compute_cache(); }
    /*async_computed_value(std::tuple<Dependencies...>&& dep)*/
        /*: dependencies(std::move(dep))*/
    /*{ val = async_compute_cache(); }*/
    async_computed_value(const Dependencies&... dep)
        : dependencies(dep...), val_cache(), fetched(false)
    { val = async_compute_cache(); }
    /*async_computed_value(Dependencies&&... dep)*/
        /*: dependencies(std::move(dep)...)*/
    /*{ val = async_compute_cache(); }*/

    async_computed_value(const async_computed_value<Class, Type, Dependencies...>&) = delete;
    async_computed_value(async_computed_value<Class, Type, Dependencies...>&&) = delete;

    virtual ~async_computed_value() { /*MSG_DEBUG("destroying task " << dependencies);*/ }

    std::string cache_filename() const
    {
        std::stringstream f;
        md5_digest filename_digest;
        compute_digest_(filename_digest);
        f << ((std::string)filename_digest) << '_' << pretty_name();
        return f.str();
    }

    std::string dependencies_digest() const
    {
        std::stringstream f;
        append_digest_(f);
        return f.str();
    }

    void _fetch() const
    {
        task_type* mut = const_cast<task_type*>(this);
        if (!mut->fetched) {
            mut->fetched = true;
            mut->val_cache = launch::access(mut->val);
        }
    }

    const Type& v() const { _fetch(); return val_cache; }
    operator const Type& () const { _fetch(); return val_cache; }
    operator Type& () { _fetch(); return val_cache; }

    const Type* operator -> () const { _fetch(); return &val_cache; }

    void move(Type& dest) { _fetch(); dest = val_cache; }

    static std::string& cache_directory() { static std::string _ = DEFAULT_CACHE_DIR; return _; }

private:
    using value<computed_type<Type>>::val;

    Type compute_cache()
    {
        Type ret;
        std::string path = cache_directory() + cache_filename();
        file_stat fs(path);
        if (fs.exists) {
            if (!fs.readable) {
                MSG_ERROR("File " << path << " is not readable.",  "Check permissions for file " << path);
                unlink(path.c_str());
            } else if (load(path, ret)) {
                /*MSG_INFO("Reloading data from path " << path);*/
                return ret;
            } else {
                /*MSG_INFO("Cache invalid. Computing data and caching in " << path);*/
                compute_and_save(path, ret);
            }
        } else {
            /*MSG_INFO("Computing data and caching in " << path);*/
            compute_and_save(path, ret);
        }
        return ret;
    }

    computed_type<Type> async_compute_cache()
    {
        return launch::run<Type>([this] ()
                {
                    return compute_cache();
                });
    }

    bool load(const std::string& filename, Type& v)
    {
        // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
        ifile ifs(filename, std::ios_base::in | std::ios_base::binary);
        cache_input ar(ifs);
        std::string check;
        ar & check;
        if (check != dependencies_digest()) {
            return false;
        }
        ar & v;
        return true;
    }

    void compute_and_save(const std::string& filename, Type& v)
    {
        if (!Class::skip_cache) {
            // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
            ofile ofs(filename, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);
            cache_output ar(ofs);
            ar & dependencies_digest();
            v = compute_(indices(), dependencies);
            ar & v;
        } else {
            v = compute_(indices(), dependencies);
        }
    }

    std::string pretty_name() const
    {
        typedef
            typename std::remove_cv<
            typename std::remove_pointer<
            typename std::remove_reference<Type>
            ::type>::type>::type simple_type;
        return typeid(simple_type).name();
    }

    /*Type compute_(const Dependencies&... t)*/
    /*{*/
        /*return Class::do_compute(t...);*/
        /*Class* tmp = reinterpret_cast<Class*>(this);*/
        /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " compute_@" << tmp << " from @" << this);*/
        /*return tmp->do_compute(t...);*/
    /*}*/

    template <int... Indices>
    Type compute_(sequence<Indices...>, const std::tuple<Dependencies...>& t)
    {
        return Class::do_compute(std::get<Indices>(t)...);
        /*Class* tmp = reinterpret_cast<Class*>(this);*/
        /*MSG_DEBUG("type of *this = " << typeid(*this).name());*/
        /*MSG_DEBUG("type of Class = " << typeid(Class).name());*/
        /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " compute_@" << tmp << " from @" << this);*/
        /*return tmp->do_compute(std::get<Indices>(t)...);*/
    }

    template <int I, int N> struct tuple_digest_ {
        static void compute(md5_digest& md5, const std::tuple<Dependencies...>& t)
        {
            /*md5 << std::get<I>(t);*/
            std::get<I>(t).to_md5(md5);
            /*std::cout << "[compute] digesting #" << I << ' ' << std::get<I>(t) << std::endl;*/
            tuple_digest_<I + 1, N>::compute(md5, t);
        }

        static void to_md5(md5_digest& md5, const std::tuple<Dependencies...>& t)
        {
            std::get<I>(t).to_md5(md5);
            /*std::cout << "[compute] digesting #" << I << ' ' << std::get<I>(t) << std::endl;*/
            tuple_digest_<I + 1, N>::to_md5(md5, t);
        }

        static void append(std::stringstream& s, const std::tuple<Dependencies...>& t)
        {
            md5_digest md5;
            /*md5 << std::get<I>(t);*/
            std::get<I>(t).to_md5(md5);
            std::string dig = md5;
            /*std::cout << "[append] digesting #" << I << ' ' << std::get<I>(t) << ": " << dig << std::endl;*/
            s << dig;
            tuple_digest_<I + 1, N>::append(s, t);
        }
    };

    template <int I> struct tuple_digest_<I, I> {
        static void compute(md5_digest&, const std::tuple<Dependencies...>&) {}
        static void to_md5(md5_digest&, const std::tuple<Dependencies...>&) {}
        static void append(std::stringstream&, const std::tuple<Dependencies...>&) {}
    };

    void compute_digest_(md5_digest& md5) const
    {
        tuple_digest_<0, sizeof...(Dependencies)>::compute(md5, dependencies);
    }

    void append_digest_(std::stringstream& s) const
    {
        tuple_digest_<0, sizeof...(Dependencies)>::append(s, dependencies);
    }

    typedef std::unordered_map<std::tuple<Dependencies...>, task_type*> registry_t;

public:
#if 1
    static registry_t& registry() { static registry_t _; return _; }

    static task_type* get(const std::tuple<Dependencies...>& t)
    {
        task_type*& ret = registry()[t];
        if (!ret) {
            ret = new task_type(t);
        }
        return ret;
    }

    static task_type* get(const Dependencies&... t)
    {
        return get(std::make_tuple(t...));
    }

    static void clear_registry() { registry().clear(); }
#else
    static Class* get(std::tuple<Dependencies...>&& t)
    {
        return new Class(t);
    }

    static Class* get(const std::tuple<Dependencies...>& t)
    {
        return new Class(t);
    }
#endif

    virtual std::ostream& output(std::ostream& os) const override
    {
        return os << "<FUTURE(" << typeid(Type).name() << ")>";
    }

    virtual
        md5_digest&
        to_md5(md5_digest& md5) const
        {
            tuple_digest_<0, sizeof...(Dependencies)>::to_md5(md5, dependencies);
            return md5;
        }
};
#endif

template <typename T>
struct naked_type {
    typedef typename std::remove_cv<
            typename std::remove_pointer<
            typename std::remove_reference<T>
            ::type>::type>::type type;
};

template <bool val, typename TASK_CLASS, typename... Elems> struct detect_composite_impl;
template <typename TASK_CLASS, typename... Elems>
struct detect_composite_impl<true, TASK_CLASS, Elems...> {
    static const bool composite = true;
    typedef std::vector<TASK_CLASS> type;
};

template <typename TASK_CLASS, typename... Elems>
struct detect_composite_impl<false, TASK_CLASS, Elems...> {
    static const bool composite = false;
    typedef TASK_CLASS type;
};

template <typename TASK_CLASS, typename... Elems>
struct detect_composite {
    typedef detect_composite_impl<
        if_any_type<predicate::is_composite, typename naked_type<Elems>::type...>::value,
        TASK_CLASS,
        Elems...> impl;
    typedef typename impl::type type;
    static const bool composite = impl::composite;
};

template <bool composite, typename TASK_CLASS, typename... Elems>
struct make_task_helper;

template <typename TASK_CLASS, typename... Elems>
struct make_task_helper<true, TASK_CLASS, Elems...> {
    static std::vector<TASK_CLASS> make(const std::tuple<Elems...>& t)
    {
        std::vector<TASK_CLASS> ret;
        cartesian_product<Elems...> cp(t);
        do {
            /*ret.emplace_back(TASK_CLASS::get(*cp));*/
            /*MSG_DEBUG("make_task " << (*cp));*/
            /*ret.emplace_back(*cp);*/
            cp.push_to(ret);
        } while (cp.next());
        return ret;
    }
};



template <typename TASK_CLASS, typename... Elems>
struct make_task_helper<false, TASK_CLASS, Elems...> {
    static TASK_CLASS make(const std::tuple<Elems...>& t) { return TASK_CLASS(t); }
};


template <typename TASK_CLASS, typename... Elems>
auto make_task(const std::tuple<Elems...>& t)
    -> typename detect_composite<TASK_CLASS, Elems...>::type
{
    /*MSG_DEBUG("make_task with " << t);*/
    return make_task_helper<detect_composite<TASK_CLASS, Elems...>::composite, TASK_CLASS, Elems...>::make(t);
}

template <typename TASK_CLASS, typename... Elems>
auto make_task_composite(const std::tuple<Elems...>& t)
    -> decltype(make_task_helper<true, TASK_CLASS, Elems...>::make)
{
    return make_task_helper<true, TASK_CLASS, Elems...>::make(t);
}

#if 1
template <typename TASK_CLASS, typename... Elems>
auto make_task(std::tuple<Elems...>&& t)
    -> typename detect_composite<TASK_CLASS, Elems...>::type
{
    /*MSG_DEBUG("make_task with " << t);*/
    /*return make_task_helper<detect_composite<TASK_CLASS, Elems...>::composite, TASK_CLASS, Elems...>::make(std::forward<std::tuple<Elems...>>(t));*/
    return make_task_helper<detect_composite<TASK_CLASS, Elems...>::composite, TASK_CLASS, Elems...>::make(t);
}

template <typename TASK_CLASS, typename... Elems>
std::vector<TASK_CLASS> make_task_composite(std::tuple<Elems...>&& t)
{
    /*return make_task_helper<true, TASK_CLASS, Elems...>::make(std::forward<std::tuple<Elems...>>(t));*/
    return make_task_helper<true, TASK_CLASS, Elems...>::make(t);
}
#endif

template <typename Class>
struct computed_collection : collection<std::vector<Class>> {
    typedef std::vector<Class> value_type;

    value_type value_cache;

    template <typename... Dependencies>
    computed_collection(const std::tuple<Dependencies...>& t)
        /*: value<std::vector<Class>>({make_task<Class>(t)})*/
        : collection<std::vector<Class>>{value_cache}
    {
        /*value_cache = make_task_composite<Class>(t);*/
        value_cache = make_task<Class>(t);
    }

    template <typename... Dependencies>
    computed_collection(const Dependencies&... deps)
        /*: value<std::vector<Class>>({make_task<Class>(t)})*/
        : collection<std::vector<Class>>{value_cache}
    {
        value_cache = make_task_composite<Class>(std::make_tuple(deps...));
        /*value_cache = make_task<Class>(std::make_tuple(deps...));*/
    }

    operator std::vector<Class>& ()
    {
        return value_cache;
    }

    size_t size() const { return value_cache.size(); }

    const Class& operator [] (size_t i) const { return value_cache[i]; }

    virtual size_t hash() const
    {
        size_t accum = 0xdeadbeef * value_cache.size();
        for (const auto& x: value_cache) {
            accum ^= x.hash();
        }
        return accum;
    }

    bool operator == (const computed_collection<Class>& cc) const
    {
        return value_cache == cc.value_cache;
    }

    static const bool composite = true;
};
}

namespace std {
    template <typename C, typename T, typename... D>
        struct hash<cache::computed_value<C, T, D...>>
            : hash<cache::with_output_func> {};

    template <typename T>
        struct hash<std::future<T>> {
            size_t operator () (const std::future<T>&) const
            {
                MSG_ERROR("Hash std::future! not implemented!", "Fix the code");
                return 0;
            }
        };
}

#endif

