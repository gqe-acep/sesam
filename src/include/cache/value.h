/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_CACHE_VALUE_H_
#define _SPEL_CACHE_VALUE_H_

#include <future>
#include <functional>

template <bool _if, typename _then, typename _else>
    struct ifelse;
template <typename _then, typename _else>
    struct ifelse<true, _then, _else> { typedef _then type; };
template <typename _then, typename _else>
    struct ifelse<false, _then, _else> { typedef _else type; };

template <typename X>
bool operator == (const std::future<X>&, const std::future<X>&) { return false; }

namespace cache {

    template <typename T> struct value;

template <typename T>
    struct value_traits {
        typedef void operator_bracket_return;
        static void helper();
    };

template <typename V>
    struct value_traits<std::vector<V>> {
        typedef value<V> operator_bracket_return;
        
        static operator_bracket_return
        helper(const value<std::vector<V>>& v, size_t i)
        {
            return {v.val[i]};
        }
    };

template <typename V>
    struct value_traits<std::vector<value<V>>> {
        typedef const value<V>& operator_bracket_return;
        
        static operator_bracket_return
        helper(const value<std::vector<V>>& v, size_t i)
        {
            return v.val[i];
        }
    };

#if 0
struct collection_helper {
        template <typename V, typename A>
            static void append(std::vector<V, A>& c, const V& x) { c.push_back(x); }
        template <typename V, typename A>
            static void append(std::set<V, A>& c, const V& x) { c.insert(x); }
        template <typename V, typename A>
            static void append(std::vector<V, A>& c, const V& x) { c.push_back(x); }
        template <typename K, typename V, typename A, typename C>
            static void append(std::map<K, V, A, C>& c, const std::pair<K, V>& x) { c.insert(x); }
    };
#endif

template <typename Type, bool is_ptr> struct output_helper;
template <typename Type> struct output_helper<Type, true> {
    static std::ostream& _ (std::ostream& os, Type v)
    {
        return os << "&(" << (*v) << ')';
    }
};
template <typename Type> struct output_helper<Type, false> {
    static std::ostream& _ (std::ostream& os, const Type& v)
    {
        return os << v;
    }
};

template <typename FutType> struct output_helper<std::future<FutType>, false> {
    static std::ostream& _ (std::ostream& os, const std::future<FutType>&)
    {
        return os << "<FUTURE(" << typeid(FutType).name() << ")>";
    }
};


template <typename TYPE>
struct value_base : with_output_func {
    TYPE val;
    typedef std::tuple<TYPE> dependencies_type;
    value_base() : val() {}
    value_base(const TYPE& v) : val(v) {}
    value_base(TYPE&& v) : val(v) {}
    value_base(const value_base<TYPE>& v) : val(v.val) {}
    value_base(value_base<TYPE>&& v) : val(v.val) {}
    virtual ~value_base() {}
    value_base<TYPE>& operator = (const value_base<TYPE>& v)
    {
        val = v.val;
        return *this;
    }
    value_base<TYPE>& operator = (value_base<TYPE>&& v)
    {
        val = v.val;
        return *this;
    }
};


template <typename TYPE>
struct value_impl : value_base<TYPE> {
    using value_base<TYPE>::value_base;
    using value_base<TYPE>::val;
    using value_base<TYPE>::operator =;
    TYPE* operator -> () const { return const_cast<TYPE*>(&val); }
};


template <typename TYPE>
struct value_impl<const TYPE> : value_base<const TYPE> {
    using value_base<const TYPE>::value_base;
    using value_base<const TYPE>::val;
    using value_base<const TYPE>::operator =;
    const TYPE* operator -> () const { return &val; }
};


template <typename TYPE>
struct value_impl<const TYPE*> : value_base<const TYPE*> {
    using value_base<const TYPE*>::value_base;
    using value_base<const TYPE*>::val;
    using value_base<const TYPE*>::operator =;
    const TYPE* operator -> () const { return val; }
};


template <typename TYPE>
struct value_impl<TYPE*> : value_base<TYPE*> {
    using value_base<TYPE>::value_base;
    using value_base<TYPE>::val;
    using value_base<TYPE>::operator =;
    TYPE* operator -> () const { return val; }
};


template <typename TYPE>
struct value : value_impl<TYPE> {
    typedef value_traits<TYPE> traits;
    typedef TYPE value_type;
    /*TYPE val;*/
    using value_impl<TYPE>::value_impl;
    using value_impl<TYPE>::val;

    struct iterator {
        typedef value<TYPE> value_type;
        value<TYPE> val;
        bool end;
        iterator(const value<TYPE>& v) : val(v), end(false) {}
        iterator() : val(), end(true) {}
        iterator(const iterator& i) : val(i.val), end(i.end) {}
        value_type operator * () const { return val; }
        iterator& operator ++ () { end = true; return *this; }
        iterator& operator ++ (int) { end = true; return *this; }
        bool operator != (const iterator& i) const { return end != i.end; }
        bool operator == (const iterator& i) const { return end == i.end; }
        iterator& operator = (const iterator& i) { val = i.val; end = i.end; return *this; }
    };
    iterator begin() const { return {*this}; }
    iterator end() const { return {}; }
    typedef iterator const_iterator;
    /*operator const TYPE& () const { return val; }*/
    operator const TYPE& () const { return val; }

    /*virtual TYPE& v() const { return *const_cast<TYPE*>(&val); }*/
    const TYPE& v() const { return val; }
    TYPE& v() { return val; }

    /*value() : val() {}*/
    /*value(const TYPE& v_) : val(v_) {}*/
    /*value(TYPE&& v_) : val(v_) {}*/

    virtual ~value() {}

    template <typename K>
        typename traits::operator_bracket_return
        operator [] (const K& k) const
        {
            return traits::helper(*this, k);
        }

    static const bool composite = false;

    virtual
    std::ostream&
    output(std::ostream& os) const
    {
        return output_helper<TYPE, std::is_pointer<typename std::remove_reference<TYPE>::type>::value>::_(os, val);
    }

    virtual
        md5_digest&
        to_md5(md5_digest& md5) const
        {
            return md5 << val;
        }
#if 0
    virtual
        cache_input&
        from_cache(cache_input& ci)
        {
            return ci & val;
        }

    virtual
        cache_output&
        to_cache(cache_output& ci)
        {
            return ci & val;
        }
#endif
    virtual size_t hash() const { return std::hash<TYPE>()(val); }

    virtual bool operator == (const value<TYPE>& v_) const
    {
        return val == v_.val;
    }

    template <typename CACHE_FILE>
        CACHE_FILE& file_io(CACHE_FILE& cf) { return cf & val; }
};


template <typename INT_TYPE>
struct range : with_output_func {
    typedef INT_TYPE value_type;
    INT_TYPE min, max;
    struct iterator {
        typedef value<INT_TYPE> value_type;
        INT_TYPE cursor;
        iterator() : cursor(0) {}
        iterator(INT_TYPE i) : cursor(i) {}
        iterator(const iterator& i) : cursor(i.cursor) {}
        value_type operator * () const { return {cursor}; }
        iterator& operator ++ () { cursor += 1; return *this; }
        iterator& operator ++ (int) { cursor += 1; return *this; }
        bool operator != (const iterator& i) const { return cursor != i.cursor; }
        bool operator == (const iterator& i) const { return cursor == i.cursor; }
        iterator& operator = (const iterator& i) { cursor = i.cursor; return *this; }
    };
    typedef iterator const_iterator;
    iterator begin() const { return {min}; }
    iterator end() const { return {max}; }

    static const bool composite = true;

    range(INT_TYPE min_, INT_TYPE max_) : min(min_), max(max_) {}

    std::ostream& output(std::ostream& os) const
    {
        return os << '[' << min << ',' << max << ')';
    }

    virtual
        md5_digest&
        to_md5(md5_digest& md5) const
        {
            return md5 << min << max;
        }
#if 0
    virtual
        cache_input&
        from_cache(cache_input& ci)
        {
            return ci & min & max;
        }

    virtual
        cache_output&
        to_cache(cache_output& ci)
        {
            return ci & min & max;
        }
#endif
    virtual size_t hash() const { std::hash<INT_TYPE> h; return h(min) ^ h(max); }
};


template <typename K> struct no_const { typedef K type; };
template <typename K> struct no_const<const K> { typedef K type; };
template <typename K> struct no_const<const K*> { typedef K* type; };
template <typename K, typename V> struct no_const<std::pair<const K, V>> { typedef std::pair<K, V> type; };

template <typename X>
struct collection_value_type {
    typedef typename ifelse<
                is_value<X>::value,
                typename no_const<X>::type,
                value<X>>::type type;
};

template <typename X, bool _> struct coll_value_to_md5_impl;
template <typename X>
struct coll_value_to_md5_impl<X, true> {
    static void _(md5_digest& md5, const X& x) { x.to_md5(md5); }
};
template <typename X>
struct coll_value_to_md5_impl<X, false> {
    static void _(md5_digest& md5, const X& x) { md5 << x; }
};

template <typename X>
void coll_value_to_md5(md5_digest& md5, const X& x)
{
    coll_value_to_md5_impl<X, is_value<X>::value>::_(md5, x);
}


template <typename COLL_TYPE>
struct collection : with_output_func {
    typedef typename COLL_TYPE::value_type value_type;
    const COLL_TYPE& coll;
    struct iterator {
        typedef typename collection_value_type<typename COLL_TYPE::value_type>::type value_type;
        /*typedef value<typename no_const<typename COLL_TYPE::value_type>::type> value_type;*/
        typename COLL_TYPE::const_iterator cursor;
        iterator() : cursor() {}
        iterator(const typename COLL_TYPE::const_iterator& i) : cursor(i) {}
        iterator(const iterator& i) : cursor(i.cursor) {}
        value_type operator * () const { return *cursor; }
        iterator& operator ++ () { ++cursor; return *this; }
        iterator& operator ++ (int) { ++cursor; return *this; }
        bool operator != (const iterator& i) const { return cursor != i.cursor; }
        bool operator == (const iterator& i) const { return cursor == i.cursor; }
        iterator& operator = (const iterator& i) { cursor = i.cursor; return *this; }
    };
    typedef iterator const_iterator;
    iterator begin() const { return {coll.begin()}; }
    iterator end() const { return {coll.end()}; }

    static const bool composite = true;

    collection(const COLL_TYPE& c) : coll(c) {}

    std::ostream& output(std::ostream& os) const
    {
        auto i = coll.begin();
        auto j = coll.end();
        if (i == j) {
            return os << "{}";
        }
        os << '{' << (*i);
        if (i != j) {
            for (++i; i != j; ++i) {
                os << ", " << (*i);
            }
        }
        return os << '}';
    }

    virtual
        md5_digest&
        to_md5(md5_digest& md5) const
        {
            for (auto& x: coll) {
                /*md5 << x;*/
                coll_value_to_md5(md5, x);
            }
            return md5;
        }
#if 0
    virtual
        cache_input&
        from_cache(cache_input& ci)
        {
            size_t size;
            ci & size;
            value_type tmp;
            for (size_t i = 0; i < size; ++i) {
                ci & tmp;
                collection_helper::append(coll, tmp);
            }
            return ci;
        }

    virtual
        cache_output&
        to_cache(cache_output& ci)
        {
            size_t size = coll.size();
            ci & size;
            for (auto& x: coll) {
                ci & x;
            }
            return ci;
        }
#endif
    virtual size_t hash() const
    {
        std::hash<typename ifelse<std::is_base_of<with_output_func, value_type>::value,
                                  with_output_func,
                                  value_type>::type> h;
        size_t accum = 0xdeadbeef;
        for (auto& x: coll) {
            accum ^= h(x);
        }
        return accum;
    }
};

}

#if 0
template <typename Elem>
void _print_elem(std::ostream& os, const typename std::enable_if<!std::is_pointer<Elem>::value, cache::value<Elem>>::type& e) { os << e.v(); }

template <typename Elem>
void _print_elem(std::ostream& os, const typename std::enable_if<std::is_pointer<Elem>::value, cache::value<Elem>>::type& e) { os << "&(" << *e.v() << ')'; }
#endif

template <typename T>
typename std::enable_if<std::is_base_of<cache::with_output_func, T>::value, std::ostream>::type&
operator << (std::ostream& os, T& wof)
{
    return wof.output(os);
}

namespace std {
    template <>
        struct hash<cache::with_output_func> {
            size_t operator () (const cache::with_output_func& wof) const { return wof.hash(); }
        };

    template <typename V>
        struct hash<cache::value<V>> : hash<cache::with_output_func> {};

    template <typename INT_TYPE>
        struct hash<cache::range<INT_TYPE>> : hash<cache::with_output_func> {};

    template <typename V, typename A>
        struct hash<std::vector<V, A>> {
            size_t _helper(const cache::with_output_func& wof) const
            {
                return wof.hash();
            }

            template <typename X>
                typename std::enable_if<!cache::is_value<X>::value, size_t>::type
                _helper(const X& x) const
                {
                    return hash<X>()(x);
                }

            size_t operator () (const std::vector<V, A>& vec) const
            {
                size_t accum = 0xdeadbeef * vec.size();
                for (const V& x: vec) {
                    accum ^= _helper(x);
                }
                return accum;
            }
        };

    template <typename V>
        struct hash<collection<V>> {
            size_t operator () (const collection<V>& vec) const
            {
                size_t accum = 0xdeadbeef * std::hash<size_t>()(vec.size());
                std::hash<V> h;
                for (const V& x: vec) {
                    accum ^= h(x);
                }
                return accum;
            }
        };

    template <typename K, typename V, typename A, typename C>
        struct hash<std::map<K, V, A, C>> {
            size_t operator () (const std::map<K, V, A, C>& map) const
            {
                size_t accum = 0xdeadbeef * map.size();
                hash<K> hk;
                hash<V> hv;
                for (const auto& kv: map) {
                    accum ^= hk(kv.first) ^ hv(kv.second);
                }
                return accum;
            }
        };

    template <typename K, typename V, typename H, typename P, typename A>
        struct hash<std::unordered_map<K, V, H, P, A>> {
            size_t operator () (const std::unordered_map<K, V, H, P, A>& map) const
            {
                size_t accum = 0xdeadbeef * map.size();
                hash<K> hk;
                hash<V> hv;
                for (auto& kv: map) {
                    accum ^= h(kv.first) ^ h(kv.second);
                }
                return accum;
            }
        };
}

#endif

