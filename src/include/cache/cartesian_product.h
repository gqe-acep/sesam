/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_CACHE_CARTESIAN_PRODUCT_H_
#define _SPEL_CACHE_CARTESIAN_PRODUCT_H_

namespace cache {

template <typename ITEM>
    using item_iterator = typename ITEM::iterator;

template <typename ITEM>
    struct iterator_context {
        typedef item_iterator<ITEM> iterator;
        item_iterator<ITEM> begin, end, cursor;
        iterator_context() : begin(), end(), cursor() {}
        iterator_context(const ITEM& i)
            : begin(i.begin()), end(i.end()), cursor(i.begin())
        {}
        iterator_context(const iterator_context<ITEM>& ic)
            : begin(ic.begin), end(ic.end), cursor(ic.cursor)
        {}
        iterator_context<ITEM>& operator = (const iterator_context<ITEM>& i)
        {
            begin = i.begin;
            end = i.end;
            cursor = i.cursor;
        }
        bool next() { ++cursor; if (cursor == end) { cursor = begin; return true; } return false; }
        typename item_iterator<ITEM>::value_type operator * () const { return *cursor; } 
    };

template <typename Item>
    struct do_next {
        typedef bool return_type;
        bool operator () (Item& i) const
        {
            return i.next();
        }
    };

template <typename Item>
    struct do_get {
        typedef typename std::remove_const<typename std::remove_reference<typename item_iterator<Item>::value_type>::type>::type return_type;
        return_type operator () (const Item& i) const
        {
            return *i;
        }
    };

struct cartesian_product_at_end_exception : std::exception {};

template <typename... ITEMS>
    struct cartesian_product {
        typedef typename make_sequence<sizeof...(ITEMS)>::type indices;
        template <typename X> struct to_bool { typedef bool type; };
        typedef std::tuple<iterator_context<ITEMS>...> iter_type;
        iter_type iter;
        bool at_end;
        cartesian_product() : iter(), at_end(true) {}
        cartesian_product(const ITEMS&... x)
            : iter(x...), at_end(false)
        { _debug_types(); }
        cartesian_product(const std::tuple<ITEMS...>& x)
            : iter(x), at_end(false)
        { _debug_types(); }
        cartesian_product(const cartesian_product<ITEMS...>& cp)
            : iter(cp.iter), at_end(cp.at_end)
        { _debug_types(); }

        template <int I, int N>
            struct _debug_types_impl {
                typedef typename std::tuple_element<I, std::tuple<ITEMS...>>::type etype;
                typedef typename std::tuple_element<I, iter_type>::type::iterator::value_type itype;
                void operator () () {
                    /*MSG_DEBUG('#' << I << ": " << typeid(etype).name() << " / " << typeid(itype).name());*/
                    _debug_types_impl<I + 1, N>()();
                }
            };

        template <int N>
            struct _debug_types_impl<N, N> {
                void operator () () {}
            };

        void _debug_types() const
        {
            _debug_types_impl<0, sizeof...(ITEMS)>()();
        }

        cartesian_product<ITEMS...>& operator = (const cartesian_product& cp)
        {
            iter = cp.iter;
            at_end = cp.at_end;
            return *this;
        }

        typedef typename indices::template with_backward_recursion<do_next,  iterator_context<ITEMS>...> nexter;
        typedef typename indices::template with_processor<do_get, iterator_context<ITEMS>...> getter;

        bool next()
        {
            if (!at_end) {
                at_end = nexter::process(iter);
            }
            return !at_end;
        }

        template <typename I> struct _get;
        template <int... Indices>
            struct _get<sequence<Indices...>> {
                static
                std::tuple<typename item_iterator<ITEMS>::value_type...>
                get(const std::tuple<iterator_context<ITEMS>...>& iter)
                {
                    return std::make_tuple(*std::get<Indices>(iter)...);
                }

                template <typename T>
                    static
                    void push_to(std::vector<T>& vec, const std::tuple<iterator_context<ITEMS>...>& iter)
                    {
                        vec.emplace_back(*std::get<Indices>(iter)...);
                    }
            };

        std::tuple<typename item_iterator<ITEMS>::value_type...> operator * () const
        {
            if (!at_end) {
                /*return getter::process(iter);*/
                return _get<indices>::get(iter);
            }
            throw cartesian_product_at_end_exception();
        }

        template <typename T>
            void push_to(std::vector<T>& vec)
            {
                if (!at_end) {
                    return _get<indices>::push_to(vec, iter);
                }
                throw cartesian_product_at_end_exception();
            }

        struct iterator {
            typedef std::tuple<typename item_iterator<ITEMS>::value_type...> value_type;
            cartesian_product<ITEMS...> cp;

            value_type operator * () const { return *cp; }

            iterator& operator ++ () { cp.next(); return *this; }
            iterator& operator ++ (int) { cp.next(); return *this; }
            bool operator == (const iterator& i) { return i.cp.at_end && cp.at_end; }
            bool operator != (const iterator& i) { return !(*this == i); }
        };

        cartesian_product<ITEMS...> copy() const
        {
            cartesian_product<ITEMS...> ret;
            ret.iter = iter;
            ret.at_end = at_end;
            return ret;
        }

        iterator begin() { return {*this}; }
        iterator end() { return {{}}; }
    };

/*template <typename... E>*/
    /*cartesian_product<E...> make_cartesian_product(E&&... x)*/
    /*{*/
        /*return {x...};*/
    /*}*/

template <typename... E>
    cartesian_product<E...> make_cartesian_product(const std::tuple<E...>& x)
    {
        return {x};
    }

}

#endif

