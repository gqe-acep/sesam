/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_MD5_H_
#define _SPEL_MD5_H_

#include <iostream>
#include <utility>
#include <string>
#include <sstream>
#include <iomanip>
#include <tuple>
#include <future>
#include <type_traits>
#include <vector>
#include <map>

#include "labelled_matrix.h"

#if 0
namespace detail {
    template<typename T>
        struct has_const_iterator
        {
            private:
                typedef char                      yes;
                typedef struct { char array[2]; } no;

                template<typename C> static yes test(typename C::const_iterator*);
                template<typename C> static no  test(...);
            public:
                static const bool value = sizeof(test<T>(0)) == sizeof(yes);
                typedef T type;
        };

    template <typename T>
        struct has_begin_end
        {
            template<typename C> static char (&f(typename std::enable_if<
                        std::is_same<decltype(static_cast<typename C::const_iterator (C::*)() const>(&C::begin)),
                        typename C::const_iterator(C::*)() const>::value, void>::type*))[1];

            template<typename C> static char (&f(...))[2];

            template<typename C> static char (&g(typename std::enable_if<
                        std::is_same<decltype(static_cast<typename C::const_iterator (C::*)() const>(&C::end)),
                        typename C::const_iterator(C::*)() const>::value, void>::type*))[1];

            template<typename C> static char (&g(...))[2];

            static bool const beg_value = sizeof(f<T>(0)) == 1;
            static bool const end_value = sizeof(g<T>(0)) == 1;
        };

    template <typename T>
        struct is_container
        : std::integral_constant<
          bool,
          has_const_iterator<T>::value
          && has_begin_end<T>::beg_value
          && has_begin_end<T>::end_value> {};
}
#endif

#ifdef USE_OPENSSL_MD5
#include <openssl/evp.h>

struct md5_digest {
    EVP_MD_CTX md_ctx;
    bool blocked;

    md5_digest()
    {
        EVP_DigestInit(&md_ctx, EVP_md5());
        blocked = false;
    }

    ~md5_digest()
    {
        EVP_MD_CTX_cleanup(&md_ctx);
    }

    operator std::string ()
    {
        unsigned char buf[EVP_MAX_MD_SIZE];
        unsigned int s;
        blocked = true;
        EVP_DigestFinal_ex(&md_ctx, buf, &s);
        std::stringstream ret;
        for (size_t i = 0; i < s; ++i) {
            ret << std::setw(2) << std::hex << (int)buf[i];
        }
        return ret.str();
    }

    template <typename ARITH>
        md5_digest& __upd_arith(ARITH x)
        {
            EVP_DigestUpdate(&md_ctx, &x, sizeof(ARITH));
            return *this;
        }

    md5_digest& update(short int x) { return __upd_arith(x); }
    md5_digest& update(unsigned short int x) { return __upd_arith(x); }
    md5_digest& update(int x) { return __upd_arith(x); }
    md5_digest& update(unsigned int x) { return __upd_arith(x); }
    md5_digest& update(long int x) { return __upd_arith(x); }
    md5_digest& update(unsigned long int x) { return __upd_arith(x); }
    md5_digest& update(long long int x) { return __upd_arith(x); }
    md5_digest& update(unsigned long long int x) { return __upd_arith(x); }
    md5_digest& update(float x) { return __upd_arith(x); }
    md5_digest& update(double x) { return __upd_arith(x); }
    md5_digest& update(long double x) { return __upd_arith(x); }

    template <typename _Iterator>
        md5_digest& update(_Iterator begin, _Iterator end)
        {
            for (; begin != end; ++begin) {
                (*this) << (*begin);
            }
            return *this;
        }

        md5_digest&
        update(const std::string& s)
        {
            /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " <- std::string");*/
            EVP_DigestUpdate(&md_ctx, s.c_str(), s.size());
            return *this;
        }

        md5_digest&
        update(bool b)
        {
            int i = (int) b;
            /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " <- bool");*/
            return update(i);
        }

        md5_digest&
        update(char c)
        {
            static char buf[8] = " "; /* endian safe */
            /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " <- char");*/
            buf[0] = c;
            EVP_DigestUpdate(&md_ctx, buf, 1);
            return *this;
        }

        /*md5_digest&*/
        /*update(const cache::with_output_func& wof)*/
        /*{*/
            /*wof.to_md5(*this);*/
            /*return *this;*/
        /*}*/

    template <typename T>
        md5_digest&
        update(const std::vector<T>& vec)
        {
            for (const T& x: vec) { (*this) << x; }
            return *this;
        }

    template <typename T>
        md5_digest&
        update(const T* ptr)
        {
            /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " <- " << typeid(T).name());*/
            /*MSG_DEBUG("update const ptr" << ptr);*/
            return (*this) << (*ptr);
        }

    template <typename T>
        md5_digest&
        update(T* ptr)
        {
            /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " <- " << typeid(T).name());*/
            /*MSG_DEBUG("update ptr" << ptr);*/
            return (*this) << (*ptr);
        }

    /* just so it compiles. unused. */
    template <typename T>
        md5_digest&
        update(const std::future<T>&)
        {
            return *this;
        }

    template <typename F, typename S>
        md5_digest&
        update(const std::pair<F, S>& p)
        {
            /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " <- " << typeid(std::pair<F, S>).name());*/
            return (*this) << p.first << p.second;
        }
};

#else


struct md5_digest {
    typedef size_t context_type;
    size_t context;
    bool blocked;

    md5_digest()
    {
        context = 0xdeadb33f;
        blocked = false;
    }

    operator std::string ()
    {
        std::stringstream s;
        s << std::hex << context;
        return s.str();
        static unsigned char hex[16] = {'0', '1', '2', '3', '4', '5', '6', '7',
                                        '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        std::string buf;
        buf.resize(sizeof(context_type) * 2);
        auto tmp = context;
        for (size_t i = 0; i < (2 * sizeof(context_type)); ++i) {
            buf[i] = hex[tmp & 0xF];
            tmp >>= 4;
        }
        return buf;
    }

#define MAGIC_CONSTANT 0x0d853211

    md5_digest& blend(size_t other_context)
    {
#define HALFBITS (sizeof(context_type) * 4)
        auto tmp = (context * MAGIC_CONSTANT);
        context = (tmp >> HALFBITS) | (tmp << HALFBITS);
        /*context |= tmp;*/
        context ^= other_context;
        return *this;
    }

    template <typename Whatever>
        md5_digest& update(Whatever x)
        {
            /*MSG_WARNING("Using hash fallback in MD5 digest of type " << typeid(x).name());*/
            return blend(std::hash<Whatever>()(x));
        }

    template <typename _Iterator>
        md5_digest& update(_Iterator begin, _Iterator end)
        {
            for (; begin != end; ++begin) {
                (*this) << (*begin);
            }
            return *this;
        }

    template <typename T>
        md5_digest&
        update(const std::vector<T>& vec)
        {
            for (const T& x: vec) { (*this) << x; }
            return *this;
        }

    template <typename T>
        md5_digest&
        update(const T* ptr)
        {
            /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " <- " << typeid(T).name());*/
            /*MSG_DEBUG("update const ptr" << ptr);*/
            return (*this) << (*ptr);
        }

    template <typename T>
        md5_digest&
        update(T* ptr)
        {
            /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " <- " << typeid(T).name());*/
            /*MSG_DEBUG("update ptr" << ptr);*/
            return (*this) << (*ptr);
        }

    /* just so it compiles. unused. */
    template <typename T>
        md5_digest&
        update(const std::future<T>&)
        {
            MSG_ERROR(__FILE__ << ':' << __LINE__ << " SHOULDN'T BE USED", "Fix the code.");
            return *this;
        }

    template <typename F, typename S>
        md5_digest&
        update(const std::pair<F, S>& p)
        {
            /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " <- " << typeid(std::pair<F, S>).name());*/
            return (*this) << p.first << p.second;
        }
};

#endif

template <typename V, typename A>
    md5_digest&
    operator << (md5_digest& md, const std::vector<V, A>& vec)
    {
        /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " md5(" << typeid(t).name() << ' ' << t << ')');*/
        return md.update(vec.begin(), vec.end());
    }

template <typename K, typename V, typename C, typename A>
    md5_digest&
    operator << (md5_digest& md, const std::map<K, V, C, A>& map)
    {
        /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " md5(" << typeid(t).name() << ' ' << t << ')');*/
        return md.update(map.begin(), map.end());
    }

template <typename T>
    md5_digest&
    operator << (md5_digest& md, const T& t)
    {
        /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " md5(" << typeid(t).name() << ' ' << t << ')');*/
        return md.update(t);
    }

template <typename T>
    md5_digest&
    operator << (md5_digest& md, const T* t)
    {
        /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " md5(" << typeid(t).name() << ' ' << t << ')');*/
        return md.update(t);
    }

template <typename T>
    md5_digest&
    operator << (md5_digest& md, T* t)
    {
        /*MSG_DEBUG(__FILE__ << ':' << __LINE__ << " md5(" << typeid(t).name() << ' ' << t << ')');*/
        return md.update(t);
    }

template <typename T>
    md5_digest&
    operator << (md5_digest& md, const std::future<T>&)
    {
        MSG_ERROR("Requested digest of std::future<" << typeid(T).name() << '>', "Fix the code.");
        return md;
    }

#if 0
template <typename _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>
md5_digest& operator << (md5_digest& md5, const Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols>& mat)
{
    md5 << mat.innerSize() << mat.outerSize();
    for (int j = 0; j < mat.outerSize(); ++j) {
        for (int i = 0; i < mat.innerSize(); ++i) {
            md5 << mat(i, j);
        }
    }
    return md5;
}
#endif

template <typename _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>
md5_digest& operator << (md5_digest& md5, const Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols>& mat)
{
    const _Scalar* data = mat.data();
    for (int i = 0; i < mat.size(); ++i) {
        md5 << data[i];
    }
    return md5;
}



template <typename MAT, typename CLAB, typename RLAB>
md5_digest& operator << (md5_digest& md5, const labelled_matrix<MAT, CLAB, RLAB>& lmat)
{
    return md5 << lmat.row_labels << lmat.column_labels << lmat.data;
}
#endif

