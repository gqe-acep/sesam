/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_CACHE_ASYNC_H_
#define _SPEL_CACHE_ASYNC_H_

#define DEFAULT_CACHE_DIR "tmp/"


namespace cache {
template <bool _MultiThread>
    struct launch_impl;

template <>
    struct launch_impl<true> {
        static std::launch& policy()
        {
            static std::launch _ = std::launch::async | std::launch::deferred;
            /*static std::launch _ = std::launch::deferred;*/
            return _;
        }

        template <typename Type>
        using computed_type = std::future<Type>;

        template <typename Type, typename FSig>
        static computed_type<Type> run(FSig f)
        {
            return std::async(policy(), f);
        }

        template <typename Type>
        static Type access(computed_type<Type>& ct) { return ct.get(); }
    };

template <>
    struct launch_impl<false> {
        template <typename Type>
            using computed_type = Type;

        template <typename Type, typename FSig>
        static computed_type<Type> run(FSig f)
        {
            return f();
        }

        template <typename Type>
        static Type access(computed_type<Type>& ct) { return ct; }
    };

typedef launch_impl<false> launch;

template <typename Type>
using computed_type = launch::computed_type<Type>;

template <typename T> struct ptr_type_impl { typedef std::shared_ptr<T> type; };
template <typename T> struct ptr_type_impl<T*> { typedef std::shared_ptr<T> type; };
template <typename T> using ptr_type = typename ptr_type_impl<T>::type;

template <typename T> constexpr std::shared_ptr<T> ptr_value(T& x) { return std::shared_ptr<T>(&x); }
template <typename T> constexpr std::shared_ptr<T> ptr_value(T* x) { return std::shared_ptr<T>(x); }

template <typename Seq, typename Tup> struct pointerize_tuple_impl;
template <int...I, typename... E>
    struct pointerize_tuple_impl<sequence<I...>, std::tuple<E...>> {
        static constexpr
            std::tuple<ptr_type<E>...>
            _(const std::tuple<E...>& t)
            {
                return {ptr_value(std::get<I>(t)...)};
            }
    };

template <typename... E>
    static constexpr
    std::tuple<ptr_type<E>...>
    pointerize_tuple(const std::tuple<E...>& t)
    {
        return pointerize_tuple_impl<typename make_sequence<sizeof...(E)>::type, std::tuple<E...>>::_(t);
    }


template <typename TaskClass, typename Class, typename Type, typename... Dependencies>
    struct async_computed_value_base_base : value<computed_type<Type>> {
        typedef TaskClass task_type;
        using value<computed_type<Type>>::output;
        typedef std::tuple<std::shared_ptr<const Dependencies>...> dependencies_type;
        typedef typename make_sequence<sizeof...(Dependencies)>::type indices;
        dependencies_type dependencies;

        async_computed_value_base_base(const std::tuple<Dependencies...>& dep)
            : value<computed_type<Type>>(), dependencies(pointerize_tuple(dep)), val_cache(), fetched(false)
        {}
        async_computed_value_base_base(const Dependencies&... dep)
            : value<computed_type<Type>>(), dependencies(ptr_value(dep)...), val_cache(), fetched(false)
        {}
        async_computed_value_base_base(const async_computed_value_base_base<TaskClass, Class, Type, Dependencies...>&) = delete;
        async_computed_value_base_base(async_computed_value_base_base<TaskClass, Class, Type, Dependencies...>&&) = delete;
        virtual ~async_computed_value_base_base() {}

    protected:
        Type val_cache;
        bool fetched;
        using value<computed_type<Type>>::val;
    };

template <typename TaskClass, typename Class, typename Type, typename... Dependencies>
    struct async_computed_value_base_with_registry : async_computed_value_base_base<TaskClass, Class, Type, Dependencies...> {
        typedef async_computed_value_base_base<TaskClass, Class, Type, Dependencies...> base_type;
        using base_type::async_computed_value_base_base;
        typedef TaskClass task_type;
        using base_type::output;
        using base_type::dependencies;
        using base_type::val_cache;
        using base_type::fetched;
        typedef typename base_type::dependencies_type dependencies_type;
        typedef std::unordered_map<std::tuple<Dependencies...>, task_type*> registry_t;

        static registry_t& registry() { static registry_t _; return _; }
        static void clear_registry() { registry().clear(); }
        static task_type* get(const Dependencies&... t) { return get(ptr_value(t)...); }
        static task_type* get(const std::tuple<Dependencies...>& t)
        {
            task_type*& ret = registry()[pointerize_tuple(t)];
            if (!ret) {
                ret = new task_type(t);
            }
            return ret;
        }
    };

template <typename TaskClass, typename Class, typename Type, typename... Dependencies>
    struct async_computed_value_base_without_registry : async_computed_value_base_base<TaskClass, Class, Type, Dependencies...> {
        typedef async_computed_value_base_base<TaskClass, Class, Type, Dependencies...> base_type;
        using base_type::async_computed_value_base_base;
        typedef TaskClass task_type;
        using base_type::output;
        using base_type::dependencies;
        using base_type::val_cache;
        using base_type::fetched;
        typedef typename base_type::dependencies_type dependencies_type;

        static void clear_registry() {}
        static task_type* get(const Dependencies&... t) { return new task_type(t...); }
        static task_type* get(const std::tuple<Dependencies...>& t) { return new task_type(t); }
    };

template <bool _WithReg, typename TaskClass, typename Class, typename Type, typename... Dependencies>
    struct async_computed_value_base_impl;

template <typename TaskClass, typename Class, typename Type, typename... Dependencies>
    struct async_computed_value_base_impl<true, TaskClass, Class, Type, Dependencies...>
            : async_computed_value_base_with_registry<TaskClass, Class, Type, Dependencies...> {
        typedef async_computed_value_base_with_registry<TaskClass, Class, Type, Dependencies...> base_type;
        using base_type::async_computed_value_base_with_registry;
        using base_type::get;
        using base_type::clear_registry;
        typedef typename base_type::dependencies_type dependencies_type;
    };

template <typename TaskClass, typename Class, typename Type, typename... Dependencies>
    struct async_computed_value_base_impl<false, TaskClass, Class, Type, Dependencies...>
            : async_computed_value_base_without_registry<TaskClass, Class, Type, Dependencies...> {
        typedef async_computed_value_base_without_registry<TaskClass, Class, Type, Dependencies...> base_type;
        using base_type::async_computed_value_base_without_registry;
        using base_type::get;
        using base_type::clear_registry;
        typedef typename base_type::dependencies_type dependencies_type;
    };


template <typename TaskClass, typename Class, typename Type, typename... Dependencies>
    struct async_computed_value_base
            : async_computed_value_base_impl<!Class::skip_registry, TaskClass, Class, Type, Dependencies...> {
        typedef async_computed_value_base_impl<!Class::skip_registry, TaskClass, Class, Type, Dependencies...> base_type;
        typedef TaskClass task_type;
        using base_type::output;
        using base_type::val_cache;
        using base_type::dependencies;
        typedef typename base_type::dependencies_type dependencies_type;
        using base_type::async_computed_value_base_impl;

        std::string dependencies_digest() const
        {
            std::stringstream f;
            append_digest_(f);
            return f.str();
        }

        virtual void _fetch() const = 0;

        const Type& v() const { _fetch(); return val_cache; }
        operator const Type& () const { _fetch(); return val_cache; }
        operator Type& () { _fetch(); return val_cache; }

        const Type* operator -> () const { _fetch(); return &val_cache; }

        void move(Type& dest) { _fetch(); dest = val_cache; }

        template <int I, int N> struct tuple_digest_ {
            static void compute(md5_digest& md5, const dependencies_type& t)
            {
                /*md5 << std::get<I>(t);*/
                std::get<I>(t)->to_md5(md5);
                /*std::cout << "[compute] digesting #" << I << ' ' << std::get<I>(t) << std::endl;*/
                tuple_digest_<I + 1, N>::compute(md5, t);
            }

            static void to_md5(md5_digest& md5, const dependencies_type& t)
            {
                std::get<I>(t)->to_md5(md5);
                /*std::cout << "[compute] digesting #" << I << ' ' << std::get<I>(t) << std::endl;*/
                tuple_digest_<I + 1, N>::to_md5(md5, t);
            }

            static void append(std::stringstream& s, const dependencies_type& t)
            {
                md5_digest md5;
                /*md5 << std::get<I>(t);*/
                std::get<I>(t)->to_md5(md5);
                std::string dig = md5;
                /*std::cout << "[append] digesting #" << I << ' ' << std::get<I>(t) << ": " << dig << std::endl;*/
                s << dig;
                tuple_digest_<I + 1, N>::append(s, t);
            }
        };

        template <int I> struct tuple_digest_<I, I> {
            static void compute(md5_digest&, const dependencies_type&) {}
            static void to_md5(md5_digest&, const dependencies_type&) {}
            static void append(std::stringstream&, const dependencies_type&) {}
        };

        void compute_digest_(md5_digest& md5) const
        {
            tuple_digest_<0, sizeof...(Dependencies)>::compute(md5, dependencies);
        }

        void append_digest_(std::stringstream& s) const
        {
            tuple_digest_<0, sizeof...(Dependencies)>::append(s, dependencies);
        }

        virtual std::ostream& output(std::ostream& os) const override
        {
            return os << "<FUTURE(" << typeid(Type).name() << ")>";
        }

        virtual
            md5_digest&
            to_md5(md5_digest& md5) const
            {
                tuple_digest_<0, sizeof...(Dependencies)>::to_md5(md5, dependencies);
                return md5;
            }

        /*Type compute_(const Dependencies&... t)*/
        /*{*/
            /*return Class::do_compute(t...);*/
        /*}*/

        template <int... Indices>
        Type compute_(sequence<Indices...>, const dependencies_type& t)
        {
            return Class::do_compute(*std::get<Indices>(t)...);
        }

        std::string pretty_name() const
        {
            typedef
                typename std::remove_cv<
                typename std::remove_pointer<
                typename std::remove_reference<Type>
                ::type>::type>::type simple_type;
            return typeid(simple_type).name();
        }

        std::string cache_filename() const
        {
            std::stringstream f;
            md5_digest filename_digest;
            compute_digest_(filename_digest);
            f << ((std::string)filename_digest) << '_' << pretty_name();
            return f.str();
        }
    };

static inline std::string& cache_directory()
{
    static std::string _ = DEFAULT_CACHE_DIR;
    return _;
}

template <typename TaskClass, typename Class, typename Type, typename... Dependencies>
    struct async_computed_value_with_cache : async_computed_value_base<TaskClass, Class, Type, Dependencies...> {
        typedef async_computed_value_base<TaskClass, Class, Type, Dependencies...> base_type;
        using base_type::async_computed_value_base;
        using base_type::dependencies_digest;
        typedef typename make_sequence<sizeof...(Dependencies)>::type indices;
        using base_type::cache_filename;
        using base_type::dependencies;
        using base_type::compute_;

        Type compute_cache()
        {
            Type ret;
            std::string path = cache_directory() + cache_filename();
            file_stat fs(path);
            if (fs.exists) {
                if (!fs.readable) {
                    MSG_ERROR("File " << path << " is not readable.",  "Check permissions for file " << path);
                    unlink(path.c_str());
                } else if (load(path, ret)) {
                    MSG_INFO("Reloading data from path " << path);
                    return ret;
                } else {
                    MSG_INFO("Cache invalid. Computing data and caching in " << path);
                    compute_and_save(path, ret);
                }
            } else {
                MSG_INFO("Computing data and caching in " << path);
                compute_and_save(path, ret);
            }
            return ret;
        }

        computed_type<Type> async_compute_cache()
        {
            return launch::run<Type>([this] ()
                    {
                        return compute_cache();
                    });
        }


        bool load(const std::string& filename, Type& v)
        {
            // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
            ifile ifs(filename, std::ios_base::in | std::ios_base::binary);
            cache_input ar(ifs);
            std::string check;
            ar & check;
            if (check != dependencies_digest()) {
                return false;
            }
            ar & v;
            return true;
        }

        void compute_and_save(const std::string& filename, Type& v)
        {
            // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
            ofile ofs(filename, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);
            cache_output ar(ofs);
            ar & dependencies_digest();
            v = compute_(indices(), dependencies);
            ar & v;
        }
    };

template <typename TaskClass, typename Class, typename Type, typename... Dependencies>
    struct async_computed_value_without_cache : async_computed_value_base<TaskClass, Class, Type, Dependencies...> {
        typedef async_computed_value_base<TaskClass, Class, Type, Dependencies...> base_type;
        using base_type::async_computed_value_base;
        using base_type::dependencies;
        using base_type::compute_;
        typedef typename make_sequence<sizeof...(Dependencies)>::type indices;
        computed_type<Type> async_compute_cache()
        {
            return launch::run<Type>([this] ()
                    {
                        return compute_(indices(), dependencies);
                    });
        }
    };

template <bool _Cache, typename Class, typename Type, typename... Dependencies>
    struct async_computed_value_impl;

template <typename Class, typename Type, typename... Dependencies>
    struct async_computed_value;

template <typename Class, typename Type, typename... Dependencies>
    struct async_computed_value_impl<true, Class, Type, Dependencies...>
            : async_computed_value_with_cache<async_computed_value<Class, Type, Dependencies...>, Class, Type, Dependencies...> {
        typedef async_computed_value_with_cache<async_computed_value<Class, Type, Dependencies...>, Class, Type, Dependencies...> base_type;
        using base_type::async_computed_value_with_cache;
        using base_type::dependencies;
        using base_type::val_cache;
        using base_type::fetched;
    };

template <typename Class, typename Type, typename... Dependencies>
    struct async_computed_value_impl<false, Class, Type, Dependencies...>
            : async_computed_value_without_cache<async_computed_value<Class, Type, Dependencies...>, Class, Type, Dependencies...> {
        typedef async_computed_value_without_cache<async_computed_value<Class, Type, Dependencies...>, Class, Type, Dependencies...> base_type;
        using base_type::async_computed_value_without_cache;
        using base_type::dependencies;
        using base_type::val_cache;
        using base_type::fetched;
    };

template <typename Class, typename Type, typename... Dependencies>
struct async_computed_value : async_computed_value_impl<!Class::skip_cache, Class, Type, Dependencies...> {
    typedef async_computed_value_impl<!Class::skip_cache, Class, Type, Dependencies...> base_type;
    using base_type::async_computed_value_impl;
    using base_type::val;
    using base_type::async_compute_cache;
    typedef async_computed_value<Class, Type, Dependencies...> task_type;

    async_computed_value(const std::tuple<Dependencies...>& dep)
        : base_type(dep)
    { val = async_compute_cache(); }

    async_computed_value(const Dependencies&... dep)
        : base_type(dep...)
    { val = async_compute_cache(); }

    void _fetch() const
    {
        task_type* mut = const_cast<task_type*>(this);
        if (!mut->fetched) {
            mut->fetched = true;
            mut->val_cache = launch::access(mut->val);
        }
    }
};
} /* namespace cache */

#endif
