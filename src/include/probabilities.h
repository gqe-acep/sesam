/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_GENOPROBA_H_
#define _SPEL_GENOPROBA_H_

/*#include <Eigen/Core>*/
#include "eigen.h"
#include <exception>
#include <limits>
#include "polynom.h"
#include "labelled_matrix.h"
#include "proba_config.h"

typedef labelled_matrix<Eigen::MatrixXd, haplo_type, double> GenoProbMatrix;


struct out_of_segment_exception : public std::exception {};

using Eigen::ArrayXXi;
using Eigen::ArrayXd;
using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::RowVectorXd;

typedef double (*prob_func) (double);

// (1 - e^(-2d))/2
constexpr inline double prob_recomb(double dist) {
    return .5 - exp(-.02 * dist) * .5;
}

// 1 - (1 - e^(-2d))/2
constexpr inline double prob_no_recomb(double dist) {
    return .5 + exp(-.02 * dist) * .5;
}


constexpr inline double reverse_prob(double d)
{
    return exp(-.02 * d);
}



template <typename T>
struct cartesian_product {
    typedef typename T::iterator value_iterator;
    typedef typename T::value_type value_type;
    std::vector<T> sets;

    void push_back(const T& x) { sets.push_back(x); }
    void push_back(T&& x) { sets.push_back(x); }

    struct iterator {
        bool carry;
        std::vector<value_iterator> components;
        cartesian_product<T>* cp;

        iterator(cartesian_product<T>* _cp_, bool c)
            : carry(c), components(), cp(_cp_)
        {
            if (!carry) {
                typename std::vector<T>::iterator i = cp->sets.begin(), j = cp->sets.end();
                for (; i != j; ++i) {
                    components.push_back(i->begin());
                    /*std::cout << "iterator stored " << (*(i->begin())) << std::endl;*/
                }
                /*for (auto s: cp->sets) {*/
                    /*ret.components.push_back({ s.begin(), s.end(), s.begin() });*/
                    /*components.push_back(s.begin());*/
                /*}*/
            }
        }

        std::vector<value_type> operator * () const
        {
            std::vector<value_type> ret;
            for (auto x: components) {
                ret.push_back(*x);
            }
            return ret;
        }

        template <typename Function>
            void apply(Function f)
            {
                size_t i = 0;
                for (auto x: components) {
                    if (!f(i++, *x)) {
                        break;
                    }
                }
            }

        iterator& operator ++ ()
        {
            auto rbeg = components.rbegin();
            auto rend = components.rend();
            auto s = cp->sets.rbegin();
            bool c = true;
            for (; c && rbeg != rend; ++rbeg, ++s) {
                c = (++(*rbeg) == s->end());
                if (c) {
                    /*std::cout << "carry!" << std::endl;*/
                    *rbeg = s->begin();
                } else {
                    break;
                }
            }
            if (rbeg == rend) {
                carry = true;
            }
            return *this;
        }

        bool operator != (const iterator& i)
        {
            /* only used to test against end, so only check carry. */
            return !carry;
        }

        bool operator == (const iterator& i)
        {
            /* only used to test against end, so only check carry. */
            return carry;
        }
    };

    iterator begin()
    {
        return iterator(this, false);
    }

    iterator end()
    {
        return iterator(this, true);
    }
};


inline ArrayXXi haplotype_break_matrix(const std::vector<haplo_type>& haplotypes)
{
    int n = haplotypes.size();
    ArrayXXi ret(n, n);
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            ret(i, j) = haplotypes[i] % haplotypes[j];
        }
    }
    return ret;
}


struct transition_probabilities {
    double prob[3];
    transition_probabilities(double dist)
    {
        double pr = prob_recomb(dist);
        double pn = 1 - pr;
        prob[0] = pn * pn;
        prob[1] = pn * pr;
        prob[2] = pr * pr;
    }
    double operator () (int i)
    {
        return prob[i];
    }
};

inline MatrixXd transition_matrix(double distance, const ArrayXXi& break_matrix) {
    transition_probabilities tp(distance);
    /*return break_matrix.unaryExpr(tp).cast<ArrayXd>().matrix();*/
    size_t sz = break_matrix.innerSize();
    MatrixXd ret(sz, sz);
    double diag_term = tp(0);
    for (size_t i = 0; i < sz; ++i) {
        ret(i, i) = diag_term;
        for (size_t j = i + 1; j < sz; ++j) {
            ret(j, i) = ret(i, j) = tp(break_matrix(i, j));
        }
    }
    return ret;
}



#ifdef TR_LEFT_IS_TRANSPOSED
#  define TR_left(x) (x.transpose())
#else
#  define TR_left(x) (x)
#endif

#ifdef TR_RIGHT_IS_TRANSPOSED
#  define TR_right(x) (x.transpose())
#else
#  define TR_right(x) (x)
#endif


struct geno_prob_computer {
    std::vector<double> locus;
    MatrixXd LV;
    Generation breakmat;

    MatrixXd compute_tr(double dist)
    {
        return eval_poly(breakmat.data, prob_recomb(dist));
    }

    std::vector<MatrixXd> TR;
    VectorXd normalizer;
    MatrixXd init_kernel;
    MatrixXd abundance_ratio;

    MatrixXd normalize_by_col(const MatrixXd& m)
    {
        MatrixXd ret = MatrixXd::Zero(m.innerSize(), m.outerSize());
        VectorXd sums = m.colwise().sum();
        for (int i = 0; i < m.outerSize(); ++i) {
            if (sums(i) != 0) {
                ret.col(i) = m.col(i) / sums(i);
            }
        }
        return ret;
    }

    void init(const labelled_matrix<PolynomMatrix, haplo_type>& break_matrix, double coef=1)
    {
        breakmat = break_matrix;
        TR.clear();
        for (size_t i = 1; i < locus.size(); ++i) {
            TR.emplace_back(compute_tr(locus[i] - locus[i - 1]));
        }

        init_kernel = eval_poly(breakmat.data, .5);
        /*normalizer = eval_poly(breakmat.data, .5).col(0);*/
        normalizer = init_kernel.col(0);
        VectorXd inv_normalizer(normalizer.innerSize());

        for (int i = 0; i < normalizer.innerSize(); ++i) {
            normalizer(i) = (normalizer(i) > 1.e-7 ? normalizer(i) : 0);
        }

        /*init_kernel.transposeInPlace();*/

        /*init_kernel = normalizer * normalizer.transpose() * coef;*/

        for (int i = 0; i < normalizer.innerSize(); ++i) {
            inv_normalizer(i) = (normalizer(i) == 0 ? 0 : 1. / normalizer(i));
            /*normalizer(i) = (normalizer(i) == 0 ? 0 : 1. / normalizer(i));*/
        }

#ifdef USE_ABUNDANCE_RATIO
        abundance_ratio = normalizer * inv_normalizer.transpose();
#else
        abundance_ratio = MatrixXd::Ones(normalizer.innerSize(), normalizer.innerSize());
#endif

        normalizer = inv_normalizer;
        std::cout << "normalizer" << std::endl << normalizer.transpose() << std::endl;
        std::cout << "LV" << std::endl << LV << std::endl;
        std::cout << "polydent" << std::endl << breakmat << std::endl;
        std::cout << "abonde de fond" << std::endl << abundance_ratio << std::endl;
    }

    VectorXd compute_at_new(double loc)
    {
        if (loc < locus.front() || loc > locus.back()) {
            throw out_of_segment_exception();
        }
        size_t l = 0;
        MatrixXd kernel = MatrixXd::Identity(LV.innerSize(), LV.innerSize()) * LV.col(l);
        for (; locus[l] < loc; ++l) {
            kernel *= LV.col(l + 1).asDiagonal() * TR[l];
        }
        /*kernel *= compute_tr(loc - locus[l - 1]) * */
        for (; l < TR.size(); ++l) {
            kernel *= LV.col(l + 1).transpose() * TR[l];
        }
        kernel *= LV.col(l).asDiagonal();
        return kernel.diagonal();
    }

    VectorXd compute_at(double loc)
    {
        return compute_at_old(loc);
    }

    VectorXd compute_at_old(double loc)
    {
        if (loc < locus.front() || loc > locus.back()) {
            throw out_of_segment_exception();
        }
        size_t l = 0, r = LV.outerSize() - 1;

        std::cout << "KERNEL0" << std::endl << init_kernel << std::endl;

        MatrixXd kernel = LV.col(0).asDiagonal() * init_kernel * LV.col(r).asDiagonal();

        std::cout << "KERNEL[l,r]" << std::endl << kernel << std::endl;
        for (; (r - 1) > l && locus[r - 1] > loc; --r) {
            kernel = kernel * LV.col(r - 1).asDiagonal() * TR_right(TR[r - 1]);
            std::cout << "KERNEL[" << l << ',' << (r - 1) << ']' << std::endl << kernel << std::endl;
        }
        for (; (l + 1) < r && locus[l + 1] <= loc; ++l) {
            /*kernel = kernel * TR[l].transpose() * LV.col(l + 1).asDiagonal();*/
            kernel = TR_left(TR[l]) * LV.col(l + 1).asDiagonal() * kernel;
            std::cout << "KERNEL[" << (l + 1) << ',' << r << ']' << std::endl << kernel << std::endl;
        }
        /*VectorXd ret = (compute_tr(locus[r] - loc) * kernel * compute_tr(loc - locus[l]).transpose()).diagonal();*/
        std::cout << "final: KERNEL[" << l << ',' << r << ']' << std::endl << kernel << std::endl;
        std::cout << "TR(" << loc << " - " << locus[l] << ")" << std::endl << (compute_tr(loc - locus[l])) << std::endl;
        std::cout << "TR(" << locus[r] << " - " << loc << ")" << std::endl << (compute_tr(locus[r] - loc)) << std::endl;
        std::cout << "KERNEL * TR(" << loc << " - " << locus[l] << ")" << std::endl << (kernel * compute_tr(loc - locus[l]).transpose()) << std::endl;
        std::cout << "TR(" << locus[r] << " - " << loc << ") * KERNEL * TR(" << loc << " - " << locus[l] << ")" << std::endl << (compute_tr(locus[r] - loc) * kernel * compute_tr(loc - locus[l]).transpose()) << std::endl;
        std::cout << "---------------------------------------------------------" << std::endl;
        /*VectorXd ret = (compute_tr(locus[r] - loc) * kernel * compute_tr(loc - locus[l])).diagonal();*/
        VectorXd ret = (
                TR_left(compute_tr(loc - locus[l]))
                * kernel
                * TR_right(compute_tr(locus[r] - loc))
            ).diagonal();
            /*).colwise().sum();*/
            /*).rowwise().sum();*/
        /*MatrixXd tmp = compute_tr(locus[r] - loc) * kernel * compute_tr(loc - locus[l]).transpose();*/
        /*std::cout << "tmp" << std::endl << tmp << std::endl;*/
        /*VectorXd ret = (tmp).colwise().sum();*/
        /*VectorXd ret = tmp.diagonal().array().sqrt().matrix();*/
        /*VectorXd ret = (compute_tr(locus[r] - loc).transpose() * kernel * compute_tr(loc - locus[l])).diagonal();*/
        /*VectorXd ret = (compute_tr(locus[r] - loc).transpose() * kernel * compute_tr(loc - locus[l]).transpose()).diagonal();*/
#ifdef NORMALIZE_WITH_ABUNDANCE
        std::cout << "ret" << std::endl << ret.transpose() << std::endl;
        for (int i = 0; i < ret.innerSize(); ++i) {
            ret(i) *= normalizer(i);
        }
        std::cout << "ret * normalizer" << std::endl << ret.transpose() << std::endl;
#endif
#ifdef NORMALIZE_TO_ONE
        ret /= ret.sum();
#endif
        std::cout << "final countdown" << std::endl << ret.transpose() << std::endl;
        std::cout << "=========================================================" << std::endl;
        return ret;
    }

    MatrixXd compute_over_segment(const std::vector<double>& steps)
    {
        MatrixXd ret(breakmat.innerSize(), steps.size());
        for (size_t i = 0; i < steps.size(); ++i) {
            ret.col(i) = compute_at(steps[i]);
        }
        return ret;
    }
};

#endif

