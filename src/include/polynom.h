/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_POLYNOM_H_
#define _SPEL_POLYNOM_H_

#include <vector>
#include <iostream>
#include <sstream>
#include <cmath>
/*#include <Eigen/Core>*/
#include "eigen.h"
#include <limits>

#include "labelled_matrix.h"

inline std::ostream& output_exponent(std::ostream& os, int expon)
{
    static const char* exponent_digits[] = {
        "\u2070",
        "\u2071",
        "\u00B2",
        "\u00B3",
        "\u2074",
        "\u2075",
        "\u2076",
        "\u2077",
        "\u2078",
        "\u2079"
    };

    if (expon) {
        output_exponent(os, expon / 10) << exponent_digits[expon % 10];
    }
    return os;
}


struct polynom : public std::vector<double> {
    static constexpr double epsilon = 1.e-12;

    char symbol;

    struct polynom_init {
        polynom* self;

        polynom& operator = (std::initializer_list<double> coefs)
        {
            self->assign(coefs.begin(), coefs.end());
            return *self;
        }
    };

    polynom() : std::vector<double>(), symbol('X') {}

    polynom(char sym) : std::vector<double>(), symbol(sym) {}

    polynom(int d) : std::vector<double>(), symbol('X') { push_back(d); }
    polynom(double d) : std::vector<double>(), symbol('X') { push_back(d); }

    polynom(std::initializer_list<double> coefs)
        : std::vector<double>(coefs), symbol('X')
    {}

    polynom(const polynom& p) : std::vector<double>(p), symbol(p.symbol) {}

    polynom_init operator << (char sym)
    {
        symbol = sym;
        return {this};
    }

    /*polynom& operator = (std::initializer_list<double> coefs)*/
    /*{*/
        /*assign(coefs.begin(), coefs.end());*/
        /*return *this;*/
    /*}*/

    double operator () (double v) const
    {
        double accum = 0;
        double x = 1;
        for (double coef: *this) {
            accum += coef * x;
            x *= v;
        }
        return accum;
    }

    polynom& normalize() {
#if 0
        for (auto& coef : *this) {
            if (coef > -epsilon && coef < epsilon) {
                coef = 0;
            }
        }
#endif
        size_t s = size();
        while(s-- && back() == 0) {
            pop_back();
        }
        return *this; 
    }

    bool operator < (const polynom& p)
    {
        if (p.size() == 1 && isinf(p[0])) {
            return true;
        }
        if (p.size() == 2 && isinf(p[0])) {
            return false;
        }
        if (p.size() < size()) {
            return false;
        }
        if (p.size() > size()) {
            return true;
        }
        for (size_t i = 0; i < size(); ++i) {
            if ((*this)[i] != p[i]) {
                return (*this)[i] < p[i];
            }
        }
        return false;
    }

    bool operator > (const polynom& p)
    {
        if (p.size() == 1 && isinf(p[0])) {
            return false;
        }
        if (p.size() == 2 && isinf(p[0])) {
            return true;
        }
        if (p.size() < size()) {
            return true;
        }
        if (p.size() > size()) {
            return false;
        }
        for (size_t i = 0; i < size(); ++i) {
            if ((*this)[i] != p[i]) {
                return (*this)[i] > p[i];
            }
        }
        return false;
    }

    bool operator == (const polynom& p) {
        if (p.size() == size()) {
            for (size_t i = 0; i < size(); ++i) {
                if (p[i] != (*this)[i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    bool operator != (const polynom& p) {
        if (p.size() == size()) {
            for (size_t i = 0; i < size(); ++i) {
                if (p[i] != (*this)[i]) {
                    return true;
                }
            }
        }
        return false;
    }

    inline polynom& operator *= (const polynom& p2)
    {
        size_t s1 = size();
        size_t s2 = p2.size();
        if (!(s1 && s2)) {
            return *this;
        }
        std::vector<double> p;
        p.resize(s1 + s2 - 1);
        for (size_t i1 = 0; i1 < s1; ++i1) {
            if (!(*this)[i1]) continue;
            for (size_t i2 = 0; i2 < s2; ++i2) {
                if (!p2[i2]) continue;
                p[i1 + i2] += (*this)[i1] * p2[i2];
            }
        }
        std::swap(*this, p);
        return normalize();
    }

    inline polynom& operator += (const polynom& p2)
    {
        size_t i1;
        size_t s1 = size();
        size_t s2 = p2.size();
        for (i1 = 0; i1 < s1 && i1 < s2; ++i1) {
            (*this)[i1] += p2[i1];
        }
        for (; i1 < s2; ++i1) {
            push_back(p2[i1]);
        }
        return normalize();
    }

    inline polynom& operator -= (const polynom& p2)
    {
        size_t i1;
        for (i1 = 0; i1 < size() && i1 < p2.size(); ++i1) {
            (*this)[i1] -= p2[i1];
        }
        for (; i1 < p2.size(); ++i1) {
            push_back(-p2[i1]);
        }
        return normalize();
    }

    inline polynom& operator /= (double f)
    {
        size_t i1;
        for (i1 = 0; i1 < size(); ++i1) {
            (*this)[i1] /= f;
        }
        return *this;
    }

    inline polynom& prune(double threshold)
    {
        size_t vp = valuation();
        double T0 = fabs((*this)[vp]) * threshold;
        double Tfac = 1;
        double T;
        size_t k;
        for (k = vp + 1; k < size() && k < vp + 52; ++k) { /* FIXME: remplacer ce 52 par 2*compute_exp_epsilon */
            T = T0 * Tfac;
            if (fabs((*this)[k]) <= T) {
                (*this)[k] = 0;
            }
            Tfac *= 2;
        }
        for (; k < size(); ++k) {
            (*this)[k] = 0;
        }
        return normalize();
    }

    /* P(X) o Q(x) */
    inline polynom o(const polynom& p) const
    {
        polynom ret(p.symbol);
        polynom tmp = {1};
        for (auto k: *this) {
            ret += tmp * k;
            tmp *= p;
        }
        return ret;
    }

    inline polynom derive() const
    {
        polynom ret;
        if (size() <= 1) {
            return ret;
        }
        ret.resize(size() - 1);
        for (size_t i = 1; i < size(); ++i) {
            ret[i - 1] = (*this)[i] * i;
        }
        return ret;
    }

    inline polynom operator * (double k) const
    {
        polynom ret(*this);
        for (auto& p: ret) { p *= k; }
        return ret.normalize();
    }

    inline polynom operator / (double k) const
    {
        polynom ret(*this);
        for (auto& p: ret) { p /= k; }
        return ret.normalize();
    }

    inline polynom operator ^ (int n) const
    {
        polynom ret = {1};
        for (; n > 0; --n) {
            ret *= *this;
        }
        return ret;
    }

    inline int valuation() const
    {
        if (!size()) {
            return std::numeric_limits<int>::min();
        }
        return std::find_if(begin(), end(),
                            [](double x) {
                                return x != 0;
                            }) - begin();
    }

    inline double norm() const
    {
        double x = 1;
        double accum = 0;
        for (auto coef: *this) {
            accum += fabs(coef) * x;
            x *= .5;
        }
        return accum;
    }

    inline polynom polynom::o(const fast_polynom& p) const
    {
        return to_d().o(p);
    }
};


struct output_poly {
    std::stringstream s;
    void add(char sym, double coef, int exponent)
    {
        if (coef == 0) {
            if (s.tellp() == 0 && exponent == 0) {
                s << '0';
            }
            return;
        }
        if (s.tellp() != 0) {
            if (coef < 0) {
                s << " - ";
            } else {
                s << " + ";
            }
        }
        if (exponent > 0) {
            if (coef != 1 && coef != -1) {
                if (coef < 0 && s.tellp() > 0) {
                    s << (-coef);
                } else {
                    s << coef;
                }
                if (exponent > 0) {
                    s << "*";
                }
            } else if(s.tellp() == 0 && coef == -1) {
                s << '-';
            }
            s << sym;
            if (exponent > 1) {
                s << '^' << exponent;
                /*output_exponent(s, exponent);*/
            }
        } else if (coef < 0 && s.tellp() > 0) {
            s << (-coef);
        } else {
            s << coef;
        }
    }
};

inline std::ostream& operator << (std::ostream& os, const output_poly& op)
{
    return os << '[' << op.s.str() << ']';
}


inline std::ostream& operator << (std::ostream& os, const polynom& p)
{
    auto coef = p.rbegin();
    auto end = p.rend();
    size_t n = p.size();
    output_poly op;
    for (; coef != end; ++coef) {
        op.add(p.symbol, *coef, --n);
    }
    return os << op;
}

inline polynom operator * (const polynom& p1, const polynom& p2)
{
    polynom ret;
    if (!(p1.size() || p2.size())) {
        return ret;
    }
    ret.resize(p1.size() + p2.size() - 1);
    for (size_t i1 = 0; i1 < p1.size(); ++i1) {
        if (!p1[i1]) continue;
        for (size_t i2 = 0; i2 < p2.size(); ++i2) {
            if (!p2[i2]) continue;
            ret[i1 + i2] += p1[i1] * p2[i2];
        }
    }
    return ret.normalize();
}

inline polynom operator + (const polynom& p1, const polynom& p2)
{
    polynom ret = p1;
    size_t i1;
    for (i1 = 0; i1 < p1.size() && i1 < p2.size(); ++i1) {
        ret[i1] += p2[i1];
    }
    for (; i1 < p2.size(); ++i1) {
        ret.push_back(p2[i1]);
    }
    return ret.normalize();
}

inline polynom operator - (const polynom& p1, const polynom& p2)
{
    polynom ret = p1;
    size_t i1;
    for (i1 = 0; i1 < p1.size() && i1 < p2.size(); ++i1) {
        ret[i1] -= p2[i1];
    }
    for (; i1 < p2.size(); ++i1) {
        ret.push_back(-p2[i1]);
    }
    while (ret.size() > 0 && ret.back() == 0) {
        ret.pop_back();
    }
    return ret.normalize();
}



inline bool operator < (const polynom& p1, const polynom& p2)
{
    if (p1.size() < p2.size()) {
        return true;
    }
    if (p1.size() == p2.size()) {
        for (size_t i = 0; i < p1.size(); ++i) {
            if (p1[i] == p2[i]) {
                continue;
            }
            return p1[i] < p2[i];
        }
    }
    return false;
}



namespace Eigen {
    template<>
    struct NumTraits<polynom> {
        typedef polynom Real;
        typedef polynom NonInteger;
        typedef polynom Nested;
        enum {
            IsComplex = 0,
            IsInteger = 1,
            ReadCost = 100,
            WriteCost = 100,
            MulCost = 1000,
            AddCost = 1000,
            IsSigned = 1,
            RequireInitialization = 1
        };
        static Real epsilon() { return { std::numeric_limits<double>::epsilon() }; }
        static Real dummy_precision() { return { std::numeric_limits<double>::epsilon() }; }
        static polynom highest() { return { INFINITY }; }
        static polynom lowest() { return { INFINITY, 0 }; }

    };

}

typedef Eigen::Matrix<polynom, Eigen::Dynamic, Eigen::Dynamic> PolynomMatrix;


inline Eigen::MatrixXd eval_poly(const PolynomMatrix& p, double x)
{
    Eigen::MatrixXd ret(p.innerSize(), p.outerSize());
    for (int i = 0; i < p.innerSize(); ++i) {
        for (int j = 0; j < p.outerSize(); ++j) {
            ret(i, j) = p(i, j)(x);
        }
    }
    return ret;
}

template <typename LABEL>
labelled_matrix<Eigen::MatrixXd, LABEL> eval_poly(const labelled_matrix<PolynomMatrix, LABEL>& p, double x)
{
    labelled_matrix<Eigen::MatrixXd, LABEL> ret(p.row_labels.begin(), p.row_labels.end(), p.column_labels.begin(), p.column_labels.end());
    for (int i = 0; i < p.innerSize(); ++i) {
        for (int j = 0; j < p.outerSize(); ++j) {
            ret.data(i, j) = p.data(i, j)(x);
        }
    }
    return ret;
}





#endif

