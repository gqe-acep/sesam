/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_MARKER_OBS_BAYES_H_
#define _SPEL_MARKER_OBS_BAYES_H_

#include "error.h"
/*#include "generation_rs_fwd.h"*/
/*#include "bayes/graph.h"*/

#if 0
static inline
size_t idx_to_ofs(const std::vector<size_t>& dim, const std::vector<size_t>& idx)
{
    size_t accum = 0;
    size_t stride = 1;
    if (dim.size() != idx.size()) { throw 0; }
    for (int i = dim.size() - 1; i >= 0; --i) {
        if (idx[i] >= dim[i]) { throw 1; }
        accum += stride * idx[i];
        stride *= dim[i];
    }
#if 0
    std::cout << "idx_to_ofs";
    for (int i = 0; i < dim.size(); ++i) {
        std::cout << ' ' << idx[i] << "[" << dim[i] << ']';
    }
    std::cout << " => " << accum << std::endl;
#endif
    return accum;
}


static inline
size_t size(const std::vector<size_t>& dim) {
    size_t stride = 1;
    for (auto d: dim) {
        stride *= d;
    }
    return stride;
}
#endif


#include "pedigree.h"
/*#include "bayes/bn.h"*/
/*#include "bayes/factor_var4.h"*/
#include "graphnode2.h"
#include "bayes/cli.h"
#include "bayes/dispatch.h"

#endif

