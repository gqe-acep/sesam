/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_PROBA_CONFIG_H_
#define _SPEL_PROBA_CONFIG_H_

/*#define LOCUS_VECTOR_IS_ABUNDANCE*/
/*#define LOCUS_VECTOR_IS_NORMALIZED*/
/*#define NORMALIZE_WITH_ABUNDANCE*/
#define NORMALIZE_TO_ONE
/*#define USE_ABUNDANCE_RATIO*/
/*#define TR_LEFT_IS_TRANSPOSED*/
/*#define TR_RIGHT_IS_TRANSPOSED*/

/*#define LOCUS_LABEL_PARENT_GENOTYPES*/
/*#define LOCUS_VECTOR_TO_BLOCK*/

/*#define KERNEL_HAS_ONES*/

#define NORMALIZE_GENERATION
/*#define BIDOUILLE_RIL*/
/*#define FULL_TRANSITION_MATRIX*/

#endif

