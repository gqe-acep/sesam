/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_LABELLED_MATRIX_H_
#define _SPEL_LABELLED_MATRIX_H_

/*#include <Eigen/Core>*/
#include "eigen.h"
#include <vector>
#include <map>
#include <iomanip>
#include "stl_output.h"

#define MATRIX_SIZE(_x_)  "dim(" #_x_ ") = (" << (_x_).innerSize() << ',' << (_x_).outerSize() << ")"


#ifndef ROTATE_DEFINED
#define ROTATE_DEFINED
namespace impl {
    template <size_t N_BITS>
        static inline size_t ROTATE(size_t value)
        {
            return (value >> N_BITS) | (value << (sizeof(size_t) * 8 - N_BITS));
        }
}
#endif


inline std::ostream& operator << (std::ostream& os, const std::pair<double, double>& p)
{
    return os << '(' << p.first << ',' << p.second << ')';
}


//template <typename VALUE_TYPE>
//std::ostream& operator << (std::ostream& os, const std::vector<VALUE_TYPE>& v)
//{
//    std::string sep = "";
//    for (auto x: v) { os << sep << x; sep = " "; }
//    return os;
//}

template <typename MATRIX, typename ROW_LABEL, typename COL_LABEL=ROW_LABEL>
struct labelled_matrix {
    typedef MATRIX matrix_type;
    typedef ROW_LABEL row_label_type;
    typedef COL_LABEL col_label_type;
    typedef typename MATRIX::Scalar Scalar;

    MATRIX data;
    std::vector<ROW_LABEL> row_labels;
    std::vector<COL_LABEL> column_labels;

    labelled_matrix()
        : data(), row_labels(), column_labels()
    {}

    labelled_matrix(size_t dim)
        : data(dim, dim), row_labels(), column_labels()
    {
        row_labels.resize(dim);
        column_labels.resize(dim);
    }

    labelled_matrix(size_t rows, size_t cols)
        : data(rows, cols), row_labels(), column_labels()
    {
        row_labels.resize(rows);
        column_labels.resize(cols);
    }

    labelled_matrix(const std::vector<COL_LABEL>& lab)
        : data(lab.size(), lab.size()), row_labels(lab), column_labels(lab)
    {}

    labelled_matrix(const std::vector<ROW_LABEL>& rlab,
                    const std::vector<COL_LABEL>& clab)
        : data(rlab.size(), clab.size()), row_labels(rlab), column_labels(clab)
    {}

    template <typename Iterator1, typename Iterator2>
    labelled_matrix(Iterator1 i1, Iterator1 j1, Iterator2 i2, Iterator2 j2)
        : data(), row_labels(i1, j1), column_labels(i2, j2)
    {
        data = MATRIX::Zero(row_labels.size(), column_labels.size());
    }

    template <typename Iterator1, typename Iterator2>
    labelled_matrix(Iterator1 i1, Iterator1 j1, Iterator2 i2, Iterator2 j2, const MATRIX& m)
        : data(m), row_labels(i1, j1), column_labels(i2, j2)
    {}

    labelled_matrix(std::initializer_list<ROW_LABEL> rlab, std::initializer_list<COL_LABEL> clab)
        : data(rlab.size(), clab.size()), row_labels(), column_labels()
    {
        row_labels = rlab;
        column_labels = clab;
    }

    template <typename Iterator1>
    labelled_matrix(Iterator1 i1, Iterator1 j1)
        : data(), row_labels(i1, j1), column_labels(i1, j1)
    {
        data = MATRIX::Zero(row_labels.size(), column_labels.size());
    }

    labelled_matrix(std::initializer_list<COL_LABEL> lab)
        : data(lab.size(), lab.size()), row_labels(), column_labels()
    {
        row_labels = lab;
        column_labels = lab;
    }

    labelled_matrix<MATRIX, COL_LABEL, ROW_LABEL> transpose() const
    {
        labelled_matrix<MATRIX, COL_LABEL, ROW_LABEL> ret(column_labels, row_labels);
        ret.data = data.transpose();
        return ret;
    }

    typename MATRIX::Scalar operator () (const ROW_LABEL& r, const COL_LABEL& c) const
    {
        return data(std::find(row_labels.begin(), row_labels.end(), r) - row_labels.begin(),
                    std::find(column_labels.begin(), column_labels.end(), c) - column_labels.begin());
    }

    typename MATRIX::Scalar& operator () (const ROW_LABEL& r, const COL_LABEL& c)
    {
        return data(std::find(row_labels.begin(), row_labels.end(), r) - row_labels.begin(),
                    std::find(column_labels.begin(), column_labels.end(), c) - column_labels.begin());
    }

    auto row(const ROW_LABEL& r)
        -> decltype(data.row(0))
        {
            return data.row(std::find(row_labels.begin(), row_labels.end(), r) - row_labels.begin());
        }

    auto col(const ROW_LABEL& r)
        -> decltype(data.col(0))
        {
            return data.col(std::find(column_labels.begin(), column_labels.end(), r) - column_labels.begin());
        }

    labelled_matrix& operator = (const labelled_matrix& lm)
    {
        row_labels = lm.row_labels;
        column_labels = lm.column_labels;
        data = lm.data;
        return *this;
    }

    template <typename EIGEN_EXPR>
    labelled_matrix& operator = (EIGEN_EXPR x) { data = x; return *this; }

    template <typename EIGEN_EXPR>
    labelled_matrix& operator *= (EIGEN_EXPR x) { data *= x; return *this; }

    template <typename EIGEN_EXPR>
    labelled_matrix& operator += (EIGEN_EXPR x) { data += x; return *this; }

    template <typename EIGEN_EXPR>
    labelled_matrix& operator -= (EIGEN_EXPR x) { data -= x; return *this; }

    template <typename EIGEN_EXPR>
    labelled_matrix& operator /= (EIGEN_EXPR x) { data /= x; return *this; }

    operator MATRIX () { return data; }
    operator const MATRIX& () const { return data; }

    size_t innerSize() const { return data.innerSize(); }
    size_t outerSize() const { return data.outerSize(); }
    int cols() const { return data.cols(); }
    int rows() const { return data.rows(); }

    struct formatted {
        const labelled_matrix<MATRIX, row_label_type, col_label_type>* m;
        Eigen::IOFormat fmt;
    };

    formatted
    format(const Eigen::IOFormat& fmt) const
    {
        return {this, fmt};
    }
};


template <typename MATRIX, typename ROW_LABEL, typename COL_LABEL>
Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic>
__make_str_matrix(const labelled_matrix<MATRIX, ROW_LABEL, COL_LABEL>& lm)
{
    Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic> tmp(lm.innerSize() + 1, lm.outerSize() + 1);
    size_t i = 0;
    /*std::cout << "Printing matrix" << std::endl;*/
    /*std::cout << "cols = " << lm.column_labels << std::endl;*/
    /*std::cout << "rows = " << lm.row_labels << std::endl;*/
    for (auto l: lm.column_labels) {
        std::stringstream x;
        x << l;
        tmp(0, ++i) = x.str();
        if (i == lm.outerSize()) {
            break;
        }
    }
    i = 0;
    for (auto l: lm.row_labels) {
        std::stringstream x;
        x << l;
        tmp(++i, 0) = x.str();
        if (i == lm.innerSize()) {
            break;
        }
    }
    for (i = 0; i < lm.innerSize(); ++i) {
        for (size_t j = 0; j < lm.outerSize(); ++j) {
            std::stringstream x;
            x << lm.data(i, j);
            tmp(i + 1, j + 1) = x.str();
        }
    }
    return tmp;
}



inline
std::ostream& operator << (std::ostream& os, const Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic>& mat)
{
    Eigen::VectorXi widths = Eigen::VectorXi::Zero(mat.outerSize());
    int inner = mat.innerSize();
    int outer = mat.outerSize();
    for (int j = 0; j < outer; ++j) {
        for (int i = 0; i < inner; ++i) {
            int l = mat(i, j).size();
            widths(j) = widths(j) < l ? l : widths(j);
        }
    }
    --inner;
    for (int i = 0; i < inner; ++i) {
        os << std::setw(widths(0)) << mat(i, 0);
        for (int j = 1; j < outer; ++j) {
            os << ' ' << std::setw(widths(j)) << mat(i, j);
        }
        os << std::endl;
    }
    os << std::setw(widths(0)) << mat(inner, 0);
    for (int j = 1; j < outer; ++j) {
        os << ' ' << std::setw(widths(j)) << mat(inner, j);
    }
    return os;
}



template <typename MATRIX, typename ROW_LABEL, typename COL_LABEL>
std::ostream& operator << (std::ostream& os, const labelled_matrix<MATRIX, ROW_LABEL, COL_LABEL>& lm)
{
    return os << __make_str_matrix(lm);
}


template <typename MATRIX, typename ROW_LABEL, typename COL_LABEL>
std::ostream& operator << (std::ostream& os, const typename labelled_matrix<MATRIX, ROW_LABEL, COL_LABEL>::formatted& lm)
{
    return os << __make_str_matrix(*lm.m).format(lm.fmt);
}


template <typename MATRIX, typename RL, typename CL1, typename CL2>
labelled_matrix<MATRIX, RL, CL2>
operator * (const labelled_matrix<MATRIX, RL, CL1>& m1, const labelled_matrix<MATRIX, CL1, CL2>& m2)
{
    labelled_matrix<MATRIX, RL, CL2> ret;
#ifndef EIGEN_NO_DEBUG
#if 0
    if (m1.column_labels != m2.row_labels) {
        MSG_ERROR("Matrix labels mismatch! Can't multiply.", "");
        MSG_ERROR("m1::cols " << m1.column_labels, "");
        MSG_ERROR("m2::rows " << m2.row_labels, "");
        /*std::cerr << "Matrix labels mismatch! Can't multiply." << std::endl;*/
        /*std::cerr << "m1::cols " << m1.column_labels << std::endl;*/
        /*std::cerr << "m2::rows " << m2.row_labels << std::endl;*/
        /*std::cerr << "m1:" << std::endl << m1 << std::endl;*/
        /*std::cerr << "m2:" << std::endl << m2 << std::endl;*/
        throw -1;
    }
#endif
#endif
    ret.row_labels = m1.row_labels;
    ret.column_labels = m2.column_labels;
    ret.data = m1.data * m2.data;
    return ret;
}

template <typename M, typename R, typename C>
bool operator == (const labelled_matrix<M, R, C>& lm1, const labelled_matrix<M, R, C>& lm2)
{
    return lm1.row_labels == lm2.row_labels && lm1.column_labels == lm2.column_labels && lm1.data == lm2.data;
}


namespace std {
template <typename T>
struct hash<std::vector<T>> {
	size_t operator () (const std::vector<T>& v)
	{
		hash<T> h;
		size_t accum = 0;
		for (auto& x: v) {
			accum ^= h(x);
		}
		return accum;
	}
};

template <typename _Scalar, int A, int B, int C, int D, int E>
struct hash<Eigen::Matrix<_Scalar, A, B, C, D, E>> {
    struct red_mat {
        size_t accum;
        void init(_Scalar s, int i, int j)
        {
            accum = hash<_Scalar>()(s);
            (void)i; (void)j;
        }
        void operator () (_Scalar s, int i, int j)
        {
            accum = impl::ROTATE<7>(accum) ^ hash<_Scalar>()(s);
            (void)i; (void)j;
        }
    };
#if 0
    size_t operator () (const value<Eigen::Matrix<_Scalar, A, B, C, D, E>>& m) const
    {
        red_mat rm;
        m->visit(rm);
        return rm.accum;
    }
#endif
    size_t operator () (const Eigen::Matrix<_Scalar, A, B, C, D, E>& m) const
    {
        red_mat rm;
        m.visit(rm);
        return rm.accum;
    }
};

template <typename M, typename R, typename C>
struct hash<labelled_matrix<M, R, C>> {
    size_t operator () (const labelled_matrix<M, R, C>& m) const
	{
		hash<M> hm;
		hash<R> hr;
		hash<C> hc;
		size_t accum = hm(m.data);
		for (auto& r: m.row_labels) { accum ^= hr(r); }
		for (auto& c: m.column_labels) { accum ^= hc(c); }
		return accum;
	}
#if 0
	size_t operator () (const value<labelled_matrix<M, R, C>>& m) const
	{
		return operator () (*m);
	}
#endif
};
}
#endif

