/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* fmin : golden section search of minimum for a given function on a given interval with given precision.
 *
 * rewritten by D.Leroux from the output of f2c for source file http://www.netlib.org/fmm/fmin.f
 *
 * Source for libf2c is in /netlib/f2c/libf2c.zip, e.g., http://www.netlib.org/f2c/libf2c.zip
 */

#ifndef _SPEL_FMIN_H_
#define _SPEL_FMIN_H_

#include <utility>
#include <cmath>
/*#include <cstdio>*/

static inline double compute_epsilon() {
    static double epsilon = 0, tol1 = 0;
    if (tol1 == 0) {
        epsilon = 1.;
        do {
            epsilon *= .5;
            tol1 = 1. + epsilon;
            /*printf("tol1 = %e\n", tol1);*/
        } while (tol1 > 1.);
        epsilon = sqrt(epsilon);
        /*printf("epsilon = %e\n", epsilon);*/
    }
    return epsilon;
}

static inline double _abs_(double x) { return fabs(x); }

static inline double d_sign(double a, double b)
{
    double abs_a = (a >= 0 ? a : -a);
    return( b >= 0 ? abs_a : -abs_a);
}


#define _eq_(_a_, _b_, _eps_) (_abs_(_a_ - _b_) <= _eps_) 


/*doublereal fmin(doublereal *ax, doublereal *bx, D_fp f, doublereal *tol)*/
template <typename Functor>
std::pair<double, double> optimize(double ax, double bx, Functor f, double tol)
{
    /* Local variables */

    /*  c__ is the squared inverse of the golden ratio */
    /*static double a, b, c__ = (3. - sqrt(5.)) * .5, d__, e, p, q, r__, u, v, w, x, fu, fv, fw, */
#define c__ (.3819660112501051517954131656343618822796908201942371378645513772947)

    double a, b, d__, e, p, q, r__, u, v, w, x, fu, fv, fw, fx, xm, eps, tol1, tol2;


    /*      an approximation  x  to the point where  f  attains a minimum  on */
    /*  the interval  (ax,bx)  is determined. */


    /*  input.. */

    /*  ax    left endpoint of initial interval */
    /*  bx    right endpoint of initial interval */
    /*  f     function subprogram which evaluates  f(x)  for any  x */
    /*        in the interval  (ax,bx) */
    /*  tol   desired length of the interval of uncertainty of the final */
    /*        result ( .ge. 0.0d0) */


    /*  output.. */

    /*  fmin  abcissa approximating the point where  f  attains a minimum */


    /*      the method used is a combination of  golden  section  search  and */
    /*  successive parabolic interpolation.  convergence is never much slower */
    /*  than  that  for  a  fibonacci search.  if  f  has a continuous second */
    /*  derivative which is positive at the minimum (which is not  at  ax  or */
    /*  bx),  then  convergence  is  superlinear, and usually of the order of */
    /*  about  1.324.... */
    /*      the function  f  is never evaluated at two points closer together */
    /*  than  eps*abs(fmin) + (tol/3), where eps is  approximately the square */
    /*  root  of  the  relative  machine  precision.   if   f   is a unimodal */
    /*  function and the computed values of   f   are  always  unimodal  when */
    /*  separated by at least  eps*abs(x) + (tol/3), then  fmin  approximates */
    /*  the abcissa of the global minimum of  f  on the interval  ax,bx  with */
    /*  an error less than  3*eps*abs(fmin) + tol.  if   f   is not unimodal, */
    /*  then fmin may approximate a local, but perhaps non-global, minimum to */
    /*  the same accuracy. */
    /*      this function subprogram is a slightly modified  version  of  the */
    /*  algol  60 procedure  localmin  given in richard brent, algorithms for */
    /*  minimization without derivatives, prentice - hall, inc. (1973). */


    /*  eps is approximately the square root of the relative machine */
    /*  precision. */

    eps = compute_epsilon();

    /*  initialization */

    a = ax;
    b = bx;
    v = a + c__ * (b - a);
    w = v;
    x = v;
    e = 0.;
    fx = f(x);
    fv = fx;
    fw = fx;

    /*  main loop starts here */

    while (1) {
        xm = (a + b) * .5;
        /*printf("a=%e\tb=%e\tx=%e\tfx=%e\tu=%e\tfu=%e\txm=%e\n", a, b, x, fx, u, fu, xm);*/
        /*tol1 = eps * _abs_(x) + *tol / 3.;*/
        tol1 = eps * _abs_(x) + tol / 3.;
        tol2 = tol1 * 2.;

        /*  check stopping criterion */

        if (_abs_(x - xm) <= tol2 - (b - a) * .5) {
            break;
        }

        /* is golden-section necessary */

        if (_abs_(e) <= tol1) {
            /*  a golden-section step */
            e = (x < xm ? b : a) - x;
            d__ = c__ * e;
        } else {

            /*  fit parabola */

            r__ = (x - w) * (fx - fv);
            q = (x - v) * (fx - fw);
            p = (x - v) * q - (x - w) * r__;
            q = (q - r__) * 2.;
            if (q > 0.) {
                p = -p;
            }
            q = _abs_(q);
            r__ = e;
            e = d__;

            /*  is parabola acceptable */

            if ((_abs_(p) >= _abs_(q * .5 * r__))
                    || (p <= q * (a - x))
                    || (p >= q * (b - x))) {
                /*  a golden-section step */
                e = (x < xm ? b : a) - x;
                d__ = c__ * e;
            } else {

                /*  a parabolic interpolation step */

                d__ = p / q;
                u = x + d__;

                /*  f must not be evaluated too close to ax or bx */

                if (u - a < tol2) {
                    d__ = d_sign(tol1, xm - x);
                }
                if (b - u < tol2) {
                    d__ = d_sign(tol1, xm - x);
                }
            }
        }

        /*  f must not be evaluated too close to x */

        if (_abs_(d__) >= tol1) {
            u = x + d__;
        } else {
            u = x + d_sign(tol1, d__);
        }
        fu = f(u);

        /*  update  a, b, v, w, and x */

        if (fu <= fx) {
            /*printf("fu <= fx (if)\n");*/
            if (u >= x) {
                /*printf("u >= x (if)\n");*/
                a = x;
            } else {
                /*printf("u < x (else)\n");*/
                b = x;
            }
            v = w;
            fv = fw;
            w = x;
            fw = fx;
            x = u;
            fx = fu;
        } else {
            /*printf("fu > fx (else)\n");*/
            if (u < x) {
                /*printf("u < x (if)\n");*/
                a = u;
            } else {
                /*printf("u >= x (else)\n");*/
                b = u;
            }
            if ((fu <= fw) || (w == x)) {
                v = w;
                fv = fw;
                w = u;
                fw = fu;
            } else if ((fu <= fv) || (v == x) || (v == w)) {
                v = u;
                fv = fu;
            }
        }
    }

    /*  end of main loop */

    return std::pair<double, double>(x, fx);
} /* fmin_ */

#endif

