/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_CHROMOSOME_H_
#define _SPEL_CHROMOSOME_H_

#include <vector>
#include "braille_plot.h"


struct marker_name_and_position {
    std::vector<std::string> marker_name;
    std::vector<double> marker_locus;

    double locus_by_name(const std::string& n) const
    {
        auto name_i = marker_name.begin();
        auto name_j = marker_name.end();
        auto locus_i = marker_locus.begin();
        /*auto locus_j = marker_locus.end();*/
        while (name_i != name_j && *name_i != n) { ++name_i; ++locus_i; }
        if (name_i == name_j) {
            MSG_ERROR("Marker " << n << " doesn't exist in this chromosome", "");
            throw std::exception();
        }
        return *locus_i;
    }
};


/* FIXME: rename compute_xya to make its role more clear. something related to displaying the genetic map */
struct compute_xya {
    template <typename StartFun, typename EndFun, typename Payload>
        void
        operator () (int padding_left, double x_factor,
                     const std::map<double, Payload>& poi,
                     StartFun&& get_start, EndFun&& get_end,
                     std::vector<int>& X, std::vector<int>& Y,
                     std::vector<int>& anchor, size_t& ymax)
        {
            std::vector<int> xend;
            for (const auto& pl: poi) {
                anchor.push_back(round(padding_left + pl.first * x_factor));
                /*int x = anchor.back() - 2 * (1 + pl.second.size());*/
                int x = get_start(anchor.back(), pl.second);
                int y;
                auto yi = std::find_if(xend.begin(), xend.end(), [&] (int xend) { return xend < x; });
                if (yi == xend.end()) {
                    /*xend.push_back(anchor.back() + 2);*/
                    xend.push_back(get_end(anchor.back(), pl.second));
                    y = xend.size() + 1;
                    yi = xend.end();
                } else {
                    /**yi = anchor.back() + 2;*/
                    *yi = get_end(anchor.back(), pl.second);
                    y = 2 + (yi - xend.begin());
                }
                for (auto xe = xend.begin(); xe < yi; ++xe) { *xe = get_end(anchor.back(), pl.second); }
                X.push_back(x);
                Y.push_back(y);
            }
            /*MSG_DEBUG("X: " << X);*/
            /*MSG_DEBUG("Y: " << Y);*/
            ymax = xend.size();
            /*MSG_DEBUG("ymax: " << ymax);*/
        }
};


struct chromosome {
    std::string name;
    marker_name_and_position raw; 
    std::vector<int> haplo_sizes;
    marker_name_and_position condensed;

    chromosome() : name(), raw(), haplo_sizes(), condensed() {}

    chromosome(const chromosome& c)
        : name(c.name), raw(c.raw)
        , haplo_sizes(c.haplo_sizes)
        , condensed(c.condensed)
    {}

    chromosome(const std::string& n,
               const std::vector<std::string>& mn,
               const std::vector<double>& ml)
        : name(n), raw{mn, ml}, haplo_sizes(), condensed()
    {
        compute_haplo_sizes();
    }

    void compute_haplo_sizes()
    {
//        MSG_DEBUG("Computing haplotype sizes for chromosome " << name);
        haplo_sizes.clear();
        condensed.marker_locus.clear();
        condensed.marker_name.clear();
        double prev = 0;
        int hs = 0;
        haplo_sizes.reserve(raw.marker_locus.size());
        for (double l: raw.marker_locus) {
            if (l != prev) {
                haplo_sizes.push_back(hs);
                hs = 1;
                prev = l;
            } else {
                ++hs;
            }
        }
        if (hs) {
            haplo_sizes.push_back(hs);
        }

        /* now compute condensed */
        condensed.marker_name.clear();
        condensed.marker_locus.clear();
        condensed.marker_name.reserve(haplo_sizes.size());
        condensed.marker_locus.reserve(haplo_sizes.size());
        for (auto h: *this) {
            condensed.marker_name.push_back(raw.marker_name[h.first]);
            condensed.marker_locus.push_back(raw.marker_locus[h.first]);
        }
    }

    chromosome& operator = (const chromosome& c)
    {
        name = c.name;
        raw = c.raw;
        haplo_sizes = c.haplo_sizes;
        condensed = c.condensed;
        return *this;
    }

    struct haplotype_iterator {
        typedef std::pair<size_t, size_t> value_type;
        std::vector<int>::const_iterator hs_i, hs_j;
        size_t m_locus_index;

        haplotype_iterator(const chromosome& c, bool at_end)
            : hs_i(at_end ? c.haplo_sizes.end() : c.haplo_sizes.begin())
            , hs_j(c.haplo_sizes.end())
            , m_locus_index(at_end ? c.raw.marker_locus.size() : 0)
        {}

        value_type operator * () const
        {
            return {m_locus_index, m_locus_index + *hs_i};
        }

        haplotype_iterator& operator ++ ()
        {
            m_locus_index += *hs_i;
            ++hs_i;
            return *this;
        }

        size_t
            operator - (const haplotype_iterator& hi) const
            {
                return hs_i - hi.hs_i;
            }

        bool operator == (const haplotype_iterator& hi) const
        {
            return hs_i == hi.hs_i;
        }

        bool operator != (const haplotype_iterator& hi) const
        {
            return !(*this == hi);
        }
    };

    haplotype_iterator begin() const { return {*this, false}; }
    haplotype_iterator end() const { return {*this, true}; }

    braille_grid
        pretty_print(size_t width, const std::map<double, std::string>& POI, const std::map<double, std::pair<double, double>>& ROI) const
        {
            int padding_left;
            return pretty_print(width, POI, ROI, padding_left);
        }

    braille_grid
        pretty_print(size_t width, const std::map<double, std::string>& POI, const std::map<double, std::pair<double, double>>& ROI, int& padding_left, bool title=true) const
        {
            std::map<double, std::string> marker_labels;
            double length = raw.marker_locus.back();
            for (const auto& haplo: *this) {
                std::stringstream label;
                double pos = raw.marker_locus[haplo.first];
                label << raw.marker_name[haplo.first];
                for (size_t m = haplo.first + 1; m < haplo.second; ++m) {
                    label << ',' << raw.marker_name[m];
                }
                marker_labels[pos] = label.str();
            }
            /*MSG_DEBUG("marker_labels begin");*/
            /*for (const auto& ml: marker_labels) { MSG_DEBUG(ml.first << ": " << ml.second); }*/
            /*MSG_DEBUG("marker_labels end");*/

            /* compute padding_left */
            padding_left = 0;
            double x0 = 0, x1 = length;
            auto compute_padding_left
                = [&] (const std::map<double, std::string>& poi)
                {
                    for (const auto& pl: poi) {
                        size_t L = pl.second.size() + 1;
                        double xL = pl.first;
                        int min_pad = (int) ceil((x1 - x0) * L / (x1 - xL) + (x0 - xL) * width / (x1 - xL));
                        if (padding_left < min_pad) {
                            padding_left = min_pad;
                        }
                    }
                };
            compute_padding_left(marker_labels);
            compute_padding_left(POI);
            padding_left *= 2;

            /*size_t llsz = marker_labels.begin()->second.size();*/
            /*if (POI.size()) {*/
                /*llsz = std::max(llsz, POI.begin()->second.size());*/
            /*}*/
            /*int padding_left = 2 * (llsz + 1);*/
            double x_factor = ((2 * width - 1) - padding_left) / length;

            std::vector<int> label_x, label_y, label_anchor;
            std::vector<int> poi_x, poi_y, poi_anchor;
            std::vector<int> roi_x, roi_y, roi_anchor;
            size_t label_ymax, poi_ymax, roi_ymax;

            compute_xya()(padding_left, x_factor,
                          marker_labels,
                          [] (int a, const std::string& s) { return a - 2 * (1 + s.size()); },
                          [] (int a, const std::string&) { return a + 2; },
                          label_x, label_y, label_anchor, label_ymax);

            compute_xya()(padding_left, x_factor,
                    POI,
                    [] (int a, const std::string& s) { return a - 2 * (1 + s.size()); },
                    [] (int a, const std::string&) { return a + 2; },
                    poi_x, poi_y, poi_anchor, poi_ymax);

            double dx = (2 * width - padding_left) / length;

            compute_xya()(padding_left, x_factor,
                    ROI,
                    [&] (int, const std::pair<double, double>& r) { return padding_left + (int) round(r.first * dx); },
                    [&] (int, const std::pair<double, double>& r) { return padding_left + 1 + (int) round(r.second * dx); },
                    roi_x, roi_y, roi_anchor, roi_ymax);

            int height, y0;
            if (poi_ymax > 0 && roi_ymax > 0) {
                height = 16 + 4 * (((int) title) + label_ymax + poi_ymax) + 2 * roi_ymax;
                y0 = 4 * (2 + ((int) title) + poi_ymax) + 4 * ((roi_ymax + 1) >> 1);
            } else {
                height = 8 + 4 * (((int) title) + label_ymax);
                y0 = 4 * (1 + (int) title);
            }
            MSG_DEBUG("height=" << height << " y0=" << y0);
            MSG_QUEUE_FLUSH();
            braille_grid grid(2 * width, height);
            grid.set_background(40, 40, 40);

            int chr_r = 255, chr_g = 255, chr_b = 255;

            grid.line(label_anchor.front(), y0, 2 * label_anchor.back(), y0, 1, 0, chr_r, chr_g, chr_b);
            grid.line(label_anchor.front(), y0 + 1, 2 * label_anchor.back(), y0 + 1, 1, 0, chr_r, chr_g, chr_b);
            auto ai = label_anchor.begin();
            auto xi = label_x.begin();
            auto yi = label_y.begin();
            for (const auto& pl: marker_labels) {
                grid.line(*ai, y0 - 1, *ai, y0 + 4 * (*yi - 1), 1, 0);
                grid.line(*ai, y0 + 4 * (*yi - 1), *ai - 3, y0 + 4 * (*yi) - 1, 1, 0);
                grid.add_label(*xi, y0 + 4 * (*yi - 1), pl.second);
                ++ai;
                ++xi;
                ++yi;
            }

            ai = poi_anchor.begin();
            xi = poi_x.begin();
            yi = poi_y.begin();
            std::vector<palette::rgb> colors = {
                {0, 160, 0},
                {0, 0, 255},
                {255, 0, 0}
            };
            /*auto ci = colors.begin(), cj = colors.end();*/
            for (const auto& pl: POI) {
                int y = y0 - 4 * *yi - 4 * ((roi_ymax + 1) >> 1);
                grid.line(*ai, y0 +2, *ai, y, 1, 0, 0, 255, 0);
                grid.line(*ai, y, *ai - 3, y - 3, 1, 0, 0, 255, 0);
                grid.add_label(*xi, y - 4, pl.second, 0, 255, 0);
                /*++ci;*/
                /*if (ci == cj) { ci = colors.begin(); }*/
                ++ai;
                ++xi;
                ++yi;
            }

            /*ci = colors.begin();*/
            yi = roi_y.begin();
            for (const auto& roi: ROI) {
                int x0 = (int) (padding_left + roi.first * dx);
                int x1 = (int) (padding_left + roi.second.first * dx);
                int x2 = (int) (padding_left + roi.second.second * dx);
                int green = 160 + (length - roi.second.second + roi.second.first) * (255 - 160) / length;
                /*grid.filled_triangle(x0, y0 - 6, x1, y0 - 1, x2, y0 - 1, ci->r, ci->g, ci->b);*/
                int y = y0 + 1 - 2 * *yi;
                grid.line(x0, y - 6, x1, y - 1, 1, 1, 0, green, 0);
                grid.line(x0, y - 6, x2, y - 1, 1, 1, 0, green, 0);
                grid.line(x1, y, x2, y, 1, 0, 0, green, 0);
                /*grid.line(x1, y + 1, x2, y + 1, 1, 0, ci->r, ci->g, ci->b);*/
                /*++ci;*/
                /*if (ci == cj) { ci = colors.begin(); }*/
                ++ai;
                ++yi;
            }

            if (title) {
                grid.add_label(0, 0, SPELL_STRING("Chromosome " << name), 0, 255, 255);
            }

            /* pixels to characters */
            padding_left >>= 1;

            return grid;
        }
};

static inline
std::ostream&
operator << (std::ostream& os, const chromosome& c)
{
    os << "<chromosome:" << c.name;
    auto nbeg = c.condensed.marker_name.begin();
    auto nend = c.condensed.marker_name.end();
    auto lbeg = c.condensed.marker_locus.begin();
    for (; nbeg != nend; ++nbeg, ++lbeg) {
        os << ' ' << (*nbeg) << ':' << (*lbeg);
    }
    return os << '>';
}


static inline
std::ostream&
operator << (std::ostream& os, const std::vector<chromosome>& map)
{
    auto chrom = map.cbegin();
    auto cend = map.cend();
    for (; chrom != cend; ++chrom) {
        os << (*chrom) << std::endl;
    }
    return os;
}

#endif

