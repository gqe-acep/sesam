/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_QTL_CHROMOSOME_H_
#define _SPEL_QTL_CHROMOSOME_H_

/*#include <Eigen/Core>*/
/*#include <Eigen/SparseCore>*/
/*#include <unsupported/Eigen/MatrixFunctions>*/
/*#include <unsupported/Eigen/KroneckerProduct>*/
#include "eigen.h"


/*using namespace Eigen;*/

struct locus_key_struc;
typedef std::shared_ptr<const locus_key_struc> locus_key;

inline bool _notnull(const void* p) { return !!p; }
inline bool _null(const void* p) { return !p; }
template <class C> bool _notnull(const C* ptr) { return _notnull(static_cast<const void*>(ptr)); }

struct locus_key_struc {
    struct no_locus_exception {
        const char* what() const { return "Bad locus"; }
    };

    static constexpr const double no_locus = -1;

    locus_key parent;
    double locus;

    locus_key_struc() : parent(NULL), locus(no_locus) {}
    locus_key_struc(const locus_key_struc* p, double l) : parent(p), locus(l) {}
    locus_key_struc(const locus_key& p, double l) : parent(p), locus(l) {}
    locus_key_struc(const std::vector<double>& loci)
    {
        if (!loci.size()) {
            parent = NULL;
            locus = no_locus;
        } else {
            std::vector<double> sorted = loci;
            std::sort(sorted.begin(), sorted.end());
            locus_key p = NULL;
            double last = sorted.back();
            sorted.pop_back();
            for (double l: sorted) {
                p = locus_key(new locus_key_struc(p, l));
            }
            parent = p;
            locus = last;
        }
    }

    bool is_empty() const
    {
        return _null(this) || locus == no_locus;
    }

    size_t locus_order(double loc) const
    {
        if (is_empty()) {
            /* FIXME: should whine. */
            return 0;
        }
        if (locus <= loc) {
            return 0;
        }
        return 1 + parent->locus_order(loc);
    }

    size_t depth() const
    {
        if (_null(this) || locus == no_locus) {
            return 0;
        }
        if (parent) {
            return 1 + parent->depth();
        }
        return 1;
    }

    bool has(double loc) const
    {
        return _notnull(this) && (loc == locus || (parent && parent->has(loc)));
    }

    void _fill_vec(std::vector<double>& vec) const
    {
        if (parent) {
            parent->_fill_vec(vec);
        }
        vec.push_back(locus);
    }

    std::vector<double> to_vec() const
    {
        std::vector<double> ret;
        _fill_vec(ret);
        return ret;
    }

    /*locus_key_struc* operator + (const locus_key& k) = delete;*/
    /*locus_key_struc* operator - (const locus_key& k) = delete;*/

    friend
        locus_key operator + (const locus_key& k, double l)
        {
            if (!k || (k->parent == NULL && k->locus == no_locus)) {
                return locus_key(new locus_key_struc(NULL, l));
            } else if (l == k->locus) {
                return k;
            } else if (l > k->locus) {
                return locus_key(new locus_key_struc(k, l));
            } else {
                locus_key k2 = k->parent + l;
                return locus_key(new locus_key_struc(k2, k->locus));
            }
        }

    friend
        locus_key operator - (const locus_key& k, double l)
        {
            if (!k || (k->parent == NULL && k->locus == no_locus)) {
                return k;
            } else if (l == k->locus) {
                return k->parent;
            } else if (l > k->locus) {
                return k;
            } else {
                locus_key k2 = k->parent - l;
                return locus_key(new locus_key_struc(k2, k->locus));
            }
        }

    friend
        std::ostream& operator << (std::ostream& os, const locus_key_struc k)
        {
            if (k.locus == no_locus) {
                return os << "no_locus";
            }
            return (k.parent ? os << k.parent << '-' : os) << k.locus;
        }

    friend
        std::ostream& operator << (std::ostream& os, const locus_key k)
        {
            if (!k) {
                return os << "no_locus";
            }
            return os << (*k);
        }

    friend
        bool operator == (const locus_key& k1, const locus_key& k2)
        {
            bool bk1 = !!k1;
            bool bk2 = !!k2;
            if (bk1 && bk2) {
                return k1->locus == k2->locus
                    && (k1->parent == k2->parent);
            } else {
                return !(bk1 || bk2);
            }
        }

    friend
        bool operator != (const locus_key& k1, const locus_key& k2)
        {
            return !(k1 == k2);
        }

    friend
        locus_key operator + (const locus_key& k1, const locus_key& k2)
        {
            return k2 ? k1 + k2->parent + k2->locus : k1;
        }

    friend
        bool operator < (const locus_key& k1, const locus_key& k2)
        {
            if (k1->is_empty()) {
                return !k2->is_empty();
            } else if (k2->is_empty()) {
                return false;
            }

            std::vector<double> v1 = k1->to_vec();
            std::vector<double> v2 = k2->to_vec();
            auto i1 = v1.begin();
            auto j1 = v1.end();
            auto i2 = v2.begin();
            auto j2 = v2.end();
            while (i1 != j1 && i2 != j2 && *i1 == *i2) { ++i1; ++i2; }
            if (i1 == j1) {
                return i2 != j2;
            } else if (i2 == j2) {
                return false;
            } else {
                return *i1 < *i2;
            }
        }

    friend
        bool operator > (const locus_key& k1, const locus_key& k2)
        {
            return k2 < k1;
        }

    friend
        locus_key operator & (const locus_key& locus_key1, const locus_key& locus_key2)
        {
            locus_key k1 = locus_key1;
            locus_key k2 = locus_key2;
            std::vector<double> inter;
            while (k1 && k2) {
                if (k1->locus < k2->locus) {
                    k2 = k2->parent;
                } else if (k1->locus > k2->locus) {
                    k1 = k1->parent;
                } else {
                    inter.push_back(k1->locus);
                    k1 = k1->parent;
                    k2 = k2->parent;
                }
            }
            return locus_key(new locus_key_struc(inter));
        }

    friend
        locus_key operator - (const locus_key& k1, const locus_key& k2)
        {
            return k2 ? k1 - k2->parent - k2->locus : k1;
        }

    /* helper to create the reduction matrix when removing a locus from a complex model block. works on one chromosome of the selection. */
    Eigen::MatrixXd reduce(std::function<int(double)> n_ancestors_at_locus, double loc_remove) const
    {
        if (locus == no_locus) {
            return MatrixXd::Ones(1, 1);
        }

        MatrixXd ret;

        int n_geno = n_ancestors_at_locus(locus);

        /*MSG_DEBUG("reduce " << (*this) << " / " << loc_remove);*/
        
        if (loc_remove == locus) {
            /*MSG_DEBUG("this locus");*/
            ret = MatrixXd::Ones(n_geno, 1);
        } else {
            /*MSG_DEBUG("not this locus");*/
            ret = VectorXd::Ones(n_geno).asDiagonal();
        }
        if (parent) {
            return kroneckerProduct(
#ifdef LK_REDUCE_R2L
                        ret
                        ,
#endif
                        parent->reduce(n_ancestors_at_locus, loc_remove)
#ifndef LK_REDUCE_R2L
                        ,
                        ret
#endif
                        );
        } else {
            return ret;
        }
    }

    /* helper to create the reduction matrix when removing a locus from a complex model block. works on one chromosome of the selection. */
    Eigen::MatrixXd reduce(const std::map<double, int>& parent_count_per_position, double loc_remove) const
    {
        if (locus == no_locus) {
            return MatrixXd::Ones(1, 1);
        }

        MatrixXd ret;

        int n_geno = parent_count_per_position.find(locus)->second;

        /*MSG_DEBUG("reduce " << (*this) << " / " << loc_remove);*/
        
        if (loc_remove == locus) {
            /*MSG_DEBUG("this locus");*/
            ret = MatrixXd::Ones(n_geno, 1);
        } else {
            /*MSG_DEBUG("not this locus");*/
            ret = VectorXd::Ones(n_geno).asDiagonal();
        }
        if (parent) {
            return kroneckerProduct(
#ifdef LK_REDUCE_R2L
                        ret
                        ,
#endif
                        parent->reduce(parent_count_per_position, loc_remove)
#ifndef LK_REDUCE_R2L
                        ,
                        ret
#endif
                        );
        } else {
            return ret;
        }
    }

    struct iterator {
        const locus_key_struc* k;
        iterator() : k(NULL) {}
        iterator(const iterator& i) : k(i.k) {}
        iterator(const locus_key_struc& lk) : k(&lk) {}
        double operator * () const { return k ? k->locus : no_locus; }
        iterator& operator ++ () { if (k) { k = &*k->parent; } return *this; }
        bool operator == (const iterator& i) { return k == i.k; }
        bool operator != (const iterator& i) { return k != i.k; }
    };

    iterator begin() const { return {*this}; }
    iterator end() const { return {}; }
};

namespace std {
    template <>
    struct hash<locus_key> {
        size_t operator () (const locus_key& k) const
        {
            hash<double> h;
            return k
                   ? (k->parent
                      ? ((*this)(k->parent) * 0xdeadbee5)
                      : 0xdeadbee5) ^ h(k->locus)
                   : 0;
        }
    };

    inline locus_key __empty_locus_key() { static locus_key _(new locus_key_struc()); return _; }

    inline
        locus_key_struc::iterator
        begin(const locus_key& lk)
        {
            if (!lk) {
                return __empty_locus_key()->begin();
            }
            return lk->begin();
        }

    inline
        locus_key_struc::iterator
        end(const locus_key& lk)
        {
            if (!lk) {
                return __empty_locus_key()->end();
            }
            return lk->end();
        }

}

/* two responsibilities :
 * - give an augmented vector of loci
 * - augment an LV matrix with QTL LVs
 */
struct qtl_chromosome {
    const chromosome* chr;
    std::vector<double> qtl;

    qtl_chromosome() : chr(NULL), qtl() {}

    qtl_chromosome(const chromosome* c)
        : chr(c), qtl()
    {}

    qtl_chromosome(const qtl_chromosome& q)
        : chr(q.chr), qtl(q.qtl)
    {}

    void rec_init_qtl(const locus_key& k)
    {
        if (k->parent && k->locus != locus_key_struc::no_locus) {
            rec_init_qtl(k->parent);
        }
        qtl.push_back(k->locus);
    }

    qtl_chromosome(const chromosome* c, const locus_key& k)
        : chr(c), qtl()
    {
        if (k && k->locus != locus_key_struc::no_locus) {
            rec_init_qtl(k);
        }
    }

    qtl_chromosome& add_qtl(const std::vector<double>& l)
    {
        std::vector<double> tmp;
        tmp.resize(l.size() + qtl.size());
        auto end = std::set_union(l.begin(), l.end(), qtl.begin(), qtl.end(), tmp.begin());
        tmp.resize(end - tmp.begin());
        qtl.swap(tmp);
        return *this;
    }

    qtl_chromosome& remove_qtl(const std::vector<double>& l)
    {
        std::vector<double> tmp;
        tmp.resize(l.size() + qtl.size());
        auto end = std::set_difference(qtl.begin(), qtl.end(), l.begin(), l.end(), tmp.begin());
        tmp.resize(end - tmp.begin());
        qtl.swap(tmp);
        return *this;
    }

    void copy_loci(std::vector<double>& loci) const
    {
        loci.clear();
        loci.resize(chr->condensed.marker_locus.size() + qtl.size());
        auto end = std::set_union(chr->condensed.marker_locus.begin(), chr->condensed.marker_locus.end(),
                                  qtl.begin(), qtl.end(), loci.begin());
        loci.resize(end - loci.begin());
    }

    struct qtl_state_iterator_type {
        const qtl_chromosome* chr;
        std::vector<int> qtl_state;
        const std::vector<VectorXd>* state_vectors;
        std::vector<int> qtl_col;
        std::vector<int> qtl_blend;

        qtl_state_iterator_type(const qtl_chromosome* c, const std::vector<VectorXd>& states)
            : chr(c), qtl_state(c->qtl.size()), state_vectors(&states), qtl_col(), qtl_blend()
        {
            qtl_col.reserve(c->qtl.size());
            qtl_blend.reserve(c->qtl.size());
            /*MSG_DEBUG("NEW QTL_STATE_ITERATOR with " << c->qtl.size() << " QTLs to iterate over " << states.size() << " states");*/
        }

        long size() const
        {
            size_t exp = qtl_state.size();
            long base = state_vectors->size();
            long ret = 1;
            while (exp) {
                ret = exp & 1 ? ret * base : ret;
                exp >>= 1;
                base *= base;
            }
            return ret;
        }

        void reset() { std::fill(qtl_state.begin(), qtl_state.end(), 0); }

        bool next()
        {
            auto i = qtl_state.rbegin();
            auto j = qtl_state.rend();
            bool carry = true;
            int sz = state_vectors->size();
            while (i != j && carry) {
                ++*i;
                carry = *i >= sz;
                if (carry) {
                    *i = 0;
                }
                ++i;
            }
            return carry;
        }

        /* output MUST be correctly sized ! */
        bool init_expansion(const MatrixXd& obs, MatrixXd& output)
        {
            /*MSG_DEBUG(MATRIX_SIZE(obs) << std::endl << obs.transpose());*/
            /*MSG_DEBUG(MATRIX_SIZE(output) << std::endl << output.transpose());*/
            int obs_col = 0;
            int output_col = 0;
            auto li = chr->chr->condensed.marker_locus.begin();
            auto lj = chr->chr->condensed.marker_locus.end();
            auto qi = chr->qtl.begin();
            auto qj = chr->qtl.end();
            auto qsi = qtl_state.begin();
            qtl_col.clear();
            bool ret = true;
            while (li != lj && qi != qj) {
                if (*li == *qi) {
                    /* if the QTL is on a marker locus */
                    /*MSG_DEBUG("Selected QTL on a marker at locus " << (*li));*/
                    qtl_col.push_back(output_col);
                    qtl_blend.push_back(obs_col);
                    output.col(output_col) = (
                            (*state_vectors)[*qsi].array() * obs.col(obs_col).array()
                        ).matrix();
                    if ((output.array().col(output_col) == 0).all()) {
                        ret = false;
                    }
                    ++li;
                    ++qi;
                    ++qsi;
                    ++obs_col;
                } else if (*li < *qi) {
                    /* if the marker locus is before the next QTL */
                    output.col(output_col) = obs.col(obs_col);
                    ++li;
                    ++obs_col;
                } else {
                    /* if the QTL is before the next marker locus */
                    qtl_col.push_back(output_col);
                    qtl_blend.push_back(-1);
                    output.col(output_col) = (*state_vectors)[*qsi];
                    ++qi;
                    ++qsi;
                }
                ++output_col;
            }
            for (; li != lj; ++li) {
                output.col(output_col++) = obs.col(obs_col++);
            }
            for (; qi != qj; ++qi, ++qsi) {
                qtl_col.push_back(output_col);
                qtl_blend.push_back(-1);
                output.col(output_col++) = (*state_vectors)[*qsi];
            }
            /*std::clog << "first iteration" << std::endl << obs << "--" << std::endl << output << std::endl;*/
            return ret;
        }

        bool update(const MatrixXd& obs, MatrixXd& output)
        {
            /*std::clog << "qsi state " << qtl_state << std::endl;*/
            for (size_t i = 0; i < qtl_col.size(); ++i) {
                if (qtl_blend[i] != -1) {
                    output.col(qtl_col[i]) = ((*state_vectors)[qtl_state[i]].array() * obs.col(qtl_blend[i]).array()).matrix();
                    if ((output.array().col(qtl_col[i]) == 0).all()) {
                        return false;
                    }
                } else {
                    output.col(qtl_col[i]) = (*state_vectors)[qtl_state[i]];
                }
            }
            /*std::clog << "new iteration" << std::endl << output << std::endl;*/
            return true;
        }
    };

    qtl_state_iterator_type qtl_state_iterator(const std::vector<VectorXd>& states) const
    {
        return qtl_state_iterator_type(this, states);
    }
};


static inline
std::ostream& operator << (std::ostream& os, const qtl_chromosome& qc)
{
    return os << "<chromosome " << qc.chr->name << " QTLs={" << qc.qtl << "}>";
}

#endif

