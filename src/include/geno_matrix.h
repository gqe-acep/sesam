/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_GENO_MATRIX_H_
#define _SPEL_GENO_MATRIX_H_

#include "eigen.h"
#include "error.h"
#include "labelled_matrix.h"
#include "symmetry.h"
#include <limits>
#include <cmath>
#include "stl_output.h"

#define FLOAT_TOL 1.e-6


#undef WITH_OVERLUMPING
// #define WITH_OVERLUMPING


/*#define eq_dbl(_x_, _y_) (fabs((_x_) - (_y_)) < FLOAT_TOL)*/
/*#define neq_dbl(_x_, _y_) (fabs((_x_) - (_y_)) >= FLOAT_TOL)*/

inline
bool eq_dbl(double a, double b) { return fabs(a - b) < FLOAT_TOL; }

inline
bool eq_dbl(double a, double b, double tol_) { return fabs(a - b) < tol_; }

inline
bool neq_dbl(double a, double b) { return fabs(a - b) >= FLOAT_TOL; }

inline
bool operator < (const std::vector<int>& v1, const std::vector<int>& v2)
{
    if (v1.size() < v2.size()) {
        return true;
    }
    if (v1.size() == v2.size()) {
        auto i1 = v1.begin();
        auto i2 = v2.begin();
        auto j1 = v1.end();
        for (; i1 != j1 && (*i1 == *i2); ++i1, ++i2);
        return i1 != j1 && (*i1 < *i2);
    }
    return false;
}


#define LOCUS_EPSILON FLOAT_TOL

inline
std::vector<double> compute_steps(const std::vector<double>& loci,
                                  double step,
                                  const std::vector<std::pair<double, double>>& exclusion)
{
    std::vector<double> ret;
    auto next_locus = loci.begin();
    auto done = loci.end();
    double cursor = *next_locus;
    auto eit = exclusion.begin();
    auto eend = exclusion.end();
    while (next_locus != done) {
        while (next_locus != done && (cursor + LOCUS_EPSILON) >= *next_locus) {
            cursor = *next_locus;
            ++next_locus;
        }
        /* FIXME check that no segment outside the exclusion zones will be skipped */
        while (eit != eend && cursor > eit->second) { ++eit; } // skip past segments
        if (eit == eend || cursor < eit->first) {
                ret.push_back(cursor);
        }
        cursor += step;
    }
    return ret;
}


inline
std::vector<double> compute_steps(const std::vector<double>& loci,
                                  double step)
{
    std::vector<std::pair<double, double>> excl;
    return compute_steps(loci, step, excl);
}



enum geno_matrix_variant_type {Nonsense=0, Gamete, DoublingGamete, SelfingGamete, Haplo, Geno, _GMVT_size};


extern geno_matrix_variant_type geno_matrix_variant_table[_GMVT_size][_GMVT_size];


/*inline*/
/*std::ostream& operator << (std::ostream& os, const label_type& l)*/
/*{*/
    /*return os << (l.first ? l.first : '*') << (l.second ? l.second : '*');*/
/*}*/

inline
std::ostream& operator << (std::ostream& os, const std::vector<label_type>& vl)
{
    for (const auto& l: vl) { os << ' ' << l; }
    return os;
}


/*inline*/
/*bool operator == (const label_type& l1, const label_type& l2)*/
/*{*/
    /*return (l1.first == l2.first && l1.second == l2.second)*/
        /*|| ((l1.first == 0 || l2.first == 0) && (l1.second == 0 || l2.second == 0));*/
/*}*/


inline
std::ostream& operator << (std::ostream& os, const std::map<char, int>& L)
{
    os << '{' << L.size() << ':';
    for (const auto& kv: L) {
        os << ' ' << kv.first;
    }
    return os << '}';
}


#ifdef WITH_OVERLUMPING

    inline
    MatrixXb latent_lump_m(const MatrixXb& collect, const MatrixXb& sym)
    {
        MSG_DEBUG_INDENT_EXPR("[latent_lump] ");
        MatrixXb tco = collect * sym;
        MSG_DEBUG("tco");
        MSG_DEBUG(tco);
        MatrixXb ret = MatrixXb::Zero(tco.rows(), tco.rows());
        int col = 0;
        for (int i = 0; i < tco.cols(); ++i) {
            bool new_one = true;
            for (int c = 0; c < col; ++c) {
                if (ret.col(c) == tco.col(i)) {
                    new_one = false;
                    break;
                }
            }
            if (new_one) {
                ret.col(col) = tco.col(i);
                ++col;
            }
        }
        MSG_DEBUG("ret");
        MSG_DEBUG(ret);
        MSG_DEBUG_DEDENT;
        return ret;
    }

    inline
    permutation_type latent_lump(const MatrixXb& collect, const permutation_type& sym)
    {
        return permutation_type{latent_lump_m(collect, sym.matrix<bool>())};
    }

inline
std::pair<std::vector<size_t>, std::vector<size_t>>
compute_size_factors(std::vector<size_t>& size_vec, const std::vector<bool>& reent)
{
    std::pair<std::vector<size_t>, std::vector<size_t>> ret;

    ret.first.resize(size_vec.size(), 1);
    ret.second.resize(size_vec.size(), 1);
    size_t total_size = 1;
    for (size_t i = 0; i < size_vec.size(); ++i) {
        if (!reent[i]) {
            total_size *= size_vec[i];
        }
    }
    /*ret.second.back() = 1;*/  // already initialized
    /*ret.first[0] = 1;*/
#if 0
    for (size_t i = 1; i < size_vec.size(); ++i) {
        ret.first[i] = ret.first[i - 1] * (reent[i - 1] ? 1 : size_vec[i - 1]);
    }
    for (size_t i = size_vec.size() - 1; i > 0; --i) {
        ret.second[i - 1] = (reent[i] ? 1 : ret.second[i]) * size_vec[i];
    }
#else
    ret.second[0] = total_size / size_vec[0];
    for (size_t i = 1; i < size_vec.size(); ++i) {
        ret.first[i] = ret.first[i - 1] * size_vec[i - 1];
        if (reent[i]) {
            ret.first[i] /= size_vec[i];
        }
        ret.second[i] = total_size / ret.first[i] / size_vec[i];
    }
#endif
    return ret;
}


#endif /* WITH_OVERLUMPING */

inline
MatrixXi classify_values(const MatrixXd& mat, double tol_)
{
    MatrixXi ret(mat.rows(), mat.cols());
    std::set<double> all_values;
    for (int i = 0; i < mat.rows(); ++i) {
        for (int j = 0; j < mat.cols(); ++j) {
            all_values.insert(mat(i, j));
        }
    }
    std::map<double, unsigned long> indices;
    double prev = std::numeric_limits<double>::infinity();
    for (double d: all_values) {
        if (eq_dbl(prev, d)) {
            indices[d] = indices[prev];
        } else {
            unsigned long s = indices.size();
            indices[d] = s;
        }
    }
    const double* dbl_data = mat.data();
    int* i_data = ret.data();
    for (int i = 0; i < mat.size(); ++i) {
        i_data[i] = indices[dbl_data[i]];
    }
    return ret;
    (void) tol_;
}


inline
MatrixXd round_values(const MatrixXd& mat, const MatrixXi& cls)
{
    std::vector<double> accum;
    std::vector<int> weight;
    int m = cls.maxCoeff();
    accum.resize(m + 1);
    weight.resize(m + 1);
    for (int i = 0; i < mat.rows(); ++i) {
        for (int j = 0; j < mat.cols(); ++j) {
            int k = cls(i, j);
            accum[k] += mat(i, j);
            ++weight[k];
        }
    }
    for (size_t i = 0; i < accum.size(); ++i) {
        accum[i] /= weight[i];
    }
    MatrixXd ret(mat.rows(), mat.cols());
    for (int i = 0; i < mat.rows(); ++i) {
        for (int j = 0; j < mat.cols(); ++j) {
            ret(i, j) = accum[cls(i, j)];
        }
    }
    return ret;
}


struct geno_matrix {
    std::string name;
    geno_matrix_variant_type variant = Nonsense; //// DEBUG: avoid error at runtime when building with option -fsanitizer=undefined
    std::vector<label_type> labels;
    MatrixXd inf_mat;
    MatrixXd p, p_inv, diag;
    VectorXd stat_dist;
    MatrixXd collect, dispatch;
    /*double norm_factor;*/

#ifdef WITH_OVERLUMPING
    symmetry_group_type symmetries;
    symmetry_group_type latent_symmetries;
#endif

#if 0
    geno_matrix() : variant(Nonsense), labels(), inf_mat(), p(), p_inv(), diag(), stat_dist() {}
    geno_matrix(geno_matrix_variant_type v,
                std::vector<label_type>&& l,
                MatrixXd& i, MatrixXd& _p, MatrixXd& pi, MatrixXd& d, VectorXd& sd)
    {
        variant = v;
        labels.swap(l);
        inf_mat.swap(i);
        p.swap(_p);
        p_inv.swap(pi);
        diag.swap(d);
        stat_dist.swap(sd);
    }


    geno_matrix(const geno_matrix& gm) { *this = gm; }
    geno_matrix(geno_matrix&& gm) { *this = std::move(gm); }

    geno_matrix& operator = (geno_matrix&& gm)
    {
        variant = gm.variant;
        labels.swap(gm.labels);
        inf_mat.swap(gm.inf_mat);
        p.swap(gm.p);
        p_inv.swap(gm.p_inv);
        diag.swap(gm.diag);
        stat_dist.swap(gm.stat_dist);
        return *this;
    }
#endif

    size_t rows() const { return inf_mat.rows(); }
    size_t cols() const { return inf_mat.cols(); }
    size_t size() const { return rows(); }

    geno_matrix& operator = (const geno_matrix& gm)
    {
        name = gm.name;
        variant = gm.variant;
        labels.assign(gm.labels.begin(), gm.labels.end());
        inf_mat = gm.inf_mat;
        p = gm.p;
        p_inv = gm.p_inv;
        diag = gm.diag;
        stat_dist = gm.stat_dist;
#ifdef WITH_OVERLUMPING
        symmetries = gm.symmetries;
        latent_symmetries = gm.latent_symmetries;
#endif
        collect = gm.collect;
        dispatch = gm.dispatch;
        return *this;
    }

    MatrixXd exp(double d) const
    {
        MatrixXd ret = p * ((d * .01 * diag).array().exp().matrix().asDiagonal()) * p_inv;
        /*MatrixXd check = (d * inf_mat).exp();*/
        /*if (!ret.isApprox(check)) {*/
            /*MSG_ERROR("BAD EXP" << std::endl << "with diag:" << std::endl << check << std::endl << "with exp:" << std::endl << ret, "");*/
        /*} else {*/
            /*MSG_INFO("GOOD EXP");*/
        /*}*/
        return ret;
    }

    void cleanup(MatrixXd& m)
    {
        m = (m.array().abs() <= FLOAT_TOL).select(MatrixXd::Zero(m.rows(), m.cols()), m);
    }

    void cleanup(VectorXd& v)
    {
        v = (v.array().abs() <= FLOAT_TOL).select(VectorXd::Zero(v.size()), v);
    }

    geno_matrix& cleanup()
    {
        cleanup(inf_mat);
        cleanup(diag);
        cleanup(p);
        cleanup(p_inv);
        return *this;
    }

    std::vector<double> unique_eigenvalues() const
    {
        std::set<double> tmp;
        for (int i = 0; i < diag.size(); ++i) {
            tmp.insert(diag(i));
        }
        return {tmp.begin(), tmp.end()};
    }

#if 0
    std::vector<MatrixXd> eigen_components() const
    {
        auto uev = unique_eigenvalues();
        std::vector<MatrixXd> ret;
        ret.reserve(uev.size());
        MSG_DEBUG_INDENT_EXPR("[EIGEN COMPONENTS] ");
        for (double ev: uev) {
            /*VectorXd tmp_diag = (diag.array() == ev).select(VectorXd::Ones(diag.size()), VectorXd::Zero(diag.size())).matrix();*/
            VectorXd tmp_diag = VectorXd::Zero(diag.size());
            for (int i = 0; i < tmp_diag.size(); ++i) {
                if (fabs(diag(i) - ev) < FLOAT_TOL) {
                    tmp_diag(i) = 1.;
                }
            }
            ret.emplace_back(p * tmp_diag.asDiagonal() * p_inv);
            MSG_DEBUG("eigenvalue " << ev << " at " << tmp_diag.transpose());
            MSG_DEBUG(ret.back());
            std::set<double> all_values;
            for (int i = 0; i < ret.back().rows(); ++i) {
                for (int j = 0; j < ret.back().cols(); ++j) {
                    all_values.insert(ret.back()(i, j));
                }
            }
            std::map<double, unsigned long> indices;
            double prev = std::numeric_limits<double>::infinity();
            for (double d: all_values) {
                if (eq_dbl(prev, d)) {
                    indices[d] = indices[prev];
                } else {
                    unsigned long s = indices.size();
                    indices[d] = s;
                }
            }
            MatrixXi grouped(ret.back().rows(), ret.back().cols());
            const double* dbl_data = ret.back().data();
            int* i_data = grouped.data();
            for (int i = 0; i < ret.back().size(); ++i) {
                i_data[i] = indices[dbl_data[i]];
            }
            MSG_DEBUG("--");
            MSG_DEBUG(('a' + grouped.array()).cast<char>());
        }
        MSG_DEBUG_DEDENT;
        return ret;
    }
#else
    std::vector<MatrixXi> eigen_components() const
    {
        auto uev = unique_eigenvalues();
        std::vector<MatrixXi> ret;
        ret.reserve(uev.size());
        /*MSG_DEBUG_INDENT_EXPR("[EIGEN COMPONENTS] ");*/
        for (double ev: uev) {
            /*VectorXd tmp_diag = (diag.array() == ev).select(VectorXd::Ones(diag.size()), VectorXd::Zero(diag.size())).matrix();*/
            VectorXd tmp_diag = VectorXd::Zero(diag.size());
            for (int i = 0; i < tmp_diag.size(); ++i) {
                if (fabs(diag(i) - ev) < FLOAT_TOL) {
                    tmp_diag(i) = 1.;
                }
            }
            /*MSG_DEBUG("eigenvalue " << ev << " at " << tmp_diag.transpose());*/
            MatrixXd tmp = p * tmp_diag.asDiagonal() * p_inv;
            /*MSG_DEBUG(tmp);*/
            std::set<double> all_values;
            for (int i = 0; i < tmp.rows(); ++i) {
                for (int j = 0; j < tmp.cols(); ++j) {
                    all_values.insert(tmp(i, j));
                }
            }
            std::map<double, unsigned long> indices;
            double prev = std::numeric_limits<double>::infinity();
            for (double d: all_values) {
                if (eq_dbl(prev, d)) {
                    indices[d] = indices[prev];
                } else {
                    unsigned long s = indices.size();
                    indices[d] = s;
                }
            }
            ret.emplace_back(tmp.rows(), tmp.cols());
            const double* dbl_data = tmp.data();
            int* i_data = ret.back().data();
            for (int i = 0; i < tmp.size(); ++i) {
                i_data[i] = indices[dbl_data[i]];
            }
            /*MSG_DEBUG("--");*/
            /*MSG_DEBUG(('A' + ret.back().array()).cast<char>());*/
        }
        /*MSG_DEBUG_DEDENT;*/
        return ret;
    }
#endif

    MatrixXd lim_inf() const
    {
        /*MSG_DEBUG("LIM_INF ####### LIM_INF");*/
        /*MSG_DEBUG((*this));*/
        MatrixXd diag_inf = (diag.array() == 0).select(VectorXd::Ones(diag.size()), VectorXd::Zero(diag.size())).matrix();
        return p * diag_inf.asDiagonal() * p_inv;
    }

    bool check_not_nan() const
    {
        if (!(inf_mat == inf_mat)) {
            MSG_DEBUG("NAN INF_MAT");
            return false;
        }
        if (!(p == p)) {
            MSG_DEBUG("NAN P");
            return false;
        }
        if (!(p_inv == p_inv)) {
            MSG_DEBUG("NAN P_INV");
            return false;
        }
        if (!(diag == diag)) {
            MSG_DEBUG("NAN DIAG");
            return false;
        }
        if (!(stat_dist == stat_dist)) {
            MSG_DEBUG("NAN STAT_DIST");
            return false;
        }
        return true;
    }

    std::map<label_type, VectorXd>
        LV_map() const;

    std::vector<label_type>
        get_unique_labels() const
        {
            std::set<label_type> latemp;
            std::vector<label_type> ret;
            ret.reserve(labels.size());
            for (const auto& l: labels) {
                if (latemp.find(l) == latemp.end()) {
                    latemp.insert(l);
                    ret.push_back(l);
                }
            }
            return ret;

            /*std::vector<label_type> unique_labels = labels;*/
            /*std::sort(unique_labels.begin(), unique_labels.end());*/
            /*auto it = std::unique(unique_labels.begin(), unique_labels.end());*/
            /*unique_labels.resize(it - unique_labels.begin());*/
            /*return unique_labels;*/
        }

    std::vector<VectorXd>
        unphased_LV() const
        {
            std::vector<VectorXd> unique_unphased_LV;
            std::vector<label_type> unique_labels = get_unique_labels();
            int sz = unique_labels.size();
            unique_unphased_LV.reserve(sz);
            for (int i = 0; i < sz; ++i) {
                VectorXd tmp = VectorXd::Zero(size());
                for (size_t s = 0; s < size(); ++s) {
                    if (labels[s] == unique_labels[i]) {
                        tmp(s) = 1;
                    }
                }
                unique_unphased_LV.push_back(tmp);
            }
            return unique_unphased_LV;
        }
};


inline
std::ostream& operator << (std::ostream& os, const geno_matrix& gm)
{
    Eigen::Map<Eigen::Array<label_type, Eigen::Dynamic, 1>> labels(const_cast<label_type*>(gm.labels.data()), gm.labels.size(), 1);
    std::set<label_type> uniq_labels(gm.labels.begin(), gm.labels.end());
#ifndef SPELL_UNSAFE_OUTPUT
    for (const auto& l: uniq_labels) {
        MSG_DEBUG(((labels == l).cast<int>().sum()) << ' ' << l << " states");
    }
#endif
    Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic> tmp(gm.rows() + 1, gm.cols() + 1);
    for (size_t i = 0; i < gm.labels.size(); ++i) {
        tmp(0, 1 + i) = "  ";
        tmp(0, 1 + i)[0] = gm.labels[i].first();
        tmp(0, 1 + i)[1] = gm.labels[i].second();
        tmp(1 + i, 0) = tmp(0, 1 + i);
    }
    for (size_t i = 0; i < gm.rows(); ++i) {
        for (size_t j = 0; j < gm.rows(); ++j) {
            std::stringstream ss; ss << gm.inf_mat(i, j);
            tmp(i + 1, j + 1) = ss.str();
        }
    }
    os << gm.name << std::endl;
    os << tmp;
    os << std::endl;
    /*os << "P" << std::endl << gm.p << std::endl;*/
    /*os << "Pinv" << std::endl << gm.p_inv << std::endl;*/
    /*MatrixXd diag = gm.diag.asDiagonal();*/
    os << "D" << std::endl << gm.diag.transpose() << std::endl;
    os << "STATIONARY DISTRIBUTION " << gm.stat_dist.transpose() << std::endl;
    /*braille_histogram<double> hist(200, 20);*/
    /*for (int i = 0; i < gm.stat_dist.size(); ++i) {*/
        /*hist.add_point(gm.stat_dist(i));*/
    /*}*/
    /*hist.render().set_background(0, 64, 0);*/
    /*os << "" << hist << std::endl;*/
#ifdef WITH_OVERLUMPING
    /*os << "SYMMETRIES" << std::endl << gm.symmetries << std::endl;*/
    /*os << "LATENT SYMMETRIES" << std::endl << gm.latent_symmetries << std::endl;*/
#endif
    os << "COLLECT " << MATRIX_SIZE(gm.collect) << std::endl;
    os << braille_grid(gm.collect) << std::endl;
    /*os << "DISPATCH" << std::endl;*/
    /*os << gm.dispatch << std::endl;*/
    return os;
}



#define P_NORM_FACTOR (.707106781186547524400844362104849039284835937688474036588339868995366239231053519425193767163820786367506923115456148512462418027925)


#define SELECT(__p, __b) ((__b) == GAMETE_R ? (__p).second : (__p).first)


#ifdef WITH_OVERLUMPING

#if 0
    inline
    bool combine_sym(const geno_matrix& ret, const geno_matrix& m1, const geno_matrix& m2,
                     std::set<symmetry_table_type>& tmp,
                     const symmetry_table_type& s1, const symmetry_table_type& s2)
    {
        /*MSG_DEBUG_INDENT_EXPR("[combine_sym] ");*/
        /*MSG_DEBUG("NEW SYM FROM");*/
        /*MSG_DEBUG(s1.dump(m1.labels));*/
        /*MSG_DEBUG("AND");*/
        /*MSG_DEBUG(s2.dump(m2.labels));*/
        /*MSG_DEBUG("...");*/
        symmetry_table_type stmp = s1 * s2;
        /*MSG_DEBUG(stmp.dump(ret.labels));*/
        MatrixXd sm = stmp.matrix().cast<double>();
        /*MSG_DEBUG(sm);*/
        bool result = false;
        if (stmp.is_consistent(ret.labels) && (sm.transpose() * ret.inf_mat * sm - ret.inf_mat).isZero(FLOAT_TOL)) {
        /*if ((sm.transpose() * ret.inf_mat * sm - ret.inf_mat).isZero(FLOAT_TOL)) {*/
            /*MSG_DEBUG("OK!" << std::endl << stmp);*/
            /*MSG_DEBUG("OK!");*/
            tmp.emplace(stmp);
            result = true;
        } else {
            /*MSG_DEBUG("NOT GOOD.");*/
            /*MSG_DEBUG(ret.inf_mat);*/
            /*MSG_DEBUG("--");*/
            /*MSG_DEBUG((sm.transpose() * ret.inf_mat * sm - ret.inf_mat));*/

            /*for (const auto& kv: stmp.switches) {*/
            /*MSG_DEBUG("switch " << kv.first << " -> " << kv.second);*/
            /*}*/
            /*for (const auto& kv: stmp.switch_map()) {*/
            /*MSG_DEBUG("switch map " << kv.first << " -> " << kv.second);*/
            /*}*/
        }
        /*MSG_DEBUG_DEDENT;*/
        return result;
    };
#endif /* 0 */

#endif /* WITH_OVERLUMPING */


inline
geno_matrix kronecker0(const geno_matrix& m1, const geno_matrix& m2)
{
    m1.check_not_nan();
    m2.check_not_nan();
    geno_matrix ret;
    ret.inf_mat.resize(m1.rows() * m2.rows(), m1.cols() * m2.cols());
    ret.labels.reserve(m1.labels.size() * m2.labels.size());
#if 0
    switch (m1.variant) {
        case Gamete:
            if (m2.variant == Gamete) {
                for (const auto& l1: m1.labels) {
                    for (const auto& l2: m2.labels) {
                        ret.labels.emplace_back(l1.first(), l2.first());
                    }
                }
                ret.variant = SelfingGamete;
                break;
            }
        case SelfingGamete:
        case DoublingGamete:
            MSG_ERROR("Gamete matrices can only be the right operand in a kronecker product", "");
            MSG_QUEUE_FLUSH();
            throw 0;
        case Haplo:
            if (m2.variant != Haplo) {
                MSG_ERROR("Only Haplo (x) Haplo is defined, not with RHS " << m2.variant, "");
                MSG_QUEUE_FLUSH();
                throw 0;
            }
            ret.variant = Geno;
            for (const auto& l1: m1.labels) {
                for (const auto& l2: m2.labels) {
                    ret.labels.emplace_back(l1.first(), l2.first());
                }
            }
            break;
        case Geno:
            switch (m2.variant) {
                case Gamete:
                    ret.variant = Haplo;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first()), GAMETE_EMPTY);
                        }
                    }
                    break;
                case DoublingGamete:
                    ret.variant = Geno;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first), SELECT(l1, (int)l2.first));
                        }
                    }
                    break;
                case SelfingGamete:
                    ret.variant = Geno;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first), SELECT(l1, (int)l2.second));
                        }
                    }
                    break;
                default:
                    MSG_ERROR("Only Geno (x) {any gamete} is defined", "");
                    MSG_QUEUE_FLUSH();
                    throw 0;
            };
            break;
        case Nonsense:
        case _GMVT_size:
            ;
    };
#else
    for (const auto& l1: m1.labels) {
        for (const auto& l2: m2.labels) {
            ret.labels.emplace_back(l1 * l2);
        }
    }
#endif
    ret.inf_mat = kroneckerProduct(m1.inf_mat, MatrixXd::Identity(m2.rows(), m2.cols()))
                + kroneckerProduct(MatrixXd::Identity(m1.rows(), m1.cols()), m2.inf_mat);
    ret.p = kroneckerProduct(m1.p, m2.p);
    ret.p_inv = kroneckerProduct(m1.p_inv, m2.p_inv);
    ret.diag = (kroneckerProduct(m1.diag, VectorXd::Ones(m2.cols()))
             + kroneckerProduct(VectorXd::Ones(m1.cols()), m2.diag));

    ret.collect = MatrixXd::Identity(ret.diag.size(), ret.diag.size());
    ret.dispatch = MatrixXd::Identity(ret.diag.size(), ret.diag.size());
    /*ret.norm_factor = m1.norm_factor * m2.norm_factor;*/

    /* Check! */
    /*MSG_DEBUG("CHECK");*/
    /*MSG_DEBUG((ret.p * ret.diag.asDiagonal() * ret.p_inv * ret.norm_factor) - ret.inf_mat);*/
    /*MSG_DEBUG("END CHECK");*/
    ret.stat_dist = kroneckerProduct(m1.stat_dist, m2.stat_dist);

    return ret.cleanup();
}


inline
geno_matrix kronecker(const geno_matrix& m1, const geno_matrix& m2)
{
    m1.check_not_nan();
    m2.check_not_nan();
    geno_matrix ret;
    ret.inf_mat.resize(m1.rows() * m2.rows(), m1.cols() * m2.cols());
    ret.labels.reserve(m1.labels.size() * m2.labels.size());

    for (const auto& l1: m1.labels) {
        for (const auto& l2: m2.labels) {
            ret.labels.emplace_back(l1 * l2);
        }
    }

    ret.inf_mat = kroneckerProduct(m1.inf_mat, MatrixXd::Identity(m2.rows(), m2.cols()))
                + kroneckerProduct(MatrixXd::Identity(m1.rows(), m1.cols()), m2.inf_mat);
    ret.p = kroneckerProduct(m1.p, m2.p);
    ret.p_inv = kroneckerProduct(m1.p_inv, m2.p_inv);
    ret.diag = (kroneckerProduct(m1.diag, VectorXd::Ones(m2.cols()))
             + kroneckerProduct(VectorXd::Ones(m1.cols()), m2.diag));

    ret.collect = MatrixXd::Identity(ret.diag.size(), ret.diag.size());
    ret.dispatch = MatrixXd::Identity(ret.diag.size(), ret.diag.size());

    ret.stat_dist = kroneckerProduct(m1.stat_dist, m2.stat_dist);

    return ret.cleanup();
}

inline
geno_matrix ancestor_matrix(const std::string& name, label_type::letter_type a)
{
    std::vector<label_type> l = {{a, a}};
    return {name, Geno, std::move(l),
            (MatrixXd(1, 1) << 0).finished(),
            (MatrixXd(1, 1) << 1).finished(),
            (MatrixXd(1, 1) << 1).finished(),
            (MatrixXd(1, 1) << 0).finished()/*, 1.*/,
            (VectorXd(1) << 1.).finished(),
            /*{{{{a, a}}, {{0, 0}}}},*/
            (MatrixXd(1, 1) << 1).finished(),
            (MatrixXd(1, 1) << 1).finished(),
#ifdef WITH_OVERLUMPING
            {},
            {}
#endif
    };
}



inline
geno_matrix recall_matrix(label_type::letter_type n)
{
    std::vector<label_type> l = {{0, n}};
    return {"", Geno, std::move(l),
            (MatrixXd(1, 1) << 0).finished(),
            (MatrixXd(1, 1) << 1).finished(),
            (MatrixXd(1, 1) << 1).finished(),
            (MatrixXd(1, 1) << 0).finished()/*, 1.*/,
            (VectorXd(1) << 1.).finished(),
            /*{{{{a, a}}, {{0, 0}}}},*/
            (MatrixXd(1, 1) << 1).finished(),
            (MatrixXd(1, 1) << 1).finished(),
#ifdef WITH_OVERLUMPING
            {},
            {}
#endif
    };
}






#ifndef WITH_OVERLUMPING
inline
geno_matrix lump_using_partition_weighted(const geno_matrix& m, const std::set<subset>& lumping_partition)
{
    m.check_not_nan();
    /*MSG_DEBUG("|| LUMPING");*/
    /*MSG_DEBUG("" << m);*/
    /*MSG_DEBUG("|| WITH PARTITION");*/
    /*MSG_DEBUG("" << lumping_partition);*/
    /*MSG_QUEUE_FLUSH();*/

    geno_matrix ret_lump;

    /* Creation of L1 */
    MatrixXd L1 = MatrixXd::Zero(lumping_partition.size(), m.cols());
    int i = 0;
    ret_lump.labels.clear();
    ret_lump.labels.reserve(lumping_partition.size());
    for (const auto& D: lumping_partition) {
        ret_lump.labels.push_back(m.labels[D.front()]);
        for (int s: D) {
            L1(i, s) = 1.;
        }
        ++i;
    }
    /*MSG_DEBUG("L1");*/
    /*MSG_DEBUG(L1);*/
    /* Creation of L2 */
    MatrixXd L2 = m.stat_dist.asDiagonal() * L1.transpose();
    L2.array().rowwise() /= L2.array().colwise().sum();
#else
inline
geno_matrix lump_using_partition_weighted(const geno_matrix& m, const MatrixXd& L1, const MatrixXd& L2)
{
    /*scoped_indent _("[LUPW] ");*/
    m.check_not_nan();
    /*MSG_DEBUG("|| LUMPING");*/
    /*MSG_DEBUG("" << m);*/
    /*MSG_DEBUG("|| WITH PARTITION");*/
    /*MSG_DEBUG("" << lumping_partition);*/
    MSG_QUEUE_FLUSH();

    geno_matrix ret_lump;

    /*MSG_DEBUG("## m.inf_mat" << std::endl << m.inf_mat);*/
    /*MSG_DEBUG("## L1" << std::endl << L1);*/
    /*MSG_DEBUG("## L2" << std::endl << L2);*/
    /*MSG_QUEUE_FLUSH();*/

    ret_lump.labels.clear();
    ret_lump.labels.reserve(L1.rows());
    int j = 0;
    for (int i = 0; i < L1.rows(); ++i) {
        while (L1(i, j) == 0.) { ++j; }
        ret_lump.labels.push_back(m.labels[j]);
    }
#endif
    /* Let's be blunt. */
    ret_lump.inf_mat =
        /* <em>And bold.</em> */
        L1 * m.inf_mat * L2;
    /*MSG_DEBUG("## ret_lump.inf_mat" << std::endl << ret_lump.inf_mat);*/

    /*MSG_DEBUG(MATRIX_SIZE(L1));*/
    /*MSG_DEBUG(L1);*/
    /*MSG_DEBUG(MATRIX_SIZE(m.stat_dist));*/
    /*MSG_DEBUG(m.stat_dist.transpose());*/
    /*MSG_QUEUE_FLUSH();*/

    ret_lump.stat_dist = L1 * m.stat_dist;

    VectorXd s = ret_lump.stat_dist.array().sqrt().matrix();
    /*MSG_DEBUG("## s = " << s.transpose());*/
    MatrixXd Uinv = s.asDiagonal();
    /*MSG_DEBUG("## Uinv" << std::endl << Uinv);*/
    MatrixXd U = s.array().inverse().matrix().asDiagonal();
    /*MSG_DEBUG("## U" << std::endl << U);*/
    MatrixXd tmp = U * ret_lump.inf_mat * Uinv;
    /*MSG_DEBUG("## tmp" << std::endl << tmp);*/

    if (!(tmp - tmp.transpose()).isZero()) {
        MSG_DEBUG("NOT SYMMETRIC!!!");
        MSG_DEBUG(tmp);
    }

    SelfAdjointEigenSolver<MatrixXd> saes(tmp);

    ret_lump.diag = saes.eigenvalues().real();
    MatrixXd p = saes.eigenvectors().real();
    MatrixXd p_inv = p.transpose();
    ret_lump.cleanup(p);
    ret_lump.cleanup(p_inv);
    /*MSG_DEBUG("================= p");*/
    /*MSG_DEBUG(p);*/
    /*MSG_DEBUG("================= p^-1");*/
    /*MSG_DEBUG(p_inv);*/
    /*MSG_DEBUG("================= diag");*/
    /*MSG_DEBUG(ret_lump.diag);*/
    /*MSG_DEBUG("================= p.p^-1");*/
    /*MSG_DEBUG((p * p_inv));*/

    ret_lump.p = Uinv * p;
    ret_lump.p_inv = p_inv * U;

    ret_lump.variant = m.variant;
    ret_lump.name=  m.name;

    ret_lump.cleanup();

#if 0
#ifdef WITH_OVERLUMPING
    {
        std::set<symmetry_table_type> tmp_sym;
        MatrixXb shrink = L1.cast<bool>();
        /*MSG_DEBUG("SHRINK . SHRINK:transpose");*/
        /*MSG_DEBUG((shrink * shrink.transpose()));*/
        for (const auto& S: m.symmetries) {
            /*tmp_sym.emplace(ret_lump.labels, shrink * S.matrix() * shrink.transpose());*/
            /*MSG_DEBUG("SHRINKING SYMMETRY");*/
            /*MSG_DEBUG(S.matrix());*/
            /*MSG_DEBUG("WITH");*/
            /*MSG_DEBUG(shrink);*/
            /*auto it_ok = tmp_sym.emplace(shrink * S.matrix() * shrink.transpose(), S.letters);*/
            /*auto it_ok =*/ tmp_sym.emplace(S.table.lump(lumping_partition), S.letters);
            /*MSG_DEBUG("RESULT");*/
            /*MSG_DEBUG(it_ok.first->matrix());*/
        }
        ret_lump.symmetries.assign(tmp_sym.begin(), tmp_sym.end());
    }
    {
        std::set<symmetry_table_type> tmp_sym;
        MatrixXb shrink = L1.cast<bool>();
        for (const auto& S: m.latent_symmetries) {
            /*tmp_sym.emplace(ret_lump.labels, shrink * S.matrix() * shrink.transpose());*/
            /*MSG_DEBUG(MATRIX_SIZE(shrink));*/
            /*MSG_DEBUG(MATRIX_SIZE(S.matrix()));*/
            /*MSG_QUEUE_FLUSH();*/
            MatrixXb ret = latent_lump_m(shrink, S.matrix());

            /*auto it_ok =*/ tmp_sym.emplace(permutation_type(ret), S.letters);
            /*MSG_DEBUG("LUMPING LATENT SYMMETRY");*/
            /*MSG_DEBUG(S.dump(m.labels, true));*/
            /*MSG_DEBUG("INTO");*/
            /*MSG_DEBUG(it_ok.first->dump(ret_lump.labels, true));*/

            /*tmp_sym.emplace(shrink * S.matrix() * shrink.transpose(), S.letters);*/
            /*tmp_sym.emplace(S);*/
        }
        ret_lump.latent_symmetries.assign(tmp_sym.begin(), tmp_sym.end());
    }
#endif
#endif

    /*MSG_DEBUG(MATRIX_SIZE(m.collect));*/
    /*MSG_DEBUG(m.collect);*/
    /*MSG_DEBUG(MATRIX_SIZE(L1));*/
    /*MSG_DEBUG(L1);*/
    /*MSG_QUEUE_FLUSH();*/
    ret_lump.collect = L1 * m.collect;
    /*MSG_DEBUG("New collect");*/
    /*MSG_DEBUG(ret_lump.collect);*/
    /*MSG_QUEUE_FLUSH();*/
    /*MSG_DEBUG(MATRIX_SIZE(m.dispatch));*/
    /*MSG_DEBUG(m.dispatch);*/
    /*MSG_DEBUG(MATRIX_SIZE(L2));*/
    /*MSG_DEBUG(L2);*/
    /*MSG_QUEUE_FLUSH();*/
    ret_lump.dispatch = m.dispatch * L2;
    /*MSG_DEBUG("New dispatch");*/
    /*MSG_DEBUG(ret_lump.dispatch);*/
    /*MSG_QUEUE_FLUSH();*/

    return ret_lump;
}




inline
void compute_LR(const geno_matrix& m, const std::set<subset>& lumping_partition, MatrixXd& L1, MatrixXd& L2)
{
    /* Creation of L1 */
    L1 = MatrixXd::Zero(lumping_partition.size(), m.cols());
    int i = 0;
    for (const auto& D: lumping_partition) {
        for (int s: D) {
            L1(i, s) = 1.;
        }
        ++i;
    }
    /* Creation of L2 */
    L2 = m.lim_inf().col(0).asDiagonal() * L1.transpose();
    L2.array().rowwise() /= L2.array().colwise().sum();
}


inline
VectorXd column_angles(const MatrixXd& g1, const MatrixXd& g2)
{
    /*MSG_DEBUG(MATRIX_SIZE(g1));*/
    /*MSG_DEBUG(g1);*/
    /*MSG_DEBUG(MATRIX_SIZE(g2));*/
    /*MSG_DEBUG(g2);*/
    VectorXd n1 = (g1.array() * g1.array()).colwise().sum().array().sqrt().matrix();
    /*MSG_DEBUG(MATRIX_SIZE(n1) << std::endl << n1.transpose());*/
    VectorXd n2 = (g2.array() * g2.array()).colwise().sum().array().sqrt().matrix();
    /*MSG_DEBUG(MATRIX_SIZE(n2) << std::endl << n2.transpose());*/
    VectorXd angles(n1.size());
    for (int i = 0; i < angles.size(); ++i) {
        double n = n1(i) * n2(i);
        if (n != 0.) {
            double dot = (g1.col(i).array() * g2.col(i).array()).sum();
            /*MSG_DEBUG("(" << g1.col(i).transpose() << ") . (" << g2.col(i).transpose() << ") = " << dot << ", n = " << n);*/
            if (dot < -n) {
                dot = -n;
            } else if (dot > n) {
                dot = n;
            }
            angles(i) = acos(dot / n);
        } else {
            angles(i) = 0.;
        }
    }
    /*MSG_DEBUG("ANGLES " << angles.transpose());*/
    return angles;
}



#ifdef WITH_OVERLUMPING

struct experimental_lumper {
    const geno_matrix& M;
    MatrixXd inf_mat_2;
    MatrixXd C0, D0;

    experimental_lumper(const geno_matrix& m)
        : M(m)
        , inf_mat_2(m.inf_mat * m.inf_mat)
        , C0(), D0()
    {
        auto P0 = partition_on_labels();
        C0 = make_collect(P0, M.inf_mat.cols());
        D0 = make_dispatch(C0);
    }

    template <typename KeyType, typename GetterFunc, typename KeyComp=std::less<KeyType>>
    void histogram(int n_data, GetterFunc getter)
    {
        braille_histogram<KeyType, KeyComp> hist(200, 20);
        for (int i = 0; i < n_data; ++i) {
            hist.add_point(getter(i));
        }
        hist.render().set_background(25, 25, 25);
        MSG_DEBUG("HISTOGRAM" << std::endl << hist);
    }


    std::set<subset>
        perfect_lumping(size_t max_size, std::set<subset>& best_not_over_max_size)
        {
            std::set<subset> P = partition_on_labels(), NP = P;
            bool not_set = true;
            do {
                auto C = make_collect(P, M.cols());
                MatrixXd mat = C * inf_mat_2;
                NP = P;
                P = update_partition(NP, mat);
                if (not_set && P.size() > max_size) {
                    best_not_over_max_size = NP;
                    not_set = false;
                }
            } while (P != NP);
            MSG_DEBUG("PERFECT LUMPING" << std::endl << braille_grid(make_collect(P, M.inf_mat.cols())).set_background(32,24,42));
            return P;
        }

    std::set<subset>
        perfect_lumping()
        {
            std::set<subset> P = partition_on_labels(), NP = P;
            do {
                auto C = make_collect(P, M.cols());
                MatrixXd mat = C * inf_mat_2;
                NP = P;
                P = update_partition(NP, mat);
            } while (P != NP);
            MSG_DEBUG("PERFECT LUMPING" << std::endl << braille_grid(make_collect(P, M.inf_mat.cols())).set_background(32,24,42));
            return P;
        }

    std::set<subset> find_lumping_partition(size_t max_size)
    {
        std::set<subset> Pms = partition_on_labels();
        std::set<subset> splits;
        bool did_split;
        do {
            did_split = false;
            auto C = make_collect(Pms, M.inf_mat.cols());
            /*auto D = make_dispatch(C);*/
            MatrixXd mat = C * inf_mat_2;
            MSG_DEBUG("Pms (" << Pms.size() << ')'  << std::endl << braille_grid(make_collect(Pms, M.inf_mat.cols())).set_background(32,16,48));

            std::map<label_type, std::vector<subset>> parts_by_label;
            for (const auto& part: Pms) {
                parts_by_label[M.labels[part.front()]].push_back(part);
            }

            std::vector<std::pair<subset, subset>> parts_and_splits;

            for (const auto & kv: parts_by_label) {
                subset best_part, best_split;
                double best = std::numeric_limits<double>::infinity();
                for (const auto& part: kv.second) {
                    splits.clear();
                    find_splits(part, mat, splits);
                    if (splits.size() == 1 && splits.begin()->size() == part.size()) {
                        continue;
                    }
                    auto i = splits.begin(), j = splits.end();
                    /*double best = std::numeric_limits<double>::infinity();*/
                    for (; i != j; ++i) {
                        auto Ptmp = Pms;
                        Ptmp.erase(part);
                        insert_split(Ptmp, part, *i);
                        MatrixXd Ctmp = make_collect(Ptmp, M.inf_mat.cols());
                        MatrixXd Dtmp = make_dispatch(Ctmp);
                        MatrixXd reduced_collect = C0 * Dtmp;
                        MatrixXd reduced_dispatch = Ctmp * D0;
                        /*MatrixXd murzien = reduced_collect * Ctmp * inf_mat_2 * Dtmp * reduced_dispatch;*/
                        MatrixXd murzien = reduced_collect * Ctmp * M.inf_mat * Dtmp * Ctmp * M.inf_mat * Dtmp * reduced_dispatch;
                        double e = (murzien).lpNorm<1>();
                        MSG_DEBUG("Score for split " << (*i) << " in part " << part << " = " << e);
                        MSG_DEBUG("Murzien:" << std::endl << murzien);
                        if (e < best) {
                            best = e;
                            best_part = part;
                            best_split = *i;
                        }
                    }
                }
                if (best_split.size() && best_split.size() != best_part.size()) {
                    MSG_DEBUG("Selected split " << best_split << " in part " << best_part << " with score=" << best);
                    parts_and_splits.emplace_back(best_part, best_split);
                }
            }

            for (const auto& ps: parts_and_splits) {
                Pms.erase(ps.first);
                did_split |= insert_split(Pms, ps.first, ps.second);
            }
        } while (did_split && Pms.size() < max_size);
        MSG_DEBUG("P (" << Pms.size() << ')' << std::endl << braille_grid(make_collect(Pms, M.inf_mat.cols())).set_background(32,16,48));
        return Pms;
    }

    geno_matrix
        do_lump(size_t max_size)
        {
            auto PP = perfect_lumping();
            auto CP = make_collect(PP, M.cols());
            auto DP = make_dispatch(CP);
            auto M_hat = lump_using_partition_weighted(M, CP, DP);
#ifdef WITH_OVERLUMPING
            if (M_hat.cols() > max_size) {
                experimental_lumper perfect(M_hat);
                MSG_DEBUG_INDENT_EXPR("[Lumped geno_matrix] ");
                MSG_DEBUG("" << perfect.M);
                MSG_DEBUG_DEDENT;
                auto P = perfect.find_lumping_partition(max_size);
                auto C = perfect.make_collect(P, perfect.M.cols());
                auto D = perfect.make_dispatch(C);
                return lump_using_partition_weighted(perfect.M, C, D);
            } else {
#endif
                return M_hat;
#ifdef WITH_OVERLUMPING
            }
#endif
        }

    bool insert_split(std::set<subset>& P, const subset& part, const subset& split)
    {
        if (split.size()) {
            P.insert(split);
        }
        subset tmp;
        subset_difference(part, split, tmp);
        if (tmp.size()) {
            P.insert(tmp);
        }
        return split.size() && tmp.size();
    }

    geno_matrix
        do_lump1(size_t)
        {
            std::set<subset> P = partition_on_labels(), NP;
            auto C = make_collect(P, M.cols());
            MatrixXd mat = C * inf_mat_2;
            NP = P;
            P = update_partition(NP, mat);
            C = make_collect(P, M.cols());
            auto D = make_dispatch(C);
            return lump_using_partition_weighted(M, C, D);
        }

    geno_matrix
        do_lump0(size_t max_size)
        {
            std::set<subset> P = partition_on_labels(), NP;
            /*std::set<subset> P = partition_on_labels_refined(), NP;*/

            size_t counter = 0;

            MatrixXd mat;

            MSG_DEBUG("INITIAL MATRIX" << std::endl << M);

            if (0)
            {
                /*NP = P;*/
                auto C = make_collect(P, M.cols());
                mat = C * inf_mat_2;
                /*MSG_DEBUG("MAT");*/
                /*MSG_DEBUG(mat);*/
                /*mat.array().colwise() /= mat.array().row(0).transpose();*/
#if 0
                for (int i = 0; i < mat.cols(); ++i) {
                    double m = mat.col(i).minCoeff();
                    if (m != 0) {
                        mat.col(i) /= m;
                    }
                }
#elif 0
                /*double min = mat.minCoeff();*/
                for (int i = 0; i < mat.rows(); ++i) {
                    for (int j = 0; j < mat.cols(); ++j) {
                        double d = mat(i, j);
                        if (d > -FLOAT_TOL && d < FLOAT_TOL) {
                            mat(i, j) = 0;
                        } else /*if (d != min)*/ {
                            mat(i, j) = 1;
                        }
                    }
                }
#else
                /*int nb = (int) ceil(-mat.minCoeff());*/
                /*braille_histogram hist(200, 50, nb ? nb : 1, .5);*/
                /*for (int i = 0; i < mat.cols(); ++i) {*/
                    /*hist.add_point(mat.col(i).minCoeff());*/
                /*}*/
                /*hist.render().set_background(25, 25, 25);*/
                /*MSG_DEBUG("HISTOGRAM" << std::endl << hist);*/

#endif
                /*MSG_DEBUG("MAT NORMALIZED");*/
                /*MSG_DEBUG(mat);*/
                NP = P;
                P = update_partition(NP, mat);
                ++counter;
            }

            if (P.size() > max_size) {
                MSG_WARNING("Initial partition bigger than max_size (" << P.size() << " vs. " << max_size << ')');
            }
            /*for (const auto& part: partition_on_labels()) {*/
                /*MSG_DEBUG_INDENT_EXPR('[' << M.labels[part.front()] << "] ");*/
                /*histogram<double>(part.size(), [&] (int i) -> double { return round(mat.col(part[i]).minCoeff() * 1024.) / 1024.; });*/
                /*MSG_DEBUG_DEDENT;*/
            /*}*/
            /*return lump_using_partition_weighted(M, P);*/

            //while (P != NP /* LOL. */ && P.size() < max_size) {
                NP = P; /* LOL. */
                /*MSG_DEBUG("Current partition");*/
                /*MSG_DEBUG("" << P);*/
                auto C = make_collect(P, M.cols());
                /*MSG_DEBUG("Current collect");*/
                /*MSG_DEBUG(C);*/
#if 0
                auto dango = build_dango(C);
                /*MSG_DEBUG("Current dango");*/
                /*MSG_DEBUG(dango);*/
                P = make_partition(dango);
                /*MSG_DEBUG("New partition");*/
                /*MSG_DEBUG("" << P);*/
#else
                mat = C * inf_mat_2;
                for (const auto& part: partition_on_labels()) {
                    MSG_DEBUG_INDENT_EXPR('[' << M.labels[part.front()] << "] ");
                    histogram<double>(part.size(), [&] (int i) -> double { return round(mat.col(part[i]).minCoeff() * 1024.) / 1024.; });
                    MSG_DEBUG_DEDENT;
                }
                P = update_partition(NP, mat);
#endif
                ++counter;
                MSG_DEBUG("Iteration went from " << NP.size() << " parts to " << P.size() << " parts.");
            //}
            /*if (P.size() > max_size) {*/
                /*NP = P;*/
                /*P = partial_split(NP, P, mat, (size_t) -1); // max_size);*/
                P = partial_split(NP, P, mat, (size_t) max_size);
                /*NP = compress_partition(P, mat, max_size);*/
            /*}*/
            MSG_DEBUG("Found partition after " << counter << " iterations");
            MSG_DEBUG("Error with lumping: " << compute_error(P));
            C = make_collect(P, M.cols());
            auto D = make_dispatch(C);
            return lump_using_partition_weighted(M, C, D);
        }

    std::set<subset>
        partition_on_labels_refined() const
        {
            MSG_DEBUG_INDENT_EXPR("[REFINED] ");
            auto PL = partition_on_labels();
            MSG_DEBUG("PL" << std::endl << PL);
            MatrixXd CT = make_collect(PL, M.cols()) * inf_mat_2;
            MSG_DEBUG("CT" << std::endl << CT);
            /*MatrixXb CTnz = (CT.array().abs() < FLOAT_TOL).select(*/
                                /*MatrixXb::Zero(CT.rows(), CT.cols()),*/
                                /*MatrixXb::Ones(CT.rows(), CT.cols())).matrix();*/
            MatrixXb CTnz(CT.rows(), CT.cols());
            for (int i = 0; i < CT.rows(); ++i) {
                for (int j = 0; j < CT.cols(); ++j) {
                    CTnz(i, j) = CT(i, j) > -FLOAT_TOL && CT(i, j) < FLOAT_TOL;
                }
            }
        
            MSG_DEBUG("CTnz" << std::endl << CTnz);
            std::vector<bool> visited(CT.cols(), false);
            std::set<subset> P;
            for (const auto& part: PL) {
                for (size_t i = 0; i < part.size(); ++i) {
                    int s1 = part[i];
                    if (visited[s1]) { continue; }
                    visited[s1] = true;
                    subset S(1, s1);
                    for (size_t j = i + 1; j < part.size(); ++j) {
                        int s2 = part[j];
                        if (visited[s2]) { continue; }
                        if (CT.col(s1) == CT.col(s2)) {
                            visited[s2] = true;
                            S.push_back(s2);
                        }
                    }
                    P.insert(S);
                }
            }
            MSG_DEBUG("P" << std::endl << P);
            MSG_DEBUG_DEDENT;
            MSG_QUEUE_FLUSH();
            return P;
        }

    std::set<subset>
        partition_on_labels() const
        {
            std::set<subset> ret;
            std::map<label_type, subset> tmp;
            for (size_t i = 0; i < M.labels.size(); ++i) {
                tmp[M.labels[i]].push_back(i);
            }
            for (const auto& kv: tmp) {
                ret.insert(kv.second);
            }
            return ret;
        }

    MatrixXd
        build_dango(const MatrixXd& C)
        {
            MatrixXd ret(2 * C.rows(), C.cols());
            ret << C, (C * inf_mat_2);
            return ret;
        }

    void
        find_splits(const subset& part, const MatrixXd& mat, std::set<subset>& ret)
        {
            std::vector<bool> visited(mat.cols(), false);
            for (size_t i = 0; i < part.size(); ++i) {
                int c1 = part[i];
                if (visited[c1]) {
                    continue;
                }
                subset S(1, c1);
                for (size_t j = i + 1; j < part.size(); ++j) {
                    int c2 = part[j];
                    if (visited[c2]) {
                        continue;
                    }
                    if ((mat.col(c1) - mat.col(c2)).isZero()) {
                        S.push_back(c2);
                        visited[c2] = true;
                    }
                }
                ret.insert(S);
            }
        }

    std::set<subset>
        update_partition(const std::set<subset>& P0, const MatrixXd& mat)
        {
            std::set<subset> ret;
            for (const auto& part : P0) {
                find_splits(part, mat, ret);
            }
            return ret;
        }

    std::set<subset>
        make_partition(const MatrixXd& mat)
        {
            std::vector<bool> visited(mat.cols(), false);
            std::set<subset> P;
            int nc1 = mat.cols();
            for (int c1 = 0; c1 < nc1; ++c1) {
                if (visited[c1]) {
                    continue;
                }
                /*visited[c1] = true;*/
                subset S(1, c1);
                for (int c2 = c1 + 1; c2 < mat.cols(); ++c2) {
                    if (visited[c2]) {
                        continue;
                    }
                    if ((mat.col(c1) - mat.col(c2)).isZero()) {
                        S.push_back(c2);
                        visited[c2] = true;
                    }
                }
                P.insert(S);
            }
            return P;
        }

    static
    MatrixXd
        make_collect(const std::set<subset>& lumping_partition, int n_cols)
        {
            /* Creation of L1 */
            MatrixXd C = MatrixXd::Zero(lumping_partition.size(), n_cols);
            int i = 0;
            for (const auto& part: lumping_partition) {
                for (int s: part) {
                    C(i, s) = 1.;
                }
                ++i;
            }
            return C;
        }

    MatrixXd
        make_dispatch(const MatrixXd& C)
        {
            /* Creation of L2 */
            /*MatrixXd D = M.stat_dist.asDiagonal() * C.transpose();*/
            /*D.array().rowwise() /= D.array().colwise().sum();*/
            MatrixXd norm_factor = (C * M.stat_dist.asDiagonal() * C.transpose()).diagonal().array().inverse().matrix().asDiagonal();
            MatrixXd D = M.stat_dist.asDiagonal() * C.transpose() * norm_factor;
            return D;
        }


    template <typename ColType>
    double
        dist_func(const ColType& c1, const ColType& c2)
        {
            VectorXd c = c1 - c2;
            return c.lpNorm<1>();
        }

    double compute_error(const std::set<subset>& P0, const std::set<subset>& P1)
    {
        MatrixXd C0 = make_collect(P0, M.inf_mat.cols());
        MatrixXd D0 = make_dispatch(C0);
        MatrixXd C1 = make_collect(P1, M.inf_mat.cols());
        MatrixXd D1 = make_dispatch(C1);
        MatrixXd DC0 = D0 * C0;
        MatrixXd DC1 = D1 * C1;
        MatrixXd error = C0 * DC0 * M.inf_mat * DC0 - DC1 * M.inf_mat * DC1 * D0;
        MSG_DEBUG("ERROR");
        MSG_DEBUG(error);
        return -(error).lpNorm<1>();
    }

    double compute_error(const std::set<subset>& P)
    {
#if 1
        MatrixXd C = make_collect(P, inf_mat_2.cols());
        MatrixXd norm_factor = (C * M.stat_dist.asDiagonal() * C.transpose()).diagonal().array().inverse().matrix().asDiagonal();
        MatrixXd D = M.stat_dist.asDiagonal() * C.transpose() * norm_factor;
        MatrixXd t_2_hat = C * inf_mat_2 * D;
        MatrixXd t_hat = C * M.inf_mat * D;
        MatrixXd t_hat_2 = t_hat * t_hat;
        return ((C0 * D) * (t_2_hat - t_hat_2) * (C * D0)).lpNorm<Eigen::Infinity>(); // / t_2_hat.rows();
#else
        const MatrixXd& T = inf_mat_2;
        MatrixXd C = make_collect(P, inf_mat_2.cols());
        MatrixXd D = make_dispatch(C);
        /*MatrixXd Phi = M.stat_dist.asDiagonal() * make_collect(partition_on_labels(), inf_mat_2.cols()).transpose();*/
        MatrixXd Phi = M.stat_dist.asDiagonal();
        /*MSG_DEBUG("Phi_t");*/
        /*MSG_DEBUG(Phi.transpose());*/
        MatrixXd CT = C * T;
        MatrixXd DC = D * C;
        /*MSG_DEBUG("C.T");*/
        /*MSG_DEBUG(CT);*/
        /*MSG_DEBUG("D.C");*/
        /*MSG_DEBUG(DC);*/
        MatrixXd f = CT * (MatrixXd::Identity(C.cols(), C.cols()) - DC) * Phi;
        double accum = 0;
        /*MSG_DEBUG("F(C)=");*/
        /*MSG_DEBUG(f);*/
        for (int r = 0; r < f.rows(); ++r) {
            accum += f.row(r).lpNorm<2>();
        }
        return accum;
#endif
    }

    static
        void
        subset_union(const subset& s1, const subset& s2, subset& output)
        {
            output.resize(s1.size() + s2.size());
            auto it = std::set_union(s1.begin(), s1.end(), s2.begin(), s2.end(), output.begin());
            output.resize(it - output.begin());
        }

    static
        void
        subset_difference(const subset& s1, const subset& s2, subset& output)
        {
            output.resize(s1.size() + s2.size());
            auto it = std::set_difference(s1.begin(), s1.end(), s2.begin(), s2.end(), output.begin());
            output.resize(it - output.begin());
        }



    std::set<subset>
        partial_split(const std::set<subset>& P0, const std::set<subset>& P1, const MatrixXd& mat, size_t max_size)
        {
            scoped_indent _("[COMPRESSING] ");
            MSG_DEBUG("" << P0);
            /*size_t n_allowed_splits = max_size - P0.size();*/

            std::vector<std::vector<VectorXd>> sub_part_columns;
            std::vector<MatrixXd> sub_part_matrices;
            std::vector<std::vector<subset>> sub_parts;
            /*std::vector<std::vector<double>> split_errors;*/
            std::vector<std::vector<int>> splits;
            for (const auto& S: P0) {
                MSG_DEBUG("IN PART " << S);
                sub_part_columns.emplace_back();
                sub_parts.emplace_back();
                auto& sp_column_vec = sub_part_columns.back();
                auto& sp_vec = sub_parts.back();
                std::vector<bool> visited(S.size(), false);
                for (int i = 0; i < (int) S.size(); ++i) {
                    if (visited[i]) { continue; } visited[i] = true;
                    sp_vec.emplace_back(1, S[i]);
                    sp_column_vec.push_back(mat.col(S[i]));
                    auto& sub_part = sp_vec.back();
                    for (int j = i + 1; j < (int) S.size(); ++j) {
                        if (visited[j]) { continue; }
                        if ((mat.col(S[i]) - mat.col(S[j])).isZero()) {
                            sub_part.push_back(S[j]);
                            visited[j] = true;
                        }
                    }
                }
#if 0
                sub_part_matrices.emplace_back(mat.rows(), sp_column_vec.size());
                MatrixXd& submat = sub_part_matrices.back();
                for (int i = 0; i < submat.cols(); ++i) {
                    submat.col(i) = sp_column_vec[i] / -sp_column_vec[i].minCoeff();
                    /*submat.col(i) = (sp_column_vec[i].array() == 0).select(*/
                                        /*MatrixXd::Zero(submat.rows(), 1),*/
                                        /*MatrixXd::Ones(submat.rows(), 1)).matrix();*/
                }
                MSG_DEBUG("subparts " << sp_vec);
                MSG_DEBUG(submat);
                MSG_DEBUG("Mt.M");
                MatrixXd tmp = submat.transpose() * submat;
                VectorXd diag = tmp.diagonal() * .5;
                for (int i = 0; i < tmp.cols(); ++i) {
                    tmp.row(i) -= diag.transpose();
                    tmp.col(i) -= diag;
                }
                MSG_DEBUG((tmp));
                MSG_DEBUG("sums");
                MSG_DEBUG(tmp.colwise().sum());
#else
                /*split_errors.emplace_back();*/
                /*auto& split_err_vec = split_errors.back();*/

                /* keep best split(s) in each part */

                splits.emplace_back();
                if (sp_vec.size() == 1) {
                    continue;
                }
                auto & split_vec = splits.back();
                std::set<subset> Ptest = P1;
                /*Ptest.erase(S);*/
                for (const auto& split: sp_vec) {
                    Ptest.erase(split);
                }
                double err_min = std::numeric_limits<double>::infinity();
                int i = 0;
                for (const auto& split: sp_vec) {
                    MSG_DEBUG("i=" << i);
                    subset rest;
                    subset_difference(S, split, rest);
                    Ptest.insert(split);
                    Ptest.insert(rest);
                    /*double err = compute_error(Ptest) * split.size();*/
                    double err = compute_error(P1, Ptest);
                    MSG_DEBUG(" * " << rest << " / " << split << ", error=" << err);
                    if (err > (err_min - FLOAT_TOL) && err < (err_min + FLOAT_TOL)) {
                        split_vec.push_back(i);
                        if (err_min > err) {
                            err_min = err;
                        }
                        MSG_DEBUG("new split_vec " << split_vec);
                    } else if (err < err_min) {
                        split_vec = {i};
                        err_min = err;
                        MSG_DEBUG("new split_vec " << split_vec << " new err_min " << err_min);
                    }
                    Ptest.erase(split);
                    Ptest.erase(rest);
                    ++i;
                }
                MSG_DEBUG("Sub-parts " << sp_vec);
                MSG_DEBUG("Splitting sub-parts " << split_vec);
#endif
            }

            std::set<subset> NP;
            auto splits_i = splits.begin();
            auto sub_parts_i = sub_parts.begin();
            for (const auto& S: P0) {
                if (splits_i->size() == 0) {
                    NP.insert(S);
                } else {
                    subset rest = S, tmp;
                    for (const auto& split: *splits_i) {
                        subset_difference(rest, sub_parts_i->at(split), tmp);
                        std::swap(rest, tmp);
                    }
                    if (rest.size() > 0) {
                        NP.insert(rest);
                    }
                    for (const auto& split: *splits_i) {
                        NP.insert(sub_parts_i->at(split));
                    }
                }
                ++splits_i;
                ++sub_parts_i;
            }

            if (P0.size() == NP.size() || NP.size() > max_size) {
                return P0;
            }

            return partial_split(NP, P1, make_collect(NP, M.cols()) * inf_mat_2, max_size);
        }

    std::set<subset>
        compress_partition0(const std::set<subset>& P0, const MatrixXd& mat, size_t max_size)
        {
            scoped_indent _("[COMPRESSING] ");
            MSG_DEBUG("" << P0);
            /*size_t n_allowed_splits = max_size - P0.size();*/
            size_t n_joins = P0.size() - max_size;
            /* ... */
            MatrixXd dist = MatrixXd::Constant(P0.size(), P0.size(), std::numeric_limits<double>::infinity());
            int i = 0;
            std::map<double, size_t, std::less<double>> dist_histogram;
            std::map<label_type, std::map<double, size_t, std::less<double>>> dist_histogram_by_label;
            auto s1i = P0.begin();
            auto s1j = P0.end();
            for (; s1i != s1j; ++s1i) {
                auto s2i = s1i;
                int j = i + 1;
                dist(i, i) = 0;
                int s1 = s1i->front();
                for (++s2i; s2i != s1j; ++s2i) {
                    int s2 = s2i->front();
                    if (M.labels[s1] != M.labels[s2]) {
                        ++j;
                        continue;
                    }
                    VectorXd v1 = mat.col(s1);
                    VectorXd v2 = mat.col(s2);
                    v1(i) = v1(j) = v1(i) + v1(j);
                    v2(i) = v2(j) = v2(i) + v2(j);
                    double d = dist_func(v1, v2);
                    dist(i, j) = dist(j, i) = d;
                    dist_histogram[d]++;
                    dist_histogram_by_label[M.labels[s1]][d]++;
                    ++j;
                }
                ++i;
            }

            MSG_DEBUG("DIST MATRIX");
            MSG_DEBUG(dist);
            MSG_DEBUG("DIST HISTOGRAMS");
            for (const auto& lh: dist_histogram_by_label) {
                MSG_DEBUG(" * " << lh.first);
                for (const auto& kv: lh.second) {
                    MSG_DEBUG(std::setw(11) << kv.first << " : " << kv.second);
                }
            }

            size_t join_accum = 0;
            double threshold = 0;
            for (const auto& kv: dist_histogram) {
                if (join_accum >= n_joins && (kv.first - threshold > FLOAT_TOL)) {
                    threshold = (kv.first + threshold) * .5;
                    break;
                }
                join_accum += kv.second;
                threshold = kv.first;
            }

            MatrixXb tmp = (dist.array() <= threshold).matrix();
            MatrixXb adj = tmp * tmp;
            MatrixXb adj2 = tmp;
            while (adj != adj2) {
                adj2 = adj;
                adj *= tmp;
            }

            MSG_DEBUG("THRESHOLD=" << threshold);
            MSG_DEBUG("ACCUM=" << join_accum);
            MSG_DEBUG("ADJACENCY MATRIX");
            MSG_DEBUG(braille_grid(adj));

            std::set<subset> ret;

            std::vector<bool> visited(P0.size(), false);
            s1i = P0.begin();
            for (int i = 0; i < (int) P0.size(); ++i, ++s1i) {
                if (visited[i]) {
                    continue;
                }
                visited[i] = true;
                subset S = *s1i;
                auto s2i = P0.begin();
                MSG_DEBUG("MERGING INTO " << S);
                for (int j = 0; j < (int) P0.size(); ++j, ++s2i) {
                    if (visited[j]) {
                        continue;
                    }
                    if (adj(i, j) && i != j) {
                        visited[j] = true;
                        S.insert(S.end(), s2i->begin(), s2i->end());
                        MSG_DEBUG(" + " << (*s2i));
                    }
                }
                ret.insert(S);
            }

            MSG_DEBUG("FINAL PARTITION");
            MSG_DEBUG("" << ret);

            return ret;
        }
};

#define DUMP_GEN(_x) do { MSG_DEBUG_INDENT_EXPR("[" #_x "] "); MSG_DEBUG(_x); MSG_DEBUG_DEDENT; } while (0)

        inline
            geno_matrix lump(const geno_matrix& M, size_t max_states=std::numeric_limits<size_t>::max())
            {
                experimental_lumper el(M);
                /*return el.derisavi().do_lump(max_states);*/
                return el.do_lump(max_states);
            }


    inline
    std::map<label_type, VectorXd>
        geno_matrix::LV_map() const
        {
            experimental_lumper el(*this);
            auto P = el.partition_on_labels();
            auto C = el.make_collect(P, inf_mat.cols());
            std::map<label_type, VectorXd> ret;
            int r = 0;
            for (const auto& S: P) {
                ret[labels[S.front()]] = C.row(r).transpose();
                ++r;
            }
            return ret;
        }

#else /* WITH_OVERLUMPING */

        inline
            geno_matrix lump(const geno_matrix& M, size_t max_states=std::numeric_limits<size_t>::max())
            {
                lumper<MatrixXd, label_type> l(M.inf_mat, M.labels);
                return lump_using_partition_weighted(M, l.refine_all());
                (void) max_states;
            }

#endif /* WITH_OVERLUMPING */


extern geno_matrix gamete, selfing_gamete, doubling_gamete;


inline
geno_matrix operator * (const geno_matrix& a, const geno_matrix& b)
{
    return kronecker(a, b);
}


inline
geno_matrix operator - (const geno_matrix& a)
{
    return lump(a * gamete);
}


inline
geno_matrix operator | (const geno_matrix& a, const geno_matrix& b)
{
    return lump((a * gamete) * (b * gamete));
}


inline
geno_matrix operator ~ (const geno_matrix& a)
{
    return lump(a * selfing_gamete);
}




struct kronecker_expr {
    std::vector<std::vector<label_type>> label_history;
    std::vector<std::vector<label_type::letter_type>> label_stacks;
    geno_matrix m1;
    std::string name;
    
    kronecker_expr(const std::string& n)
        : label_history(1)
        , label_stacks(1)
        , m1()
        , name(n)
    {}

    kronecker_expr&
        add(const geno_matrix& m2)
        {
            geno_matrix tmp;
            std::vector<std::vector<label_type>> new_history;
            std::vector<std::vector<label_type::letter_type>> new_stacks;

            if (!m2.size()) {
                tmp = m1;
                tmp.labels = compute_labels();
                /*MSG_DEBUG_INDENT_EXPR("[pre-lump] ");*/
                /*MSG_DEBUG(tmp);*/
                /*MSG_DEBUG_DEDENT;*/
                m1 = lump(tmp);

                new_history.reserve(m1.cols());
                new_stacks.reserve(m1.cols());
                int j = 0;
                /*MSG_DEBUG_INDENT_EXPR("[history] ");*/
                for (int i = 0; i < m1.collect.rows(); ++i) {
                    for (; m1.collect(i, j) == 0; ++j);
                    new_history.push_back(label_history[j]);
                    new_stacks.push_back(label_stacks[j]);
                    /*MSG_DEBUG(new_history.back() << " / " << new_stacks.back());*/
                }
                /*MSG_DEBUG_DEDENT;*/
                /*MSG_DEBUG_INDENT_EXPR("[accum] ");*/
                /*const geno_matrix& dump = *this;*/
                /*MSG_DEBUG(dump);*/
                /*MSG_DEBUG_DEDENT;*/

                label_history.swap(new_history);
                label_stacks.swap(new_stacks);

                return *this;
            }

            new_history.reserve(label_history.size() * m2.cols());
            /*MSG_DEBUG_INDENT_EXPR("[new history] ");*/
            for (const auto& h: label_history) {
                for (size_t i = 0; i < m2.cols(); ++i) {
                    new_history.push_back(h);
                    new_history.back().push_back(m2.labels[i]);
                    /*MSG_DEBUG(new_history.back());*/
                }
            }
            /*MSG_DEBUG_DEDENT;*/
            label_history.swap(new_history);
            if (!m1.size()) {
                m1 = m2;
                label_stacks.clear();
                label_stacks.reserve(m1.labels.size());
                auto hi = label_history.begin();
                for (const auto& l: m1.labels) {
                    label_stacks.emplace_back();
                    l.apply_to(label_stacks.back(), *hi++);
                }
                return *this;
            }

            tmp.inf_mat.resize(m1.rows() * m2.rows(), m1.cols() * m2.cols());
            /*tmp.labels.reserve(m1.labels.size() * m2.labels.size());*/
            new_stacks.reserve(label_stacks.size() * m2.labels.size());

            auto hi = label_history.begin();
            for (const auto& s1: label_stacks) {
                for (const auto& l2: m2.labels) {
                    /*tmp.labels.emplace_back(l1.mult(l2, *hi));*/
                    new_stacks.push_back(s1);
                    l2.apply_to(new_stacks.back(), *hi);
                    ++hi;
                }
            }

            label_stacks.swap(new_stacks);

            tmp.inf_mat
                = kroneckerProduct(m1.inf_mat, MatrixXd::Identity(m2.rows(), m2.cols()))
                + kroneckerProduct(MatrixXd::Identity(m1.rows(), m1.cols()), m2.inf_mat);
            tmp.p
                = kroneckerProduct(m1.p, m2.p);
            tmp.p_inv
                = kroneckerProduct(m1.p_inv, m2.p_inv);
            tmp.diag
                = (kroneckerProduct(m1.diag, VectorXd::Ones(m2.cols()))
                + kroneckerProduct(VectorXd::Ones(m1.cols()), m2.diag));

            tmp.collect
                = MatrixXd::Identity(tmp.diag.size(), tmp.diag.size());
            tmp.dispatch
                = MatrixXd::Identity(tmp.diag.size(), tmp.diag.size());

            tmp.stat_dist
                = kroneckerProduct(m1.stat_dist, m2.stat_dist);

            tmp.cleanup();

            m1 = tmp;

            return *this;
        }

    kronecker_expr&
        operator << (const geno_matrix& m) { return add(m); }

    std::vector<label_type>
        compute_labels() const
        {
            std::vector<label_type> ret;
            ret.reserve(label_stacks.size());
            for (const auto& s: label_stacks) {
                ret.emplace_back(s.front(), s.back());
            }
            /*MSG_DEBUG("STACKS " << label_stacks);*/
            /*MSG_DEBUG("LABELS " << ret);*/
            return ret;
        }

    operator const geno_matrix& ()
    {
        if (!m1.labels.size()) {
            m1.labels = compute_labels();
        }
        m1.name = name;
        return m1;
    }
};





#endif

