/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_ERROR_H_
#define _SPEL_ERROR_H_

#include <string>
#include <iostream>
#include <fstream>
#include <exception>
#include <set>
#include <utility>
#include <sstream>
#include <thread>
#include <mutex>
#include <functional>
#include <condition_variable>
#include <vector>
#include <deque>
#include <algorithm>
#include <iterator>
#include "stl_output.h"

extern "C" {
  #include <unistd.h>
#ifdef _WIN32
  #include <windows.h>
  #ifndef ENABLE_VIRTUAL_TERMINAL_PROCESSING
  #define ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x0004
  #endif
  #ifndef DISABLE_NEWLINE_AUTO_RETURN
  #define DISABLE_NEWLINE_AUTO_RETURN 0x0008
  #endif
#else
  #include <sys/ioctl.h>
#endif
  #include <signal.h>
}
//// some ANSI escape sequence does not work well on rstudio, neither Rgui and rcmdr
// #define _WHITE "\x1b[37;1m"
// #define _RED "\x1b[31;1m"
// #define _GREEN "\x1b[32;1m"
// #define _YELLOW "\x1b[33;1m"
// #define _CYAN "\x1b[36;1m"
// #define _NORMAL "\x1b[0;m"
#define _WHITE ""
#define _RED ""
#define _GREEN ""
#define _YELLOW ""
#define _CYAN ""
#define _NORMAL ""
// BLANKS20 is used to mimic the behaviour of "\x1b[K"
#define BLANKS20 "                    "

#define _DBG_INDENT_MARK '\x11'
#define _DBG_DEDENT_MARK '\x12'
#define _DBG_INDENT_MARK_S "\x11"
#define _DBG_DEDENT_MARK_S "\x12"
#define _DBG_SYNC_MARK '\x13'
#define _DBG_SYNC_MARK_S "\x13"

#define MSG_HANDLER_IS_SYNCED

#ifdef _WIN32
static inline const char *get_error_text() {

  static char message[256] = {0};
  FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,0,GetLastError(),0,message, 256,0);
  char *nl = strrchr(message, '\n');
  if (nl) *nl = 0;
  return message;
}
#endif

static inline std::string __fetch_string(const std::ostream& os)
{
    return dynamic_cast<const std::stringstream*>(&os)->str();
}
#define SPELL_STRING(_expr_) __fetch_string(std::stringstream() << _expr_)

//// DEBUG : quick (and dirty) workaround to deal with clang++ issue on macos
template<typename T>
static inline std::string vec_to_spell_string(const std::vector<T>& vec, const char* sep = " ", const char* prefix = "{", const char* suffix = "}")
{
    std::stringstream vts;
    vts << prefix;

    if (!vec.empty()) {
        // Convert n-1 first elements
        std::copy(vec.begin(), vec.end()-1, std::ostream_iterator<T>(vts, sep));
        vts << vec.back(); // last element, no delimiter
    }
    vts << suffix;
    return vts.str();
}


template <typename X, typename W>
using if_integral = typename std::enable_if<std::is_integral<X>::value, W>::type;

template <typename X, typename W>
using unless_integral = typename std::enable_if<!std::is_integral<X>::value, W>::type;



template <typename S>
struct builder {
    typedef std::basic_ostream<S> _Stream;
    std::stringstream _s;
    _Stream& out;

    static std::unique_lock<std::mutex> lock() { static std::mutex output_mutex; return std::unique_lock<std::mutex>(output_mutex); }

    builder(std::ostream& os) : _s(), out(os) {}
    builder(builder&& b) : _s(std::move(b._s)), out(b.out) {}
    ~builder() { auto synchronize_outputs = lock(); out << _s.str(); out.flush(); }

    builder<S>& operator << (const char* x);
    template <typename X> builder<S>& operator << (X&& x);
    builder<S>& operator << (_Stream&(*x)(_Stream&));
    builder<S>& operator << (std::ios_base& (&)(std::ios_base&));
};



template <typename S>
struct endpoint {
    typedef std::basic_ostream<S> _Stream;
    _Stream& out;

    endpoint(_Stream& _) : out(_) {}

    builder<S> operator << (const char* x);
    template <typename X> builder<S> operator << (X&& x);
    builder<S> operator << (_Stream&(*x)(_Stream&));
    builder<S> operator << (std::ios_base& (&)(std::ios_base&));
};



struct DirectOutputIsForbidden : public std::runtime_error {
    DirectOutputIsForbidden()
            : std::runtime_error("Direct output to std::cerr, cout, or clog is forbidden. Use msg_handler_t::{cout|cerr|clog}() instead.")
    {}
};


/* inspired from http://stackoverflow.com/questions/22042414/c-stream-insert-string-after-newline */
class HeaderInserter : public std::streambuf {
    std::streambuf* dest;
    bool start_of_line;
    /*int indent;*/
    std::vector<std::string> prefix;
    bool prefix_insert_mode;
    std::stringstream prefix_ss;
protected:
    int overflow(int ch) override;

public:
    HeaderInserter(std::streambuf* dest)
            : dest(dest)
            , start_of_line(true)
            /*, indent(0)*/
            , prefix()
            , prefix_insert_mode(false)
            , prefix_ss()
    {}

    void rdbuf(std::streambuf* rb) { dest = rb; }
};

class ForbidOutput : public std::streambuf {
protected:
    int overflow(int) override
    {
        /* Direct output is forbidden */
        /*throw std::ios_base::failure("Direct output is forbidden");*/
        throw DirectOutputIsForbidden();
        abort();
    }
};

/* from http://wordaligned.org/articles/cpp-streambufs */
class teebuf: public std::streambuf
{
public:
    teebuf(std::streambuf * sb1, std::streambuf * sb2) : sb1(sb1), sb2(sb2) {}
private:
    virtual int overflow(int c)
    {
        if (c == EOF) {
            return !EOF;
        } else {
            int const r1 = sb1->sputc(c);
            int const r2 = sb2->sputc(c);
            return r1 == EOF || r2 == EOF ? EOF : c;
        }
    }

    virtual int sync() {
        int const r1 = sb1->pubsync();
        int const r2 = sb2->pubsync();
        return r1 == 0 && r2 == 0 ? 0 : -1;
    }
private:
    std::streambuf * sb1;
    std::streambuf * sb2;
};


struct ostream_manager {
    /* FIXME indent/dedent must be managed by this very class... Maybe use \x1 for indent and \x2 for dedent... */

    std::streambuf* old_cout_rdbuf;
    std::streambuf* old_clog_rdbuf;
    std::streambuf* old_cerr_rdbuf;
    HeaderInserter hi;
    ForbidOutput forbid;
    std::ostream _cerr, _cout, _clog;
    endpoint<char> cerr, cout, clog;

    std::vector<teebuf> tees;

    ostream_manager()
            : old_cout_rdbuf(std::cout.rdbuf())
            , old_clog_rdbuf(std::clog.rdbuf())
            , old_cerr_rdbuf(std::cerr.rdbuf())
            , hi(old_cout_rdbuf)
            , forbid()
            , _cerr(old_cerr_rdbuf), _cout(old_cout_rdbuf), _clog(&hi)
            , cerr(_cerr), cout(_cout), clog(_clog)
            , tees()
    {
        tees.reserve(3);
#ifndef SPELL_UNSAFE_OUTPUT
        std::clog.rdbuf(&forbid);
        std::cout.rdbuf(&forbid);
        std::cerr.rdbuf(&forbid);
#else
        std::clog.rdbuf(&hi);
#endif
    }

    ~ostream_manager()
    {
#ifndef SPELL_UNSAFE_OUTPUT
        std::clog.rdbuf(old_clog_rdbuf);
        std::cout.rdbuf(old_cout_rdbuf);
        std::cerr.rdbuf(old_cerr_rdbuf);
#endif
    }

    void
    redirect(std::ostream& os)
    {
        hi.rdbuf(os.rdbuf());
        _cerr.rdbuf(os.rdbuf());
        _cout.rdbuf(os.rdbuf());
    }

    void
    restore()
    {
        hi.rdbuf(old_clog_rdbuf);
        _cerr.rdbuf(old_cerr_rdbuf);
        _cout.rdbuf(old_cout_rdbuf);
    }

    void
    tee(std::ostream& os)
    {
        restore();
        tees.clear();
        tees.emplace_back(old_cout_rdbuf, os.rdbuf());
        _cout.rdbuf(&tees.back());
        tees.emplace_back(old_cerr_rdbuf, os.rdbuf());
        _cerr.rdbuf(&tees.back());
        tees.emplace_back(old_clog_rdbuf, os.rdbuf());
        hi.rdbuf(&tees.back());
    }
};



struct msg_handler_t {
#ifdef MSG_HANDLER_IS_SYNCED
    typedef std::recursive_mutex lock_type;
    typedef std::unique_lock<lock_type> scoped_lock_type;
#else
    typedef struct {
        void lock() {}
        void unlock() {}
    } lock_type;
    typedef struct _slt {
        _slt(lock_type&) {}
    } scoped_lock_type;
#endif

    struct state_t {
        bool color;
        std::set<std::string> workarounds;
        int count;
        int debug_indent;
        bool debug_enabled;
        bool quiet;
        std::vector<std::function<void()>> hooks;
        std::ostream raw_cout;
        ostream_manager manager;
        std::unique_ptr<std::ofstream> log_file;

        const char* error() { ++count; return color ? _RED : ""; }
        const char* warning() { return color ? _YELLOW : ""; }
        const char* info() { return color ? _CYAN : ""; }
        const char* normal() { return color ? _NORMAL : ""; }

        state_t()
                : color(!!isatty(fileno(stdout))), workarounds(), count(0), debug_indent(0), debug_enabled(true), quiet(false), hooks()
                , raw_cout(std::cout.rdbuf()), manager(), log_file(nullptr)
        {
            /*std::cout << "Message handler instance created." << std::endl;*/
#ifdef _WIN32
#if 0
            // Set output mode to handle virtual terminal sequences
            HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
            if (hOut == INVALID_HANDLE_VALUE)
            {
                std::cerr << "ERROR when GetStdHandle:\n";
                std::cerr << get_error_text() << "\n";
            }

            DWORD dwMode = 0;
            GetConsoleMode(hOut, &dwMode);
            // if (!GetConsoleMode(hOut, &dwMode))
            // {
                // std::cerr << "ERROR when GetConsoleMode:\n";
                // std::cerr << get_error_text() << "\n";
            // }

            dwMode = dwMode | ENABLE_VIRTUAL_TERMINAL_PROCESSING | DISABLE_NEWLINE_AUTO_RETURN;
            SetConsoleMode(hOut, dwMode);
            // if (!SetConsoleMode(hOut, dwMode))
            // {
                // std::cerr << "ERROR when SetConsoleMode:\n";
                // std::cerr << get_error_text() << "\n";
            // }
#endif
#endif
        }
        ~state_t() { close_log_file(); }
        bool check(bool fatal);
        void reset();
        void run_hooks() { for (auto& f: hooks) { f(); } }
        void close_log_file()
        {
            if (log_file) {
                manager.restore();
                log_file->close();
                log_file.reset(nullptr);
            }

        }
        void set_log_file(const std::string& path)
        {
            close_log_file();
            log_file.reset(new std::ofstream(path, std::ios_base::out|std::ios_base::app));
            manager.tee(*log_file);
        }
    };

    static state_t& instance() { static state_t _; return _; }

    static void set_color(bool _) { instance().color = _; }
    static bool color() { return instance().color; }

    static const char* e() { return instance().error(); }
    static const char* w() { return instance().warning(); }
    static const char* i() { return instance().info(); }
    static const char* n() { return instance().normal(); }

    static bool check(bool fatal) { return instance().check(fatal); }
    static void reset() { instance().reset(); }

    static void hook(std::function<void()>&& f) { instance().hooks.push_back(f); }
    static void run_hooks() { instance().run_hooks(); }

    static void indent() { instance().debug_indent += 3; }

    static endpoint<char>& cout() { return instance().manager.cout; }
    static endpoint<char>& cerr() { return instance().manager.cerr; }
    static endpoint<char>& clog() { return instance().manager.clog; }
//    static endpoint<std::ostream>& channel(msg_channel c)
//    {
//        switch (c) {
//            case msg_channel::Err: return cerr();
//            case msg_channel::Out: return cout();
//            case msg_channel::Log: return clog();
//        };
//    }

//    static void dedent() { instance().debug_indent -= 3; }
//    static int get_indent() { return instance().debug_indent; }

//    static void enqueue(const message_handle& mh) { instance().manager.enqueue(mh); }
//    static void wait_for_flush() { instance().manager.wait_for_flush(); }

    static int termcols()
    {
#if defined(_WIN32)
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
        int columns = csbi.srWindow.Right - csbi.srWindow.Left + 1;
        return columns;
#else
        struct winsize w;
        ioctl(0, TIOCGWINSZ, &w);
        return w.ws_col;
#endif
    }

    static bool& debug_enabled() { return instance().debug_enabled; }
    static bool& quiet() { return instance().quiet; }

    static lock_type mutex;

    static void redirect(std::ostream& os) { instance().manager.redirect(os); }
    static void restore() { instance().manager.restore(); }
    static void set_log_file(const std::string& path) { instance().set_log_file(path); }
};



inline
int HeaderInserter::overflow(int ch)
{
    int retval = 0;
    if (ch == _DBG_INDENT_MARK) {
        /*indent += 3;*/
        prefix_insert_mode = !prefix_insert_mode;
        if (prefix_insert_mode) {
            prefix_ss.clear();
            prefix_ss.str(std::string());
        } else {
            prefix.push_back(prefix_ss.str());
        }
    } else if (ch == _DBG_DEDENT_MARK) {
        if (prefix_insert_mode) {
            overflow(_DBG_INDENT_MARK);
        }
        if (prefix.size()) {
            prefix.pop_back();
        }
        /*indent -= 3 * (indent > 0);*/
    } else if (prefix_insert_mode) {
        prefix_ss << (char) ch;
    } else if (ch != traits_type::eof()) {
        if (start_of_line) {
            for (const std::string& s: prefix) {
                for (const char c: s) {
                    dest->sputc(c);
//                    dest->overflow(c);
                }
            }
            /*for (int i = 0; i < indent; ++i) {*/
            /*dest->sputc(' ');*/
            /*}*/
        }
        retval = dest->sputc(ch);
//        retval = dest->overflow(ch);
        start_of_line = ch == '\n';
    }
    return retval;
}

















#if 0

enum msg_channel { Out, Err, Log };

struct message_struc {
    msg_channel channel;
    std::string message;
    message_struc(msg_channel c, std::string&& msg) : channel(c), message(std::move(msg)) {}
};

typedef std::shared_ptr<message_struc> message_handle;

/*#define MAKE_MESSAGE(_dest_, _expr_) do { std::stringstream __s; __s << _expr_; _dest_ = __s.str(); } while (0)*/




/*#define CREATE_MESSAGE(_var_, _channel_, _expr_) message_handle _var_(new message_struc {_channel_, SPELL_STRING(_expr_)});*/

#ifndef SPELL_UNSAFE_OUTPUT
//#define CREATE_MESSAGE(_channel_, _what_) if (!msg_handler_t::quiet()) msg_handler_t::enqueue(message_handle{new message_struc {_channel_, _what_}});
#define CREATE_MESSAGE(_channel_, _what_) do { msg_handler_t::channel(_channel) << _what_; } while (0)
#else
#define CREATE_MESSAGE(_channel_, _what_) if (!msg_handler_t::quiet()) switch(_channel_) { \
        case msg_channel::Out: \
            std::cout << _what_; \
            break; \
        case msg_channel::Err: \
            std::cerr << _what_; \
            break; \
        case msg_channel::Log: \
            std::clog << _what_; \
            break; \
    };
#endif




struct message_queue : public ostream_manager {
    typedef std::mutex mutex_type;
    typedef std::unique_lock<mutex_type> scoped_lock_type;

    std::deque<message_handle> m_queue;
    mutex_type m_mutex;
    mutex_type m_stream_mutex;
    std::condition_variable m_condition;
    std::condition_variable m_flush_condition;

    static void catch_SIGSEG(int signo);
    static sighandler_t& old_sighandler() { static sighandler_t _; return _; }

    bool m_stop;

    std::thread m_thread;

    message_queue()
        : ostream_manager()
        , m_queue()
        , m_mutex()
        , m_condition()
        , m_stop(false)
        , m_thread([this] () { run(); })
    {
        /*old_sighandler() = signal(SIGSEGV, catch_SIGSEG);*/
    }

    ~message_queue()
    {
        {
            scoped_lock_type lock(m_mutex);
            m_stop = true;
        }
        m_condition.notify_one();
        m_thread.join();
    }

    void enqueue(const message_handle& mh)
    {
        scoped_lock_type slt(m_mutex);
        m_queue.push_back(mh);
        m_condition.notify_one();
    }

    void lock_stream() { m_stream_mutex.lock(); }
    void unlock_stream() { m_stream_mutex.unlock(); }

    void run();

    void wait_for_flush()
    {
        scoped_lock_type lock(m_mutex);
        while (!m_queue.empty()) {
            m_flush_condition.wait(lock);
        }
    }
};





inline
void message_queue::run()
{
    message_handle  next;
    while (true)
    {
        {
            scoped_lock_type lock(m_mutex);

            m_condition.wait(lock, [this]() { return m_stop || !m_queue.empty(); });

            /*while (!m_stop && m_queue.empty()) {*/
                /*m_condition.wait(lock);*/
            /*}*/

            if (m_stop && m_queue.empty()) {
                return;
            }

            next = m_queue.front();
            m_queue.pop_front();

        }
        if (next->message == _DBG_SYNC_MARK_S) {
            m_flush_condition.notify_one();
            continue;
        } else if (next->message.size() == 0) {
            continue;
        }
        std::ostream& channel = next->channel == msg_channel::Out ? cout
                              : next->channel == msg_channel::Log ? clog
                              : cerr;
        lock_stream();
        channel << next->message << std::flush;
        unlock_stream();
        msg_handler_t::run_hooks();
    }
}

#endif



#ifdef SPELL_UNSAFE_OUTPUT

#define MSG_ERROR(_msg_expr_, _workaround_expr_) do { std::cerr << msg_handler_t::e() << _msg_expr_ << msg_handler_t::n() << std::endl; } while (0)
#define MSG_WARNING(_msg_expr_)  do { std::cout << _msg_expr_ << std::endl; } while (0)
#define MSG_INFO(_msg_expr_) do { std::cout << _msg_expr_ << std::endl; } while (0)
#define MSG_QUEUE_FLUSH()
/*
#define MSG_DEBUG(_msg_expr_) do { std::clog << _msg_expr_ << std::endl; } while (0)
/*/
#define MSG_DEBUG(_)
//*/
#define MSG_DEBUG_INDENT
#define MSG_DEBUG_INDENT_EXPR(_str_)
#define MSG_DEBUG_DEDENT

#elif 0

#define MSG_ERROR(_msg_expr_, _workaround_expr_) \
    do {\
        CREATE_MESSAGE(msg_channel::Err, SPELL_STRING(msg_handler_t::e() << "[ERR] " << _msg_expr_ << msg_handler_t::n() << std::endl));\
        std::stringstream s; s << _workaround_expr_;\
        if (s.str().size()) { msg_handler_t::instance().workarounds.insert(s.str()); }\
} while (0)

#define MSG_WARNING(_msg_expr_) \
    do {\
        CREATE_MESSAGE(msg_channel::Out, SPELL_STRING(msg_handler_t::w() << "[WRN] " << _msg_expr_ << msg_handler_t::n() << std::endl));\
} while (0)

#define MSG_INFO(_msg_expr_) \
    do {\
        CREATE_MESSAGE(msg_channel::Out, SPELL_STRING(msg_handler_t::i() << "[MSG] " << _msg_expr_ << msg_handler_t::n() << std::endl));\
} while (0)

#define MSG_QUEUE_FLUSH() do { \
    CREATE_MESSAGE(msg_channel::Log, _DBG_SYNC_MARK_S); \
    msg_handler_t::wait_for_flush(); \
} while (0)

#define MSG_DEBUG(_msg_expr_) \
    do {\
        if (msg_handler_t::debug_enabled()) {\
            CREATE_MESSAGE(msg_channel::Log, SPELL_STRING(_msg_expr_ << std::endl));\
        }\
    } while (0);

#define MSG_DEBUG_INDENT CREATE_MESSAGE(msg_channel::Log, _DBG_INDENT_MARK_S "   " _DBG_INDENT_MARK_S)
#define MSG_DEBUG_INDENT_EXPR(_str_) CREATE_MESSAGE(msg_channel::Log, _DBG_INDENT_MARK << _str_ << _DBG_INDENT_MARK)
#define MSG_DEBUG_DEDENT CREATE_MESSAGE(msg_channel::Log, _DBG_DEDENT_MARK_S)

#else

#define MSG_ERROR(_msg_expr_, _workaround_expr_) \
    do {\
        msg_handler_t::cerr() << msg_handler_t::e() << "[ERR] " << _msg_expr_ << msg_handler_t::n() << std::endl;\
        std::stringstream s; s << _workaround_expr_;\
        if (s.str().size()) { msg_handler_t::instance().workarounds.insert(s.str()); }\
} while (0)

#define MSG_WARNING(_msg_expr_) \
    do {\
        msg_handler_t::cout() << msg_handler_t::w() << "[WRN] " << _msg_expr_ << msg_handler_t::n() << std::endl;\
} while (0)

#define MSG_INFO(_msg_expr_) \
    do {\
        msg_handler_t::cout() << msg_handler_t::i() << "[MSG] " << _msg_expr_ << msg_handler_t::n() << std::endl;\
} while (0)

#define MSG_QUEUE_FLUSH()

#define MSG_DEBUG(_msg_expr_) \
    do {\
        if (msg_handler_t::debug_enabled()) {\
            msg_handler_t::clog() << _msg_expr_ << std::endl;\
        }\
    } while (0);

#define MSG_DEBUG_INDENT do { msg_handler_t::clog() << _DBG_INDENT_MARK_S "   " _DBG_INDENT_MARK_S; } while (0)
#define MSG_DEBUG_INDENT_EXPR(_str_) do { msg_handler_t::clog() << _DBG_INDENT_MARK << _str_ << _DBG_INDENT_MARK; } while (0)
#define MSG_DEBUG_DEDENT do { msg_handler_t::clog() << _DBG_DEDENT_MARK_S; } while (0)

#endif

#if 0
#define MSG_ERROR(_msg_expr_, _workaround_expr_) \
    do {\
        {msg_handler_t::scoped_lock_type _(msg_handler_t::mutex);\
        std::cerr << msg_handler_t::e() << "[ERR] " << _msg_expr_ << msg_handler_t::n() << std::endl;}\
        std::stringstream s; s << _workaround_expr_;\
        if (s.str().size()) { msg_handler_t::instance().workarounds.insert(s.str()); }\
        msg_handler_t::run_hooks();\
} while (0)

#define MSG_WARNING(_msg_expr_) \
    do {\
        msg_handler_t::scoped_lock_type _(msg_handler_t::mutex);\
        std::cerr << msg_handler_t::w() << "[WRN] " << _msg_expr_ << msg_handler_t::n() << std::endl;\
        msg_handler_t::run_hooks();\
    } while(0)

#define MSG_INFO(_msg_expr_) \
    do {\
        msg_handler_t::scoped_lock_type _(msg_handler_t::mutex);\
        std::cout << msg_handler_t::i() << "[MSG] " << _msg_expr_ << msg_handler_t::n() << std::endl;\
        msg_handler_t::run_hooks();\
    } while(0)

#define MSG_DEBUG(_msg_expr_) \
    do {\
        msg_handler_t::scoped_lock_type _(msg_handler_t::mutex);\
        std::clog << _msg_expr_ << std::endl;\
        msg_handler_t::run_hooks();\
    } while(0)
#endif


/*#ifndef _SPELL_ERROR_H_MSGQ_CATCH_SIGSEG_*/
/*#define _SPELL_ERROR_H_MSGQ_CATCH_SIGSEG_*/
//inline
//void message_queue::catch_SIGSEG(int)
//{
//    MSG_DEBUG("**SEGFAULT DETECTED**");
//    MSG_QUEUE_FLUSH();
//    signal(SIGSEGV, old_sighandler());
//    raise(SIGSEGV);
//}

/*#endif*/

struct scoped_indent {
    scoped_indent() { MSG_DEBUG_INDENT; }
    scoped_indent(const std::string& str) { MSG_DEBUG_INDENT_EXPR(str); }
    ~scoped_indent() { MSG_DEBUG_DEDENT; }
};


inline bool msg_handler_t::state_t::check(bool fatal)
{
    msg_handler_t::scoped_lock_type _(msg_handler_t::mutex);
//     MSG_INFO("count = " << count);
    if (count > 0) {
        cerr() << info() << "[MSG] " << count << " error"
               << (count > 1 ? "s were" : " was")
               << " reported.";
        if (workarounds.size()) {
            cerr() << " Suggestions to fix this:" << std::endl;
            for (auto& w: workarounds) {
                cerr() << info() << "      - " << w << normal() << std::endl;
            }
        } else {
            cerr() << normal() << std::endl;
        }
        if (fatal) {
            cout() << normal() <<"At least one fatal error encountered. Aborting process." << std::endl;
            exit(-count);
        } else {
            reset();
            return true;
        }
    }
    return false;
}

inline void msg_handler_t::state_t::reset()
{
    if (workarounds.size()) {
        MSG_WARNING(workarounds.size() << " workarounds silently discarded");
    }
    count = 0;
    workarounds.clear();
}

#define WHITE (msg_handler_t::instance().color ? _WHITE : "")
#define YELLOW (msg_handler_t::instance().color ? _YELLOW : "")
#define GREEN (msg_handler_t::instance().color ? _GREEN : "")
#define RED (msg_handler_t::instance().color ? _RED : "")
#define CYAN (msg_handler_t::instance().color ? _CYAN : "")
#define NORMAL (msg_handler_t::instance().color ? _NORMAL : "")

#ifdef NDEBUG
#define DUMP_FILE_LINE()
#else
#define DUMP_FILE_LINE() MSG_DEBUG(__FILE__ << ':' << __LINE__)
#endif

#endif

