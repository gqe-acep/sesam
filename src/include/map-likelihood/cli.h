/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_MAPQA_CLI_H_
#define _SPELL_MAPQA_CLI_H_

#include "error.h"
// #include <map>
// #include <vector>
/*#include "generation_rs.h"*/
#include "commandline.h"
/*#include "bn.h"*/
/*#include "factor_var4.h"*/
// #include "file.h"
#include "data/chromosome.h"


struct mapqa_settings_t {
    /* Configuration */
    std::string name;
    std::string work_directory;

    chromosome group;
    
    mapqa_settings_t()
        : name()
        , work_directory(".")
    {}
    
    static
    mapqa_settings_t*
    from_args(int argc, const char** argv);
};

void print_usage();


#endif

