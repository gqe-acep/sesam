/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_PROBA_DTD_H_
#define _SPEL_PROBA_DTD_H_

/*#include <Eigen/Core>*/
#include "eigen.h"
#include "labelled_matrix.h"

struct probapop_output {
    std::vector<std::vector<double>> prob;
    std::vector<double> loci;
    operator Eigen::MatrixXd () const
    {
        Eigen::MatrixXd ret(4, prob.size());
        for (size_t j = 0; j < prob.size(); ++j) {
            for (int i = 0; i < 4; ++i) {
                ret(i, j) = prob[j][i];
            }
        }
        return ret;
    }

    operator labelled_matrix<Eigen::MatrixXd, double, double> () const
    {
        std::vector<double> tmp = {0, 0, 0, 0};
        labelled_matrix<Eigen::MatrixXd, double, double> ret(tmp, loci);
        ret.data = *this;
        return ret;
    }
};

extern probapop_output* read_probapop(std::istream& is);

#endif

