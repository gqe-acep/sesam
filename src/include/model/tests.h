/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_MODEL_TESTS_H_
#define _SPEL_MODEL_TESTS_H_

struct not_enough_observations_exception : std::exception {
    const char* what() const throw()
    {
        return "Not enough observations in model! DoF > #obs";
    }
};

static inline
void f_test(const model& model_current, const model& model_new, int col_num, MatrixXd* pvalue, MatrixXd* lod)
{
    /*auto res_cur = model_current.residuals().array();*/
    /*auto res_new = model_new.residuals().array();*/
    /*VectorXd rss_cur = (res_cur * res_cur).colwise().sum();*/
    /*VectorXd rss_new = (res_new * res_new).colwise().sum();*/
	VectorXd rss_cur = model_current.rss();
	VectorXd rss_new = model_new.rss();
    int dof_cur = model_current.rank();
    int dof_new = model_new.rank();
    int nind = model_current.Y().innerSize();
#if 0
    /*if (model_new.coefficients().outerSize() == 1) {*/
        /*MSG_DEBUG("==================================================================");*/
        /*MSG_DEBUG("nind " << nind);*/
        /*MSG_DEBUG("cur" << std::endl << model_current.X().transpose());*/
        /*MSG_DEBUG("Xt" << std::endl << model_new.X().transpose());*/
        /*MSG_DEBUG("X$dim(" << model_new.X().innerSize() << ',' << model_new.X().outerSize() << ')');*/
        /*MSG_DEBUG("Y$dim(" << model_new.Y().innerSize() << ',' << model_new.Y().outerSize() << ')');*/
        /*MSG_DEBUG("Y" << std::endl << model_new.Y());*/
        /*MSG_DEBUG("XtX" << std::endl << model_new.XtX());*/
        /*MSG_DEBUG("XtX^-1" << std::endl << model_new.XtX_pseudo_inverse());*/
        MSG_DEBUG("dof_cur " << dof_cur << ' ' << MATRIX_SIZE(model_current.X()) << " dof_new " << dof_new << ' ' << MATRIX_SIZE(model_new.X()) << " rss_cur " << rss_cur.transpose() << " rss_new " << rss_new.transpose() << " keys_cur " << model_current.keys() << " keys_new " << model_new.keys());

        /*MSG_DEBUG("Y_cur" << std::endl << model_current.Y());*/
        /*MSG_DEBUG("Y_new" << std::endl << model_new.Y());*/
        MSG_DEBUG("X_cur" << std::endl << model_current.X());
        MSG_DEBUG("X_new" << std::endl << model_new.X());
        /*MSG_DEBUG("XtX_cur" << std::endl << model_current.XtX());*/
        /*MSG_DEBUG("XtX_new" << std::endl << model_new.XtX());*/

        /*MSG_DEBUG("constraint residual " << model_new.residuals().bottomRows(1));*/
        /*MSG_DEBUG("coefs " << std::endl << model_new.coefficients());*/
        /*MSG_DEBUG("==================================================================");*/
    /*}*/
#endif
	/*VectorXd F(rss_new.innerSize() + 2);*/
	/*VectorXd F(rss_new.innerSize());*/
#if 0
    if (dof_new < dof_cur || dof_new == 0 || dof_cur == 0
            || (model_current.X().array() > 2.).any()
            || (model_new.X().array() > 2.).any()
            || (model_new.rss().array() > model_current.rss().array()).any()) {
        MSG_DEBUG("dof_cur " << dof_cur << ' ' << MATRIX_SIZE(model_current.X()) << " dof_new " << dof_new << ' ' << MATRIX_SIZE(model_new.X()) << " rss_cur " << rss_cur.transpose() << " rss_new " << rss_new.transpose() << " keys_cur " << model_current.keys() << " keys_new " << model_new.keys() << std::endl << "X_cur" << std::endl << model_current.X() << std::endl << "X_new" << std::endl << model_new.X());
    }
#endif
    if (dof_new <= dof_cur) {
        /*MSG_DEBUG("dof_new[" << dof_new << "] <= dof_cur[" << dof_cur << ']');*/
        /*F = VectorXd::Zero(rss_new.innerSize() + 2);*/
        if (pvalue) {
            pvalue->col(col_num) = VectorXd::Zero(rss_new.innerSize());
        }
        if (lod) {
            lod->col(col_num) = VectorXd::Zero(rss_new.innerSize());
        }
    } else if (nind <= dof_new) {
        /*MSG_DEBUG("nind[" << nind << "] <= dof_new[" << dof_new << ']');*/
        /* FIXME: HALT! */
        /*throw not_enough_observations_exception();*/
        if (pvalue) {
            pvalue->col(col_num) = VectorXd::Zero(rss_new.innerSize());
        }
        if (lod) {
            lod->col(col_num) = VectorXd::Zero(rss_new.innerSize());
        }
        /*F = VectorXd::Zero(rss_new.innerSize() + 2);*/
        /*F = VectorXd::Zero(rss_new.innerSize());*/
    } else {
	    int dof_num = dof_new - dof_cur;
	    int dof_denom = nind - dof_new;
	    double dof_num_inv = 1. / dof_num;
	    double dof_denom_inv = 1. / dof_denom;
	    /*MSG_DEBUG("dof_num " << dof_num);*/
	    /*MSG_DEBUG("dof_denom " << dof_denom);*/
        double lod_fac = 0;
        double F;
        if (lod) {
            lod_fac = .5 / log(10.) * dof_num;
        }
	    for (int i = 0; i < rss_new.innerSize(); ++i) {
	        if (dof_num > 0) {
	            double rnew = rss_new(i);
	            set_if_much_smaller_than(rnew, rss_cur(i));
	            F = ((rss_cur(i) - rnew) * dof_num_inv) / (rnew * dof_denom_inv);
	        } else {
	            F = 0;
	        }
            if (pvalue) {
                /* NdDL : c'est immonde. */ /* NdDL 2015 : on a fait tellement de trucs pires entre temps, rétrospectivement c'est assez soft en fait. */
                if (F <= 0) {
                    (*pvalue)(i, col_num) = 0;
                } else {
                    (*pvalue)(i, col_num) = log_ibeta(dof_denom * .5, dof_num * .5,
                                                      dof_denom / (dof_denom + dof_num * F));
                }
            }
            if (lod) {
                (*lod)(i, col_num) = F * lod_fac;
            }
	    }
	}
	/*F(rss_new.innerSize()) = dof_cur;*/
	/*F(rss_new.innerSize() + 1) = dof_new;*/

    /*return F;*/
}


//static inline
//void chi2(const model& model_current, const model& model_new, int col_num, MatrixXd* ret)
//{
//    /*auto res_cur = model_current.residuals().array();*/
//    /*auto res_new = model_new.residuals().array();*/
//    /*VectorXd rss_cur = (res_cur * res_cur).colwise().sum();*/
//    /*VectorXd rss_new = (res_new * res_new).colwise().sum();*/
//	VectorXd rss_cur = model_current.rss();
//	VectorXd rss_new = model_new.rss();
//    int dof_cur = model_current.rank();
//    int dof_new = model_new.rank();
//    double Chi2 = 0;
//    for (int i = 0; i < rss_cur.size(); ++i) {
//        if (around_zero(rss_cur(i))) {
//            break;
//        }
//        /* FIXME: rnew is not used */
//        double rnew = rss_new(i);
//        set_if_much_smaller_than(rnew, rss_cur(i));
//        Chi2 += log(rss_cur(i) / rss_new(i));
//    }
//    int df = model_current.Y().outerSize() * (dof_new - dof_cur);
//    if (Chi2 <= 0 || df <= 0) {
//        (*ret)(col_num, 0) = 0;
//        /*return 0;*/
//    } else {
//        (*ret)(col_num, 0) = log_gamma_q(df * .5, Chi2 * .5);
//        /*return -log10(boost::math::log_gamma_q(df * .5, Chi2 * .5));*/
//    }
//}

static inline
VectorXd chi2(const model& model_current, const model& model_new, size_t block_size, bool LOD=false)
{
    /*auto res_cur = model_current.residuals().array();*/
    /*auto res_new = model_new.residuals().array();*/
    /*VectorXd rss_cur = (res_cur * res_cur).colwise().sum();*/
    /*VectorXd rss_new = (res_new * res_new).colwise().sum();*/
	VectorXd rss_cur = model_current.rss();
	VectorXd rss_new = model_new.rss();
    int dof_cur = model_current.rank();
    int dof_new = model_new.rank();
    double Chi2;
    MatrixXd Y = model_current.Y();
    assert(!(Y.outerSize() % block_size) && "STOOPID. BLOCK SIZE MUST BE A DIVISOR OF THE NUMBER OF COLUMNS IN Y.");
    VectorXd ret(Y.outerSize() / block_size);
    for (int c = 0, ofs = 0; ofs < Y.outerSize(); ofs += block_size, ++c) {
        Chi2 = 0;
        for (size_t i = ofs; i < (ofs + block_size); ++i) {
            double rcur = rss_cur(i);
            if (around_zero(rcur)) {
                break;
            }
            double rnew = rss_new(i);
            set_if_much_smaller_than(rnew, rcur);
            Chi2 += log(rcur / rnew);
        }
        int df = block_size * (dof_new - dof_cur);
//        MSG_DEBUG("chi2=" << Chi2);
        if (Chi2 <= 0 || df <= 0) {
            ret(c) = 0;
        } else if (LOD) {
            ret(c) = .5 / log(10) * model_new.n_obs() * Chi2;
        } else {
            ret(c) = log_gamma_q(df * .5, model_new.n_obs() * Chi2 * .5);
//            ret(c) = model_new.n_obs() * Chi2;
        }
    }
    return ret;
}

static inline
void r2(const model& mini_model, const model& final_model, int col_num, MatrixXd* ret_mat)
{
    /*auto res_mini = mini_model.residuals().array();*/
    /*auto res_final = final_model.residuals().array();*/
    /*VectorXd tss = (res_mini * res_mini).colwise().sum();*/
    /*VectorXd rss = (res_final * res_final).colwise().sum();*/
	VectorXd tss = mini_model.rss();
	VectorXd rss = final_model.rss();
    auto ret = ret_mat->col(col_num);
    for (int i = 0; i < ret.innerSize(); ++i) {
        if (around_zero(tss(i))) {
            ret(i) = 0;
        } else {
            ret(i) = (tss(i) - rss(i)) / tss(i);
        }
    }
}

static inline
VectorXd r2(const model& mini_model, const model& final_model)
{
    MatrixXd ret(mini_model.rss().innerSize(), 1);
    r2(mini_model, final_model, 0, &ret);
    return ret.col(0);
}


static inline
void
___mahalanobis(const model& MQ, const model& Mx, int col_num, MatrixXd* ret_mat)
{
/*#define DUMP(_x_) MSG_DEBUG(#_x_ << std::endl << _x_)*/
#define DUMP(_x_) #_x_ << std::endl << _x_
    const auto& CQ = MQ.coefficients();
    const auto& Cx = Mx.coefficients();
    /*DUMP(CQ);*/
    /*DUMP(Cx);*/
    const auto& Cdiff = CQ - Cx;
    const auto& RSSQ = MQ.rss();
    const auto& RSSx = Mx.rss();
    const auto XtXQ_inv = MQ.XtX_pseudo_inverse();
    const auto XtXx_inv = Mx.XtX_pseudo_inverse();
    double dofQ = .5 / (MQ.Y().innerSize() - MQ.rank());
    double dofx = .5 / (Mx.Y().innerSize() - Mx.rank());
    MatrixXd VarCovar = (dofQ * RSSQ(0, 0) * XtXQ_inv)
                      + (dofx * RSSx(0, 0) * XtXx_inv)
                      ;
#if 0
    MSG_DEBUG("========== MAHALANOBIS ==========================================================" << std::endl
    << DUMP(RSSQ) << std::endl
    << DUMP(RSSx) << std::endl
    /*<< "XQ" << std::endl << MQ.X().transpose() << std::endl*/
    << MATRIX_SIZE(MQ.X()) << std::endl
    << MATRIX_SIZE(XtXQ_inv) << std::endl
    /*<< "Xx" << std::endl << Mx.X().transpose() << std::endl*/
    << MATRIX_SIZE(Mx.X()) << std::endl
    << MATRIX_SIZE(XtXx_inv) << std::endl
    << DUMP(dofQ) << std::endl
    << DUMP(dofx) << std::endl
    << DUMP(VarCovar) << std::endl
    << "=================================================================================");
#endif
    (*ret_mat)(col_num, 0) = (Cdiff.transpose() * matrix_inverse(VarCovar) * Cdiff)(0, 0);
#undef DUMP
}


static inline
void
mahalanobis(const model& MQ, const model& Mx, int col_num, MatrixXd* ret_mat)
{
/*#define DUMP(_x_) MSG_DEBUG(#_x_ << std::endl << _x_)*/
#define DUMP(_x_) #_x_ << std::endl << _x_
    auto CQ = MQ.coefficients();
    auto Cx = Mx.coefficients();
    CQ(0, 0) = 0;
    Cx(0, 0) = 0;
    /*DUMP(CQ);*/
    /*DUMP(Cx);*/
    /*const auto& Cdiff = CQ - Cx;*/
    const auto& RSSQ = MQ.rss();
    const auto& RSSx = Mx.rss();
    const auto XtXQ_inv = MQ.XtX_pseudo_inverse();
    const auto XtXx_inv = Mx.XtX_pseudo_inverse();
    double dofQ = .5 / (MQ.Y().innerSize() - MQ.rank());
    double dofx = .5 / (Mx.Y().innerSize() - Mx.rank());
    MatrixXd VarCovar = (dofQ * RSSQ(0, 0) * XtXQ_inv)
                      + (dofx * RSSx(0, 0) * XtXx_inv)
                      ;
#if 0
    MSG_DEBUG("========== MAHALANOBIS ==========================================================" << std::endl
    << DUMP(RSSQ) << std::endl
    << DUMP(RSSx) << std::endl
    /*<< "XQ" << std::endl << MQ.X().transpose() << std::endl*/
    << MATRIX_SIZE(MQ.X()) << std::endl
    << MATRIX_SIZE(XtXQ_inv) << std::endl
    /*<< "Xx" << std::endl << Mx.X().transpose() << std::endl*/
    << MATRIX_SIZE(Mx.X()) << std::endl
    << MATRIX_SIZE(XtXx_inv) << std::endl
    << DUMP(dofQ) << std::endl
    << DUMP(dofx) << std::endl
    << DUMP(VarCovar) << std::endl
    << "=================================================================================");
#endif
    double dot = (CQ.transpose() * matrix_inverse(VarCovar) * Cx)(0, 0);
    (*ret_mat)(col_num, 0) = dot
                           / sqrt((CQ.transpose() * matrix_inverse(VarCovar) * CQ)(0, 0))
                           / sqrt((Cx.transpose() * matrix_inverse(VarCovar) * Cx)(0, 0))
                           ;
#undef DUMP
}

#endif
