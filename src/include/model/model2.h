/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_MODEL_MODEL_H_
#define _SPEL_MODEL_MODEL_H_

#include <stdexcept>

/*#include <Eigen/SVD>*/
/*#include <Eigen/QR>*/
#include "eigen.h"

#include <cmath>
#include <boost/math/special_functions/beta.hpp>
#include <boost/math/special_functions/gamma.hpp>

#include "block.h"
/*#include "labelled_matrix.h"*/
/*#include "settings.h"*/

/*#define COMPONENT_EPSILON (1.e-10)*/
#define COMPONENT_EPSILON (active_settings->tolerance)


namespace std {
template <typename T>
struct hash<std::vector<T>> {
	size_t operator () (const std::vector<T>& v)
	{
		hash<T> h;
		size_t accum = 0;
		for (auto& x: v) {
			accum ^= h(x);
		}
		return accum;
	}
};

template <typename _Scalar, int A, int B, int C, int D, int E>
struct hash<Eigen::Matrix<_Scalar, A, B, C, D, E>> {
    struct red_mat {
        size_t accum;
        void init(_Scalar s, int i, int j)
        {
            accum = hash<_Scalar>()(s);
            (void)i; (void)j;
        }
        void operator () (_Scalar s, int i, int j)
        {
            accum = impl::ROTATE<7>(accum) ^ hash<_Scalar>()(s);
            (void)i; (void)j;
        }
    };
    size_t operator () (const value<Eigen::Matrix<_Scalar, A, B, C, D, E>>& m) const
    {
        red_mat rm;
        m->visit(rm);
        return rm.accum;
    }
    size_t operator () (const Eigen::Matrix<_Scalar, A, B, C, D, E>& m) const
    {
        red_mat rm;
        m.visit(rm);
        return rm.accum;
    }
};

template <typename M, typename R, typename C>
struct hash<labelled_matrix<M, R, C>> {
    size_t operator () (const labelled_matrix<M, R, C>& m) const
	{
		hash<M> hm;
		hash<R> hr;
		hash<C> hc;
		size_t accum = hm(m.data);
		for (auto& r: m.row_labels) { accum ^= hr(r); }
		for (auto& c: m.column_labels) { accum ^= hc(c); }
		return accum;
	}
	size_t operator () (const value<labelled_matrix<M, R, C>>& m) const
	{
		return operator () (*m);
	}
};
}

static inline
bool
around_zero(double o)
{
    return o < COMPONENT_EPSILON && o > -COMPONENT_EPSILON;
}

static inline
bool
much_smaller_than(double a, double b)
{
    return a < (COMPONENT_EPSILON * b);
}

static inline
void
set_if_much_smaller_than(double& a, double b)
{
    double tmp = COMPONENT_EPSILON * b;
    if (a < tmp) {
        a = tmp;
    }
}


using namespace Eigen;

static inline
MatrixXd concat_right(const std::vector<MatrixXd>& mat_vec)
{
    size_t full_size = 0;
    MatrixXd ret;
    for (auto& m: mat_vec) {
        full_size += m.outerSize();
        /*MSG_DEBUG("preparing concat_right with matrix(" << m->innerSize() << ',' << m->outerSize() << ')');*/
    }
    ret.resize(mat_vec.front().innerSize(), full_size);
    full_size = 0;
    for (auto& m: mat_vec) {
        /*MSG_DEBUG("concat_right in M(" << ret.innerSize() << ',' << ret.outerSize() << ") at col " << full_size << "matrix(" << m->innerSize() << ',' << m->outerSize() << ')');*/
        /*ret.block(0, full_size, ret.innerSize(), m->outerSize()) = *m;*/
        ret.middleCols(full_size, m.outerSize()) = m;
        full_size += m.outerSize();
    }
    return ret;
}

static inline
MatrixXd concat_right(const collection<model_block_type>& mat_vec)
{
    size_t full_size = 0;
    MatrixXd ret;
    for (auto m: mat_vec) {
        full_size += m->outerSize();
        /*MSG_DEBUG("preparing concat_right with matrix(" << m->innerSize() << ',' << m->outerSize() << ')');*/
    }
    ret.resize(mat_vec.front()->innerSize(), full_size);
    full_size = 0;
    for (auto m: mat_vec) {
        /*MSG_DEBUG("concat_right in M(" << ret.innerSize() << ',' << ret.outerSize() << ") at col " << full_size << "matrix(" << m->innerSize() << ',' << m->outerSize() << ')');*/
        /*ret.block(0, full_size, ret.innerSize(), m->outerSize()) = *m;*/
        ret.middleCols(full_size, m->outerSize()) = m->data;
        full_size += m->outerSize();
    }
    return ret;
}

static inline
MatrixXd concat_down(const std::vector<MatrixXd>& mat_vec)
{
    size_t full_size = 0;
    MatrixXd ret;
    for (auto& m: mat_vec) {
        full_size += m.innerSize();
    }
    ret.resize(full_size, mat_vec.front().outerSize());
    full_size = 0;
    for (auto& m: mat_vec) {
        ret.middleRows(full_size, m.innerSize()) = m;
        full_size += m.innerSize();
    }
    return ret;
}

static inline
MatrixXd concat_down(const std::vector<const MatrixXd*>& mat_vec)
{
    size_t full_size = 0;
    MatrixXd ret;
    for (auto m: mat_vec) {
        full_size += m->innerSize();
    }
    ret.resize(full_size, mat_vec.front()->outerSize());
    full_size = 0;
    for (auto m: mat_vec) {
        /*ret.block(full_size, 0, m->innerSize(), ret.outerSize()) = *m;*/
        ret.middleRows(full_size, m->innerSize()) = *m;
        full_size += m->innerSize();
    }
    return ret;
}


static inline
std::pair<int, MatrixXd>
rank_and_components(const MatrixXd& M)
{
    JacobiSVD<MatrixXd> svd(M, ComputeThinU);

    std::cout << "Singular values " << svd.singularValues().transpose() << std::endl;
    int nzsv = svd.nonzeroSingularValues();

    return {nzsv, svd.matrixU().leftCols(nzsv)};
}


static inline
MatrixXd components(const MatrixXd& M, const MatrixXd& P)
{
    MatrixXd pnorm(P.innerSize(), P.outerSize());
    for (int i = 0; i < P.outerSize(); ++i) {
        pnorm.col(i) = P.col(i).normalized();
    }
    MatrixXd orth = M - pnorm * pnorm.transpose() * M; /* feu ! */
    return rank_and_components(orth).second;
}


enum class SolverType { QR, SVD };


struct model {
    model()
        : m_Y(), m_blocks(), m_X(), m_rank(), m_rss(), m_coefficients(), m_solver_type(), m_computed(false)
    {}

    model(const value<MatrixXd>& y, SolverType st = SolverType::QR)
        : m_Y(y)
        , m_blocks(), m_X()
		, m_rank(), m_rss(), m_coefficients(), m_residuals()
		, m_solver_type(st)
        , m_computed(false)
    /*{ MSG_DEBUG("new model " << __LINE__ << " with Y(" << y.innerSize() << ',' << y.outerSize() << ')'); }*/
    {}

    model(const model& mo)
        : m_Y(mo.m_Y)
        , m_blocks(mo.m_blocks), m_X(mo.m_X)
		, m_rank(mo.m_rank), m_rss(mo.m_rss), m_coefficients(mo.m_coefficients)
        , m_residuals(mo.m_residuals)
		, m_solver_type(mo.m_solver_type)
        , m_computed(mo.m_computed)
    /*{ MSG_DEBUG("new model " << __LINE__ << " with Y(" << m_Y->innerSize() << ',' << m_Y->outerSize() << ')'); }*/
    {}

    model& operator = (const model& mo)
# if 0
        = delete;
#else
    {
        m_Y = mo.m_Y;
        m_blocks = mo.m_blocks;
		m_computed = mo.m_computed;
		m_X = mo.m_X;
		m_rss = mo.m_rss;
		m_coefficients = mo.m_coefficients;
		m_residuals = mo.m_residuals;
        m_rank = mo.m_rank;
        return *this;
    }

    model& operator = (model&& mo)
    {
        m_Y = mo.m_Y;
        m_blocks.clear();
        m_blocks.swap(mo.m_blocks);
		m_computed = mo.m_computed;
		m_X = mo.m_X;
		m_rss = mo.m_rss;
		m_coefficients = mo.m_coefficients;
		m_residuals = mo.m_residuals;
        m_rank = mo.m_rank;
        return *this;
    }
#endif

    bool operator == (const model& m) const
    {
        return m_Y == m.m_Y && m_blocks == m.m_blocks && m_rss == m.m_rss;
    }

    void add_bloc(const value<model_block_type>& x)
    {
		m_computed = false;
        m_blocks.push_back(x);
    }

    void remove_bloc(const value<model_block_type>& x)
    {
		m_computed = false;
        m_blocks.erase(std::find(m_blocks.begin(), m_blocks.end(), x));
    }

    void use_SVD()
    {
        if (m_solver_type != SolverType::SVD) {
    		m_computed = false;
            m_solver_type = SolverType::SVD;
        }
    }

    void use_QR()
    {
        if (m_solver_type != SolverType::QR) {
    		m_computed = false;
            m_solver_type = SolverType::QR;
        }
    }

    SolverType solver_type() const
    {
        return m_solver_type;
    }

	void compute()
	{
		m_computed = true;
		m_X = new immediate_value<MatrixXd>(MatrixXd());
		m_coefficients = new immediate_value<MatrixXd>(MatrixXd());
		m_residuals = new immediate_value<MatrixXd>(MatrixXd());
		m_rss = new immediate_value<VectorXd>(VectorXd());
        assemble_blocks(*m_Y, *m_X, m_blocks);
		if (m_solver_type == SolverType::QR) {
			int m_columns = m_X->outerSize();
			/*MSG_DEBUG("X(" << X().innerSize() << ',' << X().outerSize() << ')');*/
			/*MSG_DEBUG("Y(" << Y().innerSize() << ',' << Y().outerSize() << ')');*/
			FullPivHouseholderQR<MatrixXd> solver(*m_X);
			solver.setThreshold(COMPONENT_EPSILON);
			m_rank = solver.rank();
			m_coefficients->resize(m_columns, Y().outerSize());
			for (int i = 0; i < Y().outerSize(); ++i) {
				m_coefficients->col(i) = solver.solve(Y().col(i));
			}
		} else {
			JacobiSVD<MatrixXd> solver(*m_X);
            m_rank = 0;
            int nzsv = solver.nonzeroSingularValues();

            for (int i = 0; i < nzsv; ++i) {
                m_rank += !around_zero(solver.singularValues()(i));
            }

			*m_coefficients = solver.solve(Y());
		}
		*m_residuals = Y() - X() * (*m_coefficients);
		*m_rss = m_residuals->array().square().colwise().sum();
	}

    const MatrixXd& X() const
    {
		if (!m_computed) { throw std::runtime_error("Model not computed"); }
        return *m_X;
    }

	const VectorXd& rss() const
	{
		if (!m_computed) { throw std::runtime_error("Model not computed"); }
		return *m_rss;
	}

    const MatrixXd& coefficients() const
    {
		if (!m_computed) { throw std::runtime_error("Model not computed"); }
        return *m_coefficients;
    }

    const MatrixXd& residuals() const
    {
		if (!m_computed) { throw std::runtime_error("Model not computed"); }
        return *m_residuals;
    }

    int rank() const
    {
		if (!m_computed) { throw std::runtime_error("Model not computed"); }
        return m_rank;
    }

    const MatrixXd& Y() const
    {
		if (!m_Y) {
			MSG_ERROR("NULL Y!", "Fix the code");
			throw 0;
		}
        return *m_Y;
    }

    model extend(const value<model_block_type>& m) const
    {
        model ret(*this);
        /*MSG_DEBUG("extend model " << __LINE__ << " with Y(" << m_Y->innerSize() << ',' << m_Y->outerSize() << ')');*/
        /*model ret(m_Y, m_solver_type);*/
        /*ret.add_bloc(X());*/
        ret.add_bloc(m);
        return ret;
    }

    MatrixXd XtX() const
    {
        return X().transpose() * X();
    }

    MatrixXd XtX_pseudo_inverse() const
    {
        JacobiSVD<MatrixXd> inverter(XtX(), ComputeFullV);
        auto& V = inverter.matrixV();
        VectorXd inv_sv(inverter.singularValues());
        for (int i = 0; i < inv_sv.innerSize(); ++i) {
            if (!around_zero(inv_sv(i))) {
                inv_sv(i) = 1. / inv_sv(i);
            } else {
                inv_sv(i) = 0.;
            }
        }
        return V * inv_sv.asDiagonal() * V.transpose();
    }

/*private:*/
    value<MatrixXd> m_Y;
    collection<model_block_type> m_blocks;
    value<MatrixXd> m_X;
    int m_rank;
    value<VectorXd> m_rss;
    value<MatrixXd> m_coefficients;
    value<MatrixXd> m_residuals;
    SolverType m_solver_type;
    /*decomposition_base* m_solver;*/
	bool m_computed;

public:
    friend
    inline
    md5_digest& operator << (md5_digest& md5, const model& m)
    {
        md5 << m.Y() << m.m_blocks;
        return md5;
    }

    friend
        inline
        std::ostream& operator << (std::ostream& os, const model& m)
        {
            os << "<model Y(" << m.m_Y->innerSize() << ',' << m.m_Y->outerSize() << "), " << m.m_blocks.size() << " blocs>";
            return os;
        }

    size_t hash() const
    {
        std::hash<model_block_type> hm;
        size_t accum = 0;
        for (const auto& b: m_blocks) {
            accum ^= hm(b);
        }
        return accum;
    }
};


namespace std {
template <>
struct hash<model> {
    size_t operator () (const model& m) { return m.hash(); }
};

}

#endif

