/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_MODEL_BLOCK_H_
#define _SPEL_MODEL_BLOCK_H_

#include "labelled_matrix.h"
#include "settings.h"
#include "cache2.h"

template <typename L> struct sub_block_builder;

template <typename L>
void
init_connected_block_builder(
		sub_block_builder<L>& ret,
		std::function<int (const population&)> get_pop_size,
		std::function<const std::vector<L>& (const population&)> get_labels);

template <typename L>
void
init_disconnected_block_builder(
		sub_block_builder<L>& ret,
		std::function<int (const population&)> get_pop_size,
		std::function<const std::vector<L>& (const population&)> get_labels);


typedef std::pair<const chromosome*, double> locus_type;

struct model_block_key {
    std::vector<locus_type> loci;
    bool qtl_block;
    bool dominance;
    bool epistasis;
};


struct model_block_type {
    std::vector<std::vector<char>> labels;
    sub_block_builder<std::vector<char>> builder;
    std::vector<labelled_matrix<MatrixXd, int, std::vector<char>>> pop_blocks;
    MatrixXd supp_block;
    bool supp_block_computed;

    model_block_type();
    virtual ~model_block_type() {}

    virtual void init_builder() {}
    virtual void compute_supp_block_impl() {}

    void compute_supp_block()
    {
        if (!supp_block_computed) {
            supp_block_computed = true;
            compute_supp_block_impl();
        }
    }

    model_block_type& operator += (locus_type l);
    model_block_type& operator -= (locus_type l);

    model_block_type operator + (locus_type l);
    model_block_type operator - (locus_type l);

    void reset_supp_block() { supp_block = MatrixXd(); supp_block_computed = false; }

    int innerSize() const { return builder.n_rows; }
    int outerSize() const { return builder.n_columns; }

protected:
    void compute_pop_blocks(const model_block_type& orig,


};



struct model_block_proba : model_block_type {
    virtual void init_builder()
    {
        std::function<int (const population&)>
            get_pop_size
            = [] (const population& pop)
              {
                  return pop.observed_traits.front().values.size();
              };
        std::function<const std::vector<L>& (const population&)>
            get_labels
            = [] (const population& pop)
              {
                  return pop_blocks[(&pop) - (&active_settings->populations[0])].column_labels;
              };
        if (active_settings->connected) {
            init_connected_block_builder(builder, get_pop_size, get_labels);
        } else {
            init_disconnected_block_builder(builder, get_pop_size, get_labels);
        }
    }

    virtual void compute_supp_block_impl()
    {
        /* per pop: two columns are in the same contrast group IIF they are observed together,
         * that is at least one observation spreads probabilities amongst them
         * this is equivalent to t(Ci) * Cj != 0
         */
        std::vector<MatrixXb> per_pop;
        for (auto& pb: pop_blocks) {
            per_pop.emplace_back(pb.outerSize(), pb.outerSize());
            auto& cg = per_pop.back();
            for (int i = 0; i < pb.outerSize(); ++i) {
                cg(i, i) = true;
                for (int j = i + 1; j < pb.outerSize(); ++j) {
                    cg(i, j) = cg(j, i) = !(pb.data.col(i).transpose() * pb.data.col(j)).isZero(COMPONENT_EPSILON);
                }
            }
        }

        /* global adjacency matrix */
        MatrixXb global = MatrixXb::Zero(builder.n_columns, builder.n_columns);
        for (size_t p = 0; p < per_pop.size(); ++p) {
            auto& cg = per_pop.back();
            sub_block_builder<std::vector<char>>::element& el = builder.elements[p];
            for (int i = 0; i < cg.innerSize(); ++i) {
                for (int j = 0; j < cg.outerSize(); ++j) {
                    global(el.columns[i], el.columns[j]) |= cg(i, j);
                }
            }
        }

        /* transitive closure */
        int sum = 0;
        int tmp = global.cast<int>().sum();
        do {
            sum = tmp;
            global *= global.transpose();
            tmp = global.cast<int>().sum();
        } while(tmp != sum);

        /* extract cliques */
        VectorXb visited = VectorXb::Zero(global.innerSize());
        std::vector<std::vector<int>> cliques;
        for (int i = 0; i < visited.innerSize(); ++i) {
            if (visited(i)) {
                continue;
            }
            visited(i) = 1;
            cliques.emplace_back();
            cliques.back().push_back(i);
            for (j = i + 1; j < visited.innerSize(); ++j) {
                if (visited(j)) {
                    continue;
                }
                if (global(i, j)) {
                    visited(j) = 1;
                    cliques.back().push_back(j);
                }
            }
        }

        /* write constraints matrix */
        supp_block = MatrixXd::Zero(cliques.size(), global.innerSize());
        for (size_t i = 0; i < cliques.size(); ++i) {
            for (int j: cliques[i]) {
                supp_block(i, j) = 1;
            }
        }
    }
};







template <typename L>
struct sub_block_builder {
	struct element {
		int first_row;
		std::vector<int> columns;
	};
	std::vector<element> elements;
	typedef typename std::vector<element>::const_iterator element_iterator_type;
	std::vector<L> labels;
	int n_rows;
	int n_columns;

	block_builder&
		operator = (block_builder&& bb)
		{
			elements.swap(bb.elements);
			labels.swap(bb.labels);
			n_rows = bb.n_rows;
			n_columns = bb.n_columns;
			return *this;
		}

	struct iterator {
		element_iterator_type ei;
		MatrixXd* output;
        int base_row;
        int base_column;

		iterator&
			operator += (const MatrixXd& block)
			{
				auto slice = output->middleRows(base_row + ei->first_row, block.innerSize());
				/*MSG_DEBUG("slice(" << slice.innerSize() << ',' << slice.outerSize() << ')');*/
				/*MSG_DEBUG("block(" << block.innerSize() << ',' << block.outerSize() << ')');*/
				for (size_t i = 0; i < block.outerSize(); ++i) {
					slice.col(base_column + ei->columns[i]) = block.col(i);
				}
				++ei;
				return *this;
			}
	};

	iterator begin(MatrixXd& output, int base_row, int base_column) const
	{
		/*output.resize(n_rows, n_columns);*/
		/*output = MatrixXd::Zero(n_rows, n_columns);*/
		return {elements.begin(), &output, base_row, base_column};
	}
};



template <typename L>
void
init_connected_block_builder(
		sub_block_builder<L>& ret,
		std::function<int (const population&)> get_pop_size,
		std::function<const std::vector<L>& (const population&)> get_labels)
{
    std::map<L, int> index;

	for (auto& kv: active_settings->populations) {
		for (const L& l: get_labels(kv.second)) {
			int sz = index.size();
			index.insert({l, sz});
		}
	}

	ret.n_rows = 0;
	ret.n_columns = index.size();
	ret.labels.reserve(ret.n_columns);
	ret.elements.reserve(active_settings->populations.size());
	for (auto& kv: index) { ret.labels.push_back(kv.first); }

	for (auto& kv: active_settings->populations) {
		ret.elements.emplace_back();
		auto& e = ret.elements.back();
		const auto& lv = get_labels(kv.second);
		e.first_row = ret.n_rows;
		e.columns.reserve(lv.size());
		for (const L& l: lv) {
			e.columns.push_back(index[l]);
		}
		ret.n_rows += get_pop_size(kv.second);
	}
}



template <typename L>
void
init_disconnected_block_builder(
		sub_block_builder<L>& ret,
		std::function<int (const population&)> get_pop_size,
		std::function<const std::vector<L>& (const population&)> get_labels)
{
    ret.n_rows = 0;
	int col_id = 0;

	ret.n_columns = 0;
	for (auto& kv: active_settings->populations) {
		ret.n_columns += get_labels(kv.second).size();
	}
	ret.labels.reserve(ret.n_columns);
	ret.elements.reserve(active_settings->populations.size());

	for (auto& kv: active_settings->populations) {
		ret.elements.emplace_back();
		auto& e = ret.elements.back();
		const auto& lv = get_labels(kv.second);
		e.first_row = ret.n_rows;
		e.columns.reserve(lv.size());
		for (const L& l: lv) {
			e.columns.push_back(col_id++);
			ret.labels.push_back(l);
		}
		ret.n_rows += get_pop_size(kv.second);
	}
}






static inline
void
assemble_blocks(MatrixXd& Y, MatrixXd& X,
                const std::vector<value<model_block_type>>& blocks,
                bool with_supp_blocks=false)
{
    int n_rows = 0;
    int n_columns = 0;
    /* compute dimensions of X */
    for (const auto& vb: blocks) {
        int tmp = vb->innerSize();
        n_rows = n_rows < tmp ? tmp : n_rows;
        n_columns += vb->outerSize();
    }
    if (with_supp_blocks) {
        for (const auto& vb: blocks) {
            vb->compute_supp_block();
            n_rows += vb->supp_block.innerSize();
        }
    }
    /* adjust height of Y if required */
    if (n_rows > Y.innerSize()) {
        MatrixXd tmp = Y;
        Y.resize(n_rows, Y.outerSize());
        Y.upperRows(tmp.innerSize()) = tmp;
        int remaining = n_rows - tmp.innerSize();
        Y.bottomRows(remaining) = MatrixXd::Zero(remaining, Y.outerSize());
    }
    /* build X */
    X = MatrixXd::Zero(n_rows, n_columns);
    n_rows = 0;
    n_columns = 0;
    for (const auto& vb: blocks) {
        const auto& builder = vb->builder.begin(X, 0, n_columns);
        n_columns += vb->outerSize();
    }
    if (with_supp_blocks) {
        n_columns = 0;
        for (const auto& vb: blocks) {
            const auto& sb = vb->supp_block;
            X.block(n_rows, n_columns, sb.innerSize(), sb.outerSize()) = sb;
            n_rows += sb.innerSize();
            n_columns += sb.outerSize();
        }
    }
}



#endif

