/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_MODEL_PRINT_H_
#define _SPELL_MODEL_PRINT_H_

#include <iostream>
#include <iomanip>
#include "eigen.h"
#include "error.h"
#include "labelled_matrix.h"

inline
std::ostream& operator << (std::ostream& os, const std::vector<char>& v)
{
    for (char c: v) { os << c; }
    return os;
}


namespace model_print {
    extern int precision;
    extern std::string field_sep;
    extern std::string section_hsep;
    extern char section_vsep;
    extern std::string section_csep;

inline
    std::ostream&
    fill(std::ostream& os, char c, size_t width)
    {
        char f = os.fill();
        os.fill(c);
        os << std::setw(width) << "";
        os.fill(f);
        return os;
    }


template <typename T>
    std::string to_string(const T& x) { std::stringstream s; s << x; return s.str(); }
inline
    std::string to_string(const std::vector<char>& x) { return std::string(x.begin(), x.end()); }
inline
    std::string to_string(const std::string& s) { return s; }
inline
    std::string to_string(double x) { std::stringstream s; s << std::setprecision(precision) << x; return s.str(); }


template <typename _Printable>
    std::ostream&
    center(std::ostream& os, const _Printable& s, size_t width)
    {
        std::string str = to_string(s);
        size_t pad = width - str.size();
        size_t w1 = pad >> 1;
        size_t w2 = pad - w1;
        /*
        char f = os.fill();
        return os << std::setfill('_') << std::setw(w1) << "" << s << std::setw(w2) << "" << std::setfill(f);
        /*/
        return os << std::setw(w1) << "" << str << std::setw(w2) << "";
        //*/
    }


template <typename _Printable>
    std::ostream&
    right(std::ostream& os, const _Printable& s, size_t width)
    {
        /*
        char f = os.fill();
        return os << std::setfill('_') << std::setw(width) << std::right << s << std::setfill(f);
        /*/
        return os << std::setw(width) << std::right << to_string(s);
        //*/
    }


template <typename _Printable>
    std::ostream&
    left(std::ostream& os, const _Printable& s, size_t width)
    {
        return os << std::setw(width) << std::left << to_string(s);
    }


enum Align { Start, Center, End };
enum Orientation { Horizontal, Vertical };

template <typename T>
    size_t width(T x) { return to_string(x).size(); }


template <typename FieldLabel>
struct with_field_labels {
    static const bool has_field_labels = true;
    std::vector<FieldLabel> m_field_labels;

    with_field_labels(const std::vector<FieldLabel>& fl)
        : m_field_labels(fl)
    {}

    size_t n_fields() const { return m_field_labels.size(); }
    std::vector<size_t> field_label_widths() const
    {
        std::vector<size_t> ret;
        ret.reserve(m_field_labels.size());
        for (const FieldLabel& f: m_field_labels) {
            ret.push_back(to_string(f).size());
        }
        return ret;
    }

    const std::vector<FieldLabel>& field_labels() const { return m_field_labels; }
};

template <>
struct with_field_labels<void> {
    static const bool has_field_labels = false;
    size_t m_n_fields;

    with_field_labels(size_t n)
        : m_n_fields(n)
    {}

    size_t n_fields() const { return m_n_fields; }
    std::vector<size_t> field_label_widths() const
    {
        std::vector<size_t> ret;
        ret.resize(m_n_fields, 0);
        return ret;
    }

    std::vector<int> field_labels() const { return {}; }
};

template <typename SectionLabel>
struct with_section_label {
    static const bool has_section_label = true;
    SectionLabel m_label;

    with_section_label(const SectionLabel& sl) : m_label(sl) {}
    size_t section_label_width() const
    {
        size_t ret = to_string(m_label).size();
        /*MSG_DEBUG("label_width(" << to_string(m_label) << ") = " << ret);*/
        return ret;
    }
};

template <>
struct with_section_label<void> {
    static const bool has_section_label = false;

    with_section_label() {}
    size_t section_label_width() const { return 0; }
};


template <typename SectionLabel, typename FieldLabel>
struct section
        : public with_section_label<SectionLabel>,
          public with_field_labels<FieldLabel> {
    section(const SectionLabel& sl, const std::vector<FieldLabel>& fl)
        : with_section_label<SectionLabel>(sl), with_field_labels<FieldLabel>(fl)
    {}

    using with_section_label<SectionLabel>::m_label;

    const SectionLabel& label() const { return m_label; }
};

template <typename FieldLabel>
struct section<void, FieldLabel>
        : public with_section_label<void>,
          public with_field_labels<FieldLabel> {
    section(const std::vector<FieldLabel>& fl)
        : with_section_label<void>(), with_field_labels<FieldLabel>(fl)
    {}

    const std::string& label() const { static std::string _; return _; }
};

template <typename SectionLabel>
struct section<SectionLabel, void>
        : public with_section_label<SectionLabel>,
          public with_field_labels<void> {
    section(const SectionLabel& sl, size_t nf)
        : with_section_label<SectionLabel>(sl), with_field_labels<void>(nf)
    {}

    using with_section_label<SectionLabel>::m_label;

    const SectionLabel& label() const { return m_label; }
};


template <>
struct section<void, void>
        : public with_section_label<void>,
          public with_field_labels<void> {
    section(size_t nf)
        : with_section_label<void>(), with_field_labels<void>(nf)
    {}

    const std::string& label() const { static std::string _; return _; }
};


struct section_size {
    size_t label_length;
    size_t size;

    section_size() : label_length(0), size(0) {}
    size_t max_size() const { return size > label_length ? size : label_length; }
    size_t padding_before() const { return (max_size() - size) >> 1; }
    size_t padding_after() const { return max_size() - size - padding_before(); }
};

template <typename RS, typename RF, typename CS, typename CF, typename MatrixType=Eigen::MatrixXd>
struct matrix_with_sections {
    typedef section<RS, RF> row_section_type;
    typedef section<CS, CF> column_section_type;
    static const int has_column_field_labels = column_section_type::has_field_labels;
    static const int has_column_section_label = column_section_type::has_section_label;
    static const int has_row_field_labels = row_section_type::has_field_labels;
    static const int has_row_section_label = row_section_type::has_section_label;
    typedef matrix_with_sections<RS, RF, CS, CF, MatrixType> this_type;
    std::vector<row_section_type> m_row_sections;
    std::vector<column_section_type> m_column_sections;
    /*const Eigen::MatrixXd& m_matrix;*/
    MatrixType m_matrix;

    matrix_with_sections(const MatrixType& m) : m_row_sections(), m_column_sections(), m_matrix(m) {}
    template <typename S, typename F>
        this_type&
        add_row_section(const S& s, const std::vector<F>& f) { m_row_sections.emplace_back(s, f); return *this; }
    template <typename SF>
        this_type&
        add_row_section(const SF& sf) { m_row_sections.emplace_back(sf); return *this; }
    template <typename SF>
        this_type&
        add_row_section(const SF& sf, size_t n) { m_row_sections.emplace_back(sf, n); return *this; }
    template <typename S, typename F>
        this_type&
        add_column_section(const S& s, const std::vector<F>& f) { m_column_sections.emplace_back(s, f); return *this; }
    template <typename SF>
        this_type&
        add_column_section(const SF& sf) { m_column_sections.emplace_back(sf); return *this; }
    template <typename SF>
        this_type&
        add_column_section(const SF& sf, size_t n) { m_column_sections.emplace_back(sf, n); return *this; }

/*private:*/

    std::vector<size_t> compute_field_widths() const
    {
        std::vector<size_t> ret;
        ret.resize(m_matrix.outerSize() + 1, 0);
        auto ri = ret.begin();
        if (has_row_field_labels) {
            for (const auto& s: m_row_sections) {
                for (const auto&f: s.field_labels()) {
                    size_t sz = to_string(f).size();
                    if (sz > *ri) {
                        *ri = sz;
                    }
                }
            }
        } else {
            *ri = 0;
        }
        ++ri;
        for (const auto& s: m_column_sections) {
            /*MSG_DEBUG("for s: column_sections");*/
            /*MSG_DEBUG("  initial d(begin, r) = " << (ri - ret.begin()));*/
            for (const auto&f: s.field_labels()) {
                *ri++ = to_string(f).size();
                /*MSG_DEBUG("  d(begin, r) = " << (ri - ret.begin()));*/
                /*MSG_QUEUE_FLUSH();*/
            }
        }
        for (int i = 0; i < m_matrix.outerSize(); ++i) {
            for (int j = 0; j < m_matrix.innerSize(); ++j) {
                size_t w = width(m_matrix(j, i));
                if (w > ret[i + 1]) {
                    ret[i + 1] = w;
                }
            }
        }
        return ret;
    }

    std::vector<section_size> compute_section_widths(const std::vector<size_t>& fwidths, size_t sep_size) const
    {
        std::vector<section_size> ret;
        ret.resize(m_column_sections.size());
        std::vector<size_t>::const_iterator wi = fwidths.begin();
        ++wi;
        for (size_t s = 0; s < m_column_sections.size(); ++s) {
            size_t nf = m_column_sections[s].n_fields();
            for (size_t i = 0; i < nf; ++i) {
                ret[s].size += *wi++;
            }
            ret[s].size += sep_size * (nf - 1);
            ret[s].label_length = m_column_sections[s].section_label_width();
        }
        return ret;
    }

    std::vector<section_size> compute_section_heights() const
    {
        std::vector<section_size> ret;
        ret.resize(m_row_sections.size());
        for (size_t s = 0; s < m_row_sections.size(); ++s) {
            ret[s].size = m_row_sections[s].n_fields();
            ret[s].label_length = m_row_sections[s].section_label_width();
        }
        return ret;
    }
};


template <typename RS, typename RF, typename CS, typename CF, typename MT>
struct section_renderer {
    typedef matrix_with_sections<RS, RF, CS, CF, MT> mws_type;
    const matrix_with_sections<RS, RF, CS, CF, MT>& mws;
    std::vector<size_t> fw;
    std::vector<section_size> sw, sh;
    size_t head_space;
    std::stringstream sep;

    section_renderer(const mws_type& _)
        : mws(_)
        , fw(mws.compute_field_widths())
        , sw(mws.compute_section_widths(fw, field_sep.size()))
        , sh(mws.compute_section_heights())
        , head_space(1)
        , sep()
    {
        if (mws.has_row_field_labels) {
            head_space += field_sep.size() + fw[0];
        }

        fill(sep, section_vsep, head_space);
        auto swi = sw.begin();
        auto swj = sw.end();
        while(swi != swj) {
            sep << section_csep;
            fill(sep, section_vsep, swi->max_size());
            ++swi;
        }
        sep << std::endl;
    }

    void insert_pad_lines(std::ostream& os, const mws_type& mws, size_t n_lines, std::string::iterator& li) const
    {
        for (size_t i = 0; i < n_lines; ++i) {
            left(os, *li++, head_space);
            auto swi = sw.begin();
            for (size_t s = 0; s < mws.m_column_sections.size(); ++s) {
                os << section_hsep;
                fill(os, ' ', swi->max_size());
                ++swi;
            }
            os << std::endl;
        }
    }

    void header(std::ostream& os) const
    {
        /* first row */
        fill(os, ' ', head_space);
        auto swi = sw.begin();
        for (const auto& s: mws.m_column_sections) {
            os << section_hsep;
            center(os, s.label(), swi->max_size());
            ++swi;
        }
        os << std::endl;

        /* second row */
        if (mws.has_column_field_labels) {
            auto fwi = fw.begin();
            fill(os, ' ', head_space);
            swi = sw.begin();
            ++fwi;
            for (const auto& s: mws.m_column_sections) {
                os << section_hsep;
                fill(os, ' ', swi->padding_before());
                auto fi = s.field_labels().begin();
                auto fj = s.field_labels().end();
                if (fi != fj) {
                    right(os, *fi++, *fwi++);
                    while (fi != fj) {
                        os << field_sep;
                        right(os, *fi++, *fwi++);
                    }
                }
                fill(os, ' ', swi->padding_after());
                ++swi;
            }
            os << std::endl;
        }
    }

    void output_sections(std::ostream& os) const
    {
        auto shi = sh.begin();

        int row_start;
        int row_end = 0;
        int column;

        for (const auto& vs: mws.m_row_sections) {
            row_start = row_end;
            column = 0;
            int pad_before = shi->padding_before();
            int pad_after = shi->padding_after();
            /*MSG_DEBUG("max_size=" << shi->max_size() << " size=" << shi->size << " pad_before=" << pad_before << " pad_after=" << pad_after);*/
            row_end = row_start + shi->size;

            os << sep.str();

            /* prepare label */
            std::stringstream label;
            center(label, vs.label(), shi->max_size());
            label << std::flush;
            /*MSG_DEBUG("LABEL[" << label.str() << ']');*/
            std::string labelstr = label.str();
            auto li = labelstr.begin();

            /* padding top */
            insert_pad_lines(os, mws, pad_before, li);

            for (int row = row_start, i = 0; row < row_end; ++row, ++i) {
                auto fwi = fw.begin();
                os << (*li++);
                if (mws.has_row_field_labels) {
                    os << field_sep;
                    right(os, vs.field_labels()[i], *fwi);
                }
                ++fwi;

                column = 0;
                auto swi = sw.begin();
                for (const auto& c: mws.m_column_sections) {
                    os << section_hsep;
                    fill(os, ' ', swi->padding_before());
                    right(os, to_string(mws.m_matrix(row, column)), *fwi++);
                    ++column;
                    for (size_t k = 1; k < c.n_fields(); ++k, ++column) {
                        os << field_sep;
                        right(os, to_string(mws.m_matrix(row, column)), *fwi++);
                    }
                    fill(os, ' ', swi->padding_after());
                    ++swi;
                }

                os << std::endl;
            }

            /* padding bottom */
            insert_pad_lines(os, mws, pad_after, li);

            ++shi;
        }
    }

    friend
        std::ostream& operator << (std::ostream& os, const section_renderer& sr)
        {
            sr.header(os);
            sr.output_sections(os);
            return os;
        }
};

} /* namespace model_print */

using model_print::matrix_with_sections;

template <typename RS, typename RF, typename CS, typename CF, typename MT>
std::ostream& operator << (std::ostream& os, const matrix_with_sections<RS, RF, CS, CF, MT>& mws)
{
    model_print::section_renderer<RS, RF, CS, CF, MT> sr(mws);
    return os << sr;
}

#endif

