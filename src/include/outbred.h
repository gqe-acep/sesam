/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_OUTBRED_H_
#define _SPEL_OUTBRED_H_

#include "markov_population.h"
#include <utility>
#include "input/read_mark.h"


struct phase {
    char mother, father;

    friend std::ostream& operator << (std::ostream& os, const phase& p)
    {
        return os << '{' << p.mother << p.father << '}';
    }
};


struct segregation {
    allele_pair mother;
    allele_pair father;
    allele_pair mother_repr;
    allele_pair father_repr;

    segregation()
        : mother({0, 0}), father({0, 0})
        , mother_repr({0, 2}), father_repr({0, 1})
    {}

    segregation(char m1, char m2, char f1, char f2)
        : mother({m1, m2}), father({f1, f2})
        , mother_repr({0, 2}), father_repr({0, 1})
    {}

    friend std::ostream& operator << (std::ostream& os, const segregation& s)
    {
        return os << '<' << s.mother << 'x' << s.father << '>';
    }

    segregation& operator *= (const phase& p)
    {
        if (p.mother == '1') {
            std::swap(mother.first, mother.second);
        }
        if (p.father == '1') {
            std::swap(father.first, father.second);
        }
        return *this;
    }

    segregation operator * (const phase& p)
    {
        segregation ret = *this;
        ret *= p;
        return ret;
    }
};



static inline int _match_obs(char first, char second, const segregation& s)
{
    int ret = 0;
    bool m0, m1, f0, f1;
    bool miss_m = first == '-';
    bool miss_f = second == '-';

    m0 = miss_m || first == ~s.mother.first;
    m1 = miss_m || first == ~s.mother.second;
    f0 = miss_f || second == ~s.father.first;
    f1 = miss_f || second == ~s.father.second;

    ret |= (m0 && f0) ? 1 << (~s.mother_repr.first + ~s.father_repr.first) : 0;
    ret |= (m0 && f1) ? 1 << (~s.mother_repr.first + ~s.father_repr.second) : 0;
    ret |= (m1 && f0) ? 1 << (~s.mother_repr.second + ~s.father_repr.first) : 0;
    ret |= (m1 && f1) ? 1 << (~s.mother_repr.second + ~s.father_repr.second) : 0;

    return ret;
}


inline int operator % (const allele_pair& obs, const segregation& s)
{
    return _match_obs(~obs.first, ~obs.second, s) | _match_obs(~obs.second, ~obs.first, s);
}


static inline std::vector<allele_pair> to_genotypes(int obs)
{
    std::vector<allele_pair> ret;
    if (obs & 1) {
        ret.push_back({'a', 'c'});
    }
    if (obs & 2) {
        ret.push_back({'a', 'd'});
    }
    if (obs & 4) {
        ret.push_back({'b', 'c'});
    }
    if (obs & 8) {
        ret.push_back({'b', 'd'});
    }
    return ret;
}


namespace read_data {
marker_data read_outbred(std::istream& datafile);
}

#endif

