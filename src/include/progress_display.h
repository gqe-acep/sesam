/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_PROGRESS_DISPLAY_H_
#define _SPEL_PROGRESS_DISPLAY_H_

struct progress_display {
	struct progress_item {
		const std::string& f;
		int idx;
		int total;
		progress_item(const std::string& f_, int i, int t) : f(f_), idx(i), total(t) {}
	};

	static std::vector<progress_item> items;
	static std::mutex mutex;

	static constexpr const char* const SAVE_CURSOR = "\033[s";
	static constexpr const char* const RESTORE_CURSOR = "\033[u";
	static constexpr const char* const ERASE_TO_EOL = "\033[K";
	static constexpr const char* const GOTO_0_0 = "\033[0;0H";

	static
		void disp()
		{
			std::lock_guard<msg_handler_t::lock_type> lock(msg_handler_t::mutex);
			std::cout << SAVE_CURSOR;
			std::cout << GOTO_0_0;
			if (items.size()) {
				std::cout << "Progress:";
				for (auto& pi: items) {
					std::cout << ' ' << pi.f;
					if (pi.total != 1) {
						std::cout << " [" << pi.idx << '/' << pi.total << ']';
					}
				}
			}
			std::cout << ERASE_TO_EOL;
			std::cout << RESTORE_CURSOR;
			std::cout << std::flush;
		}

	template <typename Ret, typename... Args>
		static 
		void push_elem(Ret (*fptr) (Args...), int i, int t)
		{
			std::lock_guard<std::mutex> _(mutex);
			items.emplace_back(get_func_name(fptr), i, t);
			disp();
		}

	static
		void pop_elem() { std::lock_guard<std::mutex> _(mutex); items.pop_back(); disp(); }
};

#endif

