/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <functional>

#include "error.h"
#include "input.h"
/*#include "generation_rs.h"*/
#include "settings.h"
#include "cache2.h"
// #include "geno_matrix.h"
#include "file.h"
#include "output_impl.h"

#ifdef SPELL_USE_XLNT

#include "excel.h"

namespace excel {
    xlnt::range_reference
    find_cell_range(xlnt::worksheet sheet, xlnt::cell_reference pos)
    {
        if (sheet[pos].is_merged()) {
            for (const auto& mrange: sheet.merged_ranges()) {
                xlnt::range r(sheet, mrange, xlnt::major_order::column, false);
                if (r.contains(pos)) {
                    return mrange;
                }
            }
        }
        return pos, pos;
    }
    
    manipulator
    new_worksheet(std::string name)
    {
        return [=] (stream& s) {
            s.bookptr->create_sheet().title(name);
            s.sheet = s.bookptr->sheet_by_title(name);
            s.origin = s.cursor = xlnt::cell_reference("A1"); };
    }
    
    manipulator
    select_worksheet(std::string name)
    {
        return [=] (stream& s) {
            if (s.bookptr->contains(name)) {
                s.sheet = s.bookptr->sheet_by_title(name);
            } else {
                new_worksheet(name)(s);
            }
        };
    }
    
    manipulator
    rename_worksheet(std::string name)
    {
        return [=] (stream& s) { s.sheet.title(name); };
    }
    
    manipulator
    move(std::string ref)
    {
        return [=] (stream& s) { s.origin = s.cursor = xlnt::cell_reference(ref); };
    }
    
    manipulator
    move(int row, int col)
    {
        return [=] (stream& s) { s.cursor = s.origin = xlnt::cell_reference("A1").make_offset(col - 1, row - 1); };
    }

    manipulator
    moveby(int row_delta, int col_delta)
    {
        return [=] (stream& s) { s.cursor = s.origin = s.cursor.make_offset(col_delta, row_delta); };
    }
    
    manipulator
    move(xlnt::cell_reference ref)
    {
        return [=] (stream& s) { s.origin = ref; s.cursor = s.origin; };
    }
    
    manipulator
    merge(int width, int height)
    {
        return [=] (stream& s) { s.sheet.merge_cells((s.cursor, s.cursor.make_offset(width - 1, height - 1))); };
    }
    
    manipulator
    apply_style(std::string name)
    {
        return [=] (stream& s) { s.sheet[s.cursor].style(name); };
    }
    
    manipulator
    create_style(std::string name, std::function<void(xlnt::style&)> init)
    {
        return [=] (stream& s)
        {
            auto style = s.bookptr->create_style(name);
            init(style);
        };
    }

    manipulator
    cell_format(std::function<void(xlnt::format&)> init)
    {
        return [=] (stream& s) {
            auto cell = s.current_cell();
            xlnt::format fmt = s.bookptr->create_format();
            if (cell.has_format()) {
                fmt = cell.format();
            }
            init(fmt);
            cell.format(fmt);
        };
    }

    manipulator
    border_bottom(xlnt::color c, xlnt::border_style sty)
    {
        return [=] (stream& s) {
            apply_border(s, xlnt::border_side::bottom, c, sty);
        };
    }
    
    manipulator
    border_right(xlnt::color c, xlnt::border_style sty)
    {
        return [=] (stream& s) {
            apply_border(s, xlnt::border_side::end, c, sty);
        };
    }
    
    manipulator
    border_top(xlnt::color c, xlnt::border_style sty)
    {
        return [=] (stream& s) {
            apply_border(s, xlnt::border_side::top, c, sty);
        };
    }
    
    manipulator
    border_left(xlnt::color c, xlnt::border_style sty)
    {
        return [=] (stream& s) {
            apply_border(s, xlnt::border_side::start, c, sty);
        };
    }
    
    manipulator
    border_box(int x1, int y1, int x2, int y2, const std::vector<std::pair<xlnt::color, xlnt::border_style>>& style)
    {
        return [=] (stream& s) {
            xlnt::border top_, left_, right_, bottom_, tl_, tr_, bl_, br_;
            xlnt::cell_reference topleft = xlnt::cell_reference("A1").make_offset(x1 - 1, y1 - 1);
            xlnt::cell_reference topright = xlnt::cell_reference("A1").make_offset(x2 - 1, y1 - 1);
            xlnt::cell_reference bottomleft = xlnt::cell_reference("A1").make_offset(x1 - 1, y2 - 1);
            xlnt::cell_reference bottomright = xlnt::cell_reference("A1").make_offset(x2 - 1, y2 - 1);

            xlnt::range_reference top_side((topleft.make_offset(1, 0), topright.make_offset(-1, 0)));
            xlnt::range_reference bottom_side((bottomleft.make_offset(1, 0), bottomright.make_offset(-1, 0)));
            xlnt::range_reference left_side((topleft.make_offset(0, 1), bottomleft.make_offset(0, -1)));
            xlnt::range_reference right_side((topright.make_offset(0, 1), bottomright.make_offset(0, -1)));
                        
            int T, L, R, B;
            switch (style.size()) {
                case 1:
                    T = L = R = B = 0;
                    break;
                case 2:
                    T = B = 0;
                    L = R = 1;
                    break;
                case 4:
                    T = 0;
                    R = 1;
                    B = 2;
                    L = 3;
                    break;
                case 0:
                default:
                    return;
            };
            if (style[T].second != xlnt::border_style::none) {
                top_.side(xlnt::border_side::top, xlnt::border::border_property().color(style[T].first).style(style[T].second));
                tl_.side(xlnt::border_side::top, xlnt::border::border_property().color(style[T].first).style(style[T].second));
                tr_.side(xlnt::border_side::top, xlnt::border::border_property().color(style[T].first).style(style[T].second));
            }
            if (style[R].second != xlnt::border_style::none) {
                right_.side(xlnt::border_side::end, xlnt::border::border_property().color(style[R].first).style(style[R].second));
                tr_.side(xlnt::border_side::end, xlnt::border::border_property().color(style[R].first).style(style[R].second));
                br_.side(xlnt::border_side::end, xlnt::border::border_property().color(style[R].first).style(style[R].second));
            }
            if (style[B].second != xlnt::border_style::none) {
                bottom_.side(xlnt::border_side::bottom, xlnt::border::border_property().color(style[B].first).style(style[B].second));
                br_.side(xlnt::border_side::bottom, xlnt::border::border_property().color(style[B].first).style(style[B].second));
                bl_.side(xlnt::border_side::bottom, xlnt::border::border_property().color(style[B].first).style(style[B].second));
            }
            if (style[L].second != xlnt::border_style::none) {
                left_.side(xlnt::border_side::start, xlnt::border::border_property().color(style[L].first).style(style[L].second));
                tl_.side(xlnt::border_side::start, xlnt::border::border_property().color(style[L].first).style(style[L].second));
                bl_.side(xlnt::border_side::start, xlnt::border::border_property().color(style[L].first).style(style[L].second));
            }
            xlnt::range(s.sheet, top_side).border(top_);
            xlnt::range(s.sheet, bottom_side).border(bottom_);
            xlnt::range(s.sheet, left_side).border(left_);
            xlnt::range(s.sheet, right_side).border(right_);
            s.sheet[topleft].border(tl_);
            s.sheet[topright].border(tr_);
            s.sheet[bottomleft].border(bl_);
            s.sheet[bottomright].border(br_);
        };
    }
    
    manipulator
    border_box(xlnt::cell_reference c0, xlnt::cell_reference c1, const std::vector<std::pair<xlnt::color, xlnt::border_style>>& style)
    {
        return border_box(c0.column().index, c0.row(), c1.column().index, c1.row(), style);
    }

    manipulator
    style(const std::string& name, std::function<void(xlnt::style&)> init)
    {
        return [=](stream& s) { xlnt::style sty = s.bookptr->create_style(name); init(sty); };
    }

    manipulator
    style(const std::string& name)
    {
        return [=](stream& s) { s.sheet[s.cursor].style(name); };
    }
    
    manipulator
    align(std::function<void(xlnt::alignment&)> init)
    {
        return [=](stream& s) { xlnt::alignment al; init(al); s.sheet[s.cursor].alignment(al); };
    }
    
    manipulator
    formula(const std::string& fmla)
    {
        return [&](stream& s) { s.sheet[s.cursor].formula(fmla); };
    }

} /* namespace excel */


excel::manipulator
excel::to_origin = [] (excel::stream& s) {
    s.cursor = s.origin;
};

excel::manipulator
excel::to_topright = [] (excel::stream& s) {
    s.cursor = (s.origin, s.cursor).top_right();
//     s.cursor = s.origin;
};

excel::manipulator
excel::to_bottomleft = [] (excel::stream& s) {
//     MSG_DEBUG("BEFORE to_bottomleft origin=" << s.origin.to_string() << " cursor=" << s.cursor.to_string());
    s.cursor = (s.origin, s.cursor).bottom_left();
//     s.cursor = s.origin;
//     MSG_DEBUG("AFTER to_bottomleft origin=" << s.origin.to_string() << " cursor=" << s.cursor.to_string());
};

excel::manipulator
excel::push = [] (excel::stream& s) {
    s.stack.push_back(s.origin.make_offset(0, 0));
    s.stack.push_back(s.cursor.make_offset(0, 0));
};

excel::manipulator
excel::pop = [] (excel::stream& s) {
    s.cursor = s.stack.back();
    s.stack.pop_back();
    s.origin = s.stack.back();
    s.stack.pop_back();
};

excel::manipulator
excel::next_col = [] (excel::stream& s) {
    if (s.sheet[s.cursor].is_merged()) {
//         int ofs = 1;
        s.cursor = find_cell_range(s.sheet, s.cursor).top_right().make_offset(1, 0);
    } else {
        s.cursor = s.cursor.make_offset(1, 0);
    }
};

excel::manipulator
excel::next_row = [] (excel::stream& s) {
    xlnt::cell_reference linestart(s.origin.column(), s.cursor.row());
    if (s.sheet[linestart].is_merged()) {
        s.cursor = find_cell_range(s.sheet, s.cursor).bottom_left().make_offset(0, 1);
    } else {
        s.cursor = linestart.make_offset(0, 1);
    }
};

excel::manipulator
excel::prev_col = [] (excel::stream& s) {
    if (s.sheet[s.cursor].is_merged()) {
//         int ofs = 1;
        s.cursor = find_cell_range(s.sheet, s.cursor).top_left().make_offset(-1, 0);
    } else {
        s.cursor = s.cursor.make_offset(-1, 0);
    }
};

excel::manipulator
excel::prev_row = [] (excel::stream& s) {
    xlnt::cell_reference linestart(s.origin.column(), s.cursor.row());
    if (s.sheet[linestart].is_merged()) {
        s.cursor = find_cell_range(s.sheet, s.cursor).top_left().make_offset(0, -1);
    } else {
        s.cursor = linestart.make_offset(0, -1);
    }
};

excel::manipulator
excel::set_origin = [] (excel::stream& s) { s.origin = s.cursor; };


#endif /* SPELL_USE_XLNT */


std::atomic<size_t> file::open_count(3);  /* stdin, stdout, stderr */
std::map<std::string, size_t> file::debug_open_files;
std::mutex file::dof_mutex;

#if 0
const algebraic_genotype algebraic_genotype::null = {{{0, 0}, {0, 0}}, algebraic_genotype::Type::Null, {0, 0, {0}}};

const polynom rs_polynom::r_poly = {0, 1};
const polynom rs_polynom::s_poly = {1, -1};
#endif

int RIL_ORDER = 10;
int MAGIC8_ORDER = 10;

#if 0
fast_polynom fast_polynom::zero = 0;
fast_polynom fast_polynom::one = 0;
fast_polynom fast_polynom::r = 0;
fast_polynom fast_polynom::s = 0;
fast_polynom fast_polynom::infinity = 0;

const algebraic_genotype algebraic_genotype::null = {{{0, 0}, {0, 0}}, algebraic_genotype::Type::Null, fast_polynom::zero};

auto& reg = impl::polynom_tables::registry(); /* acts as init */

#endif

msg_handler_t::lock_type msg_handler_t::mutex;

settings_t* active_settings = NULL;

std::thread::id settings_t::main_thread = std::this_thread::get_id();

namespace model_print {
    int precision = 4;
    std::string field_sep = "  ";
    std::string section_hsep = " | ";
    char section_vsep = '-';
    std::string section_csep = "-+-";
}


label_type empty = {GAMETE_EMPTY, GAMETE_EMPTY};

geno_matrix
    gamete = {
        "generic_gamete",
        Gamete,
        {{GAMETE_L, GAMETE_EMPTY}, {GAMETE_R, GAMETE_EMPTY}},
        (MatrixXd(2, 2) <<
         -1,  1,
          1, -1).finished(),
        /* BEWARE these matrices (p and p_inv) SHOULD be * 1/sqrt(2) */
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 1) << 0, -2).finished(),
        (VectorXd(2) << .5, .5).finished(),
        (MatrixXd(2, 2) <<
         1, 0,
         0, 1).finished(),
        (MatrixXd(2, 2) <<
         1, 0,
         0, 1).finished(),
#ifdef WITH_OVERLUMPING
        {},
            /*{{{GAMETE_L, GAMETE_L}, {GAMETE_R, GAMETE_R}}, {{0, 0}, {1, 1}}},*/
            /*{{{GAMETE_L, GAMETE_R}, {GAMETE_R, GAMETE_L}}, {{0, 1}, {1, 0}}}*/
            /*symmetry_group_type().insert({permutation_type::anti_identity(2), letter_permutation_type{}})*/
        /*},*/
        {}
#endif

    },
    doubling_gamete = {
        "doubling_gamete",
        DoublingGamete,
        {{GAMETE_L, GAMETE_L}, {GAMETE_R, GAMETE_R}},
        (MatrixXd(2, 2) <<
         -1,  1,
          1, -1).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 1) << 0, -2).finished(),
        (VectorXd(2) << .5, .5).finished(),
        (MatrixXd(2, 2) <<
         1, 0,
         0, 1).finished(),
        (MatrixXd(2, 2) <<
         1, 0,
         0, 1).finished(),
#ifdef WITH_OVERLUMPING
        {},
            /*{{{GAMETE_L, GAMETE_L}, {GAMETE_R, GAMETE_R}}, {{0, 1}, {1, 0}}}*/
            /*{{{0, 1}, {1, 0}}}*/
            /*symmetry_group_type().insert({permutation_type::identity(2), letter_permutation_type{}})*/
        /*},*/
        {}
#endif
    };

geno_matrix
    selfing_gamete = kronecker(gamete, gamete);


/*std::vector<progress_display::progress_item> progress_display::items;*/
/*std::mutex progress_display::mutex;*/
