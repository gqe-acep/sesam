// #ifndef SPELL_PEDIGREE_MAIN
// #  define SPELL_PEDIGREE_MAIN spell_pedigree
// #endif
// #ifndef SPELL_BAYES_MAIN
// #  define SPELL_BAYES_MAIN spell_marker
// #endif

#define R_NO_REMAP

#include "eigen.h"
#include <vector>
#include <map>
#include <string>
// #include <boost/filesystem.hpp>
#include "error.h"
#include "geno_matrix.h"
#include "bayes/output.h"
#include "map-likelihood/cli.h"
#include "cache/md5.h"
#include "task_pool.h"
#include "cache2.h"

#include "RWrap/RWrap.h"
#include <numeric>

extern "C" {
#include <ftw.h>
}

// Load and initialize data

// SEM (order)

// Try (order, marker)

// Flips (order)

// LOD2pt (order, order)

typedef std::vector<std::string> marker_vec;


extern int SPELL_BAYES_MAIN(int, const char**);
extern int SPELL_PEDIGREE_MAIN(int, const char**);


class Spell2PtMatrix;


extern "C" {
int clear_cache(const char *fpath, const struct stat *, int typeflag, struct FTW*)
{
    /* FIXME check for some magic bytes or something before deleting! */
    static std::regex is_cache_file("^.*/[a-f0-9]{1,16}$");
    /*std::string f(fpath);*/
    if (typeflag == FTW_F) {  /* ignore all but regular files */
        if (remove(fpath) != 0) {
            MSG_WARNING("Couldn't remove file \"" << fpath << "\": " << strerror(errno));
        /*} else {*/
            /*MSG_DEBUG("Deleted cache file <" << fpath << '>');*/
        }
    } else if (typeflag == FTW_D) {
        if (remove(fpath) != 0) {
            MSG_WARNING("Couldn't remove directory \"" << fpath << "\": " << strerror(errno));
        }
    }
    return 0;
}
}


class SpellMapTools {
private:
    gamete_LV_database gamete_LV;
    double m_convergence_threshold = 1.e-6;
    int m_max_iterations = 100;
    bool m_active = false;
    std::string wd, name;
    std::string _ped;
    std::string _base;
    std::string _gen;
    std::string _form;
    std::string _raw;
public:
    //// ADRIEN VIDAL 2020-06-16 : Matching revamped constructor.
    SpellMapTools(std::string ped_filename, std::string obs_filename, std::string obs_gen, std::string obs_format, int mt, std::string basedir);
    Rwrap::List SEM(marker_vec order);
    EM_map pSEM(marker_vec order); //// ADRIEN VIDAL : New SEM function that outputs EMmap instead of RWrap List to avoid loss of R object protection when leaving the scope of the single SEM execution.
    Rwrap::List mSEM(Rwrap::List orders);
    Spell2PtMatrix* LOD2pt(marker_vec rows, marker_vec cols);
    Spell2PtMatrix* R2pt(marker_vec rows, marker_vec cols);
    double map_likelihood(const marker_vec& order, const std::vector<double>& distances){ 
        return gamete_LV.map_likelihood(order, distances); 
    }
    gamete_LV_database::EM_computer_type EM_computer(){ 
        return gamete_LV.EM_computer(); 
    }
    EM_map EM(gamete_LV_database::EM_computer_type& emc, const marker_vec& order, bool dist_if_true_else_r=true){ 
        return emc.EM(order, dist_if_true_else_r, m_convergence_threshold, m_max_iterations); 
    }

    int max_iterations(int n)
    {
        if (n) { std::swap(m_max_iterations, n); return n; }
        else { return m_max_iterations; }
    }

    double convergence_threshold(double t)
    {
        if (t) { std::swap(m_convergence_threshold, t); return t; }
        else { return m_convergence_threshold; }
    }

    bool active_check()
    {
        if (!m_active) {
            MSG_ERROR("This instance is not active. There was a problem during initialization.", "");
            return true;
        }
        return false;
    }

    bool is_active() { return m_active; }

    std::vector<std::string>
    get_marker_names()
    {
        std::vector<std::string> ret;
        if (gamete_LV.data.size() == 0) {
            return ret;
        }
        const auto& lv = gamete_LV.data.begin()->second.lv;
        ret.reserve(lv.size());
        for (const auto& kv: lv) {
            ret.emplace_back(kv.first);
        }
        return ret;
    }

    void
    cleanup_disk_cache()
    {
        nftw(wd.c_str(), clear_cache, 10, FTW_DEPTH);
    }
    
    Rwrap::List
    get_session_data() {
        Rwrap::List ret;
        ret.add("session.id", name);
        ret.add("base.dir", _base);
        ret.add("pedigree.filename", _ped);
        ret.add("gen", _gen);
        ret.add("format", _form);
        ret.add("filename", _raw);
        return ret;
    }
};


namespace std {
    template <>
    struct hash<SpellMapTools> {
        size_t operator () (const SpellMapTools& x) const { ptrdiff_t _ = (ptrdiff_t) &x; return hash<ptrdiff_t>()(_); }
    };
}

class Spell2PtMatrix {
private:
    SpellMapTools* instance;
    std::vector<std::string> rownames, colnames;
    std::map<std::string, int> row_indices, col_indices;
    std::map<std::pair<std::string, std::string>, double> cache;
    std::map<std::pair<std::string, std::string>, double> r_cache;
    bool lod_if_true_else_r;
public:
    Spell2PtMatrix(SpellMapTools* smt, std::vector<std::string> r, std::vector<std::string> c)
        : instance(smt), rownames(r), colnames(c), row_indices(), col_indices(), cache(), r_cache(), lod_if_true_else_r(true)
    {
        for (const auto& m: rownames) {
            row_indices.emplace(m, row_indices.size());
        }
        for (const auto& m: colnames) {
            col_indices.emplace(m, col_indices.size());
        }
    }

    void rate() { lod_if_true_else_r = false; }
    void lod() { lod_if_true_else_r = true; }
    
    int is_rate() { return (int) !lod_if_true_else_r; }
    int is_lod() { return (int) lod_if_true_else_r; }
    
    marker_vec getRownames() { return rownames; }
    marker_vec getColnames() { return colnames; }
    
    std::vector<int>
    getDim()
    {
        return {(int) rownames.size(), (int) colnames.size()};
    }

    void
    get_value_impl(int i, int j, double& lod, double& r)
    {
        if (i < 1 || j < 1 || i > rownames.size() || j > colnames.size()) {
            lod = r = NA_REAL;
            return;
        }
        --i; --j;
        if (rownames[i] == colnames[j]) {
            lod = r = 0;
        }
        std::pair<std::string, std::string> key = {rownames[i], colnames[j]};
        if (key.first > key.second) {
            std::swap(key.first, key.second);
        }
        auto it = cache.find(key);
        if (it == cache.end()) {
//             MSG_INFO("Key " << key.first << ':' << key.second << " not in cache!");
            static const std::vector<double> at_worlds_end = {EM_R_MAX};
            marker_vec minimap = {rownames[i], colnames[j]};
            auto emc = instance->EM_computer();
            auto em = instance->EM(emc, minimap, false);
            double best = em.likelihood;
            r = em.distances[0];
            r_cache.emplace(key, r);
//             instance->init_2pt_tr_at_inf();
//             double inf_ref = instance->map_likelihood(minimap, at_worlds_end);
            double inf_ref = emc.twoMarkerLikelihoodAtInf();
            lod = best - inf_ref;
//             MSG_INFO("LOD(" << minimap[0] << " … " << minimap[1] << " => d=" << em.distances[0] << " best=" << best << " inf_ref=" << inf_ref << " value=" << lod);
            cache.emplace(key, lod);
        } else {
//             MSG_INFO("Key " << key.first << ':' << key.second << " in cache! " << it->first.first << ':' << it->first.second);
            lod = it->second;
            r = r_cache[key];
        }
    }


    void
    get_value_impl_nocache(int i, int j, double& lod, double& r)
    {
        if (i < 1 || j < 1 || i > rownames.size() || j > colnames.size()) {
            lod = r = NA_REAL;
            return;
        }
        --i; --j;
        if (rownames[i] == colnames[j]) {
            lod = r = NA_REAL;
            return;
        }
//             MSG_INFO("Key " << key.first << ':' << key.second << " not in cache!");
        marker_vec minimap = {rownames[i], colnames[j]};
        auto emc = instance->EM_computer();
        auto em = instance->EM(emc, minimap, false);
        double best = em.likelihood;
        r = em.distances[0];
        double inf_ref = emc.twoMarkerLikelihoodAtInf();
        lod = best - inf_ref;
//         MSG_INFO("LOD(" << minimap[0] << " … " << minimap[1] << " => d=" << em.distances[0] << " best=" << best << " inf_ref=" << inf_ref << " value=" << lod);
    }

    double get2PtValue(int i, int j)
    {
        if (!instance->is_active()) {
            return NA_REAL;
        }
        double lod, r;
        get_value_impl(i, j, lod, r);
        return lod_if_true_else_r ? lod : r;
    }
    
    double get2PtValue_nocache(int i, int j)
    {
        if (!instance->is_active()) {
            return NA_REAL;
        }
        double lod, r;
        get_value_impl_nocache(i, j, lod, r);
        return lod_if_true_else_r ? lod : r;
    }
    
    SEXP
    as_matrix();
};


namespace std {
    template <>
    struct hash<Spell2PtMatrix> {
        size_t operator () (const Spell2PtMatrix& ref) const { return hash<void*>()((void*) &ref); }
    };
}

int
invoke_spell(int (&main_func) (int argc, const char** argv), const std::vector<std::string>& arglist)
{
    std::vector<const char*> argv;
    argv.resize(arglist.size() + 1, NULL);
    for (size_t i = 0; i < arglist.size(); ++i) {
        argv[i] = arglist[i].c_str();
    }
    int ret = main_func(arglist.size(), argv.data());
    argv.clear();
    return ret;
}

//// FRANCK GAUTHIER: TODO, find equivalent header/functions for windows
extern "C" {
#   include <sys/types.h>
#   include <sys/stat.h>
#   include <unistd.h>

    unsigned int
    get_file_date(const char* filename)
    {
        struct stat buffer;
        int result;
        if ((result = stat(filename, &buffer))) {
//             MSG_INFO("stat result (" << filename << ") = " << result << " errno=" << strerror(errno));
            /* display filesystem error? */
            return 0;
        } else {
			//// FRANCK GAUTHIER 2019-09-05 : st_mtim.tv_sec is not compatible with macOS while st_mtime is compatible with linux/cygwin and macOS.
            return buffer.st_mtime; ////return buffer.st_mtim.tv_sec;
        }
    }

}

//// ADRIEN VIDAL 2020-06-16 : Replaced RWrap::dataframe observation_specs by 3 arguments (obs_filename, obs_gen, obs_format) because this RWrap object doesn't work with later R versions.
SpellMapTools::SpellMapTools(std::string ped_filename, std::string obs_filename, std::string obs_gen, std::string obs_format, int mt, std::string basedir)
    : gamete_LV()
{
//     MSG_DEBUG(__FILE__ << ':' << __LINE__);

    //MSG_INFO("OBS GEN=" << obs_gen);
    //MSG_INFO("OBS FORMAT=" << obs_format);
    //MSG_INFO("OBS FILENAME=" << obs_filename);

    //// ADRIEN VIDAL 2020-06-16 : Object conversion necessary because std::vector<const char*> used to be extracted from observation_specs.
    std::vector<const char*> cgen;
    cgen.emplace_back( obs_gen.c_str() );
    std::vector<const char*> cform;
    cform.emplace_back( obs_format.c_str() );
    std::vector<const char*> craw;
    craw.emplace_back( obs_filename.c_str() );

    //// ADRIEN VIDAL 2020-06-16 : Direct allocation of arg strings to attributes now possible.
    _gen = obs_gen;
    _form = obs_format;
    _raw = obs_filename;

    md5_digest md5;

    TaskPool::init(mt > 0 ? mt : 1);
    
    _ped = ped_filename;
    _base = basedir;
    
    md5 << ped_filename;

//     MSG_DEBUG(__FILE__ << ':' << __LINE__);

    wd = SPELL_STRING(_base << "/" << "spell-map-tools-" << ((std::string) md5));

    unsigned int ped_time, data_time;

    ped_time = data_time = get_file_date(ped_filename.c_str());

//     MSG_DEBUG(__FILE__ << ':' << __LINE__);

    for (size_t i = 0; i < cgen.size(); ++i) {
        md5 << cgen[i] << cform[i] << craw[i];
        std::time_t t = get_file_date(craw[i]);
        if (t > data_time) {
            data_time = t;
        }
    }

//     MSG_DEBUG(__FILE__ << ':' << __LINE__);

    name = md5;

    std::string spell_ped_file = SPELL_STRING(wd << '/' << name << ".cache/" << name << ".spell-pedigree.data");
    std::string spell_mark_file = SPELL_STRING(wd << '/' << name << ".cache/gamete.data");

    std::time_t spell_ped_t = get_file_date(spell_ped_file.c_str());
    std::time_t spell_mark_t = 0;

//     MSG_DEBUG(__FILE__ << ':' << __LINE__);

//     MSG_INFO("ped_time=" << ped_time);
//     MSG_INFO("spell_ped_t=" << spell_ped_t);

    /* Run spell-pedigree if needed */
    if (spell_ped_t < ped_time) {
//         MSG_DEBUG(__FILE__ << ':' << __LINE__);
        data_time = std::time(nullptr);
        std::vector<std::string> args = {"spell-pedigree", "-wd", wd, "-n", name, "-p", ped_filename};
        int ret = invoke_spell(SPELL_PEDIGREE_MAIN, args);
        if (ret) {
            MSG_ERROR("Something went wrong while loading the pedigree…", "");
            return;
        }
    } else {
//         MSG_DEBUG(__FILE__ << ':' << __LINE__);
        data_time = spell_ped_t;
        spell_mark_t = get_file_date(spell_mark_file.c_str());
    }
//     MSG_DEBUG(__FILE__ << ':' << __LINE__);

//     MSG_INFO("data_time=" << data_time);
//     MSG_INFO("spell_mark_t=" << spell_mark_t);

    /* Run spell-marker if needed */
    if (spell_mark_t < data_time && cgen.size() > 0) {
//         MSG_DEBUG(__FILE__ << ':' << __LINE__);
//         std::stringstream out;
        std::vector<std::string> args = {"spell-marker", "-wd", wd, "-n", name, "-Og", "-Op"};
        if (mt > 1) {
            args.push_back("-mt");
            args.push_back(SPELL_STRING(mt));
        }
        for (size_t i = 0; i < cgen.size(); ++i) {
            args.push_back("-m");
            args.push_back(SPELL_STRING(cgen[i] << ':' << cform[i]));
            args.push_back(craw[i]);
//             if (out.tellp()) {
//                 out << ',';
//             }
//             out << obs_gen[i];
        }
//         args.push_back("-o");
//         args.push_back(out.str());
        if (invoke_spell(SPELL_BAYES_MAIN, args)) {
            MSG_ERROR("Aborting spell-map-tools initialization because of errors…", "");
            return;
        }
    }
//     MSG_DEBUG(__FILE__ << ':' << __LINE__);

    {
//         MSG_DEBUG(__FILE__ << ':' << __LINE__);

        // R 4.1 Bug happens here: file not found.
        //ifile gam(spell_mark_file);    
        // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
        ifile gam(spell_mark_file, std::ios_base::in | std::ios_base::binary);

        rw_base() (gam, gamete_LV);
//         MSG_DEBUG(__FILE__ << ':' << __LINE__);
    }
//     MSG_DEBUG(__FILE__ << ':' << __LINE__);
    m_active = true;
}

//// ADRIEN VIDAL : New SEM function that outputs EMmap instead of RWrap List to avoid loss of R object protection when leaving the scope of the single SEM execution.
EM_map SpellMapTools::pSEM(marker_vec order){
    if (active_check()) {
        return {};
    }
//     MSG_INFO("Have this=" << ((void*)this) << " and order " << order);
    auto emc = gamete_LV.EM_computer();
    return EM(emc, order);
}

Rwrap::List map2list(const EM_map& map){
    Rwrap::List ret;
    ret.add("distances", map.distances);
    ret.add("markers", map.marker_names);
    ret.add("likelihood", map.likelihood);
    ret.add("n_iterations", map.n_iterations);
    ret.add("converged", (int) map.converged);
    ret.add("delta", map.delta);
    ret.add("r", map.r);
    return ret;
}

/*
Rwrap::List SpellMapTools::SEM(marker_vec order)
{
    if (active_check())
    {
        return {};
    }
    //     MSG_INFO("Have this=" << ((void*)this) << " and order " << order);
    auto emc = gamete_LV.EM_computer();
    return map2list(EM(emc, order));
}
*/

//// ADRIEN VIDAL : (single execution) SEM function now executes pSEM before converting output to Rwrap::List.
Rwrap::List SpellMapTools::SEM(marker_vec order){
    return map2list(pSEM(order));
}


extern "C" {
    //Rwrap::List c_SEM(SpellMapTools* smt, marker_vec order) { return smt->SEM(order); }
    //// ADRIEN VIDAL : Reworked to output EMmap for parallel execution because RWrap::List output couldn't be protected.
	EM_map c_SEM(SpellMapTools* smt, marker_vec order) { return smt->pSEM(order); }
}

//// ADRIEN VIDAL : Reworked to use c_SEM, only keeping non-R objects in memory until all threads are resolved, avoiding R object protection problems.
Rwrap::List SpellMapTools::mSEM(Rwrap::List orders)
{
    Rwrap::List result;
    collection<EM_map> jobs;
    for (int i = 0; i < (int) orders.size(); ++i) {
        marker_vec tmp = orders[i].to<marker_vec>();
//         MSG_INFO("have order " << tmp);
        jobs.push_back(make_value<>(c_SEM, as_value(this), as_value(tmp)));
    }
    for (int i = 0; i < (int) orders.size(); ++i) {
//         auto r = *jobs[i];
//         result.add(NULL, r);
        result.add(NULL, map2list(*jobs[i]));
    }
    return result;
}


/*
double
SpellMapTools::Flips(marker_vec order, int window_size)
{
    if (active_check()) {
        return NA_REAL;
    }
    std::vector<size_t> window(window_size);
    auto emc = gamete_LV.EM_computer();
    double best = EM(emc, order).likelihood;
    double ref = best;
    size_t size = order.size() - window.size() + 1;
    for (size_t i = 0; i < size; ++i) {
        marker_vec tmp_order(order);
        std::iota(window.begin(), window.end(), i);
        if (0) {
            std::stringstream ss;
            auto i = window.begin(), j = window.end();
            ss << *i;
            for (++i; i != j; ++i) {
                ss << ' ' << *i;
            }
//             MSG_INFO("Window " << ss.str());
        }
        while (std::next_permutation(window.begin(), window.end())) {
            if (0) {
                std::stringstream ss;
                auto i = window.begin(), j = window.end();
                ss << *i;
                for (++i; i != j; ++i) {
                    ss << ' ' << *i;
                }
//                 MSG_INFO("current window " << ss.str());
            }
            auto it = tmp_order.begin() + i;
            for (size_t x: window) {
                *it++ = order[x];
            }
            double tmp = EM(emc, tmp_order).likelihood;
            if (tmp > best) {
                best = tmp;
            }
            if (0) {
                std::stringstream ss;
                auto i = tmp_order.begin(), j = tmp_order.end();
                ss << *i;
                for (++i; i != j; ++i) {
                    ss << "…" << *i;
                }
//                 MSG_INFO("Flips on map " << ss.str() << " => " << tmp);
            }
        }
//         MSG_DEBUG("Done permuting.");
    }
    return ref - best;
}
*/

Spell2PtMatrix* SpellMapTools::LOD2pt(marker_vec row_order, marker_vec col_order){
    active_check();
    return new Spell2PtMatrix(this, row_order, col_order);
}


static double get_2pt__(Spell2PtMatrix* mat, int i, int j) { return mat->get2PtValue_nocache(i, j); }
/*
SEXP as_matrix_2pt(Spell2PtMatrix* mat){
    int nr = (int) mat->getRownames().size();
    int nc = (int) mat->getColnames().size();
    SEXP ret = PROTECT(Rf_allocMatrix(REALSXP, nr, nc));
    double* data = REAL(ret);
    
    collection<double> jobs;
    jobs.reserve(nr * nc);
    
    for (int j = 0; j < nc; ++j) {
        for (int i = 0; i < nr; ++i) {
            jobs.push_back(make_value(get_2pt__, as_value(mat), as_value(i + 1), as_value(j + 1)));
        }
    }
    
    auto jobs_i = jobs.begin();
    auto data_i = data;
    for (int j = 0; j < nc; ++j) {
        for (int i = 0; i < nr; ++i) {
            *data_i++ = **jobs_i++;
        }
    }

    UNPROTECT(1);

    return ret;
}

SEXP Spell2PtMatrix::as_matrix(){
    SEXP ret = PROTECT(as_matrix_2pt(this));
    UNPROTECT(1);
    return ret;
}
*/

// Merger of the two above functions:
SEXP Spell2PtMatrix::as_matrix(){
    int nr = (int) this->getRownames().size();
    int nc = (int) this->getColnames().size();
    
    collection<double> jobs;
    jobs.reserve(nr * nc);
    
    for (int j = 0; j < nc; ++j) {
        for (int i = 0; i < nr; ++i) {
            jobs.push_back(make_value(get_2pt__, as_value(this), as_value(i + 1), as_value(j + 1)));
        }
    }
    SEXP ret;
    PROTECT( ret = Rf_allocMatrix(REALSXP, nr, nc) );
    //SEXP ret = PROTECT(Rf_allocMatrix(REALSXP, nr, nc));
    double *data = REAL(ret);

    //auto jobs_i = jobs.begin();
    //auto data_i = data;
    for (int j = 0; j < nc; ++j){
        for (int i = 0; i < nr; ++i){
            //*data_i++ = **jobs_i++;
            data[i + j * nr] = *jobs[i + j * nr];
        }
    }

    UNPROTECT(1);

    return ret;
}

CLASS(Spell2PtMatrix)
    //.method(Spell2PtMatrix, Spell2PtMatrix)
    .method(Spell2PtMatrix, get2PtValue).arg("i").arg("j").auto_glue()
    .method(Spell2PtMatrix, rate).auto_glue()
    .method(Spell2PtMatrix, lod).auto_glue()
    .method(Spell2PtMatrix, is_lod).auto_glue()
    .method(Spell2PtMatrix, is_rate).auto_glue()
    .method(Spell2PtMatrix, getDim).auto_glue()
    .method(Spell2PtMatrix, getRownames).auto_glue()
    .method(Spell2PtMatrix, getColnames).auto_glue()
    .method(Spell2PtMatrix, as_matrix).auto_glue()
    ;

CLASS(SpellMapTools)
    //// ADRIEN VIDAL 2020-06-16 : Matching revamped constructor.
    .ctor<std::string, std::string, std::string, std::string, int, std::string>("pedigree.filename", "observation.filename", "observations.gen", "observations.format", "mt", "base.dir")
    .method(SpellMapTools, SEM).arg("order").auto_glue()
    .method(SpellMapTools, mSEM).arg("orders").auto_glue()
    .method(SpellMapTools, LOD2pt).arg("row.order").arg("col.order").auto_glue()
    .method_name("max.iterations", "max_iterations_", SpellMapTools, max_iterations).arg("n", "0").auto_glue()
    .method_name("convergence.threshold", "convergence_threshold_", SpellMapTools, convergence_threshold).arg("t", "0").auto_glue()
    .method_name("marker.names", "get_marker_names_", SpellMapTools, get_marker_names).auto_glue()
    .method_name("session.data", "get_session_data_", SpellMapTools, get_session_data).auto_glue()
    .method_name("cleanup.disk.cache", "cleanup_disk_cache_", SpellMapTools, cleanup_disk_cache).auto_glue()
    ;


MODULE(SeSAM)
    .add_class(SpellMapTools)
    .add_class(Spell2PtMatrix)
    .add_s3method("'['", "Spell2PtMatrix")
    .add_s3method("dim", "Spell2PtMatrix")
    .add_s3method("as.matrix", "Spell2PtMatrix")
    .add_s3method("rownames", "Spell2PtMatrix")
    .add_s3method("colnames", "Spell2PtMatrix")
//     .add_s3method("print", "Spell2PtMatrix")
    ;

    
#include "output_impl.h"
