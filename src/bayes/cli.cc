/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <input/marker_obs_formats.h>
#include "bayes.h"
#include "pedigree.h"
#include "bayes/cli.h"

extern "C"{
    #include<string.h>
    #include<libgen.h>
#ifdef _WIN32
	#include <windows.h>
}
#define BUFSIZE 4096
#else
}
#endif

#define PREDICATE CONSTRAINT_PREDICATE(bn_settings_t)
#define CALLBACK_ARGS ARGUMENT_CALLBACK_ARGS(bn_settings_t)


/* TODO FIXME */
#if 0
MatrixXd make_obs_to_symbol(const marker_observation_spec& spec, const generation_rs* gen)
{
    std::vector<allele_pair> labels = gen->unique_labels;
    MatrixXd obs_to_symbol = MatrixXd::Zero(spec.scores.size(), labels.size());
    for (auto& l: labels) {
        l = {~l.first - 'a', ~l.second - 'a'};
    }
    for (size_t r = 0; r < spec.scores.size(); ++r) {
        const std::vector<std::pair<char, char>>& geno_vec = spec.scores[r].second;
        /*MSG_DEBUG(spec.scores[r].first);*/
        for (size_t g = 0; g < geno_vec.size(); ++g) {
            allele_pair l = {geno_vec[g].first, geno_vec[g].second};
            /*MSG_DEBUG(l);*/
            /*MSG_DEBUG(((int)~l.first) << ':' << ((int)~l.second));*/
            /*size_t count = 0;*/
            for (int c = 0; c < obs_to_symbol.cols(); ++c) {
                /*MSG_DEBUG(((int)~labels[c].first) << ':' << ((int)~labels[c].second));*/
                if (labels[c] == l) {
                    obs_to_symbol(r, c) = 1.;
                    /*MSG_DEBUG(r << ',' << c << ' ' << 1.);*/
                }
            }
        }
        /*obs_to_symbol.row(r) /= obs_to_symbol.row(r).sum();*/
    }
    /*for (int c = 0; c < obs_to_symbol.cols(); ++c) {*/
        /*obs_to_symbol.col(c) /= obs_to_symbol.col(c).sum();*/
    /*}*/
    /*MSG_DEBUG(obs_to_symbol);*/
    return obs_to_symbol;
}
#endif



void convert_to_mapmaker(const pop_data_type& popdata, const std::vector<chromosome>& gm, const std::string& gen_name, const marker_observation_spec& spec, char unknown, const std::string& data_type, const std::string& filename)
{
    std::vector<char> obs_symbols;
    /*const auto& labels = popdata.generations.find(gen_name)->second->get_unique_labels();*/
    /*MSG_DEBUG("#scores = " << spec.scores.size() << " #labels = " << labels.size());*/
    /*MSG_QUEUE_FLUSH();*/
    /*MatrixXd obs_to_symbol = MatrixXd::Zero(spec.scores.size(), labels.size());*/
    /*MSG_DEBUG(MATRIX_SIZE(obs_to_symbol));*/
    /*MSG_QUEUE_FLUSH();*/

    obs_symbols.resize(spec.scores.size());

    for (size_t c = 0; c < spec.scores.size(); ++c) {
        obs_symbols[c] = spec.scores[c].first;
    }
#if 0
    for (size_t c = 0; c < spec.scores.size(); ++c) {
        std::vector<std::pair<char, char>> geno_vec;
        std::tie(obs_symbols[c], geno_vec) = spec.scores[c];
        double coef = 1. / geno_vec.size();
        for (size_t g = 0; g < geno_vec.size(); ++g) {
            allele_pair l = {geno_vec[g].first, geno_vec[g].second};
            for (int r = 0; r < obs_to_symbol.rows(); ++r) {
                if (labels[r] == l) {
                    obs_to_symbol(r, c) = coef;
                }
            }
        }
    }
#endif

    ofile output(filename); // text mode OK

    size_t n_ind = popdata.LV.data.begin()->second.find(gen_name)->second.size();
    size_t n_mark = 0;
    for (const auto& chrom: gm) {
        n_mark += chrom.raw.marker_name.size();
    }

    output << "data type " << data_type << std::endl;
    output << n_ind << ' ' << n_mark << " 0 0" << std::endl;
    
    for (const auto& chrom: gm) {
        size_t m = 0;
        for (const auto& mark: chrom.raw.marker_name) {
            output << '*' << mark << ' ';
            MSG_DEBUG("MARK " << mark);
            for (size_t ind = 0; ind < n_ind; ++ind) {
                MatrixXd obs_to_symbol; // = make_obs_to_symbol(spec, popdata.get_generation_rs(gen_name, ind));
                const auto& lv = popdata.LV(chrom.name, gen_name, ind);
                /*MSG_DEBUG("obs_to_symbol");*/
                /*MSG_DEBUG(obs_to_symbol);*/
                MSG_QUEUE_FLUSH();
                const VectorXd obs = lv.col(m);
                /*MSG_DEBUG(MATRIX_SIZE(obs));*/
                /*MSG_QUEUE_FLUSH();*/
                MatrixXd complement = (MatrixXd::Ones(obs_to_symbol.rows(), obs_to_symbol.cols()) - obs_to_symbol);
                /*VectorXd observed = (obs.array() != 0.).cast<double>().matrix();*/
                /*VectorXd not_observed = (obs.array() == 0.).cast<double>().matrix();*/
                const VectorXd& observed = obs;
                VectorXd not_observed = VectorXd::Ones(obs.size()) - obs;
                not_observed /= not_observed.sum();
                VectorXd obs_like
                    = obs_to_symbol * observed - obs_to_symbol * not_observed
                    + complement * not_observed - complement * observed;
                    /*- (MatrixXd::Ones(obs_to_symbol.rows(), obs_to_symbol.cols()) - obs_to_symbol) * observed;*/
                int max_i = 0;
                double max = 0;
                bool fail = true;
                for (int i = 0; i < obs_like.size(); ++i) {
                    if (obs_like(i) > max) {
                        fail = false;
                        max_i = i;
                        max = obs_like(i);
                    } else if (obs_like(i) == max) {
                        fail = true;
                    }
                }
                if (fail) {
                    MSG_DEBUG("FAILED TO SELECT A SINGLE OBSERVATION: " << obs_like.transpose() << " LV: " << obs.transpose());
                    output << unknown;
                    MSG_DEBUG(ind << " #");
                } else {
                    output << obs_symbols[max_i];
                    MSG_DEBUG(ind << ' ' << obs_symbols[max_i]);
                }
            }
            output << std::endl;
            ++m;
        }
    }
}



bn_settings_t* ensure(bn_settings_t*& t)
{
    if (t == NULL) {
        t = new bn_settings_t();
    }
    return t;
}


static
argument_section_list_t<bn_settings_t>
arguments = {
    {"Advanced (NOT INTENDED FOR USERS!)", "", true, {
        {{"--full-help", "--advanced-help"},
            {},
            "Display usage.",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                print_usage_impl(true, arguments);
                exit(0);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-D", "--debug"},
            {},
            "Display debug messages",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                msg_handler_t::debug_enabled() = true;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-q", "--quiet"},
            {},
            "Suppress all output (including debug output)",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                msg_handler_t::quiet() = true;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-Dp", "--dump-pop"},
            {"popdata file"},
            "Dump the contents of the given popdata file and exit.",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                std::unique_ptr<pop_data_type> test(pop_data_type::load_from(*++ai));
                MSG_DEBUG((*test));
                exit(0);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-J", "--job"},
            {"name", "begin", "end"},
            "Run the BP on the slice [begin;end]",
            false,
            {},
            [](CALLBACK_ARGS)
            {
                ensure(target)->job_name = *++ai;
                ensure(target)->job_start = to<int>(*++ai);
                ensure(target)->job_end = to<int>(*++ai);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-F", "--fifo"},
            {"path"},
            "Set the path to the FIFO for feedback (SGE)",
            false,
            {},
            [](CALLBACK_ARGS)
            {
                ensure(target)->fifo_path = *++ai;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-M", "--mapmaker-output"},
            {"popdata file", "gen name", "obs format", "unknown char", "data type string", "output file"},
            "Output observations for given generation in a mapmaker-like format using the given observation format specification and exit",
            false,
            {},
            [](CALLBACK_ARGS)
            {
                std::unique_ptr<pop_data_type> pop(pop_data_type::load_from(*++ai));
                std::string gen_name = *++ai;
                const auto& spec = ensure(target)->marker_observation_specs[*++ai]; (void) spec;
                char unknown = (*++ai)[0];
                std::string data_type = *++ai;
                std::string output = *++ai;
                /* TODO FIXME no genetic map now, need to pass it as argument here */
                /*convert_to_mapmaker(*pop, ensure(target)->map, gen_name, spec, unknown, data_type, output);*/
                exit(0);
                SAFE_IGNORE_CALLBACK_ARGS;
                (void) unknown;
            }},
    }},
    {"Miscellaneous", "", false, {
        {{"-h", "--help"},
            {},
            "Display usage.",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                print_usage_impl(false, arguments);
                exit(0);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-z", "--noise"},
            {"level"},
            "Set the noise level for marker observations.",
            false,
            {&bn_settings_t::noise},
            [](CALLBACK_ARGS)
            {
                std::string arg = *++ai;
                ensure(target)->noise = to<double>(arg);
                /*std::stringstream nz(*ai++);*/
                /*nz >> ensure(target)->noise;*/
                MSG_DEBUG("Set noise value to " << ensure(target)->noise << " from arg <" << arg << '>');
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-n", "--name"},
            {"population name"},
            "The name of this population (will also be used to create the output filename)",
            false,
            {true},
            [](CALLBACK_ARGS) {
                ensure(target)->pop_name = *++ai;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

//        {{"-g", "--qtl-generation"},
//            {"generation name"},
//            "The generation on which the traits are observed",
//            false,
//            {true},
//            [](CALLBACK_ARGS) {
//                ensure(target)->qtl_generation_name = *++ai;
//                SAFE_IGNORE_CALLBACK_ARGS;
//            }},

        {{"-wd", "--work-directory"},
            {"path"},
            "Path to directory for output files",
            false,
            {&bn_settings_t::work_directory},
            [](CALLBACK_ARGS) {
                ensure(target)->work_directory = *++ai;
                if (!check_file(ensure(target)->work_directory, true, true, false)) {
                    ensure_directories_exist(ensure(target)->work_directory);
                    check_file(ensure(target)->work_directory, true, true, true);
                }
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

    }},
    {"Job control", "Select and configure the job control scheme", false, {
        {{"-mt", "--dispatch-multithread"},
            {"n_threads"},
            "Use single-machine, multi-threading.",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                if (ensure(target)->scheme != JDS_None) {
                    MSG_ERROR("Only one job control scheme should be specified.", "Specify only one job control scheme in the commandline.");
                } else {
                    ensure(target)->scheme = JDS_MT;
                }
                ensure(target)->n_threads = to<int>(*++ai);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
        {{"-ssh", "--dispatch-SSH"},
            {"comma-separated host list"},
            "Use SSH for job dispatch.",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                if (ensure(target)->scheme != JDS_None) {
                    MSG_ERROR("Only one job control scheme should be specified.", "Specify only one job control scheme in the commandline.");
                } else {
                    ensure(target)->scheme = JDS_SSH;
                    std::string hosts(*++ai);
                    std::istringstream iss(hosts);
                    std::string host;
                    while (std::getline(iss, host, ',')) {
                        ensure(target)->ssh_hosts.emplace_back(host);
                    }
                }
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
        {{"-sge", "--dispatch-SGE"},
            {"n_jobs", "qsub options"},
            "Use SGE for job dispatch. Use '-' if you don't wish to provide any specific option.",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                if (ensure(target)->scheme != JDS_None) {
                    MSG_ERROR("Only one job control scheme should be specified.", "Specify only one job control scheme in the commandline.");
                } else {
                    ensure(target)->scheme = JDS_SGE;
                }
                ensure(target)->n_threads = to<int>(*++ai);
                std::string opts = *++ai;
                if (opts != "-") {
                    ensure(target)->qsub_opts = opts;
                }
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
    }},
    {"Inputs", "Input files and configuration of observations. You may use either a pedigree file in CSV format or a breeding design specification if you don't have pedigree data.\nThere are two essential parameters to compute the genotype probabilities: the number of ancestors and the number of observed alleles (for SNP observations). The number of ancestors is automatically computed from the given pedigree or breeding design specification. The number of alleles is computed from the marker observation specifications.", false, {
        {{"-mos", "--marker-observation-spec"},
            {"path"},
            "Path to a marker observation specification file.",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                std::string filename = *++ai;
                check_file(filename, false, false);
                /*ensure(target)->marker_observation_specs_filename = *++ai;*/
                /*ifile ifs(filename);*/
                /*ensure(target)->marker_observation_specs = read_format(filename, ifs);*/
                /*read_format(ensure(target)->marker_observation_specs, filename, ifs);*/
                std::ifstream ifs(filename); // text mode OK as marker_obs_formats::add_format(ifs) uses a json parser (see json.hpp) 
                marker_obs_formats::add_format(ifs);
                ensure(target)->marker_observation_specs_filenames.push_back(filename);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
/*
        {{"-p", "--pedigree"},
            {"path"},
            "Path to the output from spell-pedigree.",
            false,
            {},
            [](CALLBACK_ARGS)
            {
                if (ensure(target)->design_filename != "") {
                    MSG_ERROR("You must specify either a pedigree or a design, but not both.", "Don't specify the breeding design if you have pedigree data available.");
                }
                std::string filename = *++ai;
                check_file(filename, false, false);
                ensure(target)->pedigree_filename = filename;
                ensure(target)->pedigree.load(filename, false);
                MSG_DEBUG_INDENT_EXPR("[Pedigree] ");
                MSG_DEBUG("loaded from " << ensure(target)->pedigree_filename);
                MSG_DEBUG("" << ensure(target)->pedigree);
                MSG_DEBUG_DEDENT;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
*/

//        {{"-d", "--breeding-design"},
//            {"path"},
//            "Path to the breeding design specification file. The pedigree data will be created assuming one individual per unobserved generation.\nIf multiple generations are observed, please provide pedigree data instead.",
//            false,
//            {},
//            [](CALLBACK_ARGS)
//            {
//                if (ensure(target)->design_filename != "") {
//                    MSG_ERROR("You must specify either a pedigree or a design, but not both.", "Don't specify the breeding design if you have pedigree data available.");
//                }
//                std::string filename = *++ai;
//                check_file(filename, false, false);
//                /*ensure(target)->pedigree_filename = filename;*/
//                /* TODO FIXME */
//                SAFE_IGNORE_CALLBACK_ARGS;
//            }},
        {{"-m", "--marker-obs"},
            {"gen:format", "path"},
            "Path to the marker observations file of generation 'gen' with given format. This file must have as many individuals as the pedigree has for that generation.",
            false,
            {true},
            [](CALLBACK_ARGS)
            {
                std::string gen_and_format = *++ai;
                std::string filename(*++ai);
                size_t colon = gen_and_format.find_first_of(':');
                if (colon == std::string::npos) {
                    MSG_ERROR("No format was specified for the observations of generation " << gen_and_format << '.', "Specify observations format name along with the generation");
                    return;
                }
                std::string gen_name(gen_and_format.cbegin(), gen_and_format.cbegin() + colon);
                std::string format_name(gen_and_format.cbegin() + 1 + colon, gen_and_format.cend());
                /*if (!ensure(target)->design->generation[gen_name]) {*/
                    /*MSG_ERROR("Generation '" << gen_name << "' isn't defined.", "Specify a generation name that is defined in the breeding design XML file");*/
                /*}*/
#ifdef _WIN32
                // on Windows plateform, filename may start with the volume name (e.g. "C:")
				// Therefore we have to skip the volume name when calling filename.find_first_of(':')
				size_t skip_size = 0;
				// 1 - get the volume name
                char volume_name[BUFSIZE];
                if( GetVolumePathName(filename.c_str(), volume_name, sizeof(volume_name)) ){
				    std::string volumeName(volume_name);
                    // remove ending '\' the from volume name, e.g.: "C:\" => "C:"
                    if (volumeName.back() == '\\') volumeName.pop_back();
				    // 2 - if filename starts with volumeName, set skip_size to volumeName size
                    if (filename.rfind(volumeName, 0) == 0) skip_size = volumeName.size();
                }
				size_t colon1 = filename.find_first_of(':', skip_size);
#else
                size_t colon1 = filename.find_first_of(':');
#endif
                int start = -1;
                int end = -1;
                if (colon1 != std::string::npos) {
                    size_t colon2 = filename.find_first_of(':', colon1 + 1);
                    if (colon2 != std::string::npos) {
                        std::stringstream s1(filename.substr(colon1 + 1, colon2 - colon1 - 1));
                        std::stringstream s2(filename.substr(colon2 + 1));
                        s1 >> start;
                        s2 >> end;
                    } else {
                        std::stringstream s1(filename.substr(colon1 + 1));
                        s1 >> start;
                        end = start;
                    }
                    filename.resize(colon1);
                }
                if (check_file(filename, false, false)) {
                    ifile ifs(filename); // text mode OK (file.raw)
                    ensure(target)->observed_mark[gen_name] = {
                        filename, format_name, 0,
                        read_data::read_marker(ifs, start, end),
                        {}};
                }
                ensure(target)->input_generations.emplace_back(gen_name);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
        {{"-o", "--output-generations"},
            {"comma-separated list"},
            "Specifies the list of variables to extract after the computation.\nThe state probabilities for all individuals in the given generations will be extracted and made available for spell-qtl.",
            false,
            {"all generations"},
            [](CALLBACK_ARGS)
            {
                std::string og(*++ai);
                std::istringstream iss(og);
                std::string ogen;
                while (std::getline(iss, ogen, ',')) {
                    ensure(target)->output_generations.emplace_back(ogen);
                }
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
    }},
    {"Outputs", "Set the output mode. By default, only the population data file will be written. If you specify -O1, only the 1-point Parental Origin Probabilities will be written, unless you also specify -Op.", false, {
            {{"-Og", "--output-gamete-data"},
             {},
             "Output the population data file for use in spell-qtl. This is the default behaviour.",
             false,
             {},
             [](CALLBACK_ARGS)
             {
                 ensure(target)->output_mode |= bn_settings_t::OutputGametes;
                 SAFE_IGNORE_CALLBACK_ARGS;
             }},
            {{"-Op", "--output-population-data"},
             {},
             "Output the population data file for use in spell-qtl. This is the default behaviour.",
             false,
             {},
             [](CALLBACK_ARGS)
             {
                 ensure(target)->output_mode |= bn_settings_t::OutputPopData;
                 SAFE_IGNORE_CALLBACK_ARGS;
             }},
            {{"-O1", "--output-one-point-prob"},
             {},
             "Output the 1-point Parental Origin Probabilities. This will disable the output of the population data file unless -Op is also used.",
             false,
             {},
             [](CALLBACK_ARGS)
             {
                 ensure(target)->output_mode |= bn_settings_t::OutputOnePoint;
                 SAFE_IGNORE_CALLBACK_ARGS;
             }},
    }},
};


constraint_t<bn_settings_t> checks[] = {
        {
                "No marker to work on.",
                "Specify at least one genotype data file",
                CONSTRAINT_PREDICATE(bn_settings_t) {
                    return s->marker_names.size() > 0;
                }
        },
};


argument_parser<bn_settings_t> arg_map(arguments);


bn_settings_t* bn_settings_t::from_args(int argc, const char** argv)
{
    std::vector<const char*> args(argv + 1, argv + argc);
    std::vector<const char*>::iterator ai = args.begin();
    std::vector<const char*>::iterator aj = args.end();
    char* argv0 = strdup(argv[0]); //// Franck Gauthier 2020-12-15
    //::prg_name(basename(argv[0]));
    ::prg_name(basename(argv0));

    bn_settings_t* ret = NULL;

    while (ai != aj && arg_map(ret, ai, aj)) {
        ++ai;
    }

    if (ret) {
        //ret->prg_name = basename(argv[0]);
        ret->prg_name = basename(argv0);
        ret->command_line.assign(argv, argv + argc);
        for (const auto& kv: ret->observed_mark) {
            ret->marker_observation_specs[kv.second.format_name] = marker_obs_formats::get_format(ret->pedigree, kv.second.format_name);
        }
        if (ret->output_mode == 0) {
            ret->output_mode = bn_settings_t::OutputPopData;
        }
    }
    if(argv0 != NULL) free(argv0); //// Franck Gauthier 2020-12-15
    return ret;
}

void print_usage() { print_usage_impl(false, arguments); }

#include "output_impl.h"
