/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pedigree.h"
/*#include "bayes/factor_var4.h"*/
#include "bayes/output.h"
#include "cli.h"
#include "dispatch.h"
#include <fenv.h>
#include "input/marker_obs_formats.h"
#include "output_impl.h"

void print_usage();


#ifndef SPELL_BAYES_MAIN
#  define SPELL_BAYES_MAIN main
#endif

int SPELL_BAYES_MAIN(int argc, const char** argv)
{
    // ça fait planter R… oO
    // et c'est probablement inutile maintenant qu'on utilise GMP…
//     feenableexcept(FE_ALL_EXCEPT & ~FE_INEXACT);

    msg_handler_t::set_color(true);
    msg_handler_t::debug_enabled() = false;
    bn_settings_t* settings = NULL;
    try {
        settings = bn_settings_t::from_args(argc, argv);
        if (msg_handler_t::check(false)) {
            return -1;
        }
    } catch (file::error& fe) {
        MSG_ERROR("An error happened while reading input files. " << fe.what(), "");
        return -1;
    }
    if (!settings) {
        print_usage();
    } else {
        std::vector<std::string> cli(argv, argv + argc);
        //MSG_INFO("Commandline: " << cli);
        settings->pedigree_filename = settings->work_directory + "/" + settings->pop_name + ".cache/" + settings->pop_name + ".spell-pedigree.data";
        if (!check_file(settings->pedigree_filename, false, false)) {
            MSG_ERROR("Couldn'f find the spell-pedigree output file.", "Please run spell-pedigree first with the same name and work_directory parameters.");
            return -1;
        }
        settings->pedigree.load(settings->pedigree_filename, false);
        //MSG_DEBUG_INDENT_EXPR("[Pedigree] ");
        //MSG_DEBUG("loaded from " << settings->pedigree_filename);
        //MSG_DEBUG("" << settings->pedigree);
        //MSG_DEBUG_DEDENT;
        //MSG_INFO("Output gen: " << settings->output_generations);
        
        //MSG_INFO("settings->output_generations.size = " << settings->output_generations.size());
        //MSG_INFO("settings->output_mode & OutputGametes = " << (settings->output_mode & bn_settings_t::OutputGametes));
        if (settings->output_generations.size() == 0 || (settings->output_mode & bn_settings_t::OutputGametes)) {
            settings->output_generations.clear();
            settings->output_generations.reserve(settings->pedigree.generations.size());
            for (const auto& gen: settings->pedigree.generations) {
                if (gen) {
                    settings->output_generations.emplace_back(gen->name);
                }
            }
        }

        //MSG_INFO("Output gen: " << settings->output_generations);
        std::sort(settings->output_generations.begin(), settings->output_generations.end());
        auto it = std::unique(settings->output_generations.begin(), settings->output_generations.end());
        settings->output_generations.resize(it - settings->output_generations.begin());


        for (const auto& kv: settings->observed_mark) {
            settings->marker_observation_specs[kv.second.format_name]
                    = marker_obs_formats::get_format(settings->pedigree, kv.second.format_name);
        }

        std::map<std::string, ObservationDomain> obs_gen;
        for (const auto& kv: settings->observed_mark) {
            obs_gen[kv.first] = settings->marker_observation_specs[kv.second.format_name].domain;
            //MSG_DEBUG("OBSERVED GEN " << kv.first << ": " << obs_gen[kv.first]);
        }

        /*
        size_t n_alleles = 1;
        for (const auto& kv: settings->marker_observation_specs) {
            if (kv.second.domain == ODAllele) {
                if (n_alleles == 1) {
                    n_alleles = kv.second.domain_size;
                } else {
                    MSG_ERROR("Multiple and inconsistent formats (domain: allele)", "Consolidate your data and use at most ONE format per domain (alleles/ancestors) for your marker observations");
                }
            }
            settings->compiled_obs_specs[kv.first] = kv.second.compile();
        }
        for (const auto& kvfmt: settings->compiled_obs_specs) {
            MSG_DEBUG("COMPILED FORMAT " << kvfmt.first);
            for (const auto& kvscore: kvfmt.second) {
                MSG_DEBUG("  score '" << kvscore.first << "': " << kvscore.second.transpose());
            }
        }

        pedigree_bayesian_network bn = make_bn(pedigree, obs_gen, n_alleles, settings->noise, settings->tolerance);
        settings->bn = &bn;
        */

        std::set<std::string> tmp;
        for (const auto& obs: settings->observed_mark) {
            for (const auto& kv: obs.second.observations.data) {
                tmp.insert(kv.first);
            }
        }
        settings->marker_names.assign(tmp.begin(), tmp.end());
        /*MSG_DEBUG("MARKER NAMES " << settings->marker_names);*/

        constraint_t<bn_settings_t>::check(settings);

        for (size_t mn = 0; mn < settings->marker_names.size(); ++mn) {
            settings->marker_index[settings->marker_names[mn]] = mn;
        }

        if (settings->is_master()) {
            if (do_the_job(settings, "compute-alleles-per-marker")) {
                size_t n_mark = settings->count_markers();
                settings->alleles_per_marker.clear();
                settings->alleles_per_marker.resize(n_mark);
                std::set<size_t> unique_n_alleles;
                for (size_t mark = 0; mark < n_mark; ++mark) {
                    // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
                    ifile result(settings->job_filename("compute-alleles-per-marker", mark), std::ios_base::in | std::ios_base::binary);
                    /*auto& apm = settings->alleles_per_marker[mark];*/
                    std::set<char> temp;
                    rw_base()(result, temp);
                    unique_n_alleles.insert(temp.size());
                    /*MSG_DEBUG("Read an allele count of " << settings->alleles_per_marker[mark].size());*/
                    //MSG_DEBUG("Read an allele count of " << temp.size());
                }
                settings->unique_n_alleles.assign(unique_n_alleles.begin(), unique_n_alleles.end());
                {
                    // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
                    ofile unafs(settings->job_filename("unique_n_alleles", 0), std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);
                    rw_base()(unafs, settings->unique_n_alleles);
                }
                //MSG_DEBUG("Computed n_alleles: " << settings->unique_n_alleles);
                if (do_the_job(settings, "compute-factor-graphs")
                        && do_the_job(settings, "compute-LV")
                        && (settings->output_mode & bn_settings_t::OutputPopData)
                        && do_the_job(settings, "collect-LV")) {
                    // AVOID A BUG on Windows => ALWAYS open binary files in binary mode (not text mode)
                    ifile output(settings->job_filename("output", 0), std::ios_base::in | std::ios_base::binary);
                    std::string filename;
                    rw_base() (output, filename);
     //               settings->closing_messages.push_back(SPELL_STRING(GREEN << "Marker data for population `" << settings->pop_name << "' written in file " << WHITE << filename << NORMAL << std::endl));
                    settings->closing_messages.push_back(SPELL_STRING(GREEN << "Marker data written in file " << WHITE << filename << NORMAL << std::endl));
                }
            }
            /*do_the_job(settings, "dummy-one");*/
            /*do_the_job(settings, "dummy");*/
            /*do_the_job(settings, "dummy-one");*/
            settings->cleanup_job_files();
            msg_handler_t::cout() << std::endl << std::endl;
            for (auto& msg: settings->closing_messages) {
                msg_handler_t::cout() << msg;
            }
            MSG_QUEUE_FLUSH();
        } else {
            (void) do_the_job(settings, settings->job_name);
        }

        delete settings;
    }
    return 0;
}


