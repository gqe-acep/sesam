#DEFINES=-DSPELL_PEDIGREE_MAIN=spell_pedigree -DSPELL_BAYES_MAIN=spell_marker -DSPELL_UNSAFE_OUTPUT -DEIGEN_NO_DEBUG -DNDEBUG -D_GNU_SOURCE -D_GLIBCXX_USE_CXX11_ABI=1 -D_GLIBCXX_ASSERTIONS -D_GLIBCXX_DEBUG
DEFINES=-DSPELL_PEDIGREE_MAIN=spell_pedigree -DSPELL_BAYES_MAIN=spell_marker -DSPELL_UNSAFE_OUTPUT -DEIGEN_NO_DEBUG -DNDEBUG -D_GNU_SOURCE -D_GLIBCXX_USE_CXX11_ABI=1 -D_GLIBCXX_ASSERTIONS

CXX_STD=CXX14

CXX14STD="-std=c++14"

#GCC_ARGS=-I . -I ./include -I ./include/RWrap -I ./include/eigen3 -I ./include/bayes -I ./include/input -pthread $(DEFINES) -O3 -pthread -fsanitize=undefined -g -Wmaybe-uninitialized
GCC_ARGS=-I . -I ./include -I ./include/RWrap -I ./include/eigen3 -I ./include/bayes -I ./include/input -pthread $(DEFINES) -O1 -pthread -ggdb

PKG_CXX14FLAGS=$(GCC_ARGS)
PKG_CXXFLAGS=$(GCC_ARGS)
PKG_CFLAGS=$(GCC_ARGS)
CXX14FLAGS=$(GCC_ARGS)
CXXFLAGS=$(GCC_ARGS)
CFLAGS=$(GCC_ARGS)
PKG_LIBS=-lgmp -pthread -ggdb
#-lboost_system -lboost_filesystem

## -lubsan required for -fsanitize=undefined
#PKG_LIBS=-lubsan -lgmp -pthread -fsanitize=undefined

LD=g++

TARGET=spellmaptools

SRC_SRC=static_data.cc \
        bayes/main.cc bayes/cli.cc bayes/jobs.cc bayes/dispatch.cc \
        pedigree/cli.cc pedigree/main.cc \
        input/read_mark.cc \
        dlfcn_win/dlfcn_win.cc
        
DOT_SRC=$(TARGET).cc

ALL_SRC=$(DOT_SRC) $(SRC_SRC)

OBJECTS=$(ALL_SRC:.cc=.o)

### debug with gdb. compile with -ggdb. and run :
#R -d gdb -batch-silent -f loop.R
#(gdb) catch throw std::bad_alloc # or catch throw # or break std::bad_alloc 
#(gdb) set print thread-events off
#(gdb) r

